<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH.'third_party/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
use Dompdf\Options;

class Dpdf extends DOMPDF{
    function __construct(){
        $options = new Options();
        $options->set('enable_html5_parser', true);
        $options->set('isRemoteEnabled', true);
        parent::__construct($options);
    }
}

?>

