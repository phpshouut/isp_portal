<?php
$user_active = ''; $plan_active = ''; $topup_active=''; $nas_active=''; $report_active="";
$setting_open = '';
$setting_mgnt_team_user_active = ''; $setting_biling_active = '';
$locations=''; $department=''; $team=''; $myacct_active=''; $isp_active=""; $promotion_open = '';
$home_promotion = ''; $marketing_promotion = ''; $router_setup=''; $team_ticketing = '';

$controller = $this->router->fetch_class();
$method=$this->router->fetch_method();
//$navperm=$this->plan_model->leftnav_permission();
//echo "<pre>"; print_R($navperm); die;
if($controller == 'user'){
    $user_active = 'class="active"';
}elseif($controller == 'plan'){
    if($method=="topup" || $method=="add_topup" || $method=="edit_topup" || $method=="topupuser_stat" || $method=="topup_stat")
    {
    $topup_active = 'class="active"';
    }
    else
    {
         $plan_active = 'class="active"';
    }
}
else if($controller == 'nas')
{
    $nas_active='class="active"';
}
else if($controller == 'report')
{
   $report_active='class="active"'; 
}
else if($controller == 'setting'){
    $setting_open = 'open';
    if($method == 'index' || $method == ''){
        $setting_biling_active='class="active"';
    }
    elseif($method == 'user_management'){
        $setting_mgnt_team_user_active='class="active"';
    }
	 elseif($method == 'locations'){
        $locations = 'class="active"';
    }
    
     elseif($method == 'isp_detail'){
        $isp_active = 'class="active"';
    }


}
else if($controller == 'mgmt')
{
	$setting_open = 'open';
    $department='class="active"';
}

else if($controller == 'team')
{
	$setting_open = 'open';
   // $team='class="active"';
     if($method=="my_account")
    {
    $myacct_active = 'class="active"';
    }
    else
    {
         $team = 'class="active"';
    }
}
else if($controller == 'promotion'){
    $promotion_open = 'open';
    if($method == 'index' || $method == ''){
        $home_promotion='class="active"';
    }
    elseif($method == 'marketing_promotion'){
 $marketing_promotion='class="active"';
    }
    

}
else if($controller == 'ppoe')
{
$router_setup = 'class="active"';
}


$super_admin = $this->session->userdata['isp_session']['super_admin'];

if($controller == 'ttask'){
    $team_ticketing = 'class="active"';
    $isp_license_data = $this->ttask_model->isp_license_data();
}else{
    $isp_license_data = $this->user_model->isp_license_data();
}
if($isp_license_data != 0){
?>
<div class="menu-container">
                     <div class="crbnMenu">
                        <ul class="menu">
                          <li>
                            <a href="<?php echo base_url().'user'?>"  <?php echo $user_active?>><i class="fa fa-user" aria-hidden="true"></i> Users</a>
                            </li>
                            <?php if(isset($navperm['PLANS']) && $navperm['PLANS']==0){?>
                             <li>
                         <a href="<?php echo base_url().'plan'?>" <?php echo $plan_active?>><i class="fa fa-wifi" aria-hidden="true"></i> Plans</a>
                            </li>
                             <?php } ?>
                             <?php if(isset($navperm['TOPUP']) && $navperm['TOPUP']==0){?>
                            <li>
        <a href="<?php echo base_url().'plan/topup'?> " <?php echo $topup_active?>> <i class="fa fa-database" aria-hidden="true"></i> 
        Top Up
        </a>
       </li>
        <?php } ?>
       <?php if(isset($navperm['NAS']) && $navperm['NAS']==0){?>
       <li>
        <a href="<?php echo base_url().'nas'?>"  <?php echo $nas_active?>><i class="fa fa-server" aria-hidden="true"></i>
         Nas
        </a>
       </li>
       <?php } ?>
        <?php if(isset($navperm['REPORT']) && $navperm['REPORT']==0){?>
       <li>
          <a href="<?php echo base_url().'report'?>" <?php echo $report_active; ?>><i class="fa fa-file-text" aria-hidden="true"></i> Reports</a>
       </li>
        <?php } ?>
	<li>
          <a href="<?php echo base_url().'ttask'?>"  <?php echo $team_ticketing; ?>><i class="fa fa-rocket" aria-hidden="true"></i> Team Tasking</a>
       </li>
                           <li>
                              <a class="nav-link" href="#">
                              <span><i class="fa fa-cog" aria-hidden="true"></i>
Setup</span> <span class="menu-toggl caret"></span>
                              </a>
                              <ul id="setting_menu_ul">
                                  
                                 <?php if(isset($navperm['LOCATION']) && $navperm['LOCATION']==0){?>
                                 <li id="location_menu">
                                    <a href="<?php echo base_url().'setting/locations'?>" <?php echo $locations?>>
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
         Locations
         </a>
                                 </li>
                                  <?php } ?>
                                  <?php if(isset($navperm['DEPARTMENT']) && $navperm['DEPARTMENT']==0){?>
                                 <li id="team_menu">
                                    <a href="<?php echo base_url().'mgmt'?>"  <?php echo $department ?>>
           <i class="fa fa-table" aria-hidden="true"></i>
          Teams/Franchise
                                    </a>
                                 </li>
                                   <?php } ?>
				   
				   
                                  <li id="ppoe_router">
                                    <a href="<?php echo base_url().'ppoe'?>"  <?php echo $router_setup ?>>
           <i class="fa fa-wrench" aria-hidden="true"></i>
         Router Setup
                                    </a>
                                 </li>
                                
                                  
                              </ul>
                           </li>
                            <?php if(isset($navperm['PROMOTION']) && $navperm['PROMOTION']==0){?>
                           <li>
                              <a class="nav-link" href="#" <?php echo $promotion_open?>>
                              <span><i class="fa fa-bullhorn" aria-hidden="true"></i>
Promotions </span> <span class="menu-toggl caret"></span>
                              </a>
                              <ul id="promotion_menu_ul">
                                 <li id="home_menu">
                                    <a  href="<?php echo base_url().'promotion'?>"   <?php echo $home_promotion?>>
          <i class="fa fa-picture-o" aria-hidden="true"></i>
Home Page
                                    </a>
                                 </li>
                                   <li id="promotion_menu">
                                    <a  href="<?php echo base_url().'promotion/marketing_promotion'?>"   <?php echo $marketing_promotion?>>
           <i class="fa fa-picture-o" aria-hidden="true"></i>
Promotion
                                     </a>
                                 </li>
                                 
                                 
                              </ul>
                           </li>  <?php } ?>
                           <?php if(isset($navperm['PUBLICWIFI']) && $navperm['PUBLICWIFI']==1){?>
                            <li id="">
                                    <a  href="<?php echo base_url().'publicwifi'?>" style="background-color:#cccccc; color:#36465f; box-shadow: none; font-weight:600; text-transform:inherit; padding: 10px 22px"  >
       <i class="fa fa-exchange" aria-hidden="true"></i>
 Public Wifi
    <i class="fa fa-angle-right pull-right" aria-hidden="true"></i>
                                     </a>
                                 </li>
                           <?php } ?>
			    
                        </ul>
                     </div>
                  </div>



<?php
}else{
    
}

?>

<div class="footer">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <div class="row">
            <h5><?php echo isset($this->session->userdata['isp_session']['isp_name']) ? $this->session->userdata['isp_session']['isp_name'] : $this->session->userdata['isp_session']['username']; ?> <i class="fa fa-angle-right" aria-hidden="true"></i></h5>
            <h5><small>IP: <?php echo $_SERVER['REMOTE_ADDR'] ?></small></h5>
            <h5><small><?php echo date('d.m.y h:i') ?></small></h5>
            <!--h5 style="font-size: 13px">Powered By:</h5-->
        </div>
    </div>
    <!--div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <div class="row">
            <img src="<?php echo base_url() ?>assets/images/shouut_logo.png" class="img-responsive"/>
        </div>
    </div-->	
</div>


