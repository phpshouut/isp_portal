<?php
$CI =& get_instance();
if( ! isset($CI)){
    $CI = new CI_Controller();
}
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>DECIBEL | ISP</title>
      <!-- Font Awesome -->
      <link href="./assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="./assets/css/mui.min.css" rel="stylesheet">
      <link href="./assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="container-fluid" style="overflow: hidden">
         <div class="row">
            <div class="wapper">
               <div class="mui--appbar-height"></div>
               <div class="mui-container-fluid" id="right-container-fluid">
                  <div class="add_user">
                     <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                           <div class="mui--appbar-height"></div>
                           <div class="mui--appbar-height"></div>
                           <div class="mui--appbar-height"></div>
                           <div class="col-sm-12">
                              <div class="col-sm-6" style="border-right: 1px solid #d0d0d1">
                                 <center><img src="./assets/images/error-img.png" alt="" class="img-responsive pull-right"></center>
                              </div>
                              <div class="col-sm-6">
                                 <div class="col-sm-12 col-md-offset-1" style="padding-top: 25px;">
                                    <h1 style="font-size: 35px; margin:15px 0px; font-weight:500; color:#f00f64">Oops!</h1>
                                    <br/>
                                    <p style="font-size:14px; font-weight:600">We can't seem to find the page you're looking for. </p>
                                    <div class="error-btn" style="margin-top: 20px;">
                                       <a href="<?php echo base_url(); ?>" class="mui-btn mui-btn--small mui-btn--accent" style="padding:15px; line-height:8px; font-weight:600; border-radius:0px;height:40px;border-bottom:4px solid #333">GO TO HOMEPAGE</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- JQuery -->
      <script type="text/javascript" src="./assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="./assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="./assets/js/mui.min.js"></script>
      <script>
         $(document).ready(function() {
           var height = $(window).height();
            $('#main_div').css('height', height);
           $('#right-container-fluid').css('height', height);
         });
      </script>
   </body>
</html>
