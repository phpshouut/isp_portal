 <!-- Start your project here-->
 <?php $this->load->view('includes/header');?>
      <div class="loading hide" >
	   <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                     <img src="<?php echo base_url() ?>assets/images/decibel.png" class="img-responsive" />
                     </span>
                  </div>
                 <?php $this->view('left_nav'); ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">NAS</a>
                        </div>
                 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
                         <?php if($this->plan_model->is_permission(NAS,'ADD')){ ?>     
                        <li>
                                 <a href="#">
                                <button class="mui-btn mui-btn--small mui-btn--accent addnas">
                                 + ADD NAS
                                 </button>
                                 </a>
                         </li> <?php } ?>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
                <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                       <div class="right_side" style="height:auto; padding-bottom: 0px;">
                     
					  </div>
                     <div class="add_user" style="padding-top:0px;">
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12">
   <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8">
         <h1 id="search_count"></h1>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 pull-right">
         <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 14px;">
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon">
                     <i class="fa fa-search" aria-hidden="true"></i>
                  </div>
                  <input type="text" class="form-control" placeholder="Search nas" onBlur="this.placeholder='Search nas'" onFocus="this.placeholder=''"  id="searchtext">
                  <span class="searchclear" id="searchclear">
                  <img src="<?php echo base_url() ?>assets/images/clear.svg"/>
                  </span>
               </div>
               <label class="search_label">You can look up by Nas Name</label>
            </div>
         </div>
      </div>
   </div>
                                  
                                   <div class="row" id="onefilter">
      <div class="col-lg-8 col-md-8 col-sm-8">
         <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select name="filter_state" onchange="search_filter(this.value, 'state')">
                     <option value="all">All States</option>
                     <?php $this->user_model->state_list(); ?>
                  </select>
                  <label>State</label>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select class="search_citylist"  onchange="search_filter(this.value, 'city')"  name="filter_city">
                     <option value="all">All Cities</option>
                  </select>
                  <label>City</label>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select  onchange="search_filter()" class="search_zonelist"  name="filter_zone">
                     <option value="all">All Zones</option>
                     <?php //$this->user_model->zone_list(); ?>
                  </select>
                  <label>Zone</label>
               </div>
            </div>
         </div>
      </div>
     
   </div>
</div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                             <div class="row">
                              <div class="mui--appbar-height"></div>
                          		<div class="mui-tabs__pane mui--is-active" id="pane-default-1">
							      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                      <div class="row">
                                       <div class="table-responsive">
                                          <table class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>NAS NAME</th>
                                                  
                                                   <th>NAS TYPE</th>
                                                   <th>NAS IPADDR</th>
                                                   <th>NAS STATE</th>
                                                   <th>NAS CITY</th>
                                                   <th>NAS ZONE</th>
                                                   <th>STATUS</th>
                                                   <th colspan="2" class="mui--text-right">ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody id='search_gridview'>
                                                
                                                 <?php 
                                                  $i=1;
                                                   $is_editable=$this->plan_model->is_permission(NAS,'EDIT');
                                                    $is_deletable=$this->plan_model->is_permission(NAS,'DELETE');
                                                 foreach($nas_listing as $valdata){
                                                     
                                                     $state = $this->user_model->getstatename($valdata->nasstate);
                                                    $city = $this->user_model->getcityname($valdata->nasstate, $valdata->nascity);
                                                    $zone = $this->user_model->getzonename($valdata->naszone);
                                                  //  $status = ($valdata->status == 1)?'active':'inactive';
                                                     ?>
                                               <tr>
												   <td><?php echo $i;?>.</td>
                                                                                                   <td><a href="javascript:void(0);" class="editnas" rel="<?php echo $valdata->id;?>"><?php echo $valdata->shortname;?></a></td>
												   
												   <td><?php echo ($valdata->nastype==1)?"Microtik":"Others";?></td>
												   <td><?php echo $valdata->nasname;?></td>
												   <td> <?php echo $state;?> </td>
												   <td><?php echo $city;?></td>
												   <td><?php echo $zone;?></td>
                                                                                                   <?php $up = $this->nas_model->ping($valdata->nasname);
                                                                                                   if($up==0)
                                                                                                   {
                                                                                                      
                                                                                                       $nsimg='<img src="'.base_url().'assets/images/on2.png" >';
                                                                                                   }
                                                                                                   else
                                                                                                   {
                                                                                                     $nsimg='<img src="'.base_url().'assets/images/off2.png" >';  
                                                                                                   }
                                                                                                   ?>
												 <td><?php echo $nsimg;?></td>
                                                                                                   <td><a href="javascript:void(0);" class="editnas" rel="<?php echo $valdata->id;?>">Edit</a></td> 
                                                                                                   <!-- <td><a href="javascript:void(0);" class="<?php echo($is_deletable)?'deletnas':'' ?>" rel="<?php echo $valdata->id;?>">Delete</a></td> -->
												 
                                                </tr>
                                                 <?php
                                                 
                                                 $i++;
                                                 } ?>
                                               
                                             </tbody>
                                          </table>
                                       </div>
									  </div>
                                    </div>
							    </div>
							
							   </div>
                           </div>
                            
                           <!--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
                              <li>
                                 
                                 <button class="mui-btn mui-btn--small mui-btn--accent addnas">
                                 + ADD NAS
                                 </button>
                                 </a>
                              </li>
                              </ul></div>-->
                        </div>
					   </div> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>    
      <!--- Close-two Modal --->
<div class="modal fade" tabindex="-1" role="dialog" id="addNasmodal" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">
               <strong>ADD Nas</strong>
            </h4>
         </div>
         <div class="modal-body">
            <form action="" method="post" id="add_nas" autocomplete="off" onsubmit="add_nas(); return false;">
                 <input type="hidden" name="nasid" value="">
               <div class="row">
                  <!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                     <h4>NasIp format</h4>
                     <label class="radio-inline">
                     <input type="radio" required name="nasipformat"  value="Static" > Static
                     </label>
                     <label class="radio-inline">
                     <input type="radio" required name="nasipformat"  value="DDNS" > DDNS
                     </label>
                  </div>-->
                   <input type="hidden" name="mobileno" value="9873834499">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label" style="margin-top: 25px;">
                              <input type="text" name="fqdn" class="fqdn">
                              <label>NasFqdn<sup>*</sup></label>
                           </div>
                        </div>-->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <label>IP Address<sup>*</sup></label>
                           <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin-right">
                                    <div class="mui-textfield mui-textfield--float-label" style="padding-right:10px">
                                       <input type="number" class="ip1 ipd" name="ip1" required>
                                       <span class="ip_dot">.</span>
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin-right" style="padding-left:2px">
                                    <div class="mui-textfield mui-textfield--float-label" style="padding-right:10px">
                                       <input type="number" class="ip2 ipd" name="ip2" required>
                                       <span class="ip_dot">.</span>
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin">
                                    <div class="mui-textfield mui-textfield--float-label" style="padding-right:10px">
                                       <input type="number" class="ip3 ipd" name="ip3" required>
                                       <span class="ip_dot">.</span>
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin-left">
                                    <div class="mui-textfield mui-textfield--float-label">
                                       <input type="number" class="ip4 ipd" name="ip4" required>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label">
                              <input type="text" name="nas_name" id="nasname" required>
                              <label>Nas Name</label>
                           </div>
                            <span class="errornas"></span>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="nastype" required>
                                  <option value="1">Microtik</option>
                                 
                                 
                              </select>
                              <label>NAS Type<sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label">
                              <input type="text" name="nas_secret" required>
                              <label>Nas Secret</label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="select">
                               
                              <select name="state" class='statelist form-control' required >
                                 <option value="">Select States</option>
                                 <?php $this->plan_model->state_list(); ?>
                              </select>
                                <input type="hidden" class="statesel" name="statesel" value="">
                              
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="">
                              
                              <select name="city" class="search_citylist form-control"  required >
                                 <option value="all">All Cities</option>
                              </select>
                                  <input type="hidden" class="citysel" name="citysel" value="">
                              
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="">
                             
                              <select name="zone" class="zone_list search_zonelist form-control" required>
                                 <option value="all">All Zones</option>
                            
                              </select>
                              
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="modal-footer">
                           <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                           <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="ADD NAS">
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <!-- /.modal-content -->
      </div>
   </div>
</div>
      <div class="modal fade" tabindex="-1" role="dialog" id="editNasmodal" data-backdrop="static" data-keyboard="false">
          
      </div>
      
      
      <div class="modal fade" id="Deletenas_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
          <input type="hidden" id="delnasid" value="">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>Are you sure you want to Delete Nas ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyes" >YES</button>
               </div>
            </div>
         </div>
      </div>
 
     
 
 <script type="text/javascript">
     $(document).ready(function(){
         
           var height = $(window).height();
        $('#main_div').css('height', height);
        $('#right-container-fluid').css('height', height);
         
         $(document).on('click','.addnas',function(){
             $('#editNasmodal').html('');
             $('#addNasmodal').modal('show');
             
         });
         
         
            $(document).on('change','.statelist',function(){
          
          var stateid=$(this).val();
          var $this=$(this);
          $(this).closest('.row').find('.statesel').val(stateid);
         // $this.closest('.row').find('.valregion').data('stateid',stateid);
           //$this.closest('.row').find('.valregion').data('cityid','all');
           // $this.closest('.row').find('.valregion').data('zoneid','all');
          $.ajax({
        url: base_url+'plan/getcitylist',
        type: 'POST',
        dataType: 'text',
        data: 'stateid='+stateid,
        success: function(data){
        
           $this.closest('.row').find('.search_citylist').empty();
             $this.closest('.row').find('.search_citylist').append(data);
        }
        
        
    });
         
      });
      
       $(document).on('change','.search_citylist',function(){
             if($(this).val()=="addc")
          {
            $('.city_text').val('');
            var state_id=$(this).closest('.row').find('.statelist').val();
            $('.state_id').val(state_id);
            $('#add_city').modal('show'); 
            return false;
          }
           //  $('.search_citylist').change(function(){
          var cityid=$(this).val();
          var stateid=$(this).closest('.row').find('.statelist').val();
          var $this=$(this);
          $(this).closest('.row').find('.citysel').val(cityid);
       
       
          $this.closest('.row').find('.valregion').data('cityid',cityid);
          $.ajax({
        url: base_url+'plan/getzonelist',
        type: 'POST',
        dataType: 'text',
        data: 'cityid='+cityid+'&stateid='+stateid,
        success: function(data){
         
           $this.closest('.row').find('.zone_list').empty();
             $this.closest('.row').find('.zone_list').append(data);
        }
        
        
    });
         
      });
      
       $(document).on('change', '.zone_list', function () {
            if ($(this).val() == "addz") {
                $('.zone_text').val('');
                var state_id = $(this).closest('.row').find('.search_citylist').val();
                var city_id = $(this).closest('.row').find('.search_citylist').val();
                $('.state_id').val(state_id);
                $('.city_id').val(city_id);
                $('#add_zone').modal('show');
                return false;
            }
            

        });
      
         
         
         $(document).on('click','.deletnas',function(){
             
             var delnasid=$(this).attr('rel');
             $('#delnasid').val(delnasid);
             $('#Deletenas_confirmation_alert').modal('show');
             
             
         });
         
         $(document).on('click','.delyes',function(){
           
           var nasid=$('#delnasid').val();
             $.ajax({
        url: base_url+'nas/delete_nas',
        type: 'POST',
        dataType: 'json',
        data: {nasid:nasid},
        success: function(data){
         
            $('#Deletenas_confirmation_alert').modal('hide');
      
        location.reload();
           
        
        }
    });
             
         });
         
         
          $(document).on('click','.editnas',function(){
            //$('#addNasmodal').html('');
           var nasid=$(this).attr('rel');
             $.ajax({
        url: base_url+'nas/edit_nas',
        type: 'POST',
        dataType: 'json',
        data: {nasid:nasid},
        success: function(data){
            $('#editNasmodal').html(data.html);
            $('#editNasmodal').modal('show')
         //alert(data);
           // $('#Deletenas_confirmation_alert').modal('hide');
      
       // location.reload();
           
        
        }
    });
             
         });
         
         $('body').on('click','#searchclear', function(){
      $('#searchtext').val('');
      search_filter();
      
   });
   
    $('body').on('keyup', '#searchtext', function(){
      search_filter();
   });
   
         
           $(document).on('change','input:radio[name="nasipformat"]',function(){
               if($(this).val()=="Static")
               {
                 $('.fqdn').attr('required',false);
                  $('.fqdn').prop('disabled',true);
                   $('.fqdn').val('');
                  $('.ip1').prop('disabled',false);
                  $('.ip2').prop('disabled',false);
                   $('.ip3').prop('disabled',false);
                    $('.ip4').prop('disabled',false);
              $('.ip1').attr('required',true);
              $('.ip2').attr('required',true);
              $('.ip3').attr('required',true);
              $('.ip4').attr('required',true);   
               }
               else if($(this).val()=="DDNS")
               {
                  $('.fqdn').attr('required',true);
              $('.ip1').attr('required',false);
              $('.ip2').attr('required',false);
              $('.ip3').attr('required',false);
              $('.ip4').attr('required',false); 
              $('.ip1').val('');
              $('.ip2').val('');
              $('.ip3').val('');
              $('.ip4').val('');
                $('.fqdn').prop('disabled',false);
                  $('.ip1').prop('disabled',true);
                  $('.ip2').prop('disabled',true);
                   $('.ip3').prop('disabled',true);
                    $('.ip4').prop('disabled',true);
               }
           
           });
           
           $(document).on('keyup','.ipd',function(){
              // alert($(this).val().length);
               if($(this).val().length===3)
               {
                 
                   $(this).parent().parent().next().find('.ipd').focus();
               }
           });
         
     });
     
     
    /* function add_nas()
     {
         alert("aaaa");
     }*/
     
    function add_nas()
     {
      var nasname=$('#nasname').val();
    if(nasname.split(" ").length > 1){
        // alert('ssss');
        $('.errornas').html("Nas Name can't have space");
   return false;
}
else
{
     $('.errornas').html("");
}


        $('.loading').removeClass('hide');
         $('#addNasmodal').modal('hide');
         var formdata = $("#add_nas").serialize();
         $.ajax({
        url: base_url+'nas/add_nas_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
          
           
          // alert(data);
        location.reload();
        
        
        }
    });
         //alert(formdata);
     }
     
     function edit_nas()
     {
      var nasname=$('#editnasname').val();
     // alert(nasname);
    if(nasname.contains(' ')){
        $('.errornas').html("Nas Name can't have space");
   return false;
}
else
{
     $('.errornas').html("");
}

        $('.loading').removeClass('hide');
         $('#editNasmodal').modal('hide');
         var formdata = $("#edit_nas").serialize();
         $.ajax({
        url: base_url+'nas/add_nas_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
          
           

        location.reload();
        
        
        }
    });
         //alert(formdata);
     }
     
     function search_filter_city(data='')
     {
         getcitylist(data);
     }
     function getcitylist(stateid) {
    $.ajax({
        url: base_url+'user/getcitylist',
        type: 'POST',
        dataType: 'text',
        data: 'stateid='+stateid,
        success: function(data){
           
            $('.search_citylist').empty().append('<option value="all">All Cities</option>');
            $('.search_citylist').append(data);
        }
    });
}

 function getzonelist(cityid) {
    $.ajax({
        url: base_url+'nas/getzonelist',
        type: 'POST',
        dataType: 'text',
        data: 'cityid='+cityid,
        success: function(data){
           
            $('.search_zonelist').empty().append('<option value="all">All Zone</option>');
            $('.search_zonelist').append(data);
        }
    });
}

function search_filter(data='', filterby=''){
      var searchtext = $('#searchtext').val();
      $('#search_panel').addClass('hide');
      $('#allsearch_results').removeClass('hide');
      $('.loading').css('display', 'block');
      
      var formdata = '';
      var city = $('select[name="filter_city"]').val();
      var zone = $('select[name="filter_zone"]').val();
      if (filterby == 'state') {
         getcitylist(data);
         formdata += '&state='+data+ '&city='+city+'&zone='+zone;
      }
      else if(filterby == 'city')
      {
          getzonelist(data);
          var state = $('select[name="filter_state"]').val();
         formdata += '&state='+state+ '&city='+data+'&zone='+zone;
      }
        else{
         var state = $('select[name="filter_state"]').val();
         formdata += '&state='+state+ '&city='+city+'&zone='+zone;
      }

      //alert(formdata);
      $.ajax({
         url: base_url+'nas/viewall_search_results',
         type: 'POST',
         dataType: 'json',
         data: 'search_user='+searchtext+formdata,
         success: function(data){
            $('.loading').css('display', 'none');
            $('#search_count').html(data.total_results+' <small>Results</small>');
            $('#search_gridview').html(data.search_results);
         }
      });
   }
  </script>
 
 <?php $this->load->view('includes/footer');?>