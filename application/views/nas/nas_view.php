

<!-- Start your project here-->
<?php $this->load->view('includes/header');?>
<style>
   .pac-container {
   z-index: 10000 !important;
   }
   #mapCanvas {
   width: 100%;
   height: 250px;
   float: left;
   max-width:none !important;
   }
   #mapCanvas1 {
   width: 100%;
   height: 250px;
   float: left;
   max-width:none !important;
   }
</style>
<div class="loading hide" style="z-index:999999">
   <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="wapper">
         <div id="sidedrawer" class="mui--no-user-select">
            <div id="sidedrawer-brand" class="mui--appbar-line-height">
               <span class="mui--text-title">
               <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
               <img src="<?php echo $img ?>" class="img-responsive" />
               </span>
            </div>
            <?php
               $data['navperm']=$this->plan_model->leftnav_permission();
               
               $this->view('left_nav',$data); ?>
         </div>
         <header id="header">
            <nav class="navbar navbar-default">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <a class="navbar-brand" href="#">NAS</a>
                  </div>
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <?php if($this->plan_model->is_permission(NAS,'ADD')){ ?>     
                        <li>
                           <a href="#">
                           <button class="mui-btn mui-btn--small mui-btn--accent addnas">
                           + ADD NAS
                           </button>
                           </a>
                        </li>
                        <?php }
                           $this->view('includes/global_setting',$data); 
                           ?>
                     </ul>
                  </div>
               </div>
            </nav>
         </header>
         <div id="content-wrapper">
            <div class="mui--appbar-height"></div>
            <div class="mui-container-fluid" id="right-container-fluid">
               <div class="add_user">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                           <ul class="mui-tabs__bar plan_mui-tabs__bar">
                              <li class="mui--is-active">
                                 <a data-mui-toggle="tab" data-mui-controls="Top-Up-1">
                                 NAS
                                 </a>
                              </li>
                              <li>
                                 <a data-mui-toggle="tab" data-mui-controls="Top-Up-2">
                                 IP POOL
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                           <div class="mui--appbar-height"></div>
                           <div class="mui-tabs__pane mui--is-active" id="Top-Up-1">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="table-responsive">
                                       <table class="table table-striped">
                                          <thead>
                                             <tr class="active">
                                                <th>&nbsp;</th>
                                                <th>NAS NAME</th>
                                                <th>NAS TYPE</th>
                                                <th>NAS IPADDR</th>
                                                <th>NAS STATE</th>
                                                <th>NAS CITY</th>
                                                <th>NAS ZONE</th>
                                                <th>STATUS</th>
						<th>LeaseLine</th>
                                                <th colspan="2" class="mui--text-right">ACTIONS</th>
                                             </tr>
                                          </thead>
                                          <tbody id='search_gridview'>
                                             <?php 
                                                $i=1;
                                                 $is_editable=$this->plan_model->is_permission(NAS,'EDIT');
                                                  $is_deletable=$this->plan_model->is_permission(NAS,'DELETE');
                                                foreach($nas_listing as $valdata){
                                                   
                                                   $state = $this->user_model->getstatename($valdata->nasstate);
                                                  $city = $this->user_model->getcityname($valdata->nasstate, $valdata->nascity);
                                                  $zone = $this->user_model->getzonename($valdata->naszone);
                                                //  $status = ($valdata->status == 1)?'active':'inactive';
                                                   ?>
                                             <tr>
                                                <td><?php echo $i;?>.</td>
                                                <td><a href="javascript:void(0);" class="editnas" rel="<?php echo $valdata->id;?>"><?php echo $valdata->shortname;?></a></td>
                                                <td><?php echo ($valdata->nastype==1)?"Microtik":"Others";?></td>
                                                <td><?php echo $valdata->nasname;?></td>
                                                <td> <?php echo $state;?> </td>
                                                <td><?php echo $city;?></td>
                                                <td><?php echo $zone;?></td>
                                                <?php //$up = $this->nas_model->ping($valdata->nasname);
                                                   $nsimg='<img src="'.base_url().'assets/images/loader.svg" width="15%">';
                                                   
                                                   
                                                   ?>
                                                <td class="nasimg" ><?php echo $nsimg;?></td>
                                                <input type="hidden" class="nasnameip"  value="<?php echo $valdata->nasname?>">
						<?php if($valdata->is_ill_configured == 0){ ?>
                                                <td><a href="javascript:void(0);" class="configillnas"  rel="<?php echo $valdata->id;?>">Config ILL</a></td>
                                                <td><a href="javascript:void(0);">&nbsp;</a></td>
						<?php }else{ ?>
						<td><a href="javascript:void(0);" style="color:#4C9689">ILL Configured</a></td>
                                                <td><a href="javascript:void(0);" class="view_illdetails" rel="<?php echo $valdata->id;?>">View IPs</a></td>
						<?php } ?>
						<td><a href="javascript:void(0);" class="editnas" rel="<?php echo $valdata->id;?>">Edit</a></td>
                                                <!-- <td><a href="javascript:void(0);" class="<?php echo($is_deletable)?'deletnas':'' ?>" rel="<?php echo $valdata->id;?>">Delete</a></td> -->
                                             </tr>
                                             <?php
                                                $i++;
                                                } ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="mui-tabs__pane" id="Top-Up-2">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <form  action="" method="post" id="add_pool" autocomplete="off" onsubmit="add_pool(); return false;" >
                                    <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                             <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="poolname" value=""  required>
                                                <label> Pool Name<sup>*</sup></label>
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                             <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                <input type="text" <?php echo (isset($ro))?"disabled":"";?> id="startip" class="" name="startip" value=""  required>
                                                <label> Start Ip<sup>*</sup></label>
                                             </div>
                                             <span class="errorstartip" style="color:red;"></span>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                             <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" id="stopip" name="stopip" value="">
                                                <label> Stop IP</label>
                                             </div>
                                             <span class="errorstopip" style="color:red;"></span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                       <div class="row">
                                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                             <input type="submit" <?php echo (isset($ro))?"disabled":"";?> class="mui-btn mui-btn--accent btn-lg btn-block btn_setting"  value="ADD POOL" >
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                                 <div class="mui--appbar-height"></div>
                                 <div class="mui--appbar-height"></div>
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="table-responsive">
                                          <table class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>POOL NAME</th>
                                                   <th>START IP</th>
                                                   <th>STOP IP</th>
                                                   <th>IP COUNT</th>
                                                </tr>
                                             </thead>
                                             <tbody id='search_poolview'>
                                                <?php 
                                                   if(count($pool_listing)>0){
                                                   $i=1;
                                                   foreach($pool_listing as $valdata){
                                                       
                                                      
                                                    //  $status = ($valdata->status == 1)?'active':'inactive';
                                                       ?>
                                                <tr>
                                                   <td><?php //echo $i;?>.</td>
                                                   <td><?php echo $valdata->pool_name;?></td>
                                                   <td><?php echo $valdata->start_ip;?></td>
                                                   <td><?php echo $valdata->stop_ip;?></td>
                                                   <td> <?php echo $valdata->ipcount;?> </td>
                                                </tr>
                                                <?php
                                                   $i++;
                                                   }
                                                   }
                                                   ?>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!--Div for ISP Detail update-->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!--- Close-two Modal --->
<div class="modal fade" tabindex="-1" role="dialog" id="addNasmodal" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">
               <strong>ADD Nas</strong>
            </h4>
         </div>
         <div class="modal-body">
            <form action="" method="post" id="add_nas" autocomplete="off" onsubmit="add_nas(); return false;">
               <input type="hidden" name="nasid" value="">
               <div class="row">
                  <!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                     <h4>NasIp format</h4>
                     <label class="radio-inline">
                     <input type="radio" required name="nasipformat"  value="Static" > Static
                     </label>
                     <label class="radio-inline">
                     <input type="radio" required name="nasipformat"  value="DDNS" > DDNS
                     </label>
                     </div>-->
                  <input type="hidden" name="mobileno" value="9873834499">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <label>IP Address<sup>*</sup></label>
                           <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-right">
                                    <div class="mui-textfield mui-textfield--float-label" style="padding-right:10px">
                                       <input type="text" class="ip1" id="ip" name="ip" required>
                                       <!--<span class="ip_dot">.</span>-->
                                    </div>
                                    <span class="errorip" style="color:red;">
                                 </div>
                                 <!--  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin-right" style="padding-left:2px">
                                    <div class="mui-textfield mui-textfield--float-label" style="padding-right:10px">
                                       <input type="number" class="ip2 ipd" name="ip2" required>
                                       <span class="ip_dot">.</span>
                                    </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin">
                                    <div class="mui-textfield mui-textfield--float-label" style="padding-right:10px">
                                       <input type="number" class="ip3 ipd" name="ip3" required>
                                       <span class="ip_dot">.</span>
                                    </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin-left">
                                    <div class="mui-textfield mui-textfield--float-label">
                                       <input type="number" class="ip4 ipd" name="ip4" required>
                                    </div>
                                    </div>-->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label">
                              <input type="text" name="nas_name" id="nasname" required>
                              <label>Nas Name</label>
                           </div>
                           <span class="errornas" style="color:red;"></span>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="nastype" required>
                                 <option value="1">Microtik</option>
                              </select>
                              <label>NAS Type<sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label">
                              <input type="text" name="nas_secret" id="nas_secret" required>
                              <label>Nas Secret</label>
                           </div>
                           <span class="errornassecret" style="color:red;"></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="select">
                              <select name="state" class='statelist form-control' required >
                                 <option value="">Select States</option>
                                 <?php $this->plan_model->state_list(); ?>
                              </select>
                              <input type="hidden" class="statesel" name="statesel" value="">
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="">
                              <select name="city" class="search_citylist form-control"  required >
                                 <option value="all">All Cities</option>
                              </select>
                              <input type="hidden" class="citysel" name="citysel" value="">
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="">
                              <select name="zone" class="zone_list search_zonelist form-control" required>
                                 <option value="all">All Zones</option>
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" >
                        <div class="mui-textfield mui-textfield--float-label">
                           <input type="text"   id="geolocation" name="geoaddr" value="" required placeholder="">
                           <label>Geolocation</label>
                        </div>
                     </div>
                  </div>
                  <div class="row"></div>
                  <div id="mapCanvas"></div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                        <div class="mui-textfield mui-textfield--float-label">
                           <input type="text"  id="placeid"  value="" name="placeid"  readonly>
                           <label>Placeid</label>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="mui-textfield mui-textfield--float-label">
                           <input type="text"  id="lat" value="" name="lat" readonly>
                           <label>Lat</label>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="mui-textfield mui-textfield--float-label">
                           <input type="text" id="long" name="long" value=""  readonly>
                           <label>Long</label>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3"><button type="button" class="mui-btn  mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button></div>
                        <div class="col-lg-3 col-md-3 col-sm-3"><input type="submit" class="mui-btn  mui-btn--accent nassub" value="ADD NAS" style="height:36px; line-height:30px "></div>
                     </div>
                  </div>
               </div>
	    </form>
	 </div>
      </div>
      <!-- /.modal-content -->
   </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="editNasmodal" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">
               <strong>Edit Nas</strong>
            </h4>
         </div>
         <div class="modal-body">
            <form action="" method="post" id="edit_nas" autocomplete="off" onsubmit="edit_nas(); return false;">
               <div class='editnasappend'></div>
               <div class="row">
                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" >
                     <div class="mui-textfield mui-textfield--float-label">
                        <input type="text"   id="geolocation1" name="geoaddr" value="" required placeholder="">
                        <label>Geolocation</label>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div id="mapCanvas1"></div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                        <div class="mui-textfield mui-textfield--float-label">
                           <input type="text"  id="placeid1"  value="" name="placeid"  readonly>
                           <label>Placeid</label>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="mui-textfield mui-textfield--float-label">
                           <input type="text"  id="lat1" value="" name="lat" readonly>
                           <label>Lat</label>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="mui-textfield mui-textfield--float-label">
                           <input type="text" id="long1" name="long" value=""  readonly>
                           <label>Long</label>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                     <input type="submit"  class="mui-btn mui-btn--large mui-btn--accent" value="ADD NAS">
                  </div>
               </div>
	    </form>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
</div>

<div class="modal fade" id="Deletenas_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
   <input type="hidden" id="delnasid" value="">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">
               <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
            </h4>
         </div>
         <div class="modal-body" style="padding-bottom:5px">
            <p>Are you sure you want to Delete Nas ?</p>
         </div>
         <div class="modal-footer" style="text-align: right">
            <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
            <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyes" >YES</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="usergraph_modal" role="dialog" data-backdrop="static" data-keyboard="false">
   <input type="hidden" id="delnasid" value="">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">
               <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>User Graph</strong>
            </h4>
         </div>
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                  <div class="mui-textfield mui-textfield--float-label">
                     <input type="text" name="user_name" id="user_name" required>
                     <label>User Name</label>
                  </div>
                  <span class="erroruser" style="color:red;"></span>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                  <div class="mui-textfield mui-textfield--float-label">
                     <input type="password" name="password" id="password" maxlength="10" required>
                     <label>Password</label>
                  </div>
                  <span class="errorpas" style="color:red;"></span>
               </div>
            </div>
         </div>
         <div class="modal-footer" style="text-align: right">
            <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
            <button type="button" class="mui-btn  mui-btn--small mui-btn--accent submituser" >Submit</button>
         </div>
      </div>
   </div>
</div>

<!-- ILL SETUP STARTS -->
<div class="modal fade" id="add_ill_userpswd_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding:10px 15px">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="os-modal-title">ILL Router Setup</h4>
         </div>
         <form id="add_ill_userpswd" method="post" onsubmit="start_configuration(); return false;">
            <div class="modal-body" style="padding:10px 15px">
               <div class="row">
                  <div class="col-sm-6 col-xs-6">
                     <div class="mui-textfield">
                        <input type="hidden" class="tableid" name="tableid" class="form-control">
			<input type="hidden" class="nas_id_selected" name="nas_id_selected" class="form-control">
                        <input type="text" class="form-control"  name="router_ip" id="router_ip" readonly required>
                        <label>Router IP <sup>*</sup></label>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6 col-xs-6">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input type="text" class="form-control" name="router_user" id="router_user" required>
                        <label>Router Username <sup>*</sup></label>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input type="password" class="form-control"  name="router_password" id="router_password" >
                        <label>Router Password <sup>*</sup></label>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6 col-xs-6">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input type="number" class="form-control" name="router_port" id="router_port" >
                        <span class="title_box">Optional (Default 8728)</span>
                        <label>Port</label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <p id="router_connection_error" style="color:red"> </p>
               <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
               <input type="submit"  class="mui-btn mui-btn--large mui-btn--accent" value="NEXT">
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="add_interface_netmask_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding:10px 15px">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="os-modal-title">ILL Router Setup</h4>
         </div>
         <form id="add_interface_netmask" method="post" onsubmit="interface_netmask_configuration(); return false;">
            <div class="modal-body" style="padding:10px 15px">
               <div class="row">
                  <div class="col-sm-6 col-xs-6">
                     <div class="mui-select">
			<input type="hidden" class="tableid" name="tableid" class="form-control">
                        <input type="hidden" class="nas_id_selected" name="nas_id_selected" class="form-control">
                        <select name="interface_name" required></select>
                        <label>Select Interface <sup>*</sup></label>
                     </div>
                  </div>
               </div>
               <div class="row">
		  <div class="col-sm-6 col-xs-6">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input type="text" class="form-control" name="gateway_ip" id="gateway_ip" required>
                        <label>Enter Gateway IP <sup>*</sup></label>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6">
                     <div class="mui-radio">
			<label class="nopadding">IP Address Type <sup>*</sup> </label> <br/>
                        <input type="radio" name="ip_type" value="public" required> Public &nbsp;
			<input type="radio" name="ip_type" value="private" required> Private
                     </div>
                  </div>
	       </div>
	       <div class="row">
                  <div class="col-sm-6 col-xs-6">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input type="number" class="form-control" name="natmask" id="natmask" min="8" max="30" required>
                        <label>Netmask<sup>*</sup></label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <p id="interface_netmask_error" style="color:red"> </p>
               <div class="col-lg-3 col-md-3 col-sm-3">
		  <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="NEXT">
	       </div>
	       <div class="col-lg-3 col-md-3 col-sm-3 cancel_and_proceed hide"><button type="button" class="mui-btn  mui-btn--large mui-btn--accent cancel_and_proceed_btn" style="background-color:#4d4d4d; color:#fff ">CANCEL</button></div>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="add_another_gatewayip_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding:10px 15px">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="os-modal-title">ILL Router Setup</h4>
         </div>
	 <div class="modal-body" style="padding:10px 15px">
	    <div class="row">
	       <div class="col-sm-6 col-xs-6">
		  Do you want to put another IP address on the interface ?
	       </div>
	    </div>
	 </div>
	 <div class="modal-footer">
	    <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat addanother_gatewayip" style="background-color:#4d4d4d; color:#fff">Yes</button>
	    <button type="button" class="mui-btn mui-btn--large mui-btn--accent proceednext">No</button>
	 </div>
      </div>
   </div>
</div>
<div class="modal fade" id="nas_ill_ipslist_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding:10px 15px">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="os-modal-title">ILL IPs SUMMARY</h4>
         </div>
	 <div class="modal-body" style="padding:10px 15px">
	    <div class="row">
	       <div class="col-sm-6 col-xs-6 ips_listing">
	       </div>
	    </div>
	 </div>
	 <div class="modal-footer">
	    &nbsp;
	 </div>
      </div>
   </div>
</div>
<!-- ILL SETUP ENDS -->


<script type="text/javascript">
   $(document).ready(function(){
      //usergraph modal
      
      $(document).on('click','.yesgraph',function(){
      $('#usergraph_modal').modal('show');
      $(this).css('background-color', '#00C853')
      $('.nograph').css('background-color', '#4D4D4D');
      
      })
      
      $(document).on('click','.nograph',function(){
      
      
      $('#usergrph').val("0");
      $('#username').val("");
      $('#userpwd').val("");
      $(this).css('background-color', '#DD2C00')
      $('.yesgraph').css('background-color', '#4D4D4D');
      })
   
   
   
      $(document).on('click','.submituser',function(){
	 var username=$('#user_name').val();
	 var password=$('#password').val();
	 if (username=="") {
	    $('.erroruser').html("User name can't be blank");
	      return false;
	 }
	 else
	 {
	    $('.erroruser').html("");
	 }
	 if (password=="") {
	    $('.errorpas').html("Password can't be blank");
	    return false;
	 }
	 else
	 {
	    $('.errorpas').html("");
	 }
	 
	 $('#usergrph').val("1");
	 $('#username').val(username);
	 $('#userpwd').val(password);
	 
	 
	 $('#usergraph_modal').modal('hide');
      });
       
      $( ".nasnameip" ).each(function( ) {
	 var $this=$(this);
	    var nasip=$(this).val();
	    $.ajax({
	    url: base_url+'nas/status_nasip',
	    type: 'POST',
	    dataType: 'text',
	    data: 'nasip='+nasip,
	    success: function(data){
		var active="<img src='"+base_url+"assets/images/green.png'>";
		var inactive="<img src='"+base_url+"assets/images/red.png'>";
		if(data==0)
		{
		    
		     $this.closest('tr').find('.nasimg').html(active);
		}
		else
		{
		    
		    $this.closest('tr').find('.nasimg').html(inactive);
		}
		    
	     
	    }

	 });
      });
       
      var height = $(window).height();
      $('#main_div').css('height', height);
      $('#right-container-fluid').css('height', height);
      
      //code for status
      
       
      $(document).on('click','.addnas',function(){
	$('#editNasmodal').html('');  
	$('#addNasmodal').modal('show');
	initialize1();
	initialize();      
      });
       
       
      $(document).on('change','.statelist',function(){
	 var stateid=$(this).val();
	 var $this=$(this);
	 $(this).closest('.row').find('.statesel').val(stateid);
	 // $this.closest('.row').find('.valregion').data('stateid',stateid);
         //$this.closest('.row').find('.valregion').data('cityid','all');
         // $this.closest('.row').find('.valregion').data('zoneid','all');
	 $.ajax({
	    url: base_url+'plan/getcitylist',
	    type: 'POST',
	    dataType: 'text',
	    data: 'stateid='+stateid,
	    success: function(data){
	       $this.closest('.row').find('.search_citylist').empty();
	       $this.closest('.row').find('.search_citylist').append(data);
	    }
	 });
       
      });
    
      $(document).on('change','.search_citylist',function(){
           if($(this).val()=="addc")
        {
          $('.city_text').val('');
          var state_id=$(this).closest('.row').find('.statelist').val();
          $('.state_id').val(state_id);
          $('#add_city').modal('show'); 
          return false;
        }
         //  $('.search_citylist').change(function(){
        var cityid=$(this).val();
        var stateid=$(this).closest('.row').find('.statelist').val();
        var $this=$(this);
        $(this).closest('.row').find('.citysel').val(cityid);
     
     
        $this.closest('.row').find('.valregion').data('cityid',cityid);
        $.ajax({
      url: base_url+'plan/getzonelist',
      type: 'POST',
      dataType: 'text',
      data: 'cityid='+cityid+'&stateid='+stateid,
      success: function(data){
       
         $this.closest('.row').find('.zone_list').empty();
           $this.closest('.row').find('.zone_list').append(data);
      }
      
      
   });
       
    });
    
     $(document).on('change', '.zone_list', function () {
          if ($(this).val() == "addz") {
              $('.zone_text').val('');
              var state_id = $(this).closest('.row').find('.search_citylist').val();
              var city_id = $(this).closest('.row').find('.search_citylist').val();
              $('.state_id').val(state_id);
              $('.city_id').val(city_id);
              $('#add_zone').modal('show');
              return false;
          }
          
   
      });
    
       
       
       $(document).on('click','.deletnas',function(){
           
           var delnasid=$(this).attr('rel');
           $('#delnasid').val(delnasid);
           $('#Deletenas_confirmation_alert').modal('show');
           
           
       });
       
       $(document).on('click','.delyes',function(){
         
         var nasid=$('#delnasid').val();
           $.ajax({
      url: base_url+'nas/delete_nas',
      type: 'POST',
      dataType: 'json',
      data: {nasid:nasid},
      success: function(data){
       
          $('#Deletenas_confirmation_alert').modal('hide');
    
      location.reload();
         
      
      }
   });
           
       });
       
       var timer=null;
      
       
       $('body').on('click','#searchclear', function(){
    $('#searchtext').val('');
     $('#searchtext').addClass('serch_loading');
   search_filter('','','1');
    
   });
   
   $('body').on('keyup', '#searchtext', function(){
      $('#searchtext').addClass('serch_loading');
    search_filter('','','1');
   });
   
       
         $(document).on('change','input:radio[name="nasipformat"]',function(){
             if($(this).val()=="Static")
             {
               $('.fqdn').attr('required',false);
                $('.fqdn').prop('disabled',true);
                 $('.fqdn').val('');
                $('.ip1').prop('disabled',false);
                $('.ip2').prop('disabled',false);
                 $('.ip3').prop('disabled',false);
                  $('.ip4').prop('disabled',false);
            $('.ip1').attr('required',true);
            $('.ip2').attr('required',true);
            $('.ip3').attr('required',true);
            $('.ip4').attr('required',true);   
             }
             else if($(this).val()=="DDNS")
             {
                $('.fqdn').attr('required',true);
            $('.ip1').attr('required',false);
            $('.ip2').attr('required',false);
            $('.ip3').attr('required',false);
            $('.ip4').attr('required',false); 
            $('.ip1').val('');
            $('.ip2').val('');
            $('.ip3').val('');
            $('.ip4').val('');
              $('.fqdn').prop('disabled',false);
                $('.ip1').prop('disabled',true);
                $('.ip2').prop('disabled',true);
                 $('.ip3').prop('disabled',true);
                  $('.ip4').prop('disabled',true);
             }
         
         });
     //    var timer = null;
         $(document).on('keyup','.ipd',function(e ){
            
            switch (e.keyCode) {
   
       /*   case 190:
            {
                
              //    clearTimeout(timer);
      // timer = setTimeout(function () {
           
             $(this).parent().parent().next().find('.ipd').focus();
               $( ".ipd" ).each(function( i ) {
   
   });
              };*/
            
             case 8:
            {
               // alert($(this).val());
                if($(this).val()==="")
                {
                  $(this).parent().parent().prev().find('.ipd').focus();  
                }
               
            };
            
              case 9:
            {
              // var rx=/^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){1}$/;
             var rx= new RegExp("^([01][0-9][0-9]|2[0-4][0-9]|25[0-5])$");
                if(!rx.test($(this).val()))
                {
                    alert('Invalid Octet');
                    $(this).parent().parent().prev().find('.ipd').focus(); 
                }
            
               
            };
           
          }
          
        
            if($(this).val().length===3)
             {
              //  $(this).parent().parent().next().find('.ipd').focus();
               // var rx=/^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){1}$/;
                  var rx= new RegExp("^([01][0-9][0-9]|2[0-4][0-9]|25[0-5])$");
                if(!rx.test($(this).val()))
                {
                    alert('Invalid Octet');
                    $(this).focus();
                }
                else
                {
                     $(this).parent().parent().next().find('.ipd').focus();
                }
             }
         });
       
   });
   
   
   /* function add_nas()
   {
       alert("aaaa");
   }*/
   
   function add_nas()
   {
      $('.nassub').prop('disabled',true); 
      //alphanumeric character validation
       var regex = new RegExp("^[a-zA-Z0-9]+$");
   var str = $('#nas_secret').val();
   if (!regex.test(str)) {
     $('.errornassecret').html("Nas Secret can't have special Character");
      return false;
   }
   else
   {
       $('.errornassecret').html("");
   }
   
     
       
    var nasname=$('#nasname').val();
   if(nasname.split(" ").length > 1){
    
      $('.errornas').html("Nas Name can't have space");
   return false;
   }
   
   
   else
   {
      $.ajax({
      url: base_url+'nas/checknas_exist',
      type: 'POST',
      dataType: 'json',
      async:false,
      data: {nasname:nasname},
      success: function(data){
          
        if(data=="1")
        {
            $('.errornas').html("Nas Name Already exist");
             return false;
        }
        else
        {
           
   var ip=$('#ip').val();
   
   if(ip=="")
   {
   $('.errorip').html("Ip can't be blank");
   return false;
   }
   else
   {
   if(!ValidateIPaddress(ip))
   {
   
      $('.errorip').html("Ip is not valid");
       return false;
   }
   else
   {
   
      $.ajax({
       url: base_url+'nas/checknasip_exist',
       type: 'POST',
       dataType: 'json',
        async:false,
       data: {nasip:ip},
       success: function(data){
         if(data=="1")
         {
             $('.errorip').html("NasIp Already exist");
             return false;
         }
         else
         {
             $('.errorip').html("");
             
      $('.loading').removeClass('hide');
       $('#addNasmodal').modal('hide');
       var formdata = $("#add_nas").serialize();
       $.ajax({
      url: base_url+'nas/add_nas_data',
      type: 'POST',
      dataType: 'json',
      data: formdata,
      success: function(data){
        
         
        // alert(data);
    location.reload();
      
      
      }
   });
         }
       }
   });
   }
   }
        }
      }
   });
   
   }
   
   
   
       //alert(formdata);
   }
   function edit_nas()
   {
         var regex = new RegExp("^[a-zA-Z0-9]+$");
   var str = $('#editnas_secret').val();
   
   if (!regex.test(str)) {
     $('.errornassecret').html("Nas Secret can't have special Character");
      return false;
   }
   else
   {
       $('.errornassecret').html("");
   }
       
       
       var nasid=$('#editnasid').val();
   var nasname=$('#editnasname').val();
   if(nasname.split(" ").length > 1){
    
      $('.errornas').html("Nas Name can't have space");
   return false;
   }
   else
   {
      $.ajax({
      url: base_url+'nas/checknas_exist',
      type: 'POST',
      dataType: 'json',
      data: {nasname:nasname,nasid:nasid},
      success: function(data){
          
        if(data=="1")
        {
            $('.errornas').html("Nas Name Already exist");
             return false;
        }
        else
        {
            $('.errornas').html("");
            var ip=$('#editip').val();
   
   
   if(ip=="")
   {
   $('.errorip').html("Ip can't be blank");
   return false;
   }
   else
   {
   if(!ValidateIPaddress(ip))
   {
   
      $('.errorip').html("Ip is not valid");
       return false;
   }
   else
   {
   
      $.ajax({
       url: base_url+'nas/checknasip_exist',
       type: 'POST',
       dataType: 'json',
       data: {nasip:ip,nasid:nasid},
       success: function(data){
         if(data=="1")
         {
             $('.errorip').html("NasIp Already exist");
             return false;
         }
         else
         {
             $('.errorip').html("");
              $('.loading').removeClass('hide');
       $('#editNasmodal').modal('hide');
       var formdata = $("#edit_nas").serialize();
       $.ajax({
      url: base_url+'nas/add_nas_data',
      type: 'POST',
      dataType: 'json',
      data: formdata,
      success: function(data){
        
         
   
      location.reload();
      
      
      }
   });
         }
       }
   });
   }
   }
        }
      }
   });
   
   }
   
   
   
   
     
       //alert(formdata);
   }
   
   function add_pool()
   {
       var startip=$('#startip').val();
       var stopip=$('#stopip').val();
       if(!ValidateIPaddress(startip))
       {
           $('.errorstartip').html("StartIp is not valid");
           return false;
       } 
       else
       {
           $('.errorstartip').html("");
       }
       if(!ValidateIPaddress(stopip))
       {
           $('.errorstopip').html("StopIp is not valid");
           return false; 
       }
       else
       {
            $('.errorstopip').html("");
       }
       if(!CompareIP(startip,stopip))
       {
           $('.errorstopip').html("Start ip and stopIp range is wrong");
           return false; 
       }
       else
       {
           $('.errorstopip').html("");
       }
   $('.loading').removeClass('hide');
         var formdata = $("#add_pool").serialize();
       $.ajax({
      url: base_url+'nas/add_pool_data',
      type: 'POST',
      dataType: 'json',
      data: formdata,
      success: function(data){
   if(data.result==1)
   {
   $('#search_poolview').append(data.html);
    $('.loading').addClass('hide');
   
   }
   else{
   $('.errorstopip').html("Range Already added");
   $('.loading').addClass('hide');
   }
        
        
   
      //location.reload();
      
      
      }
   });
       
       
       
       
   }
   
   
   function CompareIP(startip,toip)
   {
   var from = startip;
   var to = toip;
   var fromArr = from.split(".");
   var toArr = to.split(".");
   for(i=0;i<4;i++)
   {
   if(fromArr[i]>toArr[i])
     return false;
   }
   return true;
   }
   
   function search_filter_city(data='')
   {
       getcitylist(data);
   }
   function getcitylist(stateid) {
   $.ajax({
      url: base_url+'user/getcitylist',
      type: 'POST',
      dataType: 'text',
      data: 'stateid='+stateid,
      success: function(data){
         
          $('.search_citylist').empty().append('<option value="all">All Cities</option>');
          $('.search_citylist').append(data);
      }
   });
   }
   
   function getzonelist(cityid) {
   $.ajax({
      url: base_url+'nas/getzonelist',
      type: 'POST',
      dataType: 'text',
      data: 'cityid='+cityid,
      success: function(data){
         
          $('.search_zonelist').empty().append('<option value="all">All Zone</option>');
          $('.search_zonelist').append(data);
      }
   });
   }
   
   
   function ValidateIPaddress(ipaddress)   
   {  
   if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress))  
    {  
       
      return true;  
    } 
    else
    {
       
          return false;
    }
   
   }  
   
   function search_filter(data='', filterby='',keyup=''){
    var searchtext = $('#searchtext').val();
    if(keyup=="")
    {
         $('.loading').removeClass('hide');
    }
    $('#search_panel').addClass('hide');
    $('#allsearch_results').removeClass('hide');
   
    
    var formdata = '';
    var city = $('select[name="filter_city"]').val();
    var zone = $('select[name="filter_zone"]').val();
    if (filterby == 'state') {
       getcitylist(data);
       formdata += '&state='+data+ '&city='+city+'&zone='+zone;
    }
    else if(filterby == 'city')
    {
        getzonelist(data);
        var state = $('select[name="filter_state"]').val();
       formdata += '&state='+state+ '&city='+data+'&zone='+zone;
    }
      else{
       var state = $('select[name="filter_state"]').val();
       formdata += '&state='+state+ '&city='+city+'&zone='+zone;
    }
   
    //alert(formdata);
    $.ajax({
       url: base_url+'nas/viewall_search_results',
       type: 'POST',
       dataType: 'json',
       data: 'search_user='+searchtext+formdata,
       success: function(data){
          $('.loading').addClass('hide');
          $('#search_count').html(data.total_results+' <small>Results</small>');
          $('#search_gridview').html(data.search_results);
           $('#searchtext').removeClass('serch_loading');
       }
    });
   }
</script>
<script>
   if ($(window)) {
    $(function () {
     $('.menu').crbnMenu({
      hideActive: true
     });
    });
   }
   
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA61iANDo6jqGYQm0SjXzIvmOEnks-MpG0&amp;libraries=places"  type="text/javascript"></script>
<script type="text/javascript">
   function initialize() {
   
      var input = document.getElementById('geolocation');
   //var options = {componentRestrictions: {country: 'in'}};
      var autocomplete = new google.maps.places.Autocomplete(input);
      google.maps.event.addListener(autocomplete, 'place_changed', function () {
   //  $(document).on(autocomplete, 'place_changed', function (){
   
          var place = autocomplete.getPlace();
   document.getElementById('lat').value = place.geometry.location.lat().toFixed(6);
          document.getElementById('long').value = place.geometry.location.lng().toFixed(6);
          document.getElementById('placeid').value = place.place_id;
          $("#placeid").attr('class','mui--is-dirty valid mui--is-not-empty');
          $("#lat").attr('class','mui--is-dirty valid mui--is-not-empty');
          $("#long").attr('class','mui--is-dirty valid mui--is-not-empty');
   
    var latLng = new google.maps.LatLng(place.geometry.location.lat().toFixed(6), place.geometry.location.lng().toFixed(6));
   var map = new google.maps.Map(document.getElementById('mapCanvas'), {
   zoom: 8,
   center: latLng,
   mapTypeId: google.maps.MapTypeId.ROADMAP
   });
   var marker = new google.maps.Marker({
   position: latLng,
   title: 'Point A',
   map: map,
   draggable: true
   });
      });
   }
   
   
   function initialize2() {
   
      var input = document.getElementById('geolocation1');
   var options = {componentRestrictions: {country: 'in'}};
      var autocomplete = new google.maps.places.Autocomplete(input,options);
      google.maps.event.addListener(autocomplete, 'place_changed', function () {
   //  $(document).on(autocomplete, 'place_changed', function (){
   
          var place = autocomplete.getPlace();
   document.getElementById('lat1').value = place.geometry.location.lat().toFixed(6);
          document.getElementById('long1').value = place.geometry.location.lng().toFixed(6);
          document.getElementById('placeid1').value = place.place_id;
          $("#placeid1").attr('class','mui--is-dirty valid mui--is-not-empty');
          $("#lat1").attr('class','mui--is-dirty valid mui--is-not-empty');
          $("#long1").attr('class','mui--is-dirty valid mui--is-not-empty');
   
    var latLng = new google.maps.LatLng(place.geometry.location.lat().toFixed(6), place.geometry.location.lng().toFixed(6));
   var map = new google.maps.Map(document.getElementById('mapCanvas1'), {
   zoom: 8,
   center: latLng,
   mapTypeId: google.maps.MapTypeId.ROADMAP
   });
   var marker = new google.maps.Marker({
   position: latLng,
   title: 'Point A',
   map: map,
   draggable: true
   });
      });
   }
   
   
   
   // google.maps.event.addDomListener(window, 'load', initialize);
   
</script>
<script type="text/javascript">
   var geocoder = new google.maps.Geocoder();
   
   function geocodePosition(pos) {
     geocoder.geocode({
       latLng: pos
     }, function(responses) {
   	
       if (responses && responses.length > 0) {
   		update_place_id(responses[0].place_id);
         updateMarkerAddress(responses[0].formatted_address);
       } else {
         updateMarkerAddress('Cannot determine address at this location.');
       }
     });
   }
   
   function updateMarkerStatus(str) {
     //document.getElementById('markerStatus').innerHTML = str;
   }
   
   function updateMarkerPosition(latLng) {
   	
   	document.getElementById('lat').value = latLng.lat().toFixed(6);
   	document.getElementById('long').value = latLng.lng().toFixed(6);
   
     
   }
   
   function updateMarkerAddress(str) {
     document.getElementById('geolocation').value = str;
   }
   
   function update_place_id(placeid){
   	  document.getElementById('placeid').value = placeid;
   }
   
   function initialize1() {
     var latLng = new google.maps.LatLng(28.613939, 77.209021);
     var map = new google.maps.Map(document.getElementById('mapCanvas'), {
       zoom: 8,
       center: latLng,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     });
     var marker = new google.maps.Marker({
       position: latLng,
       title: 'Point A',
       map: map,
       draggable: true
     });
     
   $('#addNasmodal').on('shown.bs.modal', function () {
                 google.maps.event.trigger(map, 'resize');
                 map.setCenter(new google.maps.LatLng(28.613939, 77.209021));
               });
     // Update current position info.
     updateMarkerPosition(latLng);
     geocodePosition(latLng);
      $("#placeid").attr('class','mui--is-dirty valid mui--is-not-empty');
               $("#lat").attr('class','mui--is-dirty valid mui--is-not-empty');
               $("#long").attr('class','mui--is-dirty valid mui--is-not-empty');
   	    $("#geolocation").attr('class','mui--is-dirty valid mui--is-not-empty');
   
     // Add dragging event listeners.
     google.maps.event.addListener(marker, 'dragstart', function() {
       updateMarkerAddress('Dragging...');
     });
   
     google.maps.event.addListener(marker, 'drag', function() {
       updateMarkerStatus('Dragging...');
       updateMarkerPosition(marker.getPosition());
     });
   
     google.maps.event.addListener(marker, 'dragend', function() {
       updateMarkerStatus('Drag ended');
       geocodePosition(marker.getPosition());
     });
   }
   
   
   
   
   
   function geocodePosition1(pos) {
     geocoder.geocode({
       latLng: pos
     }, function(responses) {
   	
       if (responses && responses.length > 0) {
   		update_place_id1(responses[0].place_id);
         updateMarkerAddress1(responses[0].formatted_address);
       } else {
         updateMarkerAddress1('Cannot determine address at this location.');
       }
     });
   }
   
   function updateMarkerStatus1(str) {
     //document.getElementById('markerStatus').innerHTML = str;
   }
   
   function updateMarkerPosition1(latLng) {
   	
   	document.getElementById('lat1').value = latLng.lat().toFixed(6);
   	document.getElementById('long1').value = latLng.lng().toFixed(6);
   
     
   }
   
   function updateMarkerAddress1(str) {
     document.getElementById('geolocation1').value = str;
   }
   
   function update_place_id1(placeid){
   	  document.getElementById('placeid1').value = placeid;
   }
   var map='';
   function initialize3(lat,long) {
     var latLng = new google.maps.LatLng(lat, long);
     var map = new google.maps.Map(document.getElementById('mapCanvas1'), {
       zoom: 8,
       center: latLng,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     });
     var marker = new google.maps.Marker({
       position: latLng,
       title: 'Point A',
       map: map,
       draggable: true
     });
     
   $('#editNasmodal').on('shown.bs.modal', function () {
                 google.maps.event.trigger(map, 'resize');
                 map.setCenter(new google.maps.LatLng(lat, long));
               });
     // Update current position info.
     updateMarkerPosition1(latLng);
     geocodePosition1(latLng);
      $("#placeid1").attr('class','mui--is-dirty valid mui--is-not-empty');
               $("#la1t").attr('class','mui--is-dirty valid mui--is-not-empty');
               $("#long1").attr('class','mui--is-dirty valid mui--is-not-empty');
   	    $("#geolocation1").attr('class','mui--is-dirty valid mui--is-not-empty');
   
     // Add dragging event listeners.
     google.maps.event.addListener(marker, 'dragstart', function() {
       updateMarkerAddress1('Dragging...');
     });
   
     google.maps.event.addListener(marker, 'drag', function() {
       updateMarkerStatus1('Dragging...');
       updateMarkerPosition1(marker.getPosition());
     });
   
     google.maps.event.addListener(marker, 'dragend', function() {
       updateMarkerStatus1('Drag ended');
       geocodePosition1(marker.getPosition());
     });
   }
   
   
   
   
   // Onload handler to fire off the app.
   //google.maps.event.addDomListener(window, 'load', initialize3);
   
     $(document).on('click','.editnas',function(){
               //$('#addNasmodal').html('');
              var nasid=$(this).attr('rel');
                $.ajax({
           url: base_url+'nas/edit_nas',
           type: 'POST',
           dataType: 'json',
           data: {nasid:nasid},
           success: function(data){
               
               $('.editnasappend').html(data.html);
   	  $('#lat1').val(data.lat);
   	  $('#long1').val(data.long);
   	  $('#placeid1').val(data.placeid);
   	  $('#geolocation1').val(data.address);
   	       initialize2();
   	      initialize3(data.lat,data.long);
   	
   	      
   	// google.maps.event.trigger(map, 'resize');
      // map.setCenter(new google.maps.LatLng(data.lat, data.long));
            
               $('#editNasmodal').modal('show');
   	   
   	       
   	   
             
              
         
          // location.reload();
              
           
           }
       });
                
            });
   
   
</script>


    
   <!--
   *************************************
   *	IIL CONFIGURATION SCRIPTS
   *************************************
   -->
<script type="text/javascript">
   $(document).on('click', '.configillnas', function(){
      var nasid=$(this).attr('rel');
      $.ajax({
	 url: base_url+'nas/getnasdetails',
	 type: 'POST',
	 dataType: 'json',
	 data: {nasid:nasid},
	 success: function(data){
	    if (data.error == '0') {
	       $(".nas_id_selected").val(nasid);
	       $("#router_ip").val(data.nasname);
               $("#add_ill_userpswd_modal").modal("show");
	    }
	 }
      });
   });
   function start_configuration() {
      $("#router_connection_error").val("");
      var router_ip = $("#router_ip").val();
      var router_user = $("#router_user").val();
      var router_password = $("#router_password").val();
      var router_port = $("#router_port").val();
      var nasid = $(".nas_id_selected").val();
      var tableid = $('.tableid').val();

      $(".loading").removeClass('hide');
      $.ajax({
         type: "POST",
         url: "<?php echo base_url()?>nas/start_configuration",
         data: "router_ip="+router_ip+"&router_user="+router_user+"&router_password="+router_password+"&router_port="+router_port+"&nasid="+nasid+"&tableid="+tableid,
	 dataType: 'json',
         success: function(data){
            $(".loading").addClass('hide');
            if (data.error == '0') {
	       $('select[name="interface_name"]').empty().append(data.interfaces);
	       $('.tableid').val(data.tableid);
               $("#add_ill_userpswd_modal").modal("hide");
               $("#add_interface_netmask_modal").modal("show");
            }else{
               $("#router_connection_error").html("Router not connected. WRONG USERNAME / PASSWORD");
            }
         }
      });
   }
   
   function interface_netmask_configuration() {
      $("#interface_netmask_error").val("");
      var interface_name = $("select[name=interface_name] option:selected").val();
      var ip_type = $("input[name=ip_type]:checked").val();
      var natmask = $("#natmask").val();
      var gateway_ip = $('#gateway_ip').val();
      var nasid = $(".nas_id_selected").val();
      var tableid = $('.tableid').val();

      var is_validip = validate_ip(gateway_ip);
      if (is_validip == 1) {
	 $(".loading").removeClass('hide');
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>nas/interface_netmask_configuration",
	    data: "interface_name="+interface_name+"&ip_type="+ip_type+"&natmask="+natmask+"&nasid="+nasid+"&gateway_ip="+gateway_ip+"&tableid="+tableid,
	    dataType: 'json',
	    success: function(data){
	       $(".loading").addClass('hide');
	       if (data.conn_error == '0') {
		  if (data.message == 'success') {
		     $("#add_ill_userpswd_modal").modal("hide");
		     $("#add_interface_netmask_modal").modal("hide");
		     $('#add_another_gatewayip_modal').modal('show');
		  }else{
		     $("#interface_netmask_error").html(data.message);
		  }
	       }else{
		  $("#interface_netmask_error").html("Router not connected. WRONG USERNAME / PASSWORD");
	       }
	    }
	 });
      }else{
	 $("#interface_netmask_error").html("Invalid Gateway IP entered.");
      }
   }
   
   $('body').on('click', '.addanother_gatewayip', function(){
      //$('form#add_interface_netmask')[0].reset();
      $('#gateway_ip').val('');
      $('#natmask').val('');
      $('.tableid').val('');
      $("#add_ill_userpswd_modal").modal("hide");
      $("#add_interface_netmask_modal").modal("show");
      $('#add_another_gatewayip_modal').modal('hide');
      $('.cancel_and_proceed').removeClass('hide');
   });
   
   $(document).on('click', '.cancel_and_proceed_btn' , function(){
      $("#add_ill_userpswd_modal").modal("hide");
      $("#add_interface_netmask_modal").modal("hide");
      $('#add_another_gatewayip_modal').modal('show');
   });
   
   $(document).on('click', '.view_illdetails' , function(){
      var nasid=$(this).attr('rel');
      $(".loading").removeClass('hide');
      $.ajax({
	 type: "POST",
	 url: "<?php echo base_url()?>nas/getill_iprange",
	 data: "nasid="+nasid,
	 dataType: 'json',
	 success: function(data){
            $('#nas_ill_ipslist_modal').modal('show');
            $('.ips_listing').html(data.ill_iplists);
	    $(".loading").addClass('hide');
	 }
      });
   });
   
   $(document).on('click', '.proceednext', function(){
      var nasid = $(".nas_id_selected").val();
      $(".loading").removeClass('hide');
      $.ajax({
	 type: "POST",
	 url: "<?php echo base_url()?>nas/add_ill_iprange",
	 data: "nasid="+nasid,
	 dataType: 'json',
	 success: function(data){
	    //$(".loading").addClass('hide');
	    //console.log(data);
	    window.location = "<?php echo base_url().'nas'; ?>";
	 }
      });
   });
   
   function validate_ip(ip) {
      if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip)){
	 return 1;
      }else{
	 return 0;
      }
   }
</script>
<?php $this->load->view('includes/footer');?>

