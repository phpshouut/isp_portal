<!DOCTYPE html>
<html lang="en">
    <head>
       <meta charset="utf-8">
       <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <meta http-equiv="x-ua-compatible" content="ie=edge">
       <title>SHOUUT | ISP</title>
       <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
       <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
       <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
       <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
       <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-material-datetimepicker.css" />
       <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body>
        <!-- Start your project here-->
        <div class="loading">
           <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
        </div>
        <div class="container-fluid">
           <div class="row">
              <div class="wapper">
                 <div id="sidedrawer" class="mui--no-user-select">
                    <div id="sidedrawer-brand" class="mui--appbar-line-height">
                       <span class="mui--text-title">
                       <?php
                       $isplogo = $this->user_model->get_ispdetail_info();
                       if($isplogo != 0){
                          echo '<img src="'.base_url()."ispmedia/logo/".$isplogo.'" class="img-responsive"/>';
                       }else{
                          echo '<img src="'.base_url().'assets/images/decibel.png" class="img-responsive"/>';
                       }
                       ?>
                       </span>
                    </div>
                    <?php
                       $leftperm['navperm']=$this->user_model->leftnav_permission();
                       $this->view('left_nav',$leftperm);
                    ?>
                 </div>
                    <header id="header">
                       <nav class="navbar navbar-default">
                          <div class="container-fluid">
                             <div class="navbar-header">
                                <a class="navbar-brand" href="#">Manage License</a>
                             </div>
                             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                &nbsp;
                             </div>
                          </div>
                       </nav>
                    </header>
                    <div id="content-wrapper">
                        <div class="mui--appbar-height"></div>
                        <div class="mui-container-fluid" id="right-container-fluid">
                            <div class="add_user">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px">
                                        <div class="mui--appbar-height"></div>
                                        <?php if($paydata->transaction == 0){ ?>
                                            <h3>Unable to get your Payment of Rs. <?php echo $paydata->amount ?>. Your TransactionID is <?php echo $paydata->txnid ?></h3> <br/>
                                                Something went wrong. Please write to us at <a href="mailto:talktous@shouut.com?Subject=For+Enquiry" style="color:#25a9e0;text-decoration:none" target="_blank">talktous@shouut.com</a> or call us @ Praveer Kochhar +91 9810678773
                                        <?php }elseif($paydata->transaction == 1){ ?>
                                            <p style=" font-size:15px; color:#41CE71">We have received your Payment of Rs. <?php echo $paydata->amount ?>. Your TransactionID is <?php echo $paydata->txnid ?> </p> <br/>
                                            You can write to us at <a href="mailto:talktous@shouut.com?Subject=For+Enquiry" style="color:#25a9e0;text-decoration:none" target="_blank">talktous@shouut.com</a> or call us @ Praveer Kochhar +91 9810678773
                                          
                                        <?php } ?>
                                        
                                        <br/><br/>
                                        <a href="<?php echo base_url().'manageLicense' ?>" class="btn btn-default">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
        </div>
        
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/material.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
        <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/crbnMenu.js"></script>
        <script type="text/javascript">
            if ($(window)) {
                $(function () {
                   $('.menu').crbnMenu({
                   hideActive: true
                   });
                });
            }
            $(document).ready(function() {
                var height = $(window).height();
                $('#main_div').css('height', height);
                $('#right-container-fluid').css('height', height);
                setTimeout(function (){ $('.loading').addClass('hide') },1000);
            });
         
        </script>
        
    </body>
</html>