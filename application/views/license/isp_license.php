<!DOCTYPE html>
<html lang="en">
    <head>
       <meta charset="utf-8">
       <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <meta http-equiv="x-ua-compatible" content="ie=edge">
       <title>SHOUUT | ISP</title>
       <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
       <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
       <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
       <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
       <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-material-datetimepicker.css" />
       <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body>
        <!-- Start your project here-->
        <div class="loading">
           <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
        </div>
        <div class="container-fluid">
           <div class="row">
              <div class="wapper">
                 <div id="sidedrawer" class="mui--no-user-select">
                    <div id="sidedrawer-brand" class="mui--appbar-line-height">
                       <span class="mui--text-title">
                       <?php
                       $isplogo = $this->user_model->get_ispdetail_info();
                       if($isplogo != 0){
                          echo '<img src="'.base_url()."ispmedia/logo/".$isplogo.'" class="img-responsive"/>';
                       }else{
                          echo '<img src="'.base_url().'assets/images/decibel.png" class="img-responsive"/>';
                       }
                       ?>
                       </span>
                    </div>
                    <?php
                       $leftperm['navperm']=$this->user_model->leftnav_permission();
                       $this->view('left_nav',$leftperm);
                    ?>
                 </div>
                    <header id="header">
                       <nav class="navbar navbar-default">
                          <div class="container-fluid">
                             <div class="navbar-header">
                                <a class="navbar-brand" href="#">Manage License</a>
                             </div>
                              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                              <?php $this->view('includes/global_setting',$leftperm);  ?>
                            </ul>
                         </div>
                          </div>
                         
                       </nav>
                    </header>
                    <div id="content-wrapper">
                        <div class="mui--appbar-height"></div>
                        <div class="mui-container-fluid" id="right-container-fluid">
                            <div class="add_user">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px">
                                          <div class="row">
                                             <div class="col-sm-12 col-xs-12">
                                                <h2>Currently Active :  <span id="acctype"></span></h2>

                                                <div class="billing_list">
                                                   <ul>
                                                      <?php if($responseCode['country_id'] == '101'){ ?>
                                                      <li style="margin-bottom:15px"><a href="javascript:void(0)" style="margin-left:0px" data-toggle="modal" data-target="#planModal">Recharge License</a></li>
                                                    <?php }else{ ?>
                                                    <li style="margin-bottom:15px"><a href="javascript:void(0)" style="margin-left:0px" data-toggle="modal" data-target="#othercountry_planModal">Recharge License</a></li>
                                                    <?php } ?>
                                                   </ul>
                                                </div>
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <label><strong>License Balance</strong></label>
                                                      <h2 id="license_balance"></h2>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <label><strong>Home License Used</strong></label>
                                                      <h2 id="home_license_used"></h2>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <label><strong>Public WiFi Lisence Used</strong></label>
                                                      <h2 id="public_license_used"></h2>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <label><strong>Total Lisence Used</strong></label>
                                                      <h2 id="total_license_used"></h2>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-sm-12 col-xs-12">
                                                      <ul class="mui-tabs__bar" style="border-bottom:none">
                                                         <li class="mui--is-active">
                                                            <a data-mui-toggle="tab" data-mui-controls="pane-default-1">Usage Report</a>
                                                         </li>
                                                         <li>
                                                            <a data-mui-toggle="tab" data-mui-controls="pane-default-2">Billing report</a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                   <div class="col-sm-12 col-xs-12">
                                                      <div class="mui-tabs__pane mui--is-active" id="pane-default-1">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="row" style="margin-top: 15px">
                                                               <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                                  <div class="mui-select" tabindex="0">
                                                                     <select name="year_filter" tabindex="-1">
                                                                        <option value="">Select Year</option>
                                                                        <?php
                                                                        for($i=2017; $i<= date('Y'); $i++){
                                                                           echo '<option value="'.$i.'">'.$i.'</option>';
                                                                        }
                                                                        ?>
                                                                     </select>
                                                                     <label>Select Year</label>
                                                                     <div class="mui-event-trigger"></div>
                                                                  </div>
                                                               </div>
                                                               <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                  <div class="mui-select" tabindex="0">
                                                                     <select name="month_filter" tabindex="-1">
                                                                        <option value="">Select Month</option>
                                                                        <?php
                                                                        for($i=1; $i<= 12; $i++){
                                                                           echo '<option value="'.$i.'">'.$i.'</option>';
                                                                        }
                                                                        ?>
                                                                     </select>
                                                                     <label>Select Month</label>
                                                                     <div class="mui-event-trigger"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="row">
                                                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                  <div class="row">
                                                                     <div class="table-responsive">
                                                                        <table class="table table-striped">
                                                                           <thead>
                                                                              <tr class="active">
                                                                                 <th>&nbsp;</th>
                                                                                 <th>MONTH</th>
                                                                                 <th>HOME LISENCES USED</th>
                                                                                 <th>PUBLIC LISENCES USED</th>
                                                                                 <th>TOTAL LISENCES VALUE (Rs.)</th>
                                                                              </tr>
                                                                           </thead>
                                                                           <tbody id="usuage_logs">
                                                                              <tr><td colspan="5">No Records Found</td></tr>
                                                                           </tbody>
                                                                        </table>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="mui-tabs__pane" id="pane-default-2">
                                                         <div class="row" style="margin-top:15px">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                               <div class="row">
                                                                  <div class="table-responsive">
                                                                     <table class="table table-striped">
                                                                        <thead>
                                                                           <tr class="active">
                                                                              <th>&nbsp;</th>
                                                                              <th>BILL NUMBER </th>
                                                                              <th>AMOUNT</th>
                                                                              <th>TAXES</th>
                                                                              <th>DATE</th>
                                                                              <th>MODE OF PAYMENT</th>
                                                                           </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                           <!--<tr>
                                                                              <td>1.</td>
                                                                              <td>123123123 </td>
                                                                              <td>25000</td>
                                                                              <td>12300</td>
                                                                              <td>10 Jan 2017 </td>
                                                                              <td><span class="Closed" style="font-weight: 400">Online</span></td>
                                                                           </tr>-->
                                                                           <tr><td colspan="6">No Records Found</td></tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
        </div>
        
        <div id="planModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                
                <?php
                    $formPostUrl = CITRUSPOSTURL;
                    $secret_key = CITRUSSECRETKEY;
                    $vanityUrl = CITRUSVANITYURL;
                    $currency = CITRUSCURRENCY;
                    //$merchantTxnId = uniqid();
                    //$dataid = $vanityUrl . $orderAmount . $merchantTxnId . $currency;
                    //$securitySignature = hash_hmac('sha1', $dataid, $secret_key);
                    $notifyUrl = base_url() . 'manageLicense/paymentnotify';
                    $returnUrl = base_url() . 'manageLicense/paymentresponse';
                ?>
                
                <!--<script src="https://context.citruspay.com/static/kiwi/js/jquery.min.js"></script>-->
                <form id="isp_planform" method="post" action="<?php echo $formPostUrl ?>" name="formorder" onsubmit="add_planbudget(); return false;" autocomplete="off">
                    <input type="hidden" id="merchantTxnId" name="merchantTxnId" value="" />
                    <input type="hidden" id="orderAmount" name="orderAmount" value="" />
                    <input type="hidden" id="currency" name="currency" value="<?php echo $currency; ?>" />
                    <input type="hidden" name="returnUrl" value="<?php echo $returnUrl; ?>" />
                    <input type="hidden" id="notifyUrl" name="notifyUrl" value="<?php echo $notifyUrl; ?>" />
                    <input type="hidden" id="secSignature" name="secSignature" value="" />
                    <input type="hidden" name="email" value="<?php echo $responseCode['email'] ?>" />
                    <input type="hidden" name="phoneNumber" value="<?php echo $responseCode['phone'] ?>" />
                    <input type="hidden" name="firstName" value="<?php echo $responseCode['isp_uid'] ?>" />
                    <input type="hidden" name="mode" id="mode" value="dropOut" />
                    <!--<input type="Submit" style="display: none;" value="Pay Now"/>-->

                    <div class="modal-content">
                     <div class="modal-header" style="padding:5px 15px">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     </div>
                     <div class="modal-body" style="overflow: hidden">
                           <div class="col-sm-12 col-xs-12">
                                 <div class="form-group" style="margin-bottom:0px">
                                    <div class="row">
                                       <div class="col-sm-6 col-xs-6" style="padding:0px">
                                          <h2 style="margin:0 0 10px">Choose the License</h2>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group" style="margin-bottom:0px">
                                    <div class="row">
                                       <div class="table-responsive">
                                          <table class="table table-striped" style="margin-bottom:0px">
                                             <tr align="center">
                                                <th rowspan="2" width="30%" align="center">
                                                   <center>Prepaid License<br/> Fees (INR)</center>
                                                </th>
                                                <th colspan="2">
                                                   <center>Per License Rate (INR)</center>
                                                </th>
                                             </tr>
                                             <tr align="center">
                                                <th>
                                                   <center>Home WiFi(Per Active)</center>
                                                </th>
                                                <th>
                                                   <center>Public WiFi Per AP</center>
                                                </th>
                                             </tr>
                                             <tr align="center">
                                                <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                                <input type="radio" name="select_module" value="25000" data-homewifi="10" data-publicwifi="150" required /></span> 25,000</td>
                                                <td>10</td>
                                                <td>150</td>
                                             </tr>
                                             <tr align="center">
                                                <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                                <input type="radio" name="select_module" value="50000" data-homewifi="10" data-publicwifi="140" required /></span> 50,000</td>
                                                <td>10</td>
                                                <td>140</td>
                                             </tr>
                                             <tr align="center">
                                                <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                                <input type="radio" name="select_module" value="100000" data-homewifi="9" data-publicwifi="125" required /></span> 1,00,000</td>
                                                <td>9</td>
                                                <td>125</td>
                                             </tr>
                                             <tr align="center">
                                                <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                                <input type="radio" name="select_module" value="250000" data-homewifi="8" data-publicwifi="110" required /></span> 2,50,000</td>
                                                <td>8</td>
                                                <td>110</td>
                                             </tr>
                                             <tr align="center">
                                                <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                                <input type="radio" name="select_module" value="500000" data-homewifi="7" data-publicwifi="100" required /></span> 5,00,000</td>
                                                <td>7</td>
                                                <td>100</td>
                                             </tr>
                                             <tr align="center">
                                                <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                                <input type="radio" name="select_module" value="1000000" data-homewifi="6" data-publicwifi="100" required /></span> 10,00,000</td>
                                                <td>6</td>
                                                <td>100</td>
                                             </tr>
                                             <tr align="center">
                                                <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                                <input type="radio" name="select_module" value="2500000" data-homewifi="5" data-publicwifi="100" required /></span> 25,00,000</td>
                                                <td>5</td>
                                                <td>100</td>
                                             </tr>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group" style="margin-bottom:10px;">
                                    <div class="row">*Taxes will be applied as applicable.</div>
                                 </div>
                                 <div class="form-group" style="margin-bottom:10px;">
                                    <div class="row">
                                       <div class="col-sm-12 col-xs-12 text-center">
                                          <span style="font-size:16px; color:#000"><a href="#" style="color: #000">Definitions</a>|</span>
                                          <span style="font-size:16px;color:#000"><a href="#" style="color: #000">How it works</a>|</span>
                                          <span style="font-size:16px;color:#000"><a href="#" style="color: #000">Additional Services</a>|</span>
                                       </div>
                                    </div>
                                 </div>
                           </div>
                     </div>
                     <div class="modal-footer" style="margin-bottom:10px;">
                        <div class="col-lg-9">
                           <input type="submit" class="paybtn btn mui-btn--accent" style="width: 130px ; line-height: 20px; height:40px;" value="PAY NOW"/>
                           <button type="button" class="btn btn-default" onclick="reset_planmodule()">Reset</button>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
        
        <div id="othercountry_planModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <form id="othercountryisp_planform" method="post" action="<?php echo base_url().'manageLicense/pgredirect' ?>" autocomplete="off">
                    <div class="modal-content">
                     <div class="modal-header" style="padding:5px 15px">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     </div>
                     <div class="modal-body" style="overflow: hidden">
                           <div class="col-sm-12 col-xs-12">
                                 <div class="form-group" style="margin-bottom:0px">
                                    <div class="row">
                                       <div class="col-sm-6 col-xs-6" style="padding:0px">
                                          <h2 style="margin:0 0 10px">Choose the License</h2>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group" style="margin-bottom:0px">
                                    <div class="row">
                                       <div class="table-responsive">
                                          <table class="table table-striped" style="margin-bottom:0px">
                                             <tr align="center">
                                                <th rowspan="2" width="30%" align="center">
                                                   <center>Prepaid License<br/> Fees </center>
                                                </th>
                                                <th colspan="2">
                                                   <center>Per License Rate</center>
                                                </th>
                                             </tr>
                                             <tr align="center">
                                                <th>
                                                   <center>Home WiFi(Per Active)</center>
                                                </th>
                                                <th>
                                                   <center>Public WiFi Per AP</center>
                                                </th>
                                             </tr>
                                             <tr align="center">
                                                <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                                <input type="radio" name="select_module" value="1" data-homewifi="0.50" data-publicwifi="50" required /></span> 1</td>
                                                <td>0.50</td>
                                                <td>50</td>
                                             </tr>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group" style="margin-bottom:10px;">
                                    <div class="row">*Taxes will be applied as applicable.</div>
                                 </div>
                                 <div class="form-group" style="margin-bottom:10px;">
                                    <div class="row">
                                       <div class="col-sm-12 col-xs-12 text-center">
                                          <span style="font-size:16px; color:#000"><a href="#" style="color: #000">Definitions</a>|</span>
                                          <span style="font-size:16px;color:#000"><a href="#" style="color: #000">How it works</a>|</span>
                                          <span style="font-size:16px;color:#000"><a href="#" style="color: #000">Additional Services</a>|</span>
                                       </div>
                                    </div>
                                 </div>
                           </div>
                     </div>
                     <div class="modal-footer" style="margin-bottom:10px;">
                        <div class="col-lg-9">
                           <input type="submit" class="paybtn btn mui-btn--accent" style="width: 130px ; line-height: 20px; height:40px;" value="PAY NOW"/>
                           <button type="button" class="btn btn-default" onclick="reset_planmodule()">Reset</button>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
        
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/material.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
        <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/crbnMenu.js"></script>
        <script type="text/javascript">
            if ($(window)) {
                $(function () {
                   $('.menu').crbnMenu({
                   hideActive: true
                   });
                });
            }
            $(document).ready(function() {
                var height = $(window).height();
                $('#main_div').css('height', height);
                $('#right-container-fluid').css('height', height);
                setTimeout(function (){ $('.loading').addClass('hide') },1000);
                
                var ispadmin_uid = "<?php echo ISPID ?>";
                $('.loading').removeClass('hide');
                $.ajax({
                    url: base_url+'manageLicense/ispbilling_details',
                    type: 'POST',
                    data: 'ispadmin_uid='+ispadmin_uid,
                    dataType: 'json',
                    async: false,
                    success: function(data){
                        //{"wallet_amt":"0.00","balance_amt":0,"total_license_used":"0.00","home_license_used":"0"}
                        $('#acctype').html(data.decibel_account.toUpperCase());
                        $('#license_balance').html(data.balance_amt);
                        $('#home_license_used').html(data.home_license_used);
                        $('#public_license_used').html(data.public_license_used);
                        $('#total_license_used').html(data.total_license_used);
                        if (data.usuage_logs != '') {
                            $('#usuage_logs').html(data.usuage_logs);
                        }
                        $('.loading').addClass('hide');
                    }
                });
            });
            
            function add_planbudget() {
                var decibel_plan_budget = $('input[type=radio][name=select_module]:checked').val();
                var homewifi_budget = $('input[type=radio][name=select_module]:checked').data('homewifi');
                var publicwifi_budget = $('input[type=radio][name=select_module]:checked').data('publicwifi');
                
                decibel_plan_budget = parseInt(decibel_plan_budget) + parseInt(decibel_plan_budget * 0.18); 
                $('#orderAmount').val(decibel_plan_budget);
                $('.paybtn').addClass('hide');
                $.ajax({
                    url: base_url+'manageLicense/hashmac',
                    type: 'POST',
                    data: 'orderAmount='+decibel_plan_budget,
                    dataType: 'json',
                    async: false,
                    success: function(data){
                        var hashmac = data.ssign;
                        $('#secSignature').val(hashmac);
                        $('#merchantTxnId').val(data.merchantTxnId);
                        document.getElementById("isp_planform").submit();
                    }
                });
             }
         
        </script>
        
    </body>
</html>