<?php
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");

?>

<center><h1>Please do not refresh this page...</h1></center>
<form id="stripedetailform" action="<?php echo base_url().'manageLicense/spaysuccess' ?>" method="post">
	<div style="margin:auto;">
		<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
		data-key="pk_live_fEHEWhOB8hlOt5r9uJSLvXd7"
		data-amount="<?php echo $TXN_AMOUNT*100;?>" data-description="Payment For <?php echo "$".$TXN_AMOUNT;?>">
		</script>
	</div>
	<input type="hidden" name="amt" value="<?php echo $TXN_AMOUNT*100;?>">
	<input type="hidden" name="userid" value="<?php echo $CUST_ID;?>">
		 
</form>
<script type="text/javascript">var base_url = "<?php echo base_url() ?>";</script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
 $(function(){  // document.ready function...
   $('.stripe-button-el').css('display', 'none');
   setTimeout(function(){
      $('.stripe-button-el').trigger( "click" );
    },5000);
});

$(document).on("DOMNodeRemoved",".stripe_checkout_app", close);

function close(){
  window.location = base_url+'manageLicense';
}
</script>
</body>
</html>