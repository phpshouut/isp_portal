
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title><?php echo $this->session->userdata['isp_session']['isp_name'] ?> </title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-material-datetimepicker.css" />
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <style type="text/css">
	 .pac-container{
	    z-index: 1051 !important;
	 }
      </style>
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading">
	 <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                     <?php
                     $isplogo = $this->ttask_model->get_ispdetail_info();
                     if($isplogo != 0){
                        echo '<img src="'.base_url()."ispmedia/logo/".$isplogo.'" class="img-responsive"/>';
                     }else{
                        echo '<img src="'.base_url().'assets/images/decibel.png" class="img-responsive"/>';
                     }
                     ?>
                     </span>
                  </div>
                  <?php
		     $leftperm['navperm']=$this->ttask_model->leftnav_permission();
		     $this->view('left_nav',$leftperm);
		  ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">Team Task Management</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
			      <?php
                                $this->load->view('includes/global_setting',$leftperm);
			      ?>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="add_user">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <ul class="mui-tabs__bar">
                                    <li class="mui--is-active" onclick="open_tasklist()">
                                       <a data-mui-toggle="tab" data-mui-controls="opentickets_pane" class="tab_menu">
                                       Manage Tasks
                                       </a>
                                    </li>
                                    <li onclick="add_taskpane()">
                                       <a data-mui-toggle="tab" data-mui-controls="addnewtickets_pane" class="tab_menu">
                                       Add Tasks
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <input type="hidden" class="current_taxapplicable" id="current_taxapplicable" value="" />
			      <input type="hidden" id="ispcountry_currency" value="<?php //echo $ispcodet['currency'] ?>" />
				 <div class="mui-tabs__pane mui--is-active" id="opentickets_pane">
				    <div class="">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:10px 0px;padding:0px">
					  <div class="row">
					     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
						<div class="mui-select">
						   <select name="filterby_assignedto" onchange="filtertask()">
						      <option value="">Select Member</option>
						      <?php echo $this->ttask_model->ticket_sorters_list(); ?>
						   </select>
						   <label>Select Member</label>
						</div>
					     </div>
					     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
						<div class="mui-select">
						   <select name="filterby_taskstatus" onchange="filtertask()">
						      <option value="">Select Status</option>
						      <option value="0">Open</option>
						      <option value="1">Pending</option>
						      <option value="2">Closed</option>
						   </select>
						   <label>Select Task Status<sup>*</sup></label>
						</div>
					     </div>
					     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
						<div class="mui-select">
						   <select name="filterby_priority" onchange="filtertask()">
						      <option value="">Select Priority</option>
						      <option value="low">Low</option>
						      <option value="medium">Medium</option>
						      <option value="high">High</option>
						   </select>
						   <label>Select Priority<sup>*</sup></label>
						</div>
					     </div>
					  </div>
				       </div>
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:10px 0px;padding:0px">
					  <div class="table-responsive">
					     <table class="table table-striped">
						<thead>
						   <tr class="active">
						      <th>&nbsp;</th>
						      <th>TASK</th>
						      <th>ASSIGNED BY</th>
						      <th>ASSIGNED TO</th>
						      <th>PRIORITY</th>
						      <th>ADDED ON</th>
						      <th>TAT DAYS</th>
						      <th>STATUS</th>
						      <th colspan="2">ACTIONS</th>
						   </tr>
						</thead>
						<tbody id="allopentaskdiv"></tbody>
					     </table>
					  </div>
				       </div>
				    </div>
				 </div>
				 <div class="mui-tabs__pane" id="addnewtickets_pane">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:20px 0px;padding:0px">
					  <form action="" method="post" id="task_request_form" autocomplete="off" onsubmit="add_team_task_request(); return false;">
					     <div class="col-sm-7 col-xs-7">
                                                <div class="row" style="margin-bottom:20px;">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <div class="mui-textfield">
                                                           <input type="text" name="task_name" value="" maxlength="50"  required>
                                                           <label>Task Name<sup>*</sup></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						      <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
							 <div class="input-group" style="margin-top:-5px">
							    <div class="input-group-addon" style="padding:6px 10px">
							       <i class="fa fa-calendar"></i>
							    </div>
							    <input type="text" class="form-control task_assigndate" id="" placeholder="Task Datetime*" name="task_assign_datetimeformat" required>
							    <input name="task_assign_datetime" type="hidden" value="">
							 </div>
						      </div>
						   </div>
                                                 </div>
                                                <div class="row" style="margin-bottom:20px;">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="mui-textfield" id="tktdtme">
                                                        <textarea style="resize: vertical;" rows="3" name="task_description" required></textarea>
							 <label>Task Description<sup>*</sup></label>
						      </div>
						   </div>
                                                 </div>
						<div class="row">
						   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						      <div class="mui-select">
							<select name="task_assignto" required>
							    <option value="">Assign Task To</option>
							    <?php echo $this->ttask_model->ticket_sorters_list(); ?>
							 </select>
							 <label>Assign Task To<sup>*</sup></label>
						      </div>
						   </div>
						   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						      <div class="mui-select">
							 <select name="task_priority" required>
							    <option value="">Select Ticket Priority</option>
							    <option value="low">Low</option>
							    <option value="medium">Medium</option>
							    <option value="high">High</option>
							 </select>
							 <label>Priority<sup>*</sup></label>
						      </div>
						   </div>
						</div>
						<div class="form-group">
                                                    <div class="row">
                                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                          <div class="mui-radio">
                                                             <label class="nopadding">Choose Geolocation Type <sup>*</sup> </label> &nbsp;
                                                             <input type="radio" name="geoaddress_searchtype" required="required" value="byaddr" checked>By Address &nbsp;
                                                             <input type="radio" name="geoaddress_searchtype" required="required" value="bylatlng">By Latitude/Longitude
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                                 <div class="geobyaddr">
                                                    <div class="form-group">
                                                       <div class="row">
                                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px;">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                   <input type="text" id="geolocation" name="geoaddr" value="" required placeholder="">
                                                                   <label>GeoAddress</label>
                                                               </div>
                                                             </div>
                                                          </div>
                                                       </div>
                                                    </div>
                                                    <div class="form-group">
                                                       <div class="row">
                                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                   <input type="text"  id="placeid"  value="" name="placeid"  readonly>
                                                                   <label>Placeid</label>
                                                                 </div>
                                                             </div>
                                                             
                                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                   <input type="text"  id="lat" value="" name="lat" readonly>
                                                                   <label>Lat</label>
                                                                </div>
                                                             </div>
                                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">   
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                   <input type="text" id="long" name="long" value=""  readonly>
                                                                   <label>Long</label>
                                                               </div>
                                                             </div>
                                                          </div>
                                                       </div>
                                                     </div>
                                                 </div>
                                                 <div class="geobylatlng hide">
                                                    <div class="form-group">
                                                       <div class="row">
                                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding">         
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                   <input type="text" name="geolat" id="geolat" value="">
                                                                   <label>Lat</label>
                                                                </div>
                                                             </div>
                                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">   
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                   <input type="text" name="geolong" id="geolong" value="">
                                                                   <label>Long</label>
                                                               </div>
                                                             </div>
                                                             <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="margin-top:20px; background-color:#a1a1a1; text-align:center; padding:5px; color:#ffffff;">
                                                                <span onclick="reverse_geolocation()" style="cursor:pointer">GET Address</span>
                                                             </div>
                                                          </div>
                                                       </div>
                                                    </div>
                                                    <div class="form-group">
                                                       <div class="row">
                                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px;">
                                                                <div class="mui-textfield">
                                                                   <input type="text" id="geoaddress" name="geoaddress"  value="" readonly>
                                                                   <label>GeoAddress</label>
                                                                 </div>
                                                             </div>
                                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px;">
                                                                <div class="mui-textfield">
                                                                   <input type="text" id="geoplaceid" name="geoplaceid"  value="" readonly>
                                                                   <label>Placeid</label>
                                                                 </div>
                                                             </div>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
						<button type="button" class="mui-btn mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
						<input type="submit" class="mui-btn mui-btn--primary mui-btn--flat" style="background-color:#f00f64; color:#fff" value="ADD" >
					     </div>
					  </form>
				       </div>
				    </div>
				 </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="edittaskModal" data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <h4 class="modal-title">
		     <strong>MANAGE TASK</strong>
		  </h4>
	       </div>
	       
	       <div class="modal-body">
		  <div class="row">
		     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="row">
			   <ul class="mui-tabs__bar">
			      <li class="mui--is-active">
				 <a data-mui-toggle="tab" data-mui-controls="edittask_pane" class="tab_menu">
				 TASK DETAILS
				 </a>
			      </li>
			      <li onclick="showtask_comments()">
				 <a data-mui-toggle="tab" data-mui-controls="addtaskcomment_pane" class="tab_menu" id="">
				 TASK PROGRESS / COMMENTS
				 </a>
			      </li>
			   </ul>
			</div>
		     </div>
		     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
			<div class="mui-tabs__pane mui--is-active" id="edittask_pane" style="margin:20px 0px;">
			   <form action="" method="post" id="edittask_request_form" autocomplete="off" onsubmit="edit_team_task_request(); return false;">
			      <input type="hidden" name="taskid_foredit" value="" />
			      <div class="row">
				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				    <div class="mui-textfield">
				       <input type="text" name="task_name" value="" maxlength="50"  required>
				       <label>Task Name<sup>*</sup></label>
				    </div>
				 </div>
				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				    <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
				       <div class="input-group" style="margin-top:-5px">
					  <div class="input-group-addon" style="padding:6px 10px">
					     <i class="fa fa-calendar"></i>
					  </div>
					  <input type="text" class="form-control task_assigndate" id="" placeholder="Task Datetime*" name="task_assign_datetimeformat" disabled required>
					  <input name="task_assign_datetime" type="hidden" value="">
				       </div>
				    </div>
				 </div>
			       </div>
			      <div class="row">
				  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				    <div class="mui-textfield" id="tktdtme">
				      <textarea style="resize: vertical;" rows="3" name="task_description" required></textarea>
				       <label>Task Description<sup>*</sup></label>
				    </div>
				 </div>
			       </div>
			      <div class="row">
				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				    <div class="mui-select">
				      <select name="task_assignto" required>
					  <option value="">Assign Task To</option>
				       </select>
				       <label>Assign Task To<sup>*</sup></label>
				    </div>
				 </div>
				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				    <div class="mui-select">
				       <select name="task_priority" required>
					  <option value="">Select Ticket Priority</option>
					  <option value="low">Low</option>
					  <option value="medium">Medium</option>
					  <option value="high">High</option>
				       </select>
				       <label>Priority<sup>*</sup></label>
				    </div>
				 </div>
			      </div>
			      <div class="row">
				 <!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				    <div class="mui-radio">
				       <label class="nopadding">Alert Member<sup>*</sup> </label> &nbsp;
				       <input type="radio" name="alert_member" required="required" value="1" checked>Yes &nbsp;
				       <input type="radio" name="alert_member" required="required" value="0">No
				    </div>
				 </div>-->
				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				    <div class="mui-select">
				       <select name="edittask_status" required>
					  <option value="0">Open</option>
					  <option value="1">Pending</option>
					  <option value="2">Closed</option>
				       </select>
				       <label>Task Status <sup>*</sup></label>
				    </div>
				 </div>
			      </div>
			      <div class="row">
				 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				    <div class="mui-radio">
				       <label class="nopadding">Choose Geolocation Type <sup>*</sup> </label> &nbsp;
				       <input type="radio" name="geoaddress_searchtype" required="required" value="byaddr">By Address &nbsp;
				       <input type="radio" name="geoaddress_searchtype" required="required" value="bylatlng">By Latitude/Longitude
				    </div>
				 </div>
			      </div>
			      <div class="geobyaddr">
				 <div class="form-group">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px;">
					     <div class="mui-textfield">
						<input type="text" id="geolocation_onedit" name="geoaddr" value="" required placeholder="">
						<label>GeoAddress</label>
					    </div>
					  </div>
				       </div>
				    </div>
				 </div>
				 <div class="form-group">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
					     <div class="mui-textfield">
						<input type="text"  id="placeid_onedit"  value="" name="placeid"  readonly>
						<label>Placeid</label>
					      </div>
					  </div>
					  
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
					     <div class="mui-textfield">
						<input type="text"  id="lat_onedit" value="" name="lat" readonly>
						<label>Lat</label>
					     </div>
					  </div>
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">   
					     <div class="mui-textfield">
						<input type="text" id="long_onedit" name="long" value=""  readonly>
						<label>Long</label>
					    </div>
					  </div>
				       </div>
				    </div>
				  </div>
			      </div>
			      <div class="geobylatlng hide">
				 <div class="form-group">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding">         
					     <div class="mui-textfield">
						<input type="text" name="geolat" id="geolat_onedit" value="">
						<label>Lat</label>
					     </div>
					  </div>
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">   
					     <div class="mui-textfield">
						<input type="text" name="geolong" id="geolong_onedit" value="">
						<label>Long</label>
					    </div>
					  </div>
					  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="margin-top:20px; background-color:#a1a1a1; text-align:center; padding:5px; color:#ffffff;">
					     <span onclick="reverse_geolocation_onedit()" style="cursor:pointer">GET Address</span>
					  </div>
				       </div>
				    </div>
				 </div>
				 <div class="form-group">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px;">
					     <div class="mui-textfield">
						<input type="text" id="geoaddress_onedit" name="geoaddress"  value="" readonly>
						<label>GeoAddress</label>
					      </div>
					  </div>
					  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px;">
					     <div class="mui-textfield">
						<input type="text" id="geoplaceid_onedit" name="geoplaceid"  value="" readonly>
						<label>Placeid</label>
					      </div>
					  </div>
				       </div>
				    </div>
				 </div>
			      </div>
			      <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
			      <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="Save" />
			   </form>
			</div>
			<div class="mui-tabs__pane" id="addtaskcomment_pane" style="margin:20px 0px;">
			   <form action="" method="post" id="addtask_comment_form" autocomplete="off" onsubmit="addtask_comment(); return false;">
			      <input type="hidden" name="taskid_foredit" value="" />
			      <div class="row">
				  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				    <div class="mui-textfield">
				      <textarea style="resize: vertical;" rows="2" name="task_comment" required></textarea>
				       <label>Task Comment<sup>*</sup></label>
				    </div>
				 </div>
			       </div>
			      <input type="submit" class="mui-btn mui-btn--large mui-btn--accent pull-right" value="Save" />
			   </form>
			   <div class="row">
			      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 25px">
				 <ul class="task_cmmnt_history">
				    <li style="color: #c1c1c1">No comments added yet!!</li>
				 </ul>
			      </div>
			   </div>
			</div>
		     </div>
		  </div>
	       </div>
	       <div class="modal-footer">
	       </div>
	    </div>
	    <!-- /.modal-content -->
	 </div>
      </div>
      
      <!-- /Start your project here-->

      <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/material.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/moment-with-locales.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap-material-datetimepicker.js"></script>
      <link href="<?php echo base_url() ?>assets/css/bootstrap-toggle.min.css" rel="stylesheet">
      <script src="<?php echo base_url() ?>assets/js/bootstrap-toggle.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/crbnMenu.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA61iANDo6jqGYQm0SjXzIvmOEnks-MpG0&amp;libraries=places"  type="text/javascript"></script>
      <script type="text/javascript">
         if ($(window)) {
            $(function () {
               $('.menu').crbnMenu({
               hideActive: true
               });
            });
         }
         $(document).ready(function() {
            var height = $(window).height();
            $('#main_div').css('height', height);
            setTimeout(function (){ $('.loading').addClass('hide') },1000);
	    open_tasklist();
	    
	    $('.task_assigndate').bootstrapMaterialDatePicker({
               weekStart: 0,
	       format: 'DD-MM-YYYY HH:mm',
	       //shortTime : true,
	       maxDate: moment()
            }).on('change', function(e, date){

	       var dateObj = new Date(date);
	       var month = (dateObj.getMonth()+1).toString();
	       var day = dateObj.getDate().toString();
	       var seconds = dateObj.getSeconds();
	       var year = dateObj.getFullYear();
	       var dbtktdatetime =  year + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + dateObj.getHours() +":"+ dateObj.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;;
               $('input[name="task_assign_datetime"]').val(dbtktdatetime);
            });
	    
	    $('body').on('change', 'input[type=radio][name=geoaddress_searchtype]', function(){
	       var searchtype = $(this).val();
	       if (searchtype == 'byaddr') {
		  $('.geobyaddr').removeClass('hide');
		  $('.geobylatlng').addClass('hide');
		  $('#geolocation').attr('required','required');
		  $('#geolat').removeAttr('required');
		  $('#geolong').removeAttr('required');
		  $('#geolat_onedit').removeAttr('required');
		  $('#geolong_onedit').removeAttr('required');
	       }else if (searchtype == 'bylatlng') {
		  $('.geobyaddr').addClass('hide');
		  $('.geobylatlng').removeClass('hide');
		  $('#geolocation').removeAttr('required');
		  $('#geolat').attr('required','required');
		  $('#geolong').attr('required','required');
		  $('#geolat_onedit').attr('required','required');
		  $('#geolong_onedit').attr('required','required');
	       }
	    });
            $.material.init();
         });
	 
	 function initialize() {
	    var input = document.getElementById('geolocation');
	    //var options = {componentRestrictions: {country: 'in'}};
	    var autocomplete = new google.maps.places.Autocomplete(input);
	    google.maps.event.addListener(autocomplete, 'place_changed', function () {
	       var place = autocomplete.getPlace();
	       document.getElementById('lat').value = place.geometry.location.lat().toFixed(6);
	       document.getElementById('long').value = place.geometry.location.lng().toFixed(6);
	       document.getElementById('placeid').value = place.place_id;
	       $("#placeid").attr('class','mui--is-dirty valid mui--is-not-empty');
	       $("#lat").attr('class','mui--is-dirty valid mui--is-not-empty');
	       $("#long").attr('class','mui--is-dirty valid mui--is-not-empty');
	    });
	 }
	  
	 function reverse_geolocation() {
	    var geolat = $('#geolat').val();
	    var geolng = $('#geolong').val();
	    
	    if (geolat == '' && geolng == '') {
	       window.alert("Please enter Latitude & Longitude of the location."); 
	    }else{
	       var latlng = {lat: parseFloat(geolat), lng: parseFloat(geolng)};
	       var geocoder = new google.maps.Geocoder;
	       geocoder.geocode({'location': latlng}, function(results, status) {
		  if (status === 'OK') {
		     if (results[0]) {
			$('#geoaddress').val(results[0].formatted_address);
			$('#geoplaceid').val(results[0].place_id);
		     } else {
			window.alert('No results found');
		     }
		  } else {
		    window.alert('Geocoder failed due to: ' + status);
		  }
	       });
	    }
	 }
	 google.maps.event.addDomListener(window, 'load', initialize);

	 /******* EDIT TASK ************/
	 function initialize_onedit() {
	    var input = document.getElementById('geolocation_onedit');
	    //var options = {componentRestrictions: {country: 'in'}};
	    var autocomplete = new google.maps.places.Autocomplete(input);
	    google.maps.event.addListener(autocomplete, 'place_changed', function () {
	       var place = autocomplete.getPlace();
	       document.getElementById('lat_onedit').value = place.geometry.location.lat().toFixed(6);
	       document.getElementById('long_onedit').value = place.geometry.location.lng().toFixed(6);
	       document.getElementById('placeid_onedit').value = place.place_id;
	       $("#placeid_onedit").attr('class','mui--is-dirty valid mui--is-not-empty');
	       $("#lat_onedit").attr('class','mui--is-dirty valid mui--is-not-empty');
	       $("#long_onedit").attr('class','mui--is-dirty valid mui--is-not-empty');
	    });
	 }
	  
	 function reverse_geolocation_onedit() {
	    var geolat = $('#geolat_onedit').val();
	    var geolng = $('#geolong_onedit').val();
	    
	    if (geolat == '' && geolng == '') {
	       window.alert("Please enter Latitude & Longitude of the location."); 
	    }else{
	       var latlng = {lat: parseFloat(geolat), lng: parseFloat(geolng)};
	       var geocoder = new google.maps.Geocoder;
	       geocoder.geocode({'location': latlng}, function(results, status) {
		  if (status === 'OK') {
		     if (results[0]) {
			$('#geoaddress_onedit').val(results[0].formatted_address);
			$('#geoplaceid_onedit').val(results[0].place_id);
		     } else {
			window.alert('No results found');
		     }
		  } else {
		    window.alert('Geocoder failed due to: ' + status);
		  }
	       });
	    }
	 }
	 google.maps.event.addDomListener(window, 'load', initialize_onedit);

	 function add_taskpane() {
	    $('form#task_request_form')[0].reset();
	    $('.geobyaddr').removeClass('hide');
	    $('.geobylatlng').addClass('hide');
	    $('#geolocation').attr('required','required');
	    $('#geolat').removeAttr('required');
	    $('#geolong').removeAttr('required');
	 }
	 function add_team_task_request() {
	    $('.loading').removeClass('hide');
	    var formdata = $('form#task_request_form').serialize();
	    $.ajax({
	       url: base_url+'ttask/add_task',
	       type: 'POST',
	       data: formdata,
	       dataType: 'json',
	       success: function(result){
		  open_tasklist();
	       }
	    });
	 }
	 
	 function open_tasklist() {
	    $('.loading').removeClass('hide');
	    var team_membr = $('select[name="filterby_assignedto"] option[value=""]').prop('selected', true);
	    var taskstatus = $('select[name="filterby_taskstatus"] option[value=""]').prop('selected', true);
	    var priority = $('select[name="filterby_priority"] option[value=""]').prop('selected', true);
	    $.ajax({
	       url: base_url+'ttask/open_tasklist',
	       type: 'POST',
	       dataType: 'json',
	       success: function(result){
		  mui.tabs.activate('opentickets_pane');
		  $('#allopentaskdiv').html(result.task_list);
		  $('.loading').addClass('hide');
	       }
	    });
	 }
	 
	 function showedittaskModal(taskid) {
	    $('.loading').removeClass('hide');
	    $('form#edittask_request_form')[0].reset();
	    $('input[name="taskid_foredit"]').val(taskid);
	    
	    $.ajax({
	       url: base_url+'ttask/taskdetails',
	       type: 'POST',
	       dataType: 'json',
	       data: 'taskid='+taskid,
	       success: function(result){
		  $('.loading').addClass('hide');
		  $('form#edittask_request_form input[name="task_name"]').val(result.task_name);
		  $('form#edittask_request_form input[name="task_assign_datetimeformat"]').val(result.task_assign_datetimeformat);
		  $('form#edittask_request_form input[name="task_assign_datetime"]').val(result.task_assign_datetime);
		  $('form#edittask_request_form textarea[name="task_description"]').val(result.task_description);
		  $('form#edittask_request_form select[name="task_assignto"]').empty().append(result.task_assignto);
		  $('form#edittask_request_form select[name="task_priority"] option[value="'+result.task_priority+'"]').prop('selected', true);
		  $('form#edittask_request_form input[name="geoaddress_searchtype"][value="'+result.geoaddress_searchtype+'"]').prop('checked', true);
		  if (result.geoaddress_searchtype == 'byaddr') {
		     $('.geobyaddr').removeClass('hide');
		     $('.geobylatlng').addClass('hide');
		     $('#geolocation_onedit').attr('required','required');
		     $('#geolat_onedit').removeAttr('required');
		     $('#geolong_onedit').removeAttr('required');
		     
		     $('form#edittask_request_form input[name="geoaddr"]').val(result.geoaddr);
		     $('form#edittask_request_form input[name="placeid"]').val(result.placeid);
		     $('form#edittask_request_form input[name="lat"]').val(result.latitude);
		     $('form#edittask_request_form input[name="long"]').val(result.longitude);
		     
		  }else if (result.geoaddress_searchtype == 'bylatlng') {
		     $('.geobyaddr').addClass('hide');
		     $('.geobylatlng').removeClass('hide');
		     $('#geolocation_onedit').removeAttr('required');
		     $('#geolat_onedit').attr('required','required');
		     $('#geolong_onedit').attr('required','required');
		     
		     $('form#edittask_request_form input[name="geoaddress"]').val(result.geoaddr);
		     $('form#edittask_request_form input[name="geoplaceid"]').val(result.placeid);
		     $('form#edittask_request_form input[name="geolat"]').val(result.latitude);
		     $('form#edittask_request_form input[name="geolong"]').val(result.longitude);
		  }
		  mui.tabs.activate('edittask_pane');
		  $('.task_cmmnt_history').html('');
		  $('#edittaskModal').modal('show');
	       }
	    });
	 }
	 
	 function edit_team_task_request() {
	    $('.loading').css({'z-index' : '999999'});
	    $('.loading').removeClass('hide');
	    var formdata = $('form#edittask_request_form').serialize();
	    $.ajax({
	       url: base_url+'ttask/edit_task',
	       type: 'POST',
	       data: formdata,
	       dataType: 'json',
	       success: function(result){
		  open_tasklist();
		  $('.loading').removeAttr('style');
		  $('.loading').addClass('hide');
	       }
	    });
	 }
	 function addtask_comment(){
	    $('.loading').css({'z-index' : '999999'});
	    $('.loading').removeClass('hide');
	    var formdata = $('form#addtask_comment_form').serialize();
	    $.ajax({
	       url: base_url+'ttask/addtask_comment',
	       type: 'POST',
	       data: formdata,
	       dataType: 'json',
	       success: function(result){
		  $('form#addtask_comment_form')[0].reset();
		  $('.task_cmmnt_history').html(result.task_summary);
		  $('.loading').removeAttr('style');
		  $('.loading').addClass('hide');
	       }
	    });
	 }
	 function showtask_comments(){
	    var taskid = $('input[name="taskid_foredit"]').val();
	    $.ajax({
	       url: base_url+'ttask/showtask_comment',
	       type: 'POST',
	       data: 'taskid_foredit='+taskid,
	       dataType: 'json',
	       success: function(result){
		  $('form#addtask_comment_form')[0].reset();
		  $('.task_cmmnt_history').html(result.task_summary);
	       }
	    });
	 }
	 function filtertask() {
	    $('.loading').removeClass('hide');
	    var team_membr = $('select[name="filterby_assignedto"] option:selected').val();
	    var taskstatus = $('select[name="filterby_taskstatus"] option:selected').val();
	    var priority = $('select[name="filterby_priority"] option:selected').val();
	    $.ajax({
	       url: base_url+'ttask/filtertask',
	       type: 'POST',
	       data: 'team_membr='+team_membr+'&taskstatus='+taskstatus+'&priority='+priority,
	       dataType: 'json',
	       success: function(result){
		  mui.tabs.activate('opentickets_pane');
		  $('#allopentaskdiv').html(result.task_list);
		  $('.loading').addClass('hide');
	       }
	    });
	 }
      </script>

   </body>
</html>