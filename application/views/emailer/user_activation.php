<?php

$message = '    
    <table width="920" align="center" style="border:1px solid #A3A3A3;" cellpadding="0" cellspacing="0">
       <tr>
          <td>
             <table width="911" align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                   <td width="219" style="background-color:#F4F2F3;" valign="top">
                      <table width="100%" border="0" style="border-bottom-right-radius:15px; background-color:#FFFFFF;">
                         <tr>
                            <td height="30" width="50px;" />
                         </tr>
                         <tr>
                            <td valign="top" align="left" style="padding-left:20px;">
                               <img src="'.base_url().'assets/images/emailer/shouut_mob_logo.svg" alt="logo" width="160px" />
                            </td>
                         </tr>
                         <tr>
                            <td style="font-family: Open Sans, sans-serif; font-size:12px; padding-left:20px;font-weight:bold;">
                               <span style="color:#F00F64;font-weight:bold;"><br /> Giant Tech Labs Pvt. Ltd,</span><br />
                               <table width="100%" border="0">
                                  <tr>
                                     <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:bold;">Second Floor, 840 Chirag Delhi,</td>
                                  </tr>
                                  <tr>
                                     <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:bold;"> New Delhi - 110017</td>
                                  </tr>
                                  <tr>
                                     <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:bold;">Ph.No : +91 9810678773</td>
                                  </tr>
                                  <tr>
                                     <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:bold;">E-mail : talktous@shouut.com </td>
                                  </tr>
                               </table>
                            </td>
                         </tr>
                      </table>
                   </td>
                   <td width="646" style="background-color:#F4F2F3;" valign="top">
                      <table width="100%" border="0" cellpadding="0" cellspacing="0">
                         <tr>
                            <td width="100%" style="background-color:#FFFFFF;" align="right" valign="top">&nbsp;</td>
                         </tr>
                         <tr>
                            <td bgcolor="#FFFFFF" style="height:auto;">
                               <table width="100%" border="0" style="border-top-left-radius:15px;background-color:#F4F2F3;">
                                  <tr>
                                     <td width="55%" height="195px;" style="padding-left:35px;font-family: Open Sans, sans-serif; font-size:12px;font-weight:600;">
                                        <table width="100%" border="0">
                                           <tr>
                                              <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:600;"><span style="line-height: 25px;">Name</span> : <span style="line-height: 25px;">'.ucfirst($firstname).' '.ucfirst($lastname).'</span></td>
                                           </tr>
                                           <tr>
                                              <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:600;">Address : '.$address.'</td>
                                           </tr>
                                           <tr>
                                              <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:600;">'.$state.'</td>
                                           </tr>
                                           <tr>
                                              <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:600;">India</td>
                                           </tr>
                                           <tr>
                                              <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:600;">Home : </td>
                                           </tr>
                                           <tr>
                                              <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:600;">Mobile : '.$mobile.'</td>
                                           </tr>
                                        </table>
                                     </td>
                                     <td width="55%">
                                        <table width="91%" border="0">
                                           <tr>
                                              <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:600;">User Id : '.$uid.'</td>
                                           </tr>
                                           <tr>
                                              <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:600;">A/C No : '.$uid.'</td>
                                           </tr>
                                        </table>
                                     </td>
                                  </tr>
                               </table>
                            </td>
                         </tr>
                         <tr height="20px">
                            <td />
                         </tr>
                      </table>
                   </td>
                </tr>
                <tr>
                   <td colspan="2" bgcolor="#F4F2F3" height="20px" />
                </tr>
                <tr>
                    <td colspan="2">
                        <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"  style="margin-bottom:20px;padding:10px;">
                            <tr>
                                <td>Hi, '.ucfirst($firstname).' '.ucfirst($lastname).'</td>
                            </tr>
                            <tr height="20px">&nbsp;</tr>
                            <tr>
                                <td>YOUR ACCOUNT CREDENTIALS ARE AS BELOW: </td>
                            </tr>
                            <tr>
                                <td>Username: <strong>'.$uid.'</strong> <br/>
                                    Password: <strong>'.$password.'</strong>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr bgcolor="#F4F2F3">
                   <td colspan="2">
                      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="border-radius:15px;background-color:#CCCCCC;">
                         <tr height="25px;" style="font-weight:bold; font-size:12px;">
                            <td style="padding-left:20px; font-size:20px; color:#000" height="50px; font-family: Open Sans, sans-serif;">Terms and Conditions </td>
                         </tr>
                         <tr>
                            <td style="font-family: Open Sans, sans-serif; font-size:12px;color:#7A7A7A;font-weight:500; border-radius:15px;">
                               <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: Open Sans, sans-serif; font-size:12px;border-bottom-right-radius:15px;border-bottom-left-radius:15px;background-color:#FFF; padding:0px 5px">
                                  <tr>
                                     <td>
                                        <!-- start Terms and conditions!-->
                                        <ol style="line-height:25px;">
                                           <br />
                                           <li>Payment to be made via online mode. Pl visit ACT portal for to make payment.</li>
                                           <li>ACT reserves the right to disconnect the service, without any further notice, If not paid by applicable due date an amount of Rs.500 will be levied for re-connection.</li>
                                           <li>Service Tax No: AACCA8907BST001,TIN 07747118211:,PAN:AACCA8907B.</li>
                                           <li>Service Tax as prescribed by Govt. Of India from time to time shall apply.</li>
                                           <li>All disputes are subject to Delhi jurisdiction only.</li>
                                           <b><u>Additional Terms</u></b>
                                           <li>ACT Shall levy late fee charge in case the bill is paid after the due date.</li>
                                           <li>Collection Pickup charges will be Rs. 100/-.</li>
                                           <li>Any disagreement in the invoice should be informed within 15 days from the date of this invoice, failing which all charges will be considered as valid.</li>
                                           <li>For any queries on the invoice please call 01142340000 or write at helpdesk.ncr@actcorp.in.</li>
                                           <li> This is electronically generated bill and does not require signature.</li>
                                        </ol>
                                        <!-- End Terms and conditions!-->
                                     </td>
                                  </tr>
                               </table>
                            </td>
                         </tr>
                      </table>
                   </td>
                </tr>
                <tr bgcolor="#F4F2F3">
                   <td colspan="2" height="20px;" />
                </tr>
                <tr bgcolor="#F4F2F3">
                   <td colspan="2" height="20px;" />
                </tr>
             </table>
          </td>
       </tr>
       <tr bgcolor="#F4F2F3">
          <td height="10px;" />
       </tr>
    </table>';
    
    echo $message;
?>