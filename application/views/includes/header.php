<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title><?php echo $this->session->userdata['isp_session']['isp_name'] ?></title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css?version=1.22" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-material-datetimepicker.css" />
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
       <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/misc.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/ckeckbox.js"></script>
     
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/crbnMenu.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plan_misc.js?version=1.10"></script>
        
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/moment-with-locales.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap-material-datetimepicker.js"></script>
      
   </head>
   <body>