<?php $this->load->view('includes/header');?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/image_crop/demo.css" />
    <div class="container-fluid">
        <div class="row">
            <div class="wapper">
                <div id="sidedrawer" class="mui--no-user-select">
                    <div id="sidedrawer-brand" class="mui--appbar-line-height">
                        <span class="mui--text-title">
                           <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                     <img src="<?php echo $img ?>" class="img-responsive" />
                        </span>
                    </div>
                   <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
                 $this->view('left_nav',$data); ?>
                </div>
                <header id="header">
                   <nav class="navbar navbar-default">
                      <div class="container-fluid">
                         <div class="navbar-header">
                            <a class="navbar-brand" href="#">Banner Promotions</a>
                         </div>
                         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                              <?php $this->view('includes/global_setting',$data);  ?>
                            </ul>
                         </div>
                      </div>
                      
                   </nav>
                </header>
                <div id="content-wrapper">
                    <div class="mui--appbar-height"></div>
                    <div class="mui--appbar-height"></div>
                    <div class="mui-container-fluid" id="right-container-fluid">
                        <div class="add_user" style="padding-top:0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="mui-tabs__pane mui--is-active" id="Top-Up-1">
                                                <form method="post" action="<?php echo base_url()?>promotion/add_home_banner" enctype="multipart/form-data">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                                            <div class="row">
                                                                <div class="row">
                                                                    <!--label class="radio-inline">
                                                                        <input type="radio" name="banner_number"  value="1" checked> Banner 1
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" name="banner_number"  value="2"> Banner 2
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" name="banner_number"  value="3">  Banner 3
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" name="banner_number" value="4">  Banner 4
                                                                    </label-->
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                                        <div class="mui-select">
                                                                            <select name="banner_number" <?php echo (isset($ro))?"disabled":"";?> id="banner_number">
                                                                               <option value ="1">Banner 1</option>
                                                                              <option value ="2">Banner 2</option>
                                                                              <option value ="3">Banner 3</option>
                                                                              <option value ="4">Banner 4</option>
                                                                            </select>
                                                                            <label>Select Banner<sup>*</sup></label>
                                                                        </div>
                                                                     </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mui--appbar-height"></div>
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                            <div class="mui-select">
                                                                <select name="plan" <?php echo (isset($ro))?"disabled":"";?> id="plan">
                                                                   <option value ="">Select</option>
                                                                   <?php
                                                                   foreach($service_list as $plan_list1)
                                                                   
                                                                   if($plan_list1['plantype'] != '0'){
                                                                     echo '<option value="'.$plan_list1['srvid'].'">'.$plan_list1['srvname'].'</option>';
                                                                   }
                                                                   ?>
                                                                </select>
                                                                <label>Select Plan<sup></sup></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                                            <center>
                                                                <h5>OR</h5>
                                                            </center>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                            <div class="mui-select">
                                                                <select name="topup" <?php echo (isset($ro))?"disabled":"";?> id="topup">
                                                                    <option value ="">Select</option>
                                                                    <?php
                                                                    foreach($service_list as $plan_list1)
                                                                    
                                                                    if($plan_list1['plantype'] == '0'){
                                                                      echo '<option value="'.$plan_list1['srvid'].'">'.$plan_list1['srvname'].'</option>';
                                                                    }
                                                                    ?>
                                                                 </select>
                                                                 <label>Select Plan<sup></sup></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                            <div class="mui-textfield mui-textfield--float-label">
								
                                                                    <textarea name="banner_description" <?php echo (isset($ro))?"disabled":"";?> id="banner_description"></textarea>
								<label>Description</label>
                                                                <span class="pull-right ">
							<span class="banner_description_add">0</span>/
							<span class="banner_description_dec">150</span>
						    </span>
							    </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="padding-left:0px;">
                                                            <!--input type="file" id="exampleInputFile" name="banner_image" class="hide"/>
                                                                <span id="but" <?php echo (isset($ro))?"disabled":"";?> class="mui-btn mui-btn--small mui-btn--primary" style="height:35px; padding: 0px 20px; background-color:#36465f; font-weight:600;    line-height: 34.6px;">
                                                                Upload Image
                                                                </span>
                                                                <span style="color: red" id="provider_logo_error"></span-->
                                                                 <div class="dropzone" data-ajax="false" data-resize="true" data-originalsize="false" data-width="1200" data-height="300" style="width: 400px; height:100px;">
                                                                    <input type="file" name="thumb"  />
                                                                </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                            <div class="images-preview">
								<img id="preview1" src="" style="display: none" />
							    </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <input type="submit" <?php echo (isset($ro))?"disabled":"";?> class="mui-btn mui-btn--accent" style="height:35px; padding: 0px 20px; line-height: 35px; font-weight:500" value="UPDATE">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="table-responsive">
                                                <table class="table table-striped">
                                                   <thead>
                                                      <tr class="active">
                                                         
                                                         <th>BANNER NUMBER</th>
                                                         <th>PLAN NAME / TOP UP </th>
                                                         <th>IMAGE</th>
                                                         <th>DESCRIPTION</th>
                                                         
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                    <?php
                                                    
                                                    foreach($banner_list as $banner_list1){
                                                        echo '<tr>
                                                        <td>Banner '.$banner_list1['banner_number'].'</td>
                                                         
                                                         <td>'.$banner_list1['plan_name'].'</td>
                                                         <td>
                                                            <a href="#" onclick="view_image(\''.$banner_list1["image_path"].'\')">
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
							    </a>
                                                         </td>
                                                         <td>'.$banner_list1['description'].'</td>
                                                         
                                                      </tr>';
                                                    
                                                    }
                                                    ?>
                                                      
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="mui-tabs__pane" id="Top-Up-2">Pane-2</div>
                                    <div class="mui-tabs__pane" id="Top-Up-3">Pane-3</div>
                                    <div class="mui-tabs__pane" id="Top-Up-4">Pane-4</div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    
    
    <!--ADD BILLING TRANSACTION Modal -->
        <div class="modal fade" tabindex="-1" role="dialog" id="image_view" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">
                     <strong>PREVIEW IMAGE</strong>
                  </h4>
               </div>
               <div class="modal-body">
				   <div class="row">
					   <div class="col-lg-12n col-md-12 col-sm-12">
					   	<div class="modal-body_img">
						<img src="" id="image_banner_show"/>
					        </div>
					   </div>
				   </div>
               </div>
            </div>
            <!-- /.modal-content -->
         </div>
      </div>
      <!--end ADD BILLING TRANSACTION Modal -->
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/image_crop/html5imageupload.js"></script>
     <script type="text/javascript">var base_url ="<?php echo base_url()?>";</script>
      <script>
        $('.dropzone').html5imageupload();
         $(document).ready(function() {
           var height = $(window).height();
            $('#main_div').css('height', height);
           $('#right-container-fluid').css('height', height);
         
         // on user type change change page(form)
         $('input[type=radio][name=user_type_radio]').change(function() {
         	if (this.value == 'lead' || this.value == 'enquiry') {
            		$("#user_type_lead_enquiry").show();
         $("#user_type_customer").hide();
         	}
         	else if (this.value == 'customer') {
            		$("#user_type_customer").show();
         $("#user_type_lead_enquiry").hide();
         	}
         }); 
         
         });
      </script>
      <script>
         $( '#but' ).click( function() {
            //$( '#exampleInputFile' ).trigger( 'click' );
         } );
         
        function view_image(image_path) {
        //code
            var image_path_show = image_path;
            $('#image_banner_show').attr('src', image_path_show);
            $("#image_view").modal('show');
            
        }
      </script>
      
      <script type="text/javascript">
        
         
         
        // on plan change disable or enable topup pannel
        $("#plan").change(function(){
            if ($(this).val() == '') {
                //enable
                $("#topup").prop("disabled", false);
            }else{
                //disable
                $("#topup").prop("disabled", true);
            }
        });
        // on topup change disable or enable plan pannel
        $("#topup").change(function(){
    
            if ($(this).val() == '') {
                //enable
                $("#plan").prop("disabled", false);
            }else{
                //disable
                $("#plan").prop("disabled", true);
            }
        });
        
        $("#banner_description").keyup(function() {
	    el = $(this);
	    if (el.val().length > 150) {
		el.val(el.val().substr(0, 150));
	    } else {
		$(".banner_description_add").text(el.val().length);
		$(".banner_description_dec").text(150 - el.val().length);
	    }
	});
        //on  image select show image
    (function() {
        var URL = window.URL || window.webkitURL;
        /*var input = document.querySelector('#exampleInputFile');
        var preview = document.querySelector('#preview1');
        // When the file input changes, create a object URL around the file.
        input.addEventListener('change', function () {
            $("#preview1").show();
            preview.src = URL.createObjectURL(this.files[0]);
        });*/

    })();
//check brand logo size
    var _URL = window.URL || window.webkitURL;
    /*$("#exampleInputFile").change(function(e) {
        var file, img;
        if ((file = this.files[0])) {
            img = new Image();
            img.onload = function() {
                var image_width = this.width;
                var image_height = this.height;
               
                    if(image_width !=  1200 || image_height != 300){
                        $("#provider_logo_error").html("Image size should be  of width 1200px and height 300px");
                        $('input[type="submit"]').attr('disabled','disabled');
                    }else{
                            $('input[type="submit"]').removeAttr('disabled');

                        $("#provider_logo_error").html("");

                    }
               

            };
            img.onerror = function() {
                $('input[type="submit"]').attr('disabled','disabled');
                alert( "not a valid file:");
            };
            img.src = _URL.createObjectURL(file);
        }
    });*/
    
    
      </script>
        <script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
         
         $(document).ready(function(){
//class="active" style="display: block;"
$("#promotion_menu_ul").css("display","block");
$("#home_menu").addClass("menu_active");

});
      </script>
   </body>
</html>