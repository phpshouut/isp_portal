<?php $this->load->view('includes/header');?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/image_crop/demo.css" />
    <div class="container-fluid">
	<div class="row">
	    <div class="wapper">
		<div id="sidedrawer" class="mui--no-user-select">
		    <div id="sidedrawer-brand" class="mui--appbar-line-height">
			<span class="mui--text-title">
			   <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                     <img src="<?php echo $img ?>" class="img-responsive" />
			</span>
		    </div>
		    <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
                 $this->view('left_nav',$data); ?>
		</div>
		<header id="header">
		   <nav class="navbar navbar-default">
		      <div class="container-fluid">
			 <div class="navbar-header">
			    <a class="navbar-brand" href="#">Marketing Promotions</a>
			 </div>
			 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                              <?php $this->view('includes/global_setting',$data);  ?>
                            </ul>
                         </div>
		      </div>
		      
		   </nav>
		</header>
		<div id="content-wrapper">
		    <div class="mui--appbar-height"></div>
		    <div class="mui--appbar-height"></div>
		    <div class="mui-container-fluid" id="right-container-fluid">
			<div class="add_user" style="padding-top:0px;">
			    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
				    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="row">
					    <div class="mui-tabs__pane mui--is-active" id="Top-Up-1">
						<form method="post" action="<?php echo base_url()?>promotion/add_marketing_promotion_banner" enctype="multipart/form-data" id="create_promotion">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						    <div class="row">
							
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
							    <div class="mui-textfield mui-textfield--float-label">
								<input type="text" <?php echo (isset($ro))?"disabled":"";?> name="title" id="title" required>
								<label>Title</label>
								<span class="pull-right ">
								    <span class="marketing_promotion_title_add">0</span>/
								    <span class="marketing_promotion_title_dec">40</span>
								</span>
							    </div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							    <div class="mui-textfield mui-textfield--float-label">
								<!--input type="text"  required-->
								    <textarea name="description" <?php echo (isset($ro))?"disabled":"";?> id="desctiopn" required></textarea>
								<label>Description</label>
								<span class="pull-right ">
								    <span class="marketing_promotion_description_add">0</span>/
								    <span class="marketing_promotion_description_dec">150</span>
								</span>
							    </div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
							    <div class="mui-textfield mui-textfield--float-label">
								<input type="text" <?php echo (isset($ro))?"disabled":"";?> name="promotion_offer" id="promotion_offer" required>
								<label>Promotion Offer</label>
								<span class="pull-right ">
								    <span class="marketing_promotion_offer_add">0</span>/
								    <span class="marketing_promotion_offer_dec">40</span>
								</span>
							    </div>
							</div>
						    </div>
						    <div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
							    <div class="mui-textfield mui-textfield--float-label">
								<input type="text" <?php echo (isset($ro))?"disabled":"";?> name="redirect_link" id="redirect_link" required>
								<label>Link for Redirection</label>
							    </div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
							    <div class="mui-textfield mui-textfield--float-label" style="padding-top:0px">
								<div class="input-group" style="margin-top:-5px">
								    <div class="input-group-addon" style="padding:6px 10px 6px 0px">
									 <i class="fa fa-calendar"></i>
								    </div>
								    <div class="mui-textfield mui-textfield--float-label">
									<input type="text" <?php echo (isset($ro))?"disabled":"";?> class="date-start" id="start_date" placeholder="Start Date*" name='start_date'>
								    </div>
								</div>
							    </div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							    <div class="mui-textfield mui-textfield--float-label" style="padding-top:0px">
								<div class="input-group" style="margin-top:-5px">
								    <div class="input-group-addon" style="padding:6px 10px 6px 0px">
								       <i class="fa fa-calendar"></i>
								    </div>
								    <div class="mui-textfield mui-textfield--float-label">
								       <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="date-end" id="end_date" name="end_date" placeholder="End Date*">
								    </div>
								</div>
							    </div>
							</div>
						    </div>
						    <div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
							    <!--input type="file" id="exampleInputFile" name="banner_image" class="hide"/>
							    <span id="but" <?php echo (isset($ro))?"disabled":"";?> class="mui-btn mui-btn--small mui-btn--primary" style="height:35px; padding: 0px 20px; background-color:#36465f; font-weight:600 ;line-height: 34.6px;">
							    Upload Image
							    </span>
							    <span style="color: red" id="provider_logo_error"></span-->
							    <div class="dropzone" data-ajax="false" data-resize="true" data-originalsize="false" data-width="1400" data-height="800" style="width: 298px; height:170px;">
								<input type="file" name="thumb"  />
                                                            </div>
							    
							    <input type="submit" <?php echo (isset($ro))?"disabled":"";?> class="mui-btn mui-btn--accent" value="Create Promotion">
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
							    <div class="images-preview">
								<img id="preview1" src="" style="display: none" />
							    </div>
							</div>
							
						    </div>
						   
						  
						</div>
						</form>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
						    <div class="row">
							<div class="table-responsive">
							    <table class="table table-striped">
								<thead>
								   <tr class="active">
								      <th>&nbsp;</th>
								      <th>TITLE</th>
								      <th>DESCRIPTION</th>
								      <th>OFFER</th>
								      <th>IMAGE</th>
								      <th>START DATE</th>
								      <th>END DATE</th>
								      <th>STATUS</th>
								      <th colspan="2">
									<center>ACTION</center>
								      </th>
								   </tr>
								</thead>
								<tbody>
								    <?php
								    $i = 1;
                                                                       $is_deletable = $this->plan_model->is_permission(PROMOTION, 'DELETE');
								    foreach($marketing_promotion_list as $data){
									$expire_class = 'Closed';
									$expire_msg = 'Live';
									if($data['is_expired'] == 1){
									    $expire_class = 'open';
									    $expire_msg = 'Expired';
									}
									?>
									<td><strong><?php echo $i?></strong></td>
									<td><?php echo $data['title'];?></td>
									<td><?php echo $data['description'];?></td>
									<td><?php echo $data['promotion_offer'];?></td>
									<td>
									    <a href="#" onclick="view_image('<?php echo $data['image_path'];?>')">
										<i class="fa fa-eye" aria-hidden="true"></i>
									    </a>
									</td>
									<td><?php echo $data['start_date'];?></td>
									<td><?php echo $data['end_date'];?></td>
									<td><span class="<?php echo $expire_class?>"><?php echo $expire_msg?> </span></td>
									<td>
									    <a href="#" onclick="edit_promotion('<?php echo $data['id'];?>')">Edit</a>
									</td>
									<td>
                                                                           <?php if($is_deletable){?>
									    <a href="<?php echo base_url()?>promotion/delete_marketing_promotion?id=<?php echo $data['id']?>" onclick="return confirmDialog();">Delete</a>
									    <script>
										function confirmDialog() {
										    return confirm("Are you sure you want to delete this record?")
										}
									    </script>
                                                                           <?php }
                                                                           else{?>
                                                                              <a href="#" >Delete</a>
                                                                           <?php } ?>
									</td>
								    </tr>
									<?php
									$i++;
								    }
								    ?>
								   
							    
                                                      
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /Start your project here-->
      
       <!--ADD BILLING TRANSACTION Modal -->
          <div class="modal fade" tabindex="-1" role="dialog" id="image_view" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">
                     <strong>PREVIEW IMAGE</strong>
                  </h4>
               </div>
               <div class="modal-body">
				   <div class="row">
					   <div class="col-lg-12n col-md-12 col-sm-12">
					   	<div class="modal-body_img">
						<img src="" id="image_banner_show"/>
					        </div>
					   </div>
				   </div>
               </div>
            </div>
            <!-- /.modal-content -->
         </div>
      </div>
      <!--end ADD BILLING TRANSACTION Modal -->
      
      
      
      
       <!--ADD BILLING TRANSACTION Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="edit_view" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
		<div class="modal-header" style="padding:10px 15px 0px 15px">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    <h4 class="modal-title">
		       <strong>Edit Promotion</strong>
		    </h4>
		</div>
		<div class="modal-body" style="padding:0px 15px 15px 15px">
		    <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
			    <form method="post" action="<?php echo base_url()?>promotion/update_marketing_promotion_banner" enctype="multipart/form-data" id="update_promotion">
				<div class="form-group">
				    <div class="col-lg-12 col-md-12 col-sm-12">
					<div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<input type="hidden" name="promotion_id" id="promotion_id_edit">
						<div class="mui-textfield ">
						    <input type="text" <?php echo (isset($ro))?"disabled":"";?> name="title" id="title_edit">
						    <label>Title</label>
						    <span class="pull-right ">
							<span class="marketing_promotion_title_edit_add">0</span>/
							<span class="marketing_promotion_title_edit_dec">40</span>
						    </span>
						</div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<div class="mui-textfield ">
						    
							<textarea name="description" <?php echo (isset($ro))?"disabled":"";?> id="desctiption_edit"></textarea>
						    <label>Description</label>
						    <span class="pull-right ">
							<span class="marketing_promotion_title_desctiption_add">0</span>/
							<span class="marketing_promotion_desctiption_edit_dec">150</span>
						    </span>
						</div>
                                            </div>
					    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <div class="mui-textfield ">
								<input type="text" <?php echo (isset($ro))?"disabled":"";?> name="promotion_offer_edit" id="promotion_offer_edit" required>
								<label>Promotion Offer</label>
								<span class="pull-right ">
								    <span class="marketing_promotion_offer_edit_add">0</span>/
								    <span class="marketing_promotion_offer_edit_dec">40</span>
								</span>
							    </div>
					    </div>
					   
                                        </div>
				    </div>
				</div>
				<div class="form-group">
				    <div class="col-lg-12 col-md-12 col-sm-12">
					<div class="row">
					     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" >
						<div class="mui-textfield ">
						    <input type="text" <?php echo (isset($ro))?"disabled":"";?> name="redirect_link" id="redirect_link_edit">
						    <label>Link for Redirection</label>
						</div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-top:0px">
                                                    <div class="input-group" style="margin-top:-5px">
							<div class="input-group-addon" style="padding:6px 10px 6px 0px">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <div class="mui-textfield ">
                                                            <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="date-start" id="start_date_edit" name="start_date" placeholder="Start Date*">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-top:0px">
                                                    <div class="input-group" style="margin-top:-5px">
                                                        <div class="input-group-addon" style="padding:6px 10px 6px 0px">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <div class="mui-textfield ">
                                                            <input type="text"<?php echo (isset($ro))?"disabled":"";?>  class="date-end" id="end_date_edit" name="end_date" placeholder="End Date*">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
				    </div>
				</div>
				<div class="form-group">
				    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <!--input type="file" id="exampleInputFile_edit" name="banner_image" class="hide"/>
                                            <span id="but_edit" <?php echo (isset($ro))?"disabled":"";?> class="mui-btn mui-btn--small mui-btn--primary" style="height:35px; padding: 0px 20px; background-color:#36465f; font-weight:600; line-height:35.6px ">
                                                      Upload Image
                                            </span>
					    
					    <br /><span style="color: red" id="provider_logo_error_edit"></span-->
					    <div class="dropzone" data-ajax="false" data-resize="true" data-originalsize="false" data-width="1400" data-height="800" style="width: 298px; height:170px;">
                                                                    <input type="file" name="thumb_edit"  />
                                                                </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <div class="modal-images-preview">
						<img id="preview1_edit" src="" style="display: none" />
					    </div>
                                        </div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <div class="modal-images-preview">
						<img id="preview1_edit_old" src="" />
					    </div>
                                        </div>
                                    </div>
				</div>
				<div class="form-group">
				    <div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
					    <input type="submit" <?php echo (isset($ro))?"disabled":"";?> class="mui-btn mui-btn--accent" value="Update Promotion">
					</div>
				    </div>
				</div>
			    </form> 
			</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
      <!--end ADD BILLING TRANSACTION Modal -->
     <script type="text/javascript" src="<?php echo base_url()?>assets/js/image_crop/html5imageupload.js"></script>
      <script>
	$('.dropzone').html5imageupload();
         $(document).ready(function() {
           var height = $(window).height();
            $('#main_div').css('height', height);
           $('#right-container-fluid').css('height', height);
         
         // on user type change change page(form)
         $('input[type=radio][name=user_type_radio]').change(function() {
         	if (this.value == 'lead' || this.value == 'enquiry') {
            		$("#user_type_lead_enquiry").show();
         $("#user_type_customer").hide();
         	}
         	else if (this.value == 'customer') {
            		$("#user_type_customer").show();
         $("#user_type_lead_enquiry").hide();
         	}
         }); 
         
         });
      </script>
      <script>
	// on button click open image choose box
         $( '#but' ).click( function() {
            //$( '#exampleInputFile' ).trigger( 'click' );
         } );
	  $( '#but_edit' ).click( function() {
            //$( '#exampleInputFile_edit' ).trigger( 'click' );
         } );
	  // create promotion time check validation
	 $("#create_promotion").submit(function(){
	    var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
	    var match_url =  urlregex.test($("#redirect_link").val());
	   
	    
	    if ($("#promotion_offer").val() == '' || $("#title").val() == '' || $("#desctiopn").val() == '' || $("#redirect_link").val() == '' || $("#start_date").val() == '' || $("#end_date").val() == '') {
		$("#provider_logo_error").html("Please fill all the field");
		
		return false;
	    }else{
		 var match_url =  urlregex.test($("#redirect_link").val());
		 if (match_url == true) {
		    return true;
		}else{
		    $("#provider_logo_error").html("Invalid redirect url");
		     return false;
		}
		   
	    }
	   
	});
	 // update promotion time check validation
	 $("#update_promotion").submit(function(){
	    
	    if ($("#promotion_offer_edit").val() == '' || $("#title_edit").val() == '' || $("#desctiopn_edit").val() == '' || $("#redirect_link_edit").val() == '' || $("#start_date_edit").val() == '' || $("#end_date_edit").val() == '' ) {
		$("#provider_logo_error_edit").html("Please fill all the field");
		return false;
	    }else{
		 var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
		 var match_url =  urlregex.test($("#redirect_link_edit").val());
		 
		 if (match_url == true) {
		    return true;
		}else{
		    $("#provider_logo_error_edit").html("Invalid redirect url");
		     return false;
		}
		   
	    }
	   
	});
	 // on view image icon click show image on popup
	 function view_image(image_path) {
        //code
            var image_path_show = image_path;
	    $('#image_banner_show').attr('src', image_path_show);
            $("#image_view").modal('show');
            
        }
	// on edit promotion click show data on popup
	function edit_promotion(promotion_id) {
	    $("#promotion_id_edit").val(promotion_id);
	   $.ajax({
		    type: "POST",
		    url: "<?php echo base_url()?>promotion/edit_marketing_promotion_view",
		    async: false,
		    cache: false,
		    dataType: 'json',
		    data: "promotion_id="+promotion_id,
		    success: function(data){
			$("#title_edit").val(data.title);
			$("#desctiption_edit").val(data.description);
			$("#promotion_offer_edit").val(data.promotion_offer);
			$("#redirect_link_edit").val(data.redirect_link);
			$("#start_date_edit").val(data.start_date);
			$("#end_date_edit").val(data.end_date);
			$('#preview1_edit_old').attr('src', data.image_path);
			$("#edit_view").modal("show");
		    }
	       });
	    
	}
	// input text limit set
	$("#title").keyup(function() {
	    el = $(this);
	    if (el.val().length > 40) {
		el.val(el.val().substr(0, 40));
	    } else {
		$(".marketing_promotion_title_add").text(el.val().length);
		$(".marketing_promotion_title_dec").text(40 - el.val().length);
	    }
	});
	$("#title_edit").keyup(function() {
	    el = $(this);
	    if (el.val().length > 40) {
		el.val(el.val().substr(0, 40));
	    } else {
		$(".marketing_promotion_title_edit_add").text(el.val().length);
		$(".marketing_promotion_title_edit_dec").text(40 - el.val().length);
	    }
	});
	$("#promotion_offer").keyup(function() {
	    el = $(this);
	    if (el.val().length > 40) {
		el.val(el.val().substr(0, 40));
	    } else {
		$(".marketing_promotion_offer_add").text(el.val().length);
		$(".marketing_promotion_offer_dec").text(40 - el.val().length);
	    }
	});
	$("#promotion_offer_edit").keyup(function() {
	    el = $(this);
	    if (el.val().length > 40) {
		el.val(el.val().substr(0, 40));
	    } else {
		$(".marketing_promotion_offer_edit_add").text(el.val().length);
		$(".marketing_promotion_offer_edit_dec").text(40 - el.val().length);
	    }
	});
	$("#desctiopn").keyup(function() {
	    el = $(this);
	    if (el.val().length > 150) {
		el.val(el.val().substr(0, 150));
	    } else {
		$(".marketing_promotion_description_add").text(el.val().length);
		$(".marketing_promotion_descripton_dec").text(150 - el.val().length);
	    }
	});
	$("#desctiption_edit").keyup(function() {
	    el = $(this);
	    if (el.val().length > 150) {
		el.val(el.val().substr(0, 150));
	    } else {
		$(".marketing_promotion_title_desctiption_add").text(el.val().length);
		$(".marketing_promotion_desctiption_edit_dec").text(150 - el.val().length);
	    }
	});
      </script>
    
    
      <script type="text/javascript">
         $(document).ready(function(){
            $('.date-start').bootstrapMaterialDatePicker
	    ({
		
		clearButton: true,
		weekStart: 0,
		format: 'DD.MM.YYYY H:m:s',
		minDate: moment()
	    }).on('change', function(e, date)
	    {
		$('.date-end').bootstrapMaterialDatePicker('setMinDate', date);
	    });
            
	    $('.date-end').bootstrapMaterialDatePicker
	    ({
		
		clearButton: true,
		weekStart: 0,
		format: 'DD.MM.YYYY H:m:s',
		minDate: moment()
	    }).on('change', function(e, date)
	    {
		$('.date-start').bootstrapMaterialDatePicker('setMaxDate', date);
	    });
            
            $.material.init()
         });
	 
	 
	   //on  image select show image
    (function() {
        var URL = window.URL || window.webkitURL;
        /*var input = document.querySelector('#exampleInputFile');
        var preview = document.querySelector('#preview1');
        input.addEventListener('change', function () {
	    $("#preview1").show();
            preview.src = URL.createObjectURL(this.files[0]);
        });*/

	
	 /*var input1 = document.querySelector('#exampleInputFile_edit');
        var preview1 = document.querySelector('#preview1_edit');
        // When the file input changes, create a object URL around the file.
        input1.addEventListener('change', function () {
	    $("#preview1_edit").show();
            preview1.src = URL.createObjectURL(this.files[0]);
        });*/
    })();
//check brand logo size
    var _URL = window.URL || window.webkitURL;
    /*$("#exampleInputFile").change(function(e) {
        var file, img;
        if ((file = this.files[0])) {
            img = new Image();
            img.onload = function() {
                var image_width = this.width;
                var image_height = this.height;
              
                    if(image_width !=  1400 || image_height != 800){
                        $("#provider_logo_error").html("Image size should be  of width 1400px and height 800px");
                        $('input[type="submit"]').attr('disabled','disabled');
                    }else{
                            $('input[type="submit"]').removeAttr('disabled');

                        $("#provider_logo_error").html("");

                    }
               

            };
            img.onerror = function() {
                $('input[type="submit"]').attr('disabled','disabled');
                alert( "not a valid file:");
            };
            img.src = _URL.createObjectURL(file);
        }
    });
    $("#exampleInputFile_edit").change(function(e) {
        var file, img;
        if ((file = this.files[0])) {
            img = new Image();
            img.onload = function() {
                var image_width = this.width;
                var image_height = this.height;
              
                    if(image_width !=  1400 || image_height != 800){
                        $("#provider_logo_error_edit").html("Image size should be  of width 1400px and height 800px");
                        $('input[type="submit"]').attr('disabled','disabled');
                    }else{
                            $('input[type="submit"]').removeAttr('disabled');

                        $("#provider_logo_error_edit").html("");

                    }
               

            };
            img.onerror = function() {
                $('input[type="submit"]').attr('disabled','disabled');
                alert( "not a valid file:");
            };
            img.src = _URL.createObjectURL(file);
        }
    });*/
    
    
      </script>
        <script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
         
                $(document).ready(function(){
//class="active" style="display: block;"
$("#promotion_menu_ul").css("display","block");
$("#promotion_menu").addClass("menu_active");

});
      </script>
   </body>
</html>