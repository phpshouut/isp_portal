 <!-- Start your project here-->
 <?php $this->load->view('includes/header');?>
      <div class="loading" style="display:none">
	   <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                      <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                     <img src="<?php echo $img ?>" class="img-responsive" />
                     </span>
                  </div>
                <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
                 $this->view('left_nav',$data); ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">Team</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
                               <?php if($this->plan_model->is_permission(DEPARTMENT,'ADD')){ ?> 
                             <li>
                                 <a href="<?php echo base_url()?>mgmt/add_dept">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD DEPARTMENT
                                 </button>
                                 </a>
                             </li>
                               <?php } ?>
                                <?php if($this->plan_model->is_permission(TEAM,'ADD')){ ?> 
                             <li>
                                 <a href="<?php echo base_url()?>team/add_team">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD TEAM
                                 </button>
                                 </a>
                            </li>
                             <?php }
			     $this->load->view('includes/global_setting',$data);
			     ?>
                              
                              
                               
                           <!--<li>
                                 <a href="#">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD LOCATION
                                 </button>
                                 </a>
                              </li>-->
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
                <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                       <div class="right_side" style="height:auto; padding-bottom: 0px;">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              
                           </div>
                         
						   </div>
					  </div>
                     <div class="add_user" style="padding-top:0px;">
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                               <!-- <ul class="plan_mui-tabs__bar">
								  <li class="plan_mui--is-active">
								  <a data-mui-toggle="tab" data-mui-controls="pane-default-1" >PPPoE Plans</a>
								  </li>
								  <li>
								  <a data-mui-toggle="tab" data-mui-controls="pane-default-2">
								  Leased Plans</a>
								  </li>
								</ul>-->
                              </div>
                           </div>
                                
                            
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                             <div class="row">
                              <div class="mui--appbar-height"></div>
                          		<div class="mui-tabs__pane mui--is-active" id="pane-default-1">
							      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                      <div class="row">
                                       <div class="table-responsive">
                                          <table class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>NAME</th>
                                                   <th>EMAIL</th>
                                                   <th>PHONE NO.</th>
                                                 
                                                  
                                                  
                                                   <th  class="mui--text-right" colspan="2">ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody id='search_gridview'>
                                                
                                                 <?php 
                                                 if(count($teamlist)>0){
                                                  $i=1;
                                                //  $is_editable=$this->plan_model->is_permission(TEAM,'EDIT');
                                                    $is_deletable=$this->plan_model->is_permission(TEAM,'DELETE');
                                                 foreach($teamlist as $valdata){
                                                  

                                                     
                                                     ?>
                                               <tr>
												   <td><?php echo $i;?>.</td>
                                                                                                   <td><?php echo $valdata->name;?></td>
												   <td><?php echo $valdata->email;?></td>
                                                                                                   <td><?php echo $valdata->phone;?></td>
                                                                                                   <td> <a href="<?php echo base_url()."team/edit_team/".$valdata->id?>">Edit</a></td> 
                                                                                                   <td><?php if($is_deletable){?><a href="javascript:void(0);" class="deletuser"  rel="<?php echo $valdata->id;?>" >Delete</a><?php } ?></td> 
												   
                                                </tr>
                                                 <?php
                                                 
                                                 $i++;
                                                 } 
                                                 }
                                                 else
                                                 {?>
                                                <tr><td colspan="4">No Team Member Added</td></tr>
                                                 <?php 
                                                 
                                                 }
                                                     
                                                 ?>
                                               
                                             </tbody>
                                          </table>
                                       </div>
									  </div>
                                    </div>
							    </div>
							<div class="mui-tabs__pane" id="pane-default-2">Pane-2</div>
							   </div>
                           </div>
                        </div>
					   </div> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
 
 
  <div class="modal fade" id="Deleteuser_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
          <input type="hidden" id="deluserid" value="">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>Are you sure you want to Delete User ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyes" >YES</button>
               </div>
            </div>
         </div>
      </div>

 
 <script type="text/javascript">
     
     $(document).ready(function(){
     
      var height = $(window).height();
  //    alert(height);
            $('#main_div').css('height', height);
	    $('#right-container-fluid').css('height', height);
	    
	    setTimeout(function (){
	       $('.loading').css('display', 'none');
	    },1000);
    
     $(document).on('click','.deletuser',function(){
             var deluserid=$(this).attr('rel');
             $('#deluserid').val(deluserid);
             $('#Deleteuser_confirmation_alert').modal('show');
             
             
         });
         
         $(document).on('click','.delyes',function(){
           
           var userid=$('#deluserid').val();
             $.ajax({
        url: base_url+'team/delete_user',
        type: 'POST',
        dataType: 'json',
        data: {userid:userid},
        success: function(data){
         
            $('#Deleteuser_confirmation_alert').modal('hide');
      
        location.reload();
           
        
        }
    });
             
         });
     
 });
</script>
 <script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
         //setting_menu_ul
$(document).ready(function(){
//class="active" style="display: block;"
$("#setting_menu_ul").css("display","block");
$("#team_menu").addClass("menu_active");

});
      </script>
 <?php $this->load->view('includes/footer');?>