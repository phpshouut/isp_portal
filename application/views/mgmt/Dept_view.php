 <!-- Start your project here-->
 <?php $this->load->view('includes/header');?>
      <div class="loading" style="display:none">
	   <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                       <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                     <img src="<?php echo $img ?>" class="img-responsive" />
                     </span>
                  </div>
                  <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
                 $this->view('left_nav',$data); ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">Teams</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
			        <?php $sessiondata = $this->session->userdata('isp_session');
				$classm="";
				if($sessiondata['super_admin']==1)
				{
				   if($sessiondata['is_franchise']==0){
				?>
			       <li>
                                 <a href="<?php echo base_url()?>mgmt/add_franchise">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD FRANCHISE
                                 </button>
                                 </a>
                            </li>
			       <?php
				   
			       }
			       else{
				      $classm="mui--is-active"; 
				   }
				}
			       
			       else{
				   $classm="mui--is-active";
			       }?>
                               <?php if($this->plan_model->is_permission(DEPARTMENT,'ADD')){ ?> 
                             <li>
                                 <a href="<?php echo base_url()?>mgmt/add_dept">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD DEPARTMENT
                                 </button>
                                 </a>
                             </li>
                               <?php } ?>
                                <?php if($this->plan_model->is_permission(DEPARTMENT,'ADD')){ ?> 
                             <li>
                                 <a href="<?php echo base_url()?>team/add_team">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD TEAM
                                 </button>
                                 </a>
                            </li>
                             <?php }
			     $this->load->view('includes/global_setting',$data);
			     ?>
			     
                              
                               
                           <!--<li>
                                 <a href="#">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD LOCATION
                                 </button>
                                 </a>
                              </li>-->
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
                <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                       <div class="right_side" style="height:auto; padding-bottom: 0px;">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              
                           </div>
                         
						   </div>
					  </div>
                         <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                <ul class="mui-tabs__bar plan_mui-tabs__bar">
				   
				<?php 	if($sessiondata['super_admin']==1)
				{
				   if($sessiondata['is_franchise']==0){
				?>
				<li class="mui--is-active">
						      <a data-mui-toggle="tab" data-mui-controls="Top-Up-3">
							  Franchise
							  </a>
								  </li>
				   <?php }
				} ?>
								  <li class="<?php echo $classm;?>">
						      <a data-mui-toggle="tab" data-mui-controls="Top-Up-1">
							  Department
							  </a>
								  </li>
								  <li>
								  <a data-mui-toggle="tab" data-mui-controls="Top-Up-2">
								  Team
							      </a>
								  </li>
								  
								</ul>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                             <div class="row">
                              <div class="mui--appbar-height"></div>
			      <?php 	if($sessiondata['super_admin']==1)
				{
				   if($sessiondata['is_franchise']==0){
				?>
			      <div class="mui-tabs__pane mui--is-active" id="Top-Up-3">
							    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                      <div class="row">
                                       <div class="table-responsive">
                                          <table class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>Franchise Name</th>
                                                   <th>Franchise Email</th>
						   <th>Franchise Phone</th>
                                                 
                                                  
                                                  
                                                   <th class="mui--text-center" colspan="2">ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody id='search_gridview'>
                                                
                                                 <?php 
                                                 if(count($franchlist)>0){
                                                  $i=1;
                                                  
                                                    $is_deletable=$this->plan_model->is_permission(DEPARTMENT,'DELETE');
                                                 foreach($franchlist as $valdata){
                                                  

                                                     
                                                     ?>
                                               <tr>
												   <td><?php echo $i;?>.</td>
                                                                                                   <td><?php echo $valdata->isp_name;?></td>
												   <td><?php echo $valdata->email;?></td>
												    <td><?php echo $valdata->phone;?></td>
                                                                                                   <td class="mui--text-center"><a href="<?php echo base_url()."mgmt/edit_franchise/".$valdata->id?>">Edit</a>&nbsp;&nbsp;&nbsp; 
                                                                                                 <?php if($is_deletable){?><a href="javascript:void(0);" class="deletfranchise"  rel="<?php echo $valdata->id;?>" >Delete</a><?php } ?></td> 
												   
                                                </tr>
                                                 <?php
                                                 
                                                 $i++;
                                                 } 
                                                 }
                                                 else
                                                 {?>
                                                <tr ><td colspan="5">No Franchise Added</td></tr>
                                                 <?php 
                                                 
                                                 }
                                                     
                                                 ?>
                                               
                                             </tbody>
                                          </table>
                                       </div>
									  </div>
                                    </div>
							    </div>
			       <?php }
				} ?>
                          		<div class="mui-tabs__pane <?php echo $classm;?>" id="Top-Up-1">
							    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                      <div class="row">
                                       <div class="table-responsive">
                                          <table class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>Department NAME</th>
                                                   <th>Team Member</th>
                                                 
                                                  
                                                  
                                                   <th class="mui--text-center" colspan="2">ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody id='search_gridview'>
                                                
                                                 <?php 
                                                 if(count($deptlist)>0){
                                                  $i=1;
                                                  
                                                    $is_deletable=$this->plan_model->is_permission(DEPARTMENT,'DELETE');
                                                 foreach($deptlist as $valdata){
                                                  

                                                     
                                                     ?>
                                               <tr>
												   <td><?php echo $i;?>.</td>
                                                                                                   <td><?php echo $valdata->dept_name;?></td>
												   <td><?php echo $valdata->team_count;?></td>
                                                                                                   <td class="mui--text-center"><a href="<?php echo base_url()."mgmt/edit_dept/".$valdata->id?>">Edit</a>&nbsp;&nbsp;&nbsp; 
                                                                                                 <?php if($is_deletable){?><a href="javascript:void(0);" class="deletdept"  rel="<?php echo $valdata->id;?>" >Delete</a><?php } ?></td> 
												   
                                                </tr>
                                                 <?php
                                                 
                                                 $i++;
                                                 } 
                                                 }
                                                 else
                                                 {?>
                                                <tr ><td colspan="4">No Department Added</td></tr>
                                                 <?php 
                                                 
                                                 }
                                                     
                                                 ?>
                                               
                                             </tbody>
                                          </table>
                                       </div>
									  </div>
                                    </div>
							    </div>
                              <div class="mui-tabs__pane" id="Top-Up-2">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                      <div class="row">
                                       <div class="table-responsive">
                                          <table class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>NAME</th>
                                                   <th>EMAIL</th>
                                                   <th>DEPARTMENT</th>
                                                   <th>PHONE NO.</th>
						   <th>Password.</th>
                                                 
                                                  
                                                  
                                                   <th  class="mui--text-center" colspan="3">ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody id='search_gridview'>
                                                
                                                 <?php 
                                                 if(count($teamlist)>0){
                                                  $i=1;
                                                //  $is_editable=$this->plan_model->is_permission(TEAM,'EDIT');
                                                    $is_deletable=$this->plan_model->is_permission(TEAM,'DELETE');
                                                 foreach($teamlist as $valdata){
                                                  

                                                     
                                                     ?>
                                               <tr>
												   <td><?php echo $i;?>.</td>
                                                                                                   <td><?php echo $valdata->name;?></td>
												   <td><?php echo $valdata->email;?></td>
                                                                                                   <td><?php echo $valdata->dept_name;?></td>
                                                                                                   <td><?php echo $valdata->phone;?></td>
												   <td class="pwdupd<?php echo $valdata->id;?>"><?php echo $valdata->orig_pwd;?></td>
												    <td class="mui--text-center"><a href="javascript:void(0);" class="changepwd"  rel="<?php echo $valdata->id;?>" >Change password</a>&nbsp;&nbsp;&nbsp;
                                                                                                   <a href="<?php echo base_url()."team/edit_team/".$valdata->id?>">Edit</a>&nbsp;&nbsp;&nbsp;  
                                                                                                   <?php if($is_deletable){?><a href="javascript:void(0);" class="deletuser"  rel="<?php echo $valdata->id;?>" >Delete</a><?php } ?></td> 
												   
                                                </tr>
                                                 <?php
                                                 
                                                 $i++;
                                                 } 
                                                 }
                                                 else
                                                 {?>
                                                <tr><td colspan="4">No Team Member Added</td></tr>
                                                 <?php 
                                                 
                                                 }
                                                     
                                                 ?>
                                               
                                             </tbody>
                                          </table>
                                       </div>
									  </div>
                                    </div>
                              </div>
							  
							   </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      
      <div class="modal fade" id="change_pwdconfirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
         
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p id="erromsg">Are you sure you want to Change password ?</p>
               </div>
	       <form action="" method="post" id="update_pwd" autocomplete="off"  onsubmit="update_pwd(); return false;">
		     <input type="hidden" id="teamid" name="teamid" value="">
			  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	       <div class="mui-textfield mui-textfield--float-label">
                        <input type="text" name="changepwd" class="city_text" required>
                        <label>New Password</label>
                    </div>
			  </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <input type="submit" class="mui-btn  mui-btn--small mui-btn--accent" value="Update">
	       </form>
               </div>
            </div>
         </div>
      </div>
 
 
  <div class="modal fade" id="Deletedept_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
          <input type="hidden" id="deldeptid" value="">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p id="erromsg">Are you sure you want to Delete Department ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyes" >YES</button>
               </div>
            </div>
         </div>
      </div>
 
 
 <div class="modal fade" id="Deleteuser_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
          <input type="hidden" id="deluserid" value="">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>Are you sure you want to Delete User ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyesuser" >YES</button>
               </div>
            </div>
         </div>
      </div>
 
 
 <div class="modal fade" id="Deletefranch_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
          <input type="hidden" id="delfranchid" value="">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>Are you sure you want to Delete Franchise ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyesfranch" >YES</button>
               </div>
            </div>
         </div>
      </div>

 
 <script type="text/javascript">
     
     $(document).ready(function(){
     
      var height = $(window).height();
  //    alert(height);
            $('#main_div').css('height', height);
	    $('#right-container-fluid').css('height', height);
	    
	    setTimeout(function (){
	       $('.loading').css('display', 'none');
	    },1000);
	    
	    $(document).on('click','.changepwd',function(){
	       var id=$(this).attr('rel');
	       $('#teamid').val(id);
	       $('#change_pwdconfirmation_alert').modal('show');
	       
	       });
	    
	    
    
     $(document).on('click','.deletdept',function(){
          $('#erromsg').html("Are you sure you want to Delete Department ?");
            $('.delyes').removeClass('hide');
             var deldeptid=$(this).attr('rel');
             $('#deldeptid').val(deldeptid);
           //  $('#Deletedept_confirmation_alert').modal('show');
              $('#deldeptid').val(deldeptid);
            $.ajax({
                url: base_url + 'mgmt/check_dept_deletable',
                type: 'POST',
                dataType: 'json',
                data: {deldeptid: deldeptid},
                success: function (data) {
                    if (data == 1)
                    {
                        $('#Deletedept_confirmation_alert').modal('show');
                    } else
                    {
                        $('#Deletedept_confirmation_alert').modal('show');
                        $('#erromsg').html("Department Can't be deleted already assigned to Team");
                        $('.delyes').addClass('hide');
                    }

                    // $('#Deletenas_confirmation_alert').modal('hide');

                    //  location.reload();


                }
            });
             
             
         });
         
         $(document).on('click','.delyes',function(){
           
           var deptid=$('#deldeptid').val();
             $.ajax({
        url: base_url+'mgmt/delete_dept',
        type: 'POST',
        dataType: 'json',
        data: {deptid:deptid},
        success: function(data){
         
            $('#Deletedept_confirmation_alert').modal('hide');
      
        location.reload();
           
        
        }
    });
         });
         
	 //franchise delete
	  $(document).on('click','.deletfranchise',function(){
          $('#erromsg').html("Are you sure you want to Delete Franchise ?");
            $('.delyesfranch').removeClass('hide');
             var delfranchid=$(this).attr('rel');
             $('#delfranchid').val(delfranchid);
             $('#Deletefranch_confirmation_alert').modal('show');
           
             
             
         });
         
         $(document).on('click','.delyesfranch',function(){
           
           var franchid=$('#delfranchid').val();
             $.ajax({
        url: base_url+'mgmt/delete_franchise',
        type: 'POST',
        dataType: 'json',
        data: {franchid:franchid},
        success: function(data){
         
            $('#Deletefranch_confirmation_alert').modal('hide');
      
        location.reload();
           
        
        }
    });
             
         });
	 
	 
           $(document).on('click','.deletuser',function(){
             var deluserid=$(this).attr('rel');
             $('#deluserid').val(deluserid);
             $('#Deleteuser_confirmation_alert').modal('show');
             
             
             
         });
         
         $(document).on('click','.delyesuser',function(){
           
           var userid=$('#deluserid').val();
             $.ajax({
        url: base_url+'team/delete_user',
        type: 'POST',
        dataType: 'json',
        data: {userid:userid},
        success: function(data){
         
          $('#Deleteuser_confirmation_alert').modal('hide');
	        location.reload();
         }
    });
             
         });
     
 });
</script>
 <script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
         //setting_menu_ul
$(document).ready(function(){
//class="active" style="display: block;"
$("#setting_menu_ul").css("display","block");
$("#team_menu").addClass("menu_active");

});

      function update_pwd()
     {
         $('.loading').removeClass('hide');
         var teamid = $('#teamid').val();
        var formdata = $("#update_pwd").serialize();
        
         $.ajax({
        url: base_url+'team/update_userpwd',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
          
	  if (data.resultcode==1) {
	       $('#change_pwdconfirmation_alert').modal('hide');
	       $('.pwdupd'+teamid).html(data.origpwd);
	  }
           
          
        }
    });
        
     }
      </script>
 <?php $this->load->view('includes/footer');?>