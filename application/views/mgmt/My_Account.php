 <!-- Start your project here-->
 <?php $this->load->view('includes/header');?>
      <div class="loading" style="display:none">
	   <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                      <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                     <img src="<?php echo $img ?>" class="img-responsive" />
                     </span>
                  </div>
                 <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
                 $this->view('left_nav',$data); ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">My Account</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
                             <li>
                                 <a href="<?php echo base_url()?>mgmt/add_dept">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD DEPARTMENT
                                 </button>
                                 </a>
                              </li>
                              <li>
                                 <a href="<?php echo base_url()?>mgmt/add_team">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD TEAM
                                 </button>
                                 </a>
                              </li>
                              <?php $this->load->view('includes/global_setting',$data);?>
                               
                           <!--<li>
                                 <a href="#">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD LOCATION
                                 </button>
                                 </a>
                              </li>-->
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
                <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                       <div class="right_side" style="height:auto; padding-bottom: 0px;">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              
                           </div>
                         
						   </div>
					  </div>
                     <div class="add_user" style="padding-top:0px;">
                          
                       <?php echo validation_errors(); ?>
                    
                          <form action="<?php echo base_url()?>team/edit_account_data" id="add_dept" method="post" autocomplete="off" onsubmit="//add_dept(); return false;">
                              <input type="hidden" name="user_id" value="<?php  echo (isset($userdata['id']))?$userdata['id']:set_value('id');?>">          
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br>
                                            <div class="row">
                                                <h2>My Account</h2>
                                                 <div class="form-group">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                       <div class="row">
                                                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="email" disabled  name="username" value="<?php  echo (isset($userdata['username']))?$userdata['username']:set_value('username');?>" required>
                                                                    <label>Email As Username<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                       </div>
                                                         <input type="hidden"  name="username" value="<?php echo $userdata['username'];?>">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="text" name="firstname" value="<?php echo (isset($userdata['name']))?$userdata['name']:set_value('firstname');?>" required>
                                                                    <label>First Name<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                            <!--<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="text">
                                                                    <label>Middle Name<sup>*</sup></label>
                                                                </div>
                                                            </div>-->

                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="text" name="lastname" value="<?php echo (isset($userdata['lastname']))?$userdata['lastname']:set_value('lastname');?>" >
                                                                    <label>Last Name<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                           <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="email"  name="email" value="<?php echo (isset($userdata['email']))?$userdata['email']:set_value('email');?>" required>
                                                                    <label>Email<sup>*</sup></label>
                                                                </div>
                                                            </div>-->
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="number" name="phone" value="<?php echo (isset($userdata['phone']))?$userdata['phone']:set_value('phone');?>" required>
                                                                    <label>Mobile Number <sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                           
                                                           <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="password" name="old_pwd" >
                                                                    <label>Old Password<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                           
                                                                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="password" name="newpwd">
                                                                    <label>New Password<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                           

                                                            
                                                        </div>

                                                      


                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:25px">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                 <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
 
 
 <div class="modal fade" id="Deletetopup_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
          <input type="hidden" id="deltopupid" value="">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p id="erromsg">Are you sure you want to Delete Top Up ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyes" >YES</button>
               </div>
            </div>
         </div>
      </div>

  <script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
         //setting_menu_ul
$(document).ready(function(){
//class="active" style="display: block;"
//$("#setting_menu_ul").css("display","block");
$("#myaccount_menu").addClass("menu_active");

});
      </script>
 
 <?php $this->load->view('includes/footer');?>
