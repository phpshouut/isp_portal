 <!-- Start your project here-->
 <?php $this->load->view('includes/header');?>
      <div class="loading" style="display:none">
	   <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                      <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                     <img src="<?php echo $img ?>" class="img-responsive" />
                     </span>
                  </div>
                 <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
                 $this->view('left_nav',$data); ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">Team</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
			      
			         <?php $sessiondata = $this->session->userdata('isp_session');
				
				if($sessiondata['super_admin']==1)
				{
				   if($sessiondata['is_franchise']==0){
				?>
			       <li>
                                 <a href="<?php echo base_url()?>mgmt/add_franchise">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD FRANCHISE
                                 </button>
                                 </a>
                            </li>
			       <?php
				 }
                                }?>
                            <?php if($this->plan_model->is_permission(DEPARTMENT,'ADD')){ ?> 
                             <li>
                                 <a href="<?php echo base_url()?>mgmt/add_dept">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD DEPARTMENT
                                 </button>
                                 </a>
                             </li>
                               <?php } ?>
                                <?php if($this->plan_model->is_permission(TEAM,'ADD')){ ?> 
                             <li>
                                 <a href="<?php echo base_url()?>team/add_team">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD TEAM
                                 </button>
                                 </a>
                            </li>
                             <?php }
			     $this->load->view('includes/global_setting',$data);
			     ?>
                              
                              
                               
                           <!--<li>
                                 <a href="#">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD LOCATION
                                 </button>
                                 </a>
                              </li>-->
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
                <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                       <div class="right_side" style="height:auto; padding-bottom: 0px;">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              
                           </div>
                         
						   </div>
					  </div>
                     <div class="add_user" style="padding-top:0px;">
                          
                       <?php echo validation_errors(); ?>
                    
                          <form action="<?php echo base_url()?>team/add_team_data" id="add_dept" method="post" autocomplete="off" onsubmit="//add_dept(); return false;">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br>
                                            <div class="row">
                                                <h2>Team</h2>
                                                 <div class="form-group">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                <div class="mui-select">
                                                                    <select name="dept" required>
                                                                       <?php
                                                                       foreach($dept as $val){
                                                                       ?>
                                                                        <option value="<?php echo $val->id;?>"><?php echo $val->dept_name; ?></option>
                                                                       <?php } ?>
                                                                    </select>
                                                                    <label>Select Department<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="email" name="username" value="<?php echo set_value('username');?>" required>
                                                                    <label>Enter Email as Username<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="text" name="firstname" value="<?php echo set_value('firstname');?>" required>
                                                                    <label>First Name<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                            <!--<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="text">
                                                                    <label>Middle Name<sup>*</sup></label>
                                                                </div>
                                                            </div>-->

                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="text" name="lastname" value="<?php echo set_value('lastname');?>" required>
                                                                    <label>Last Name<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <!--<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="email"  name="email" value="<?php echo set_value('email');?>" required>
                                                                    <label>Email<sup>*</sup></label>
                                                                </div>
                                                            </div>-->
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="number" name="phone" value="<?php echo set_value('phone');?>" required>
                                                                    <label>Mobile Number <sup>*</sup></label>
                                                                </div>
                                                            </div>

                                                           <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="password" name="password" value="<?php echo set_value('password');?>" required>
                                                                    <label>Password <sup>*</sup></label>
                                                                </div>-->
                                                            </div>
                                                        </div>

                                                      


                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:25px">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                 <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
 
 
 <div class="modal fade" id="Deletetopup_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
          <input type="hidden" id="deltopupid" value="">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p id="erromsg">Are you sure you want to Delete Top Up ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyes" >YES</button>
               </div>
            </div>
         </div>
      </div>

 
 <script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
         //setting_menu_ul
$(document).ready(function(){
//class="active" style="display: block;"
$("#setting_menu_ul").css("display","block");
$("#team_menu").addClass("menu_active");

});
      </script>
 <?php $this->load->view('includes/footer');?>
