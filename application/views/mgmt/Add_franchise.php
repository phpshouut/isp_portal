<!-- Start your project here-->
<?php $this->load->view('includes/header'); ?>
<div class="loading hide" >
    <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="wapper">
            <div id="sidedrawer" class="mui--no-user-select">
                <div id="sidedrawer-brand" class="mui--appbar-line-height">
                    <span class="mui--text-title">
                        <?php $img = (isset($ispdetail->logo_image) && $ispdetail->logo_image != '') ? base_url() . "ispmedia/logo/" . $ispdetail->logo_image : base_url() . "assets/images/decibel.png" ?>
                        <img src="<?php echo $img ?>" class="img-responsive" />
                    </span>
                </div>
                <?php
                $data['navperm'] = $this->plan_model->leftnav_permission();

                $this->view('left_nav', $data);
                ?>
            </div>
            <header id="header">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">Franchise</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                 <?php $sessiondata = $this->session->userdata('isp_session');
				if($sessiondata['super_admin']==1)
				{
				?>
			       <li>
                                 <a href="<?php echo base_url()?>mgmt/add_franchise">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD FRANCHISE
                                 </button>
                                 </a>
                            </li>
			       <?php } ?>
<?php if ($this->plan_model->is_permission(DEPARTMENT, 'ADD')) { ?> 
                                    <li>
                                        <a href="<?php echo base_url() ?>mgmt/add_dept">
                                            <button class="mui-btn mui-btn--small mui-btn--accent">
                                                + ADD DEPARTMENT
                                            </button>
                                        </a>
                                    </li>
                                <?php } ?>
<?php if ($this->plan_model->is_permission(TEAM, 'ADD')) { ?> 
                                    <li>
                                        <a href="<?php echo base_url() ?>team/add_team">
                                            <button class="mui-btn mui-btn--small mui-btn--accent">
                                                + ADD TEAM
                                            </button>
                                        </a>
                                    </li>
<?php }
$this->load->view('includes/global_setting',$data);
?>


                                <!--<li>
                                      <a href="#">
                                      <button class="mui-btn mui-btn--small mui-btn--accent">
                                      + ADD LOCATION
                                      </button>
                                      </a>
                                   </li>-->
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            <div id="content-wrapper">
                <div class="mui--appbar-height"></div>
                <div class="mui-container-fluid" id="right-container-fluid">
                    <div class="right_side" style="height:auto; padding-bottom: 0px;">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            </div>

                        </div>
                    </div>
                    <div class="add_franchise" style="padding-top:0px;">
                        <form action="" id="add_franchise" method="post" autocomplete="off" onsubmit="add_franchise(); return false;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br>
                                <div class="row">
                                    <h2>Franchise</h2>
                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <input type="hidden" name="franchise_id" value="">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <div class="mui-textfield mui-textfield--float-label">
                                                        <input type="text" name="franchise_name" required>
                                                        <label>Franchise Name<sup>*</sup></label>
                                                    </div>
                                                </div>
                                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <div class="mui-textfield mui-textfield--float-label">
                                                        <input type="text" name="legal_name" required>
                                                        <label>Franchise Legal Name<sup>*</sup></label>
                                                    </div>
                                                </div>
                                                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <div class="mui-textfield mui-textfield--float-label">
                                                        <input type="email" name="franchise_email" required>
                                                        <label>Franchise email(username)<sup>*</sup></label>
                                                    </div>
                                                </div>
                                                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="franchise_mobile" pattern="[789][0-9]{9}" required>
                                                      <label>Mobile(password) <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                </div>
                                            </div>
					    <div class="row"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						     
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="number" class="tax" name="margin" min="0" step="0.001" value=""required>
                                                                         <span class="title_box_left">
                                                            %
                                                            </span>
                                                                        <label>Franchise Margin<sup>*</sup></label>
                                                                    
                                                                </div>
						    </div>
					    
					    </div>
					    </div>

                                            
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                                                    <h4>Region</h4>
                                                    <?php $isallindia_perm=$this->mgmt_model->department_region_access();?>
                                                   <!-- <label class="radio-inline">
                                                        <input type="radio" <?php echo (!$isallindia_perm)?"disabled":""; ?> required name="region"  value="allindia" > All india
                                                    </label>-->
                                                    <label class="radio-inline">
                                                        <input type="radio" required name="region"  value="region" > Region wise
                                                    </label>
                                                </div>
                                            </div>
                                            <div class='row '>
                                                <input type="hidden" name="regiondat" id="regiondat" value="">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hide regionappend">
                                                    <div class="row xxxx">
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <div class="">

                                                                <!-- onchange="search_filter_city(this.value)"-->
                                                                <select name="state" class="statelist form-control" id="statelist"    required>
                                                                    <option value="">All State</option>
<?php $this->plan_model->state_list(); ?>
                                                                </select>
                                                                <input type="hidden" class="statesel" name="statesel" value="">
                                                                <!--<label>State</label>-->

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <div class="">
                                                                <select name="city" class="search_citylist form-control" >
                                                                    <option value="all">Select Cities</option>

                                                                </select>
                                                                <input type="hidden" class="citysel" name="citysel" value="">
                                                                <!--<label>City</label>-->
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <div class="">
                                                                <select name="zone" class="zone_list form-control" >
                                                                    <option value="all">Select Zones</option>

                                                                </select>
                                                                <!--<label>Zone</label>-->
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 btnreg">
                                                            <button class="mui-btn mui-btn--small mui-btn--accent addregion">
                                                                + ADD Region
                                                            </button>
                                                            <span class="rem"></span>

                                                        </div>

                                                        <input type="hidden" class="valregion" rel="" data-stateid="all" data-cityid="all" data-zoneid="all" data-mapid="">
                                                    </div>


                                                </div>  
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px; margin-top: 20px;">

                                                </div>   
                                            
                                            </div>

                                           

                                            






                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:25px">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE">
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="Deletetopup_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
    <input type="hidden" id="deltopupid" value="">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                </h4>
            </div>
            <div class="modal-body" style="padding-bottom:5px">
                <p id="erromsg">Are you sure you want to Delete Top Up ?</p>
            </div>
            <div class="modal-footer" style="text-align: right">
                <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
                <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyes" >YES</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {

        $(document).on('click', '.addregion', function () {
            // var $x=$(this).closest('.row').clone();
            var $block = $(this).closest('.row');

            var $clone = $block.clone();


            $clone.find('option:selected').prop("selected", false);
            $clone.find('.search_citylist').html('').append('<option  value="all">All Cities</option>');
            $clone.find('.zone_list').html('').append('<option  value="all">All Zones</option>');
            $clone.find('.valregion').attr('data-zoneid', '');
            $clone.find('.valregion').attr('data-mapid', '');
            $clone.find('.valregion').attr('data-cityid', '');
            $clone.find('.valregion').attr('data-stateid', '');
            $clone.appendTo('.regionappend').find('.rem').html("<button class='mui-btn mui-btn--small mui-btn--accent remregion'>-</button>");

            return false;
        });

        $(document).on('click', '.remregion', function () {

            $(this).closest('.row').remove();
        });

        $(document).on('change', 'input:radio[name="region"]', function () {

            if ($(this).val() === "region")
            {
                $('.regionappend').removeClass('hide');
                $('.statelist').attr('required', true);
                $('.search_citylist').attr('required', true);
                $('.zone_list').attr('required', true);
            } else
            {
                $('.regionappend').addClass('hide');
                $('.statelist').attr('required', false);
                $('.search_citylist').attr('required', false);
                $('.zone_list').attr('required', false);
            }

        });


        $(document).on('click', '.classro', function () {
            if ($(this).prop("checked") == true)
            {
                $(this).closest('.col-lg-6').find('.chk').prop('checked', false);
            }




        });

        $(document).on('click', '.chkae', function () {
            if ($(this).prop("checked") == true)
            {
                $(this).closest('.col-lg-6').find('.classro').prop('checked', false);
                $(this).closest('.col-lg-6').find('.chkhd').prop('checked', false);
            }

        });

        $(document).on('click', '.chkhd', function () {
            if ($(this).prop("checked") == true)
            {
                $(this).closest('.col-lg-6').find('.chkn').prop('checked', false);
            }
        });





        $(document).on('change', '.statelist', function () {
            var stateid = $(this).val();
            var $this = $(this);
            $(this).closest('.row').find('.statesel').val(stateid);
            $this.closest('.row').find('.valregion').data('stateid', stateid);
            $this.closest('.row').find('.valregion').data('cityid', 'all');
            $this.closest('.row').find('.valregion').data('zoneid', 'all');
            $.ajax({
                url: base_url + 'plan/getcitylist',
                type: 'POST',
                dataType: 'text',
                data: 'stateid=' + stateid,
                success: function (data) {

                    $this.closest('.row').find('.search_citylist').empty();
                    $this.closest('.row').find('.search_citylist').append(data);
                }


            });

        });

        $(document).on('change', '.search_citylist', function () {
            if ($(this).val() == "addc")
            {
                $('.city_text').val('');
                var state_id = $(this).closest('.row').find('.statelist').val();
                $('.state_id').val(state_id);
                $('#add_city').modal('show');
                return false;
            }
            var cityid = $(this).val();
            var stateid = $(this).closest('.row').find('.statelist').val();
            var $this = $(this);
            $(this).closest('.row').find('.citysel').val(cityid);


            $this.closest('.row').find('.valregion').data('cityid', cityid);
            $.ajax({
                url: base_url + 'plan/getzonelist',
                type: 'POST',
                dataType: 'text',
                data: 'cityid=' + cityid + '&stateid=' + stateid,
                success: function (data) {

                    $this.closest('.row').find('.zone_list').empty();
                    $this.closest('.row').find('.zone_list').append(data);
                }


            });

        });


        $(document).on('change', '.zone_list', function () {
            if ($(this).val() == "addz") {
                $('.zone_text').val('');
                var state_id = $(this).closest('.row').find('.search_citylist').val();
                var city_id = $(this).closest('.row').find('.search_citylist').val();
                $('.state_id').val(state_id);
                $('.city_id').val(city_id);
                $('#add_zone').modal('show');
                return false;
            }
            var zoneid = $(this).val();
            var $this = $(this);
            $this.closest('.row').find('.valregion').data('zoneid', zoneid);

        });
    })


    function add_franchise()
    {

       


        var regionarr = [];
        $(".valregion").each(function () {
            var stateid = $(this).data('stateid');
            var cityid = $(this).data('cityid');
            var zoneid = $(this).data('zoneid');
            var mapid = $(this).data('mapid');
            var fd = stateid + "::" + cityid + "::" + zoneid + "::" + mapid;
            regionarr.push(fd);
        });
        $('#regiondat').val(regionarr);
        var formdata = $("#add_franchise").serialize();



        // alert(data);
        $('.loading').removeClass('hide');
        $.ajax({
            url: base_url + 'mgmt/add_franchise_data',
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function (data) {
                //  alert(data);
               window.location = base_url + "mgmt";
                $('.loading').addClass('hide');
                // mui.tabs.activate('Plan-settings');


            }
        });
    }
</script>
<script>
    if ($(window)) {
        $(function () {
            $('.menu').crbnMenu({
                hideActive: true
            });
        });
    }
    //setting_menu_ul
    $(document).ready(function () {
//class="active" style="display: block;"
        $("#setting_menu_ul").css("display", "block");
        $("#team_menu").addClass("menu_active");

    });
</script>
<?php $this->load->view('includes/footer'); ?>
