

<!-- Start your project here-->
<?php $this->load->view('includes/header');?>
<div class="loading hide" >
   <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="wapper">
         <div id="sidedrawer" class="mui--no-user-select">
            <div id="sidedrawer-brand" class="mui--appbar-line-height">
               <span class="mui--text-title">
               <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
               <img src="<?php echo $img ?>" class="img-responsive" />
               </span>
            </div>
            <?php
               $data['navperm']=$this->plan_model->leftnav_permission();
               
               $this->view('left_nav',$data); ?>
         </div>
         <header id="header">
            <nav class="navbar navbar-default">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <a class="navbar-brand" href="#">Department</a>
                  </div>
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <?php $sessiondata = $this->session->userdata('isp_session');
                           if($sessiondata['super_admin']==1)
                           {
                              if($sessiondata['is_franchise']==0){
                           ?>
                        <li>
                           <a href="<?php echo base_url()?>mgmt/add_franchise">
                           <button class="mui-btn mui-btn--small mui-btn--accent">
                           + ADD FRANCHISE
                           </button>
                           </a>
                        </li>
                        <?php
                           }
                                                      }?>
                        <?php if($this->plan_model->is_permission(DEPARTMENT,'ADD')){ ?> 
                        <li>
                           <a href="<?php echo base_url()?>mgmt/add_dept">
                           <button class="mui-btn mui-btn--small mui-btn--accent">
                           + ADD DEPARTMENT
                           </button>
                           </a>
                        </li>
                        <?php } ?>
                        <?php if($this->plan_model->is_permission(TEAM,'ADD')){ ?> 
                        <li>
                           <a href="<?php echo base_url()?>team/add_team">
                           <button class="mui-btn mui-btn--small mui-btn--accent">
                           + ADD TEAM
                           </button>
                           </a>
                        </li>
                        <?php }
                           $this->load->view('includes/global_setting',$data);
                           ?>
                        <!--<li>
                           <a href="#">
                           <button class="mui-btn mui-btn--small mui-btn--accent">
                           + ADD LOCATION
                           </button>
                           </a>
                           </li>-->
                     </ul>
                  </div>
               </div>
            </nav>
         </header>
         <div id="content-wrapper">
            <div class="mui--appbar-height"></div>
            <div class="mui-container-fluid" id="right-container-fluid">
               <div class="right_side" style="height:auto; padding-bottom: 0px;">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     </div>
                  </div>
               </div>
               <?php //echo "<pre>"; print_R($dept); die;?>
               <div class="add_user" style="padding-top:0px;">
                  <form action="" id="add_dept" method="post" autocomplete="off" onsubmit="add_dept(); return false;">
                     <input type="hidden" name="dept_id" value="<?php echo $dept['id'];?>">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <br>
                        <div class="row">
                           <h2>Department</h2>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                       <div class="mui-textfield mui-textfield--float-label">
                                          <input type="text" <?php echo (isset($ro))?"disabled":"";?> name="dept_name" value="<?php echo $dept['dept_name']?>" required>
                                          <label>Department Name<sup>*</sup></label>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                       <label class="checkbox-inline ">
                                       <input type="checkbox" class="" <?php echo (isset($ro))?"disabled":"";?> <?php echo ($dept['assign_ticket']==1)?"checked":""; ?> id="inlineCheckbox1" name="assigntkt" value="1"> Assign Ticket
                                       </label>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                                       <?php $isallindia_perm=$this->mgmt_model->department_region_access();?>
                                       <h4>Region</h4>
                                       <!-- <label class="radio-inline">
                                          <input type="radio" required name="region" <?php echo (isset($ro))?"disabled":"";?> <?php echo (!$isallindia_perm)?"disabled":""; ?> value="allindia" <?php echo ($dept['region_type']=="allindia")?"checked":""; ?> > All india
                                          </label>-->
                                       <label class="radio-inline">
                                       <input type="radio" required name="region" <?php echo (isset($ro))?"disabled":"";?> value="region" <?php echo ($dept['region_type']=="region")?"checked":""; ?> > Region wise
                                       </label>
                                    </div>
                                 </div>
                                 <div class='row '>
                                    <input type="hidden" name="regiondat" id="regiondat" value="">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo ($dept['region_type']=="region")?"":"hide"; ?> regionappend">
                                       <?php
                                          $i=0;
                                          foreach($dept_region as $vald){       ?>                 
                                       <div class="row xxxx">
                                          <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="">
                                                <!-- onchange="search_filter_city(this.value)"-->
                                                <select name="state" <?php echo (isset($ro))?"disabled":"";?> class="statelist form-control rmsel" id="statelist"    required>
                                                   <option value="">Select States</option>
                                                   <?php $this->plan_model->state_list($vald->state_id); ?>
                                                </select>
                                                <input type="hidden" class="statesel" name="statesel" value="<?php echo $vald->state_id;?>">
                                                <!--<label>State</label>-->
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="">
                                                <select name="city" <?php echo (isset($ro))?"disabled":"";?> class="search_citylist form-control rmsel" >
                                                <?php $this->plan_model->getcitylist($vald->state_id,$vald->city_id); ?>
                                                </select>
                                                <input type="hidden" class="citysel" name="citysel" value="<?php echo $vald->city_id;?>">
                                                <!--<label>City</label>-->
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="">
                                                <select name="zone" <?php echo (isset($ro))?"disabled":"";?> class="zone_list form-control rmsel" >
                                                <?php $this->plan_model->getzonelist($vald->city_id,$vald->zone_id); ?>
                                                </select>
                                                <!--<label>Zone</label>-->
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3 btnreg">
                                             <button class="mui-btn mui-btn--small mui-btn--accent addregion" <?php echo (isset($ro))?"disabled":"";?>>
                                             + ADD Region
                                             </button>
                                             <?php if($i>0){ ?>
                                             <span class="rem"><button class="mui-btn mui-btn--small mui-btn--accent remregion" <?php echo (isset($ro))?"disabled":"";?> rel="<?php echo $vald->id;?>">-</button></span>
                                             <?php }
                                                else{?>
                                             <span class="rem" ></span>
                                             <?php } ?>
                                          </div>
                                          <input type="hidden" class="valregion" rel="" data-stateid="<?php echo $vald->state_id;?>" data-cityid="<?php echo ($vald->city_id!=0 && $vald->city_id!='')?$vald->city_id:'all';?>" data-zoneid="<?php echo ($vald->zone_id!=0 && $vald->zone_id!='')?$vald->zone_id:'all';?>" data-mapid="<?php echo $vald->id;?>">
                                       </div>
                                       <?php
                                          $i++;
                                          
                                          } ?>
                                       <?php if($dept['region_type']=="allindia" || count($dept_region)==0){?>
                                       <div class="row xxxx">
                                          <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="">
                                                <!-- onchange="search_filter_city(this.value)"-->
                                                <select name="state" <?php echo (isset($ro))?"disabled":"";?> class="statelist form-control rmsel" id="statelist"    required>
                                                   <option value="">Select States</option>
                                                   <?php $this->user_model->state_list(); ?>
                                                </select>
                                                <input type="hidden" class="statesel" name="statesel" value="">
                                                <!--<label>State</label>-->
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="">
                                                <select name="city" <?php echo (isset($ro))?"disabled":"";?> class="search_citylist form-control rmsel" >
                                                   <option value="all">All Cities</option>
                                                </select>
                                                <input type="hidden" class="citysel" name="citysel" value="">
                                                <!--<label>City</label>-->
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="">
                                                <select name="zone" <?php echo (isset($ro))?"disabled":"";?> class="zone_list form-control rmsel" >
                                                   <option value="all">All Zones</option>
                                                </select>
                                                <!--<label>Zone</label>-->
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3 btnreg">
                                             <button <?php echo (isset($ro))?"disabled":"";?> class="mui-btn mui-btn--small mui-btn--accent addregion">
                                             + ADD Region
                                             </button>
                                             <span class="rem"></span>
                                          </div>
                                          <input type="hidden" class="valregion" rel="" data-stateid="all" data-cityid="all" data-zoneid="all" data-mapid="">
                                       </div>
                                       <?php } ?>
                                    </div>
                                 </div>
				 <div class="row">
                                    <h4>Dashboard</h1>
                                 </div>
				 <?php foreach($dashboard_tabdata as $dashbval){ ?>
                                 <div class="row">
                                    <input type="hidden" name="tabmenu[]" value="<?php echo $dashbval->id; ?>">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                       <label><?php echo $dashbval->tab_name;?></label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 chkrow ">
                                       <?php if($dashbval->is_addable==1){?>
                                       <label class="checkbox-inline ">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall" <?php echo (isset($dept_permission[$dashbval->id]->is_add) && $dept_permission[$dashbval->id]->is_add==1)?"checked":"";?> id="inlineCheckbox1" name="perm_<?php echo $dashbval->id; ?>[]" value="add"> Add
                                       </label>
                                       <?php } ?>
                                       <?php if($dashbval->is_editable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall"  <?php echo (isset($dept_permission[$dashbval->id]->is_edit) && $dept_permission[$dashbval->id]->is_edit==1)?"checked":"";?> id="inlineCheckbox2" name="perm_<?php echo $dashbval->id; ?>[]" value="edit"> Show
                                       </label>
                                       <?php } ?>
                                       <?php if($dashbval->is_deletable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall" <?php echo (isset($dept_permission[$dashbval->id]->is_delete) && $dept_permission[$dashbval->id]->is_delete==1)?"checked":"";?> id="inlineCheckbox3" name="perm_<?php echo $dashbval->id; ?>[]" value="delete"> Delete
                                       </label>
                                       <?php } ?>
                                       <?php if($dashbval->is_readable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> <?php echo(isset($dept_permission[$dashbval->id]->is_readonly)  && $dept_permission[$dashbval->id]->is_readonly==1)?"checked":""; ?> class="classro  chkn chkall" <?php echo (isset($dept_permission[$dashbval->id]->is_readonly) && $dept_permission[$dashbval->id]->is_readonly==1)?"checked":"";?>  id="inlineCheckbox4" name="perm_<?php echo $dashbval->id; ?>[]" value="ro"> Read Only
                                       </label>
                                       <?php } ?>
                                       <?php if($dashbval->is_hidable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> <?php echo(isset($dept_permission[$dashbval->id]->is_hide) && $dept_permission[$dashbval->id]->is_hide==1)?"checked":""; ?> class="chkhd chk chkall" <?php echo (isset($dept_permission[$dashbval->id]->is_hide) && $dept_permission[$dashbval->id]->is_hide==1)?"checked":"";?>  id="inlineCheckbox5" name="perm_<?php echo $dashbval->id; ?>[]" value="hide"> Hide
                                       </label>
                                       <?php } ?>
                                       <span class="errorchek" style="color:red;"></span>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <div class="row">
                                    <h4></h4>
                                 </div>
                                 <div class="row">
                                    <h4>
                                    User Management</h1>
                                 </div>
                                 <?php foreach($user_tabdata as $userval){ ?>
                                 <div class="row">
                                    <input type="hidden" name="tabmenu[]" value="<?php echo $userval->id; ?>">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                       <label><?php echo $userval->tab_name;?></label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 chkrow ">
                                       <?php if($userval->is_addable==1){?>
                                       <label class="checkbox-inline ">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall" <?php echo (isset($dept_permission[$userval->id]->is_add) && $dept_permission[$userval->id]->is_add==1)?"checked":"";?> id="inlineCheckbox1" name="perm_<?php echo $userval->id; ?>[]" value="add"> Add
                                       </label>
                                       <?php } ?>
                                       <?php if($userval->is_editable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall"  <?php echo (isset($dept_permission[$userval->id]->is_edit) && $dept_permission[$userval->id]->is_edit==1)?"checked":"";?> id="inlineCheckbox2" name="perm_<?php echo $userval->id; ?>[]" value="edit"> Edit
                                       </label>
                                       <?php } ?>
                                       <?php if($userval->is_deletable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall" <?php echo (isset($dept_permission[$userval->id]->is_delete) && $dept_permission[$userval->id]->is_delete==1)?"checked":"";?> id="inlineCheckbox3" name="perm_<?php echo $userval->id; ?>[]" value="delete"> Delete
                                       </label>
                                       <?php } ?>
                                       <?php if($userval->is_readable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> <?php echo(isset($dept_permission[$userval->id]->is_readonly)  && $dept_permission[$userval->id]->is_readonly==1)?"checked":""; ?> class="classro  chkn chkall" <?php echo (isset($dept_permission[$userval->id]->is_readonly) && $dept_permission[$userval->id]->is_readonly==1)?"checked":"";?>  id="inlineCheckbox4" name="perm_<?php echo $userval->id; ?>[]" value="ro"> Read Only
                                       </label>
                                       <?php } ?>
                                       <?php if($userval->is_hidable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> <?php echo(isset($dept_permission[$userval->id]->is_hide) && $dept_permission[$userval->id]->is_hide==1)?"checked":""; ?> class="chkhd chk chkall" <?php echo (isset($dept_permission[$userval->id]->is_hide) && $dept_permission[$userval->id]->is_hide==1)?"checked":"";?>  id="inlineCheckbox5" name="perm_<?php echo $userval->id; ?>[]" value="hide"> Hide
                                       </label>
                                       <?php } ?>
                                       <span class="errorchek" style="color:red;"></span>
                                    </div>
                                 </div>
				   <?php
					if($userval->tab_name == 'BILLING'){
					     echo '<div class="billtabactivity" style="padding:10px 20px;background-color:#e1e1e1">';
					     $bill_tabactivity = $this->mgmt_model->get_tab_data('BILLING');
					     foreach($bill_tabactivity as $billtabval){ ?>
						  <div class="row">
						       <input type="hidden" name="tabmenu[]" value="<?php echo $billtabval->id; ?>">
						       <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							  <label><?php echo $billtabval->tab_name;?></label>
						       </div>
						       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 chkrow ">
							  <?php if($billtabval->is_addable==1){?>
							  <label class="checkbox-inline ">
							  <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall" <?php echo (isset($dept_permission[$billtabval->id]->is_add) && $dept_permission[$billtabval->id]->is_add==1)?"checked":"";?> id="inlineCheckbox1" name="perm_<?php echo $billtabval->id; ?>[]" value="add"> Add
							  </label>
							  <?php } ?>
							  <?php if($billtabval->is_editable==1){?>
							  <label class="checkbox-inline">
							  <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall"  <?php echo (isset($dept_permission[$billtabval->id]->is_edit) && $dept_permission[$billtabval->id]->is_edit==1)?"checked":"";?> id="inlineCheckbox2" name="perm_<?php echo $billtabval->id; ?>[]" value="edit"> Edit
							  </label>
							  <?php } ?>
							  <?php if($billtabval->is_deletable==1){?>
							  <label class="checkbox-inline">
							  <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall" <?php echo (isset($dept_permission[$billtabval->id]->is_delete) && $dept_permission[$billtabval->id]->is_delete==1)?"checked":"";?> id="inlineCheckbox3" name="perm_<?php echo $billtabval->id; ?>[]" value="delete"> Delete
							  </label>
							  <?php } ?>
							  <?php if($billtabval->is_readable==1){?>
							  <label class="checkbox-inline">
							  <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> <?php echo(isset($dept_permission[$billtabval->id]->is_readonly)  && $dept_permission[$billtabval->id]->is_readonly==1)?"checked":""; ?> class="classro  chkn chkall" <?php echo (isset($dept_permission[$billtabval->id]->is_readonly) && $dept_permission[$billtabval->id]->is_readonly==1)?"checked":"";?>  id="inlineCheckbox4" name="perm_<?php echo $billtabval->id; ?>[]" value="ro"> Read Only
							  </label>
							  <?php } ?>
							  <?php if($billtabval->is_hidable==1){?>
							  <label class="checkbox-inline">
							  <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> <?php echo(isset($dept_permission[$billtabval->id]->is_hide) && $dept_permission[$billtabval->id]->is_hide==1)?"checked":""; ?> class="chkhd chk chkall" <?php echo (isset($dept_permission[$billtabval->id]->is_hide) && $dept_permission[$billtabval->id]->is_hide==1)?"checked":"";?>  id="inlineCheckbox5" name="perm_<?php echo $billtabval->id; ?>[]" value="hide"> Hide
							  </label>
							  <?php } ?>
							  <span class="errorchek" style="color:red;"></span>
						       </div>
						    </div>
						  <?php
						  if($billtabval->tab_name == 'MONTHLY TAB'){
						       $monthlytab_activity = $this->mgmt_model->get_tab_data('MONTHLYTAB');
						       echo '<div class="mbilltabactivity" style="padding:10px 30px;background-color:#e1e1e1">';
						       foreach($monthlytab_activity as $mbilltabval){ ?>
						       <div class="row">
							  <input type="hidden" name="tabmenu[]" value="<?php echo $mbilltabval->id; ?>">
							  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							     <label><?php echo $mbilltabval->tab_name;?></label>
							  </div>
							  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 chkrow ">
							     <?php if($mbilltabval->is_addable==1){?>
							     <label class="checkbox-inline ">
							     <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall" <?php echo (isset($dept_permission[$mbilltabval->id]->is_add) && $dept_permission[$mbilltabval->id]->is_add==1)?"checked":"";?> id="inlineCheckbox1" name="perm_<?php echo $mbilltabval->id; ?>[]" value="add"> Add
							     </label>
							     <?php } ?>
							     <?php if($mbilltabval->is_editable==1){?>
							     <label class="checkbox-inline">
							     <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall"  <?php echo (isset($dept_permission[$mbilltabval->id]->is_edit) && $dept_permission[$mbilltabval->id]->is_edit==1)?"checked":"";?> id="inlineCheckbox2" name="perm_<?php echo $mbilltabval->id; ?>[]" value="edit"> Edit
							     </label>
							     <?php } ?>
							     <?php if($mbilltabval->is_deletable==1){?>
							     <label class="checkbox-inline">
							     <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall" <?php echo (isset($dept_permission[$mbilltabval->id]->is_delete) && $dept_permission[$mbilltabval->id]->is_delete==1)?"checked":"";?> id="inlineCheckbox3" name="perm_<?php echo $mbilltabval->id; ?>[]" value="delete"> Delete
							     </label>
							     <?php } ?>
							     <?php if($mbilltabval->is_readable==1){?>
							     <label class="checkbox-inline">
							     <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> <?php echo(isset($dept_permission[$mbilltabval->id]->is_readonly)  && $dept_permission[$mbilltabval->id]->is_readonly==1)?"checked":""; ?> class="classro  chkn chkall" <?php echo (isset($dept_permission[$mbilltabval->id]->is_readonly) && $dept_permission[$mbilltabval->id]->is_readonly==1)?"checked":"";?>  id="inlineCheckbox4" name="perm_<?php echo $mbilltabval->id; ?>[]" value="ro"> Read Only
							     </label>
							     <?php } ?>
							     <?php if($mbilltabval->is_hidable==1){?>
							     <label class="checkbox-inline">
							     <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> <?php echo(isset($dept_permission[$mbilltabval->id]->is_hide) && $dept_permission[$mbilltabval->id]->is_hide==1)?"checked":""; ?> class="chkhd chk chkall" <?php echo (isset($dept_permission[$mbilltabval->id]->is_hide) && $dept_permission[$mbilltabval->id]->is_hide==1)?"checked":"";?>  id="inlineCheckbox5" name="perm_<?php echo $mbilltabval->id; ?>[]" value="hide"> Hide
							     </label>
							     <?php } ?>
							     <span class="errorchek" style="color:red;"></span>
							  </div>
						       </div>
					     <?php  	}
						       echo '</div>';
						  }?>
				   <?php    }
					echo '</div>';
					}
				   ?>
                                 <?php } ?>
                                 <div class="row">
                                    <h4></h4>
                                 </div>
                                 <div class="row">
                                    <h4>System Management</h4>
                                 </div>
                                 <?php foreach($left_tabdata as $leftval){ ?>
                                 <div class="row">
                                    <input type="hidden" name="tabmenu[]" value="<?php echo $leftval->id; ?>">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                       <label><?php echo $leftval->tab_name;?></label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 chkrow">
                                       <?php if($leftval->is_addable==1){?>
                                       <label class="checkbox-inline ">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall" <?php echo (isset($dept_permission[$leftval->id]->is_add) && $dept_permission[$leftval->id]->is_add==1)?"checked":"";?>  id="inlineCheckbox1" name="perm_<?php echo $leftval->id; ?>[]" value="add"> Add
                                       </label>
                                       <?php } ?>
                                       <?php if($leftval->is_editable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall"  <?php echo (isset($dept_permission[$leftval->id]->is_edit) && $dept_permission[$leftval->id]->is_edit==1)?"checked":"";?> id="inlineCheckbox2" name="perm_<?php echo $leftval->id; ?>[]" value="edit"> Edit
                                       </label>
                                       <?php } ?>
                                       <?php if($leftval->is_deletable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall"  <?php echo (isset($dept_permission[$leftval->id]->is_delete) && $dept_permission[$leftval->id]->is_delete==1)?"checked":"";?> id="inlineCheckbox3" name="perm_<?php echo $leftval->id; ?>[]" value="delete"> Delete
                                       </label>
                                       <?php } ?>
                                       <?php if($leftval->is_readable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> <?php echo(isset($dept_permission[$leftval->id]->is_readonly) && $dept_permission[$leftval->id]->is_readonly==1)?"checked":""; ?> class="classro  chkn chkall" <?php echo (isset($dept_permission[$leftval->id]->is_readonly) && $dept_permission[$leftval->id]->is_readonly==1)?"checked":"";?> id="inlineCheckbox4" name="perm_<?php echo $leftval->id; ?>[]" value="ro"> Read Only
                                       </label>
                                       <?php } ?>
                                       <?php if($leftval->is_hidable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> <?php echo(!isset($dept_permission[$leftval->id]->is_hide) && $dept_permission[$leftval->id]->is_hide==1)?"checked":""; ?> class="chkhd chk chkall" <?php echo (isset($dept_permission[$leftval->id]->is_hide) && $dept_permission[$leftval->id]->is_hide==1)?"checked":"";?> id="inlineCheckbox5" name="perm_<?php echo $leftval->id; ?>[]" value="hide"> Hide
                                       </label>
                                       <?php } ?>
                                       <span class="errorchek" style="color:red;" ></span>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <div class="row">
                                    <h4>Setting</h4>
                                 </div>
                                 <?php foreach($setting_tabdata as $settval){ ?>
                                 <div class="row">
                                    <input type="hidden" name="tabmenu[]" value="<?php echo $settval->id; ?>"> 
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                       <label><?php echo $settval->tab_name;?></label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 chkrow ">
                                       <?php if($settval->is_addable==1){?>
                                       <label class="checkbox-inline ">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall" <?php echo (isset($dept_permission[$settval->id]->is_add) && $dept_permission[$settval->id]->is_add==1)?"checked":"";?>  id="inlineCheckbox1" name="perm_<?php echo $settval->id; ?>[]" value="add"> Add
                                       </label>
                                       <?php } ?>
                                       <?php if($settval->is_editable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall"  <?php echo (isset($dept_permission[$settval->id]->is_edit) && $dept_permission[$settval->id]->is_edit==1)?"checked":"";?> id="inlineCheckbox2" name="perm_<?php echo $settval->id; ?>[]" value="edit"> Edit
                                       </label>
                                       <?php } ?>
                                       <?php if($settval->is_deletable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox" <?php echo (isset($ro))?"disabled":"";?> class="chkn chk chkae chkall" <?php echo (isset($dept_permission[$settval->id]->is_delete) && $dept_permission[$settval->id]->is_delete==1)?"checked":"";?> id="inlineCheckbox3" name="perm_<?php echo $settval->id; ?>[]" value="delete"> Delete
                                       </label>
                                       <?php } ?>
                                       <?php if($settval->is_readable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox"  <?php echo (isset($ro))?"disabled":"";?> <?php echo(isset($dept_permission[$settval->id]->is_readonly) && $dept_permission[$settval->id]->is_readonly==1)?"checked":""; ?> class="classro  chkn chkall" <?php echo (isset($dept_permission[$settval->id]->is_readonly) && $dept_permission[$settval->id]->is_readonly==1)?"checked":"";?> id="inlineCheckbox4" name="perm_<?php echo $settval->id; ?>[]" value="ro"> Read Only
                                       </label>
                                       <?php } ?>
                                       <?php if($settval->is_hidable==1){?>
                                       <label class="checkbox-inline">
                                       <input type="checkbox"  <?php echo (isset($ro))?"disabled":"";?> <?php echo(isset($dept_permission[$settval->id]->is_hide) && $dept_permission[$settval->id]->is_hide)?"checked":""; ?> class="chkhd chk chkall" <?php echo (isset($dept_permission[$settval->id]->is_hide) && $dept_permission[$settval->id]->is_hide==1)?"checked":"";?> id="inlineCheckbox5" name="perm_<?php echo $settval->id; ?>[]" value="hide"> Hide
                                       </label>
                                       <?php } ?>
                                       <span class="errorchek" style="color:red;"></span>
                                    </div>
                                 </div>
                                 <?php } ?>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:25px">
                                 <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                       <input type="submit" <?php echo (isset($ro))?"disabled":"";?> class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="Deletetopup_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
   <input type="hidden" id="deltopupid" value="">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">
               <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
            </h4>
         </div>
         <div class="modal-body" style="padding-bottom:5px">
            <p id="erromsg">Are you sure you want to Delete Top Up ?</p>
         </div>
         <div class="modal-footer" style="text-align: right">
            <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
            <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyes" >YES</button>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function(){
        $('.statelist').attr('required',false);
           $(document).on('click','.addregion',function(){
         // var $x=$(this).closest('.row').clone();
         
           var $block = $(this).closest('.row');
   
   
   var $clone = $block.clone();
   
      // Find the selected value in the clone, and remove
   $clone.find('option:selected').prop("selected", false);
    $clone.find('.valregion').attr('data-zoneid','');
   $clone.find('.valregion').attr('data-mapid','');
   $clone.find('.valregion').attr('data-cityid','');
   $clone.find('.valregion').attr('data-stateid','');
   $clone.appendTo('.regionappend').find('.rem').html("<button class='mui-btn mui-btn--small mui-btn--accent remregion'>-</button>");  
   
      return false;
      });
      $(document).on('click','.remregion',function(){
          
          $(this).closest('.row').remove();
          var id=$(this).attr('rel');
          if(id!=="" && id!==undefined){
                  $.ajax({
           url: base_url+'mgmt/delete_region_dept',
           type: 'POST',
           dataType: 'text',
           data: 'id='+id,
           success: function(data){
   
              
           }
   
   
       });
   }
      });
      
       $(document).on('change','input:radio[name="region"]',function(){
       
       if($(this).val()==="region")
       {
           $('.regionappend').removeClass('hide');
            $('.statelist').attr('required',true);
       }
       else
       {
            $('.regionappend').addClass('hide');
             $('.statelist').attr('required',false);
       }
       
      });
      
      $(document).ready(function(){
	  if($('input[name="perm_3[]"][value="ro"]').is(':checked')){
	       $('input[name="perm_34[]"]').prop('disabled', true);
	       $('input[name="perm_35[]"]').prop('disabled', true);
	       $('input[name="perm_36[]"]').prop('disabled', true);
	       $('input[name="perm_37[]"]').prop('disabled', true);
	       $('input[name="perm_38[]"]').prop('disabled', true);
	       $('input[name="perm_39[]"]').prop('disabled', true);
	       $('input[name="perm_40[]"]').prop('disabled', true);
	       $('input[name="perm_41[]"]').prop('disabled', true);
	       $('input[name="perm_42[]"]').prop('disabled', true);
	       $('input[name="perm_43[]"]').prop('disabled', true);
	       $('input[name="perm_44[]"]').prop('disabled', true);
	  }else if($('input[name="perm_3[]"][value="hide"]').is(':checked')){
	       $('input[name="perm_34[]"]').prop('disabled', true);
	       $('input[name="perm_35[]"]').prop('disabled', true);
	       $('input[name="perm_36[]"]').prop('disabled', true);
	       $('input[name="perm_37[]"]').prop('disabled', true);
	       $('input[name="perm_38[]"]').prop('disabled', true);
	       $('input[name="perm_39[]"]').prop('disabled', true);
	       $('input[name="perm_40[]"]').prop('disabled', true);
	       $('input[name="perm_41[]"]').prop('disabled', true);
	       $('input[name="perm_42[]"]').prop('disabled', true);
	       $('input[name="perm_43[]"]').prop('disabled', true);
	       $('input[name="perm_44[]"]').prop('disabled', true);
	  }else if($('input[name="perm_44[]"][value="ro"]').is(':checked')){
	       $('input[name="perm_34[]"]').prop('disabled', true);
	       $('input[name="perm_35[]"]').prop('disabled', true);
	       $('input[name="perm_36[]"]').prop('disabled', true);
	       $('input[name="perm_37[]"]').prop('disabled', true);
	       $('input[name="perm_38[]"]').prop('disabled', true);
	       $('input[name="perm_39[]"]').prop('disabled', true);
	       $('input[name="perm_40[]"]').prop('disabled', true);
	       $('input[name="perm_41[]"]').prop('disabled', true);
	  }else if($('input[name="perm_44[]"][value="hide"]').is(':checked')){
	       $('input[name="perm_34[]"]').prop('disabled', true);
	       $('input[name="perm_35[]"]').prop('disabled', true);
	       $('input[name="perm_36[]"]').prop('disabled', true);
	       $('input[name="perm_37[]"]').prop('disabled', true);
	       $('input[name="perm_38[]"]').prop('disabled', true);
	       $('input[name="perm_39[]"]').prop('disabled', true);
	       $('input[name="perm_40[]"]').prop('disabled', true);
	       $('input[name="perm_41[]"]').prop('disabled', true);
	  }else{
	       $('input[name="perm_34[]"]').prop('disabled', false);
	       $('input[name="perm_35[]"]').prop('disabled', false);
	       $('input[name="perm_36[]"]').prop('disabled', false);
	       $('input[name="perm_37[]"]').prop('disabled', false);
	       $('input[name="perm_38[]"]').prop('disabled', false);
	       $('input[name="perm_39[]"]').prop('disabled', false);
	       $('input[name="perm_40[]"]').prop('disabled', false);
	       $('input[name="perm_41[]"]').prop('disabled', false);
	       $('input[name="perm_42[]"]').prop('disabled', false);
	       $('input[name="perm_43[]"]').prop('disabled', false);
	       $('input[name="perm_44[]"]').prop('disabled', false);
	  }
      });
      
      $(document).on('click', '.classro', function () {
          if ($(this).prop("checked") == true)
          {
              $(this).closest('.col-lg-6').find('.chk').prop('checked', false);
          }
	  
	  var actionitem = $(this).attr('name');
	  if ((actionitem == 'perm_3[]') && ($(this).prop("checked") == true)) {
	       $('input[name="perm_34[]"]').prop('checked', false);
	       $('input[name="perm_35[]"]').prop('checked', false);
	       $('input[name="perm_36[]"]').prop('checked', false);
	       $('input[name="perm_37[]"]').prop('checked', false);
	       $('input[name="perm_38[]"]').prop('checked', false);
	       $('input[name="perm_39[]"]').prop('checked', false);
	       $('input[name="perm_40[]"]').prop('checked', false);
	       $('input[name="perm_41[]"]').prop('checked', false);
	       $('input[name="perm_42[]"]').prop('checked', false);
	       $('input[name="perm_43[]"]').prop('checked', false);
	       $('input[name="perm_44[]"]').prop('checked', false);
	       
	       $('input[name="perm_34[]"]').prop('disabled', false);
	       $('input[name="perm_35[]"]').prop('disabled', false);
	       $('input[name="perm_36[]"]').prop('disabled', false);
	       $('input[name="perm_37[]"]').prop('disabled', false);
	       $('input[name="perm_38[]"]').prop('disabled', false);
	       $('input[name="perm_39[]"]').prop('disabled', false);
	       $('input[name="perm_40[]"]').prop('disabled', false);
	       $('input[name="perm_41[]"]').prop('disabled', false);
	       $('input[name="perm_42[]"]').prop('disabled', false);
	       $('input[name="perm_43[]"]').prop('disabled', false);
	       $('input[name="perm_44[]"]').prop('disabled', false);
	       
	       $('input[name="perm_34[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_35[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_36[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_37[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_38[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_39[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_40[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_41[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_42[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_43[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_44[]"][value="ro"]').prop('checked', true);
	       
	       $('input[name="perm_34[]"]').prop('disabled', true);
	       $('input[name="perm_35[]"]').prop('disabled', true);
	       $('input[name="perm_36[]"]').prop('disabled', true);
	       $('input[name="perm_37[]"]').prop('disabled', true);
	       $('input[name="perm_38[]"]').prop('disabled', true);
	       $('input[name="perm_39[]"]').prop('disabled', true);
	       $('input[name="perm_40[]"]').prop('disabled', true);
	       $('input[name="perm_41[]"]').prop('disabled', true);
	       $('input[name="perm_42[]"]').prop('disabled', true);
	       $('input[name="perm_43[]"]').prop('disabled', true);
	       $('input[name="perm_44[]"]').prop('disabled', true);
	  }
	  else if ((actionitem == 'perm_44[]') && ($(this).prop("checked") == true)) {
	       $('input[name="perm_34[]"]').prop('checked', false);
	       $('input[name="perm_35[]"]').prop('checked', false);
	       $('input[name="perm_36[]"]').prop('checked', false);
	       $('input[name="perm_37[]"]').prop('checked', false);
	       $('input[name="perm_38[]"]').prop('checked', false);
	       $('input[name="perm_39[]"]').prop('checked', false);
	       $('input[name="perm_40[]"]').prop('checked', false);
	       $('input[name="perm_41[]"]').prop('checked', false);
	       
	       $('input[name="perm_34[]"]').prop('disabled', false);
	       $('input[name="perm_35[]"]').prop('disabled', false);
	       $('input[name="perm_36[]"]').prop('disabled', false);
	       $('input[name="perm_37[]"]').prop('disabled', false);
	       $('input[name="perm_38[]"]').prop('disabled', false);
	       $('input[name="perm_39[]"]').prop('disabled', false);
	       $('input[name="perm_40[]"]').prop('disabled', false);
	       $('input[name="perm_41[]"]').prop('disabled', false);
	       
	       $('input[name="perm_34[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_35[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_36[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_37[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_38[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_39[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_40[]"][value="ro"]').prop('checked', true);
	       $('input[name="perm_41[]"][value="ro"]').prop('checked', true);
	       
	       $('input[name="perm_34[]"]').prop('disabled', true);
	       $('input[name="perm_35[]"]').prop('disabled', true);
	       $('input[name="perm_36[]"]').prop('disabled', true);
	       $('input[name="perm_37[]"]').prop('disabled', true);
	       $('input[name="perm_38[]"]').prop('disabled', true);
	       $('input[name="perm_39[]"]').prop('disabled', true);
	       $('input[name="perm_40[]"]').prop('disabled', true);
	       $('input[name="perm_41[]"]').prop('disabled', true);
	  }
      });
   
      $(document).on('click', '.chkae', function () {
          if ($(this).prop("checked") == true)
          {
              $(this).closest('.col-lg-6').find('.classro').prop('checked', false);
              $(this).closest('.col-lg-6').find('.chkhd').prop('checked', false);
          }
	  
	  var actionitem = $(this).attr('name');
	  if ((actionitem == 'perm_3[]') && ($(this).prop("checked") == true)) {
	       $('input[name="perm_34[]"]').prop('checked', false); $('input[name="perm_34[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_35[]"]').prop('checked', false); $('input[name="perm_35[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_36[]"]').prop('checked', false); $('input[name="perm_36[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_37[]"]').prop('checked', false); $('input[name="perm_37[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_38[]"]').prop('checked', false); $('input[name="perm_38[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_39[]"]').prop('checked', false); $('input[name="perm_39[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_40[]"]').prop('checked', false); $('input[name="perm_40[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_41[]"]').prop('checked', false); $('input[name="perm_41[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_42[]"]').prop('checked', false); $('input[name="perm_42[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_43[]"]').prop('checked', false); $('input[name="perm_43[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_44[]"]').prop('checked', false); $('input[name="perm_44[]"][value="edit"]').prop('checked', true);
	       
	       $('input[name="perm_34[]"]').prop('disabled', false);
	       $('input[name="perm_35[]"]').prop('disabled', false);
	       $('input[name="perm_36[]"]').prop('disabled', false);
	       $('input[name="perm_37[]"]').prop('disabled', false);
	       $('input[name="perm_38[]"]').prop('disabled', false);
	       $('input[name="perm_39[]"]').prop('disabled', false);
	       $('input[name="perm_40[]"]').prop('disabled', false);
	       $('input[name="perm_41[]"]').prop('disabled', false);
	       $('input[name="perm_42[]"]').prop('disabled', false);
	       $('input[name="perm_43[]"]').prop('disabled', false);
	       $('input[name="perm_44[]"]').prop('disabled', false);
	  }
	  else if ((actionitem == 'perm_44[]') && ($(this).prop("checked") == true)) {
	       $('input[name="perm_34[]"]').prop('checked', false); $('input[name="perm_34[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_35[]"]').prop('checked', false); $('input[name="perm_35[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_36[]"]').prop('checked', false); $('input[name="perm_36[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_37[]"]').prop('checked', false); $('input[name="perm_37[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_38[]"]').prop('checked', false); $('input[name="perm_38[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_39[]"]').prop('checked', false); $('input[name="perm_39[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_40[]"]').prop('checked', false); $('input[name="perm_40[]"][value="edit"]').prop('checked', true);
	       $('input[name="perm_41[]"]').prop('checked', false); $('input[name="perm_41[]"][value="edit"]').prop('checked', true);
	       
	       $('input[name="perm_34[]"]').prop('disabled', false);
	       $('input[name="perm_35[]"]').prop('disabled', false);
	       $('input[name="perm_36[]"]').prop('disabled', false);
	       $('input[name="perm_37[]"]').prop('disabled', false);
	       $('input[name="perm_38[]"]').prop('disabled', false);
	       $('input[name="perm_39[]"]').prop('disabled', false);
	       $('input[name="perm_40[]"]').prop('disabled', false);
	       $('input[name="perm_41[]"]').prop('disabled', false);
	  }
   
      });
   
      $(document).on('click', '.chkhd', function () {
          if ($(this).prop("checked") == true)
          {
              $(this).closest('.col-lg-6').find('.chkn').prop('checked', false);
          }
	  var actionitem = $(this).attr('name');
	  if ((actionitem == 'perm_3[]') && ($(this).prop("checked") == true)) {
	       $('input[name="perm_34[]"]').prop('checked', false);
	       $('input[name="perm_35[]"]').prop('checked', false);
	       $('input[name="perm_36[]"]').prop('checked', false);
	       $('input[name="perm_37[]"]').prop('checked', false);
	       $('input[name="perm_38[]"]').prop('checked', false);
	       $('input[name="perm_39[]"]').prop('checked', false);
	       $('input[name="perm_40[]"]').prop('checked', false);
	       $('input[name="perm_41[]"]').prop('checked', false);
	       $('input[name="perm_42[]"]').prop('checked', false);
	       $('input[name="perm_43[]"]').prop('checked', false);
	       $('input[name="perm_44[]"]').prop('checked', false);
	       
	       $('input[name="perm_34[]"]').prop('disabled', false);
	       $('input[name="perm_35[]"]').prop('disabled', false);
	       $('input[name="perm_36[]"]').prop('disabled', false);
	       $('input[name="perm_37[]"]').prop('disabled', false);
	       $('input[name="perm_38[]"]').prop('disabled', false);
	       $('input[name="perm_39[]"]').prop('disabled', false);
	       $('input[name="perm_40[]"]').prop('disabled', false);
	       $('input[name="perm_41[]"]').prop('disabled', false);
	       $('input[name="perm_42[]"]').prop('disabled', false);
	       $('input[name="perm_43[]"]').prop('disabled', false);
	       $('input[name="perm_44[]"]').prop('disabled', false);
	       
	       $('input[name="perm_34[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_35[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_36[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_37[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_38[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_39[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_40[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_41[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_42[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_43[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_44[]"][value="hide"]').prop('checked', true);
	       
	       $('input[name="perm_34[]"]').prop('disabled', true);
	       $('input[name="perm_35[]"]').prop('disabled', true);
	       $('input[name="perm_36[]"]').prop('disabled', true);
	       $('input[name="perm_37[]"]').prop('disabled', true);
	       $('input[name="perm_38[]"]').prop('disabled', true);
	       $('input[name="perm_39[]"]').prop('disabled', true);
	       $('input[name="perm_40[]"]').prop('disabled', true);
	       $('input[name="perm_41[]"]').prop('disabled', true);
	       $('input[name="perm_42[]"]').prop('disabled', true);
	       $('input[name="perm_43[]"]').prop('disabled', true);
	       $('input[name="perm_44[]"]').prop('disabled', true);
	  }
	  else if ((actionitem == 'perm_44[]') && ($(this).prop("checked") == true)) {
	       $('input[name="perm_34[]"]').prop('checked', false);
	       $('input[name="perm_35[]"]').prop('checked', false);
	       $('input[name="perm_36[]"]').prop('checked', false);
	       $('input[name="perm_37[]"]').prop('checked', false);
	       $('input[name="perm_38[]"]').prop('checked', false);
	       $('input[name="perm_39[]"]').prop('checked', false);
	       $('input[name="perm_40[]"]').prop('checked', false);
	       $('input[name="perm_41[]"]').prop('checked', false);
	       
	       $('input[name="perm_34[]"]').prop('disabled', false);
	       $('input[name="perm_35[]"]').prop('disabled', false);
	       $('input[name="perm_36[]"]').prop('disabled', false);
	       $('input[name="perm_37[]"]').prop('disabled', false);
	       $('input[name="perm_38[]"]').prop('disabled', false);
	       $('input[name="perm_39[]"]').prop('disabled', false);
	       $('input[name="perm_40[]"]').prop('disabled', false);
	       $('input[name="perm_41[]"]').prop('disabled', false);
	       
	       $('input[name="perm_34[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_35[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_36[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_37[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_38[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_39[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_40[]"][value="hide"]').prop('checked', true);
	       $('input[name="perm_41[]"][value="hide"]').prop('checked', true);
	       
	       $('input[name="perm_34[]"]').prop('disabled', true);
	       $('input[name="perm_35[]"]').prop('disabled', true);
	       $('input[name="perm_36[]"]').prop('disabled', true);
	       $('input[name="perm_37[]"]').prop('disabled', true);
	       $('input[name="perm_38[]"]').prop('disabled', true);
	       $('input[name="perm_39[]"]').prop('disabled', true);
	       $('input[name="perm_40[]"]').prop('disabled', true);
	       $('input[name="perm_41[]"]').prop('disabled', true);
	  }
      });
      
    $(document).on('change','.statelist',function(){
        var stateid=$(this).val();
        var $this=$(this);
          $(this).closest('.row').find('.statesel').val(stateid);
        $this.closest('.row').find('.valregion').data('stateid',stateid);
        $.ajax({
      url: base_url+'plan/getcitylist',
      type: 'POST',
      dataType: 'text',
      data: 'stateid='+stateid,
      success: function(data){
      
         $this.closest('.row').find('.search_citylist').empty().append('<option value="all">All Cities</option>');
           $this.closest('.row').find('.search_citylist').append(data);
      }
      
      
   });
       
    });
    
     $(document).on('change','.search_citylist',function(){
          if($(this).val()=="addc")
        {
          $('.city_text').val('');
          var state_id=$(this).closest('.row').find('.statelist').val();
          $('.state_id').val(state_id);
          $('#add_city').modal('show'); 
          return false;
        }
        var cityid=$(this).val();
         var stateid = $(this).closest('.row').find('.statelist').val();
       $(this).closest('.row').find('.citysel').val(cityid);
       var $this=$(this);
        $this.closest('.row').find('.valregion').data('cityid',cityid);
        $.ajax({
      url: base_url+'plan/getzonelist',
      type: 'POST',
      dataType: 'text',
       data: 'cityid=' + cityid + '&stateid=' + stateid,
      success: function(data){
       
         $this.closest('.row').find('.zone_list').empty();
           $this.closest('.row').find('.zone_list').append(data);
      }
      
      
   });
       
    });
    
    
     $(document).on('change','.zone_list',function(){
         if($(this).val()=="addz"){
                      $('.zone_text').val('');
               var state_id=$(this).closest('.row').find('.search_citylist').val();
               var city_id=$(this).closest('.row').find('.search_citylist').val();
               $('.state_id').val(state_id);
               $('.city_id').val(city_id);
              $('#add_zone').modal('show'); 
              return false;
         }
        var zoneid=$(this).val();
        var $this=$(this);
          $this.closest('.row').find('.valregion').data('zoneid',zoneid);
       
    });
   })
   
   
   function add_dept()
   {
   
   var valid=true;
   $('.chkrow').each(function(){
     var checked=false; 
   
     $(this).find('.chkall:checked').each(function() {
        
   checked=true;
   });
   
   if(checked==false)
   {
   $(this).find('.errorchek').html('Please Check Permission');
   valid=false;
   // return false;
   }
   else
   {
   $(this).find('.errorchek').html('');
   //  valid=true;
   }
      
   })
   
   
   if(valid==false)
   {
      return false;
   }
   
     $('input[name="perm_34[]"]').prop('disabled', false);
     $('input[name="perm_35[]"]').prop('disabled', false);
     $('input[name="perm_36[]"]').prop('disabled', false);
     $('input[name="perm_37[]"]').prop('disabled', false);
     $('input[name="perm_38[]"]').prop('disabled', false);
     $('input[name="perm_39[]"]').prop('disabled', false);
     $('input[name="perm_40[]"]').prop('disabled', false);
     $('input[name="perm_41[]"]').prop('disabled', false);
     $('input[name="perm_42[]"]').prop('disabled', false);
     $('input[name="perm_43[]"]').prop('disabled', false);
     $('input[name="perm_44[]"]').prop('disabled', false);
   
   var regionarr=[];
      $( ".valregion" ).each(function() {
          var stateid=$(this).data('stateid');
          var cityid=$(this).data('cityid');
          var zoneid=$(this).data('zoneid');
           var mapid=$(this).data('mapid');
          var fd=stateid+"::"+cityid+"::"+zoneid+"::"+mapid;
          	regionarr.push(fd);
   });
   $('#regiondat').val(regionarr);
    var formdata = $("#add_dept").serialize();
    
    
    
   // alert(data);
   var deptid = $('input[name="dept_id"]').val();
   $('.loading').removeClass('hide');
   $.ajax({
      url: base_url+'mgmt/add_perm_data',
      type: 'POST',
      dataType: 'json',
      data: formdata,
      success: function(data){
        //  alert(data);
             window.location = base_url+"mgmt/edit_dept/"+deptid;
         $('.loading').addClass('hide');
        // mui.tabs.activate('Plan-settings');
           
       
      }
   });
   }
</script>
<script>
   if ($(window)) {
    $(function () {
     $('.menu').crbnMenu({
      hideActive: true
     });
    });
   }
   //setting_menu_ul
   $(document).ready(function(){
   //class="active" style="display: block;"
   $("#setting_menu_ul").css("display","block");
   $("#team_menu").addClass("menu_active");
   
   });
</script>
<?php $this->load->view('includes/footer');?>

