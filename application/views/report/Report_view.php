

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title><?php echo $this->session->userdata['isp_session']['isp_name'] ?> </title>
      <!-- Font Awesome -->
      <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css">
      <!-- Bootstrap core CSS -->
      <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
      <!-- Material Design Bootstrap -->
      <link href="<?php echo base_url()?>assets/css/mui.min.css" rel="stylesheet">
      <!-- Your custom styles (optional) -->
      <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
      <link href="<?php echo base_url()?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/datepicker/bootstrap-datetimepicker.min.css" />
      <style>
	.form-calendar { padding-bottom:0px; margin: 0px; }
	.calendar_control {
	    width: 30%;
	    height: auto;
	    padding:0px;
	    float: left;
	}
	.fa-calendar{
	    color: #263646;
	}
      </style>
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading hide" >
         <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                     <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                     <img src="<?php echo $img ?>" class="img-responsive" />
                     </span>
                  </div>
                  <?php  $data['navperm']=$this->plan_model->leftnav_permission();
                     $this->view('left_nav',$data); ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">Reports</a>
                        </div>
                        <div class="col-sm-2 col-xs-2" style="margin-top:5px">
                           <?php $sessiondata = $this->session->userdata('isp_session');
                              $franchisearr=$this->report_model->get_franchise($sessiondata['isp_uid']);
                              
                              $style="none";
                              if($sessiondata['super_admin']==1)
                              {
                              if($sessiondata['is_franchise']==0){
                               $style="block";
                                }
                              }?>
                           <select name="franchis" class="form-control franchise" style="display:<?php echo $style;?>;">
                              <?php
                                 if(count($franchisearr)>0)
                                 {
                                    foreach($franchisearr as $fval)
                                    {
                                 ?>
                              <option value="<?php echo $fval['isp_uid']?>"><?php echo $fval['isp_name']?></option>
                              <?php
                                 }
                                 }
                                 ?>
                           </select>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
                              <li>
                                 <a href="add_user.html">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD USER
                                 </button>
                                 </a>
                              </li>
                              <li>
                                 <a href="add_plan.html">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD PLAN
                                 </button>
                                 </a>
                              </li>
                              <?php
                                 $this->view('includes/global_setting',$data); 
                                 ?>
                              <!--<li>
                                 <a href="#">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD LOCATION
                                 </button>
                                 </a>
                                 </li>-->
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="mui--appbar-height"></div>
                     <div class="add_user" style="padding-top:0px;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <ul class="mui-tabs__bar plan_mui-tabs__bar">
                                       <li class="mui--is-active" rel="perspective">
                                          <a data-mui-toggle="tab" id="perspuser" data-mui-controls="prospective" >Prospective Users</a>
                                       </li>
                                       <li rel="active">
                                          <a data-mui-toggle="tab" id="actvuser" data-mui-controls="active_users">
                                          Active Users</a>
                                       </li>
                                       <li rel="billing">
                                          <a data-mui-toggle="tab" id="billreport" data-mui-controls="billing_reports">
                                          Billing Reports</a>
                                       </li>
                                       <li rel="complaint">
                                          <a data-mui-toggle="tab" id="complaint_report" data-mui-controls="complaints_reports">
                                          Complaints Reports</a>
                                       </li>
                                       <li rel="plan">
                                          <a data-mui-toggle="tab" id="plantopup" data-mui-controls="plan_top_ups">
                                          Plan & Top-Ups</a>
                                       </li>
				       <li rel="custom">
                                          <a data-mui-toggle="tab" id="customtab" data-mui-controls="custom_reporting">
                                          Custom Reports</a>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="mui--appbar-height"></div>
                                    <div class="mui-tabs__pane mui--is-active" id="prospective">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row" style="margin-bottom:20px">
                                             <div class="col-lg-3 col-md-3 col-sm-3 nomargin-left">
                                                <div id="chartContainer" style="height: 200px; width: 70%;"></div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3">
                                                <?php $totaluser=$perspective['count']['inactivecount']+$perspective['count']['enquirycount']+$perspective['count']['leadcount'];?>
                                                <div class="donut_chart_text">
                                                   <a href="javascript:void(0);" class="perspfltr" rel="all">
                                                      <h1 style="margin-bottom:10px"><span class="ptot"><?php echo $totaluser;?></span> Total Prospects</h1>
                                                   </a>
                                                   <br/>
                                                   <a href="javascript:void(0);" class="perspfltr" rel="lead">
                                                      <h2 style="margin: 5px 0px!important;">
                                                         <i class="fa fa-circle" aria-hidden="true" style="color:#36465F"></i>
                                                         <span class="pleads"><?php echo $perspective['count']['leadcount'];?> Leads</span>
                                                      </h2>
                                                   </a>
                                                   <a href="javascript:void(0);" class="perspfltr" rel="enquiry">
                                                      <h2 style="margin: 5px 0px!important;">
                                                         <i class="fa fa-circle" aria-hidden="true" style="color:#E4AF23"></i>
                                                         <span class="penq"><?php echo $perspective['count']['enquirycount'];?> Enquiries</span>
                                                      </h2>
                                                   </a>
                                                   <a href="javascript:void(0);" class="perspfltr" rel="inactive">
                                                      <h2 style="margin: 5px 0px!important;">
                                                         <i class="fa fa-circle" aria-hidden="true" style="color:#F4474A"></i>
                                                         <span class="pinact"><?php echo $perspective['count']['inactivecount'];?> Inactive</span>
                                                      </h2>
                                                   </a>
                                                </div>
                                                <input type="hidden" name="perspclickfltr" id="perspclickfltr" value="all">
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 nomargin-left">
                                                <div class="">
                                                   <select class="form-control persp_filter">
                                                      <option value="all">All Users</option>
                                                      <option value="lead">Lead Users</option>
                                                      <option value="enquiry">Enquiry Users</option>
                                                      <option value="inactive">Inactive Users</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-lg-9 col-md-9 col-sm-9 text-right ">
                                                <span class="perspexcelerr" style="color:red"></span>
                                                <i class="fa fa-file-excel-o perspexcel" style="font-size: 34px; cursor:pointer;" aria-hidden="true"></i>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="table-responsive" id='search_perspview'>
                                                <table id="persp_table" class="table table-striped">
                                                   <thead>
                                                      <tr class="active">
                                                         <th>&nbsp;</th>
                                                         <th>NAME</th>
                                                         <th>EMAIL</th>
                                                         <th>MOBILE</th>
                                                         <th>STAGE / STATUS</th>
                                                         <th>KYC STATUS</th>
                                                         <th class="mui--text-right">ACTIONS</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody >
                                                      <?php
                                                         $i=1;
                                                          foreach($perspective['list'] as $vallist){
                                                              $usertype=(isset($vallist->usertype))?($vallist->usertype==1)?"Lead":"Enquiry":"Inactive";
                                                              $kyc=(isset($vallist->kyc_details))?($vallist->kyc_details==1)?"Done":"Not Done":"-";
                                                              $url=(isset($vallist->usertype))?base_url()."user/add/".$vallist->id:base_url()."user/edit/".$vallist->id;
                                                              ?>
                                                      <tr>
                                                         <td><?php echo $i; ?>.</td>
                                                         <td><a href="<?php echo $url; ?>"><?php echo $vallist->firstname." ".$vallist->lastname;  ?></a></td>
                                                         <td><?php echo $vallist->email;  ?></td>
                                                         <td>+91 <?php echo $vallist->mobile;  ?></td>
                                                         <td><?php echo $usertype?></td>
                                                         <td><?php echo $kyc ?></td>
                                                         <td class="mui--text-right"><span><a href="<?php echo $url; ?>">View</a></span>  </td>
                                                      </tr>
                                                      <?php
                                                         $i++;
                                                          } ?>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="mui-tabs__pane" id="active_users">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row" style="margin-bottom:20px">
                                             <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                   <div class="donut_chart_active">
                                                      <ul>
                                                         <li>
                                                            <a href="javascript:void(0);"  class="textactive daysfiletr">
                                                               <h2><span id="totalactive"></span> Total Active Users 
                                                                  <i class="fa fa-angle-right pull-right"></i>
                                                               </h2>
                                                            </a>
                                                         </li>
                                                         <li>
                                                            <a href="javascript:void(0);"  rel="last30" class="daysfiletr ">
                                                            <span id="last30days"></span> Active in last 30 days
                                                            <i class="fa fa-angle-right pull-right" style="font-size: 24px;font-weight:300;margin-top:-1.5px"></i>
                                                            </a>
                                                         </li>
                                                         <li>
                                                            <a href="javascript:void(0);" rel="last180" class="daysfiletr">
                                                            <span id="last180days"></span> Active between last 30 - 180 days
                                                            <i class="fa fa-angle-right pull-right" style="font-size: 24px;font-weight:300;margin-top:-1.5px"></i>
                                                            </a>
                                                         </li>
                                                         <li>
                                                            <a href="javascript:void(0);" rel="more180" class="daysfiletr">
                                                            <span id="more180days"></span>  Active for more than 180 days
                                                            <i class="fa fa-angle-right pull-right" style="font-size: 24px;font-weight:300;margin-top:-1.5px"></i>
                                                            </a>
                                                         </li>
                                                         <input type="hidden" id="daysfilter" value="">
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-lg-6 col-md-6 col-sm-6 donut_col-lg-7">
                                                <div class="row">
                                                   <div class="col-lg-6 col-md-6 col-sm-6 nomargin-left">
                                                      <div id="activeuserContainer" style="height: 200px; width: 70%; margin: auto;"></div>
                                                   </div>
                                                   <div class="col-lg-6 col-md-6 col-sm-6">
                                                      <div class="donut_chart_text">
                                                         <a href="javascript:void(0);" class="actvfilter" rel="all">
                                                            <h1 style="margin-bottom:10px"><span id="totalsusp"></span> Active Users</h1>
                                                         </a>
                                                         <br/>
                                                         <a href="javascript:void(0);" class="actvfilter" rel="online">
                                                            <h3 style="margin: 5px 0px!important;">
                                                               <i class="fa fa-circle" aria-hidden="true" style="color:#36465F"></i>
                                                               <span><span id="onlinecount"></span> Online</span>
                                                            </h3>
                                                         </a>
                                                         <a href="javascript:void(0);" class="actvfilter" rel="offline">
                                                            <h3 style="margin: 5px 0px!important;">
                                                               <i class="fa fa-circle" aria-hidden="true" style="color:#E4AF23"></i>
                                                               <span><span id="offlinecount"></span> Offline</span>
                                                            </h3>
                                                         </a>
                                                         <a href="javascript:void(0);" class="actvfilter" rel="suspended">
                                                            <h3 style="margin: 5px 0px!important;">
                                                               <i class="fa fa-circle" aria-hidden="true" style="color:#F4474A"></i>
                                                               <span><span id="suspcount"></span> Suspended</span>
                                                            </h3>
                                                         </a>
                                                         <input type="hidden" name="hnactiveusr" id="hnactiveusr" value="all">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 nomargin-left">
                                                <div class="mui-select">
                                                   <select class="filterstatus">
                                                      <option value="all">All</option>
                                                      <option value="online">Online</option>
                                                      <option value="offline">Offline</option>
                                                      <option value="suspended">Suspended</option>
                                                   </select>
                                                   <label>Status</label>
                                                </div>
                                             </div>
                                             <div class="col-lg-9 col-md-9 col-sm-9 text-right ">
                                                <span class="activeexcelerr" style="color:red"></span>
                                                <i class="fa fa-file-excel-o activeexcel" style="font-size: 34px; cursor:pointer;" aria-hidden="true"></i>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="table-responsive" id="search_activegrid">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="mui-tabs__pane" id="billing_reports">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row" style="margin-bottom:20px">
                                             <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                   <div class="donut_chart_active">
                                                      <ul>
                                                         <li>
                                                            <a href="javascript:void(0);" rel="all" class="textactive bdaysfiletr">
                                                               <h2><span id="btotalactive"></span> Total Active Users 
                                                                  <i class="fa fa-angle-right pull-right"></i>
                                                               </h2>
                                                            </a>
                                                         </li>
                                                         <li>
                                                            <a href="javascript:void(0);"  rel="last30" class="bdaysfiletr ">
                                                            <span id="blast30days"></span> Active in last 30 days
                                                            <i class="fa fa-angle-right pull-right" style="font-size: 24px;font-weight:300;margin-top:-1.5px"></i>
                                                            </a>
                                                         </li>
                                                         <li>
                                                            <a href="javascript:void(0);" rel="last180" class="bdaysfiletr">
                                                            <span id="blast180days"></span> Active between last 30 - 180 days
                                                            <i class="fa fa-angle-right pull-right" style="font-size: 24px;font-weight:300;margin-top:-1.5px"></i>
                                                            </a>
                                                         </li>
                                                         <li>
                                                            <a href="javascript:void(0);" rel="more180" class="bdaysfiletr">
                                                            <span id="bmore180days"></span>  Active for more than 180 days
                                                            <i class="fa fa-angle-right pull-right" style="font-size: 24px;font-weight:300;margin-top:-1.5px"></i>
                                                            </a>
                                                         </li>
                                                         <input type="hidden" id="bdaysfiletr" value="">
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-lg-6 col-md-6 col-sm-6 donut_col-lg-7">
                                                <div class="row">
                                                   <div class="col-lg-12 col-md-12 col-sm-12">
                                                      <h2>Avg. Revenue: <span id="avgr">₹ 999.00 /</span> mo</h2>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-lg-6 col-md-6 col-sm-6">
                                                      <div id="bperContainer" style="height: 160px; width: 100%; margin: auto;"></div>
                                                   </div>
                                                   <div class="col-lg-6 col-md-6 col-sm-6">
                                                      <div id="bpayContainer" style="height: 160px; width: 100%; margin: auto;"></div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="">
                                                   <select class="form-control filterpercstatus">
                                                      <option value="all">Show All</option>
                                                      <option value="25perc">25%</option>
                                                      <option value="50perc">50%</option>
                                                      <option value="75perc">75%</option>
                                                      <option value="90perc">90%</option>
                                                      <option value="100perc">100%</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 nomargin-left">
                                                <div class="">
                                                   <select class="form-control filterpaymstatus">
                                                      <option value="all">Show All</option>
                                                      <option value="p500"><₹500</option>
                                                      <option value="500p1000">₹500 - ₹1000</option>
                                                      <option value="1000p2000">₹1000 - ₹2000</option>
                                                      <option value="p2000">>₹2000</option>
                                                   </select>
                                                </div>
                                             </div>
					  </div>
                                          <div class="row">
					     <div class="col-lg-3 col-md-3 col-sm-3 ">
                                                <div class="">
                                                   <select class="form-control expirydays">
                                                      <option value="">Select Expiry Days</option>
                                                      <option value="1day">1 Days</option>
                                                      <option value="2day">2 Days</option>
                                                      <option value="3day">3 Days</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 text-right ">
                                                <span class="excelerr" style="color:red"></span>
                                                <i class="fa fa-file-excel-o billingexcel" style="font-size: 34px; cursor:pointer;" aria-hidden="true"></i>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="table-responsive" id="bsearch_activegrid">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="mui-tabs__pane" id="complaints_reports">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row" style="margin-bottom:20px">
                                             <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                   <div class="donut_chart_active" style="height:220px">
                                                      <div class="col-lg-12 col-md-12 col-sm-12" style="padding:15px">
                                                         <h2>Complaints</h2>
                                                         <div class="col-lg-12 col-md-12 col-sm-12">
                                                            <div class="row">
                                                               <span class="filter_comp donut-btn-active donut-btn" rel="all">Show All</span>
                                                               <?php foreach($complaint_type as $ckey=>$cval){?>
                                                               <span class="filter_comp donut-btn" rel="<?php echo $ckey; ?>"><?php echo $cval ;?></span>
                                                               <?php } ?>
                                                            </div>
                                                            <input type="hidden" value="all" class="filter_compchange">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-lg-6 col-md-6 col-sm-6 donut_col-lg-7">
                                                <div class="row">
                                                   <div class="col-lg-12 col-md-12 col-sm-12">
                                                      <h2>Open: <span id="opent" style="color:#f00f64;"></span> </h2>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 ">
                                                      <div id="compuserContainer" style="height: 160px; width:100%; margin: auto;"></div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 nomargin-left">
                                                <div >
                                                   <select class="form-control filter_complaint" onchange="filter_complaint(this.value);">
                                                      <option value="all">All</option>
                                                      <option value="open">Open</option>
                                                      <option value="close">Close</option>
                                                      <option value="in_progress">In Progress</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-lg-9 col-md-9 col-sm-9 text-right ">
                                                <span class="comperr" style="color:red"></span>
                                                <i class="fa fa-file-excel-o compexcel" style="font-size: 34px; cursor:pointer;" aria-hidden="true"></i>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="table-responsive" id="search_compgrid">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="mui-tabs__pane" id="plan_top_ups">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row" style="margin-bottom:20px">
                                             <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                   <div class="donut_chart_active">
                                                      <ul>
                                                         <li>
                                                            <a href="javascript:void(0);"  class="pdaysfiletr textactive" rel="all">
                                                               <h2><span id="ptotalactive"></span> Total Active Users 
                                                                  <i class="fa fa-angle-right pull-right"></i>
                                                               </h2>
                                                            </a>
                                                         </li>
                                                         <li>
                                                            <a href="javascript:void(0);"  rel="last30" class="pdaysfiletr ">
                                                            <span id="plast30days"></span> Active in last 30 days
                                                            <i class="fa fa-angle-right pull-right" style="font-size: 24px;font-weight:300;margin-top:-1.5px"></i>
                                                            </a>
                                                         </li>
                                                         <li>
                                                            <a href="javascript:void(0);" rel="last180" class="pdaysfiletr">
                                                            <span id="plast180days"></span> Active between last 30 - 180 days
                                                            <i class="fa fa-angle-right pull-right" style="font-size: 24px;font-weight:300;margin-top:-1.5px"></i>
                                                            </a>
                                                         </li>
                                                         <li>
                                                            <a href="javascript:void(0);" rel="more180" class="pdaysfiletr">
                                                            <span id="pmore180days"></span>  Active for more than 180 days
                                                            <i class="fa fa-angle-right pull-right" style="font-size: 24px;font-weight:300;margin-top:-1.5px"></i>
                                                            </a>
                                                         </li>
                                                         <input type="hidden" id="pdaysfilter" value="all">
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-lg-6 col-md-6 col-sm-6 donut_col-lg-7">
                                                <div class="row">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 ">
                                                      <div id="planuserContainer" style="height: 200px; width: 100%; margin: auto;"></div>
                                                   </div>
                                                   <div class="col-lg-6 col-md-6 col-sm-6">
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 nomargin-left">
                                                <div class="">
                                                   <select class="form-control filterplant" name="filterplant">
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-lg-9 col-md-9 col-sm-9 text-right ">
                                                <span class="plantopuperr" style="color:red"></span>
                                                <i class="fa fa-file-excel-o plantopupexcel" style="font-size: 34px; cursor:pointer;" aria-hidden="true"></i>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="table-responsive" id="psearch_activegrid">
                                             </div>
                                          </div>
                                       </div>
                                    </div>

				    <div class="mui-tabs__pane" id="custom_reporting">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <div class="row">
					     <div class="col-lg-8 col-md-8 col-sm-8">
						<div class="row">
						   <form method="post" id="customreportform" action="" onsubmit="getcustom_reportsheet(); return false;">
						      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							 <select name="customreport_type" class="form-control" style="margin-bottom:0px; box-shadow:inset 0 -2px 0 #aaa79c;" required>
							    <option value="">Select Report Type</option>
							    <option value="pendingpayment_sheet">Pending Payments</option>							    
							    <option value="addtowallet_sheet">Payment received</option>
							    <option value="newusers_sheet">New users added</option>
							    <option value="suspendedusers_sheet">Suspended Users</option>
							    <option value="terminatedusers_sheet">Terminated Users</option>
							    <option value="onoffusers_sheet">Online/Offline Users</option>
							    <option value="usersinvoice_sheet">User Invoives</option>
							    <option value="onlinepayments_sheet">Online Payments</option>
							    <option value="userstickets_sheet">Tickets & Complaints</option>
							    <option value="smsreports_sheet">SMS Reports</option>
							 </select>
						      </div>
						      <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
							 <div class="form-group dtrnge" style="margin-bottom:0px">
							    <div class="input-group">
							       <div class="input-group-addon">
								  <i class="fa fa-calendar fa-2x" aria-hidden="true"></i>
							       </div>
							       <input type="text" class="form-control" name="customreport_daterange" value="dd.mm.yyyy-dd.mm.yyyy"/>
							    </div>
							 </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							 <input type="submit" class="mui-btn mui-btn--small mui-btn--accent" value="submit" />
						      </div>
						   </form>
						</div>
					     </div>
					     <div class="col-lg-4 col-md-4 col-sm-4 pull-right">
						<button class="mui-btn mui-btn--small mui-btn--accent pull-right" style="background-color:#1c2a39;" onclick="custom_rptexport()">Export to Excel</button>
					     </div>
					  </div>
					  <div class="row">
					     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<h2 id="reportlist_total">&nbsp;</h2>
					     </div>
					     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">&nbsp;</div>
					     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 searchuseropt hide">
						<input type="text" class="serch-form-control form-control" placeholder="Search with UID" onFocus="this.placeholder=''" onBlur="this.placeholder='Search with UID'" style="box-shadow:none;height: 40px;" id="search_customfilteruser" />
					     </div>
					  </div>
					  <div class="row">
					     <div class="table-responsive">
						<table class="table table-striped" id="reportlist">
						   
						</table>
					     </div>
					  </div>
					  <div class="loadmore_loader hide">
					     <img src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
					  </div>
					  <input type='hidden' id="crplimit" value="" />
					  <input type='hidden' id="crpoffset" value="" />
					  <input type='hidden' id="crploadmore" value="1" />
					  <input type='hidden' id="seldaterange" value="" />
					  <input type='hidden' id="selrptype" value="" />
				       </div>
				    </div>
				 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal" tabindex="-1" id="paymentalertModal" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Send Payment Alert</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
	       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		  <div class="mui-textfield">
		     <textarea name="paymentalerttxt" required></textarea>
		     <input type="hidden" name="uuid_payalert" value="" />
		     <label>Text SMS<sup>*</sup></label>
		  </div>
	       </div>
	      </div>
            </div>
            <div class="modal-footer">
	      <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
	       <button type="submit" class="mui-btn mui-btn--large mui-btn--accent sendpayalertbtn">Send</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="alertModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		  <h4 class="modal-title">
		     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>ALERT</strong>
		  </h4>
	       </div>
	       <div class="modal-body" style="padding-bottom:5px">
		  <p id="alertMsgTxt"></p>  
	       </div>
	       <div class="modal-footer" style="text-align: right">
		  <div class="form-group" style="overflow:hidden;">
		     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right">
			      <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
			   </div>
			</div>
		     </div>
		  </div>
	       </div>
	    </div>
         </div>
      </div>
      <!-- /Start your project here-->
      <!-- SCRIPTS -->
      <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-3.1.1.min.js"></script>
      <!-- Bootstrap core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
      <!-- MDB core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/mui.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/canvasjs.min.js"></script>
      <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/report.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/crbnMenu.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/dataTables.bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/moment-with-locales.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap-material-datetimepicker.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/datepicker/daterangepicker.js"></script>
      <script type="text/javascript">
         var paneIds = ['pane-api-1', 'pane-api-2','pane-api-3','pane-api-4'],
         currPos = 0;
         
         function activateNext() {
         // increment id
         currPos = (currPos + 1) % paneIds.length;
         
         // activate tab
         mui.tabs.activate(paneIds[currPos]);
         }
      </script>
      <script type="text/javascript">
         window.onload = function () {
             
              CanvasJS.addColorSet("brownShades",
                         [//colorSet Array
         
                         "#36465F",
                         "#E4AF23",
                         "#F4474A"
                                       
                         ]);
         	var chart = new CanvasJS.Chart("chartContainer",
         	{
                      colorSet: "brownShades",
         		
         		data: [
         		{
         			/*type: "pie",
                                 toolTipContent: " #percent %",
                                 indexLabel: "#percent%",*/
                                  //showInLegend: true,
                                  type: "doughnut",
         			indexLabelFontFamily: " 'Open Sans', sans-serif;",       
         			indexLabelFontSize: 7,
         			indexLabelFontWeight: "600",
         			//startAngle:0,
         			indexLabelFontColor: "white",       
         			indexLabelLineColor: "darkgrey", 
         			indexLabelPlacement: "inside", 
         			toolTipContent: "#percent %",
         			//showInLegend: true,
         			indexLabel: "#percent%", 
         			dataPoints: [
                                     { y: <?php echo $perspective['count']['leadcount'] ?>},
         				{ y: <?php echo $perspective['count']['enquirycount'] ?>},
         				{ y: <?php echo $perspective['count']['inactivecount'] ?>}
         				
         			]
         		}
         		]
         	});
         	chart.render();
         }
         	
      </script>
      <script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
         
         
      </script>
      <script>
         $(document).ready(function() {
             
             /* $('.loading').removeClass('hide');
         setTimeout(function (){
         $('.loading').addClass('hide');
         
          },1000);*/
             
           $('#persp_table').DataTable({
         
         "bLengthChange": false,//for num of record in one page
         "bFilter": false,// for search box
         "bInfo": false,// for num of record show in page hide
         /*"aoColumnDefs": [{  // column shorting by column number
         'bSortable': false,
         'aTargets': [ 1,2,3,4,5,6,7,8]
         }],*/
         "bSort": false ,// column shorting
	 "pageLength": 50,
         drawCallback: function(settings) {
         var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
         pagination.toggle(this.api().page.info().pages > 1);
         }
           });
           
          
         } );
      </script>
      <script type="text/javascript">
	 $(function() {
	    $('#reportlist').html('');
	    $('#seldaterange').val('');
	    $('#selrptype').val('');
	    $('#reportlist_total').html('&nbsp;');
	    $('.searchuseropt').addClass('hide');
	    $('#search_customfilteruser').val('');
	    
	    $('input[name="customreport_daterange"]').daterangepicker({
	       locale: {
		  format: 'DD.MM.YYYY',
	       },
	       startDate: moment().startOf('month').format('DD.MM.YYYY'),
	       maxDate: moment()
	    });
	 });
	 
	 $('select[name="customreport_type"]').change(function(){
	    var optselected = this.value;
	    if (optselected == 'pendingpayment_sheet') {
	       $('input[name="customreport_daterange"]').prop('disabled', true);
	       $('.dtrnge').addClass('hide');
	    }else{
	       $('input[name="customreport_daterange"]').prop('disabled', false);
	       $('.dtrnge').removeClass('hide');
	    }
	 });
	 
	 $('body').on('click', '#customtab', function(){
	    $('#reportlist').html('');
	    $('#seldaterange').val('');
	    $('#selrptype').val('');
	    $('#reportlist_total').html('&nbsp;');
	    $('.searchuseropt').addClass('hide');
	    $('#search_customfilteruser').val('');
	    $('select[name="customreport_type"] option[value=""]').prop("selected", true);
	    $('input[name="customreport_daterange"]').prop('disabled', false);
	    $('.dtrnge').removeClass('hide');
	    $('.loadmore_loader').addClass('hide');
	    $('input[name="customreport_daterange"]').daterangepicker({
	       locale: {
		  format: 'DD.MM.YYYY',
	       },
	       startDate: moment().startOf('month').format('DD.MM.YYYY'),
	       maxDate: moment()
	    });
	 });
	 
	 function getcustom_reportsheet() {
	    $('#reportlist').html('<img src="<?php echo base_url() ?>assets/images/loader.svg"/>');
	    var formdata = $('#customreportform').serialize();
	    var isp_uid= $('.franchise').find(":selected").val();
	    $('#crplimit').val('50');
	    $('#crpoffset').val('0');
	    $('#crploadmore').val('1');
	    
	    var seldaterange = $('input[name="customreport_daterange"]').val();
	    var selrptype = $('select[name="customreport_type"] option:selected').val();
	    $('#seldaterange').val(seldaterange);
	    $('#selrptype').val(selrptype);
	    $('.searchuseropt').removeClass('hide');
	    $('#search_customfilteruser').val('');
	    var search_user = $('#search_customfilteruser').val();
	    
	    var limit = $('#crplimit').val();
	    var offset = $('#crpoffset').val();
	    $.ajax({
	       url: base_url+'report/custom_reportsheet',
	       type: 'POST',
	       data: formdata+'&isp_uid='+isp_uid+'&limit='+limit+'&offset='+offset+'&search_user='+search_user,
	       dataType: 'json',
	       success: function(result){
		  $('#reportlist_total').html('Total Records: '+result.reportlist_total);
		  $('#reportlist').html(result.reportlist);
		  $('#crpoffset').val(result.offset);
		  $('#crploadmore').val(result.loadmore);
	       }
	    });
	 }
	 
	 $(document).ready(function() {
	    $(window).scroll(function(){
	       var loaded = $('#crploadmore').val();	       
	       if ($(window).scrollTop() == $(document).height() - $(window).height() && loaded != 0){
		  
		  $('.loadmore_loader').removeClass('hide');
		  var formdata = $('#customreportform').serialize();
		  var isp_uid= $('.franchise').find(":selected").val();
		  var limit = $('#crplimit').val();
		  var offset = $('#crpoffset').val();
		  var search_user = $('#search_customfilteruser').val();
		  $.ajax({
		     url: base_url+'report/custom_reportsheet',
		     type: 'POST',
		     data: formdata+'&isp_uid='+isp_uid+'&limit='+limit+'&offset='+offset+'&search_user='+search_user,
		     dataType: 'json',
		     success: function(result){
			$('.loadmore_loader').addClass('hide');
			$('#reportlist_total').html('Total Records: '+result.reportlist_total);
			$('#reportlist').append(result.reportlist);
			$('#crpoffset').val(result.offset);
			$('#crploadmore').val(result.loadmore);
		     }
		  });
	       }
	    });
	 });
	 
	 function custom_rptexport() {
	    var isp_uid= $('.franchise').find(":selected").val();
	    var seldaterange = $('#seldaterange').val();
	    var selrptype = $('#selrptype').val();
	    if ((seldaterange != '') && (selrptype != '')) {
	       $('.loading').removeClass('hide');
	       $.ajax({
		  url: base_url+'report/exportcustom_reportsheet',
		  type: 'POST',
		  data: 'customreport_daterange='+seldaterange+'&customreport_type='+selrptype+'&isp_uid='+isp_uid,
		  dataType: 'json',
		  success: function(result){
		     $('.loading').addClass('hide');
		     window.location = base_url+"reports/"+isp_uid+"_customreport.xlsx";
		  }
	       });
	    }else{
	       alert("Please select filters & submit before export.");
	    }
	 }
	 
	 $(document).on('click', '.send_pendingamt_alert', function() {
	    var smstosend = $(this).next('span.smstxt').text();
	    var uuid_payalert = $(this).data('uuid');
	    $('input[name="uuid_payalert"]').val(uuid_payalert);
	    $('textarea[name="paymentalerttxt"]').val(smstosend);
	    $('#paymentalertModal').modal('show');
	 });
	 
	 $(document).on('click', '.sendpayalertbtn', function(){
	    var uuid = $('input[name="uuid_payalert"]').val();
	    var smstosend = $('textarea[name="paymentalerttxt"]').val();
	    
	    if (smstosend != '') {
	       $('#paymentalertModal').modal('hide');
	       $('.loading').removeClass('hide');
	       $.ajax({
		  url: base_url+'report/send_payment_duealert',
		  type: 'POST',
		  data: 'uuid='+uuid+'&smstosend='+smstosend,
		  success: function(result){
		     $('.loading').addClass('hide');
		     $('#alertModal').modal('show');
		     $('#alertMsgTxt').html('Payment due message sent successfully');
		  }
	       });
	    }
	 });
	 setTimeout(function (){
	    $('body').on('keyup', '#search_customfilteruser', function(){
	       var search_user = $('#search_customfilteruser').val();
	       
	       var isp_uid= $('.franchise').find(":selected").val();
	       var seldaterange = $('#seldaterange').val();
	       var selrptype = $('#selrptype').val();
	       if ((seldaterange != '') && (selrptype != '') && (search_user != '')) {
		  $('#crplimit').val('50');
		  $('#crpoffset').val('0');
		  $('#crploadmore').val('1');
	    
		  var limit = $('#crplimit').val();
		  var offset = $('#crpoffset').val();
		  $('#reportlist').html('<img src="<?php echo base_url() ?>assets/images/loader.svg"/>');
		  $.ajax({
		     url: base_url+'report/custom_reportsheet',
		     type: 'POST',
		     dataType: 'json',
		     data: 'customreport_daterange='+seldaterange+'&customreport_type='+selrptype+'&isp_uid='+isp_uid+'&search_user='+search_user+'&limit='+limit+'&offset='+offset,
		     success: function(result){
			$('#reportlist_total').html('Total Records: '+result.reportlist_total);
			$('#reportlist').html(result.reportlist);
			$('#crpoffset').val(result.offset);
			$('#crploadmore').val(result.loadmore);
			$('.searchuseropt').removeClass('hide');
			$('#search_customfilteruser').val(search_user);
		     }
		  });
	       }else{
		  getcustom_reportsheet();
	       }
	    });
	 },1000);
      </script>
   </body>
</html>

