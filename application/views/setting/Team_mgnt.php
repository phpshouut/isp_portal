<?php $this->load->view('includes/header');?>
<!-- Start your project here-->
<div class="container-fluid">
    <div class="row">
        <div class="wapper">
            <div id="sidedrawer" class="mui--no-user-select">
                <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                      <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                     <img src="<?php echo $img ?>" class="img-responsive" />
                     </span>
                </div>
                 <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
                 $this->view('left_nav',$data); ?>

            </div>
            <header id="header">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">TEAM USERS </a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <!--a href="#">
                                        <div class="mui-btn---small">
                                            <img src="<?php echo base_url() ?>assets/images/close.svg"/>
                                        </div>
                                    </a-->
                                </li>
                               <?php $this->load->view('includes/global_setting',$data); ?>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            <div id="content-wrapper">
                <div class="mui--appbar-height"></div>
                <div class="mui-container-fluid" id="right-container-fluid">
                    <div class="add_user">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <ul class="mui-tabs__bar plan_mui-tabs__bar">
                                        <li class="mui--is-active">
                                            <a data-mui-toggle="tab"
                                               data-mui-controls="Create_Department">
                                                Create Department
                                            </a>
                                        </li>
                                        <li class="disabled">
                                            <a data-mui-toggle="tab" data-mui-controls="create_team_user">
                                                Create Team User
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="mui--appbar-height"></div>
                                <div class="mui-tabs__pane mui--is-active" id="Create_Department">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="table-responsive">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <h2>Department</h2>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <a data-toggle="modal" data-target="#department" class="mui-btn mui-btn--small mui-btn--accent pull-right">
                                                                + Add Department
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr class="active">
                                                        <th>&nbsp;</th>
                                                        <th colspan="2">DEPARTMENT NAME</th>
                                                        <th colspan="1">ACCOUNTS</th>
                                                        <th colspan="2" class="mui--text-center">ACTIONS</th>
                                                        <th>&nbsp;</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>1.</td>
                                                        <td colspan="2">Lorem Ipsum Dolor</td>
                                                        <td>99.99.99.99</td>
                                                        <td>
                                                            <a href="#">Edit</a>
                                                        </td>
                                                        <td><a href="#">Status</a></td>
                                                        <td><a href="#">Delete</a></td>
                                                    </tr>

                                                    <tr>
                                                        <td>2.</td>
                                                        <td colspan="2">Lorem Ipsum Dolor</td>
                                                        <td>99.99.99.99</td>
                                                        <td>
                                                            <a href="#">Edit</a>
                                                        </td>
                                                        <td><a href="#">Status</a></td>
                                                        <td><a href="#">Delete</a></td>
                                                    </tr>

                                                    <tr>
                                                        <td>3.</td>
                                                        <td colspan="2">Lorem Ipsum Dolor</td>
                                                        <td>99.99.99.99</td>
                                                        <td>
                                                            <a href="#">Edit</a>
                                                        </td>
                                                        <td><a href="#">Status</a></td>
                                                        <td><a href="#">Delete</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>4.</td>
                                                        <td colspan="2">Lorem Ipsum Dolor</td>
                                                        <td>99.99.99.99</td>
                                                        <td>
                                                            <a href="#">Edit</a>
                                                        </td>
                                                        <td><a href="#">Status</a></td>
                                                        <td><a href="#">Delete</a></td>
                                                    </tr>

                                                    <tr>
                                                        <td>5.</td>
                                                        <td colspan="2">Lorem Ipsum Dolor</td>
                                                        <td>99.99.99.99</td>
                                                        <td>
                                                            <a href="#">Edit</a>
                                                        </td>
                                                        <td><a href="#">Status</a></td>
                                                        <td><a href="#">Delete</a></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mui-tabs__pane" id="create_team_user">
                                    <form>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br>
                                            <div class="row">
                                                <h2>Team User</h2>
                                                <div class="form-group">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                <div class="mui-select">
                                                                    <select>
                                                                        <option>Option 1</option>
                                                                        <option>Option 2</option>
                                                                        <option>Option 3</option>
                                                                        <option>Option 4</option>
                                                                        <option>Option 5</option>
                                                                    </select>
                                                                    <label>Select Department<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="text">
                                                                    <label>User Name<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="text">
                                                                    <label>First Name<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="text">
                                                                    <label>Middle Name<sup>*</sup></label>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="text">
                                                                    <label>Last Name<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="email">
                                                                    <label>Email<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="tel">
                                                                    <label>Mobile Number <sup>*</sup></label>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-textfield mui-textfield--float-label">
                                                                    <input type="password">
                                                                    <label>Password <sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                <div class="mui-select">
                                                                    <select>
                                                                        <option>Option 1</option>
                                                                        <option>Option 2</option>
                                                                        <option>Option 3</option>
                                                                        <option>Option 4</option>
                                                                        <option>Option 5</option>
                                                                    </select>
                                                                    <label>Select State<sup>*</sup></label>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-select">
                                                                    <select>
                                                                        <option>Option 1</option>
                                                                        <option>Option 2</option>
                                                                        <option>Option 3</option>
                                                                        <option>Option 4</option>
                                                                        <option>Option 5</option>
                                                                    </select>
                                                                    <label>Select City<sup>*</sup></label>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <div class="mui-select">
                                                                    <select>
                                                                        <option>Option 1</option>
                                                                        <option>Option 2</option>
                                                                        <option>Option 3</option>
                                                                        <option>Option 4</option>
                                                                        <option>Option 5</option>
                                                                    </select>
                                                                    <label>Select Zone<sup>*</sup></label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row" style="margin-bottom:15px">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="row">
                                                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 nopadding-left">
                                                                        <h4 class="nomargin">Super Admin</h4>
                                                                    </div>
                                                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 nopadding-left">
                                                                        <label class="radio-inline">
                                                                            <input type="radio" name="admin_yes" id="optionsRadios1" value="yes"> Yes
                                                                        </label>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" name="admin_yes" id="optionsRadios2" value="no"> No
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row" style="margin-bottom:15px">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="row">
                                                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 nopadding-left">
                                                                        <h4 class="nomargin">Ticket Sorter</h4>
                                                                    </div>
                                                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 nopadding-left">
                                                                        <label class="radio-inline">
                                                                            <input type="radio" name="optionsRadios" id="ticketyes" value="option1"> Yes
                                                                        </label>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" name="optionsRadios" id="ticketno" value="option2"> No
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:25px">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                <a  class="mui-btn mui-btn--accent btn-lg btn-block">NEXT</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--- De-Activate User Modal --->
<div class="modal fade" id="department" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom: 0px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Department</strong></h4>
            </div>
            <div class="modal-body" style="padding-bottom:5px">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form>
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                <strong>Department Name</strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="mui-textfield mui-textfield--float-label">
                                    <input type="text">
                                    <label>Enter Department Name</label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer" style="text-align:right">
                <button type="button" class="mui-btn mui-btn--small mui-btn--accent">SUBMIT</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var height = $(window).height();
        $('#main_div').css('height', height);
        $('#right-container-fluid').css('height', height);

        // on user type change change page(form)
        $('input[type=radio][name=user_type_radio]').change(function() {
            if (this.value == 'lead' || this.value == 'enquiry') {
                $("#user_type_lead_enquiry").show();
                $("#user_type_customer").hide();
            }
            else if (this.value == 'customer') {
                $("#user_type_customer").show();
                $("#user_type_lead_enquiry").hide();
            }
        });

    });
</script>
 <script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
         //setting_menu_ul
$(document).ready(function(){
//class="active" style="display: block;"
$("#setting_menu_ul").css("display","block");
$("#team_menu").addClass("menu_active");

});
      </script>
</body>
</html>
