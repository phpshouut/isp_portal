

<link href="<?php echo base_url() ?>assets/css/image_crop/demo.css" rel="stylesheet">
<?php $this->load->view('includes/header');?>
<!-- Start your project here-->
<div class="container-fluid">
   <div class="loading hide" >
      <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
   </div>
   <div class="row">
      <div class="wapper">
         <div id="sidedrawer" class="mui--no-user-select">
            <div id="sidedrawer-brand" class="mui--appbar-line-height">
               <span class="mui--text-title">
               <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
               <img src="<?php echo $img ?>" class="img-responsive" />
               </span>
            </div>
            <?php
               $data['navperm']=$this->plan_model->leftnav_permission();
               
               $this->view('left_nav',$data); ?>
         </div>
         <header id="header">
            <nav class="navbar navbar-default">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <a class="navbar-brand" href="#">ISP DETAIL</a>
                  </div>
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <?php $this->view('includes/global_setting',$data);  ?>
                     </ul>
                  </div>
               </div>
            </nav>
         </header>
         <div id="content-wrapper">
            <div class="mui--appbar-height"></div>
            <div class="mui-container-fluid" id="right-container-fluid">
               <div class="add_user">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                           <ul class="mui-tabs__bar plan_mui-tabs__bar">
                              <li class="mui--is-active">
                                 <a data-mui-toggle="tab" data-mui-controls="Top-Up-1">
                                 ISP SETUP
                                 </a>
                              </li>
                              <li>
                                 <a data-mui-toggle="tab" data-mui-controls="Top-Up-4">
                                 ISP BANKING
                                 </a>
                              </li>
                              <li>
                                 <a data-mui-toggle="tab" data-mui-controls="Top-Up-2">
                                 ABOUT US
                                 </a>
                              </li>
                              <li>
                                 <a data-mui-toggle="tab" data-mui-controls="Top-Up-3">
                                 SERVICES
                                 </a>
                              </li>
                              <li>
                                 <a data-mui-toggle="tab" data-mui-controls="Top-Up-5">
                                 INVOICE SETUP
                                 </a>
                              </li>
                              <li>
                                 <a data-mui-toggle="tab" data-mui-controls="Top-Up-6">
                                 APK SETUP
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                           <div class="mui--appbar-height"></div>
                           <div class="mui-tabs__pane mui--is-active" id="Top-Up-1">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png";
                                    $logorequired=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?"":"required";?>
                                 <div class="row">
                                    <form action="<?php echo base_url()?>setting/add_ispdetail" method="post" enctype='multipart/form-data' id="add_ispdetail" >
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-top:20px">
                                                <div class="dropzone" data-width="900" data-ajax="false" data-resize="true" data-originalsize="false" data-height="300" style="width: 100%; height:100px;">
                                                   <input type="file" name="isp_logo" <?php echo $logorequired;?> />
                                                </div>
                                                <br/>
                                                <span class="search_label" style="margin:10px 0px; padding:10px 0px ">Min image size 600x200. Image proportion 3:1 horizontal. Image format .png with transparent background.</span>            
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <img id="preview2" style="height:100px; width:100%;" class="profile-user-img img-responsive " src="<?php echo $img;?>" style = "margin-bottom:10px">
                                             </div>
                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"> <span style="color: red" id="provider_logo_error"></span></div>
                                          </div>
                                       </div>
                                       <!-- <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-top:20px">
                                              <input type="file" id="file" <?php echo (isset($ro))?"disabled":"";?> name="file" class="hide" <?php echo (isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?"":"required"; ?>/>
                                          <span id="but" <?php echo (isset($ro))?"disabled":"";?> class="mui-btn mui-btn--small mui-btn--primary" style="height:35px; padding: 0px 20px; background-color:#36465f; font-weight:600;line-height: 35.6px;">
                                          Upload Image
                                          </span><br/>
                                          <span class="search_label" style="margin:10px 0px; padding:10px 0px ">Min image size 600x200. Image proportion 3:1 horizontal. Image format .png with transparent background.</span>            
                                              </div>
                                              <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                          <img id="preview2" style="height:100px; width:300px;" class="profile-user-img img-responsive " src="<?php echo $img;?>" style = "margin-bottom:10px">
                                          </div>
                                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"> <span style="color: red" id="provider_logo_error"></span></div>
                                              
                                          
                                          
                                          </div>
                                          
                                          </div>-->
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="ispname" value="<?php echo (isset($ispbaseinfo[0]->isp_name) && $ispbaseinfo[0]->isp_name!='')?$ispbaseinfo[0]->isp_name:""; ?>" disabled required>
                                                   <label> Isp name<sup>*</sup></label>
                                                </div>
                                             </div>
                                             <input type="hidden" name="ispname" value="<?php echo (isset($ispbaseinfo[0]->isp_name) && $ispbaseinfo[0]->isp_name!='')?$ispbaseinfo[0]->isp_name:""; ?>" id="isp_name">
                                             <input type="hidden" name="company_name" value="<?php echo (isset($ispbaseinfo[0]->legal_name) && $ispbaseinfo[0]->legal_name!='')?$ispbaseinfo[0]->legal_name:""; ?>" id="company_name">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="company_name" value="<?php echo (isset($ispbaseinfo[0]->legal_name) && $ispbaseinfo[0]->legal_name!='')?$ispbaseinfo[0]->legal_name:""; ?>" disabled required>
                                                   <label> Registered Company Name<sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="tin_number" value="<?php echo (isset($ispdetail->tin_number) && $ispdetail->tin_number!='')?$ispdetail->tin_number:""; ?>">
                                                   <label> TIN Number</label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="service_tax_number" value="<?php echo (isset($ispdetail->service_tax_no) && $ispdetail->service_tax_no!='')?$ispdetail->service_tax_no:"" ?>">
                                                   <label>  Service Tax Number</label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
					     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="cin_number" value="<?php echo (isset($ispdetail->cin_number) && $ispdetail->cin_number!='')?$ispdetail->cin_number:""; ?>">
                                                   <label> CIN Number</label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> name="pancard" value="<?php echo (isset($ispdetail->pancard) && $ispdetail->pancard!='')?$ispdetail->pancard:""; ?>">
                                                   <label>  Pancard</label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="gstnumber" value="<?php echo (isset($ispdetail->gst_no) && $ispdetail->gst_no!='')?$ispdetail->gst_no:""; ?>">
                                                   <label> GST Number</label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="hsn_number" value="<?php echo (isset($ispdetail->hsn_number) && $ispdetail->hsn_number!='')?$ispdetail->hsn_number:""; ?>">
                                                   <label> HSN Number</label>
                                                </div>
                                             </div>
                                             
                                          </div>
                                       </div>
				       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="sac_number" value="<?php echo (isset($ispdetail->sac_number) && $ispdetail->sac_number!='')?$ispdetail->sac_number:""; ?>">
                                                   <label> SAC Number</label>
                                                </div>
                                             </div>
					  </div>
				       </div>
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="address_1" value="<?php echo (isset($ispdetail->address1) && $ispdetail->address1!='')?$ispdetail->address1:""; ?>">
                                                   <label> Address 1</label>
                                                </div>
                                             </div>
					    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="address_2" value="<?php echo (isset($ispdetail->address2) && $ispdetail->address2!='')?$ispdetail->address2:""; ?>">
                                                   <label> Address 2</label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?>  maxlength="6" pattern="[0-9]{6}" name="pincode" value="<?php echo (isset($ispdetail->pincode) && $ispdetail->pincode!='')?$ispdetail->pincode:""; ?>">
                                                   <label>  Pin Code</label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="city" value="<?php echo (isset($ispdetail->city) && $ispdetail->city!='')?$ispdetail->city:""; ?>">
                                                   <label> City</label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
				       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="state" value="<?php echo (isset($ispdetail->state) && $ispdetail->state!='')?$ispdetail->state:""; ?>">
                                                   <label> State</label>
                                                </div>
                                             </div>
					  </div>
				       </div>
				       
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="tel" <?php echo (isset($ro))?"disabled":"";?> class="" name="help_number1" required value="<?php echo (isset($ispdetail->help_number1) && $ispdetail->help_number1!='')?$ispdetail->help_number1:""; ?>"> 
                                                   <label> Helpline Number 1<sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="tel" <?php echo (isset($ro))?"disabled":"";?> class="" name="help_number2" value="<?php echo (isset($ispdetail->help_number2) && $ispdetail->help_number2!='')?$ispdetail->help_number2:""; ?>">
                                                   <label> Helpline Number 2</label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="tel" <?php echo (isset($ro))?"disabled":"";?> class="" name="help_number3" value="<?php echo (isset($ispdetail->help_number3) && $ispdetail->help_number3!='')?$ispdetail->help_number3:""; ?>">
                                                   <label> Helpline Number 3</label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="email" <?php echo (isset($ro))?"disabled":"";?> class="" name="supp_email" value="<?php echo (isset($ispdetail->support_email) && $ispdetail->support_email!='')?$ispdetail->support_email:""; ?>" required>
                                                   <label> Support Email<sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="tel" <?php echo (isset($ro))?"disabled":"";?> class="" name="sale_email"  value="<?php echo (isset($ispdetail->sales_email) && $ispdetail->sales_email!='')?$ispdetail->sales_email:""; ?>" required>
                                                   <label> Sales Email<sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="web"  value="<?php echo (isset($ispdetail->website) && $ispdetail->website!='')?$ispdetail->website:""; ?>" required>
                                                   <label>website<sup>*</sup></label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <?php $img=(isset($ispdetail->logo_signature) && $ispdetail->logo_signature!='')?base_url()."ispmedia/logo/".$ispdetail->logo_signature:base_url()."assets/images/decibel.png";
                                          $signrequired=(isset($ispdetail->logo_signature) && $ispdetail->logo_signature!='')?"":"required";
                                          ?>
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-top:20px">
                                                <div class="dropzone" data-width="600" data-ajax="false" data-resize="true" data-originalsize="false" data-height="200" style="width: 100%; height:100px;">
                                                   <input type="file" name="isp_signature" <?php echo $signrequired;?> />
                                                </div>
                                                <br/>
                                                <span class="search_label" style="margin:10px 0px; padding:10px 0px ">Min image size 200x100. Image proportion 2:1 horizontal. Image format .png with transparent background.</span>            
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <img id="preview3" style="height:100px; width:100%;" class="profile-user-img img-responsive " src="<?php echo $img;?>" style = "margin-bottom:10px">
                                             </div>
                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"> <span style="color: red" id="sign_logo_error"></span></div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" <?php echo (isset($ro))?"disabled":"";?>  id="geolocation" name="geoaddr" value="<?php echo (isset($ispdetail->geoaddress) && $ispdetail->geoaddress!='')?$ispdetail->geoaddress:""; ?>" required placeholder="">
                                                   <label>Geolocation</label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text"  id="placeid"  value="<?php echo (isset($ispdetail->place_id) && $ispdetail->place_id!='')?$ispdetail->place_id:""; ?>" name="placeid"  readonly>
                                                   <label>Placeid</label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text"  id="lat" value="<?php echo (isset($ispdetail->lat) && $ispdetail->lat!='')?$ispdetail->lat:""; ?>" name="lat" readonly>
                                                   <label>Lat</label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" id="long" name="long" value="<?php echo (isset($ispdetail->long) && $ispdetail->long!='')?$ispdetail->long:""; ?>"  readonly>
                                                   <label>Long</label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <input type="submit" <?php echo (isset($ro))?"disabled":"";?> class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE" >
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                           <div class="mui-tabs__pane" id="Top-Up-2">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <form action="<?php echo base_url()?>setting/add_aboutus" method="post" id="add_aboutus" >
                                    <div class="row">
                                       <input type="hidden" name="userid" value="<?php echo (isset($ispdetail->id) && $ispdetail->id!='')?$ispdetail->id:""; ?>"> 
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                             <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                <textarea required <?php echo (isset($ro))?"disabled":"";?> id="editor1" name="about_us">
                                                <?php echo (isset($ispdetail->about_us) && $ispdetail->about_us!='')?$ispdetail->about_us:""; ?>
                                                </textarea> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                             <input type="submit" <?php echo (isset($ro))?"disabled":"";?> <?php echo (isset($ispdetail->id) && $ispdetail->id!='')?"":"disabled"; ?> class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE" >
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                           <div class="mui-tabs__pane" id="Top-Up-3">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <form  action="<?php echo base_url()?>setting/add_services" method="post" id="add_services" autocomplete="off" >
                                    <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                             <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                <textarea required <?php echo (isset($ro))?"disabled":"";?> <?php echo (isset($ispdetail->id) && $ispdetail->id!='')?"":"disabled"; ?> id="editor2" name="services">
                                                <?php echo (isset($ispdetail->services) && $ispdetail->services!='')?$ispdetail->services:""; ?>
                                                </textarea> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                             <input type="submit" <?php echo (isset($ro))?"disabled":"";?> <?php echo (isset($ispdetail->id) && $ispdetail->id!='')?"":"disabled"; ?> class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE" >
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
			   <div class="mui-tabs__pane" id="Top-Up-4">
			    <form action="" method="post" id="add_ispbanking" autocomplete="off" onsubmit="add_ispbanking(); return false;">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
					     <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
						<input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="isp_bankname" value="<?php echo (isset($ispdetail->bank_name) && $ispdetail->bank_name!='')?$ispdetail->bank_name:""; ?>" required>
						<label> Bank Name<sup>*</sup></label>
					     </div>
					  </div>
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
					     <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
						<input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="isp_bankaddress" value="<?php echo (isset($ispdetail->branch_address) && $ispdetail->branch_address!='')?$ispdetail->branch_address:""; ?>" required>
						<label> Branch Address<sup>*</sup></label>
					     </div>
					  </div>
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
					     <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
						<input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="isp_bankifsc_code" value="<?php echo (isset($ispdetail->ifsc_code) && $ispdetail->ifsc_code!='')?$ispdetail->ifsc_code:""; ?>" required>
						<label> IFSC Code<sup>*</sup></label>
					     </div>
					  </div>
					  
				       </div>
				    </div>
				    
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
					     <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
						<input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="isp_bankholder_name" value="<?php echo (isset($ispdetail->account_holder_name) && $ispdetail->account_holder_name!='')?$ispdetail->account_holder_name:"" ?>" required >
						<label> Account Holder Name <sup>*</sup></label>
					     </div>
					  </div>
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
					     <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
						<input type="text" <?php echo (isset($ro))?"disabled":"";?> class="" name="isp_bankaccount_number" value="<?php echo (isset($ispdetail->account_number) && $ispdetail->account_number !='')?$ispdetail->account_number:""; ?>" required>
						<label> Account Number <sup>*</sup></label>
					     </div>
					  </div>
				       </div>
				    </div>

				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
					     <input type="submit" <?php echo (isset($ro))?"disabled":"";?> class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE" >
					  </div>
				       </div>
				    </div>
                                 </div>
			    </form>
			   </div>
                           <div class="mui-tabs__pane" id="Top-Up-5">
                              <div class="row">
                                 <div class="col-sm-12 col-xs-12">
                                    <div class="row">
                                       <form action="<?php echo base_url()?>setting/customizeinvoice" method="post" id="customizeinvoice_form" autocomplete="off" enctype='multipart/form-data'>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="color" name="billinvoicecolor" value="<?php echo $ispdetail->invoice_color; ?>" required/>
                                                         <label>Bill Invoice Color<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-4 col-xs-4">
                                                      <div class="dropzone" data-width="150" data-ajax="false" data-originalsize="false" data-height="150" style="width: 150px; height:150px;">
                                                         <input type="file" name="billbanner1"  />
                                                      </div>
                                                      <!--<div class="mui-textfield">
                                                         <span class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600" onclick="triggerbillbanner('billbanner1')">CHOOSE FILE</span>
                                                         <input type="file" name="billbanner1" accept="image/*" class="billbanner1 hide" />
                                                         <label>Bill Banner 1</label>
                                                      </div>-->
                                                      <div id="bb1">
                                                         <?php
                                                         if ($ispdetail->invoice_banner1 != '') {
                                                            echo "<img id='previewbb1' src='".base_url().'assets/media/invoicebanner/'.$ispdetail->invoice_banner1."' style='width:50%'/>";    
                                                         }
                                                         ?>
                                                      </div>
                                                   </div>
                                                   <div class="col-sm-4 col-xs-4">
                                                      <div class="dropzone" data-width="400" data-ajax="false" data-originalsize="false" data-height="100" style="width: 400px; height:100px;">
                                                         <input type="file" name="billbanner2"  />
                                                      </div>
                                                      <!--<div class="mui-textfield">
                                                         <span class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600" onclick="triggerbillbanner('billbanner2')">CHOOSE FILE</span>
                                                         <input type="file" name="billbanner2" accept="image/*" class=" billbanner2 hide" />
                                                         <label>Bill Banner 2</label>
                                                      </div>-->
                                                      <div id="bb2">
                                                         <?php
                                                         if ($ispdetail->invoice_banner2 != '') {
                                                            echo "<img id='previewbb1' src='".base_url().'assets/media/invoicebanner/'.$ispdetail->invoice_banner2."' style='width:50%'/>";    
                                                         }
                                                         ?>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          
                                          <div class="form-group">
                                             <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block" value="Save Changes" />
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="mui-tabs__pane" id="Top-Up-6">
                              <div class="col-sm-12 col-xs-12">
                                 <div class="row">
                                    <form action="<?php echo base_url()?>setting/apksetup" method="post" id="apksetup_form" autocomplete="off" enctype='multipart/form-data'>
                                       <div class="form-group">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-sm-3 col-xs-3">
                                                   <h3 style="margin:0px">APK Background Image <sup>*</sup></h3>
                                                   <div class="dropzone" data-width="400" data-ajax="false" data-originalsize="false" data-height="600" style="width: 200px; height:300px;">
                                                      <input type="file" name="apk_bgimage"  />
                                                   </div>
                                                   <div id="bb1">
                                                   </div>
                                                </div>
                                                <div class="col-sm-2 col-xs-2">
                                                   <?php
                                                   if ($ispdetail->apk_bgimage != '') {
                                                      echo "<img id='apkbb1' src='".base_url().'assets/media/apksetup/'.$ispdetail->apk_bgimage."' style='width:100%'/>";    
                                                   }
                                                   ?>
                                                </div>
                                                <div class="col-sm-3 col-xs-3">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="color" name="apk_theme_color" value="<?php echo $ispdetail->apk_theme_color ?>" required/>
                                                      <label>APK Theme Color<sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       
                                       <div class="form-group">
                                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                             <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block" value="Save Changes" />
                                          </div>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!--Div for ISP Detail update-->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- /Start your project here-->
<script>
   $(document).ready(function() {
       var height = $(window).height();
       $('#main_div').css('height', height);
       $('#right-container-fluid').css('height', height);
       
       
          var input1 = document.querySelector('#file');
       var preview1 = document.querySelector('#preview2');
       // When the file input changes, create a object URL around the file.
       input1.addEventListener('change', function () {
           preview1.src = URL.createObjectURL(this.files[0]);
           //$('#preview1').css({'height':'70','width':'70'});
       });
   });
   
   
   function add_ispdetail()
    {
         $('.loading').removeClass('hide');
        var formdata = $("#add_ispdetail").serialize();
        $.ajax({
       url: base_url+'setting/add_ispdetail',
       type: 'POST',
       dataType: 'json',
       data: formdata,
       success: function(data){
            $('.loading').addClass('hide');
       }
   });
        //alert(formdata);
    }
   
</script>
<script>
   $( '#but' ).click( function() {
      $( '#file' ).trigger( 'click' );
   } );
   
     $( '#buts' ).click( function() {
      $( '#file1' ).trigger( 'click' );
   } );
   
   
      var _URL = window.URL || window.webkitURL;
   $("#file").change(function(e) {
   var file, img;
   if ((file = this.files[0])) {
      img = new Image();
      img.onload = function() {
          var image_width = this.width;
          var image_height = this.height;
          var image_name = $('#file').val();
       var ext= getExt(image_name);
         
              if(image_width <  600 || image_height < 200){
                //  alert(image_width+"::"+image_height);
                  $("#provider_logo_error").html("Minimum Image size should be  of width 600px and height 200px");
                  $('input[type="submit"]').attr('disabled','disabled');
              }else{
                var  ratio = parseInt(image_width)/parseInt(image_height);
               
               
                     if(ratio==3)
                {
                    if(ext=="png" || ext=="PNG")
                    {
                          $('input[type="submit"]').removeAttr('disabled');
                           $("#provider_logo_error").html(""); 
                      }
                      else
                      {
                            $("#provider_logo_error").html("Image Format must be png");
                           $('input[type="submit"]').attr('disabled','disabled'); 
                      }
                }
                else
                {
                  $("#provider_logo_error").html("Ratio of logo to be 1:3");
                  $('input[type="submit"]').attr('disabled','disabled');
                }
                
                
               // alert(ratio);
               
                  
                     
   
              }
         
   
      };
      img.onerror = function() {
          $('input[type="submit"]').attr('disabled','disabled');
          alert( "not a valid file:");
      };
      img.src = _URL.createObjectURL(file);
   }
   });
   
   $("#file1").change(function(e) {
   var file, img;
   if ((file = this.files[0])) {
      img = new Image();
      img.onload = function() {
          var image_width = this.width;
          var image_height = this.height;
          var image_name = $('#file1').val();
       var ext= getExt(image_name);
         //alert(ext);
              if(image_width <  200 || image_height < 100){
                //  alert(image_width+"::"+image_height);
                  $("#provider_logo_error").html("Minimum Image size should be  of width 200px and height 100px");
                  $('input[type="submit"]').attr('disabled','disabled');
              }else{
                var  ratio = parseInt(image_width)/parseInt(image_height);
               
               
                     if(ratio==2)
                {
                    if(ext=="png" || ext=="PNG")
                    {
                          $('input[type="submit"]').removeAttr('disabled');
                           $("#sign_logo_error").html(""); 
                      }
                      else
                      {
                            $("#sign_logo_error").html("Image Format must be png");
                           $('input[type="submit"]').attr('disabled','disabled'); 
                      }
                }
                else
                {
                  $("#sign_logo_error").html("Ratio of logo to be 1:2");
                  $('input[type="submit"]').attr('disabled','disabled');
                }
                
                
               // alert(ratio);
               
                  
                     
   
              }
         
   
      };
      img.onerror = function() {
          $('input[type="submit"]').attr('disabled','disabled');
          alert( "not a valid file:");
      };
   
     img.src  = _URL.createObjectURL(file);
   $('#preview3').attr('src',img.src);
   }
   });
   
   function getExt(filename) {
   var dot_pos = filename.lastIndexOf(".");
   if(dot_pos == -1)
   return "";
   return filename.substr(dot_pos+1).toLowerCase();
   }
   function add_aboutus()
   {
    $('.loading').removeClass('hide');
   var formdata = $("#add_aboutus").serialize();
   $.ajax({
   url: base_url+'setting/add_aboutus',
   type: 'POST',
   dataType: 'json',
   data: formdata,
   success: function(data){
       $('.loading').addClass('hide');
   }
   });
   //alert(formdata);
   }
   
   function add_services()
   {
    $('.loading').removeClass('hide');
   var formdata = $("#add_services").serialize();
   $.ajax({
   url: base_url+'setting/add_services',
   type: 'POST',
   dataType: 'json',
   data: formdata,
   success: function(data){
       $('.loading').addClass('hide');
   }
   });
   //alert(formdata);
   }
   
   function add_ispbanking() {
	$('.loading').removeClass('hide');
	var formdata = $("#add_ispbanking").serialize();
	$.ajax({
	    url: base_url+'setting/add_ispbanking',
	    type: 'POST',
	    dataType: 'json',
	    data: formdata,
	    success: function(data){
		$('.loading').addClass('hide');
	    }
	});
    }
   
</script>
<script>
   if ($(window)) {
    $(function () {
     $('.menu').crbnMenu({
      hideActive: true
     });
    });
   }
   //setting_menu_ul
   $(document).ready(function(){
   //class="active" style="display: block;"
   //$("#setting_menu_ul").css("display","block");
   $("#ispdetail_menu").addClass("menu_active");
   
   
   });
</script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/image_crop/html5imageupload.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>ckeditor/ckeditor.js"></script>
<script>
   CKEDITOR.replace( 'editor1' );
   CKEDITOR.replace( 'editor2' );
</script>
<!-- script to get the geolocation and set lat and long -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA61iANDo6jqGYQm0SjXzIvmOEnks-MpG0&amp;libraries=places"  type="text/javascript"></script>
<script type="text/javascript">
   $('.dropzone').html5imageupload();
    function initialize() {
   
       var input = document.getElementById('geolocation');
     //  var options = {componentRestrictions: {country: 'in'}};
       var autocomplete = new google.maps.places.Autocomplete(input);
       google.maps.event.addListener(autocomplete, 'place_changed', function () {
           var place = autocomplete.getPlace();
           document.getElementById('lat').value = place.geometry.location.lat().toFixed(6);
           document.getElementById('long').value = place.geometry.location.lng().toFixed(6);
           document.getElementById('placeid').value = place.place_id;
           $("#placeid").attr('class','mui--is-dirty valid mui--is-not-empty');
           $("#lat").attr('class','mui--is-dirty valid mui--is-not-empty');
           $("#long").attr('class','mui--is-dirty valid mui--is-not-empty');
       });
   }
   google.maps.event.addDomListener(window, 'load', initialize);
</script>

<!--http://preview.codecanyon.net/item/html-5-upload-image-ratio-with-drag-and-drop/full_screen_preview/8712634?_ga=2.129999492.920028791.1511003309-714574992.1511003309-->
   <link href="<?php echo base_url() ?>assets/css/image_crop/demo.css" rel="stylesheet">
   <script type="text/javascript" src="<?php echo base_url() ?>assets/js/image_crop/html5imageupload.js"></script>
   <script type="text/javascript">
      $(document).ready(function(){
	 $('.dropzone').html5imageupload();
      });
   </script>
</body>
</html>

