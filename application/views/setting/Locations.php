<?php $this->load->view('includes/header');?>
<!-- Start your project here-->
<div class="loading" style="display: none;">
    <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="wapper">
            <div id="sidedrawer" class="mui--no-user-select">
                <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                      <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                     <img src="<?php echo $img ?>" class="img-responsive" />
                     </span>
                </div>
                <?php
                
                $data['navperm']=$this->plan_model->leftnav_permission();
                $this->view('left_nav',$data); 
                $state_list = $this->setting_model->state_list_dropdown();
                $state_id_send = '';
                if(isset($_POST['state']) && $_POST['state'] != ''){
                    $state_id_send = $_POST['state'];
                }
                $city_list = $this->setting_model->city_list_dropdown($state_id_send);
                ?>
            </div>
            <header id="header">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">Locations</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                          <ul class="nav navbar-nav navbar-right">
                                
                               <?php   $this->view('includes/global_setting',$data); ?>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            <div id="content-wrapper">
                <div class="mui--appbar-height"></div>
                <div class="mui-container-fluid" id="right-container-fluid">
                    <div class="add_user">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <div class="row">
                                                <h1> <small style="color:#424143">Zones</small></h1>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <div class="row">
                                                <div class="mui_location_list">
                                                    
                                                     <?php 
                                                      $is_editable=$this->plan_model->is_permission(LOCATION,'EDIT');
                                                       $disable=(!$is_editable)?"disabled":"";
                                                     if($this->plan_model->is_permission(LOCATION,'ADD')){ ?>   
                                                    <ul>
                                                       <!-- <li><a href="#" data-toggle="modal" data-target="#add_franchisee">Add Franchisee </a></li>-->
                                                        <li><a href="#" data-toggle="modal" data-target="#add_zone">Add Zone </a></li>
                                                        <li><a href="#" data-toggle="modal" data-target="#add_city">Add City </a></li>
                                                    </ul>
                                                     <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px">
                                    <div class="row">
                                        <ul class="mui-tabs__bar plan_mui-tabs__bar">
                                            <li class="mui--is-active">
                                                <a data-mui-toggle="tab" data-mui-controls="zones">Zones</a>
                                            </li>
                                            <!--<li>
                                                <a data-mui-toggle="tab" data-mui-controls="franchisees">
                                                    Franchisees
                                                </a>
                                            </li>-->
                                            <li>
                                                <a data-mui-toggle="tab" data-mui-controls="cities">
                                                    Cities
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="mui--appbar-height"></div>

                                        <div class="mui-tabs__pane mui--is-active" id="zones">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            <form method="post" accept-charset="utf-8" action="<?php echo base_url("setting/locations"); ?>">
                                                                <div class="col-lg-3 col-md-3 col-sm-3">
                                                                    <div class="mui-select">
                                                                        <select name="filter_state" onchange="search_filter_zone(this.value, 'state')">
                                                                            <option value="all">All States</option>
                                                                            <?php $this->user_model->state_list(); ?>
                                                                        </select>
                                                                        <label>State</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-3">
                                                                    <div class="mui-select">
                                                                        <select class="search_citylistzone"  onchange="search_filter_zone(this.value, 'city')"  name="filter_city">
                                                                            <option value="">All Cities</option>
                                                                        </select>
                                                                        <label>City</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pull-right">
                                                                    <div class="row">
                                                                        <div class="form-group">
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">
                                                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                                                </div>
                                                                                <input type="text" class="form-control" placeholder="Search Zone" onBlur="this.placeholder='Search Zone'" onFocus="this.placeholder=''"  id="searchzonetext">
                                                                                <span class="searchclear" id="searchcityclear">
                                                                                    <img src="<?php echo base_url() ?>assets/images/clear.svg"/>
                                                                                </span>
                                                                            </div>
                                                                            <label class="search_label">You can look up by Zone</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            <div class="table-responsive">

                                                                <table class="table table-striped">
                                                                    <thead>
                                                                    <tr class="active">
                                                                        <th>&nbsp;</th>
                                                                        <th colspan="2">ZONE NAME</th>
                                                                        <th>CITY</th>
                                                                        <th>STATE</th>
                                                                        
                                                                        <th class="mui--text-center" colspan="2">ACTIONS</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody id="search_gridviewzone">
                                                                    <?php
                                                                    $i = 1;
                                                                    
                                                                    // $is_editable=$this->plan_model->is_permission(LOCATION,'EDIT');
                                                                    foreach($zones as $zones1){
                                                                        ?>
                                                                        <tr>
                                                                            <td><?php echo $i;?></td>
                                                                            <td colspan="2"><?php echo $zones1->zone_name; ?></td>
                                                                            <td><?php echo $zones1->city_name; ?></td>
                                                                            <td><?php echo $zones1->state; ?></td>
                                                                         
                                                                            <td class="mui--text-right"
                                                                                onclick="edit_zone('<?php echo $zones1->state?>',
                                                                                    '<?php echo $zones1->city_name?>',
                                                                                    '<?php echo $zones1->zone_name?>',
                                                                                    '<?php echo $zones1->id?>')"><a
                                                                                    href="#">Edit</a></td>
                                                                                     <td class="mui--text-right"
                                                                                onclick="delete_zone('<?php echo $zones1->id?>')"><a
                                                                                    href="#">Delete</a></td>
                                                                         
                                                                        </tr>
                                                                        <?php
                                                                        $i++;
                                                                    }
                                                                    ?>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                 <!--  <div class="mui-tabs__pane" id="franchisees">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="table-responsive">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left: 0px;">
                                                                    <div class="mui-select">
                                                                        <select>
                                                                            <option>All Zone</option>
                                                                            <option>Option 2</option>
                                                                            <option>Option 3</option>
                                                                            <option>Option 4</option>
                                                                        </select>
                                                                        <label>Zone</label>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left: 0px;">
                                                                    <div class="mui-select">
                                                                        <select>
                                                                            <option>All Cities</option>
                                                                            <option>Option 2</option>
                                                                            <option>Option 3</option>
                                                                            <option>Option 4</option>
                                                                        </select>
                                                                        <label>Cities</label>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pull-right">
                                                                    <div class="row">
                                                                        <form role="search">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                                                    </div>
                                                                                    <input type="text" class="form-control" placeholder="Lookup Locations" onBlur="this.placeholder='Lookup Locations'" onFocus="this.placeholder=''">
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <table class="table table-striped">
                                                            <thead>
                                                            <tr class="active">
                                                                <th>&nbsp;</th>
                                                                <th colspan="2">FRANCHISEE NAME</th>
                                                                <th>ZONE</th>
                                                                <th>CITY</th>
                                                                <th>STATE</th>
                                                                <th class="mui--text-right">ACTIONS</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>1.</td>
                                                                <td colspan="2">Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td class="mui--text-right"><a href="#">Edit</a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2.</td>
                                                                <td colspan="2">Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td class="mui--text-right"><a href="#">Edit</a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>3.</td>
                                                                <td colspan="2">Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td class="mui--text-right"><a href="#">Edit</a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>4.</td>
                                                                <td colspan="2">Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td class="mui--text-right"><a href="#">Edit</a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>5.</td>
                                                                <td colspan="2">Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td>Lorem Ipsum Dolor</td>
                                                                <td class="mui--text-right"><a href="#">Edit</a></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>-->
                                        <div class="mui-tabs__pane" id="cities">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="table-responsive">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="row">
                                                                
                                                                  <div class="col-lg-3 col-md-3 col-sm-3">
                                                                    <div class="mui-select">
                                                                        <select name="filter_statec" onchange="search_filter_city(this.value, 'state')">
                                                                            <option value="all">All States</option>
                                                                            <?php $this->user_model->state_list(); ?>
                                                                        </select>
                                                                        <label>State</label>
                                                                    </div>
                                                                </div>
                                                  <!--<div class="col-lg-3 col-md-3 col-sm-3">
                                                                    <div class="mui-select">
                                                                        <select class="search_citylistcity"  onchange="search_filter_city(this.value, 'city')"  name="filter_cityc">
                                                                            <option value="">All Cities</option>
                                                                        </select>
                                                                        <label>City</label>
                                                                    </div>
                                                                </div>-->
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pull-right">
                                                                    <div class="row">
                                                                        <div class="form-group">
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">
                                                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                                                </div>
                                                                                <input type="text" class="form-control" placeholder="Search City" onBlur="this.placeholder='Search City'" onFocus="this.placeholder=''"  id="searchcitytext">
                                                                                <span class="searchclear" id="searchcityclear">
                                                                                    <img src="<?php echo base_url() ?>assets/images/clear.svg"/>
                                                                                </span>
                                                                            </div>
                                                                            <label class="search_label">You can look up by City</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                

                                                               

                                                              
                                                            </div>
                                                        </div>
                                                        <table class="table table-striped">
                                                            <thead>
                                                            <tr class="active">
                                                                <th>&nbsp;</th>
                                                                <th colspan="2">CITY</th>
                                                                <th>STATE</th>
                                                                <th>ZONE COUNT</th>
                                                                <!--<th>FRANCHISEE COUNT</th>-->
                                                                <th class="mui--text-right">ACTIONS</th>
                                                            </tr>
                                                            </thead>
                                                            
                                                            <tbody id="searchzone_grid">
                                                           
                                                              <?php
                                                                    $i = 1;
                                                                    foreach($city as $city1){
                                                                        ?>
                                                                        <tr>
                                                                            <td><?php echo $i;?></td>
                                                                            <td colspan="2"><?php echo $city1->city_name; ?></td>
                                                                            <td><?php echo $city1->state; ?></td>
                                                                            <td><?php echo $city1->zonecount; ?></td>
                                                                           
                                                                            <td class="mui--text-right"
                                                                               
                                                                                onclick="edit_city('<?php echo $city1->city_name?>',
                                                                                    '<?php echo $city1->city_state_id?>','<?php echo $city1->city_id;?>')"><a
                                                                                    href="#">Edit</a></td>
                                                                        </tr>
                                                                        <?php
                                                                        $i++;
                                                                    }
                                                                    ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Start your project here-->

<!--- Add city User Modal --->
<div class="modal fade" id="add_city" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>ADD CITY</strong></h4>
            </div>
            <form method="post" action="<?php echo base_url()?>setting/add_city">
                <div class="modal-body" style="padding-bottom:5px">
                    <p>Add a new city to your location listing.</p>
                    <div class="mui-textfield mui-textfield--float-label">
                        <input type="text" name="city_add" required>
                        <label>City Name</label>
                    </div>
                    <div class="mui-select">
                        <select id="add_city_state" name="add_city_state" required>
                            <option value="">Select State</option>
                            <?php
                            foreach($state_list->result() as $state_list1){
                                ?>
                                <option value="<?php echo $state_list1->id ?>"><?php echo $state_list1->state; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <label>State</label>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                    <button type="submit" class="mui-btn mui-btn--large mui-btn--accent">ADD</button>
                </div>
            </form>

        </div>
    </div>
</div>
<!--- End Add city User Modal --->


<!--- Edit city User Modal --->
<div class="modal fade" id="edit_city" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Edit CITY</strong></h4>
            </div>
            <form method="post" action="<?php echo base_url()?>setting/update_city">
                <div class="modal-body" style="padding-bottom:5px">
                    <input type="hidden" id="cityid" name="cityid" value="">
                    <p>Add a new city to your location listing.</p>
                    <div class="mui-textfield mui-textfield--float-label">
                        <input type="text" <?php echo $disable;?> id="city_name" name="city_add" required>
                        <label>City Name</label>
                    </div>
                    <div class="mui-select">
                        <select id="edit_city_state" <?php echo $disable;?> name="add_city_state" required>
                            <option value="">Select State</option>
                            <?php
                            foreach($state_list->result() as $state_list1){
                                ?>
                                <option value="<?php echo $state_list1->id ?>"><?php echo $state_list1->state; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <label>State</label>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                    <button type="submit" <?php echo $disable;?> class="mui-btn mui-btn--large mui-btn--accent">Update</button>
                </div>
            </form>

        </div>
    </div>
</div>
<!--- End edit city User Modal --->


<!--- Add Zone User Modal --->
<div class="modal fade" id="add_zone" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>ADD ZONE</strong></h4>
            </div>
            <form method="post" action="<?php echo base_url()?>setting/add_zone">
                <div class="modal-body" style="padding-bottom:5px">
                    <p>Add a new zone to your location listing.</p>
                    <div class="mui-textfield mui-textfield--float-label">
                        <input type="text" name="zone_name" required>
                        <label>Zone Name</label>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="mui-select">
                                <select id="add_zone_state" name="add_zone_state" required>
                                    <option value="">Select State</option>
                                    <?php
                                    foreach($state_list->result() as $state_list1){
                                        ?>
                                        <option value="<?php echo $state_list1->id ?>"><?php echo $state_list1->state; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label>State</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                            <div class="mui-select">
                                <select id="add_zone_city" name="add_zone_city" required>
                                    <option value="">Select City</option>
                                </select>
                                <label>City</label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px ">
                            <a class="mui-btn mui-btn--small mui-btn--flat" style="color:#29ABE2 ">Add City</a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                    <button type="submit" class="mui-btn mui-btn--large mui-btn--accent">ADD</button>
                </div>
            </form>

        </div>
    </div>
</div>
<!--- End Add zone User Modal --->

<!--- Edit Zone User Modal --->
<div class="modal fade" id="edit_zone" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>EDIT ZONE</strong></h4>
            </div>
            <form method="post" action="<?php echo base_url()?>setting/update_zone">
                <div class="modal-body" style="padding-bottom:5px">
                    <p>Edit  zone to your location listing.</p>
                      <div class="mui-textfield">
                        <input type="hidden" name="zone_id" id="zone_id">
                        <input type="text" name="zone_name" <?php echo $disable;?> id="zone_name_edit" required>
                        <label>Zone Name</label>
                    </div>
                    <div class="mui-textfield ">
                        <input type="text" name="state_name_edit" <?php echo $disable;?> id="state_name_edit" readonly required>
                        <label>City</label>
                    </div>
                    <div class="mui-textfield ">
                        <input type="text" name="city_name_edit" <?php echo $disable;?> id="city_name_edit" readonly required>
                        <label>City</label>
                    </div>
                   

                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                    <button type="submit" <?php echo $disable;?> class="mui-btn mui-btn--large mui-btn--accent">UPDATE</button>
                </div>
            </form>

        </div>
    </div>
</div>
<!--- End Edit zone User Modal --->

<!-- Delete zone-->
<div class="modal fade" id="delete_zone" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Delete ZONE</strong></h4>
            </div>
            <form method="post" action="<?php echo base_url()?>setting/delete_zone">
                <div class="modal-body" style="padding-bottom:5px">
                    <p>Are you sure You want to delete <span id="delzone"></span></p>
                    <div class="row">
                        <input type="hidden" id="hdnzoneid"  name="zoneid" value="">
                     <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                            <div class="mui-select">
                                <select id="hdnzone_name"  name="newzone_id"  style="display:none;" disabled required>
                                    <option value="">Select Zone</option>
                                </select>
                                <label class="lblezone">Please change the zone</label>
                            </div>
                        </div>
                     </div>

                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                    <button type="submit" <?php echo $disable;?> class="mui-btn mui-btn--large mui-btn--accent">Delete</button>
                </div>
            </form>

        </div>
    </div>
</div>
<!-- delete zone modal-->


<!--- Add Zone User Modal --->
<div class="modal fade" id="add_franchisee" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>ADD FRANCHISEE</strong></h4>
            </div>
            <div class="modal-body" style="padding-bottom:5px">
                <p>Add a new franchisee to your location listing.</p>
                <div class="mui-textfield mui-textfield--float-label">
                    <input type="text" name="franchisee_name" required>
                    <label>Franchisee Display Name<sup>*</sup></label>
                </div>
                <div class="mui-textfield mui-textfield--float-label">
                    <input type="text" name="username" required>
                    <label>Username<sup>*</sup></label>
                </div>
                <div class="mui-textfield mui-textfield--float-label">
                    <input type="password" name="password" required>
                    <label>Password*<sup>*</sup></label>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="mui-select">
                            <select id="add_franchisee_state" name="add_franchisee_state" required>
                                <option value="">Select State</option>
                                <?php
                                foreach($state_list->result() as $state_list1){
                                    ?>
                                    <option value="<?php echo $state_list1->id ?>"><?php echo $state_list1->state; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <label>State</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <div class="mui-select">
                            <select id="add_franchisee_city" name="add_franchisee_city" required>
                                <option value="">Select City</option>
                            </select>
                            <label>City</label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px ">
                        <a class="mui-btn mui-btn--small mui-btn--flat" style="color:#29ABE2 ">Add City</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <div class="mui-select">
                            <select id="add_franchisee_zone" name="add_franchisee_zone" required>
                                <option value="">Select Zone</option>
                            </select>
                            <label>Zone</label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px ">
                        <a class="mui-btn mui-btn--small mui-btn--flat" style="color:#29ABE2 ">Add Zone</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="text-align: right">
                <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                <button type="button" class="mui-btn mui-btn--large mui-btn--accent">ADD</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var height = $(window).height();
        $('#main_div').css('height', height);
        $('#right-container-fluid').css('height', height);

        // on user type change change page(form)
        $('input[type=radio][name=user_type_radio]').change(function() {
            if (this.value == 'lead' || this.value == 'enquiry') {
                $("#user_type_lead_enquiry").show();
                $("#user_type_customer").hide();
            }
            else if (this.value == 'customer') {
                $("#user_type_customer").show();
                $("#user_type_lead_enquiry").hide();
            }
        });
        
        
            $('body').on('click','#searchcityclear', function(){
      $('#searchzonetext').val('');
      search_filter_zone();
      
   });
   
    $('body').on('keyup', '#searchzonetext', function(){
      search_filter_zone();
   });
   
       $('body').on('keyup', '#searchcitytext', function(){
      search_filter_city();
   });
        

    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function (){
            $('.loading').hide();
        },1000);
    });
</script>


<script type="text/javascript">
    var paneIds = ['pane-api-1', 'pane-api-2','pane-api-3','pane-api-4'],
        currPos = 0;

    function activateNext() {
        // increment id
        currPos = (currPos + 1) % paneIds.length;

        // activate tab
        mui.tabs.activate(paneIds[currPos]);
    }

    //add zone on state change city list get
    $("#add_zone_state, #add_franchisee_state").change(function(){
        $('.loading').show();
        var state_id = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>setting/state_city_list",
            cache: false,
            data: "state_id="+state_id,
            success: function(data){
                $('.loading').hide();
                $('#add_zone_city').html(data);
                $('#add_franchisee_city').html(data);
            }
        });
    });
    //add Franchisee on city change zone list
    $("#add_franchisee_city").change(function(){
        $('.loading').show();
        var city_id = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>setting/city_zone_list",
            cache: false,
            data: "city_id="+city_id,
            success: function(data){
                $('.loading').hide();
                $('#add_franchisee_zone').html(data);
            }
        });
    });

    function edit_zone(state,city,zone,zone_id){
        $("#zone_id").val(zone_id);
        $("#zone_name_edit").val(zone);
        $("#state_name_edit").val(city);
        $("#city_name_edit").val(state);
        $("#edit_zone").modal('show');
    }
    
    function delete_zone(zone_id)
    {
        // $('.loading').css('display', 'block');
        
          $.ajax({
         url: base_url+'setting/get_deletezone_data',
         type: 'POST',
         dataType: 'json',
         data: {zone_id:zone_id},
         success: function(data){
            if (data.is_deletable==1) {
                 $('#delzone').html(data.zonename);
                $('#hdnzoneid').val(data.zone_id);
                $('#hdnzone_name').css('display','none');
                $('#hdnzone_name').prop('disabled',true);
                $('#hdnzone_name').prop('required',false);
                $('.lblezone').addClass('hide');
                $("#delete_zone").modal('show');
            }
            else
            {
                $('#delzone').html(data.zonename);
                $('#hdnzoneid').val(data.zone_id);
                $('#hdnzone_name').html(data.html);
                $('#hdnzone_name').css('display','block');
                $('#hdnzone_name').prop('disabled',false);
                $('#hdnzone_name').prop('required',true);
                $('.lblezone').removeClass('hide');
                $("#delete_zone").modal('show');
            }
            //alert(data.html);
           //  alert(data.search_results);
            //$('.loading').css('display', 'none');
            //$('#search_count').html(data.total_results+' <small>Results</small>');
          //  $('#search_gridviewzone').html(data.search_results);
         }
      });
    }
    
    
  
  
    
    
    function edit_city(city,stateid,cityid){
       // $("#zone_id").val(zone_id);
        //$("#zone_name_edit").val(zone);
        $("#city_name").val(city);
        $("#edit_city_state").val(stateid);
          $("#cityid").val(cityid);
        $("#edit_city").modal('show');
     //   cityid
    }
    
    function search_filter_zone(data='', filterby=''){
      var searchtext = $('#searchzonetext').val();
      $('#search_panel').addClass('hide');
      $('#allsearch_results').removeClass('hide');
     // $('.loading').css('display', 'block');
      
      var formdata = '';
    //  var city = $('select[name="filter_city"]').val();
     // var zone = $('select[name="filter_zone"]').val();
      if (filterby == 'state') {
          $('.search_citylistzone').find('option:selected').prop("selected", false);
           $('.search_zonelist').find('option:selected').prop("selected", false);
         getcitylist(data);
           var city = $('select[name="filter_city"]').val();
      var zone = $('select[name="filter_zone"]').val();
         formdata += '&state='+data+ '&city='+city+'&zone='+zone;
      }
      else if(filterby == 'city')
      {
          $('.search_zonelist').find('option:selected').prop("selected", false);
         // getzonelist(data);
          var state = $('select[name="filter_state"]').val();
          var zone = $('select[name="filter_zone"]').val();
         formdata += '&state='+state+ '&city='+data+'&zone='+zone;
      }
        else{
         var state = $('select[name="filter_state"]').val();
         var city = $('select[name="filter_city"]').val();
          var zone = $('select[name="filter_zone"]').val();
         formdata += '&state='+state+ '&city='+city+'&zone='+zone;
      }

      //alert(formdata);
      $.ajax({
         url: base_url+'setting/view_searchzone_list',
         type: 'POST',
         dataType: 'json',
         data: 'search_user='+searchtext+formdata,
         success: function(data){
           //  alert(data.search_results);
            $('.loading').css('display', 'none');
            //$('#search_count').html(data.total_results+' <small>Results</small>');
            $('#search_gridviewzone').html(data.search_results);
         }
      });
   }
   
   
    function search_filter_city(data='', filterby=''){
      var searchtext = $('#searchcitytext').val();
      $('#search_panel').addClass('hide');
      $('#allsearch_results').removeClass('hide');
     // $('.loading').css('display', 'block');
      
      var formdata = '';
    //  var city = $('select[name="filter_city"]').val();
     // var zone = $('select[name="filter_zone"]').val();
      if (filterby == 'state') {
          $('.search_citylistcity').find('option:selected').prop("selected", false);
           $('.search_zonelist').find('option:selected').prop("selected", false);
         getcitylist(data);
           var city = $('select[name="filter_cityc"]').val();
      var zone = $('select[name="filter_zonec"]').val();
         formdata += '&state='+data+ '&city='+city+'&zone='+zone;
      }
      else if(filterby == 'city')
      {
          $('.search_zonelist').find('option:selected').prop("selected", false);
         // getzonelist(data);
          var state = $('select[name="filter_state"]').val();
          var zone = $('select[name="filter_zone"]').val();
         formdata += '&state='+state+ '&city='+data+'&zone='+zone;
      }
        else{
         var state = $('select[name="filter_state"]').val();
         var city = $('select[name="filter_city"]').val();
          var zone = $('select[name="filter_zone"]').val();
         formdata += '&state='+state+ '&city='+city+'&zone='+zone;
      }

      //alert(formdata);
      $.ajax({
         url: base_url+'setting/view_searchcity_list',
         type: 'POST',
         dataType: 'json',
         data: 'search_user='+searchtext+formdata,
         success: function(data){
           //  alert(data.search_results);
            $('.loading').css('display', 'none');
            //$('#search_count').html(data.total_results+' <small>Results</small>');
            $('#searchzone_grid').html(data.search_results);
         }
      });
   }
    
     function getcitylist(stateid) {
    $.ajax({
        url: base_url+'user/getcitylist',
        type: 'POST',
        dataType: 'text',
        data: 'stateid='+stateid,
        success: function(data){
           
            $('.search_citylistzone').empty().append('<option value="">All Cities</option>');
            $('.search_citylistzone').append(data);
        }
    });
}


</script>
  <script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
         //setting_menu_ul
$(document).ready(function(){
//class="active" style="display: block;"
$("#setting_menu_ul").css("display","block");
$("#location_menu").addClass("menu_active");

});
      </script>
</body>
</html>
