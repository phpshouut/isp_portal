<!-- Start your project here-->
<?php $this->load->view('includes/header');
?>
<div class="loader loading hide" >
    <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
</div>
      <!-- Start your project here-->
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                <div id="sidedrawer-brand" class="mui--appbar-line-height">
                    <span class="mui--text-title">
                           <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                        <img src="<?php echo $img ?>" class="img-responsive" />
                    </span>
                </div>

                <?php
                $data['navperm'] = $this->plan_model->leftnav_permission();
                // echo "<pre>"; print_R($data);
                $this->view('left_nav', $data);
                ?>
            </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">Router Configuration</a>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="add_user">
                        <div class="row mtconnection" style="display:block;">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="input_container">
									<h2>Nas Connection</h2>
									
									  <div class="mui-textfield mui-textfield--float-label">
										<input type="text" id="ip" onblur="check_nassetup()">
										<label>Nas IP</label>
									  </div>
									  <span class="help-inline naserr"></span>
									    <div class="mui-textfield mui-textfield--float-label">
										<input type="text" id="username">
										<label>Username </label>
									  </div>
									  <span class="help-inline"></span>
									    <div class="mui-textfield mui-textfield--float-label">
										<input type="text" id="password">
										<label>Password </label>
									  </div>
									  <span class="help-inline" id="login_error"></span>
									 <input type="hidden" name="hdnip" id="hdnip" value="">
								<input type="hidden" name="hdnusername" id="hdnusername" value="">
								  <input type="hidden" name="hdnpwd" id="hdnpwd" value="">
								  <input type="hidden" name="hdnvlanname" id="hdnvlanname" value="" >
								  <input type="hidden" name="hdninterface" id="hdninterface" value="" >
								  <input type="hidden" name="hdnvlanid" id="hdnvlanid" value="" >
								  <input type="hidden" name="hdnseltype" id="hdnseltype" value="">
								    <input type="hidden" name="hdnpublicip" id="hdnpublicip" value="">
								      <input type="hidden" name="hdnconnid" id="hdnconnid" value="">
								      
									   
									   
									   <div class="mui-row">
										<div class="col-sm-2 col-xs-2">&nbsp;</div>
										<div class="col-sm-8 col-xs-8">
										<center>
										<button type="submit" class="mui-btn mui-btn--accent btn-lg lgin" onclick="submit_form()">LOGIN</button>
											</center>
										</div>
									  <div class="col-sm-2 col-xs-2">&nbsp;</div>
									</div>
									
								</div>
							</div>
                           </div>
			
			                        <div class="row radiovlan" style="display:none;">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="input_container">
									<h2>Select Type</h2>
									
									
									   <div class="mui-row">
									      <div class="form-group">
											  <div class="col-md-12">
												<div class="radio radio-primary">
												  <label>
													<input name="contype"  value="vlan"  type="radio">
													vlan
												  </label>
												</div>
												<div class="radio radio-primary">
												  <label>
													<input name="contype"  value="ethernet" type="radio">
													Ethernet
												  </label>
												</div>
											  </div>
											</div>
										</div>
								      <span class="help-inline" id="vlanerr"></span>
									   
									  
									  
									
								</div>
							</div>
                           </div>
			     
			     <div class="row vlandet" style="display:none;">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="input_container">
									<h2>Interface</h2>
									<div id="intrflist">
     </div>
									 <span class="help-inline intrerr" ></span>
									  <span class="help-inline errorvlanid" ></span>
									
									  <div class="mui-textfield mui-textfield--float-label">
										<input type="text" id="vlanid" name="vlanid" onblur="check_vlanid()">
										<label>Vlan ID</label>
									  </div>
									  <span class="help-inline errorvlan"></span>
									  
									    <div class="mui-textfield mui-textfield--float-label">
										<input type="text" id="vlan" name="vlan" onblur="checkvlanname()">
										<label>Vlan Name</label>
									  </div>
									  <span class="help-inline errorsetup"></span>
									  <span id="successsetup" style = "color:green"></span>
								     
									   <div class="mui-row">
										<div class="col-sm-2 col-xs-2">&nbsp;</div>
										<div class="col-sm-8 col-xs-8">
										<center>
										<button type="submit" class="setup mui-btn mui-btn--accent btn-lg" rel="vlan" disabled="disabled">Next</button>
											</center>
										</div>
									  <div class="col-sm-2 col-xs-2">&nbsp;</div>
									</div>
									
								</div>
							</div>
                           </div>
			     
			     
			     <div class="row ethrnetdet" style="display:none;">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="input_container">
									<h2>Ethernet</h2>
									<div id="ethrlist">
     </div> <span class="help-inline etherr"></span>
									  
								      
									   
									   <div class="mui-row">
										<div class="col-sm-2 col-xs-2">&nbsp;</div>
										<div class="col-sm-8 col-xs-8">
										<center>
										<button type="submit" class="setup mui-btn mui-btn--accent btn-lg " rel="ethernet">Next</button>
											</center>
										</div>
									  <div class="col-sm-2 col-xs-2">&nbsp;</div>
									</div>
									
								</div>
							</div>
                           </div>
			     
			     
			     <div class="row ppoelist" style="display:none;">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="input_container">
									<h2>ISP ADDRESS</h2>
									<div id="ppoelistd">
     </div>
									  <span class="help-inline ppoeerr">Please correct the error</span>
									
									 
								      
									   
									   <div class="mui-row">
										<div class="col-sm-2 col-xs-2">&nbsp;</div>
										<div class="col-sm-8 col-xs-8">
										<center>
										<button type="submit" class="nextppoe mui-btn mui-btn--accent btn-lg ">Next</button>
											</center>
										</div>
									  <div class="col-sm-2 col-xs-2">&nbsp;</div>
									</div>
									
								</div>
							</div>
                           </div>
						
						
			<div class="row iprangecust" style="display:none;">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="input_container">
									<h2>IP Range</h2>
									
									  <div class="mui-textfield mui-textfield--float-label">
										<input type="text" id="rangeip" name="rangeip" onblur="validate_iprange();">
										<label>IP Range</label>
									  </div>
									  <span class="help-inline rangeiperr"></span>
									  <span class="successsetop" style = "color:green"></span>
								    
									    
									   <div class="mui-row">
										<div class="col-sm-2 col-xs-2">&nbsp;</div>
										<div class="col-sm-8 col-xs-8">
										<center>
										<button type="submit" class="nextcustip mui-btn mui-btn--accent btn-lg ">Set Up</button>
											</center>
										</div>
									  <div class="col-sm-2 col-xs-2">&nbsp;</div>
									</div>
									
								</div>
							</div>
                           </div>
			
			<div class="row noofseesion" style="display:none;">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="input_container">
									<h2>Username or Email</h2>
									
									  <div class="mui-textfield mui-textfield--float-label">
										<input type="text" id="nosession" value="1" name="nosession">
										<label>No. of Session</label>
									  </div>
									  <span class="help-inline sessionerr"></span>
									   <span class="suucessset" style = "color:green"></span>
								    
									  
									   <div class="mui-row">
										<div class="col-sm-2 col-xs-2">&nbsp;</div>
										<div class="col-sm-8 col-xs-8">
										<center>
										<button type="submit" class="sessioncustip mui-btn mui-btn--accent btn-lg ">Set Up</button>
											</center>
										</div>
									  <div class="col-sm-2 col-xs-2">&nbsp;</div>
									</div>
									
								</div>
							</div>
                           </div>
			     

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   
      <!-- /Start your project here-->
      <!-- SCRIPTS -->
      <!-- JQuery -->
       <script type="text/javascript" src="<?php echo base_url() ?>assets/js/router_misc.js"></script>
      <script>
	    if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
         $(document).ready(function() {
           var height = $(window).height();
            $('#main_div').css('height', height);
           $('#right-container-fluid').css('height', height);
         });
	     $(document).ready(function(){
//class="active" style="display: block;"
$("#setting_menu_ul").css("display","block");
$("#ppoe_router").addClass("menu_active");

});
      </script>
  
   
      </script>
      
      <?php $this->load->view('includes/footer'); ?>
     
  