<?php
$isp_detail = $this->user_model->get_isp_name();
$isp_name = $isp_detail['isp_name'];
$fevicon = $isp_detail['fevicon_icon'];
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <link rel="icon" href="<?php echo base_url()?>assets/images/<?php echo $fevicon?>" type="image/x-icon"/>
      <title><?php echo $isp_name; ?> </title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading">
	 <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                     <?php
                     $isplogo = $this->user_model->get_ispdetail_info();
                     if($isplogo != 0){
                        echo '<img src="'.base_url()."ispmedia/logo/".$isplogo.'" class="img-responsive"/>';
                     }else{
                        echo '<img src="'.base_url().'assets/images/decibel.png" class="img-responsive"/>';
                     }
                     ?>
                     </span>
                  </div>
                  <?php
		     $leftperm['navperm']=$this->user_model->leftnav_permission();
		     $this->view('left_nav',$leftperm);
		  ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <span class="navbar-brand">Users</span>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
			      <?php if($this->user_model->is_permission(CREATEUSER,'ADD')){ ?>
                              <li>
                                 <a href="javascript:void(0)" onclick="setup_adduser()">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD USER
                                 </button>
                                 </a>
                              </li>
			      <?php } ?>
			      <?php if($this->user_model->is_permission(PLANS,'ADD')){ ?>
                              <li>
                                 <a href="javascript:void(0)" onclick="check_tax_plan()">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD PLAN
                                 </button>
                                 </a>
                              </li>
			      <?php } ?>
			      <?php if($this->user_model->is_permission(TOPUP,'ADD')){ ?>
			      <li>
                                 <a href="javascript:void(0)" onclick="check_tax_topup()"">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD TOPUP
                                 </button>
                                 </a>
                              </li>
			      <?php }
			      $this->load->view('includes/global_setting',$leftperm);
			      
			      ?>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <ul>
                                 <li id="active_users">
                                    <h5>ACTIVE USERS</h5>
                                    <h4><i class="fa fa-user" aria-hidden="true"></i> <?php echo $active_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li id="inactive_users">
                                    <h5>INACTIVE USERS</h5>
                                    <h4><i class="fa fa-wifi" aria-hidden="true"></i> <?php echo $inactive_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li id="lead_enquiry">
                                    <h5>LEADS & ENQUIRIES</h5>
                                    <h4><i class="fa fa-question-circle" aria-hidden="true"></i> <?php echo $lead_enquiry_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li style="background-color:#f4474a"  id="user_complaints">
                                    <h5>COMPLAINTS</h5>
                                    <h4><i class="fa fa-exclamation-circle" aria-hidden="true"></i> <?php echo $complaints?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li style="background-color:#f4474a" id="payment_overdue">
                                    <h5>PAYMENT OVERDUE</h5>
                                    <h4> <!--<i class="fa fa-inr" aria-hidden="true"></i>--><?php echo $ispcodet['currency'] ?> <?php echo $payment_dues?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
				 <li style="background-color:#f4474a" id="otherdues">
                                    <h5>OTHER DUES</h5>
                                    <h4> <!--<i class="fa fa-inr" aria-hidden="true"></i>--><?php echo $ispcodet['currency'] ?> <?php echo $otherdues?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                              </ul>
                           </div>
			   <div id="lead_enquiry_results">
			      <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                       <h1>Lead & Enquiry (<span id="lead_enquiry_count"></span>)</h1>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 pull-right">
                                       <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 14px;">
                                          <div class="form-group">
                                             <div class="input-group">
                                                <div class="input-group-addon">
                                                   <i class="fa fa-search" aria-hidden="true"></i>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Search Leads" onBlur="this.placeholder='Search Leads'" onFocus="this.placeholder=''"  id="searchlead_enquiry">
                                                <span class="searchclear" class="searchclear">
                                                <img src="<?php echo base_url() ?>assets/images/clear.svg"/>
                                                </span>
                                             </div>
                                             <label class="search_label">You can look up by name, email, UID or mobile</label>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="row" id="onefilter">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                       <div class="row">
                                          <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="mui-select">
                                                <select name="filter_lelocality" onchange="search_lefilter()">
                                                   <option value="">User Category*</option>
                                                   <?php $this->user_model->usage_locality(); ?>
                                                </select>
                                                <label>User Category</label>
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="mui-select">
                                                <select name="filter_lestate" onchange="search_lefilter(this.value, 'state')">
                                                   <option value="">All States</option>
                                                   <?php $this->user_model->state_list(); ?>
                                                </select>
                                                <label>State</label>
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="mui-select">
                                                <select class="search_citylist"  onchange="search_lefilter(this.value, 'city')"  name="filter_lecity">
                                                   <option value="">All Cities</option>
                                                </select>
                                                <label>City</label>
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="mui-select">
                                                <select class="search_zonelist" onchange="search_lefilter()"  name="filter_lezone">
                                                   <option value="">All Zones</option>
                                                   <?php //$this->user_model->zone_list(); ?>
                                                </select>
                                                <label>Zone</label>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="table-responsive">
                                    <table class="table table-striped">
                                       <thead style="font-size:11px">
                                          <tr class="active">
                                             <th>&nbsp;</th>
                                             <!--<th>
                                                <div class="checkbox" style="margin-top:0px; margin-bottom:0px;">
                                                   <label>
                                                   <input type="checkbox" class="collapse_allcheckbox"> 
                                                   </label>
                                                </div>
                                             </th>-->
                                             <th>FULL NAME</th>
                                             <th>TYPE</th>
                                             <th>USER CATEGORY</th>
                                             <th>EMAIL</th>
                                             <th>PHONE</th>
                                             <th>STATE</th>
                                             <th>CITY</th>
                                             <th>ZONE</th>
                                             <th>ACTION</th>
                                          </tr>
                                       </thead>
                                       <tbody id="lead_enquiry_gridview" style="font-size:11px"></tbody>
                                    </table>
                                 </div>
                              </div>
			   </div>
			</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="taxerr" role="dialog"  data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-body" style="padding-bottom:5px">
		     <p id="taxmsg">Please enter Tax before.</p>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button"  class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button> 
		  </div>
	       </div>
	 </div>
      </div>
      
      <div class="modal fade" id="billingerr" role="dialog"  data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-body" style="padding-bottom:5px">
		     <p id="billerrmsg"></p>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button"  class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button> 
		  </div>
	       </div>
	 </div>
      </div>

      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/material.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/misc.js?version=3.1"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/crbnMenu.js"></script>
      <script type="text/javascript">
         if ($(window)) {
            $(function () {
               $('.menu').crbnMenu({
               hideActive: true
               });
            });
         }
      </script>
      <script type="text/javascript">
         $('body').on('click', '#lead_enquiry', function(){
	    window.location.href = base_url+'user/lead_enquiry';
	 });
	 
	 $('body').on('click', '#user_complaints', function(){
	    window.location.href = base_url+'user/complaints';
	 });
	 
	 $('body').on('click', '#inactive_users', function(){
	    window.location.href = base_url+'user/status/inact';
	 });
	 
	 $('body').on('click', '#active_users', function(){
	    window.location.href = base_url+'user/status/act';
	 });
         $('body').on('click', '#payment_overdue', function(){
	    window.location.href = base_url+'user/pay_overdue'; 
	 });
         $('body').on('click', '#otherdues', function(){
	    window.location.href = base_url+'user/otherdues'; 
	 });
	 
         $('body').on('click','.searchclear', function(){
            $('#searchlead_enquiry').val('');
            search_lefilter();
            
         });
         $('body').on('keyup', '#searchlead_enquiry', function(){
            search_lefilter();
         });
         
         $('body').on('click', '#lead_enquiry', function(){
	    window.location.href = base_url+'user/lead_enquiry';
	 });
	 
	 $('body').on('click', '#user_complaints', function(){
	    window.location.href = base_url+'user/complaints';
	 });
         
         $('body').on('change', '.collapse_allcheckbox', function(){
            var status = this.checked;
            $('.collapse_checkbox').each(function(){ 
               this.checked = status;
            });
            if (status) {
               $('#onefilter').css('display', 'none');
               $('#secondfilter').css('display', 'block');
            }else{
               $('#onefilter').css('display', 'block');
               $('#secondfilter').css('display', 'none');
            }
         });
         
          $('body').on('change', '.collapse_checkbox', function(){
            if(this.checked == false){ 
               $(".collapse_allcheckbox")[0].checked = false; 
            }
            if ($('.collapse_checkbox:checked').length == $('.collapse_checkbox').length ){
               $(".collapse_allcheckbox")[0].checked = true; 
            }
            
            if ($('.collapse_checkbox:checked').length > 0) {
               $('#onefilter').css('display', 'none');
               $('#secondfilter').css('display', 'block');
            }else{
               $('#onefilter').css('display', 'block');
               $('#secondfilter').css('display', 'none');
            }
         });
         
         function delete_lead_enquiry(leid) {
            var c = confirm('Are you sure, you want to delete this user ?');
            if (c) {
               $('.loading').removeClass('hide');
               $.ajax({
                  url: base_url+'user/delete_subscriber',
                  type: 'POST',
                  dataType: 'json',
                  data: 'userid='+leid,
                  success: function(data){
                     window.location.href = base_url+'user/lead_enquiry';
                  }
               });
            }
         }
         
         function search_lefilter(data='', filterby=''){
            var searchtext = $('#searchlead_enquiry').val();
            $('#search_panel').addClass('hide');
            $('#allsearch_results').addClass('hide');
            $('#lead_enquiry_results').removeClass('hide');
            $('.loading').removeClass('hide')
            
            var formdata = '';
            var city = $('select[name="filter_lecity"]').val();
            var zone = $('select[name="filter_lezone"]').val();
            var locality = $('select[name="filter_lelocality"]').val();
            if (filterby == 'state') {
               getcitylist(data);
               formdata += '&state='+data+ '&city=&zone='+zone+'&locality='+locality;
            }else if (filterby == 'city') {
               var state = $('select[name="filter_lestate"]').val();
               getzonelist(data);
               formdata += '&state='+state+'&city='+data+'&zone=&locality='+locality;
            }else{
               var state = $('select[name="filter_lestate"]').val();
               formdata += '&state='+state+ '&city='+city+'&zone='+zone+'&locality='+locality;
            }
      
            //alert(formdata);
            $.ajax({
               url: base_url+'user/lead_enquiry_filter',
               type: 'POST',
               dataType: 'json',
               data: 'search_user='+searchtext+formdata,
               success: function(data){
                  $('.loading').addClass('hide');
                  $('#lead_enquiry_count').html(data.total_results+' <small>Results</small>');
                  $('#lead_enquiry_gridview').html(data.search_results);
               }
            });
         }
         $(document).ready(function() {
	    var height = $(window).height();
            $('#main_div').css('height', height);
	    //$('#right-container-fluid').css('height', height);
	    
            var total_results = "<?php echo $lead_enquiry['total_results']; ?>";
            var search_results = "<?php echo $lead_enquiry['search_results']; ?>" ;
	    $('#lead_enquiry_count').html(total_results+' <small>Results</small>');
	    $('#lead_enquiry_gridview').html(search_results);
	    $('#lead_enquiry_results').removeClass('hide');
	    $('.loading').addClass('hide');

         });
      </script>
   </body>
</html>