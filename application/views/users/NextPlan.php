

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="row">
            <div class="mui-tabs__pane mui--is-active " id="nxt_Plan-details">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                     <h2><span class="nxt_plan_name"></span> <span id="nxt_plan_price"></span> <span id="nxt_custom_planprice"></span> <a href="javascript:void(0)" onclick="show_customize_plan_pricing_modal()" style="font-size: 12px; margin-left: 50px; text-decoration: underline"> Set Custom Plan Pricing </a></h2>
                     
                     <div class="form-group">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:5px;">
                           <h4>TYPE OF PLAN: &nbsp;
                            <span class="nxt_detplan_type" style="font-weight:normal; font-size:14px"></span>
                           </h4> 
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:5px;">
                           <h4>PLAN DURATION: &nbsp;
                            <span class="nxt_detplan_duration" style="font-weight:normal; font-size:14px"></span>
                           </h4> 
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px">
                           <div class="row">
                                <div class="mui-textfield">
                                   <input type="text" class="nxt_detplan_desc" value="" disabled>
                                   <label>Plan Description</label>
                                </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group" id="user_type_ful_plan_enquiry" style="display: block">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:20px;">
                           <div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding-left">
                                 <div class="mui-textfield">
                                    <input type="text" class="nxt_detdwnld_rate" value="" disabled>
                                    <span class="title_box">Kbps</span>
                                    <label>Download Rate<sup>*</sup></label>
                                 </div>
                              </div>
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                 <div class="mui-textfield">
                                    <input type="text" class="nxt_detupld_rate" value="" disabled>
                                    <span class="title_box">Kbps</span>
                                    <label>Upload Rate<sup>*</sup></label>
                                 </div>
                              </div>
                           </div>
                           <div class ="row nxt_timeplan" style="margin-top:15px; display:none;">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding-left">
                                 <div class="mui-textfield" style="padding-top: 10px">
                                    <input type="text"  class="nxt_detplan_timelimit" value="" disabled>
                                    <label>Time Limit</label>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
                                 <h4>Time Calculated on: &nbsp;
                                    <span class="nxt_dettime_calculated_on" style="font-weight:normal; font-size:14px"></span>
                                 </h4>
                              </div>
                           </div>
                           <div class="row dataplan" style="margin-top: 15px; display:none;">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                 <div class="mui-textfield">
                                    <input type="text" class="nxt_detdata_limit" value="" disabled >
                                    <span class="title_box">Gbps</span>
                                    <label>Data Limit<sup>*</sup></label>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                 <h4>Date Calculated on: &nbsp;
                                    <span class="nxt_detdata_calcn" style="font-weight:normal; font-size:14px"></span>
                                 </h4>
                              </div>
                           </div>
                           <div class="row nxt_fupclass" style="display:none;">
                              <h4>Post FUP limit</h4>
                              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                 <div class="mui-textfield">
                                    <input type="text" value="" class="nxt_detfup_dwnl_rate" disabled>
                                    <span class="title_box">Kbps</span>
                                    <label>Download Rate<sup>*</sup></label>
                                 </div>
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                 <div class="mui-textfield">
                                    <input type="text" value="" class="nxt_detfup_upld_rate" disabled>
                                    <span class="title_box">Kbps</span>
                                    <label>Upload Rate<sup>*</sup></label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:25px">
                           <div class="row">
                              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                 <input type="button" class="mui-btn mui-btn--accent btn-lg btn-block" value="Apply Plan" style="padding:0" onclick="applyplan_fornextcycle()" />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

