<?php
$isp_detail = $this->user_model->get_isp_name();
$isp_name = $isp_detail['isp_name'];
$fevicon = $isp_detail['fevicon_icon'];
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <link rel="icon" href="<?php echo base_url()?>assets/images/<?php echo $fevicon?>" type="image/x-icon"/>
      <title><?php echo $isp_name; ?> </title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading">
	 <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                     <?php
                     $isplogo = $this->user_model->get_ispdetail_info();
                     if($isplogo != 0){
                        echo '<img src="'.base_url()."ispmedia/logo/".$isplogo.'" class="img-responsive"/>';
                     }else{
                        echo '<img src="'.base_url().'assets/images/decibel.png" class="img-responsive"/>';
                     }
                     ?>
                     </span>
                  </div>
                  <?php
		     $leftperm['navperm']=$this->user_model->leftnav_permission();
		     $this->view('left_nav',$leftperm);
		  ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <span class="navbar-brand">Users</span>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
			      <?php if($this->user_model->is_permission(CREATEUSER,'ADD')){ ?>
                              <li>
                                 <a href="javascript:void(0)" onclick="setup_adduser()">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD USER
                                 </button>
                                 </a>
                              </li>
			      <?php } ?>
			      <?php if($this->user_model->is_permission(PLANS,'ADD')){ ?>
                              <li>
                                 <a href="javascript:void(0)" onclick="check_tax_plan()">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD PLAN
                                 </button>
                                 </a>
                              </li>
			      <?php } ?>
			      <?php if($this->user_model->is_permission(TOPUP,'ADD')){ ?>
			      <li>
                                 <a href="javascript:void(0)" onclick="check_tax_topup()"">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD TOPUP
                                 </button>
                                 </a>
                              </li>
			      <?php }
			      $this->load->view('includes/global_setting',$leftperm);
			      ?>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <ul>
                                 <li id="active_users">
                                    <h5>ACTIVE USERS</h5>
                                    <h4><i class="fa fa-user" aria-hidden="true"></i> <?php echo $active_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li id="inactive_users">
                                    <h5>INACTIVE USERS</h5>
                                    <h4><i class="fa fa-wifi" aria-hidden="true"></i> <?php echo $inactive_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li id="lead_enquiry">
                                    <h5>LEADS & ENQUIRIES</h5>
                                    <h4><i class="fa fa-question-circle" aria-hidden="true"></i> <?php echo $lead_enquiry_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li style="background-color:#f4474a"  id="user_complaints">
                                    <h5>COMPLAINTS</h5>
                                    <h4><i class="fa fa-exclamation-circle" aria-hidden="true"></i> <?php echo $complaints?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li style="background-color:#f4474a" id="payment_overdue">
                                    <h5>PAYMENT OVERDUE</h5>
                                    <h4> <!--<i class="fa fa-inr" aria-hidden="true"></i>--><?php echo $ispcodet['currency'] ?> <?php echo $payment_dues?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
				 <li style="background-color:#f4474a" id="otherdues">
                                    <h5>OTHER DUES</h5>
                                    <h4> <!--<i class="fa fa-inr" aria-hidden="true"></i>--><?php echo $ispcodet['currency'] ?> <?php echo $otherdues?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                              </ul>
                           </div>
			   
			   <div id="active_inactive_results">
                              <div class="col-lg-12 col-md-12 col-sm-12">
				 
                                 <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                       <h1 id="search_count"></h1>
                                       <h3 style="margin-top:0px">
                                          Online: <span id="oncount"></span> | Offline: <span id="offcount"></span>
                                       </h3>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 pull-right">
                                       <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 14px;">
                                          <div class="form-group">
                                             <div class="input-group">
                                                <div class="input-group-addon">
                                                   <i class="fa fa-search" aria-hidden="true"></i>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Search Logs" onBlur="this.placeholder='Search Logs'" onFocus="this.placeholder=''"  id="searchtext">
                                                <span class="searchclear" id="searchclear">
                                                <img src="<?php echo base_url() ?>assets/images/clear.svg"/>
                                                </span>
                                             </div>
                                             <label class="search_label">You can look up by name, email, UID or mobile</label>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="row" id="onefilter">
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                       <div class="mui-select">
                                          <select name="filter_netstatus" onchange="search_filter()">
                                             <option value="">Status</option>
                                             <option value="online">Online</option>
                                             <option value="offline">Offline</option>
                                          </select>
                                          <label>User Status</label>
                                       </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                       <div class="mui-select">
                                          <select name="filter_locality" onchange="search_filter()">
                                             <option value="">User Category</option>
                                             <?php $this->user_model->usage_locality(); ?>
                                          </select>
                                          <label>User Category</label>
                                       </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                       <div class="mui-select">
                                          <select name="filter_state" onchange="search_filter(this.value, 'state')">
                                             <option value="">All States</option>
                                             <?php $this->user_model->state_list(); ?>
                                          </select>
                                          <label>State</label>
                                       </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                       <div class="mui-select">
                                          <select class="search_citylist"  onchange="search_filter(this.value, 'city')"  name="filter_city">
                                             <option value="">All Cities</option>
                                          </select>
                                          <label>City</label>
                                       </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                       <div class="mui-select">
                                          <select class="search_zonelist"  onchange="search_filter()"  name="filter_zone">
                                             <option value="">All Zones</option>
                                             <?php //$this->user_model->zone_list(); ?>
                                          </select>
                                          <label>Zone</label>
                                       </div>
                                    </div>
				    <div class="col-lg-2 col-md-2 col-sm-2 hide moreoptfilter">
                                       <div class="mui-select">
                                          <select name="filter_moreoptions" class="filter_moreoptions">
                                             <option value="">More Options</option>
                                             <option value="sendsms_tousers">Send SMS</option>
					     <option value="exportexcel_tousers">Export to Excel</option>
                                          </select>
                                          <label>More Options</label>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row" id="secondfilter" style="display:none">
                                    <div class="col-lg-5 col-md-5 col-sm-5">
                                       <div class="row">
                                          <div class="col-lg-6 col-md-6 col-sm-6 nopadding-right">
                                             <div class="switch-field">
                                                <input type="radio" id="switch_left" name="switch_2" value="active" checked/>
                                                <label for="switch_left">ACTIVE</label>
                                                <input type="radio" id="switch_right" name="switch_2" value="inactive" />
                                                <label for="switch_right" data-toggle="modal" data-target="#inactive">INACTIVE</label>
                                             </div>
                                          </div>
                                          <div class="col-lg-6 col-md-6 col-sm-6">
                                             <div class="mui-textfield">
                                                <input type="text" class="date" placeholder="dd.mm.yyyy">
                                                <label>Set Expiry</label>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                       <div class="view_user_right_list">
                                          <ul>
                                             <li><a href="#">Email Users</a></li>
                                             <li><a href="#">Reset password</a></li>
                                             <li><a href="#">Reset MAC</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="table-responsive">
                                    <table class="table table-striped">
                                       <thead style="font-size:11px">
                                          <tr class="active">
                                             <th>&nbsp;</th>
                                             <th>
                                                <div class="checkbox" style="margin-top:0px; margin-bottom:0px;">
                                                   <label>
                                                   <input type="checkbox" class="collapse_allcheckbox"> 
                                                   </label>
                                                </div>
                                             </th>
                                             <th>USERNAME</th>
                                             <th>FULL NAME</th>
                                             <th>STATE</th>
                                             <th>CITY</th>
                                             <th>ZONE</th>
                                             <th>PLAN</th>
                                             <th>ACTIVE FROM</th>
                                             <th>PAID TILL</th>
                                             <th>EXPIRE ON</th>
                                             <th>STATUS</th>
                                             <th>BALANCE</th>
                                             <th>TICKETS</th>
                                          </tr>
                                       </thead>
                                       <tbody id="search_gridview" style="font-size:11px"></tbody>
                                    </table>
                                 </div>
                              </div>
			      <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="row" style="text-align:center">
                                    <input type='hidden' id="plimit" value="" />
                                    <input type='hidden' id="poffset" value="" />
				    <input type='hidden' id="ploadmoredata" value="1" />
				    <input type="hidden" data-ajaxready='1' id="checkajaxonscroll" />
                                    <div class="ploadmore_loader hide">
                                       <img src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
                                    </div>
                                 </div>
                              </div>
                           </div>
			</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="taxerr" role="dialog"  data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-body" style="padding-bottom:5px">
		     <p id="taxmsg">Please enter Tax before.</p>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button"  class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button> 
		  </div>
	       </div>
	 </div>
      </div>
      
      <div class="modal fade" id="billingerr" role="dialog"  data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-body" style="padding-bottom:5px">
		     <p id="billerrmsg"></p>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button"  class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button> 
		  </div>
	       </div>
	 </div>
      </div>
      
      <div class="modal fade" id="sendsms_modal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
             <div class="modal-content">
                 <div class="modal-header">
                     <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                     <h4 class="modal-title"><strong>SEND SMS</strong></h4>
                 </div>
                 <form action="" method="post" id="sendsms_flyform" autocomplete="off" onsubmit="sendsms_tousers(); return false;">
		     <input type="hidden" name="filteruids" value="" />
                     <div class="modal-body" style="padding-bottom:5px">
                         <p>Send Greetings, SMS Alerts & Many more you want to.</p>
                         <div class="mui-textfield mui-textfield--float-label">
                             <input type="text" name="textmsg_tosend" class="textmsg_tosend" required>
                             <label>Message</label>
                         </div>
                     </div>
                     <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat bulksmsbtn" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff">CANCEL</button>
                        <input type="submit" class="mui-btn mui-btn--large mui-btn--accent bulksmsbtn" value="SEND MESSAGE">
			<span class="bulksmsloader"></span>
                     </div>
                 </form>
     
             </div>
         </div>
      </div>
      <div class="modal fade" id="trigger_successModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		  <h4 class="modal-title">&nbsp;</h4>
	       </div>
	       <div class="modal-body" style="padding-bottom:5px">
		  <p id="alertSuccessMsgTxt"></p>  
	       </div>
	       <div class="modal-footer" style="text-align: right">&nbsp;</div>
	    </div>
         </div>
      </div>

      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/material.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/misc.js?version=3.1"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/crbnMenu.js"></script>
      <script type="text/javascript">
         if ($(window)) {
            $(function () {
               $('.menu').crbnMenu({
               hideActive: true
               });
            });
         }
      </script>
      <script type="text/javascript">
          $('body').on('click', '#lead_enquiry', function(){
	    window.location.href = base_url+'user/lead_enquiry';
	 });
	 
	 $('body').on('click', '#user_complaints', function(){
	    window.location.href = base_url+'user/complaints';
	 });
	 
	 $('body').on('click', '#inactive_users', function(){
	    window.location.href = base_url+'user/status/inact';
	 });
	 
	 $('body').on('click', '#active_users', function(){
	    window.location.href = base_url+'user/status/act';
	 });
         
	 $('body').on('click', '#payment_overdue', function(){
	    window.location.href = base_url+'user/pay_overdue'; 
	 });
	 $('body').on('click', '#otherdues', function(){
	    window.location.href = base_url+'user/otherdues'; 
	 });
	 
         $('body').on('click','#searchclear', function(){
            $('#searchtext').val('');
            search_filter();
            
         });
         $('body').on('keyup', '#searchtext', function(){
            search_filter();
         });
         $('body').on('change', '.collapse_allcheckbox', function(){
            var status = this.checked;
            $('.collapse_checkbox').each(function(){ 
               this.checked = status;
            });
            if (status) {
               $('.moreoptfilter').removeClass('hide');
            }else{
	       $('.moreoptfilter').addClass('hide');
            }
         });
         
         $('body').on('change', '.collapse_checkbox', function(){
            if(this.checked == false){ 
               $(".collapse_allcheckbox")[0].checked = false; 
            }
            if ($('.collapse_checkbox:checked').length == $('.collapse_checkbox').length ){
               $(".collapse_allcheckbox")[0].checked = true; 
            }
            
            if ($('.collapse_checkbox:checked').length > 0) {
               $('.moreoptfilter').removeClass('hide');
            }else{
               $('.moreoptfilter').addClass('hide');
            }
         });
         
         $('body').on('change', '.collapse_allcheckbox', function(){
            var status = this.checked;
            $('.collapse_checkbox').each(function(){ 
               this.checked = status;
            });
         });
         
	 $('select.filter_moreoptions').on('change', function() {
	    var selfilter = $(this).val();
	    if (selfilter == 'sendsms_tousers') {
	       $('#sendsms_flyform')[0].reset();
	       $('.bulksmsbtn').removeClass('hide');
	       $('.bulksmsloader').html('');
	       $('input[name="filteruids"]').val('');
	       $('#sendsms_modal').modal('show');
	    }else if (selfilter == 'exportexcel_tousers') {
	       exportexcel_tousers();
	    }
	 });
	 
	 function sendsms_tousers() {
	    seluids = [];
	    $("input:checkbox[name=paydueuids]:checked").each(function(){
	       seluids.push($(this).val());
	    });
	    if (seluids.length > 0) {
	       $('.bulksmsbtn').addClass('hide');
	       $('.bulksmsloader').html('<img src="<?php echo base_url() ?>assets/images/loader.svg" width="8%" /> &nbsp; Please wait & Don\'t Refresh the Page.');
	       $('input[name="filteruids"]').val(seluids);
	       var formdata = $('#sendsms_flyform').serialize();
	       $.ajax({
		  url: base_url+'user/sendbulksms_tousers',
		  type: 'POST',
		  dataType: 'json',
		  data: formdata,
		  success: function(result){
		     $('#sendsms_modal').modal('hide');
		     $('#alertSuccessMsgTxt').html(result.gateway_message);
		     $('#trigger_successModal').modal('show');
		  }
	       });
	    }else{
	       $('.bulksmsbtn').removeClass('hide');
	       $('.bulksmsloader').html('');
	    }
	 }
	 
	 function exportexcel_tousers() {
	    seluids = [];
	    $("input:checkbox[name=paydueuids]:checked").each(function(){
	       seluids.push($(this).val());
	    });
	    if (seluids.length > 0) {
	       $.ajax({
		  url: base_url+'user/exportexcel_tousers',
		  type: 'POST',
		  data: 'seluids='+seluids,
		  success: function(result){
		     window.open(base_url+'assets/media/pdfexcel_dwnld/'+isp_uid+'_userlisting.xlsx','_blank' );
		  }
	       });
	    }
	 }
         
         function search_filter(data='', filterby=''){
            var searchtext = $('#searchtext').val();
            $('#search_panel').addClass('hide');
            $('#allsearch_results').removeClass('hide');
            $('.loading').css('display', 'block');
            
            var formdata = '';
            var city = $('select[name="filter_city"]').val();
            var zone = $('select[name="filter_zone"]').val();
            var locality = $('select[name="filter_locality"]').val();
            var netstatus = $('select[name="filter_netstatus"]').val();
            if (filterby == 'state') {
               getcitylist(data);
               formdata += '&state='+data+ '&city=&zone='+zone+'&locality='+locality+'&netstatus='+netstatus;
            }else if (filterby == 'city') {
               var state = $('select[name="filter_state"]').val();
               getzonelist(data);
               formdata += '&state='+state+'&city='+data+'&zone=&locality='+locality+'&netstatus='+netstatus;
            }else{
               var state = $('select[name="filter_state"]').val();
               formdata += '&state='+state+ '&city='+city+'&zone='+zone+'&locality='+locality+'&netstatus='+netstatus;
            }
            
            formdata += '&filtertype='+$('#filtertype').val();
            //alert(formdata);
            $.ajax({
               url: base_url+'user/payoverdue_userlist',
               type: 'POST',
               dataType: 'json',
               data: 'search_user='+searchtext+formdata,
               success: function(data){
                  $('.loading').css('display', 'none');
                  var filtertype = $('#filtertype').val();
                  $('#search_count').html('Total Users ('+data.total_results+' <small>Results</small>)');
                  $('#search_gridview').html(data.search_results);
               }
            });
         }
         
         $(document).ready(function() {
	    var height = $(window).height();
            $('#main_div').css('height', height);
	    //$('#right-container-fluid').css('height', height);
	    
            var total_results = "<?php echo $payoverdue['total_results']; ?>";
            var search_results = "<?php echo $payoverdue['search_results']; ?>" ;
            var oncount = "<?php echo $payoverdue['online_users'] ?>";
            var offcount = "<?php echo $payoverdue['offline_users'] ?>";
	    var paylimit = "<?php echo $payoverdue['limit'] ?>";
	    var payoffset = "<?php echo $payoverdue['offset'] ?>";
	    var payloadmore = "<?php echo $payoverdue['loadmore'] ?>";
            
            $('#search_count').html('Total Users ('+total_results+' <small>Results</small>)');
            $('#oncount').html(oncount);
            $('#offcount').html(offcount);
	    $('#search_gridview').html(search_results);
	    $('#active_inactive_results').removeClass('hide');
	    $('#plimit').val(paylimit); $('#poffset').val(payoffset);
	    $('.loading').addClass('hide');

         });
      </script>
      <script type="text/javascript">
	 $(document).ready(function() {
	    $(window).scroll(function(){
	       //if last scroll is not done, don't start another one.
	       if ($('#checkajaxonscroll').data('ajaxready') == 0){
		  return;
	       }
	       var loaded = $('#ploadmoredata').val();	       
	       if ($(window).scrollTop() == $(document).height() - $(window).height() && loaded != 0){

		  $('#checkajaxonscroll').data('ajaxready', 0);
		  $('.ploadmore_loader').removeClass('hide');
		  var limit = $('#plimit').val();
		  var offset = $('#poffset').val();
		  
		  var formdata = '';
		  var searchtext = $('#searchtext').val();
		  var state = $('select[name="filter_state"]').val();
		  var city = $('select[name="filter_city"]').val();
		  var zone = $('select[name="filter_zone"]').val();
		  var locality = '';
		  var netstatus = $('select[name="filter_netstatus"]').val();
		  if (typeof netstatus != 'undefined') {
		     netstatus = $('select[name="filter_netstatus"]').val();
		  }else{
		     netstatus = '';
		  }
		  var sortlistby = $('#sortby').val();
		  var orderby = $('#orderby').val();
		  var param = "<?php echo $filtertype; ?>";
		  formdata += '&state='+state+ '&city='+city+'&zone='+zone+'&locality='+locality+'&netstatus='+netstatus;
		  
		  $.ajax({
		     url: base_url+'user/payoverdue_userlist',
		     type: 'POST',
		     dataType: 'json',
		     data: 'search_user='+searchtext+'&limit='+limit+'&offset='+offset+formdata,
		     success: function(data){
			$('.loading').css('display', 'none');
			var filtertype = $('#filtertype').val();
			$('#search_count').html('Total Users ('+data.total_results+' <small>Results</small>)');
			$('#search_gridview').append(data.search_results);
			var nxtlimit = data.limit;
			var nxtofset = data.offset;
			$('#plimit').val(nxtlimit); $('#poffset').val(nxtofset);
			$('#ploadmoredata').val(data.loadmore);
			$('.ploadmore_loader').addClass('hide');
			$('#checkajaxonscroll').data('ajaxready', 1);
		     }
		  });
	       }
	    });
	 });
      </script>
      <script type="text/javascript">
         function migrate_tobilling(uid) {
            window.location.href = base_url+'user/edit/'+uid+'/b';
         }
         function migrate_totickets(uid) {
            window.location.href = base_url+'user/edit/'+uid+'/t';
         }
      </script>
      <script type="text/javascript">
	 /************** PERMISSION SETTING ***********/
	 $(document).ready(function() {
	    <?php
	       $superadmin = $this->session->userdata['isp_session']['super_admin'];
	       $active_hideperm = $this->user_model->is_permission('ACTIVEUSERS','HIDE');
	       $inactive_hideperm = $this->user_model->is_permission('INACTIVEUSERS','HIDE');
	       $leadenqry_hideperm = $this->user_model->is_permission('LEADENQUIRYUSERS','HIDE');
	       $complaint_hideperm = $this->user_model->is_permission('COMPLAINTSUSERS','HIDE');
	       $paymentdue_hideperm = $this->user_model->is_permission('PAYMENTDUEUSERS','HIDE');
	       $otherdue_hideperm = $this->user_model->is_permission('OTHERDUESUSERS','HIDE');
	    ?>   
	    <?php if(($active_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#active_users').addClass('hide');
	    <?php }else{ ?>
		  $('li#active_users').removeClass('hide');
	    <?php } ?>
	    <?php if(($inactive_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#inactive_users').addClass('hide');
	    <?php }else{ ?>
		  $('li#inactive_users').removeClass('hide');
	    <?php } ?>
	    <?php if(($leadenqry_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#lead_enquiry').addClass('hide');
	    <?php }else{ ?>
		  $('li#lead_enquiry').removeClass('hide');
	    <?php } ?>
	    <?php if(($complaint_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#user_complaints').addClass('hide');
	    <?php }else{ ?>
		  $('li#user_complaints').removeClass('hide');
	    <?php } ?>
	    <?php if(($paymentdue_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#payment_overdue').addClass('hide');
	    <?php }else{ ?>
		  $('li#payment_overdue').removeClass('hide');
	    <?php } ?>
	    <?php if(($otherdue_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#otherdues').addClass('hide');
	    <?php }else{ ?>
		  $('li#otherdues').removeClass('hide');
	    <?php } ?>
	 });
      </script>
   </body>
</html>