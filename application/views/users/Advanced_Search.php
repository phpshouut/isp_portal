
<div class="col-lg-12 col-md-12 col-sm-12">
   <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8">
         <h1 id="advsearch_count"></h1>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 pull-right">
         <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 14px;">
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon">
                     <i class="fa fa-search" aria-hidden="true"></i>
                  </div>
                  <input type="text" class="form-control" placeholder="Search Logs" onBlur="this.placeholder='Search Logs'" onFocus="this.placeholder=''"  id="advsearchtext">
                  <span class="searchclear" id="advsearchclear">
                  <img src="<?php echo base_url() ?>assets/images/clear.svg"/>
                  </span>
               </div>
               <label class="search_label">You can look up by name, email, UID or mobile</label>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
   <input type="hidden" id="usertype_advsearch" />
   <div class="row" id="onefilter">
      <div class="col-lg-8 col-md-8 col-sm-8">
         <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select name="advfilter_locality" onchange="advsearch_filter()">
                     <option value="">Usuage Locality*</option>
                     <?php $this->user_model->usage_locality(); ?>
                  </select>
                  <label>Usuage Locality</label>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select name="advfilter_state" onchange="advsearch_filter(this.value, 'state')">
                     <option value="">All States</option>
                     <?php $this->user_model->state_list(); ?>
                  </select>
                  <label>State</label>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select class="search_citylist"  onchange="advsearch_filter(this.value, 'city')"  name="advfilter_city">
                     <option value="">All Cities</option>
                  </select>
                  <label>City</label>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select class="search_zonelist"  onchange="advsearch_filter()"  name="advfilter_zone">
                     <option value="">All Zones</option>
                  </select>
                  <label>Zone</label>
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
         <div class="row pull-right">
            <div class="col-lg-12 col-md-12 col-sm-12">
               <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                  <div class="row">
                     <h4 class="toggle_heading">Show Priority</h4>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                  <label class="switch">
                     <input type="checkbox">
                     <div class="slider round"></div>
                  </label>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="row" id="secondfilter" style="display:none">
      <div class="col-lg-5 col-md-5 col-sm-5">
         <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
               <div class="switch-field">
                  <input type="radio" id="switch_left" name="switch_2" value="active" checked/>
                  <label for="switch_left">ACTIVE</label>
                  <input type="radio" id="switch_right" name="switch_2" value="inactive" />
                  <label for="switch_right" data-toggle="modal" data-target="#inactive">INACTIVE</label>
               </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
               <div class="mui-textfield">
                  <input type="text" class="date" placeholder="dd.mm.yyyy">
                  <label>Set Expiry</label>
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-7 col-md-7 col-sm-7">
         <div class="view_user_right_list">
            <ul>
               <li><a href="#">Email Users</a></li>
               <li><a href="#">Reset password</a></li>
               <li><a href="#">Reset MAC</a></li>
            </ul>
         </div>
      </div>
   </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
   <div class="table-responsive">
      <table class="table table-striped">
         <thead>
            <tr class="active">
               <th>&nbsp;</th>
               <th>
                  <div class="checkbox" style="margin-top:0px; margin-bottom:0px;">
                     <label>
                     <input type="checkbox" class="collapse_allcheckbox"> 
                     </label>
                  </div>
               </th>
               <th>USERNAME</th>
               <th>FULL NAME</th>
               <th>STATE</th>
               <th>CITY</th>
               <th>ZONE</th>
               <th>PLAN</th>
               <th>ACTIVE FROM</th>
               <th>PAID TILL</th>
               <th>EXPIRE ON</th>
               <th>STATUS</th>
               <th>BALANCE</th>
               <th>TICKETS</th>
            </tr>
         </thead>
         <tbody id="advsearch_gridview"></tbody>
      </table>
   </div>
</div>


<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
   $('body').on('click','#advsearchclear', function(){
      $('#advsearchtext').val('');
      advsearch_filter();
      
   });
   $('body').on('keyup', '#advsearchtext', function(){
      advsearch_filter();
   });
   
   /*$('body').on('change', '.collapse_allcheckbox', function(){
      var status = this.checked;
      $('.collapse_checkbox').each(function(){ 
         this.checked = status;
      });
      if (status) {
         $('#onefilter').css('display', 'none');
         $('#secondfilter').css('display', 'block');
      }else{
         $('#onefilter').css('display', 'block');
         $('#secondfilter').css('display', 'none');
      }
   });
   
    $('body').on('change', '.collapse_checkbox', function(){
      if(this.checked == false){ 
         $(".collapse_allcheckbox")[0].checked = false; 
      }
      if ($('.collapse_checkbox:checked').length == $('.collapse_checkbox').length ){
         $(".collapse_allcheckbox")[0].checked = true; 
      }
      
      if ($('.collapse_checkbox:checked').length > 0) {
         $('#onefilter').css('display', 'none');
         $('#secondfilter').css('display', 'block');
      }else{
         $('#onefilter').css('display', 'block');
         $('#secondfilter').css('display', 'none');
      }
   });*/
   
   $('body').on('change', '.collapse_allcheckbox', function(){
      var status = this.checked;
      $('.collapse_checkbox').each(function(){ 
         this.checked = status;
      });
   });
   
   function advsearch_filter(data='', filterby=''){
      var searchtext = $('#advsearchtext').val();
      $('#search_panel').addClass('hide');
      $('#allsearch_results').addClass('hide');
      $('.loading').removeClass('hide')
      
      var formdata = '';
      var advsearch_category = $('#usertype_advsearch').val();
      var city = $('select[name="advfilter_city"]').val();
      var zone = $('select[name="advfilter_zone"]').val();
      var locality = $('select[name="advfilter_locality"]').val();
      if (filterby == 'state') {
         getcitylist(data);
         formdata += '&advsearch_state='+data+ '&advsearch_city=&advsearch_zone='+zone+'&advsearch_locality='+locality;
      }else if (filterby == 'city') {
         var state = $('select[name="filter_state"]').val();
         getzonelist(data);
         formdata += '&advsearch_state='+state+'&advsearch_city='+data+'&advsearch_zone=&advsearch_locality='+locality;
      }else{
         var state = $('select[name="filter_state"]').val();
         formdata += '&advsearch_state='+state+ '&advsearch_city='+city+'&advsearch_zone='+zone+'&advsearch_locality='+locality;
      }

      //alert(formdata);
      $.ajax({
         url: base_url+'user/advanced_search',
         type: 'POST',
         dataType: 'json',
         data: 'search_user='+searchtext+'&advsearch_category='+advsearch_category+formdata,
         success: function(data){
            $('#advsearch_results').removeClass('hide');
            $('.loading').addClass('hide')
            
            $('select[name="advfilter_locality"]').val(data.locality);
            $('select[name="advfilter_state"]').val(data.state);
            $('select[name="advfilter_city"]').val(data.city);
            $('select[name="advfilter_zone"]').val(data.zone);
            $('#advsearch_count').html(data.total_results+' <small>Results</small>');
            $('#advsearch_gridview').html(data.search_results);
         }
      });
   }
</script>