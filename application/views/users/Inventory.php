
<?php $category=$this->plan_model->inventory_category_list();
$inventory_list=$this->plan_model->inventory_list($uid);
//echo "<pre>"; print_R($inventory_list);//die;
?>
<div class="col-sm-12 col-xs-12" style="margin-bottom: 15px;">
      <div class="row">
         <div class="col-sm-12 col-xs-12">
            <h2>Manage user inventory</h2>
	     <form action="" method="post" id="add_user_inventory" autocomplete="off" onsubmit="add_user_inventory(); return false;">
            <input type="hidden" name="subscriber_uid" value="<?php echo (isset($uid))?$uid:"";?>">
               <div class="row">
                 
                  <div class="col-lg-3 col-md-3 col-sm-3">
                    
                        
                            <select class="category_name form-control" name="category_name" required>
                               <option value="" >Select Category</option>
			       <?php foreach($category as $val){?>
			       <option value="<?php echo $val->id;?>" ><?php echo $val->category_name;?></option>
			       <?php } ?>
                                
                                 </select>
                             
                            
                      </div>
                  <div class="col-sm-3 col-xs-3" style="padding-top: 5px;">
                     <button class="mui-btn mui-btn--accent mui-btn--small" data-toggle="modal" data-target="#addcategory">Add Category </button>
                  </div>
               </div>
               <div class="row">
                  
                  <div class="col-lg-3 col-md-3 col-sm-3">
                  
                        
                            <select class="brand_name form-control" name="brand_name" required>
                            <option value="">Select Brand</option>
                                 </select>
                              
                          
                  </div>
                  <div class="col-sm-3 col-xs-3" style="padding-top: 5px;">
                     <button class="mui-btn mui-btn--accent mui-btn--small" data-toggle="modal" data-target="#addbrand">Add Brand </button>
                  </div>
               </div>
               
               <div class="row">
                  
                   <div class="col-lg-3 col-md-3 col-sm-3">
               
                   
                            <select class="prod_name form-control" name="prod_name" required>
                               <option value="">Select Product</option>
                                
                                 </select>
                              
                       
                  </div>
                  <div class="col-sm-3 col-xs-3" style="padding-top: 5px;">
                     <button class="mui-btn mui-btn--accent mui-btn--small" data-toggle="modal" data-target="#add_product">Add Product </button>
                  </div>
               </div>
               
               <div class="row">
                  
                   <div class="col-lg-3 col-md-3 col-sm-3">
                    
                       <div class="mui-textfield mui-textfield--float-label">
                        <input class="mui--is-empty mui--is-untouched mui--is-pristine" name="unit" type="text" required>
                        <label>Enter units/length/weight</label>
                     </div>
                           
                  </div>
               </div>
	       
	        <div class="row">
                  
                   <div class="col-lg-3 col-md-3 col-sm-3">
                   
                       <div class="mui-textfield mui-textfield--float-label">
                        <input class="mui--is-empty mui--is-untouched mui--is-pristine" name="serial_number" type="text" required>
                        <label>Serial Number<sup>*</sup></label>
                     </div>
                            
                  </div>
               </div>
	       
	        
                <div class="form-group">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
                           <div class="row">
                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                               <input type="submit" class="mui-btn mui-btn--accent btn-lg " value="Add Product">
                               </div>
                              </div>
                              </div>
                          </div>
	     </form>
          
         </div>
      </div>
      <div class="row">
		 <div class="table-responsive">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr class="active">
                                                                   
                                                                    <th>CATEGORY</th>
                                                                     <th>BRAND</th>
                                                                    <th>PRODUCT NAME </th>
                                                                    <th>SERIAL NUMBER</th>
                                                                    <th>COST PRICE</th>
								    <th>SALE PRICE</th>
								     <th>STATUS</th>
                                                                    <th colspan="3" class="mui--text-center">ACTIONS</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id='user_gridview'>
								   <?php
								   foreach($inventory_list as $val){
								    $isreplaced=($val->is_replaced==1)?"Replaced":"-";
								    ?>
								    
								    <tr>
										     <td class='rcat'><?php echo $val->category_name;?></td>
										     <td class='rbrand'><?php echo $val->brand_name;?></td>
										     <td class='rproduct'><?php echo $val->product_name;?></td>
										     <td class='rserial'><?php echo $val->serial_number;?></td>
										     <td class='rprice'><?php echo $val->cost_price*$val->unit_no;?></td>
										     <td class='saleprice'><?php echo $val->sales_price*$val->unit_no;?></td>
										     <td class='status'><?php echo $isreplaced;?></td>
										     <td> <a href="javascript:void(0);" class="invtry_readable edituser" id="edituser_<?php echo $val->id;?>" rel="<?php echo $val->id;?>">Edit</a></td> 
                                                                        <td><a href="javascript:void(0);" class="invtry_readable deletuser"  rel="<?php echo $val->id;?>">Delete</a></td>
									 <td><a href="javascript:void(0);" class="invtry_readable replaceuser"  rel="<?php echo $val->id;?>"><?php if($val->is_replaced==0){?>Replace<?php } ?></a></td> 
								    </tr>
								   
								   <?php } ?>
								 </tbody>
                                                        </table>
		 </div>
      </div>
   </div>


<!-- ADD CATEGORY MODAL -->
      <div class="modal fade" tabindex="-1" role="dialog" id="addcategory" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" style="font-size:30px;">
							<center><strong>Add Category</strong></center>
					    </h4>
	       </div>
               <div class="modal-body">

                  <div class="row">
					  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
					  <form action="" method="post" id="add_category" autocomplete="off" onsubmit="add_category(); return false;">
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h2 style="font-size: 16px; margin-bottom:0px; margin-top:15px; font-weight:400; ">Category Name </h2>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="mui-textfield mui-textfield--float-label">
                           <input type="text" name="cat_name" value="">
                           <label>Category Name<sup>*</sup></label>
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                  </div>
               </div>
               <div class="modal-footer">
                 <center>
                  <button type="button addcategory" class="mui-btn mui-btn--large mui-btn--accent catadd">ADD</button>
				   </center>
               </div></form>
            </div>
            <!-- /.modal-content -->
         </div>
      </div>
      <!-- ADD CATEGORY MODAL -->
      
      <!-- EDIT USER MODAL -->
      <div class="modal fade" tabindex="-1" role="dialog" id="edit_user" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" style="font-size:30px;">
							<center><strong>Edit User Inventory</strong></center>
					    </h4>
	       </div>
               <div class="modal-body appendedit">
                
               </div>
               <div class="modal-footer">
                 
               </div>
            </div>
            <!-- /.modal-content -->
         </div>
      </div>
      <!-- ADD CATEGORY MODAL -->
      
      <!-- ADD BRAND MODAL MODAL -->
      <div class="modal fade" tabindex="-1" role="dialog" id="addbrand" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" style="font-size:30px;">
							<center><strong>Add Brand</strong></center>
					    </h4>
	       </div>
	      - <form action="" method="post" id="add_brand" autocomplete="off" onsubmit="add_brand(); return false;">
               <div class="modal-body">
		 
                 
                  <div class="row">
				 
					  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h2 style="font-size: 16px;  margin-bottom:0px; margin-top:15px; font-weight:400; ">Select Category</h2>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                         
                           <select class="cat_namemodal form-control" name="category_name" required>
                             <?php foreach($category as $val){?>
			       <option value="<?php echo $val->id;?>" ><?php echo $val->category_name;?></option>
			       <?php } ?>
                           </select>
                          
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
		     
                  </div>
                   <div class="row">
					  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h2 style="font-size: 16px;  margin-bottom:0px; margin-top:15px; font-weight:400; ">Brand Name</h2>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="mui-textfield mui-textfield--float-label">
                           <input type="text" name="brand_name" value="">
                           <label>Brand Name<sup>*</sup></label>
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                  </div>
               </div>
	       
               <div class="modal-footer">
                 <center>
                  <button type="button addbrand" class="mui-btn mui-btn--large mui-btn--accent brandadd">ADD</button>
				   </center>
               </div></form>
            </div>
            <!-- /.modal-content -->
         </div>
      </div>
      <!-- ADD BRAND MODAL -->
      
      
      <div class="modal fade" tabindex="-1" role="dialog" id="add_product" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog" style="margin-top:0px;">
          
	    <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" style="font-size:30px;">
							<center><strong>Add Product</strong></center>
					    </h4>
               </div>
	       <form action="" method="post" id="add_prodform" autocomplete="off" onsubmit="add_product(); return false;">
               <div class="modal-body">

                  <div class="row">
					  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h2 style="font-size: 16px;  margin-bottom:0px; margin-top:8px; font-weight:400;">Select Category</h2>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        
                          
                              <select class="cat_namemodal form-control" name="category_name" required>
				  <option value="">Select Category</option>
                             <?php foreach($category as $val){?>
			       <option value="<?php echo $val->id;?>" ><?php echo $val->category_name;?></option>
			       <?php } ?>
                           </select>
                     
                           
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                  </div>
                  <div class="row">
					  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h2 style="font-size: 16px;  margin-bottom:0px; margin-top:8px; font-weight:400;">Select Brand</h2>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                         
                           <select class="brand_selmodal form-control" name="brand_name" required>
                              <option value="">Select Brand</option>
                           </select>
                          
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                  </div>
                   <div class="row">
					  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h2 style="font-size: 16px;  margin-bottom:0px; margin-top:8px; font-weight:400;">Product Name</h2>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="mui-textfield mui-textfield--float-label">
                           <input type="text" name="product_name" required>
                           <label>Product Name<sup>*</sup></label>
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                  </div>
                  <div class="row">
		     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h2 style="font-size: 16px;  margin-bottom:0px; margin-top:8px; font-weight:400;">Model Name</h2>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="mui-textfield mui-textfield--float-label">
                           <input type="text" name="model_name" value="" required>
                           <label>Model Name<sup>*</sup></label>
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                  </div>
		  
		  <div class="row">
		     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h2 style="font-size: 16px;  margin-bottom:0px; margin-top:8px; font-weight:400;">Cost Price </h2>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="mui-textfield mui-textfield--float-label">
                           <input type="number" name="cost_price" value="" required>
                           <label>Cost Price<sup>*</sup></label>
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                  </div>
		  
		  <div class="row">
		     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h2 style="font-size: 16px;  margin-bottom:0px; margin-top:8px; font-weight:400;">Sales Price </h2>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="mui-textfield mui-textfield--float-label">
                           <input type="number" name="sale_price" value="" required>
                            <label>Sales Price<sup>*</sup></label>
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                  </div>
                  
                   
                
                  <div class="row">
					  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h2 style="font-size: 16px;  margin-bottom:0px; margin-top:8px; font-weight:400;">Price Calculation</h2>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <select class="prod_name form-control" name="unit" required>
                               <option value="unit">Per Unit</option>
                                <option value="meter">Per Meter</option>
				 <option value="kg">Per KG</option>
                                 </select>
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                  </div>
               </div>
               <div class="modal-footer">
                 <center>
                  <button type="button add_product" class="mui-btn mui-btn--large mui-btn--accent prodadd">ADD</button>
				   </center>
               </div>
	       </form>
            </div>
	    
            <!-- /.modal-content -->
         </div>
      </div>
      
      
      <div class="modal fade" id="Deleteuser_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
          <input type="hidden" id="deluserid" value="">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>Are you sure you want to Delete Inventory ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyes" >YES</button>
               </div>
            </div>
         </div>
      </div>
      
      <div class="modal fade" id="replaceuser_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
          <input type="hidden" id="repuserid" value="">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>Are you sure you want to Replace Inventory ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent repyes" >YES</button>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript">
		 
		 $(document).ready(function(){
			$(document).on('change','.cat_namemodal',function(){
				  $('.loading').removeClass('hide');
				  // var formdata = $("#add_product").serialize();
				  var catid=$(this).val();
				  $.ajax({
				  url: base_url+'plan/brand_list',
				  type: 'POST',
				  dataType: 'json',
				  data: {catid:catid},
				  success: function(data){
				    //  alert(data.html);
				    $('.brand_selmodal').html("<option value=''>Select Brand</option>"+data.html);
					 $('.loading').addClass('hide');	
				    
				  }
				    });
				  
				  });
			
			
			$(document).on('change','.category_name',function(){
				   $('.loading').removeClass('hide');
				  // var formdata = $("#add_product").serialize();
				  var catid=$(this).val();
				  $.ajax({
				  url: base_url+'plan/brand_list',
				  type: 'POST',
				  dataType: 'json',
				  data: {catid:catid},
				  success: function(data){
				    //  alert(data.html);
				    $('.brand_name').html("<option value=''>Select Brand</option>"+data.html);
					 $('.loading').addClass('hide');	
				    
				  }
				    });
				  
				  });
			
			
			$(document).on('change','.brand_name',function(){
				  // var formdata = $("#add_product").serialize();
				   $('.loading').removeClass('hide');
				  var brandid=$(this).val();
				  $.ajax({
				  url: base_url+'plan/product_list',
				  type: 'POST',
				  dataType: 'json',
				  data: {brandid:brandid},
				  success: function(data){
				    //  alert(data.html);
				    $('.prod_name').html("<option value=''>Select Product</option>"+data.html);
				   $('.loading').addClass('hide');
				    
				  }
				    });
				  
				  });
			
			
			$(document).on('click','.edituser',function(){
				  $('.loading').removeClass('hide');
				  var id = $(this).attr('rel');
				   $.ajax({
				  url: base_url+'plan/edit_user_inventory',
				  type: 'POST',
				  dataType: 'json',
				  data: {id:id},
				  success: function(data){
				      //alert();
				      $('.appendedit').html(data.html);
				      $('#edit_user').modal('show');
				      $('.loading').addClass('hide');
				    
				  }
				    });
				  
				  
				  });
			
			
			 $(document).on('click','.deletuser',function(){
             $('.loading').removeClass('hide');
             var deluserid=$(this).attr('rel');
             $('#deluserid').val(deluserid);
             $('#Deleteuser_confirmation_alert').modal('show');
	     $('.loading').addClass('hide');
             
             
         });
         
         $(document).on('click','.delyes',function(){
		 $('.loading').removeClass('hide');
           var $this =$(this);
           var userid=$('#deluserid').val();
             $.ajax({
        url: base_url+'plan/delete_inventoryuser',
        type: 'POST',
        dataType: 'json',
        data: {userid:userid},
        success: function(data){
         
            $('#Deleteuser_confirmation_alert').modal('hide');
      
        $('#edituser_'+userid).closest('tr').remove();
	$('.loading').addClass('hide');
           
        
        }
    });
             
         });
	 
	  $(document).on('click','.replaceuser',function(){
             $('.loading').removeClass('hide');
             var repuserid=$(this).attr('rel');
             $('#repuserid').val(repuserid);
             $('#replaceuser_confirmation_alert').modal('show');
	     $('.loading').addClass('hide');
             
             
         });
         
         $(document).on('click','.repyes',function(){
		 $('.loading').removeClass('hide');
           var $this =$(this);
           var userid=$('#repuserid').val();
             $.ajax({
        url: base_url+'plan/replace_inventoryuser',
        type: 'POST',
        dataType: 'json',
        data: {userid:userid},
        success: function(data){
         
            $('#replaceuser_confirmation_alert').modal('hide');
      
        $('#edituser_'+userid).closest('tr').find('.status').html("Replaced");
	$('#edituser_'+userid).closest('tr').find('.replaceuser').css("display","none");
	$('.loading').addClass('hide');
           
        
        }
    });
             
         });
	 
	 
			
			
				  
				  
		 });
		 
		 
		 
		  function add_category()
     {
         $('.loading').removeClass('hide');
	 $('.catadd').prop("disabled",true);
         //alert('zzzzzzzz');
        var formdata = $("#add_category").serialize();
        // alert(formdata);
         $.ajax({
        url: base_url+'plan/add_inventory_category',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
          //  alert(data.html);
	  $('.category_name').append(data.html);
	  $('.cat_namemodal').append(data.html);
	  
                      //$("input[class='category_name'][value='"+data.city_id+"']").closest('.row').find('.zone_list').append(data.html);
                      $('.loading').addClass('hide');
		      $('.catadd').prop("disabled",true);
                        $('#addcategory').modal('hide'); 
              // alert($("input[class='citysel'][value='"+data.city_id+"']").closest('.row').html());
          
        }
    });
         //alert(formdata);
     }
     
     
      function add_brand()
     {
         $('.loading').removeClass('hide');
	 $('.brandadd').prop("disabled",true);
         //alert('zzzzzzzz');
        var formdata = $("#add_brand").serialize();
        // alert(formdata);
         $.ajax({
        url: base_url+'plan/add_inventory_brand',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
          //  alert(data.html);
	  $('.brand_name').append(data.html);
                      //$("input[class='category_name'][value='"+data.city_id+"']").closest('.row').find('.zone_list').append(data.html);
                      $('.loading').addClass('hide');
		      $('.brandadd').prop("disabled",true);
                        $('#addbrand').modal('hide'); 
              // alert($("input[class='citysel'][value='"+data.city_id+"']").closest('.row').html());
          
        }
    });
         //alert(formdata);
     }
     
     
     
     
     function add_product()
     {
         $('.loading').removeClass('hide');
	 //$('.prodadd').prop("disabled",true);
         //alert('zzzzzzzz');
        var formdata = $("#add_prodform").serialize();
       //  alert(formdata);
         $.ajax({
        url: base_url+'plan/add_inventory_product',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
          //  alert(data.html);
	  $('.prod_name').append(data.html);
                      //$("input[class='category_name'][value='"+data.city_id+"']").closest('.row').find('.zone_list').append(data.html);
                      $('.loading').addClass('hide');
		     // $('.prodadd').prop("disabled",true);
                        $('#add_product').modal('hide'); 
              // alert($("input[class='citysel'][value='"+data.city_id+"']").closest('.row').html());
          
        }
    });
         //alert(formdata);
     }
     
     function add_user_inventory() {
	$('.loading').removeClass('hide');	
        var formdata = $("#add_user_inventory").serialize();
        // alert(formdata);
         $.ajax({
        url: base_url+'plan/add_user_inventory',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
         
               $('#user_gridview').append(data.html);
	       $('.loading').addClass('hide');
          
        }
    });
     }
     
     function edit_submituser_inventory() {
		 $('.loading').removeClass('hide');
	  var formdata = $("#edit_user_inventory").serialize();
	  var $this=$(this);
        // alert(formdata);
         $.ajax({
        url: base_url+'plan/edit_user_inventory_submit',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
          //  alert(data.html);
	 $('#edituser_'+data.id).closest('tr').find('.rcat').html(data.rcat);
	 $('#edituser_'+data.id).closest('tr').find('.rbrand').html(data.rbrand);
	 $('#edituser_'+data.id).closest('tr').find('.rproduct').html(data.rproduct);
	 $('#edituser_'+data.id).closest('tr').find('.rserial').html(data.rserial);
	 $('#edituser_'+data.id).closest('tr').find('.rprice').html(data.rprice);
	  $('.loading').addClass('hide');
	 // $('.prod_name').append(data.html);
           $('#edit_user').modal('hide');        
          
        }
    });	
     }
     
     
   
</script>