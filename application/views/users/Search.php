
<div class="col-lg-12 col-md-12 col-sm-12">
   <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8">
         <h1 id="search_count"></h1>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 pull-right">
         <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 14px;">
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon">
                     <i class="fa fa-search" aria-hidden="true"></i>
                  </div>
                  <input type="text" class="form-control" placeholder="Search Logs" onBlur="this.placeholder='Search Logs'" onFocus="this.placeholder=''"  id="searchtext">
                  <span class="searchclear" id="searchclear">
                  <img src="<?php echo base_url() ?>assets/images/clear.svg"/>
                  </span>
               </div>
               <label class="search_label">You can look up by name, email, UID or mobile</label>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
   <div class="row" id="onefilter">
      <div class="col-lg-8 col-md-8 col-sm-8">
         <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select name="filter_locality" onchange="search_filter()">
                     <option value="">User Category*</option>
                     <?php $this->user_model->usage_locality(); ?>
                  </select>
                  <label>User Category</label>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select name="filter_state" onchange="search_filter(this.value, 'state')">
                     <option value="">All States</option>
                     <?php $this->user_model->state_list(); ?>
                  </select>
                  <label>State</label>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select class="search_citylist"  onchange="search_filter(this.value, 'city')"  name="filter_city">
                     <option value="">All Cities</option>
                  </select>
                  <label>City</label>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select class="search_zonelist"  onchange="search_filter()"  name="filter_zone">
                     <option value="">All Zones</option>
                  </select>
                  <label>Zone</label>
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
         <div class="row pull-right">
            <div class="col-lg-12 col-md-12 col-sm-12">
               <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                  <div class="row">
                     <h4 class="toggle_heading">Show Priority</h4>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                  <label class="switch">
                     <input type="checkbox">
                     <div class="slider round"></div>
                  </label>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="row" id="secondfilter" style="display:none">
      <div class="col-lg-5 col-md-5 col-sm-5">
         <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 nopadding-right">
               <div class="switch-field">
                  <input type="radio" id="switch_left" name="switch_2" value="active" checked/>
                  <label for="switch_left">ACTIVE</label>
                  <input type="radio" id="switch_right" name="switch_2" value="inactive" />
                  <label for="switch_right" data-toggle="modal" data-target="#inactive">INACTIVE</label>
               </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
               <div class="mui-textfield">
                  <input type="text" class="date" placeholder="dd.mm.yyyy">
                  <label>Set Expiry</label>
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-7 col-md-7 col-sm-7">
         <div class="view_user_right_list">
            <ul>
               <li><a href="#">Email Users</a></li>
               <li><a href="#">Reset password</a></li>
               <li><a href="#">Reset MAC</a></li>
            </ul>
         </div>
      </div>
   </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
   <div class="table-responsive">
      <table class="table table-striped">
         <thead>
            <tr class="active">
               <th>&nbsp;</th>
               <th>
                  <div class="checkbox" style="margin-top:0px; margin-bottom:0px;">
                     <label>
                     <input type="checkbox" class="collapse_allcheckbox"> 
                     </label>
                  </div>
               </th>
               <th>USERNAME</th>
               <th>FULL NAME</th>
               <th>STATE</th>
               <th>CITY</th>
               <th>ZONE</th>
               <th>PLAN</th>
               <th>ACTIVE FROM</th>
               <th>PAID TILL</th>
               <th>EXPIRE ON</th>
               <th>STATUS</th>
               <th>BALANCE</th>
               <th>TICKETS</th>
            </tr>
         </thead>
         <tbody id="search_gridview"></tbody>
      </table>
   </div>
</div>


<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
   $('body').on('click','#searchclear', function(){
      $('#searchtext').val('');
      search_filter();
      
   });
   $('body').on('keyup', '#searchtext', function(){
      search_filter();
   });
   
   /*$('body').on('change', '.collapse_allcheckbox', function(){
      var status = this.checked;
      $('.collapse_checkbox').each(function(){ 
         this.checked = status;
      });
      if (status) {
         $('#onefilter').css('display', 'none');
         $('#secondfilter').css('display', 'block');
      }else{
         $('#onefilter').css('display', 'block');
         $('#secondfilter').css('display', 'none');
      }
   });
   
    $('body').on('change', '.collapse_checkbox', function(){
      if(this.checked == false){ 
         $(".collapse_allcheckbox")[0].checked = false; 
      }
      if ($('.collapse_checkbox:checked').length == $('.collapse_checkbox').length ){
         $(".collapse_allcheckbox")[0].checked = true; 
      }
      
      if ($('.collapse_checkbox:checked').length > 0) {
         $('#onefilter').css('display', 'none');
         $('#secondfilter').css('display', 'block');
      }else{
         $('#onefilter').css('display', 'block');
         $('#secondfilter').css('display', 'none');
      }
   });*/
   $('body').on('change', '.collapse_allcheckbox', function(){
      var status = this.checked;
      $('.collapse_checkbox').each(function(){ 
         this.checked = status;
      });
   });
   
   function search_filter(data='', filterby=''){
      var searchtext = $('#searchtext').val();
      $('#search_panel').addClass('hide');
      $('#allsearch_results').removeClass('hide');
      $('.loading').css('display', 'block');
      
      var formdata = '';
      var city = $('select[name="filter_city"]').val();
      var zone = $('select[name="filter_zone"]').val();
      var locality = $('select[name="filter_locality"]').val();
      if (filterby == 'state') {
         getcitylist(data);
         formdata += '&state='+data+ '&city=&zone='+zone+'&locality='+locality;
      }else if (filterby == 'city') {
         var state = $('select[name="filter_state"]').val();
         getzonelist(data);
         formdata += '&state='+state+'&city='+data+'&zone=&locality='+locality;
      }else{
         var state = $('select[name="filter_state"]').val();
         formdata += '&state='+state+ '&city='+city+'&zone='+zone+'&locality='+locality;
      }

      //alert(formdata);
      $.ajax({
         url: base_url+'user/viewall_search_results',
         type: 'POST',
         dataType: 'json',
         data: 'search_user='+searchtext+formdata,
         success: function(data){
            $('.loading').css('display', 'none');
            $('#search_count').html(data.total_results+' <small>Results</small>');
            $('#search_gridview').html(data.search_results);
         }
      });
   }
</script>