<?php
$isp_detail = $this->user_model->get_isp_name();
$isp_name = $isp_detail['isp_name'];
$fevicon = $isp_detail['fevicon_icon'];
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <link rel="icon" href="<?php echo base_url()?>assets/images/<?php echo $fevicon?>" type="image/x-icon"/>
      <title><?php echo $isp_name; ?> </title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading">
	 <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                     <?php
                     $isplogo = $this->user_model->get_ispdetail_info();
                     if($isplogo != 0){
                        echo '<img src="'.base_url()."ispmedia/logo/".$isplogo.'" class="img-responsive"/>';
                     }else{
                        echo '<img src="'.base_url().'assets/images/decibel.png" class="img-responsive"/>';
                     }
                     ?>
                     </span>
                  </div>
                  <?php
		     $leftperm['navperm']=$this->user_model->leftnav_permission();
		     $this->view('left_nav',$leftperm);
		  ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <span class="navbar-brand">Users</span>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
			      <?php if($this->user_model->is_permission(CREATEUSER,'ADD')){ ?>
                              <li>
                                 <a href="javascript:void(0)" onclick="setup_adduser()">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD USER
                                 </button>
                                 </a>
                              </li>
			      <?php } ?>
			      <?php if($this->user_model->is_permission(PLANS,'ADD')){ ?>
                              <li>
                                 <a href="javascript:void(0)" onclick="check_tax_plan()">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD PLAN
                                 </button>
                                 </a>
                              </li>
			      <?php } ?>
			      <?php if($this->user_model->is_permission(TOPUP,'ADD')){ ?>
			      <li>
                                 <a href="javascript:void(0)" onclick="check_tax_topup()">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD TOPUP
                                 </button>
                                 </a>
                              </li>
			      <?php }
			      $this->load->view('includes/global_setting',$leftperm);
			      ?>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <ul>
                                 <li id="active_users">
                                    <h5>ACTIVE USERS</h5>
                                    <h4><i class="fa fa-user" aria-hidden="true"></i> <?php echo $active_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li id="inactive_users">
                                    <h5>INACTIVE USERS</h5>
                                    <h4><i class="fa fa-wifi" aria-hidden="true"></i> <?php echo $inactive_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li id="lead_enquiry">
                                    <h5>LEADS & ENQUIRIES</h5>
                                    <h4><i class="fa fa-question-circle" aria-hidden="true"></i> <?php echo $lead_enquiry_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li style="background-color:#f4474a"  id="user_complaints">
                                    <h5>COMPLAINTS</h5>
                                    <h4><i class="fa fa-exclamation-circle" aria-hidden="true"></i> <?php echo $complaints?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li style="background-color:#f4474a" id="payment_overdue">
                                    <h5>PAYMENT OVERDUE</h5>
                                    <h4> <!--<i class="fa fa-inr" aria-hidden="true"></i>--><?php echo $ispcodet['currency'] ?> <?php echo $payment_dues?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
				 <li style="background-color:#f4474a" id="otherdues">
                                    <h5>OTHER DUES</h5>
                                    <h4> <!--<i class="fa fa-inr" aria-hidden="true"></i>--><?php echo $ispcodet['currency'] ?> <?php echo $otherdues?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                              </ul>
                           </div>
			   
			   <div id="search_panel">
			      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				 <h1><small>Total</small> <?php echo $total_users?> <small>Users</small></h1>
			      </div>
			      <div class="row">
				 <div class="serch_box">
				    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-md-offset-3">
				       <div class="input-group" id="adv-search">
					  <input type="text" class="serch-form-control form-control" placeholder="Lookup Users" onFocus="this.placeholder=''" onBlur="this.placeholder=' Lookup Users'" style="box-shadow:none" id="search_user" />
					  <div class="input-group-btn">
					     <div class="btn-group" role="group">
						<div class="dropdown dropdown-lg">
						   <button type="button" class="btn btn-primary dropdown-toggle" style="border-bottom-right-radius :4px; border-top-right-radius :4px;">
						   Go
						   </button>
						   <div class="dropdown-menu dropdown-menu-right" role="menu" style="border-radius:0px; margin-top: 2px;">
						      <div class="row">
							 <div class="search-dropdown">
							    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left: 0px;">
							       <h4 id="total_search_count"></h4>
							    </div>
							    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right: 0px;">
							       <h4><span>
								  <a href="javascript:void(0)" onclick="viewall_search()">View All</a>
								  </span>
							       </h4>
							    </div>
							 </div>
							 <div id="search_results"></div>
						      </div>
						   </div>
						</div>
					     </div>
					  </div>
				       </div>
				       <div class="row advance_filters_div" style="display: none">
					  <form action="" method="post" id="advsearch_form" autocomplete="off" onsubmit="advanced_search(); return false;">
					     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
						   <h2><small style="color:#424143; font-weight:600">ADVANCE FILTERS</small></h2>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px;">
						   <div class="row" style="margin-bottom:15px;">
						      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px;">
							 <div class="row">
							    <label class="radio-inline" style="font-weight: 400;">
							       <input type="radio" name="advsearch_category" value="1"> LEADS
							    </label>
							    <label class="radio-inline" style="font-weight: 400">
							       <input type="radio" name="advsearch_category" value="3"> CUSTOMERS
							    </label>
							 </div>
						      </div>
						   </div>
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
						      <div class="row">
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-left">
							    <div class="mui-select">
							       <select name="advsearch_locality">
								  <option value="">User Category*</option>
								  <?php $this->user_model->usage_locality(); ?>
							       </select>
							       <label>User Category</label>
							    </div>
							 </div>
						      </div>
						   </div>
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
						      <div class="row">
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nomargin-left">
							    <div class="mui-select">
							       <select name="advsearch_state" onchange="getcitylist(this.value)">
								  <option value="">State*</option>
								  <?php $this->user_model->state_list(); ?>
							       </select>
							       <label>Select State</label>
							    </div>
							 </div>
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <div class="mui-select">
							       <select name="advsearch_city" class="search_citylist" onchange="getzonelist(this.value)">
								  <option value="">City*</option>
							       </select>
							       <label>Select City</label>
							    </div>
							 </div>
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <div class="mui-select">
							       <select name="advsearch_zone" class="search_zonelist">
								  <option value="">Zone*</option>
							       </select>
							       <label>Select Zone</label>
							    </div>
							 </div>
						      </div>
						   </div>
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
						      <div class="row">
							 <input type="submit" class="mui-btn mui-btn--sm mui-btn--accent" value="Search" />
						      </div>
						   </div>
						</div>
					     </div>
					  </form>
				       </div>
				    </div>
				 </div>
				 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
				    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				       <span 
					  style="color:#29abe2; font-size:13px; font-weight:600; text-decoration:underline; cursor: pointer" id="advance_filters">Advance Filters
				       </span>
				    </div>
				 </div>
			      </div>
			   </div>
			   <div id="allsearch_results" class="hide">
			      <?php $this->view('users/Search'); ?>
			   </div>
			   <div id="advsearch_results" class="hide">
			      <?php $this->view('users/Advanced_Search'); ?>
			   </div>
			</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="taxerr" role="dialog"  data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-body" style="padding-bottom:5px">
		     <p id="taxmsg">Please enter Tax before.</p>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button"  class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button> 
		  </div>
	       </div>
	 </div>
      </div>
      
      <div class="modal fade" id="billingerr" role="dialog"  data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-body" style="padding-bottom:5px">
		     <p id="billerrmsg"></p>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button"  class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button> 
		  </div>
	       </div>
	 </div>
      </div>
      
      <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/misc.js?version=3.1"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/crbnMenu.js"></script>
      <script type="text/javascript">
         if ($(window)) {
            $(function () {
               $('.menu').crbnMenu({
               hideActive: true
               });
            });
         }
      </script>
      <script type="text/javascript">
	 setTimeout(function (){
	    $('body').on('keyup', '#search_user', function(){
	       var search_user = $('#search_user').val();
	       $.ajax({
		  url: base_url+'user/filtersearch',
		  type: 'POST',
		  dataType: 'json',
		  data: 'search_user='+search_user,
		  success: function(data){
		     //alert(data.total_results);
		     $('.dropdown').addClass('open');
		     $('#total_search_count').html(data.total_results+' <small>Results</small>');
		     $('#search_results').html(data.search_results);
		  }
	       });
	    });
	 },1000);
	 
	 $('body').on('click', '#lead_enquiry', function(){
	    window.location.href = base_url+'user/lead_enquiry';
	 });
	 
	 $('body').on('click', '#user_complaints', function(){
	    window.location.href = base_url+'user/complaints';
	 });
	 
	 $('body').on('click', '#inactive_users', function(){
	    window.location.href = base_url+'user/status/inact';
	 });
	 
	 $('body').on('click', '#active_users', function(){
	    window.location.href = base_url+'user/status/act';
	 });
	 
	 $('body').on('click', '#payment_overdue', function(){
	    window.location.href = base_url+'user/pay_overdue'; 
	 });
	 $('body').on('click', '#otherdues', function(){
	    window.location.href = base_url+'user/otherdues'; 
	 });
	 
	 function viewall_search(){
	    var search_user = $('#search_user').val();
	    $('#search_panel').addClass('hide');
	    $('.loading').removeClass('hide');
	    setTimeout(function (){
	       $('.loading').addClass('hide');
	       $('#allsearch_results').removeClass('hide');
	    },1000);
	    
	    $.ajax({
	       url: base_url+'user/viewall_search_results',
	       type: 'POST',
	       dataType: 'json',
	       data: 'search_user='+search_user,
	       success: function(data){
		  $('#searchtext').val(search_user);
		  $('#search_count').html(data.total_results+' <small>Results</small>');
		  $('#search_gridview').html(data.search_results);
	       }
	    });
	 }
	 
	 function advanced_search() {
	    var formdata = $('#advsearch_form').serialize();
	    var usertype_advsearch = $('input[name="advsearch_category"]:checked').val();
	    $('#search_panel').addClass('hide');
	    $('#allsearch_results').addClass('hide');
	    $('#advance_filters').modal('hide');
	    $('.loading').removeClass('hide');
	    $.ajax({
	       url: base_url+'user/advanced_search',
	       type: 'POST',
	       dataType: 'json',
	       data: formdata,
	       success: function(data){
		  $('.loading').addClass('hide');
		  $('#advsearch_results').removeClass('hide');

		  $('#usertype_advsearch').val(usertype_advsearch);
		  $('select[name="advfilter_locality"]').val(data.locality);
		  $('select[name="advfilter_state"]').val(data.state);
		  $('select[name="advfilter_city"]').val(data.city);
		  $('select[name="advfilter_zone"]').val(data.zone);
		  $('#advsearch_count').html(data.total_results+' <small>Results</small>');
		  $('#advsearch_gridview').html(data.search_results);
	       }
	    });
	 }
	 
         $(document).ready(function() {
	    var height = $(window).height();
            $('#main_div').css('height', height);
	    $('#right-container-fluid').css('height', height);
	    
	    setTimeout(function (){
	       $('.loading').addClass('hide');
	    },1000);
	    
	    $('#advance_filters').click(function() {
	       //alert("Hello! I am an alert box!!");
               $('.advance_filters_div').slideToggle("fast");
	    });
         });
      </script>
      <script type="text/javascript">
	 /************** PERMISSION SETTING ***********/
	 $(document).ready(function() {
	    <?php
	       $superadmin = $this->session->userdata['isp_session']['super_admin'];
	       $active_hideperm = $this->user_model->is_permission('ACTIVEUSERS','HIDE');
	       $inactive_hideperm = $this->user_model->is_permission('INACTIVEUSERS','HIDE');
	       $leadenqry_hideperm = $this->user_model->is_permission('LEADENQUIRYUSERS','HIDE');
	       $complaint_hideperm = $this->user_model->is_permission('COMPLAINTSUSERS','HIDE');
	       $paymentdue_hideperm = $this->user_model->is_permission('PAYMENTDUEUSERS','HIDE');
	       $otherdue_hideperm = $this->user_model->is_permission('OTHERDUESUSERS','HIDE');
	    ?>   
	    <?php if(($active_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#active_users').addClass('hide');
	    <?php }else{ ?>
		  $('li#active_users').removeClass('hide');
	    <?php } ?>
	    <?php if(($inactive_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#inactive_users').addClass('hide');
	    <?php }else{ ?>
		  $('li#inactive_users').removeClass('hide');
	    <?php } ?>
	    <?php if(($leadenqry_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#lead_enquiry').addClass('hide');
	    <?php }else{ ?>
		  $('li#lead_enquiry').removeClass('hide');
	    <?php } ?>
	    <?php if(($complaint_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#user_complaints').addClass('hide');
	    <?php }else{ ?>
		  $('li#user_complaints').removeClass('hide');
	    <?php } ?>
	    <?php if(($paymentdue_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#payment_overdue').addClass('hide');
	    <?php }else{ ?>
		  $('li#payment_overdue').removeClass('hide');
	    <?php } ?>
	    <?php if(($otherdue_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#otherdues').addClass('hide');
	    <?php }else{ ?>
		  $('li#otherdues').removeClass('hide');
	    <?php } ?>
	 });
      </script>
   </body>
</html>

