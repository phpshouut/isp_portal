<?php
$isp_detail = $this->user_model->get_isp_name();
$isp_name = $isp_detail['isp_name'];
$fevicon = $isp_detail['fevicon_icon'];
?>


<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <link rel="icon" href="<?php echo base_url()?>assets/images/<?php echo $fevicon?>" type="image/x-icon"/>
      <title><?php echo $isp_name; ?> </title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-material-datetimepicker.css" />
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading">
	 <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                     <?php
                     $isplogo = $this->user_model->get_ispdetail_info();
                     if($isplogo != 0){
                        echo '<img src="'.base_url()."ispmedia/logo/".$isplogo.'" class="img-responsive"/>';
                     }else{
                        echo '<img src="'.base_url().'assets/images/decibel.png" class="img-responsive"/>';
                     }
                     ?>
                     </span>
                  </div>
                  <?php
		     $leftperm['navperm']=$this->user_model->leftnav_permission();
		     $this->view('left_nav',$leftperm);
		  ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <span class="navbar-brand">Users</span>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
			      <?php if($this->user_model->is_permission(CREATEUSER,'ADD')){ ?>
                              <li>
                                 <a href="javascript:void(0)" onclick="setup_adduser()">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD USER
                                 </button>
                                 </a>
                              </li>
			      <?php } ?>
			      <?php if($this->user_model->is_permission(PLANS,'ADD')){ ?>
                              <li>
                                 <a href="javascript:void(0)" onclick="check_tax_plan()">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD PLAN
                                 </button>
                                 </a>
                              </li>
			      <?php } ?>
			      <?php if($this->user_model->is_permission(TOPUP,'ADD')){ ?>
			      <li>
                                 <a href="javascript:void(0)" onclick="check_tax_topup()">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD TOPUP
                                 </button>
                                 </a>
                              </li>
			      <?php }
			      $this->load->view('includes/global_setting',$leftperm);
			      ?>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <ul>
                                 <li id="active_users">
                                    <h5>ACTIVE USERS</h5>
                                    <h4><i class="fa fa-user" aria-hidden="true"></i> <?php echo $active_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li id="inactive_users">
                                    <h5>INACTIVE USERS</h5>
                                    <h4><i class="fa fa-wifi" aria-hidden="true"></i> <?php echo $inactive_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li id="lead_enquiry">
                                    <h5>LEADS & ENQUIRIES</h5>
                                    <h4><i class="fa fa-question-circle" aria-hidden="true"></i> <?php echo $lead_enquiry_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li style="background-color:#f4474a"  id="user_complaints">
                                    <h5>COMPLAINTS</h5>
                                    <h4><i class="fa fa-exclamation-circle" aria-hidden="true"></i> <?php echo $complaints?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li style="background-color:#f4474a" id="payment_overdue">
                                    <h5>PAYMENT OVERDUE</h5>
                                    <h4><?php echo $ispcodet['currency'] ?> <?php echo $payment_dues?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
				 <li style="background-color:#f4474a" id="otherdues">
                                    <h5>OTHER DUES</h5>
                                    <h4><?php echo $ispcodet['currency'] ?> <?php echo $otherdues?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                              </ul>
                           </div>
			   <div id="complaints_results">
			      <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                       <h1>Complaints (<span id="complaints_count"></span>)</h1>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 pull-right">
                                       <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 14px;">
                                          <div class="form-group">
                                             <div class="input-group">
                                                <div class="input-group-addon">
                                                   <i class="fa fa-search" aria-hidden="true"></i>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Search Complaints" onBlur="this.placeholder='Search Complaints'" onFocus="this.placeholder=''"  id="search_user_complaint">
                                                <span class="searchclear" class="searchclear">
                                                <img src="<?php echo base_url() ?>assets/images/clear.svg"/>
                                                </span>
                                             </div>
                                             <label class="search_label">You can look up by name, email, UID or mobile</label>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="row" id="onefilter">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                       <div class="row">
                                          <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="mui-select">
                                                <select name="filter_bytickettype" onchange="filter_complaints()">
                                                   <!--<option value="alltkt">All Tickets</option>-->
						   <option value="opentkt">Open</option>
						   <option value="pendingtkt">Pending</option>
						   <option value="closedtkt">Closed</option>
                                                </select>
                                                <label>Tickets Type</label>
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3">
					     <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
						<div class="input-group">
						   <div class="input-group-addon" style="padding:6px 10px">
						      <i class="fa fa-calendar"></i>
						   </div>
						   <input type="text" class="form-control tkttmezone" id="" placeholder="Select Timezone" name="filter_bytickettimezone" value="">
						</div>
					     </div>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="mui-select">
                                                <select name="filter_byticketpriority" onchange="filter_complaints()">
                                                   <option value="">Select Priority</option>
						   <option value="low">Low</option>
                                                   <option value="medium">Medium</option>
						   <option value="high">High</option>
                                                </select>
                                                <label>Priority</label>
                                             </div>
                                          </div>
					  <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="mui-select">
                                                <select name="filter_byticketassignto" onchange="filter_complaints()">
                                                   <option value="">Team Member</option>
						   <?php $this->user_model->ticket_sorters_list(); ?>
                                                </select>
                                                <label>Assign To</label>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="table-responsive">
                                    <table class="table table-striped">
                                       <thead style="font-size:11px">
                                          <tr class="active">
					     <th>&nbsp;</th>
					     <th>SUBSCRIBER</th>
					     <th>USER ID</th>
					     <th>REQUEST ID</th>
					     <th>DATE</th>
					     <th>TYPE</th>
					     <th>STATUS</th>
					     <th>TAT DAYS</th>
					     <th>PRIORITY</th>
					     <!--<th colspan="4" style="text-align:center">ACTIONS</th>-->
					  </tr>
                                       </thead>
                                       <tbody id="complaints_gridview" style="font-size:11px"></tbody>
                                    </table>
                                 </div>
                              </div>
			   </div>
			</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      
      <div class="modal fade" tabindex="-1" role="dialog" id="editserviceRequestModal" data-backdrop="static"     data-keyboard="false">
         <div class="modal-dialog" style="margin-top:4%">
            <form action="" method="post" id="edit_ticket_request_form" autocomplete="off" onsubmit="edit_ticket_request(); return false;">
	       <input type="hidden" name="edcustomer_id" class="subscriber_userid" value=''>
	       <input type="hidden" name="edtktid" value="" />
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h4 class="modal-title">
                        <strong>EDIT SERVICE REQUEST</strong>
                     </h4>
                  </div>
                  <div class="modal-body">
                     <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-textfield">
                              <input type="text" name="edticketid" value="" readonly  required>
                              <label>Ticket ID <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-textfield" id="edtktdtme">
			      <input name="edticket_datetime" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" readonly required/>
                              <label>Ticket DateTime<sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-textfield">
                              <input type="text" name="edcustomer_uuid" value="" readonly required>
                              <label>Customer ID <sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="edticket_type" required disabled>
                                 <option value="">Select Ticket Type</option>
                                 <option value="change_address">Change Address</option>
                                 <option value="change_plan">Change Plan</option>
                                 <option value="terminate_service">Terminate Service</option>
                                 <option value="suspend_service">Suspend Service</option>
                                 <option value="service_down">Service Down</option>
                                 <option value="router_not_working">Router Not Working</option>
                                 <option value="other_request">Other Request</option>
                              </select>
                              <label>Ticket Types <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="edticket_priority" required disabled>
                                 <option value="">Select Ticket Priority</option>
                                 <option value="low">Low</option>
                                 <option value="medium">Medium</option>
                                 <option value="high">High</option>
                              </select>
                              <label>Priority<sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="edticket_assignto" required disabled>
                                 <option value="">Assign Ticket</option>
                              </select>
                              <label>Assign Ticket<sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield">
                              <textarea style="resize: vertical;" rows="4" name="edticket_description" required readonly></textarea>
                              <label>Ticket Description<sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label">
                              <input type="text" name="edticket_comment">
                              <label>Comments (If any)</label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="mui-btn mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                     <input type="submit" class="mui-btn mui-btn--primary mui-btn--flat" style="background-color:#f00f64; color:#fff" value="EDIT" >
                  </div>
               </div>
            </form>
         </div>
      </div>
      <div class="modal fade" id="close_ticket_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>Are you sure you want to close the ticket ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" onclick="sendotp_tktclose()">YES</button>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade" id="ticket_closedModal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <input type="hidden" name="tktcloseid" value="">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
		     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CLOSE TICKET ALERT</strong>
		  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>To close the ticket of <strong class="customer_name"></strong>, please enter the OTP sent to regestiered mobile no. <strong class="customer_phone"></strong>
                  </p>
                  <div class="mui-textfield mui-textfield--float-label" style="margin-bottom:0px">
                     <input type="text" name="tktclose_otp" maxlength="4" minlength="4" required>
                     <label>Enter 4-Digit Mobile No.</label>
                  </div>
		  <label id="tktclose_otp_err" style="color:#f00; font-size:12px;"></label>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="verify_confirm_tktclose()">CLOSE TICKET</button>
               </div>
            </div>
         </div>
      </div>
      
      <div class="modal fade" id="taxerr" role="dialog"  data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-body" style="padding-bottom:5px">
		     <p id="taxmsg">Please enter Tax before.</p>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button"  class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button> 
		  </div>
	       </div>
	 </div>
      </div>
      
      <div class="modal fade" id="billingerr" role="dialog"  data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-body" style="padding-bottom:5px">
		     <p id="billerrmsg"></p>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button"  class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button> 
		  </div>
	       </div>
	 </div>
      </div>

      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/material.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/misc.js?version=3.1"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/crbnMenu.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/moment-with-locales.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap-material-datetimepicker.js"></script>
      
      <script type="text/javascript">
         if ($(window)) {
            $(function () {
               $('.menu').crbnMenu({
               hideActive: true
               });
            });
         }
      </script>
      <script type="text/javascript">
         $('body').on('click','.searchclear', function(){
            $('#search_user_complaint').val('');
            filter_complaints();
            
         });
         $('body').on('keyup', '#search_user_complaint', function(){
	    setTimeout(function (){
	       filter_complaints();
	    }, 2000);
         });
         
	 $('body').on('click', '#inactive_users', function(){
	    window.location.href = base_url+'user/status/inact';
	 });
	 
	 $('body').on('click', '#active_users', function(){
	    window.location.href = base_url+'user/status/act';
	 });
	 
         $('body').on('click', '#lead_enquiry', function(){
	    window.location.href = base_url+'user/lead_enquiry';
	 });
	 
	 $('body').on('click', '#user_complaints', function(){
	    window.location.href = base_url+'user/complaints';
	 });
	 $('body').on('click', '#payment_overdue', function(){
	    window.location.href = base_url+'user/pay_overdue'; 
	 });
	 $('body').on('click', '#otherdues', function(){
	    window.location.href = base_url+'user/otherdues'; 
	 });
	 
	 $(document).ready(function() {
	    var height = $(window).height();
            $('#main_div').css('height', height);
	    $('#right-container-fluid').css('height', height);
	    
	    $('.tkttmezone').bootstrapMaterialDatePicker({
	       weekStart: 0,
	       time: false,
	       clearButton: true,
	       format: 'DD-MM-YYYY',
	       maxDate: moment()
	    }).on('change', function(e, date){
	       filter_complaints();
	    });
	    
            var total_results = "<?php echo $complaints_list['total_results']; ?>";
            var search_results = "<?php echo $complaints_list['search_results']; ?>" ;
	    $('#complaints_count').html(total_results+' <small>Results</small>');
	    $('#complaints_gridview').html(search_results);
	    
	    $('.loading').addClass('hide');
         });
	 function filter_complaints() {
	    var searchtext = $('#search_user_complaint').val();
            $('.loading').removeClass('hide')
            
            var formdata = '';
	    var tkttype = $('select[name="filter_bytickettype"] option:selected').val();
	    var tkttimezone = $('input[name="filter_bytickettimezone"]').val();
	    var tktpriority = $('select[name="filter_byticketpriority"] option:selected').val();
	    var tktassignto = $('select[name="filter_byticketassignto"] option:selected').val();
	    
	    formdata = "&tkttype="+tkttype+'&tkttimezone='+tkttimezone+'&tktpriority='+tktpriority+'&tktassignto='+tktassignto;
	    
	    $.ajax({
               url: base_url+'user/complaints_filter',
               type: 'POST',
               dataType: 'json',
               data: 'search_user='+searchtext+formdata,
               success: function(data){
                  $('.loading').addClass('hide');
                  $('#complaints_count').html(data.total_results+' <small>Results</small>');
                  $('#complaints_gridview').html(data.search_results);
               }
            });
	 }
	 function close_ticket_confirmation_alert(tktid) {
	    $('input[name="tktcloseid"]').val(tktid);
	    $('#close_ticket_confirmation_alert').modal('show');
	 }
	 function sendotp_tktclose() {
	    var custid = $('input[name="edcustomer_id"]').val();
	    var cust_phone = $('[id^="phone_'+custid+'"]').val();
	    $.ajax({
	       url: base_url+'user/send_otp_tktclose',
	       type: 'POST',
	       data: 'phone='+cust_phone,
	       async: false,
	       success: function(){
		  
	       }
	    });
	    
	    $('input[name="tktclose_otp"]').val('');
	    $('#tktclose_otp_err').html('');
	    $('#close_ticket_confirmation_alert').modal('hide');
	   //alert($('[id^="custname_'+custid+'"]').text());
	    $('.customer_name').html($('[id^="custname_'+custid+'"]').text());
	    
	    for (idx=2; idx < 7 ; idx++) {
	       cust_phone = cust_phone.substring(0, idx) + '*' + cust_phone.substring(idx+1);
	    }
	    $('.customer_phone').html(cust_phone);
	    $('#ticket_closedModal').modal('show');
	    
	 }
	 
	 function verify_confirm_tktclose() {
	    var custid = $('input[name="edcustomer_id"]').val();
	    var cust_phone = $('[id^="phone_'+custid+'"]').val();
	    var otp = $('input[name="tktclose_otp"]').val();
	    var tktid =  $('input[name="tktcloseid"]').val();
	    $.ajax({
	       url: base_url+'user/verify_confirm_tktclose',
	       type: 'POST',
	       data: 'phone='+cust_phone+'&otp='+otp+'&tktid='+tktid,
	       async: false,
	       success: function(data){
		  if (data == 0) {
		     $('#tktclose_otp_err').html('OTP you have entered not get matched.');
		  }else{
		     $('#ticket_closedModal').modal('hide');
		     window.location.href = base_url+'user/complaints';
		  }
	       }
	    });
	 }
	 
      </script>
      <script type="text/javascript">
	 /************** PERMISSION SETTING ***********/
	 $(document).ready(function() {
	    <?php
	       $superadmin = $this->session->userdata['isp_session']['super_admin'];
	       $active_hideperm = $this->user_model->is_permission('ACTIVEUSERS','HIDE');
	       $inactive_hideperm = $this->user_model->is_permission('INACTIVEUSERS','HIDE');
	       $leadenqry_hideperm = $this->user_model->is_permission('LEADENQUIRYUSERS','HIDE');
	       $complaint_hideperm = $this->user_model->is_permission('COMPLAINTSUSERS','HIDE');
	       $paymentdue_hideperm = $this->user_model->is_permission('PAYMENTDUEUSERS','HIDE');
	       $otherdue_hideperm = $this->user_model->is_permission('OTHERDUESUSERS','HIDE');
	    ?>   
	    <?php if(($active_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#active_users').addClass('hide');
	    <?php }else{ ?>
		  $('li#active_users').removeClass('hide');
	    <?php } ?>
	    <?php if(($inactive_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#inactive_users').addClass('hide');
	    <?php }else{ ?>
		  $('li#inactive_users').removeClass('hide');
	    <?php } ?>
	    <?php if(($leadenqry_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#lead_enquiry').addClass('hide');
	    <?php }else{ ?>
		  $('li#lead_enquiry').removeClass('hide');
	    <?php } ?>
	    <?php if(($complaint_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#user_complaints').addClass('hide');
	    <?php }else{ ?>
		  $('li#user_complaints').removeClass('hide');
	    <?php } ?>
	    <?php if(($paymentdue_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#payment_overdue').addClass('hide');
	    <?php }else{ ?>
		  $('li#payment_overdue').removeClass('hide');
	    <?php } ?>
	    <?php if(($otherdue_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#otherdues').addClass('hide');
	    <?php }else{ ?>
		  $('li#otherdues').removeClass('hide');
	    <?php } ?>
	 });
      </script>
   </body>
</html>