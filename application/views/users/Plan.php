

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="row">
            <div class="mui-tabs__pane mui--is-active " id="Plan-details">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                     <h2><span class="detplan_name_text"></span>&nbsp;<span id="plan_price"></span>&nbsp;<span id="displaycustom_planprice"></span> <a href="javascript:void(0)" id="custplanbtnopt" onclick="show_customize_plan_pricing_modal()" style="font-size: 12px; margin-left: 50px; text-decoration: underline"> Set Custom Plan Pricing </a></h2>
                     <div class="form-group">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:5px;">
                           <h4>TYPE OF PLAN: &nbsp;
                            <input type="hidden" name="plan_type_id" id="plan_type_id" value="" />
                            <span class="detplan_type" style="font-weight:normal; font-size:14px"></span>
                           </h4> 
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:5px;">
                           <h4>PLAN DURATION: &nbsp;
                            <span class="detplan_duration" style="font-weight:normal; font-size:14px"></span>
                           </h4> 
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px">
                           <div class="row">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px">
                                 <div class="mui-textfield">
                                    <input type="text" class="detplan_name" value="" disabled>
                                    <label>Plan Name<sup>*</sup></label>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                 <div class="mui-textfield">
                                    <input type="text" class="detplan_desc" value="" disabled>
                                    <label>Plan Description</label>
                                 </div>
                              </div>
                              <!--<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                 <div class="mui-textfield">
                                    <input type="text"  class="detplan_validity" value="" disabled>
                                    <label>Plan Validity</label>
                                 </div>
                              </div>-->
                           </div>
                        </div>
                     </div>
                     <div class="form-group" id="user_type_ful_plan_enquiry" style="display: block">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:20px;">
                           <div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding-left">
                                 <div class="mui-textfield">
                                    <input type="text" class="detdwnld_rate" value="" disabled>
                                    <span class="title_box">Kbps</span>
                                    <label>Download Speed<sup>*</sup></label>
                                 </div>
                              </div>
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                 <div class="mui-textfield">
                                    <input type="text" class="detupld_rate" value="" disabled>
                                    <span class="title_box">Kbps</span>
                                    <label>Upload Speed<sup>*</sup></label>
                                 </div>
                              </div>
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nonfup_custspeed" style="display:none">
                                 <a href="javascript:void(0)" onclick="show_customize_planspeed_modal()" style="font-size: 13px;  text-decoration: underline" class="customize_planspeed"> Customize PlanSpeed </a>
                              </div>
                           </div>
                           <div class ="row timeplan" style="margin-top:15px; display:none;">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding-left">
                                 <div class="mui-textfield" style="padding-top: 10px">
                                    <input type="text"  class="detplan_timelimit" value="" disabled>
                                    <label>Time Limit</label>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
                                 <h4>Time Calculated on: &nbsp;
                                    <span class="dettime_calculated_on" style="font-weight:normal; font-size:14px"></span>
                                 </h4>
                              </div>
                           </div>
                           <div class="row dataplan" style="margin-top: 15px; display:none;">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                 <div class="mui-textfield">
                                    <input type="text" class="detdata_limit" value="" disabled >
                                    <input type="hidden" name="plandatalimit" id="plandatalimit" value="" />
                                    <span class="title_box">GB</span>
                                    <label>Data Limit<sup>*</sup></label>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                 <h4>Data Calculated on: &nbsp;
                                    <input type="hidden" name="plandata_calculatedon" id="plandata_calculatedon" value="" />
                                    <span class="detdata_calcn" style="font-weight:normal; font-size:14px"></span>
                                 </h4>
                              </div>
                           </div>
                           <div class="row fupclass" style="display:none;">
                              <h4>Post FUP limit</h4>
                              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                 <div class="mui-textfield">
                                    <input type="text" value="" class="detfup_dwnl_rate" disabled>
                                    <span class="title_box">Kbps</span>
                                    <label>Download Speed<sup>*</sup></label>
                                 </div>
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                 <div class="mui-textfield">
                                    <input type="text" value="" class="detfup_upld_rate" disabled>
                                    <span class="title_box">Kbps</span>
                                    <label>Upload Speed<sup>*</sup></label>
                                 </div>
                              </div>
                              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                 <?php if(isset($postfup_data) && $postfup_data != '0KB'){
                                    echo '<a href="javascript:void(0)" onclick="show_customize_planspeed_modal()" style="font-size: 12px; margin-left: 50px; text-decoration: underline"> Customize PostFUP Speed </a>';
                                 }?>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

