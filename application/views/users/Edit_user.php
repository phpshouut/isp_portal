<?php
//echo '<pre>'; print_r($this->session->userdata['isp_session']); die;
if($record != 0){
   foreach($record as $recordobj){
      $subscriber_uid = $recordobj->uid;
      $subscriber_id = $recordobj->id;
      $subscriber_username = $recordobj->username;
      $first_name = $recordobj->firstname;
      $middle_name = $recordobj->middlename; 
      $last_name = $recordobj->lastname;
      $email = $recordobj->email;
      $phone = $recordobj->mobile;
      $altphone = $recordobj->alt_mobile;
      $dob = $recordobj->dob;
      $flat_number = $recordobj->flat_number;
      $address1 = $recordobj->address;
      $address2 = $recordobj->address2;
      $state = $recordobj->state;
      $city = $recordobj->city;
      $zone = $recordobj->zone;
      $pancard = $recordobj->pancard;
      $locality = $recordobj->usuage_locality;
      $priority_account = $recordobj->priority_account;
      $active_user = $recordobj->enableuser;
      $mac = $recordobj->mac;
      $ipaddr_type = $recordobj->ipaddr_type;
      $maclockedstatus = $recordobj->maclockedstatus;
      $geoaddr=$recordobj->geoaddress;
      $placeid=$recordobj->place_id;
      $lat=(round($recordobj->lat)==0)?"":$recordobj->lat;
      $long=(round($recordobj->longitude)==0)?"":$recordobj->longitude;
      $account_activated_on = ($recordobj->account_activated_on == '0000-00-00 00:00:00') ? '' : $recordobj->account_activated_on;
      $customerpassword = $recordobj->orig_pwd;
      $user_plan_type = $recordobj->user_plan_type;
      $subscriber_gstin_number = $recordobj->gstin_number;
      $taxtype = $recordobj->taxtype;
      
      $session_data = $this->session->userdata('isp_session');
      $session_data['active_user'] = "1";
      $this->session->set_userdata("isp_session", $session_data);
      
      $billaddr = $recordobj->billaddr;
      $billing_username = $recordobj->billing_username;
      $billing_mobileno = $recordobj->billing_mobileno;
      $billing_email = $recordobj->billing_email;
      $billing_flat_number = $recordobj->billing_flat_number;
      $billing_address = $recordobj->billing_address;
      $billing_address2 = $recordobj->billing_address2;
      $billing_state = $recordobj->billing_state;
      $billing_city = $recordobj->billing_city;
      $billing_zone = $recordobj->billing_zone;
      $geoaddress_searchtype = $recordobj->geoaddress_searchtype;

   }
}
$radiuspassword = $this->user_model->radcheck_password($subscriber_uid);
$dataArr = $this->user_model->live_usage($subscriber_uid);

$isp_detail = $this->user_model->get_isp_name();
$isp_name = $isp_detail['isp_name'];
$fevicon = $isp_detail['fevicon_icon'];
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <link rel="icon" href="<?php echo base_url()?>assets/images/<?php echo $fevicon?>" type="image/x-icon"/>
      <title><?php echo $isp_name; ?> </title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-material-datetimepicker.css" />
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/datepicker/bootstrap-datetimepicker.min.css" />
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading"><img src="<?php echo base_url() ?>assets/images/loader.svg"/></div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                     <?php
                     $isplogo = $this->user_model->get_ispdetail_info();
                     if($isplogo != 0){
                        echo '<img src="'.base_url()."ispmedia/logo/".$isplogo.'" class="img-responsive"/>';
                     }else{
                        echo '<img src="'.base_url().'assets/images/decibel.png" class="img-responsive"/>';
                     }
                     ?>
                     </span>
                  </div>
                  <?php
		     $leftperm['navperm']=$this->user_model->leftnav_permission();
		     $this->view('left_nav',$leftperm);
		  ?>
               </div>
	       <?php if($record == 0){ ?>
	       <script type="text/javascript">$(document).ready(function(){$('#norecord_found').modal('show');});</script>
	       <div class="modal fade" id="norecord_found" role="dialog" data-backdrop="static" data-keyboard="false">
		  <div class="modal-dialog modal-sm">
		     <div class="modal-content">
			<div class="modal-header">
			   <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
			   <h4 class="modal-title"><strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
NOTICE</strong></h4>
			</div>
			<div class="modal-body" style="padding-bottom:5px">
			   <p>OOPS!! No Record Found. Please try again with another search.</p>
			</div>
			<div class="modal-footer" style="text-align: right">
			   <a href="<?php echo base_url().'user' ?>" class="mui-btn mui-btn--large mui-btn--accent">BACK</a>
			</div>
		     </div>
		  </div>
	       </div>
	       <?php }else{ ?>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <ul class="nav navbar-nav">
                           <li>
                              <h3 style="margin-top:5px"><span>User: </span><?php echo ucfirst($first_name).' '.ucfirst($last_name) ?> <small style="font-size:60%">(<?php echo $subscriber_uid ?>), <span id="current_activeplan"></span></small></h3>
                           </li>
			
                        </ul>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
			      <?php $ustathideperm = $this->user_model->is_permission('USERSTATUS','HIDE');
				 if($ustathideperm){ ?>
			      <li style="margin: 10px" class="active_addc">
				 <input type="hidden" id="account_activated_on" value="<?php echo $account_activated_on ?>" />
                                 <input data-toggle="toggle" data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger" type="checkbox" id="userstatus_changealert" value="<?php echo $active_user ?>" style="height: 30px">
                              </li>
			      <?php } ?>
                              <li>
                                 <a href="<?php echo base_url().'user' ?>">
                                    <span class="mui-btn mui-btn--small mui-btn--accent" style="height:36px; line-height:35px; font-weight:600">CANCEL &amp; EXIT</span>
                                 </a>
                              </li>
			      <?php
				 $this->load->view('includes/global_setting',$leftperm);
			      ?>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="add_user">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <ul class="mui-tabs__bar">
				    <?php $globalstyle = '0'; ?>
				    <?php $hideperm = $this->user_model->is_permission(PD,'HIDE');
				    if($hideperm){ ?>
                                    <li id="personal_details">
                                       <a data-mui-toggle="tab" data-mui-controls="Personal_details" id="editclick_personal_details">
                                       User Details
                                       </a>
                                    </li>
				    <?php } ?>
				    <?php $khideperm = $this->user_model->is_permission(KYC,'HIDE'); $kstyle='';
				    if($khideperm){ ?>
                                    <li id="kyc_details">
                                       <a data-mui-toggle="tab" data-mui-controls="KYC_details" class="tab_menu" id="editclick_kyc_details">
                                       KYC
                                       </a>
                                    </li>
				    <?php } ?>
				    <?php $phideperm = $this->user_model->is_permission(PLANASSIGN,'HIDE'); $pstyle='';
				    if($phideperm){ ?>
                                    <li id="plantab" <?php echo "class='".$pstyle."'"; ?>>
                                       <a data-mui-toggle="tab" data-mui-controls="Plan_details" class="tab_menu" id="editclick_plan_details">
                                       Plans
                                       </a>
                                    </li>
				    <?php } ?>
				    <?php $thideperm = $this->user_model->is_permission(TOPUPASSIGN,'HIDE'); if($thideperm){ ?>
				    <li id="topuptab">
                                       <a data-mui-toggle="tab" data-mui-controls="Topup" class="tab_menu" id="editclick_topup_details">
                                      Topup
                                       </a>
                                    </li>
				    <?php } ?>
				    <?php $bhideperm = $this->user_model->is_permission(BILLING,'HIDE'); if($bhideperm){ ?>
                                    <li id="billtab">
                                       <!--class="tab_menu"-->
                                       <a data-mui-toggle="tab" data-mui-controls="Billing" class="tab_menu" id="editclick_billing_details">
                                       Billing
                                       </a>
                                    </li>
				    <?php } ?>
				    <?php $rhideperm = $this->user_model->is_permission(SERVICEREQUEST,'HIDE'); if($rhideperm){ ?>
                                    <li id="tickettab">
                                       <a data-mui-toggle="tab" data-mui-controls="Requests" class="tab_menu" onclick="fetch_allopenticket()">
                                       Tickets
                                       </a>
                                    </li>
				    <?php } ?>
				    <?php $shideperm = $this->user_model->is_permission(SETTING,'HIDE'); if($shideperm){ ?>
				    <li id="settingtab">
                                       <a data-mui-toggle="tab" data-mui-controls="Settings" class="tab_menu" id="settingclick_details">
                                       Settings
                                       </a>
                                    </li>
				    <?php } ?>
				    <?php $lhideperm = $this->user_model->is_permission(LOGS,'HIDE'); if($lhideperm){ ?>
                                    <li id="logtab">
                                       <a data-mui-toggle="tab" data-mui-controls="Logs" class="tab_menu" onclick="usuage_logs()">
                                       Logs & Activity
                                       </a>
                                    </li>
				    <?php } ?>
				    <?php $syshideperm = $this->user_model->is_permission('SYSLOG','HIDE'); if($syshideperm){ ?>
				    <li id="syslogtab">
                                       <a data-mui-toggle="tab" data-mui-controls="sysogs" class="tab_menu" >
                                       Syslogs
                                       </a>
                                    </li>
				    <?php } ?>
				    <?php $invtryhideperm = $this->user_model->is_permission('INVENTORY','HIDE');
				       if($invtryhideperm){ ?>
				    <li id="inventorytab">
                                      <a data-mui-toggle="tab" data-mui-controls="inventory" class="tab_menu" >
                                      Inventory
                                      </a>
                                   </li>
				    <?php } ?>
				    <?php $notifyhideperm = $this->user_model->is_permission('NOTIFICATIONS','HIDE');
				       if($notifyhideperm){ ?>
				    <li id="notificationtab">
                                      <a data-mui-toggle="tab" data-mui-controls="notifications" class="tab_menu" id="user_notification_listing">
                                      Notifications
                                      </a>
                                   </li>
				    <?php } ?>
				    <?php $diaghideperm= $this->user_model->is_permission('DIAGNOSTICS','HIDE'); if($diaghideperm){ ?>
				    <li id="diagnostictab">
                                      <a data-mui-toggle="tab" data-mui-controls="diagnostic" class="tab_menu" id="user_diagnostic">
                                      Diagnostic
                                      </a>
                                   </li>
				    <?php } ?>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <input type="hidden" class="current_taxapplicable" id="current_taxapplicable" value="" />
			      <input type="hidden" id="ispcountry_currency" value="<?php echo $ispcodet['currency'] ?>" />
                              <div class="mui--appbar-height"></div>
			      <?php if($hideperm){ ?>
                              <div class="mui-tabs__pane" id="Personal_details">
                                 <?php
				    $status_indicator = ''; $logoffbtn = '';
				    $userstatus = $this->user_model->check_user_onlineoffline($subscriber_uid);
				    if($userstatus == 'online'){
				       $status_indicator = "<img src='".base_url()."assets/images/green.png' alt='' />";
				       //$logoffbtn = '<a href="javascript:void(0)" onclick="routerlogout(\''.$subscriber_uid.'\')" style="font-size:12px; background-color:#c1c1c1; padding:5px; color:#fff">Router Logout</a>';
				    }else{
				       $status_indicator = "<img src='".base_url()."assets/images/red.png' alt='' />";
				    }
				    
				    $imperdata = base64_encode($subscriber_uid.'__'.$customerpassword);
				 ?>
				 
                                 <h2>Personal Details <span class="user_onlineoffline_status"><?php echo $status_indicator. '&nbsp;&nbsp;'.$logoffbtn ?></span> <a href="<?php echo base_url().'consumer/login/impersonate_user?ud='.$imperdata ?>" style="float:right;" title="Login to User Account" target="_blank">Impersonate User</a></h2>
				 <div class="form-group">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <h4>STAGE</h4>
                                       <div class="row">
                                          <label class="radio-inline">
                                          <input type="radio" name="user_type_radio" value="lead"> Lead
                                          </label>
                                          <label class="radio-inline">
                                          <input type="radio" name="user_type_radio" value="enquiry"> Enquiry
                                          </label>
                                          <label class="radio-inline">
                                          <input type="radio" name="user_type_radio" value="customer" id="usertype_customer"> Customer
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                                 <form action="" method="post" id="edit_subscriber_form" autocomplete="off" onsubmit="edit_customer(); return false;">
				    <input type="hidden" name="subscriber_usertype" id="subscriber_usertype" value='3'>
                                    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row" id="user_type_lead_enquiry">
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"  style="padding-left:0px;">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_fname" value="<?php echo $first_name; ?>" required>
                                                         <label>First Name<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_mname" value="<?php echo $middle_name; ?>">
                                                         <label>Middle Name</label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_lname" value="<?php echo $last_name; ?>" required>
                                                         <label>Last Name<sup>*</sup></label>
                                                      </div>
                                                   </div>
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="email" name="subscriber_email" value="<?php echo $email; ?>" required>
                                                         <label>Email ID<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="tel" name="subscriber_phone" maxlength="10" minlength="10" value="<?php echo $phone; ?>" pattern="[6789][0-9]{9}" required>
                                                         <label>Mobile No.<sup>*</sup></label>
                                                      </div>
                                                   </div>
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="tel" name="subscriber_altphone" maxlength="10" minlength="10" value="<?php echo $altphone; ?>" pattern="[6789][0-9]{9}">
                                                         <label>Alternate Mobile No.</label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
                                                         <div class="input-group" style="margin-top:-5px">
                                                            <div class="input-group-addon" style="padding:6px 10px">
                                                               <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control dobdate" id="" placeholder="Date of Birth*" name="subscriber_dob" value="<?php echo $dob; ?>">
                                                         </div>
                                                      </div>
                                                   </div>
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                      <div class="mui-select">
                                                         <select name="usuage_locality" required>
                                                            <option value="">Usuage Locality*</option>
                                                            <?php $this->user_model->usage_locality($locality); ?>
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"  style="padding-left:0px;">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_flatno" value="<?php echo $flat_number; ?>" required>
                                                         <label>Flat / Door No.<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_address1" value="<?php echo $address1; ?>" required>
                                                         <label>Address Line 1<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_address2" value="<?php echo $address2; ?>">
                                                         <label>Address Line 2</label>
                                                      </div>
                                                   </div>
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display: block; font-size: 16px; margin-bottom: 20px; padding-left: 0; padding-top: 25px;">
                                                      <div class="">
                                                         <select name="state" id="state_customer" onchange="getcitylist(this.value)" required style="width:100%">
                                                            <option value="">State*</option>
                                                            <?php $this->user_model->state_list($state); ?>
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display: block; font-size: 16px; margin-bottom: 20px; padding-left: 0;">
                                                      <div class="">
                                                         <select name="city" id="city_customer" class="citylist" onchange="getzonelist(this.value)" style="width:100%" required>
                                                            <option value="">City*</option>
							 <?php $this->user_model->getcitylist($state,$city); ?>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display: block; font-size: 16px; margin-bottom: 20px; padding-left: 0;">
                                                      <div class="">
                                                         <select name="zone" class="zonelist" style="width:100%" onchange="check_toadd_newzone(this.value, 'customer')" required>
                                                            <option value="">Zone*</option>
                                                            <?php $this->user_model->zone_list($city, $zone, $state); ?>
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
					  <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
						   <div class="mui-checkbox">
						      <label>
						        <input type="hidden" name="billaddr" value="0" />
							<input type="checkbox" name="billaddr" value="1" checked>&nbsp; &nbsp;
							<h4 style="display:inline-block;margin:0px">Billing Address (Same as above)</h4>
						      </label>
						   </div>
						</div>
					     </div>
					  </div>
					  <div class="billingaddr_div hide">
					     <div class="form-group">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						   <div class="row">
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text" class="requiredforbilling" name="billing_subscriber_name" value="<?php echo $billing_username; ?>" >
							    <label>Billing Name<sup>*</sup></label>
							 </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="tel" class="requiredforbilling" name="billing_subscriber_phone" maxlength="10" minlength="10" value="<?php echo $billing_mobileno; ?>" pattern="[6789][0-9]{9}" >
							    <label>Billing Mobile No.<sup>*</sup></label>
							 </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="email" class="requiredforbilling" name="billing_subscriber_email" value="<?php echo $billing_email; ?>" >
							    <label>Billing Email ID<sup>*</sup></label>
							 </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"  style="padding-left:0px;">
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text" class="requiredforbilling" name="billing_subscriber_flatno" value="<?php echo $billing_flat_number; ?>" >
							    <label>Billing Flat / Door No.<sup>*</sup></label>
							 </div>
						      </div>
						   </div>
						</div>
					     </div>
					     <div class="form-group">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						   <div class="row">
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text" class="requiredforbilling" name="billing_subscriber_address1" value="<?php echo $billing_address; ?>">
							    <label>Billing Address Line 1<sup>*</sup></label>
							 </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" >
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text" name="billing_subscriber_address2" value="<?php echo $billing_address2; ?>">
							    <label>Billing Address Line 2</label>
							 </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display: block; font-size: 16px; margin-bottom: 20px; padding-left: 0; padding-top: 25px;">
							 <div class="">
							    <select name="billing_state" id="billing_state_customer" onchange="getcitylist(this.value,'','','bstate')" style="width:100%" class="requiredforbilling">
							       <option value="">Billing State*</option>
							       <?php $this->user_model->state_list($billing_state); ?>
							    </select>
							 </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display: block; font-size: 16px; margin-bottom: 20px; padding-left: 0; padding-top: 25px;">
							 <div class="">
							    <select name="billing_city" id="billing_city_customer" class="bcitylist requiredforbilling" onchange="getzonelist(this.value,'','','bcity')" style="width:100%">
							       <option value="">Billing City*</option>
							    <?php $this->user_model->getcitylist($billing_state,$billing_city); ?>
							    </select>
							 </div>
						      </div>
						      
						   </div>
						</div>
					     </div>
					     <div class="form-group">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						   <div class="row">
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display: block; font-size: 16px; margin-bottom: 20px; padding-left: 0px;">
							 <div class="">
							    <select name="billing_zone" class="bzonelist requiredforbilling" style="width:100%" onchange="check_toadd_newzone(this.value, 'customer')">
							       <option value="">Billing Zone*</option>
							       <?php $this->user_model->zone_list($billing_city, $billing_zone, $billing_state); ?>
							    </select>
							 </div>
						      </div>
						   </div>
						</div>
					     </div>
					  </div>
					  <div class="form-group">
					     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="row"><hr></div>
					      </div>
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
						      <div class="mui-textfield mui-textfield--float-label">
							 <input type="text" name="gstin_number" value="<?php echo $subscriber_gstin_number ?>">
							 <label>GSTIN Number<sup>*</sup></label>
						      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                      <h4>Tax Type</h4>
						      <?php
						      $sgst_style=''; $igst_style='';
						      if($taxtype == '1'){
							 $sgst_style = "checked='checked'";
						      }else{
							 $igst_style = "checked='checked'";
						      }
						      ?>
						      
						      <label class="radio-inline">
						      <input type="radio" name="taxtype" value="1" required <?php echo $sgst_style ?>> CGST/SGST
						      </label>
						      <label class="radio-inline">
						      <input type="radio" name="taxtype" value="2" required <?php echo $igst_style ?>> IGST
						      </label>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_username" class="mui--is-untouched mui--is-dirty mui--is-not-empty" value="<?php echo $subscriber_username ?>" required>
                                                         <label>Username<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_uuid" readonly="readonly" class="mui--is-untouched mui--is-dirty mui--is-not-empty" style="color:#acacac" value="<?php echo $subscriber_uid ?>">
                                                         <label>UID<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
					  <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px">
                                                      <div class="row" style="margin-top:25px">
                                                         <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <h4 class="toggle_heading"> Priority Account</h4>
                                                         </div>
                                                         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <label class="switch">
                                                               <input type="checkbox" name="priority_account" id="priority_account" value="<?php echo $priority_account ?>" <?php echo ($priority_account==1) ? 'checked': '' ?>>
                                                               <div class="slider round"></div>
                                                            </label>
                                                         </div>
                                                      </div>
                                                   </div>
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px">
                                                      <div class="row" style="margin-top:25px">
							 <h4 style="display:inline-block"> Router Password: &nbsp;<i class='fa fa-key' aria-hidden='true'></i> <?php echo $radiuspassword; ?></h4>
							 <?php
							    $super_admin = $this->session->userdata['isp_session']['super_admin'];
							    if(($super_admin == '1') && ($active_user == '1')){
							       echo '&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="show_editrouterpasswordModal(\''.$radiuspassword.'\')">EDIT</a>';
							    }
							 ?>
						      
						      </div>
						   </div>
						   
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px">
                                                      <div class="row" style="margin-top:25px">
							 <h4 style="display:inline-block"> Customer Password: &nbsp;<i class='fa fa-key' aria-hidden='true'></i> <?php echo $customerpassword; ?></h4>
						      </div>
						   </div>
                                                </div>
                                             </div>
                                          </div>
					  
					  <?php
					  $byaddrchk=''; $bylatlngchk='';
					  if($geoaddress_searchtype == 'byaddr'){
					     $byaddrchk = 'checked="checked"';
					  }elseif($geoaddress_searchtype == 'bylatlng'){
					     $bylatlngchk = 'checked="checked"';
					  }
					  ?>
					  <div class="form-group">
					     <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                   <div class="mui-radio">
						      <label class="nopadding">Choose Geolocation Type <sup>*</sup> </label> &nbsp;
						      <input type="radio" name="geoaddress_searchtype" required="required" value="byaddr" <?php echo $byaddrchk ?>>By Address &nbsp;
						      <input type="radio" name="geoaddress_searchtype" required="required" value="bylatlng" <?php echo $bylatlngchk ?>>By Latitude/Longitude
						   </div>
                                                </div>
                                             </div>
					  </div>
					  <div class="geobyaddr hide">
					     <div class="form-group">
						<div class="row">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text" id="geolocation" name="geoaddr" value="<?php echo $geoaddr; ?>" required placeholder="">
							    <label>GeoAddress</label>
							</div>
						      </div>
						   </div>
						</div>
					     </div>
					     <div class="form-group">
						<div class="row">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text"  id="placeid"  value="<?php echo $placeid; ?>" name="placeid"  readonly>
							    <label>Placeid</label>
							  </div>
						      </div>
						      
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text"  id="lat" value="<?php echo $lat; ?>" name="lat" readonly>
							    <label>Lat</label>
							 </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">   
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text" id="long" name="long" value="<?php echo $long; ?>"  readonly>
							    <label>Long</label>
							</div>
						      </div>
						   </div>
						</div>
					      </div>
					  </div>
					  <div class="geobylatlng hide">
					     <div class="form-group">
						<div class="row">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding">         
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text" name="geolat" id="geolat" required value="<?php echo $lat; ?>">
							    <label>Lat</label>
							 </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">   
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text" name="geolong" id="geolong" required value="<?php echo $long; ?>">
							    <label>Long</label>
							</div>
						      </div>
						      <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="margin-top:20px; background-color:#a1a1a1; text-align:center; padding:5px; color:#ffffff;">
							 <span onclick="reverse_geolocation()" style="cursor:pointer">GET Address</span>
						      </div>
						   </div>
						</div>
					     </div>
					     <div class="form-group">
						<div class="row">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
							 <div class="mui-textfield">
							    <input type="text" id="geoaddress" name="geoaddress"  value="<?php echo $geoaddr; ?>" readonly>
							    <label>GeoAddress</label>
							  </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
							 <div class="mui-textfield">
							    <input type="text" id="geoplaceid" name="geoplaceid"  value="<?php echo $placeid; ?>" readonly>
							    <label>Placeid</label>
							  </div>
						      </div>
						   </div>
						</div>
					     </div>
					  </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px">
                                                      <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE" >
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <h3 class="create_headings">
                                                         Discard Changes
                                                      </h3>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
			      <?php } ?>
                              <?php if($khideperm){ ?>
			      <div class="mui-tabs__pane" id="KYC_details">
                                 <!--<h2>KYC Details</h2>-->
                                 <form action="" method="post" enctype="multipart/form-data" autocomplete="off" onsubmit="edit_subscriber_documents(); return false;" id="subscriber_documents_form">
                                    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
				    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
				    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row">
					  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
					     <div class="row">
						<div class="form-group">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <h4>Photo ID Documents</h4>
						      
						      <div class="idproof_divmaker">
							 <div class="row">
							    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							       <div class="mui-select" style="margin-bottom:5px">
								  <select name="idproof_doctype" onchange="activate_iddocupload()" >
								     <option value="">Select Document</option>
								     <option value="pan_card">Pan Card</option>
								     <option value="voterid_card">VoteriId Card</option>
								     <option value="aadhar_card">Aadhar Card</option>
								     <option value="passport">Passport</option>
								     <option value="driving_license">Driving License</option>
								     <option value="identity_card">Identity Card</option>
								  </select>
								  <input type="hidden" name="idproof_doctype_arr[]" id="idproof_doctype_arr"/>
							       </div>						 
							    </div>
							    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input type="text" maxlength="15" class="idproof_docnumber" >
								  <input type="hidden" name="idproof_docnumber_arr[]" id="idproof_docnumber_arr">
								  <label>Enter Document Number</label>
							       </div>
							    </div>
							    <div class="idpfileperoption">
							       <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" id="default_iddocdiv">
								  <span class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600">CHOOSE FILE</span>
							       </div>
							    </div>
							 </div>
							 <div class="row"><div id="idprooflisting"></div></div>
						      </div>
						   </div>
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px">&nbsp;</div>
						</div>
						
						<div class="form-group">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <h4>Address Proof Documents</h4>
						      <div class="row">
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <div class="mui-select" style="margin-bottom:5px">
							       <select name="kyc_documents" onchange="activate_docupload()" >
								  <option value="">Select Document</option>
								  <option value="pan_card">Pan Card</option>
								  <option value="aadhar_card">Aadhar Card</option>
								  <option value="bank_statement">Bank Statement</option>
								  <option value="rent_agreement">Rent Agreement</option>
								  <option value="electricity_bill">Electricity Bill</option>
								  <option value="ration_card">Ration Card</option>
								  <option value="driving_license">Driving License</option>
								  <option value="passport">Passport</option>
								  <option value="identity_card">Identity Card</option>
								  <option value="voterid_card">VoteriId Card</option>
								  <option value="registration_certificate">Registration Certificate</option>
							       </select>
							    <input type="hidden" name="kyc_documents_arr[]" id="kyc_documents_arr"/>
							    </div>
							 </div>
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <div class="mui-textfield mui-textfield--float-label">
							       <input type="text" class="kycdocnumber" maxlength="15"  >
							       <input type="hidden" name="kycdocnumber_arr[]" id="kycdocnumber_arr">
							       <label>Enter Document Number</label>
							    </div>
							 </div>
							 <div class="fileperoption">
							    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" id="defaultdocdiv">
							       <span class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600">CHOOSE FILE</span>
							    </div>
							 </div>
						      </div>
						      <div class="row"><div id="kyclisting"></div></div>
						   </div>
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px">&nbsp;</div>
						</div>
						
						<div class="form-group">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <h4>Corporate Documents</h4>
						      <div class="row">
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <div class="mui-select" style="margin-bottom:5px">
							       <select name="corporate_doctype" onchange="activate_corpdocupload()">
								  <option value="">Select Document</option>
								  <option value="electricity_bill">Electricity Bill</option>
								  <option value="authority_letter">Authority Letter</option>
								  <option value="registration">Registration</option>
							       </select>
							    <input type="hidden" name="corporate_doctype_arr[]" id="corporate_doctype_arr"/>
							    </div>
							 </div>
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <div class="mui-textfield mui-textfield--float-label">
							       <input type="text" class="corporate_docnumber">
							       <input type="hidden" name="corporate_docnumber_arr[]" id="corporate_docnumber_arr">
							       <label>Enter Document Number</label>
							    </div>
							 </div>
							 <div class="corpfileperoption">
							    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" id="default_corporatedocdiv">
							       <span class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600">CHOOSE FILE</span>
							    </div>
							 </div>
						      </div>
						      <div class="row"><div id="corporatelisting"></div></div>
						   </div>
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px">&nbsp;</div>
						</div>
						
						<div class="form-group">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="row">
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <h4>User Image</h4>
							    <span class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600" id="trigger_userimage">CHOOSE FILE</span>
							    <input type="file" id="user_profile_image" class="hide" name="user_profile_image" accept="image/*" />
							    <div id="user_image"></div>
							 </div>
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <h4>User Signature</h4>
							    <span class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600" id="trigger_signatureimage">CHOOSE FILE</span>
							    <input type="file" id="user_signature_image" class="hide" name="user_signature_image" accept="image/*" />
							    <div id="user_signature"></div>
							 </div>
						      </div>
						   </div>
						</div>
						<div class="form-group">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:25px">
						   <div class="row">
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							 <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block" value="SAVE" >
						      </div>
						   </div>
						</div>
					     </div>
					     </div>
					  </div>
					  
					  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					     <div class="row">
						<div class="form-group">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <label>Uploaded Files</label>
						      <a href="<?php echo base_url().'user/downloadkyc/'.$subscriber_id ?>" style="float:right;cursor:pointer;" >Download <i class="fa fa-download" aria-hidden="true"></i></a>
						   </div>
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="uploaded_docs"></div>
						</div>
					     </div>
					  </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                              <?php } ?>
			      <?php if($phideperm){ ?>
			      <div class="mui-tabs__pane <?php echo $pstyle ?>" id="Plan_details">
                                 <h2>
				    <a href="javascript:void(0)" class="plantabb" id="active_plan">Active Plan</a>
				    <?php if($active_user == '1'){ ?>
				    | <a href="javascript:void(0)" class="plantabb" id="next_cycle_plan">Change Plan</a>
				    <?php } ?>
				 </h2>
                                 <form action="" method="post" autocomplete="off" onsubmit="add_subscriber_plan(); return false;" id="subscriber_plandetail_form">
				    <input type="hidden" name="editplan" class="editplan" value='1'>
				    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
                                    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
				    <input type="hidden" name="plan_duration" class="plan_duration" value='1'>
				    <input type="hidden" name="custom_planid" class="custom_planid" value="" />
				    <input type="hidden" name="custom_planprice" class="custom_planprice" value="" />
				    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row">
                                          <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
					     <div class="form-group">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						   <div class="row">
						      <input type="radio" name="user_plan_type" value="prepaid" required /> Prepaid Plan User
						      &nbsp; &nbsp;
						      <input type="radio" name="user_plan_type" value="postpaid" required /> Postpaid Plan User
						   </div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hide" style="margin:10px 0px;color:#aeaeae; font-size:14px" id="prebillmonthlyopt">
						   <div class="row">
						      <input type="checkbox" name="prebill_oncycle" value="1" /> Monthly Bill according to BillingCycle
						   </div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						   <div class="row">
						      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left:0px;">
							 <div class="mui-select">
							    <select name="assign_plan" id="assign_plan" onchange="getplandetails(this.value)" required>
							    </select>
							 </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							 <a href="<?php echo base_url().'plan/add_plan' ?>" style="display:inline-block; margin-top: 20px;" target="_blank"> Create New Plan </a>
						      </div>
						      <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="padding: 20px 0px 0px">
							 AutoRenew at Every Cycle: <a class="planautorenewal" onclick="change_planautorenewal('<?php echo $subscriber_uid ?>')" href="javascript:void(0)"><img src="<?php echo base_url() ?>assets/images/on2.png" rel="disable"></a>
						      </div>
						   </div>
						</div>
						<div class="row">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							 <div class="row hide" id="plan_details">
							    <?php $this->load->view('users/Plan', $dataArr); ?>
							 </div>
						      </div>
						   </div>
						</div>
						<div class="form-group" style="overflow:hidden;">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:25px">
						      <div class="row">
							 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							    <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block" id="plansubmit" value="SAVE" >
							 </div>
						      </div>
						   </div>
						</div>
					     </div>
					     <hr/>
                                          </div>
                                          <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                             <div class="row">
                                                <div class="detail_plan">
                                                   <h4>OTHER DETAILS</h4>
                                                   <div class="row">
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <h5>Plan start date</h5>
                                                      </div>
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <h5 id="plan_start_date">: dd.mm.yyyy</h5>
                                                      </div>
                                                   </div>
                                                   <!--<div class="row">
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <h5>Paid Till</h5>
                                                      </div>
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <h5 id="plan_payment_till">: dd.mm.yyyy</h5>
                                                      </div>
                                                   </div>-->
                                                   <div class="row">
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <h5>Expiry On</h5>
                                                      </div>
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <h5 id="plan_expiry_on">: dd.mm.yyyy</h5>
                                                      </div>
                                                      <!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <h5 class="reset">Change</h5>
                                                      </div>-->
                                                   </div>
						   <div class="row">
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <h5>Next Billing Date</h5>
                                                      </div>
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <h5 id="next_billing_date">: dd.mm.yyyy</h5>
                                                      </div>
                                                   </div>
                                                   <h4 style="margin-top:20px">DATA CONSUMPTION</h4>
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <div class="row">
							 <?php
							    //echo '<pre>'; print_r($dataArr); die;
							    $used_percent = $dataArr['uses_percent'];
							    $usedlimit = $dataArr['live_usage'];
							    $availabledatamb = $dataArr['uses_limit_msg'];
							    $plan_type = $dataArr['plan_type'];
							    echo '
							    <div class="progress">
							       <div class="progress-bar" style="width: '.$used_percent.'%">'.$usedlimit.'</div>
							    </div>';
							    if($plan_type == '3'){
							       $postfup_data = $dataArr['postfup_data'];
							       $postfup_percent = $dataArr['postfup_percent'];
							       $topup_data = $dataArr['topup_data'];
							       $topup_percent = $dataArr['topup_percent'];
							       
							       
						   echo '      <div class="progress postfup_progress" style="height:auto;">
								  <span>POST FUP</span><br/>
								  <div class="progress-bar" style="width: '.$postfup_percent.'%">'.$postfup_data.'</div>
							       </div>
							       <div class="progress datatopup_progress" style="height:auto;">
								  <span>DATA TOPUP</span><br/>
								  <div class="progress-bar" style="width: '.$topup_percent.'%">'.$topup_data.'</div>
							       </div>';
							    }
						echo '	    <span style="float:right">'.$availabledatamb.'</span>';
							 ?>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <div class="row">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
				 <form action="" method="post" autocomplete="off" id="nextcycle_planform" class="hide">
                                    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
				    <input type="hidden" name="curr_user_plan_type" id="curr_user_plan_type" value="" />
				    <input type="hidden" name="custom_planid" class="custom_planid" value="" />
				    <input type="hidden" name="custom_planprice" class="custom_planprice" value="" />
                              
			            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row">
                                          <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
					     <div class="form-group">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						   <div class="row">
						      <input type="radio" name="nextuser_plan_type" value="prepaid" required /> Prepaid Plan User
						      &nbsp; &nbsp;
						      <input type="radio" name="nextuser_plan_type" value="postpaid" required /> Postpaid Plan User
						   </div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hide" style="margin:10px 0px;color:#aeaeae; font-size:14px" id="nextprebillmonthlyopt">
						   <div class="row">
						      <input type="checkbox" name="nextprebill_oncycle" value="1" /> Monthly Bill according to BillingCycle
						   </div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						   <div class="row">
						      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left:0px;">
							 <div class="mui-select">
							    <select name="nextassign_plan" id="nextassign_plan" onchange="nextplan_details(this.value)" required>
							    </select>
							 </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							 <a href="<?php echo base_url().'plan' ?>" style="display:block; margin-top: 20px;"> Create New Plan </a>
						      </div>
						      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							 <a href="javascript:void(0)" onclick="cancel_nextcycleplan()" style="display:block; margin-top: 20px;"> Cancel NextCycle Plan </a>
						      </div>
						   </div>
						</div>
						<div class="row">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							 <div class="row hide" id="nextplan_details">
							    <?php $this->view('users/NextPlan'); ?>
							 </div>
						      </div>
						   </div>
						</div>
						
					     </div>
					     <hr/>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                              <?php } ?>
			      <?php if($thideperm){ ?>
			      <div class="mui-tabs__pane" id="Topup">
				 <div class="col-lg-12 col-md-12 col-sm-12">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12">
					  <div class="row">
					     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-left">
						<h2>TOPUP</h2>
					     </div>
					     <!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 onauthorized">
						<button class="mui-btn mui-btn--small mui-btn--accent pull-right" id="add_datatopup">
						TOPUP
						</button>
					     </div>-->
					  </div>
				       </div>
				    </div>
				    <div class="row unauthorized hide" >
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <h3>User may be INACTIVE or No plan associated with this user</h3>
				       </div>
				    </div>
				       
				    <div class="onauthorized">
				       <div class="row">
					  <div class="mui-panel">
					     <div class="row">
						<form action="" method="post" id="addtopup_form" autocomplete="off" onsubmit="assingtopup_touser(); return false;">
						   <div class="form-group">
						      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:10px ">
							 <label class="radio-inline">
							 <input type="radio" name="topuptype" value="1" checked> Data Top up
							 </label>
							 <label class="radio-inline">
							 <input type="radio" name="topuptype" value="3"> Speed Top up
							 </label>
							 <label class="radio-inline">
							 <input type="radio" name="topuptype" value="2"> Data Unaccountancy Top up
							 </label>
						      </div>
						   </div>
						   <div class="form-group">
						      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							 <div class="row">
							    <div class="col-sm-3 col-xs-3">
							       <div class="mui-select">
								  <select name="active_topups" onchange="topup_pricing(this.value)" required>
								  </select>
								  <label>Active Topups<sup>*</sup></label>
							       </div>
							    </div>
							    <div class="col-sm-7 col-xs-7 hide" id="topuptimer">
							       <div class="row">
								  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
								     <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
									<div class="input-group" style="margin-top:-5px">
									   <div class="input-group-addon" style="padding:6px 10px">
									      <i class="fa fa-calendar"></i>
									   </div>
									   <input type="text" class="form-control top_startdate topup_validate"  name="topup_startdate" placeholder="TopUp Start Date*" disabled>
									</div>
								     </div>
								  </div>
								  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
								     <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
									<div class="input-group" style="margin-top:-5px">
									   <div class="input-group-addon" style="padding:6px 10px">
									      <i class="fa fa-calendar"></i>
									   </div>
									   <input type="text" class="form-control top_enddate topup_validate" name="topup_enddate" placeholder="TopUp End Date*" disabled  >
									</div>
								     </div>
								  </div>
							       </div>
							    </div>
							 </div>
						      </div>
						   </div>
						   <div class="form-group">
						      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							 <input type="hidden" id="topup_actualcost" />
							 <input type="hidden" name="topup_totalcost" value="" />
							 <input type="hidden" id="topup_dayscount" name="topup_dayscount" value="0" />
							 <input type="hidden" name="topup_weekdays" value="" />
							 Daily TopUp Cost: <span id="topup_price"></span> &nbsp;&nbsp;&nbsp;
							 Total Cost: <span id="topup_totalcost"></span> <br/>
							 Applicable Days: <span id="applicable_days"></span>
						      </div>
						   </div>
						   <div class="form-group">
						      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							 <input type="submit" class="mui-btn mui-btn--small mui-btn--accent" value="Apply TopUp" />
						      </div>
						      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 hide" id="topup_err" style="color:#f00;"></div>
						   </div>
						</form>
					     </div>
					  </div>
				       </div>
				       <div class="row" >
					  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					     <div class="table-responsive">
						<table class="table table-striped">
						   <thead>
						      <tr class="active">
							 <th>&nbsp;</th>
							 <th>APPLIED ON</th>
							 <th>TOPUP TYPE</th>
							 <th>TOPUP NAME</th>
							 <th>TOPUP PRICE</th>
							 <th>TOPUP START DATE</th>
							 <th>TOPUP END DATE</th>
							 <th>STATUS</th>
							 <th>ACTIONS</th>
						      </tr>
						   </thead>
						   <tbody id="userassoc_topuplist"></tbody>
						</table>
					     </div>
					  </div>
				       </div>
				    </div>
				 </div>
			      </div>
                              <?php } ?>
                              <?php if($bhideperm){ ?>
			      <div class="mui-tabs__pane" id="Billing">
				 <div class="row">
				    <div class="col-sm-12 col-xs-12">
				       <ul class="mui-tabs__bar">
					  <?php $mbillhideperm= $this->user_model->is_permission('MONTHLYTAB','HIDE');
					     if($mbillhideperm){ ?>
					  <li id="mbilltab">
					     <a data-mui-toggle="tab" data-mui-controls="active_billing_pane">Monthly Billing</a>
					  </li>
					  <?php } ?>
					  <?php $cbillhideperm= $this->user_model->is_permission('CUSTOMBILLS','HIDE');
					     if($cbillhideperm){ ?>
					  <li id="cbilltab">
					     <a data-mui-toggle="tab" data-mui-controls="custom_billing_pane" id="click_custombillingtab">Custom Billing</a>
					  </li>
					  <?php } ?>
					  <?php $sbillhideperm= $this->user_model->is_permission('SETUPBILLS','HIDE');
					     if($sbillhideperm){ ?>
					  <li id="sbilltab">
					     <a data-mui-toggle="tab" data-mui-controls="setup_billing_pane">Setup Billing</a>
					  </li>
					  <?php } ?>
				       </ul>
				    </div>
				    <div class="col-sm-12 col-xs-12">
				       <?php if($mbillhideperm){ ?>
				       <div class="mui-tabs__pane" id="active_billing_pane">
					  <div class="row">
					     <div class="col-sm-12 col-xs-12">
						<div id="inactive_billing"  class="hide">
						   No Active Plan Associated With This User.
						</div>
						<div id="active_billing" class="hide">
						   <h2 style="font-size:15px; margin:15px 0px;">
						   Wallet Balance: <span id="user_wallet_amt"></span> |
						   Wallet Credit Limit: <span id="wallet_credit_limit"><?php echo $ispcodet['currency'] ?> 0.00</span> &nbsp;
						   <a href="javascript:void(0)" style="text-decoration:underline" onclick="exceed_walletlimit('modalshow')" id="exceedlimit">Change Limit</a> |
						   Blocked Balance: <span id="plan_blocked_amt"><?php echo $ispcodet['currency'] ?> 0.00</span>&nbsp;<a href="javascript:void(0)"id="_advanced_prepaydetails" onclick="showadvancedpayment_details()" title="Advanced Payment Details"><i class="fa fa-info-circle" aria-hidden="true"></i></a> &nbsp; <a href="javascript:void(0)" id="_advance_prepayModal">Advance Prepay</a>
						   | <a href="javascript:void(0)" id="_addtowallet">Add to Wallet</a>
						   
						   </h2>
						   <input type="hidden" id="net_walletamt" />
						   <div class="row" style="clear:both">
						      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							 <div class="row">
							    <div class="col-sm-6 col-xs-6">
							       <div style="font-size:15px; text-align:right;">
								  <a href="javascript:void(0)" onclick="exportbillpdf_tousers()" class="hide billpdfexport" title="export to pdf" style="float: left;margin-right:10px;"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a> &nbsp;
								  <a href="javascript:void(0)" onclick="exportbillexcel_tousers()" class="hide billexcelexport" title="export to excel" style="float: left;margin-right:10px;"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
								  <form action="<?php echo base_url().'user/pdfexport_userbilling' ?>" method="post" class="hide billpdf_exportform" target="_blank">
								     <input type="hidden" name="billids_toexport" value=""/>
	       
								  </form>
								  <form action="<?php echo base_url().'user/excelexport_userbilling' ?>" method="post" class="hide billexcel_exportform" target="_blank">
								     <input type="hidden" name="billids_toexport" value=""/>
	       
								  </form>
								  <span id="totalbill_amt"></span>
							       </div>
							       <div class="table-responsive">
								  <table class="table table-striped">
								     <thead>
									<tr class="active">
									   <th>&nbsp;</th>
									   <th>
									      <div class="checkbox" style="margin-top:0px; margin-bottom:0px;"><label><input type="checkbox" class="collapse_allbillcheckbox"></label></div>
									   </th>
									   <th>BillNo.</th>
									   <th>BillType</th>
									   <th>Amount</th>
									   <th>DateAdded</th>
									   <th colspan="3">Action</th>
									</tr>
								     </thead>
								     <tbody id="debitbill_summary"></tbody>
								  </table>
							       </div>
							       <center>
								  <input type='hidden' id="bill_limit" value="" />
								  <input type='hidden' id="bill_offset" value="" />
								  <div class="bill_loadmore hide">
								     <span style="padding:5px; border:1px solid; cursor:pointer" onclick="loadmore_billing('user','loadmore_content','customerbilling','')">Load More</span>
								  </div>
								  <div class="bill_loadmore_loader hide">
								     <img src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
								  </div>
							       </center>
							    </div>
							    <div class="col-sm-6 col-xs-6 nopadding-left">
							       <div style="font-size:15px; text-align:right;">
								  <span id="totalwallet_amt"></span>
							       </div>
							       <div class="table-responsive">
								  <table class="table table-striped">
								     <thead>
									<tr class="active">
									   <th>&nbsp;</th>
									   <th>
									      <!--<div class="checkbox" style="margin-top:0px; margin-bottom:0px;"><label><input type="checkbox" class="collapse_allrcptcheckbox"></label></div>-->
									      &nbsp;
									   </th>
									   <th>RcptNo</th>
									   <th>Amount</th>
									   <th>PayMethod</th>
									   <th>RcptDate</th>
									   <th colspan="2">Action</th>
									</tr>
								     </thead>
								     <tbody id="creditbill_summary"></tbody>
								  </table>
							       </div>
							       <center>
								  <input type='hidden' id="rcptbill_limit" value="" />
								  <input type='hidden' id="rcptbill_offset" value="" />
								  <div class="rcptbill_loadmore hide">
								     <span style="padding:5px; border:1px solid; cursor:pointer" onclick="loadmore_rcptbilling('user','loadmore_content','receiptbilling','')">Load More</span>
								  </div>
								  <div class="rcptbill_loadmore_loader hide">
								     <img src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
								  </div>
							       </center>
							    </div>
							 </div>
						      </div>
						   </div>
						</div>
					     </div>
					  </div>
				       </div>
				       <?php } ?>
				       <?php if($cbillhideperm){ ?>
				       <div class="mui-tabs__pane" id="custom_billing_pane">
					  <div class="row">
					     <div class="col-sm-12 col-xs-12">
						<div class="col-sm-3 col-xs-3 pull-right addcustbillbtn">
						   <button class="mui-btn mui-btn--accent pull-right" style="height:30px; line-height: 30px; margin-top:15px; font-size:12px;" id="_custombillModal">Add Custom Billing<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
						</div>
						<div class="col-sm-12 col-xs-12 listcustombilldiv">
						   <div class="row">
						      <h3 style="margin:0px">Balance Amount: <span id="showbalcustbillamt"></span></h3>
						      <div class="row">
							 <div class="col-sm-7 col-xs-7">
							    <span id="showtotalcustbillamt" style="display:block;text-align:right;width:100%;"></span>
							    <div class="table-responsive">
							       <table class="table table-striped">
								  <thead>
								     <tr class="active">
									<th>&nbsp;</th>
									<th>BillNo.</th>
									<th>Enter Rcpt</th>
									<th>Items</th>
									<th>Amount</th>
									<!--<th>STATUS</th>-->
									<th>Action</th>
									<th>AddedOn</th>
									<!--<th>MODE OF PAYMENT</th>-->
									<!--<th colspan="2">ACTION</th>-->
								     </tr>
								  </thead>
								  <tbody id="custombill_summary">
								     
								  </tbody>
							       </table>
							    </div>
							 </div>
							 <div class="col-sm-5 col-xs-5">
							    <span id="showtotalcustbillrcpt" style="display:block;text-align:right;width:100%;"></span>
							    <div class="table-responsive">
							       <table class="table table-striped">
								  <thead>
								     <tr class="active">
									<th>&nbsp;</th>
									<th>BillNo.</th>
									<th>RcptNo.</th>
									<th>Amount</th>
									<th>AddedOn</th>
									<th>Payment</th>
									<th>Action</th>
									<!--<th colspan="2">ACTION</th>-->
								     </tr>
								  </thead>
								  <tbody id="custombill_rcptsummary">
								     
								  </tbody>
							       </table>
							    </div>
							 </div>
						      </div>
						   </div>
						</div>
						<div class="col-sm-12 col xs-12 addcustombilldiv hide">
						   <h2 id="custformtitle" style="margin:10px 0px 0px; color:#a1a1a1"></h2>
						   <div class="custbill_errortext"></div>
						   <div class="custbillloading hide" style="width:100%;position:fixed;text-align:center;z-index:9999"><img src="<?php echo base_url() ?>assets/images/loader.svg"/></div>
						   <div class="row">
						      <form method="post" action="" id="custombillingform" autocomplete="off" onsubmit="generate_custombilling(); return false;">
							 <input type="hidden" class="custom_billid" name="custom_billid" value="" />
							 <div class="invoice_container">
					 
							     <div class="form-group">
								 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								     <div class="row">
									 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
									     <div class="mui-textfield">
										 <input type="text" placeholder="Invoice number" name="custom_billnumber" value="" required>
										 <label class="invoice_lable">Invoice number<sup>*</sup></label>
									     </div>
									 </div>
									 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 hidewhenedit">
									     <div class="mui-textfield" style="padding-top:0px">
										 <div class="input-group" style="margin-top:-5px">
										     <div class="input-group-addon" style="padding:6px 10px">
											 <i class="fa fa-calendar"></i>
										     </div>
										     <div class="mui-textfield">
											 <input type="text" class="custom_billdate" placeholder="Created date*" required>
											  <input type="hidden" name="custom_billdbdate" />
											 <label class="invoice_lable">Created date<sup>*</sup></label>
										     </div>
										 </div>
									     </div>
									 </div>
								     </div>
								 </div>
							     </div>
							     <div class="form-group">
								 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  invoice_tb">
								     <div class="invoice_add_btn">
									 <label class="invoice-control_lable">
									  Add Company
									 </label>
								     </div>
								     <div class="selcompany_custbill">
								       <div class="col-sm-5 col-xs-5" style="padding:0px">
									  <select class="form-control" name="billcompany_name" id="company_namelist" onchange="calculate_custbilltax(this.value)" required><option value="">Select Company</option></select>
								       </div>
								       <div class="col-sm-2 col-xs-2" style="padding:15px">
									  <a href="javascript:void(0)" onclick="showadd_companyModal()">
									     <span><i class="fa fa-plus" aria-hidden="true"></i></span>
									     <span>ADD MORE</span>
									  </a>
								       </div>
								    </div>
								 </div>
								 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								     <div class="invoice_add_btn">
									  <label class="invoice-control_lable">
									   Add Product
									  </label>
								      </div>
								     <div class="selproduct_custbill">
									<div class="col-sm-5 col-xs-5" style="padding:0px">
									   <select class="form-control" name="billcompany_topics" id="topics_namelist" required onchange="gettopicdetails(this.value)"><option value="">Select Product</option></select>
									</div>
									<div class="col-sm-2 col-xs-2" style="padding:15px">
									   <a href="javascript:void(0)" onclick="showadd_topicModal()">
									      <span><i class="fa fa-plus" aria-hidden="true"></i></span>
									      <span>ADD MORE</span>
									   </a>
									</div>
									<div class="col-sm-2 col-xs-2" style="padding:15px">
									   <a href="javascript:void(0)" onclick="showadded_topicModal()">
									      <span><i class="fa fa-eye" aria-hidden="true"></i></span>
									      <span>View Products</span>
									   </a>
									</div>
								     </div>
								 </div>
								 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 otherproductdetails hide">
								  <div class="row">
								     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
									 <div class="mui-textfield">
									     <input type="text" placeholder="Price" name="billprodprice" value="" required>
									     <label class="invoice_lable">Price<sup>*</sup></label>
									 </div>
								     </div>
								     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
									 <div class="mui-textfield">
									     <input type="text" placeholder="Quantity" name="billprodqty" value="1" pattern="^[1-9]\d*$" required>
									     <label class="invoice_lable">Qty<sup>*</sup></label>
									 </div>
								     </div>
								     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
									 <button type="button" class="mui-btn mui-btn--danger mui-btn--raised pull-right invoice_btn_m" style="width:110px; height:25px; padding:0px" onclick="addcustombill_invoice()">Add to List</button>
								     </div>
								  </div>
								 </div>
								 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
								     <div class="table-responsive">
									 <table class="table">
									     <thead>
										 <tr>
										     <th colspan="5">Label</th>
										     <th>Price</th>
										     <th>Quantity</th>
										     <th>&nbsp;</th>
										     <th class="text-right">Total</th>
										 </tr>
									     </thead>
									     <tbody id="custbill_prodlist">
										 <tr bgcolor="#f9f9f9" id="prod_blanklist"><td colspan="5" width="550px">&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td class="text-right">&nbsp;</td><td class="text-right">&nbsp;</td></tr>
										 <tr bgcolor="#f9f9f9">
										     <td colspan="6">
											 <textarea class="form-control invoice-control" name="cuctombill_remarks"></textarea>
										     </td>
					 
										     <td colspan="2">Subtotal</td>
										     <td class="text-right" id="invoice_subtotal">0</td>
										     <td class="text-right">&nbsp;</td>
										 </tr>
										 <tr bgcolor="#ffffff">
										     <td colspan="6">
											This text will be displayed on the invoice.
										     </td>
					 
										     <td colspan="2"><strong>Taxes <span id="custbilltax_ratio"></span></strong></td>
										     <td colspan="1" class="text-right" id="custbilltaxamt"></td>
										 </tr>
										 <tr bgcolor="#ffffff">
										     <td colspan="6">&nbsp;</td>
										     <td colspan="2"><strong>Discount</strong></td>
										     <td colspan="1" class="text-right">
										       <input type="text" name="custbill_discount" id="custbill_discount" pattern="^[0-9]\d*$" style="width:40px;text-align:right;" value="0"/>
										     </td>
										 </tr>
										 <tr bgcolor="#ffffff">
										     <td colspan="6">&nbsp;</td>
										     <td colspan="2"><strong>Net Amount</strong></td>
										     <td colspan="1" class="text-right" id="totalcustbillamt">0</td>
										    
										 </tr>
									     </tbody>
									 </table>
								     </div>
								 </div>
							     </div>
					 
							 </div>
							 <div class="invoice_container">
							    <div class="form-group">
							       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								  <div class="invoice_add_btn">
								     <label class="invoice-control_lable" style="padding: 5px !important; font-size: 13px">Bill Period Type</label>
								 </div>
								  <label class="radio-inline">
								     <input type="radio" name="custom_billing_type" value="monthly"> Monthly-wise
								  </label>
								  <label class="radio-inline">
								     <input type="radio" name="custom_billing_type" value="day"> Day-wise
								  </label>
							       </div>
							    </div>
							     <div class="form-group">
								 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								     <div class="row">
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 monthly-wise hide">
									   <select class="form-control" name="custombill_monthwise">
									      <option value="0">Select Billing Period</option>
									      <?php
									      for($m=1; $m <=12; $m++){
										 echo '<option value="'.$m.'">At every '.$m.' month</option>';
									      }
									      ?>
									   </select>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 day-wise hide">
									   <select class="form-control" name="custombill_daywise">
									      <option value="0">Select Billing Period</option>
									      <?php
									      for($d=1; $d <=365; $d++){
										 echo '<option value="'.$d.'">At every '.$d.' day</option>';
									      }
									      ?>
									   </select>
									</div>
								     </div>
								 </div>
							     </div>
							     <div class="form-group">
								 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								  <small class="invoice_small">Please note that, this will generate one or more extra bills if latest invoice date + billing period is less than Today date</small> 
								 </div>
							     </div>
							 </div>
							 <div class="invoice_container">
					 
							     <div class="form-group">
								 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								     <label class="invoice-control_lable">Admin notes</label>
								     <textarea class="form-control invoice-control" rows="3" style="margin:10px 0px; height:80px"  name="cuctombill_comments"></textarea>
								     <small class="invoice_small">This text will not be displayed on the invoice and is never shown to client.</small>
								 </div>
							     </div>
							 </div>
							 
							 <input type="submit" class="mui-btn mui-btn--accent mui-btn--raised pull-right invoice_btn_m" style="height: 36px;line-height: 18px;" value="Save"/>
					 
							 <button type="button" class="mui-btn mui-btn--danger mui-btn--raised pull-right invoice_btn_m" onclick="custombill_listing()">cancel</button>
						      </form>
						      <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
							 <div class="modal-dialog modal-sm" role="document">
							       <div class="modal-content">
								<div class="modal-header">
								       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								       <h4 class="modal-title" id=""><strong>Delete row</strong></h4>
								  </div>
								  <div class="modal-body" style="padding-top:0px;">
									  <div class="col-sm-12 col-xs-12">
										  <p>Do you really want to delete this row?</p>
									  </div>
								 </div>
								 <div class="modal-footer">
								       <center>  
									       <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
									       <button type="button" class="btn btn-danger">Delete</button>
								       </center>  
								 </div>
							       </div>
							 </div>
						      </div>
						   </div>
						</div>
					     </div>
					  </div>
				       </div>
				       <?php } ?>
				       <?php if($sbillhideperm){ ?>
				       <div class="mui-tabs__pane" id="setup_billing_pane">
					  <div class="row" style="clear:both">
					     <div class="billing_list" style="margin:5px 0px">
						<ul style="float:right">
						   <li style="margin:0px 5px"><button class="mui-btn mui-btn--small mui-btn--accent diag_readable" style="height:36px; line-height:35px; font-weight:600" id="_installationModal">Add Installation Cost</button></li>
						   <li style="margin:0px 5px"><button class="mui-btn mui-btn--small mui-btn--accent diag_readable" style="height:36px; line-height:35px; font-weight:600" id="_securityModal">Add Security Cost</button></li>
						</ul>
					     </div>
					     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="row">
						   <div class="col-sm-12 col-xs-12">
						      <div class="table-responsive">
							 <table class="table table-striped">
							    <thead>
							       <tr class="active">
								  <th colspan="8" style="color: #000;font-size:14px">Installation & Security Charges</th>
							       </tr>
							    </thead>
							    <tbody id="instsecu_bill_summary"></tbody>
							 </table>
						      </div>
						   </div>
						</div>
					     </div>
					  </div>
				       </div>
				       <?php } ?>
				    </div>
				 </div>
                              </div>
			      <?php } ?>
                              <?php if($shideperm){ ?>
			      <div class="mui-tabs__pane" id="Settings">
				 <h2>SETTINGS</h2>
                                 <form action="" method="post" id="syssetting_form" autocomplete="off" onsubmit="system_setting(); return false;">
				    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
				    <div class="form-group">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-bottom:15px">
					  <h4>LOGIN TYPE</h4>
					  <div class="row" style="margin-top:5px;">
					     <span style="padding-left:15px">
						<input name="logintype" class="logintype" value="pppoe" type="radio" checked required><span class="circle"></span><span class="check"></span> PPPOE &nbsp; &nbsp;
						<input name="logintype" class="logintype" value="hotspot" type="radio" required><span class="circle"></span><span class="check"></span> HOTSPOT &nbsp; &nbsp;
                                                <input name="logintype" class="logintype" value="ill_ipuser" type="radio" required><span class="circle"></span><span class="check"></span> IP User
					     </span>
					  </div>
					  <div class="row nomargin-left addhotspotMacID hide">
					     <input type="hidden" name="radchk_clearid" id="radchk_clearid" value="" />
					     <input type="hidden" name="radchk_simuseid" id="radchk_simuseid" value="" />
					     <input type="text" name="hotspotMacID" id="hotspotMacID" />
					     <label id="hotspotMacID_err" style="color:#f00"></label>
					  </div>
				       </div>
				    </div>
                                    <div class="form-group ill_userpanel hide">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-bottom:15px">
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="mui-select">
                                                   <select name="nas_illoptions" required>
                                                      <option value="">Select Nas</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="mui-radio">
                                                   <label class="nopadding">IP Address Type <sup>*</sup> </label> <br/>
                                                   <input type="radio" name="ill_iptype" value="public" checked required> Public &nbsp;
                                                   <input type="radio" name="ill_iptype" value="private" required> Private
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="">
                                                   <select class="form-control" name="nas_iprange[]" multiple required>
                                                      <option value="">Select Available IP</option>
                                                   </select>
                                                   <!--<select class="form-control" name="nas_iprange" required>
                                                      <option value="">Select Available IP</option>
                                                   </select>-->
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
				    <div class="form-group not_ill_userpanel">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
					  <h4>IP ADDRESS TYPE</h4>
					  <div class="row nomargin-left">
					     <label>
					     <input name="ipaddrtype" class="ipaddrtype" value="0" type="radio" required><span class="circle"></span><span class="check"></span> Static IP
					     </label>
					     <label style="padding-left:30px">
					     <input name="ipaddrtype" class="ipaddrtype" value="1" type="radio" required><span class="circle"></span><span class="check"></span> Dynamic IP
					     </label>
					     <label style="padding-left:30px">
					     <input name="ipaddrtype" class="ipaddrtype" value="2" type="radio" required><span class="circle"></span><span class="check"></span> IP Pool
					     </label>
					  </div>
					  <div class="row nomargin-left addstaticip hide">
					     <input type="text" name="ipaddress" id="ipaddress" />
					     <label id="staticipaddr_err" style="color:#f00"></label>
					  </div>
					  <div class="row addippoolip hide">
					     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<strong>Available IPs:</strong> <span id="total_poolips"></span> <br/>
						<strong>Assigned IPAddress:</strong> <span id="poolip_assigned"></span>
						<div class="mui-select">
						   <select name="ipaddress_pool" id="ipaddress_pool" >
						      <option value="">Select IP Pool</option>
						   </select>
						</div>
						<label id="poolipaddr_err" style="color:#f00"></label>
					     </div>
					  </div>
				       </div>
				    </div>
				   
				    <div class="form-group">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-bottom:15px">
					  <h4>MAC ID BINDING</h4>
					  <div class="row nomargin-left">
					     <strong>Current MAC ID: </strong>
					     <label id="curr_macid"><?php echo $mac; ?></label>
					  </div>
					  <div class="row nomargin-left" style="margin-top:5px;">
					     <strong>Bind Mac: </strong>
					     <span style="padding-left:30px">
						<input name="bindmaclock" value="1" type="radio" required><span class="circle"></span><span class="check"></span> Yes &nbsp; &nbsp;
						<input name="bindmaclock" value="0" type="radio" required><span class="circle"></span><span class="check"></span> No
					     </span>
					     <!--<a href="javascript:void(0)" style="padding-left:30px" data-toggle="modal" data-target="#resetMacModal" >Reset Mac</a>-->
					  </div>
				       </div>
				    </div>
				    <div class="form-group">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <div class="row">
					     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block" value="SAVE" />
					     </div>
					  </div>
				       </div>
				    </div>
                                 </form>
                              </div>
                              <?php } ?>
                              <?php if($rhideperm){ ?>
			      <div class="mui-tabs__pane" id="Requests">
                                 <!--<h2 class="isticketperm">
                                    <a href="javascript:void(0)" id="create_newticket">Create New Service Request</a>
                                 </h2>-->
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                       <h2>Service Requests</h2>
                                    </div>
                                 </div>
				 <div class="col-sm-12 col-xs-12">
				    <ul class="mui-tabs__bar">
				       <li class="mui--is-active" >
					  <a data-mui-toggle="tab" data-mui-controls="opentickets_pane" onclick="fetch_allopenticket()">Open Tickets</a>
				       </li>
				       <li>
					  <a data-mui-toggle="tab" id="ticketclosedli" data-mui-controls="closetickets_pane">Tickets History</a>
				       </li>
				       <li>
					  <a data-mui-toggle="tab" id="tickettypeli" data-mui-controls="addtickettype_pane">Tickets Types</a>
				       </li>
				       <li>
					  <a data-mui-toggle="tab" id="create_newticket" data-mui-controls="addnewtickets_pane">Add New Ticket</a>
				       </li>
				    </ul>
				 </div>
				 <div class="mui-tabs__pane mui--is-active" id="opentickets_pane">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:50px 0px;">
					  <div class="table-responsive">
					     <table class="table table-striped">
						<thead>
						   <tr class="active">
						      <th>&nbsp;</th>
						      <th>REQUEST ID</th>
						      <th>DATE</th>
						      <th>TYPE</th>
						      <th colspan="2">STATUS</th>
						      <th style="text-align:center">TAT DAYS</th>
						      <th>PRIORITY</th>
						      <th>ACTIONS</th>
						      <th>CREATED BY</th>
						      <th style="text-align:center">ASSIGN TO</th>
						   </tr>
						</thead>
						<tbody id="allticketdiv"></tbody>
					     </table>
					  </div>
				       </div>
				    </div>
				 </div>
				 <div class="mui-tabs__pane" id="closetickets_pane">
				    <!--<div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:10px 0px 0px;text-align:right;">
					  <span style="font-size:16px;">Filter By: </span> &nbsp; &nbsp;
					  <span><select name="ticket_type" style="font-size:15px;"></select></span> &nbsp; &nbsp;
					  <span>
					     <input name="search_tktno" id="search_tktno" placeholder="Search Ticket" style="
					     height:20px;width:20%;padding: 0px 5px;border:none;border-bottom:1px solid #dddddd;"/>
					  </span> &nbsp; &nbsp;
				       </div>
				    </div>-->
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:50px 0px;">
					  <div class="table-responsive">
					     <table class="table table-striped">
						<thead>
						   <tr class="active">
						      <th>&nbsp;</th>
						      <th>REQUEST ID</th>
						      <th>DATE</th>
						      <th>TYPE</th>
						      <th colspan="2">STATUS</th>
						      <th style="text-align:center">TAT DAYS</th>
						      <th>PRIORITY</th>
						      <th>CREATED BY</th>
						      <th style="text-align:center">ASSIGN TO</th>
						      <th>CLOSED ON</th>
						      <th>ACTIONS</th>
						   </tr>
						</thead>
						<tbody id="allclosedticketdiv"></tbody>
					     </table>
					  </div>
				       </div>
				    </div>
				 </div>
				 <div class="mui-tabs__pane" id="addtickettype_pane">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:50px 0px;">
					  <form action="" method="post" id="ticket_types_form" autocomplete="off" onsubmit="add_ticket_types(); return false;">
					     <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
					     <input type="hidden" name="tickettypeid" value=''>
					     <div class="col-sm-5 col-xs-5">
						<div class="row" style="margin-bottom:20px;">
						   <div class="mui-textfield">
						      <input type="text" name="tickettype_name" value="" required>
						      <label>Ticket Type <sup>*</sup></label>
						   </div>
						</div>
						<input type="submit" class="mui-btn mui-btn--primary mui-btn--flat" style="background-color:#f00f64; color:#fff" value="SUBMIT" >
					     </div>
					  </form>
					  <div class="col-sm-5 col-xs-5" style="border-left:1px solid #f1f1f1">
					     <div class="table-responsive">
						<table class="table table-striped">
						   <thead>
						      <tr class="active">
							 <th>&nbsp;</th>
							 <th>TYPE</th>
							 <th colspan="2" style="text-align:center">ACTIONS</th>
						      </tr>
						   </thead>
						   <tbody id="alltickettypesdiv"></tbody>
						</table>
					     </div>
					  </div>
				       </div>
				    </div>
				 </div>
				 <div class="mui-tabs__pane" id="addnewtickets_pane">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:50px 0px;">
					  <form action="" method="post" id="ticket_request_form" autocomplete="off" onsubmit="add_ticket_request(); return false;">
					     <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
					     <div class="col-sm-7 col-xs-7">
						<div class="row" style="margin-bottom:20px;">
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						      <div class="mui-textfield">
							 <input type="text" name="ticketid" value="" readonly="readonly"  required>
							 <label>Ticket ID <sup>*</sup></label>
						      </div>
						   </div>
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						      <div class="mui-textfield" id="tktdtme">
							 <label>Ticket DateTime<sup>*</sup></label>
						      </div>
						   </div>
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						      <div class="mui-textfield">
							 <input type="text" name="customer_uuid" value="" readonly="readonly" required>
							 <label>Customer ID <sup>*</sup></label>
						      </div>
						   </div>
						</div>
						<div class="row">
						   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						      <div class="mui-select">
							 <select name="ticket_type" required>
							    <?php //echo $this->user_model->ticketstype_optionslist(); ?>
							 </select>
							 <label>Ticket Types <sup>*</sup></label>
						      </div>
						   </div>
						   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						      <div class="mui-select">
							 <select name="ticket_priority" required>
							    <option value="">Select Ticket Priority</option>
							    <option value="low">Low</option>
							    <option value="medium">Medium</option>
							    <option value="high">High</option>
							 </select>
							 <label>Priority<sup>*</sup></label>
						      </div>
						   </div>
						</div>
						<div class="row">
						   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						      <div class="mui-select">
							<select name="ticket_assignto">
							    <option value="">Assign Ticket</option>
							 </select>
							 <label>Assign Ticket<sup></sup></label>
						      </div>
						   </div>
						   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						      <div class="mui-textfield">
							 <textarea style="resize: vertical;" rows="4" name="ticket_description" required></textarea>
							 <label>Ticket Description<sup>*</sup></label>
						      </div>
						   </div>
						</div>
						<div class="row">
						   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						      <div class="mui-textfield mui-textfield--float-label">
							 <input type="text" name="ticket_comment">
							 <label>Comments (If any)</label>
						      </div>
						   </div>
						</div>
						<button type="button" class="mui-btn mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
						<input type="submit" class="mui-btn mui-btn--primary mui-btn--flat" style="background-color:#f00f64; color:#fff" value="ADD" >
					     </div>
					  </form>
				       </div>
				    </div>
				 </div>
			      </div>
                              <?php } ?>
                              <?php if($lhideperm){ ?>
			      <div class="mui-tabs__pane activity_logstab" id="Logs">
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				       
				       <div class="row">
					  <div class="col-sm-3 col-xs-3">
					     <div class="row">
					     <div class="garfes_div">
						 <ul>
						   <li>
						     <input type="radio" value="1" id="usage-garfes" name="logs_activity_graph" checked="checked">
						     <label for="usage-garfes">USAGE GRAPH </label>
						     <div class="check"></div>
						   </li>
						 </ul>
					      </div>
					     </div>
					    </div>
					  <div class="col-sm-4 col-xs-4">
					     <div class="row">
					      <div class="garfes_div">
						  <ul>
						     <li>
						      <input type="radio" value="2" id="speed-garfes" name="logs_activity_graph">
						      <label for="speed-garfes">SPEED GRAPH</label>
						      <div class="check"><div class="inside"></div></div>
						    </li>
						  </ul>
					       </div>
					     </div>
					   </div>
                                        </div>

				       <div id="usages_graph_div">
					  <div class="row" >
					     <div class="col-sm-2 col-xs-2">
						<h2>Usage</h2>
					     </div>
					     <div class="col-sm-10 col-xs-10">
						<div class="col-md-4 col-sm-4 col-xs-4 demo">
						   <i class="glyphicon glyphicon-calendar fa fa-calendar datefinder_icons"></i>
						   <input value="<?php echo $this->user_model->usage_graph_billing_date($subscriber_uid)?>" name="datefilter" class="datefinder" type="text" placeholder="DD-MM-YYYY" onblur="this.placeholder='DD-MM-YYYY'" onfocus="this.placeholder=''" >
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
						   <div class="form-inline">
						      <label class="form-inline_lable"><span>Total used: </span><strong id="total_used">0 GB</strong> </label>
						      <label class="form-inline_lable"><span>Total download: </span><strong id="total_download">0 GB</strong> </label>
						      <label class="form-inline_lable"><span>Total upload: </span><strong id="total_upload">0 GB</strong> </label>
						   </div>
						</div>
					     </div>
					  </div>
					  <div class="bar_chart" id="chartContainer" style="height: 300px; width: 80%">
					     <img src="" class="img-responsive"/>
					  </div>
				       </div>
				       
                                       
				       <div id="data_graph_div" style="display:none">
					  <div class="row" >
                                            <div class="col-sm-3 col-xs-3"><h2>Data</h2></div>
                                            <div class="col-sm-6 col-xs-6">
					       <div class="row">
                                                <div class="col-md-12 demo">
						   <div class="row"> 
						   <div class="col-sm-3 col-xs-3">
						      <label style=" font-size: 12px; font-weight: 600;">Select Date Filter</label>
						   </div>
						   <div class="col-sm-4 col-xs-4">
						      <div class="datefilters">
							 <select  name="data_graph_value" id="data_graph_value"  class="select_datefilters" onchange="data_logs()">
							    <option value="today">Today</option>
							    <option value="yesterday">Yesterday</option>
							    <option value="last_week">Last week</option>
							    <option value="last_month"> Last month</option>
			
						      <option value="last_year">Last year</option>
			
						   </select>
			
						      </div>
			
						   </div>
						   </div>
                                                 </div>
						</div>
                                            </div>
					  </div>
					  <div class="bar_chart" id="DatachartContainer" style="height: 300px; width: 80%">
					    <img src="" class="img-responsive"/>
					  </div>
				       </div>
				       
				       
                                      
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <hr/>
                                          <div class="row">
					     <div class="col-sm-2 col-xs-2">
						<h2>Account Activity</h2>
					     </div>
					     <div class="col-sm-10 col-xs-10">
						<div class="col-md-4 col-sm-4 col-xs-4 demo">
						   <i class="glyphicon glyphicon-calendar fa fa-calendar datefinder_icons"></i>
						   <input name="activitylogfilter" class="datefinder" type="text" placeholder="DD-MM-YYYY" onblur="this.placeholder='DD-MM-YYYY'" onfocus="this.placeholder=''" >
						</div>
					     </div>
					  </div>
					  <div id="useractivitylogs"></div>
					  <span class="hide curr_pgno">1</span>
					  <div id="pgnavli"></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
			      <?php } ?>
			      <?php if($syshideperm){ ?>
			      <div class="mui-tabs__pane" id="sysogs">
				  <?php
				  $datauser['uid']=$subscriber_uid;
				  $this->view('users/Syslog',$datauser); ?>
			      </div>
			      <?php } ?>
			      <?php if($invtryhideperm){ ?>
			      <div class="mui-tabs__pane" id="inventory">
			      <?php
				 $datauser['uid']=$subscriber_uid;
				 $this->view('users/Inventory',$datauser); ?>
			      </div>
			      <?php } ?>
			      <?php if($notifyhideperm){ ?>
			      <div class="mui-tabs__pane" id="notifications">
				 <div class="col-sm-12 col-xs-12">
				    <div class="row">
				       <div class="col-sm-3 col-xs-3 nopadding-left" style="width:18%">
					  <h2>NOTIFICATIONS</h2>
				       </div>
				       <div class="col-sm-9 col-xs-9 nopadding-left gateway_message" style="color:#f00"></div>
				    </div>
				 </div>
				 <div class="form-group" style="margin-bottom:25px">
				    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-3 col-xs-3">
					     <h4>SMS NOTIFICATIONS</h4>
					  </div>
					  <div class="col-sm-3 col-xs-3" style="margin-top:5px">
					     <label class="radio-inline">
					     <input type="radio" name="sms_notify" value="1"> ON
					     </label>
					     <label class="radio-inline">
					     <input type="radio" name="sms_notify" value="0" checked> OFF
					     </label>
					  </div>
				       </div>
				       <div class="row">
					  <div class="col-sm-3 col-xs-3">
					     <h4>EMAIL NOTIFICATIONS</h4>
					  </div>
					  <div class="col-sm-3 col-xs-3" style="margin-top:5px">
					     <label class="radio-inline">
					     <input type="radio" name="email_notify" value="1"> ON
					     </label>
					     <label class="radio-inline">
					     <input type="radio" name="email_notify" value="0" checked> OFF
					     </label>
					  </div>
					  <div class="col-sm-3 col-xs-3">
					     <button type="submit" class="mui-btn mui-btn--small mui-btn--accent mui-btn--raised" onclick="savenotification_alerts()">SAVE</button>
					  </div>
				       </div>
				    </div>
				 </div>
				 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px">
				    <div class="row" >
				       <div class="form-group">
					  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					     <div class="row">
						<div class="col-sm-2 col-xs-2">
						   <h4 style="margin-top:25px">SEND SMS</h4>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						   <div class="mui-textfield mui-textfield--float-label">
						      <input type="text" name="textsms_tosend" id="textsms_tosend" value="" >
						      <label>ENTER SMS TEXT HERE</label>
						   </div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						   <button type="submit" class="mui-btn mui-btn--small mui-btn--accent mui-btn--raised" style="margin-top:25px" onclick="send_txtsms_touser()">SEND NOW</button>
						</div>
					     </div>
					  </div>
				       </div>
				    </div>
				 </div>
				 <div class="col-sm-12 col-xs-12">
				    <div class="table-responsive" style="height:250px; overflow-x:auto;">
				       <table class="table table-striped">
					  <thead>
					     <tr class="active">
						<th class="col-sm-2 col-xs-2">SENT DATETIME</th>
						<th class="col-sm-2 col-xs-2">MESSAGE TYPE</th>
						<th class="col-sm-2 col-xs-2">SEND TO</th>
						<th class="col-sm-6 col-xs-6">MESSAGE CONTENT </th>
					     </tr>
					  </thead>
					  <tbody id="notificationlisting">
					     
					  </tbody>
				       </table>
				    </div>
				 </div>
			      </div>
			      <?php } ?>
			      <?php if($diaghideperm){ ?>
			      <div class="mui-tabs__pane" id="diagnostic">
				 <div class="col-sm-12 col-xs-12">
				    <div class="row">
				       <div class="col-sm-3 col-xs-3 nopadding-left" style="width:18%">
					  <h2>DIAGNOSTIC &nbsp; <span class="user_onlineoffline_status"></span></h2>
				       </div>
				       <div class="col-sm-9 col-xs-9 nopadding-left gateway_message" style="color:#f00"></div>
				    </div>
				 </div>
				 <div class="form-group" style="margin-bottom:25px">
				    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				       <div class="row">
					  <ul class="list-inline">
					     <li>
						<button class="mui-btn mui-btn--small mui-btn--accent diag_readable" style="height:36px; line-height:35px; font-weight:600" onclick="radius_diagnostic()">RADIUS LOGS</button>
					     </li>
					     <li>
						<button class="mui-btn mui-btn--small mui-btn--accent diag_readable" style="height:36px; line-height:35px; font-weight:600" onclick="mikrotik_logs_modal_open()">MIKROTIK LOGS</button>
					     </li>
					     <li>
						<button class="mui-btn mui-btn--small mui-btn--accent diag_readable" style="height:36px; line-height:35px; font-weight:600" onclick="router_authlogs('<?php echo $subscriber_uid ?>')">AUTHENTICATION LOGS</button>
					     </li>
					     <li>
						<button class="mui-btn mui-btn--small mui-btn--accent diag_readable" style="height:36px; line-height:35px; font-weight:600" onclick="check_configuration('<?php echo $subscriber_uid ?>')">PLAN-NAS CONFIGURATION</button>
					     </li>
					     <li>
						<button class="mui-btn mui-btn--small mui-btn--accent diag_readable" style="height:36px; line-height:35px; font-weight:600" onclick="routerlogout('<?php echo $subscriber_uid ?>')">ROUTER LOGOUT</button>
					     </li>
					     <li>
						<button class="mui-btn mui-btn--small mui-btn--accent diag_readable" style="height:36px; line-height:35px; font-weight:600" data-toggle="modal" data-target="#resetMacModal">RESET MAC</button>
					     </li>
						  <li>
						<button class="mui-btn mui-btn--small mui-btn--accent diag_readable data_refresh" data-uid="<?php echo $subscriber_uid ?>" style="height:36px; line-height:35px; font-weight:600" >Refresh Data</button>
					     </li>
					  </ul>
				       </div>
				    </div>
				 </div>
				 <div class="col-sm-12 col-xs-12" id="radius_diag_data_range" style="display:none; margin-bottom: 10px;">
				    <div class="col-md-4 col-sm-4 col-xs-4 demo">
				       <i class="glyphicon glyphicon-calendar fa fa-calendar datefinder_icons"></i>
				       <input name="radius_datefilter" class="datefinder" type="text" placeholder="DD-MM-YYYY" onblur="this.placeholder='DD-MM-YYYY'" onfocus="this.placeholder=''" value="<?php echo $this->user_model->usage_graph_billing_date($subscriber_uid)?>" />
				    </div>
				    <div class="col-md-3 col-sm-3 col-xs-3 demo">
				       <h3 style="margin:0px">Total Download: <span id="total_download_logs"></h3>
				    </div>
				    <div class="col-md-3 col-sm-3 col-xs-3 demo">
				       <h3 style="margin:0px">Total Upload: <span id="total_upload_logs"></h3>
				    </div>
				    <div class="col-md-2 col-sm-2 col-xs-2 demo">
				       <button class="mui-btn mui-btn--small mui-btn--accent pull-right" style="background-color:#1c2a39;" onclick="radiuslogs_rptexport()">Export to Excel</button>
				    </div>
				 </div>
				 <div class="col-sm-12 col-xs-12" id="radius_diag_data"></div>
				 <div class="col-sm-12 col-xs-12" id="authlogs_data"></div>
				 <!-- mikrotik log modal start -->
				 <div class="modal fade" id="mikrotik_log_modal" role="dialog" data-backdrop="static" data-keyboard="false">	    
				    <div class="modal-dialog modal-sm">
				       <div class="modal-content">
					  <div class="modal-header">
					     <button type="button" class="close" data-dismiss="modal">&times;</button>
					     <h4 class="modal-title"><strong>Enter Router Detail</strong></h4>
					  </div>
					  <div class="modal-body" style="padding-bottom:5px">
					     <div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						   <div class="mui-select">
						      <!--<input type="text" name="microtik_router_ip" required="required" maxlength="20">-->
						      <select name="microtik_router_ip" required="required">
							 <option value="">Select Router IP</option>
							 <?php echo $this->user_model->nasips_listing(); ?>
						      </select>
						      <label>Router IP<sup>*</sup></label>
						   </div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						   <div class="mui-textfield">
						      <input type="text" name="microtik_router_username" required="required" maxlength="20">
						      <label>Router Username<sup>*</sup></label>
						   </div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						   <div class="mui-textfield">
						      <input type="text" name="microtik_router_password" required="required" maxlength="20">
						      <label>Router Password<sup>*</sup></label>
						   </div>
						</div>
						<center><p id="mikrotic_router_error" style="color: red"></p></center>
					     </div>
					  </div>
					  <div class="modal-footer" style="text-align: right">
					     <button type="button" class="receiptbtn mui-btn mui-btn--small mui-btn--accent" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
					     <input type="button" class="receiptbtn mui-btn mui-btn--small mui-btn--accent" value="OK" onclick="mikrotik_log_detail()" />
					  </div>
				       </div>
				    </div>
				 </div>
				 <!-- mikrotik log modal end -->
			      </div>
			      <?php } ?>   
			   </div>
                        </div>
                     </div>
                  </div>
               </div>
	       <?php } ?>
	    </div>
         </div>
      </div>

      <!--Modal Begins-->
      <div class="modal fade" tabindex="-1" role="dialog" id="installationModal" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="installation_billing_form" autocomplete="off" onsubmit="installation_charges(); return false;">
	    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <input type="hidden" name="connection_type" class="connection_type" value="" />
            <input type="hidden" name="install_bill_transaction_type" value="installation" />
	    <input type="hidden" name="installation_billid" value="" />
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <h4 class="modal-title">
			<strong>INSTALLATION BILLING</strong>
		     </h4>
		  </div>
		  
		  <div class="modal-body">
		     <div class="row" style="margin-bottom:10px">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <input type="radio" name="installcharges_applicable" value="waivedoff" required /> Waived Off &nbsp; &nbsp;
			   <input type="radio" name="installcharges_applicable" value="applycharges" required /> Chargeable
			</div>
		     </div>
		     <div class="visible_oncharge">
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield" id="install_billdtme">
				 <label>Bill DateTime<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input type="text" name="install_bill_number" id="install_bill_number" readonly>
				 <label>Bill Number<sup>*</sup></label>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-select">
				 <select name="install_bill_payment_mode" required>
				    <option value="">Select Payment Mode</option>
				    <option value="cash">Cash</option>
				    <option value="cheque">Cheque</option>
				    <option value="dd">Demand Draft</option>
				 </select>
				 <label>Mode of Payment<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield" style="padding-top: 15px">
				 <input type="text" name="install_chqddrecpt">
				 <label>Cheque/DD/Receipt Number</label>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield" style="padding-top: 25px">
				 <input type="text" name="install_bill_amount" data-billtype="installation" class="bill_amount" id="install_bill_amount" required>
				 <label>Amount Received<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="">
				 <label style="display:block;">Discount (If Any)</label>
				 <input type="radio" name="install_discounttype" value="percent" checked/> Percent Discount &nbsp;&nbsp;
				 <input type="radio" name="install_discounttype" value="flat" /> Flat Discount
				 <select name="install_bill_percentdiscount" class="form-control" style="font-size:12px">
				    <option value="0" selected>Select Discount %</option>
				    <?php
				    for($d=1; $d<=100; $d++){
				       echo '<option value="'.$d.'">'.$d.'%</option>';
				    }
				    ?>
				 </select>
				 <input type="number" name="install_bill_flatdiscount" min="0" value="0" class="form-control hide"  style="font-size:12px"/>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-radio">
				 <label class="nopadding">Include Tax </label> <br/>
				 <input type="radio" name="install_bill_withtax" value="1" checked> Yes &nbsp;&nbsp;
				 <input type="radio" name="install_bill_withtax" value="0"> No
			      </div>
			   </div>
			   <input type="hidden" name="install_send_copy_to_customer" value="0" >
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-radio">
				 <label class="nopadding">Payment Received </label> <br/>
				 <input type="radio" name="install_bill_payment_received" value="1"> Yes &nbsp;&nbsp;
				 <input type="radio" name="install_bill_payment_received" value="0" checked> No
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input type="text" name="install_bill_total_amount" class="bill_total_amount" readonly="readonly" required >
				 <label>Total Amount</label>
			      </div>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <textarea name="install_bill_comments"></textarea>
			      <label>Comments</label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <textarea name="install_bill_remarks"></textarea>
			      <label>Remarks</label>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="formsubmit mui-btn mui-btn--large mui-btn--accent" value="Save" />
                     <img class="formloader hide" src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
		  </div>
	       </div>
	       <!-- /.modal-content -->
	    </div>
	 </form>
      </div>
      
      <div class="modal fade" tabindex="-1" role="dialog" id="securityModal" data-backdrop="static" data-keyboard="false">
         <form action="" method="post" id="security_billing_form" autocomplete="off" onsubmit="security_charges(); return false;">
	    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <input type="hidden" name="connection_type" class="connection_type" value="" />
            <input type="hidden" name="security_bill_transaction_type" value="security" />
	    <input type="hidden" name="security_billid" value="" />
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <h4 class="modal-title">
			<strong>SECURITY BILLING</strong>
		     </h4>
		  </div>
		  <div class="modal-body">
		     <div class="row" style="margin-bottom:10px">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <input type="radio" name="securitycharges_applicable" value="waivedoff" required /> Waived Off &nbsp; &nbsp;
			   <input type="radio" name="securitycharges_applicable" value="applycharges" required /> Chargeable
			</div>
		     </div>
		     <div class="security_visible_oncharge">
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield" id="security_billdtme">
				 <label>Bill DateTime<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input type="text" name="security_receipt_number" id="security_receipt_number">
				 <label>Cheque/DD/Receipt Number<sup></sup></label>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-select">
				 <select name="security_bill_transaction_type" required>
				    <option value="">Select Transaction Type</option>
				    <option value="security" selected>Security Cost</option>
				 </select>
				 <label>Transaction Type<sup>*</sup></label>
			      </div>
			   </div>-->
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-select">
				 <select name="security_bill_payment_mode" required>
				    <option value="">Select Payment Mode</option>
				    <option value="cash">Cash</option>
				    <option value="cheque">Cheque</option>
				    <option value="dd">Demand Draft</option>
				 </select>
				 <label>Mode of Payment<sup>*</sup></label>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield" style="padding-top: 25px">
				 <input type="text" name="security_bill_amount" data-billtype="security" class="bill_amount" id="security_bill_amount" required>
				 <label>Amount Received<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="">
				 <label style="display:block;">Discount (If Any)</label>
				 <input type="radio" name="security_discounttype" value="percent" checked /> Percent Discount &nbsp;&nbsp;
				 <input type="radio" name="security_discounttype" value="flat" /> Flat Discount
				 <select name="security_bill_percentdiscount" class="form-control" style="font-size:12px">
				    <option value="0" selected>Select Discount %</option>
				    <?php
				    for($d=1; $d<=100; $d++){
				       echo '<option value="'.$d.'">'.$d.'%</option>';
				    }
				    ?>
				 </select>
				 <input type="number" name="security_bill_flatdiscount" min="0" value="0" class="form-control hide"  style="font-size:12px"/>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-radio">
				 <label class="nopadding">Include Tax </label> <br/>
				 <input type="radio" name="security_bill_withtax" value="1" checked> Yes &nbsp;&nbsp;
				 <input type="radio" name="security_bill_withtax" value="0"> No
			      </div>
			   </div>
			   <input type="hidden" name="security_send_copy_to_customer" value="0">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-radio">
				 <label class="nopadding">Payment Received </label> <br/>
				 <input type="radio" name="security_bill_payment_received" value="1"> Yes &nbsp;&nbsp;
				 <input type="radio" name="security_bill_payment_received" value="0" checked> No
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input type="text" name="security_bill_total_amount" class="bill_total_amount" readonly="readonly" required>
				 <label>Total Amount</label>
			      </div>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <textarea name="security_bill_comments"></textarea>
			      <label>Comments</label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <textarea name="security_bill_remarks"></textarea>
			      <label>Remarks</label>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="formsubmit mui-btn mui-btn--large mui-btn--accent" value="Save" />
                     <img class="formloader hide" src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
		  </div>
	       </div>
	       <!-- /.modal-content -->
	    </div>
	 </form>
      </div>
      
      <div class="modal fade" tabindex="-1" role="dialog" id="advance_prepayModal" data-backdrop="static" data-keyboard="false" style="z-index:9999999">
         <form action="" method="post" id="advancedpay_billing_form" autocomplete="off" onsubmit="advance_prepaid(); return false;">
	    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
            <input type="hidden" name="advprepay_bill_transaction_type" value="advprepay" />
	    <input type="hidden" name="connection_type" class="connection_type" value="" />
            <input type="hidden" name="plan_monthly_cost" id="plan_monthly_cost" value="" />
	    <input type="hidden" name="userassoc_planid" id="userassoc_planid" value="" />
	    <input type="hidden" name="prevadvpayid" id="prevadvpayid" value="" />
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <h4 class="modal-title">
			<strong>ADVANCED PREPAY BILLING</strong>
		     </h4>
		  </div>
		  <div class="modal-body">
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield" id="advprepay_billdtme">
			      <label>Bill DateTime<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="advprepay_bill_number" id="advprepay_bill_number" readonly>
			      <label>Bill Number<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="advprepay_bill_payment_mode" required>
				 <option value="">Select Payment Mode</option>
				 <option value="cash">Cash</option>
				 <option value="cheque">Cheque</option>
				 <option value="dd">Demand Draft</option>
				 <option value="viapaytm">Paytm</option>
				 <option value="netbanking">NetBanking</option>
			      </select>
			      <label>Mode of Payment<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield" style="padding-top: 15px">
			      <input type="text" name="advprepay_chqddrecpt" required>
			      <label>Cheque/DD/Paytm No./TranscID/Rcpt No.<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="">
                              <label>Select Number Of Month</label>
			      <select name="advprepay_month_count" class="form-control advprepay_month_count" required>
				 <option value="">Select Number Of Month</option>
				 <?php
				 for($d=1; $d<=12; $d++){
				    echo '<option value="'.$d.'">'.$d.'</option>';
				 }
				 ?>
			      </select>
			      
			   </div>
			</div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="advprepay_freemonth_count">
				 <option value="0" selected>Select Number Of Free Month</option>
				 <?php
				 for($d=1; $d<=12; $d++){
				    echo '<option value="'.$d.'">'.$d.'</option>';
				 }
				 ?>
			      </select>
			      <label>Select Number Of Free Month</label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="advprepay_bill_amount" readonly="readonly" required>
			      <label>Calculated Amount<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="">
			      <label style="display:block;">Discount (If Any)</label>
			      <input type="radio" name="advprepay_discounttype" value="percent" checked /> Percent Discount &nbsp;&nbsp;
			      <input type="radio" name="advprepay_discounttype" value="flat" /> Flat Discount
			      <select name="advprepay_bill_percentdiscount" class="form-control" style="font-size:12px">
				 <option value="0" selected>Select Discount %</option>
				 <?php
				 for($d=1; $d<=100; $d++){
				    echo '<option value="'.$d.'">'.$d.'%</option>';
				 }
				 ?>
			      </select>
			      <input type="number" name="advprepay_bill_flatdiscount" min="0" value="0" class="form-control hide"  style="font-size:12px"/>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="advprepay_bill_total_amount" readonly="readonly" required>
			      <label>Total Amount</label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-radio">
			      <label class="nopadding">Send Copy To Customer</label> <br/>
			      <input type="radio" name="advprepay_send_copy_to_customer" value="1" checked> Yes &nbsp;&nbsp;
			      <input type="radio" name="advprepay_send_copy_to_customer" value="0"> No
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <span class="adverr hide" style="color:#FF4081">You can't choose Free Months & Discount both at the same time.</span> <br/>
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="SAVE" />
		  </div>
	       </div>
	       <!-- /.modal-content -->
	    </div>
	 </form>
      </div>
      
      <div class="modal fade" id="delete_advpaymentrcptModal" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:9999999">
         <div class="modal-dialog modal-sm">
            <form action="" id="deladvpayform" method="post" autocomplete="off" onsubmit="delete_advprepaid_rcpt(); return false;">
	       <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	       <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	       <input type="hidden" name="advprepayid" value=''>
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title">
			<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
		     </h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <p>How do you want to manage this receipt amount ?</p>
		     <div class="row" style="margin-bottom:10px">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <input type="radio" name="advprepay_addtowallet" value="1" required /> Delete & Add to Wallet
			   <br/>
			   <input type="radio" name="advprepay_addtowallet" value="0" required /> Delete Only
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--large mui-btn--accent del_advpaybtn" value="Submit" />
		  </div>
	       </div>
	    </form>
         </div>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="addtowalletModal" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="addtowallet_billing_form" autocomplete="off" onsubmit="addtowallet(); return false;">
	    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <input type="hidden" name="addtowallet_bill_transaction_type" value="addtowallet" />
	    <input type="hidden" name="walletid_foredit"  value="" />
	    <input type="hidden" name="walletbillid_foredit" value="" />
	    
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <h4 class="modal-title">
			<strong>ADD TO WALLET</strong>
		     </h4>
		  </div>
		  
		  <div class="modal-body">
		     <div class="row">
			<!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield" id="addtowallet_billdtme">
			      <label>Bill DateTime<sup>*</sup></label>
			   </div>
			</div>-->
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
			      <div class="input-group" style="margin-top:-5px">
				 <div class="input-group-addon" style="padding:6px 10px">
				    <i class="fa fa-calendar"></i>
				 </div>
				 <input type="text" class="form-control walletamt_receivedate" id="" placeholder="Amount Received On*" name="addtowallet_bill_datetimeformat" required>
				 <input name="addtowallet_bill_datetime" type="hidden" value="">
			      </div>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="addtowallet_receipt_number" id="addtowallet_bill_number" required>
			      <label>Cheque/DD/Receipt Number<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="addtowallet_payment_mode" required>
				 <option value="">Select Payment Mode</option>
				 <option value="cash">Cash</option>
				 <option value="cheque">Cheque</option>
				 <option value="dd">Demand Draft</option>
				 <option value="viapaytm">Paytm</option>
				 <option value="netbanking">NetBanking</option>
			      </select>
			      <label>Mode of Payment<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="addtowallet_amount" required>
			      <label>Amount Received<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 chqddpaytm hide">
			   <div class="mui-textfield">
			      <input type="text" name="update_cheque_dd_paytm" required="required" pattern="^[0-9]\d*$">
			      <label>Cheque/DD/Paytm No./Transaction ID<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="bill_bankdetails hide">
			<div class="row">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding-left">
				 <div class="mui-textfield">
				    <input type="tel" name="bill_bankaccount_number" maxlength="20">
				    <label>Account Number</label>
				 </div>
			      </div>
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding-left">
				 <div class="mui-textfield">
				    <!--<input type="tel" class="req_cheque_dd" name="bill_bankissued_date">-->
				    <div class="input-group" style="margin-top:-5px">
				       <div class="input-group-addon" style="padding:6px 10px">
					  <i class="fa fa-calendar"></i>
				       </div>
				       <input type="text" class="form-control receiptdate" id="" placeholder="Issue Date" name="bill_bankissued_date" value="">
				    </div>
				    <label>Issued Date</label>
				 </div>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding-left">
				 <div class="mui-textfield">
				    <input type="text" name="bill_bankname">
				    <label>Bank Name</label>
				 </div>
			      </div>
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				 <div class="mui-textfield">
				    <input type="text" name="bill_bankaddress" class="nopadding-left">
				    <label>Bank Address</label>
				 </div>
			      </div>
			   </div>
			</div>
			<!--<div class="row">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				 <div class="mui-textfield">
				    <input type="file" name="bill_slipimage">
				    <label>Slip Image</label>
				 </div>
			      </div>
			   </div>
			</div>-->
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-radio">
			      <label class="nopadding">Amout Received</label> <br/>
			      <input type="radio" name="wallet_amount_received" value="1" required> Confirmed &nbsp;&nbsp;
			      <input type="radio" name="wallet_amount_received" value="0" required> Pending
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-radio">
			      <label class="nopadding">Send Copy To Customer</label> <br/>
			      <input type="radio" name="addtowallet_send_copy_to_customer" value="1" checked> Yes &nbsp;&nbsp;
			      <input type="radio" name="addtowallet_send_copy_to_customer" value="0"> No
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="Submit" />
		  </div>
	       </div>
	       <!-- /.modal-content -->
	    </div>
	 </form>
      </div>
      
      <div class="modal fade" id="user_activatedModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><strong>ACTIVATE USER?</strong></h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
		  <input type="hidden" id="otp_to_activate_user" value="" />
		  <input type="hidden" id="phone_to_activate_user" value="" />
                  <p>To Activate <span id="activate_username"></span>, please enter the OTP sent to your regestiered mobile no. <span id="activate_userphone"></span>
                  </p>
                  <div class="mui-textfield mui-textfield--float-label" style="margin-bottom:0px">
                     <input type="text" name="enter_otp_to_activate_user" id="enter_otp_to_activate_user">
                     <label>Enter 4-Digit Mobile No.</label>
                  </div>
		  <label id="activate_otp_err"></label>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                  <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="active_user_confirmation()">ACTIVATE</button>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade" id="user_inactiveAlert" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm" style="width:500px">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>USER CANNOT BE MADE ACTIVE</strong>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>Please check these details to make User Active</p>
		  <ul style="margin:15px;list-style-type: none">
		     <li><span id="pedactive"></span> &nbsp; Complete Personal Details</li>
		     <li><span id="kycactive"></span> &nbsp; Upload atleast one KYC document</li>
		     <li><span id="planactive"></span> &nbsp; Plan is associated with the user</li>
		     <li><span id="inscactive"></span> &nbsp; Installation & Security Charges paid by the User</li>
		  </ul>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade" id="enterReceiptModal" role="dialog" data-backdrop="static" data-keyboard="false">
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title"><strong>Bill Receipt</strong> (Pending Amount: <span id="billpendingcost"></span>)</h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <div class="row">
			<div class="col-sm-12 col-xs-12">
			   <ul class="mui-tabs__bar">
			      <li class="mui--is-active hideifbillpaid">
				 <a data-mui-toggle="tab" data-mui-controls="add_receipt_pane">Add Receipt</a>
			      </li>
			      <li>
				 <a data-mui-toggle="tab" data-mui-controls="past_receipt_pane" onclick="getbillpastreceipts()">Past Receipts</a>
			      </li>
			   </ul>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="mui-tabs__pane mui--is-active hideifbillpaid" id="add_receipt_pane">
			      <div class="mui--appbar-height"></div>
			      <form action="" method="post" id="receiptupdate_form" autocomplete="off" onsubmit="update_receiptform(); return false;">
				 <input type="hidden" name="billid_toupdate" id="billid_toupdate" />
				 <input type="hidden" name="billtype_toupdate" id="billtype_toupdate" />
				 <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
				 <input type="hidden" id="billpendingamount" value=""/>
				 <div class="row">
				    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px;">
					  <div class="mui-textfield">
					     <input type="tel" name="update_receipt_number" required="required" maxlength="20">
					     <label>Receipt Number<sup>*</sup></label>
					  </div>
				       </div>
				       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px;">
					  <div class="mui-textfield">
					     <input type="tel" id="update_receipt_amount" name="update_receipt_amount" required="required" pattern="^[1-9]\d*$">
					     <label>Receipt Amount<sup>*</sup></label>
					     <span id="receiptamount_errtxt"></span>
					     <input type="hidden" id="receiptamount_err" value="0" />
					  </div>
				       </div>
				    </div>
				 </div>
				 <div class="row">
				    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px;">
					  <div class="mui-select">
					     <select name="update_payment_mode" required>
						<option value="">Select Payment Mode</option>
						<option value="cash">Cash</option>
						<option value="cheque">Cheque</option>
						<option value="dd">Demand Draft</option>
						<option value="viapaytm">via Paytm</option>
						<option value="viawallet">via Wallet</option>
						<option value="netbanking">via NetBanking</option>
					     </select>
					     <label>Mode of Payment<sup>*</sup></label>
					  </div>
				       </div>
				       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 chqddpaytm hide">
					  <div class="mui-textfield">
					     <input type="text" name="update_cheque_dd_paytm" required="required" pattern="^[0-9]\d*$">
					     <label>Cheque/DD/Paytm Number<sup>*</sup></label>
					  </div>
				       </div>
				    </div>
				    
				    <div class="bill_bankdetails hide">
				       <div class="row">
					  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="mui-textfield">
						   <input type="tel" name="bill_bankaccount_number" maxlength="20">
						   <label>Account Number</label>
						</div>
					     </div>
					     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="mui-textfield">
						   <!--<input type="tel" class="req_cheque_dd" name="bill_bankissued_date">-->
						   <div class="input-group" style="margin-top:-5px">
						      <div class="input-group-addon" style="padding:6px 10px">
							 <i class="fa fa-calendar"></i>
						      </div>
						      <input type="text" class="form-control receiptdate" id="" placeholder="Issue Date" name="bill_bankissued_date" value="">
						   </div>
						   <label>Issued Date</label>
						</div>
					     </div>
					  </div>
				       </div>
				       <div class="row">
					  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="mui-textfield">
						   <input type="text" name="bill_bankname">
						   <label>Bank Name</label>
						</div>
					     </div>
					     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="mui-textfield">
						   <input type="text" name="bill_bankaddress">
						   <label>Bank Address</label>
						</div>
					     </div>
					  </div>
				       </div>
				       <div class="row">
					  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="mui-textfield">
						   <input type="file" name="bill_slipimage">
						   <label>Slip Image</label>
						</div>
					     </div>
					  </div>
				       </div>
				    </div>

				    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:5px">
				       <button type="button" class="receiptbtn mui-btn mui-btn--small mui-btn--accent" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
				       <input type="submit" class="receiptbtn mui-btn mui-btn--small mui-btn--accent" value="Save" />
				    </div>
				    
				 </div>
			      </form>
			   </div>
			   <div class="mui-tabs__pane" id="past_receipt_pane">
			      <div class="row">
				 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="pastbill_listingdiv">
				    
				 </div>
			      </div>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer" style="text-align: right">&nbsp;</div>
	       </div>
	    </div>
      </div>
      <div class="modal fade" id="payment_receipt" role="dialog">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header" style="padding-bottom: 0px;">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><strong>EMAIL PAYMENT RECEIPT</strong></h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <p>Email Payment Receipt for Bill dated dd.mm.yy, paid on dd.mm.yy to</p>
                     </div>
                     <form>
                        <div class="row">
                           <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                              <label class="radio-inline">
                              <input type="radio" name="optradio">
                              </label>
                           </div>
                           <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                              <strong>User�s registered email ID</strong>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="padding-top:20px;">
                              <label class="radio-inline">
                              <input type="radio" name="optradio">
                              </label>
                           </div>
                           <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                              <div class="mui-textfield mui-textfield--float-label">
                                 <input type="text">
                                 <label>Enter other email ID</label>
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="modal-footer" style="text-align:right">
                  <button type="button" class="mui-btn mui-btn--small mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                  <button type="button" class="mui-btn mui-btn--small mui-btn--accent">SEND</button>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="documentModal" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">
                     <strong>DOCUMENTS PREVIEW</strong>
                  </h4>
               </div>
               <div class="modal-body">
                  <div class="row" id="docpreview"></div>
               </div>
               <div class="modal-footer">&nbsp;</div>
            </div>
         </div>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="editserviceRequestModal" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog" style="margin-top:4%">
            <form action="" method="post" id="edit_ticket_request_form" autocomplete="off" onsubmit="edit_ticket_request(); return false;">
	       <input type="hidden" name="edcustomer_id" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	       <input type="hidden" name="edtktid" value="" />
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h4 class="modal-title">
                        <strong>EDIT SERVICE REQUEST</strong>
                     </h4>
                  </div>
                  <div class="modal-body">
                     <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-textfield">
                              <input type="text" name="edticketid" value="" readonly  required>
                              <label>Ticket ID <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-textfield" id="edtktdtme">
			      <input name="edticket_datetime" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" readonly required/>
                              <label>Ticket DateTime<sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-textfield">
                              <input type="text" name="edcustomer_uuid" value="" readonly required>
                              <label>Customer ID <sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="edticket_type" required disabled></select>
                              <label>Ticket Types <sup>*</sup></label>
                           </div>
                        </div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield">
                              <textarea style="resize: vertical;" rows="2" name="edticket_description" required readonly></textarea>
                              <label>Ticket Description<sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="edticket_assignto" required disabled>
                                 <option value="">Assign Ticket</option>
                              </select>
                              <label>Assign Ticket<sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="edticket_priority" required>
                                 <option value="">Select Ticket Priority</option>
                                 <option value="low">Low</option>
                                 <option value="medium">Medium</option>
                                 <option value="high">High</option>
                              </select>
                              <label>Priority<sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label">
                              <input type="text" name="edticket_comment">
                              <label>Comments (If any)</label>
                           </div>
                        </div>
                     </div>
		     <div class="row">
			<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			   <div class='mui-textfield mui-textfield--float-label' id="edit_tcomment_list">
			   </div>
			</div>
		     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="mui-btn mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                     <input type="submit" class="mui-btn mui-btn--primary mui-btn--flat" style="background-color:#f00f64; color:#fff" value="SAVE" >
                  </div>
               </div>
            </form>
         </div>
      </div>
      <div class="modal fade" id="assignTicketModal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="assignTicketMember_form" autocomplete="off" onsubmit="updateTicketMember(); return false;">
	    <input type="hidden" name="tktid_toupdate" id="tktid_toupdate" />
            <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title"><strong>ASSIGN TICKET MEMBER</strong></h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="mui-select">
			      <select name="team_member" required>
				 <option value="">Select Team Member</option>
			      </select>
			      <label>Assign Ticket<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="mui-textfield">
			      <textarea style="resize: vertical;" rows="1" name="chngtktstatus_description" required></textarea>
			      <label>Comment<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn mui-btn--small mui-btn--accent" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--small mui-btn--accent" value="Save" />
		  </div>
	       </div>
	    </div>
	 </form>
      </div>
      <div class="modal fade" id="changeticket_statusconfirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="changeticket_statusform" autocomplete="off" onsubmit="changeticket_statusconfirmation(); return false;">
	    <input type="hidden" name="chngtktstatus" value="" />
	    <input type="hidden" name="chngtktstatusid" value="" />
	    <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title">
			<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
		     </h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <p>Are you sure you want to change the ticket status ?</p><br/>
		     <div class="mui-textfield">
			<textarea style="resize: vertical;" rows="2" name="chngtktstatus_description" required></textarea>
			<label>Comment<sup>*</sup></label>
		     </div>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal" onclick="fetch_allopenticket()">CANCEL</button>
		  <input type="submit" class="mui-btn mui-btn--small mui-btn--accent" value="YES" />
		  </div>
	       </div>
	    </div>
	 </form>
      </div>
      <div class="modal fade" id="ticket_closedModal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <input type="hidden" name="tktcloseid" value="">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
		     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CLOSE TICKET ALERT</strong>
		  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>To close the ticket of <strong class="customer_name"></strong>, please enter the OTP sent to regestiered mobile no. <strong class="customer_phone"></strong>
                  </p>
                  <div class="mui-textfield mui-textfield--float-label" style="margin-bottom:0px">
                     <input type="text" name="tktclose_otp" maxlength="4" minlength="4" required>
                     <label>Enter 4-Digit Mobile No.</label>
                  </div>
		  <label id="tktclose_otp_err" style="color:#f00; font-size:12px;"></label>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="verify_confirm_tktclose()">CLOSE TICKET</button>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade" id="ticketsummary_modal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog">
	     <div class="modal-content">
		 <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title">
			 <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>Ticket Summary</strong>
		     </h4>
		 </div>
		 <div class="modal-body" style="padding-bottom:5px">
		     <ul id="tktsummarylist"></ul>
		 </div>
		 <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
		 </div>
	     </div>
	 </div>
     </div> 
      <div class="modal fade" tabindex="-1" role="dialog" id="edit_documentModal" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">
                     <strong>DOCUMENTS PREVIEW</strong>
                  </h4>
               </div>
               <div class="modal-body">
                  <div class="row" id="edit_docpreview"></div>
               </div>
               <div class="modal-footer">&nbsp;</div>
            </div>
         </div>
      </div>

      
      <div class="modal fade" tabindex="-1" role="dialog" id="nextcycle_planModal" data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <h4 class="modal-title">
		     <strong>NEXT CYCLE PLAN</strong>
		  </h4>
	       </div>
	       <div class="modal-body">
		  <?php $this->view('users/NextPlan'); ?>
	       </div>
	       <div class="modal-footer">
		  &nbsp;
	       </div>
	    </div>
	    <!-- /.modal-content -->
	 </div>
      </div>
      

      
      <div class="modal fade" id="exceedWalletLimitModal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="exceed_walletLimit_form" autocomplete="off" onsubmit="exceed_walletlimit(); return false;">
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title"><strong>Exceed Wallet Limit</strong></h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="mui-radio">
			      <label class="nopadding">Change WalletLimit Via<sup>*</sup> </label> &nbsp;
			      <input type="radio" name="change_walletlimit_option" required="required" value="bycash">By Amount &nbsp;
			      <input type="radio" name="change_walletlimit_option" required="required" value="bydays">By Days
			   </div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="mui-textfield bycashopt hide">
			      <input type="number" id="update_walletLimit" name="update_walletLimit_bycash" required="required" min="0" >
			      <label>Wallet Limit Amount<sup>*</sup></label>
			   </div>
			   <div class="mui-select bydaysopt hide">
			      <select name="update_walletLimit_bydays" required>
				 <option value="">Select Days</option>
				 <?php
				 for($wd=1;$wd<=31;$wd++){
				    echo '<option value="'.$wd.'">'.$wd.'</option>';
				 }
				 ?>
			      </select>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn mui-btn--small mui-btn--accent" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--small mui-btn--accent" value="Save" />
		  </div>
	       </div>
	    </div>
	 </form>
      </div>

      
      <div class="modal fade" id="add_zoneModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" onclick="resetzonevalue()">&times;</button>
                     <h4 class="modal-title"><strong>ADD ZONE</strong></h4>
                 </div>
                 <form action="" method="post" id="addzone_fly" autocomplete="off" onsubmit="add_newzone(); return false;">
                     <div class="modal-body" style="padding-bottom:5px">
                         <p>Add a new zone to your location listing.</p>
                         <div class="mui-textfield mui-textfield--float-label">
                             <input type="text" name="zone_name" class="zone_text" required>
                             <label>Zone Name</label>
                         </div>
                         <input type="hidden" class="state_id" name="state_id" value="">
                          <input type="hidden" class="city_id" name="add_zone_city" value="">
                     </div>
                     <div class="modal-footer" style="text-align: right">
                         <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff " onclick="resetzonevalue()">CANCEL</button>
                           <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="ADD ZONE">
                     </div>
                 </form>
     
             </div>
         </div>
     </div>
     
      <div class="modal fade" id="add_cityModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" onclick="resetcityvalue()">&times;</button>
                     <h4 class="modal-title"><strong>ADD CITY</strong></h4>
                 </div>
                 <form action="" method="post" id="addcity_fly" autocomplete="off" onsubmit="add_newcity(); return false;">
                     <div class="modal-body" style="padding-bottom:5px">
                         <p>Add a new City to your location listing.</p>
                         <div class="mui-textfield mui-textfield--float-label">
                             <input type="text" name="city_add" class="city_text" required>
                             <label>City Name</label>
                         </div>
                         <input type="hidden" class="state_id" name="add_city_state" value="">
                       
                     </div>
                     <div class="modal-footer" style="text-align: right">
                         <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff " onclick="resetcityvalue()">CANCEL</button>
                           <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="ADD CITY">
                     </div>
                 </form>
     
             </div>
         </div>
      </div>

      
      <div class="modal fade" id="userstatus_changealertModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog" style="width:500px">
            <form action="" method="post" id="userstatus_change" autocomplete="off" onsubmit="update_userstatus(); return false;">
	       <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	       <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title"><strong>INACTIVATE USER?</strong></h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <input type="hidden" id="otp_to_activate_user" value="" />
		     <input type="hidden" id="phone_to_activate_user" value="" />
		     <p>To make inactivate <span class="userfname"><?php echo ucfirst($first_name) ?> <?php echo ucfirst($last_name) ?></span>, please select the one of the option below:
		     </p>
		     
		     <div class="col-lg-12 col-md-12 col-sm-12" style="margin:5px 0px">
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 nopadding">
			      <label class="radio-inline">
				 <input type="radio" class="inactivate_type" name="inactivate_type" value="suspended" required> Temporary Suspension
			      </label>
			   </div>
			
			   <div class="col-lg-6 col-md-6 col-sm-6">
			      <input type="hidden" name="suspend_days" values="0"> 			      
			   </div>
			</div>
		     </div>
		     <div class="col-lg-12 col-md-12 col-sm-12 nopadding" style="margin-bottom:0px">
			<label class="radio-inline">
			   <input type="radio" class="inactivate_type" name="inactivate_type" value="terminate" required> Terminate Services
			</label>
		     </div>
   
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="INACTIVATE" />
		  </div>
	       </div>
	    </form>
         </div>
      </div>

      
      
      
      <div class="modal fade" id="userstat_toActiveModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
            <form action="" method="post" autocomplete="off" onsubmit="resetuser_activate(); return false;">
	       <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	       <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title">
			<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
		     </h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <p>Are you sure you want to activate this user ?</p>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
		     <input type="submit" class="mui-btn  mui-btn--small mui-btn--accent" value="YES" />
		  </div>
	       </div>
	    </form>
         </div>
      </div>
      
      <!--user verifaication modal-->
      <div class="modal fade" id="userverifcation_toActiveModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
            <form action="" method="post" autocomplete="off"  return false;">
	       <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	       <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title">
			<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
		     </h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <p>Do You want to verify this user ?</p>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn  mui-btn--small mui-btn--accent notverifyuser" style="background-color:#4D4D4D" >NO</button>
		     <button type="button" class="mui-btn  mui-btn--small mui-btn--accent verifyuser">YES</button>
		  </div>
	       </div>
	    </form>
         </div>
      </div>

      <div class="modal fade" id="resetMacModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION RESET MAC</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p class="resetsucctxt">Are you sure you want to reset Mac for this user ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent resetsuccbtn" onclick="resetUserMac()">YES</button>
               </div>
            </div>
         </div>
      </div>
	  
	   <!-- Modal for Refresh DATA Success-->
	   <div class="modal fade" id="refreshdata" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p class="resetsucctxt">Data refreshed Successfully.</p>
               </div>
               
            </div>
         </div>
      </div>
	  
	  
      <div class="modal fade" id="editrouterpasswordModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
	    <form action="" method="post" autocomplete="off" onsubmit="editrouterpassword(); return false;">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title">
			<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>ROUTER PASSWORD CHANGE</strong>
		     </h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <div class="mui-textfield" style="margin-bottom:0px">
			<input type="text" name="routerpassword" id="routerpassword" required>
			<label>Router Password <sup>*</sup></label>
		     </div>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="SAVE" />
		  </div>
	       </div>
	    </form>
         </div>
      </div>

      
      <div class="modal fade" id="changePlanModal" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:9999999">
         <div class="modal-dialog modal-sm">
            <form action="" method="post" id="changePlanForm" autocomplete="off" onsubmit="apply_planfornow(); return false;">
	       <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	       <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title">
			<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
		     </h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <p>How do you want to calculate billing so far on Days or Data </p>
		     <input type="radio" name="prevplan_calcon" value="data" required /> Data Used: <span id="datacostxt"></span> <br/>
		     <input type="radio" name="prevplan_calcon" value="days" required /> Days: <span id="dayscostxt"></span> <br/>
		  </div>
		  <input type="hidden" name="datacost" id="datacost" value="" />
		  <input type="hidden" name="dayscost" id="dayscost" value="" />
		  <input type="hidden" name="prev_srvid" id="prev_srvid" value="" />
		  <input type="hidden" name="prev_billid" id="prev_billid" value="" />
		  <input type="hidden" name="prev_billpaid" id="prev_billpaid" value="" />
		  <input type="hidden" name="prev_user_plantype" id="prev_user_plantype" value="" />
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
		     <input type="submit" class="mui-btn  mui-btn--small mui-btn--accent" value="SUBMIT" />
		  </div>
	       </div>
	    </form>
         </div>
      </div>
      
      
      <div class="modal fade" id="planChangeAlertModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		  <h4 class="modal-title">
		     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CHANGE PLAN...</strong>
		  </h4>
	       </div>
	       <div class="modal-body" style="padding-bottom:5px">
		  <p>How do you want to apply this plan ?</p>
		  <p id="plandur_alert" style="color:#a1a1a1"></p>
	       </div>
	       <div class="modal-footer" style="text-align: right">
		  <div class="form-group" style="overflow:hidden;">
		     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="row">
			   <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
			      <a href="javascript:void(0)" class="mui-btn mui-btn--accent btn-lg btn-block" onclick="showconfirmplan_modal();">NOW</a>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6  disable_onadvprepay">
			      <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block" value="NEXT CYCLE" onclick="return add_nextcycleplan();">
			   </div>
			</div>
		     </div>
		  </div>
	       </div>
	    </div>
         </div>
      </div>
      
      <div class="modal fade" id="viewbillModal" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:9999999">
         <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
	       </div>
	       <div class="modal-body" style="padding-bottom:5px" id="billhtmldiv">
	       </div>
	       <div class="modal-footer" style="text-align: right">
	       </div>
	    </div>
         </div>
      </div>
      

      <div class="modal fade" id="alertModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		  <h4 class="modal-title">
		     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>ALERT</strong>
		  </h4>
	       </div>
	       <div class="modal-body" style="padding-bottom:5px">
		  <p id="alertMsgTxt"></p>  
	       </div>
	       <div class="modal-footer" style="text-align: right">
		  <div class="form-group" style="overflow:hidden;">
		     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right">
			      <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
			   </div>
			</div>
		     </div>
		  </div>
	       </div>
	    </div>
         </div>
      </div>
      <div class="modal fade" id="customPlanPriceModal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="customplanprice_form" autocomplete="off" onsubmit="addcustom_planprice(); return false;">
	    <input type="hidden" name="custom_srvid" id="custom_srvid" />
	    <input type="hidden" name="custom_srvid_duration" id="custom_srvid_duration" />
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title"><strong>CUSTOMIZE PLAN PRICE</strong></h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <label>Current Plan Price(pm) :</label>&nbsp;&nbsp;<span id="displaycurr_plan_price"></span>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="mui-textfield">
			      <input type="number" id="custom_plan_price" name="custom_plan_price" min="36" required="required" pattern="^[1-9]\d*$">
			      <label>Custom Plan Price<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="custplanbtn mui-btn mui-btn--small mui-btn--accent" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="custplanbtn mui-btn mui-btn--small mui-btn--accent" value="Save" />
		  </div>
	       </div>
	    </div>
	 </form>
      </div>
      
      
      
      
      <!----------------------------------------------------------------------------------------------------------->
      <!-- CUSTOM BILLING SECTION -->
      <div class="modal fade" tabindex="-1" role="dialog" id="add_companyModal" data-backdrop="static" data-keyboard="false">
	 <form class="mui-form" action="" method="post" id="addispcompany_form" autocomplete="off" onsubmit="addisp_company(); return false;">
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <center>
			<h4 class="modal-title" style="font-size:20px; font-weight:600;color:#f00f64">Add Company</h4>
		     </center>
		  </div>
		  <div class="modal-body">
		     <div class="row">
			   <div class="row">
			      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
			      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				 <h2 style="font-size: 16px; margin-top:15px">Add Company</h2>
			      </div>
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				 <div class="mui-textfield mui-textfield--float-label">
				    <input type="text" name="bill_company_name" value="" required>
				    <label>Add Company</label>
				 </div>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
			      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				 <h2 style="font-size: 16px; margin-top:15px">Taxes <small>(IGST,SGST,CGST)</small></h2>
			      </div>
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				 <div class="mui-textfield mui-textfield--float-label">
				    <input type="text" name="bill_company_tax" value="" min="1" required="required" pattern="^[1-9]\d*$">
				    <label>Taxes (IGST,SGST,CGST) </label>
				 </div>
			      </div>
			   </div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <center>
			<input type="submit" class="mui-btn mui-btn--accent" value="ADD" />
		     </center>
		  </div>
	       </div>
	    </div>
	 </form>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="add_topicModal" data-backdrop="static" data-keyboard="false">
	 <form class="mui-form" action="" method="post" id="addisptopic_form" autocomplete="off" onsubmit="addisp_topics(); return false;">
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <center>
			<h4 class="modal-title" style="font-size:20px; font-weight:600;color:#f00f64">Add Product</h4>
		     </center>
		  </div>
		  <div class="modal-body">
		     <div class="row">
			<div class="col-sm-12 col-xs-12">
			   <div class="col-sm-12 col-xs-12">
			      <div class="row">
				 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				    <h2 style="font-size: 16px; margin-top:15px">Name </h2>
				 </div>
				 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="text" name="topic_name" required>
				       <label>Name <sup>*</sup></label>
				    </div>
				 </div>
			      </div>
			      <div class="row">
				 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				    <h2 style="font-size: 16px; margin-top:20px">Price </h2>
				 </div>
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="row" style="margin-top:25px">
				       <label class="radio-inline">
				       <input name="bill_topictype" value="standard" type="radio" required> Standard 
				       </label>
				       <label class="radio-inline">
				       <input name="bill_topictype" value="dynamic" type="radio" required> Dynamic
				       </label>
				    </div>
				 </div>
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="text" name="topic_rate">
				       <label>Rate</label>
				    </div>
				 </div>
			      </div>
			      <div class="row">
				 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				    <h2 style="font-size: 16px; margin-top:20px">Tax Type</h2>
				 </div>
				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				    <div class="row" style="margin-top:25px">
				       <label class="radio-inline">
				       <input type="radio" name="taxtypes" value="igst" required /> IGST  
				       </label>
				       <label class="radio-inline">
				       <input type="radio" name="taxtypes" value="sgst_cgst" required /> SGST / CGST
				       </label>
				    </div>
				 </div>
			      </div>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <center>
			<input type="submit" class="mui-btn mui-btn--accent" value="Add Product" />
		     </center>
		  </div>
	       </div>
	    </div>
	 </form>
      </div>
      
      <div class="modal fade" id="confirm_cancel_custombill" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CANCEL CONFIRMATION </strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p class="resetsucctxt">Are you sure you want to cancel added custom bills ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" onclick="closeCustomBillHandler(1)">YES</button>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" tabindex="-1" role="dialog" id="customBillReceiptModal" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="custombill_receiptform" autocomplete="off" onsubmit="addcustombillrcpt(); return false;">
	    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <input type="hidden" name="rcptcustom_billid" value="" />
	    <input type="hidden" name="rcptcustom_billno" value="" />
	    
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <h4 class="modal-title">
			<strong>ADD AMOUNT RECEIPT</strong>
		     </h4>
		  </div>
		  
		  <div class="modal-body">
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
			      <div class="input-group" style="margin-top:-5px">
				 <div class="input-group-addon" style="padding:6px 10px">
				    <i class="fa fa-calendar"></i>
				 </div>
				 <input type="text" class="form-control rcptcustom_billdate" id="" placeholder="Amount Received On*"  required>
				 <input name="rcptcustom_billdatetime" type="hidden" value="">
			      </div>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="rcptcustom_receipt_number" id="rcptcustom_receipt_number" required>
			      <label>Cheque/DD/Receipt Number<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="rcptcustom_payment_mode" required>
				 <option value="">Select Payment Mode</option>
				 <option value="cash">Cash</option>
				 <option value="cheque">Cheque</option>
				 <option value="dd">Demand Draft</option>
				 <option value="viapaytm">Paytm</option>
				 <option value="netbanking">NetBanking</option>
			      </select>
			      <label>Mode of Payment<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="number" name="rcptcustom_amount" min="1" required>
			      <label>Amount Received<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 rcptcustom_chqddpaytm hide">
			   <div class="mui-textfield">
			      <input type="text" name="rcptcustom_cheque_dd_paytm" required="required" pattern="^[0-9]\d*$">
			      <label>Cheque/DD/Paytm No./Transaction ID<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="rcptcustom_bill_bankdetails hide">
			<div class="row">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding-left">
				 <div class="mui-textfield">
				    <input type="tel" name="bill_bankaccount_number" maxlength="20">
				    <label>Account Number</label>
				 </div>
			      </div>
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding-left">
				 <div class="mui-textfield">
				    <!--<input type="tel" class="req_cheque_dd" name="bill_bankissued_date">-->
				    <div class="input-group" style="margin-top:-5px">
				       <div class="input-group-addon" style="padding:6px 10px">
					  <i class="fa fa-calendar"></i>
				       </div>
				       <input type="text" class="form-control receiptdate" id="" placeholder="Issue Date" name="bill_bankissued_date" value="">
				    </div>
				    <label>Issued Date</label>
				 </div>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding-left">
				 <div class="mui-textfield">
				    <input type="text" name="bill_bankname">
				    <label>Bank Name</label>
				 </div>
			      </div>
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				 <div class="mui-textfield">
				    <input type="text" name="bill_bankaddress" class="nopadding-left">
				    <label>Bank Address</label>
				 </div>
			      </div>
			   </div>
			</div>
			<!--<div class="row">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				 <div class="mui-textfield">
				    <input type="file" name="bill_slipimage">
				    <label>Slip Image</label>
				 </div>
			      </div>
			   </div>
			</div>-->
		     </div>
		  </div>
		  <div class="modal-footer">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="Add Receipt" />
		  </div>
	       </div>
	       <!-- /.modal-content -->
	    </div>
	 </form>
      </div>
      
      <div class="modal fade" id="showadded_topicmodal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog modal-sm">
	    <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title">
			<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>Custom Products List</strong>
		     </h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <ul id="viewadded_topiclist">
			
		     </ul>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
		  </div>
	    </div>
	 </div>
     </div>
      <!-- BILLING SECTION EDITABLE MODALS -->
      <div class="modal fade" tabindex="-1" role="dialog" id="editbillingModal" data-backdrop="static" data-keyboard="false">
         <form action="" method="post" id="editbilling_form" autocomplete="off" onsubmit="updatebilldetails(); return false;" style="margin-top:-30px">
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <input type="hidden" name="editbillid" id="editbillid" value="" />
	    <input type="hidden" name="editbilltype" id="editbilltype" value="" />
	    <input type="hidden" name="editbill_receiptamount" id="editbill_receiptamount" value="" />
	    <input type="hidden" name="editbill_prevbillamount" id="editbill_prevbillamount" value="" />
	    <input type="hidden" name="editbillpaid" id="editbillpaid" value="" />
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <h4 class="modal-title">
			<strong>EDIT <span id="billtypetext"></span> BILL</strong>
			<span class="hide" id="billplanname"></span>
		     </h4>
		  </div>
		  <div class="modal-body">
		     <div class="disabledwhenpayadvanced">
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input id="editdbbilldtme" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="">
				 <label>Bill DateTime<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input type="text" id="editdbbill_number" name="editdbbill_number" readonly>
				 <label>Bill Number<sup>*</sup></label>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input type="text" name="editdbbill_billamount" id="editdbbill_billamount" required>
				 <label>Total Amount<sup>*</sup></label>
				 <span id="billamount_errtxt"></span>
				 <input type="hidden" id="billamount_err" value="0" />
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="">
				 <label style="display:block;">Discount (If Any)</label>
				 <input type="radio" name="discounttype" value="percent" /> Percent Discount &nbsp;&nbsp;
				 <input type="radio" name="discounttype" value="flat" /> Flat Discount
				 <select name="editdbbill_percentdiscount" class="form-control" style="font-size:12px">
				    <option value="0" selected>Select Discount %</option>
				    <?php
				    for($d=1; $d<=100; $d++){
				       echo '<option value="'.$d.'">'.$d.'%</option>';
				    }
				    ?>
				 </select>
				 <input type="number" name="editdbbill_flatdiscount" min="0" value="0" class="form-control hide"  style="font-size:12px"/>
			      </div>
			   </div>
			</div>
			<div class="row" style="margin-bottom:20px">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <h3 style="margin:0px;font-size:13px;">Additional Charges :</h3>
			      <div class="addichrgrow row">
				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				    <input type="text" name="additional_chrgtxt[]" class="form-control" value="" placeholder="Enter Charges Details" style="font-size:12px" />
				 </div>
				 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
				    <input type="number" name="additional_chrgval[]" class="form-control" value="" style="font-size:12px"/>
				 </div>
				 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 addrmvclass">
				    <a href="javascript:void(0)" onclick="addmorecharges()"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add</a>
				 </div>
			      </div>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="editdbbill_netamount" id="editdbbill_netamount" readonly required style="font-size:16px" />
			      <label>Net Amount (inclusive taxes)<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="editdbbill_hsncode" id="editdbbill_hsncode">
			      <label>HSN Code</label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <textarea name="editdbbill_comments"></textarea>
			      <label>Comments</label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <textarea name="editdbbill_remarks"></textarea>
			      <label>Remarks</label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="">
                              <label style="display:block;">is advanced paid</label>
			      <input type="radio" name="is_advanced_paid" value="1" /> Yes &nbsp;&nbsp;
			      <input type="radio" name="is_advanced_paid" value="0" checked/> No
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="Save" />
		  </div>
	       </div>
	       <!-- /.modal-content -->
	    </div>
	 </form>
      </div>
      
      
      <div class="modal fade" tabindex="-1" role="dialog" id="editplanspeedModal" data-backdrop="static" data-keyboard="false">
         <form action="" method="post" id="editplanspeed_form" autocomplete="off" onsubmit="update_planspeed(); return false;">
	    <input name="assigned_activeplan" id="assigned_activeplan" type="hidden" value="">
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <h4 class="modal-title">
			<strong>Edit PlanSpeed</strong>
		     </h4>
		  </div>
		  <div class="modal-body">
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input name="subscriber_uuid" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="<?php echo $subscriber_uid ?>" required>
			      <label>UID<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input name="editplan_uploadspeed" type="text" value="" required>
			      <label>Upload Speed (in Kbps)<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input name="editplan_downloadspeed" type="text" value="" required>
			      <label>Download Speed (in Kbps)<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-radio">
			      <label class="nopadding">Send Alert To Customer</label> <br/>
			      <input type="radio" name="editplan_speedalert" value="1"> Yes &nbsp;&nbsp;
			      <input type="radio" name="editplan_speedalert" value="0" checked> No
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="Save" />
		  </div>
	       </div>
	       <!-- /.modal-content -->
	    </div>
	 </form>
      </div>
      
      <div class="modal fade" id="payinstsecurityModal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="payinstsecurity_form" autocomplete="off" onsubmit="payinstsecuritybill(); return false;">
	    <input type="hidden" name="billid" value="" />
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title"><strong>Enter Receipt to Pay</strong></h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
			      <div class="input-group" style="margin-top:-5px">
				 <div class="input-group-addon" style="padding:6px 10px">
				    <i class="fa fa-calendar"></i>
				 </div>
				 <input type="text" class="form-control instsecu_billdate" id="" placeholder="Amount Received On*"  required>
				 <input name="instsecu_billdatetime" type="hidden" value="">
			      </div>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="tel" name="update_receipt_number" required="required" maxlength="20">
			      <label>Receipt Number<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="rcptcustom_payment_mode" required>
				 <option value="">Select Payment Mode</option>
				 <option value="cash">Cash</option>
				 <option value="cheque">Cheque</option>
				 <option value="dd">Demand Draft</option>
				 <option value="viapaytm">Paytm</option>
				 <option value="netbanking">NetBanking</option>
			      </select>
			      <label>Mode of Payment<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="number" name="rcptcustom_amount" min="1" required>
			      <label>Amount Received<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 rcptcustom_chqddpaytm hide">
			   <div class="mui-textfield">
			      <input type="text" name="rcptcustom_cheque_dd_paytm" required="required" pattern="^[0-9]\d*$">
			      <label>Cheque/DD/Paytm No./Transaction ID<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="rcptcustom_bill_bankdetails hide">
			<div class="row">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding-left">
				 <div class="mui-textfield">
				    <input type="tel" name="bill_bankaccount_number" maxlength="20">
				    <label>Account Number</label>
				 </div>
			      </div>
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding-left">
				 <div class="mui-textfield">
				    <!--<input type="tel" class="req_cheque_dd" name="bill_bankissued_date">-->
				    <div class="input-group" style="margin-top:-5px">
				       <div class="input-group-addon" style="padding:6px 10px">
					  <i class="fa fa-calendar"></i>
				       </div>
				       <input type="text" class="form-control receiptdate" id="" placeholder="Issue Date" name="bill_bankissued_date" value="">
				    </div>
				    <label>Issued Date</label>
				 </div>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding-left">
				 <div class="mui-textfield">
				    <input type="text" name="bill_bankname">
				    <label>Bank Name</label>
				 </div>
			      </div>
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				 <div class="mui-textfield">
				    <input type="text" name="bill_bankaddress" class="nopadding-left">
				    <label>Bank Address</label>
				 </div>
			      </div>
			   </div>
			</div>
			<!--<div class="row">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				 <div class="mui-textfield">
				    <input type="file" name="bill_slipimage">
				    <label>Slip Image</label>
				 </div>
			      </div>
			   </div>
			</div>-->
		     </div>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="custplanbtn mui-btn mui-btn--small mui-btn--accent" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="custplanbtn mui-btn mui-btn--small mui-btn--accent" value="Save" />
		  </div>
	       </div>
	    </div>
	 </form>
      </div>
      <div class="modal fade" id="walletbalhistoryModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		  <h4 class="modal-title">
		     <i class="fa fa-money" aria-hidden="true"></i>&nbsp;<strong>Wallet Balance History</strong>
		  </h4>
	       </div>
	       <div class="modal-body" style="padding-bottom:5px">
		  <p id="walletbalhistory"></p>  
	       </div>
	       <div class="modal-footer"></div>
	    </div>
         </div>
      </div>

      
      <div class="modal fade" id="advprepaydetailsModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <center>
		     <h4 class="modal-title" style="font-size:20px; font-weight:600;">Monthly Plan's Advanced Details</h4>
		  </center>
	       </div>
	       <div class="modal-body" style="padding-bottom:25px">
		  <div class="table-responsive">
		     <table class="table table-striped">
			<thead>
			   <tr class="active">
			      <th>&nbsp;</th>
			      <th>Bill No.</th>
			      <th>Receipt No.</th>
			      <th>Plan</th>
			      <th>Total Amount</th>
			      <th>Pay Months</th>
			      <th>Free Months</th>
			      <th>Added On</th>
			      <th>Action</th>
			   </tr>
			</thead>
			<tbody id="advprepay_listing">
			</tbody>
		     </table>
		  </div>
	       </div>
	    </div>
	 </div>
      </div>
      
      <!--check config setup-->
      <div class="modal modal-container fade" id="is_config_proper_setup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
	 <div class="modal-dialog" role="document">
	    <div class="modal-content">
	       <div class="modal-header" style="padding-left: 0px;">
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
		  <div class="modal-title">
		     <center>
			<h4 style="color:#414042;font-weight: bold">Check Configuration</h4>
		     </center>
		  </div>
	       </div>
	       <div class="modal-body" style="padding-top:0px">
		  <div class="row">
		     <div class="col-sm-10 col-xs-12 col-md-offset-1">
		       <div class="single category">
			<ul class="list-unstyled">
			      <li>
				 Is Nas Setup
				 <span class="pull-right">
				       <button class="btn-view-fund btn btn-default btn-xs" type="button">
					  <span class="glyphicon glyphicon-remove" aria-hidden="true" id="check_conf_nas_setup_no" style="display:none"></span>
					  <span class="glyphicon glyphicon-ok" aria-hidden="true" id="check_conf_nas_setup_yes" style="display:none"></span>
				       </button>
				 </span>
			      </li>
			      <li>
				 Is Plan Attached
				 <span class="pull-right">
				       <button class="btn-view-fund btn btn-default btn-xs" type="button">
					  <span class="glyphicon glyphicon-remove" aria-hidden="true" id="check_plan_setup_no" style="display:none"></span>
					  <span class="glyphicon glyphicon-ok" aria-hidden="true" id="check_plan_setup_yes" style="display:none"></span>
				       </button>
				 </span>
			      </li>
			      
			      <li>
				 Is Nas And Plan Associated
				 <span class="pull-right">
				       <button class="btn-view-fund btn btn-default btn-xs" type="button">
					  <span class="glyphicon glyphicon-remove" aria-hidden="true" id="check_conf_nas_plan_association_no" style="display:none"></span>
					  <span class="glyphicon glyphicon-ok" aria-hidden="true" id="check_conf_nas_plan_association_yes" style="display:none"></span>
				       </button>
				 </span>
			      </li>
			      
			      
			</ul>
		     </div>
		     </div>
		  </div>
	       </div>
	    </div>
	 </div>
      </div>
      
      <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/material.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/moment-with-locales.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap-material-datetimepicker.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/misc.js?version=13.6"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/billing.js?version=13.5"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/docupload.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/kycupload.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/data_topup.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/notification_alert.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/corpupload.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/canvasjs.min.js"></script>
      <link href="<?php echo base_url() ?>assets/css/bootstrap-toggle.min.css" rel="stylesheet">
      <script src="<?php echo base_url() ?>assets/js/bootstrap-toggle.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/crbnMenu.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/loadmore.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/datepicker/daterangepicker.js"></script>
      <script type="text/javascript">
         if ($(window)) {
            $(function () {
               $('.menu').crbnMenu({
               hideActive: true
               });
            });
         }
      </script>
      <script type="text/javascript">
	 $('body').on('click', '.slider', function(){
            var priority = ($('#priority_account').val() == 0) ? 1 : 0;
            $('#priority_account').val(priority);
         });
      </script>
      <script type="text/javascript" > 
	 $(document).ready(function() {
            var height = $(window).height();
            $('#main_div').css('height', height);
            $('#right-container-fluid').css('height', height);
	    window.scrollTo(0,0);
	    $('input[type="submit"]').addClass('is_editperm');
	    $('.is_editperm').css({'pointer-events' : 'auto'});
	    <?php if($billaddr != '1'){ ?>
	       $('input[name="billaddr"]').removeAttr('checked');
	       $('.billingaddr_div').removeClass('hide');
	       $('.requiredforbilling').attr('required', 'required');
	    <?php } ?>

	    /************** PERMISSION SETTING ***********/
	    <?php
	       $superadmin = $this->session->userdata['isp_session']['super_admin'];
	       $ustathideperm = $this->user_model->is_permission('USERSTATUS','HIDE');
	       $ustateditperm = $this->user_model->is_permission('USERSTATUS','EDIT');
	       $ustatreadperm = $this->user_model->is_permission('USERSTATUS','RO');
	       
	       $hideperm = $this->user_model->is_permission(PD,'HIDE');
	       $editperm = $this->user_model->is_permission(PD,'EDIT');
	       $readperm = $this->user_model->is_permission(PD,'RO');
	       
	       $khideperm = $this->user_model->is_permission(KYC,'HIDE');
	       $keditperm = $this->user_model->is_permission(KYC,'EDIT');
	       $kreadperm = $this->user_model->is_permission(KYC,'RO');
	       
	       $phideperm = $this->user_model->is_permission(PLANASSIGN,'HIDE');
	       $peditperm = $this->user_model->is_permission(PLANASSIGN,'EDIT');
	       $preadperm = $this->user_model->is_permission(PLANASSIGN,'RO');
	       
	       $thideperm = $this->user_model->is_permission(TOPUPASSIGN,'HIDE');
	       $teditperm = $this->user_model->is_permission(TOPUPASSIGN,'EDIT');
	       $treadperm = $this->user_model->is_permission(TOPUPASSIGN,'RO');
	       
	       $bhideperm = $this->user_model->is_permission(BILLING,'HIDE');
	       $beditperm = $this->user_model->is_permission(BILLING,'EDIT');
	       $breadperm = $this->user_model->is_permission(BILLING,'RO');
	       
	       $rhideperm = $this->user_model->is_permission(SERVICEREQUEST,'HIDE');
	       $reditperm = $this->user_model->is_permission(SERVICEREQUEST,'EDIT');
	       $rreadperm = $this->user_model->is_permission(SERVICEREQUEST,'RO');
	       
	       $shideperm = $this->user_model->is_permission(SETTING,'HIDE');
	       $seditperm = $this->user_model->is_permission(SETTING,'EDIT');
	       $sreadperm = $this->user_model->is_permission(SETTING,'RO');

	       $lhideperm = $this->user_model->is_permission(LOGS,'HIDE');
	       $leditperm = $this->user_model->is_permission(LOGS,'EDIT');
	       $lreadperm = $this->user_model->is_permission(LOGS,'RO');
	       
	       $syshideperm = $this->user_model->is_permission('SYSLOG','HIDE');
	       $syseditperm = $this->user_model->is_permission('SYSLOG','EDIT');
	       $sysreadperm = $this->user_model->is_permission('SYSLOG','RO');
	       
	       $invtryhideperm = $this->user_model->is_permission('INVENTORY','HIDE');
	       $invtryeditperm = $this->user_model->is_permission('INVENTORY','EDIT');
	       $invtryreadperm = $this->user_model->is_permission('INVENTORY','RO');
	       
	       $notifyhideperm = $this->user_model->is_permission('NOTIFICATIONS','HIDE');
	       $notifyeditperm = $this->user_model->is_permission('NOTIFICATIONS','EDIT');
	       $notifyreadperm = $this->user_model->is_permission('NOTIFICATIONS','RO');
	       
	       $diaghideperm = $this->user_model->is_permission('DIAGNOSTICS','HIDE');
	       $diageditperm = $this->user_model->is_permission('DIAGNOSTICS','EDIT');
	       $diagreadperm = $this->user_model->is_permission('DIAGNOSTICS','RO');
	       
	       $mbillhideperm = $this->user_model->is_permission('MONTHLYTAB','HIDE');
	       $mbilleditperm = $this->user_model->is_permission('MONTHLYTAB','EDIT');
	       $mbillreadperm = $this->user_model->is_permission('MONTHLYTAB','RO');
	       
	       $cbillhideperm = $this->user_model->is_permission('CUSTOMBILLS','HIDE');
	       $cbilleditperm = $this->user_model->is_permission('CUSTOMBILLS','EDIT');
	       $cbillreadperm = $this->user_model->is_permission('CUSTOMBILLS','RO');
	       
	       $sbillhideperm = $this->user_model->is_permission('SETUPBILLS','HIDE');
	       $sbilleditperm = $this->user_model->is_permission('SETUPBILLS','EDIT');
	       $sbillreadperm = $this->user_model->is_permission('SETUPBILLS','RO');
	       
	       /**************** HIDE TAB CASES **********************/
	       if($hideperm){ ?>
		  $('#personal_details').addClass('mui--is-active');
		  $('#Personal_details').addClass('mui--is-active');
	 <?php
		  if($editperm == false){
		     if($readperm == true){ ?>
			$('#edit_subscriber_form input').prop('disabled', true);
			$('#edit_subscriber_form select').prop('disabled', true);
	    <?php    }
		  }
	       }elseif($khideperm){ ?>
		  $('#kyc_details').addClass('mui--is-active');
		  $('#KYC_details').addClass('mui--is-active');
		  defaultkyctab_details();
		  <?php
		  if($keditperm == false){
		     if($kreadperm == true){ ?>
			defaultkyctab_details();
			$('#subscriber_documents_form input').prop('disabled', true);
			$('#subscriber_documents_form select').prop('disabled', true);
	    <?php    }
		  } ?>
	 <?php }elseif($phideperm){ ?>
		  $('#plantab').addClass('mui--is-active');
		  $('#Plan_details').addClass('mui--is-active');
		  defaultplantab_details();
		  <?php
		  if($peditperm == false){
		     if($preadperm == true){ ?>
		     $('#subscriber_plandetail_form input').prop('disabled', true);
		     $('#subscriber_plandetail_form select').prop('disabled', true);
		     $('#nextcycle_planform input').prop('disabled', true);
		     $('#nextcycle_planform select').prop('disabled', true);
		     $('#next_cycle_plan').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
		     $('a.customize_planspeed').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
		     $('a.planautorenewal').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
	    <?php    }
		  } ?>
	 <?php }elseif($thideperm){  ?>
		  $('#topuptab').addClass('mui--is-active');
		  $('#Topup').addClass('mui--is-active');
		  defaulttopuptab_details();
		  <?php
		  if($teditperm == false){
		     if($treadperm == true){ ?>
		     $('#addtopup_form input').prop('disabled', true);
		     $('#addtopup_form select').prop('disabled', true);
	    <?php    }
		  } ?>
	 <?php }elseif($bhideperm){  ?>
		  $('#billtab').addClass('mui--is-active');
		  $('#Billing').addClass('mui--is-active');
		  defaultbilltab_details();
		  <?php
		  if($beditperm == false){
		     if($breadperm == true){ ?>
			$('.billing_list a').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
	    <?php    }
		  } ?>
	 <?php }elseif($rhideperm){  ?>
		  $('#tickettab').addClass('mui--is-active');
		  $('#Requests').addClass('mui--is-active');
		  fetch_allopenticket();
		  <?php
		  if($reditperm == false){
		     if($rreadperm == true){ ?>
			$('.isticketperm a').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
	    <?php    }
		  } ?>
	 <?php }elseif($shideperm){  ?>
		  $('#settingtab').addClass('mui--is-active');
		  $('#Settings').addClass('mui--is-active');
		  defaultsettingtab_details();
		  <?php
		  if($seditperm == false){
		     if($sreadperm == true){ ?>
			$('form#syssetting_form a').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
			$('form#syssetting_form').find('input,button').prop('disabled', true);
	    <?php    }
		  } ?>
	    <?php
	       }elseif($lhideperm){  ?>
		  $('#logtab').addClass('mui--is-active');
		  $('#Logs').addClass('mui--is-active');
		  usuage_logs();
	    <?php
	       }elseif($syshideperm){  ?>
		  $('#syslogtab').addClass('mui--is-active');
		  $('#sysogs').addClass('mui--is-active');
		  getsyslogdetails();
	    <?php
	       }elseif($invtryhideperm){  ?>
		  $('#inventorytab').addClass('mui--is-active');
		  $('#inventory').addClass('mui--is-active');
	    <?php
	       }elseif($notifyhideperm){  ?>
		  $('#notificationtab').addClass('mui--is-active');
		  $('#notifications').addClass('mui--is-active');
		  user_notification_listing();
	    <?php
	       }elseif($diaghideperm){  ?>
		  $('#diagnostictab').addClass('mui--is-active');
		  $('#diagnostic').addClass('mui--is-active');
	    <?php
	       }
	    ?>
	    /**************** HIDE TAB CASES END **********************/
	    <?php
	    if($mbillhideperm){ ?>
	       $('#mbilltab').addClass('mui--is-active');
	       $('#active_billing_pane').addClass('mui--is-active');
	    <?php
	       }elseif($cbillhideperm){ ?>
		  $('#cbilltab').addClass('mui--is-active');
		  $('#custom_billing_pane').addClass('mui--is-active');
		  custombill_listing();
	    <?php
	       }elseif($sbillhideperm){ ?>
		  $('#sbilltab').addClass('mui--is-active');
		  $('#setup_billing_pane').addClass('mui--is-active');
		  user_billing_listing();
	    <?php
	       } ?>
	    
	    <?php if($superadmin != '1'){ ?>
	       <?php if($rreadperm){ ?>
		  $('.isticketperm a').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
	       <?php } ?>
	       <?php if($treadperm){ ?>
		  $('#addtopup_form input').prop('disabled', true);
		  $('#addtopup_form select').prop('disabled', true);
	       <?php } ?>
	       <?php if($preadperm == true){ ?>
		  $('a#next_cycle_plan').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
		  $('a.customize_planspeed').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
		  $('a.planautorenewal').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
	       <?php } ?>
	       <?php if($sreadperm == true){ ?>
		  $('form#syssetting_form a').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
		  $('form#syssetting_form').find('input,button').prop('disabled', true);
	       <?php } ?>
	       <?php if($invtryreadperm == true){ ?>
		  $('form#add_user_inventory').find('input, textarea, button, select').prop('disabled', true);
		  $('.invtry_readable').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
	       <?php } ?>
	       <?php if($notifyreadperm == true){ ?>
		  $('#notifications').find('input, button').prop('disabled', true);
	       <?php } ?>
	       <?php if($diagreadperm == true){ ?>
		  $('.diag_readable').prop('disabled', true);
	       <?php } ?>

	    <?php } ?>
	    /************** PERMISSION SETTING ***********/
	    
	    var active_user = "<?php echo $active_user; ?>";
	    var user_plan_type = "<?php echo $user_plan_type; ?>";
	    if (active_user == '1') {
	       $('#userstatus_changealert').parent().removeClass('btn-danger off');
               $('#userstatus_changealert').parent().addClass('btn-success on');
	       $('select[name="assign_plan"]').prop('disabled', true);
	       $('input[name="user_plan_type"]').prop('disabled', true);
	       $('input[name="prebill_oncycle"]').prop('disabled', true);
	       $('input[name="prebill_oncycle"]').css('opacity', '0.2');
	       $('#plansubmit').css({'cursor' : 'not-allowed', 'pointer-events' : 'auto'});
	       $('#plansubmit').attr('disabled','disabled');
	       <?php
	       if($ustathideperm){
		  if($ustateditperm == false){
		     if($ustatreadperm == true){ ?>
			$('#userstatus_changealert').prop('disabled', true);
			$('.btn-success').css('cursor', 'not-allowed');
	 <?php	     }
		  }
	       } ?>
	    }else{
	       $('#userstatus_changealert').val(0);
	       $('#userstatus_changealert').parent().removeClass('btn-success');
	       $('#userstatus_changealert').parent().addClass('btn-danger off');
	       <?php
	       if($ustathideperm){
		  if($ustateditperm == false){
		     if($ustatreadperm == true){ ?>
			$('#userstatus_changealert').prop('disabled', true);
			$('.btn-danger').css('cursor', 'not-allowed');
	 <?php	     }
		  }
	       } ?>
	    }
	    
	    $('#usertype_customer').attr('checked','checked');
            $('.radio-inline').css('color', '#b3b3b3');
            $('input[type="radio"][name="user_type_radio"]').attr('disabled','disabled');
	    
            // on user type change change page(form)
            var defaultchked = $('input[type=radio][name=user_type_radio]:checked').val();
            if (defaultchked == 'lead') {
               $('.tab_menu').css({'cursor' : 'not-allowed'});
               $('.tab_menu').attr('disabled','disabled');
            }
            $('input[type=radio][name=user_type_radio]').change(function() {
               $('.citylist').empty().append('<option value="">City*</option>');
               if (this.value == 'lead' || this.value == 'enquiry') {
                  if (this.value == 'lead') {
                     $('#subscriber_usertypele').val(1);
                  }else{
                     $('#subscriber_usertypele').val(2);
                  }
                  $('.tab_menu').css({'cursor' : 'not-allowed'});
                  $('.tab_menu').attr('disabled','disabled');
                  $("#user_type_lead_enquiry")[0].reset();
                  $("#user_type_lead_enquiry").show();
                  $("#user_type_customer").hide();
               }else if (this.value == 'customer') {
                  $('#subscriber_usertype').val(3);
                  $('.tab_menu').css({'cursor' : 'not-allowed'});
                  $('.tab_menu').attr('disabled','disabled');
                  $("#user_type_customer")[0].reset();
                  /*
                  var date = new Date();
                  var components = [
                      date.getYear(),
                      date.getMonth(),
                      date.getDate(),
                      date.getHours(),
                      date.getMinutes(),
                      date.getSeconds(),
                      date.getMilliseconds()
                  ];
                  var uuid = components.join("");
                
                  //var uuid = "<?php //echo random_string('alnum', 6); ?>";
                  //$('input[name="subscriber_uuid"]').val(uuid);
                  //$('input[name="subscriber_username"]').val(uuid);  */
                  
                  $("#user_type_customer").show();
                  $("#user_type_lead_enquiry").hide();
               }
            });
	    
	    <?php if(isset($activate_tab) && $activate_tab == 'b'){ ?>
		     
		     window.scrollTo(0,0);
		     mui.tabs.activate('Billing');
		     $('.loading').removeClass('hide');
		     var subsid = $('.subscriber_userid').val();
		     var subs_uuid = $('.subscriber_uuid').val();
		     $.ajax({
			 url: base_url+'user/planassoc_withuser',
			 type: 'POST',
			 dataType: 'json',
			 data: 'subsid='+subsid+'&subs_uuid='+subs_uuid,
			 success: function(data){
			     $('.loading').addClass('hide');
			     if (data == 0) {
				 $('#inactive_billing').removeClass('hide');
				 $('#active_billing').addClass('hide');
			     }else{
				 $('#inactive_billing').addClass('hide');
				 $('#active_billing').removeClass('hide');
				 user_billing_listing();
			     }
			 }
		     });
	    <?php
		  }
	    ?>
	    
	    <?php if(isset($activate_tab) && $activate_tab == 't'){ ?>

		     window.scrollTo(0,0);
		     mui.tabs.activate('Requests');
		     fetch_allopenticket();
	    <?php
		  }
	    ?>
	    
	    $.ajax({
	       url: base_url+'user/taxapplicable',
	       type: 'POST',
	       async: false,
	       success: function(datatax){
		  $('.current_taxapplicable').val(datatax);
	       }
	    });
	    
	    setTimeout(function (){ $('.loading').addClass('hide') },1000);
         });
      </script>
      <script type="text/javascript">
	 $('body').on('change', 'input[name="topuptype"]', function(){
	    $('#topup_err').addClass('hide');
	    $('#topup_err').html('');
	    $('#topup_price').html('<?php echo $ispcodet['currency'] ?> 0');
	    $('#topup_totalcost').html('<?php echo $ispcodet['currency'] ?> 0');
	    $('input[name="topup_totalcost"]').val('0');
	    $('#topup_actualcost').val('');
	    $('.top_startdate').val('');
	    $('.top_enddate').val('');
	    $('#topup_dayscount').val('0');
	    $('#applicable_days').html('-');
	    $('.topup_validate').prop("disabled", true);
	    var topup_filter = $(this).val();
	    var cust_uuid = $('.subscriber_uuid').val();
	    $('.top_startdate').bootstrapMaterialDatePicker({ format : 'DD-MM-YYYY', time:false,clearButton: true, minDate : moment().startOf('day').add(1, 'd') });
	    $('.top_enddate').bootstrapMaterialDatePicker({ format : 'DD-MM-YYYY', time:false,clearButton: true, minDate : moment().startOf('day').add(2, 'd') });
	    
	    $.ajax({
		url: base_url+'user/datatopup_check',
		type: 'POST',
		dataType: 'json',
		data: 'uuid='+cust_uuid+'&topup_filter='+topup_filter,
		async: false,
		success: function(result){
		    var active_topuplist = result.active_topuplist;
		    $('#topuptimer').addClass('hide');
		    if ((topup_filter == '2') || (topup_filter == '3')) {
			$('#topuptimer').removeClass('hide');
			$('.topup_validate').attr('required', true);
		    }else{
			$('.topup_validate').attr('required',false);
		    }
		    $('select[name="active_topups"]').empty().append(active_topuplist);;
		}
	    });
	});

	$('body').on('change', '.topup_validate', function(){
	    var startdate = $('.top_startdate').val();
	    var enddate = $('.top_enddate').val();
	    if((startdate !='') || (enddate !='')) {
	       var sdarr = startdate.split('-');
	       var edarr = enddate.split('-');
	       var sdformat = sdarr[2]+", "+sdarr[1]+", "+sdarr[0];
	       var edformat = edarr[2]+", "+edarr[1]+", "+edarr[0];
	       var topup_weekdays = $('input[name="topup_weekdays"]').val();
	       var diffdays = 0;
	       var daysArr = [];
	       var from = new Date(sdformat);
	       var to = new Date(edformat);
	       var DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
	       var d = from;
	       while (d <= to) {
		  var dayname = DAYS[d.getDay()];
		  daysArr.push(DAYS[d.getDay()]);
		  if (topup_weekdays.indexOf(dayname) > -1) {
		     diffdays += 1;
		  }
		  d = new Date(d.getTime() + (24 * 60 * 60 * 1000));
	       }
	       var topup_actualcost = $('#topup_actualcost').val();
	       if (topup_actualcost != '') {
		  var topup_totalcost = topup_actualcost * diffdays;
		  $('#topup_dayscount').val(diffdays);
		  $('input[name="topup_totalcost"]').val(topup_totalcost);
		  $('#topup_totalcost').html('<?php echo $ispcodet['currency'] ?>  '+topup_totalcost+'.00');
	       }else{
		  $('#topup_dayscount').val('0');
		  $('input[name="topup_totalcost"]').val('0');
		  $('#topup_totalcost').html('<?php echo $ispcodet['currency'] ?> 0');
	       }
	    }
	    
	});
	
	//refresh data function
	$(document).on('click','.data_refresh',function(){
		var uid=$(this).data('uid');
		$.ajax({
		url: base_url+'user/refresh_data',
		type: 'POST',
		dataType: 'json',
		data: 'uid='+uid,
		async: false,
		success: function(result){
		   $('#refreshdata').modal('show');
		}
	    });
	})
	
	function topup_pricing(topid) {
	   $('#topup_actualcost').val('');
	   var topuptype = $('input[name="topuptype"]:checked').val();
	   if (topid == '' && ((topuptype == '2') || (topuptype == '3'))) {
	       $('.topup_validate').prop("disabled", true);
	       $('.top_startdate').val('');
	       $('.top_enddate').val('');
	   }else{
	       $('.topup_validate').prop("disabled", false);
	       $.ajax({
		   url: base_url+'user/topup_pricing',
		   type: 'POST',
		   data: 'topupid='+topid+'&topuptype='+topuptype,
		   dataType: 'json',
		   success: function(result){
		       $('#topup_actualcost').val(result.topup_price);
		       $('#topup_price').html('<?php echo $ispcodet['currency'] ?>  '+result.topup_price);
		       if (topuptype == '1') {
			   $('input[name="topup_totalcost"]').val(result.topup_price);
			   $('#topup_totalcost').html('<?php echo $ispcodet['currency'] ?>  '+result.topup_price);
		       }else{
			   $('.top_startdate').bootstrapMaterialDatePicker({ format : 'DD-MM-YYYY', time:false,clearButton: true, minDate : moment().startOf('day').add(1, 'd') });
			   $('.top_enddate').bootstrapMaterialDatePicker({ format : 'DD-MM-YYYY', time:false,clearButton: true, minDate : moment().startOf('day').add(2, 'd') });
			
			   $('input[name="topup_weekdays"]').val(result.days);
			   $('#applicable_days').html(result.days);
			   var startdate = $('.top_startdate').val();
			   var enddate = $('.top_enddate').val();
			   if((startdate =='') || (enddate =='')) {
			       $('#topup_dayscount').val('0');
			       $('input[name="topup_totalcost"]').val('0');
			       $('#topup_totalcost').html('<?php echo $ispcodet['currency'] ?> 0');
			   }else{
			      var sdarr = startdate.split('-');
			      var edarr = enddate.split('-');
			      var sdformat = sdarr[2]+", "+sdarr[1]+", "+sdarr[0];
			      var edformat = edarr[2]+", "+edarr[1]+", "+edarr[0];
			      var topup_weekdays = $('input[name="topup_weekdays"]').val();
			      var diffdays = 0;
			      var daysArr = [];
			      var from = new Date(sdformat);
			      var to = new Date(edformat);
			      var DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
			      var d = from;
			      while (d <= to) {
				 var dayname = DAYS[d.getDay()];
				 daysArr.push(DAYS[d.getDay()]);
				 if (topup_weekdays.indexOf(dayname) > -1) {
				    diffdays += 1;
				 }
				 d = new Date(d.getTime() + (24 * 60 * 60 * 1000));
			      }
			      var topup_actualcost = $('#topup_actualcost').val();
			      if (topup_actualcost != '') {
				 var topup_totalcost = topup_actualcost * diffdays;
				 $('#topup_dayscount').val(diffdays);
				 $('input[name="topup_totalcost"]').val(topup_totalcost);
				 $('#topup_totalcost').html('<?php echo $ispcodet['currency'] ?>  '+topup_totalcost+'.00');
			      }else{
				 $('#topup_dayscount').val('0');
				 $('input[name="topup_totalcost"]').val('0');
				 $('#topup_totalcost').html('<?php echo $ispcodet['currency'] ?> 0');
			      }
			   }

		       }
		   }
	       });
	   }
	}
      </script>
      <script type="text/javascript">
         $(document).ready(function(){
            $('.date').bootstrapMaterialDatePicker({
               format: 'DD-MM-YYYY',
               time: false,
               //clearButton: true
            });
	    
	    $('.receiptdate').bootstrapMaterialDatePicker({
               format: 'DD-MM-YYYY',
               time: false,
               //clearButton: true
            });
	    
	    var d = new Date(); var currY = d.getFullYear();
            var validY = currY - 18;
            $('.dobdate').bootstrapMaterialDatePicker({
               maxDate: moment("12/31/"+validY),
               format: 'DD-MM-YYYY',
               time: false,
               //clearButton: true
            });
	    
            $('.date-start').bootstrapMaterialDatePicker({
               weekStart: 0,
	       format: 'DD-MM-YYYY',
	       shortTime : true
            }).on('change', function(e, date){
                  $('.date-end').bootstrapMaterialDatePicker('setMinDate', date);
               });
            <?php if($dob != ''){ ?>
            $('.min-date').bootstrapMaterialDatePicker({ format : 'DD-MM-YYYY', minDate : <?php echo $dob; ?> });
	    <?php }else{ ?>
	    $('.min-date').bootstrapMaterialDatePicker({ format : 'DD-MM-YYYY', minDate : new Date() });
	    <?php } ?>
            
	    
	    $('.walletamt_receivedate').bootstrapMaterialDatePicker({
               weekStart: 0,
	       format: 'DD-MM-YYYY HH:mm',
	       //shortTime : true,
	       maxDate: moment()
            }).on('change', function(e, date){

	       var dateObj = new Date(date);
	       var month = (dateObj.getMonth()+1).toString();
	       var day = dateObj.getDate().toString();
	       var seconds = dateObj.getSeconds();
	       var year = dateObj.getFullYear();
	       var dbtktdatetime =  year + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + dateObj.getHours() +":"+ dateObj.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;
               $('input[name="addtowallet_bill_datetime"]').val(dbtktdatetime);
            });
	    
	    $('.custom_billdate').bootstrapMaterialDatePicker({
               weekStart: 0,
	       format: 'DD-MM-YYYY',
	       time: false,
	       maxDate: moment()
            }).on('change', function(e, date){
	       var dateObj = new Date(date);
	       var month = (dateObj.getMonth()+1).toString();
	       var day = dateObj.getDate().toString();
	       var year = dateObj.getFullYear();
	       var seconds = dateObj.getSeconds();
	       var dbtktdatetime =  year + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + dateObj.getHours() +":"+ dateObj.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;
               $('input[name="custom_billdbdate"]').val(dbtktdatetime);
            });
	    
	    $('.rcptcustom_billdate').bootstrapMaterialDatePicker({
               weekStart: 0,
	       format: 'DD-MM-YYYY',
	       time: false,
	       maxDate: moment()
            }).on('change', function(e, date){
	       var dateObj = new Date(date);
	       var month = (dateObj.getMonth()+1).toString();
	       var day = dateObj.getDate().toString();
	       var year = dateObj.getFullYear();
	       var seconds = dateObj.getSeconds();
	       var dbtktdatetime =  year + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + dateObj.getHours() +":"+ dateObj.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;
               $('input[name="rcptcustom_billdatetime"]').val(dbtktdatetime);
            });
	    
	    $('.instsecu_billdate').bootstrapMaterialDatePicker({
               weekStart: 0,
	       format: 'DD-MM-YYYY HH:mm',
	       //shortTime : true,
	       maxDate: moment()
            }).on('change', function(e, date){

	       var dateObj = new Date(date);
	       var month = (dateObj.getMonth()+1).toString();
	       var day = dateObj.getDate().toString();
	       var seconds = dateObj.getSeconds();
	       var year = dateObj.getFullYear();
	       var dbtktdatetime =  year + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + dateObj.getHours() +":"+ dateObj.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;
               $('input[name="instsecu_billdatetime"]').val(dbtktdatetime);
            });
	    
	    
            $.material.init()
         });

      </script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA61iANDo6jqGYQm0SjXzIvmOEnks-MpG0&amp;libraries=places"  type="text/javascript"></script>
      <script type="text/javascript">
	 $('body').on('change', 'input[type=radio][name=geoaddress_searchtype]', function(){
	    var searchtype = $(this).val();
	    if (searchtype == 'byaddr') {
	       $('.geobyaddr').removeClass('hide');
	       $('.geobylatlng').addClass('hide');
	       $('#geolocation').attr('required','required');
	       $('#geolat').removeAttr('required');
	       $('#geolong').removeAttr('required');
	    }else if (searchtype == 'bylatlng') {
	       $('.geobyaddr').addClass('hide');
	       $('.geobylatlng').removeClass('hide');
	       $('#geolocation').removeAttr('required');
	       $('#geolat').attr('required','required');
	       $('#geolong').attr('required','required');
	    }
	 });
	 $('document').ready(function(){
	    <?php if($geoaddress_searchtype == 'byaddr'){ ?>
	       $('.geobyaddr').removeClass('hide');
	       $('.geobylatlng').addClass('hide');
	       $('#geolocation').attr('required','required');
	       $('#geolat').removeAttr('required');
	       $('#geolong').removeAttr('required');
	    <?php }elseif($geoaddress_searchtype == 'bylatlng'){ ?>
	       $('.geobyaddr').addClass('hide');
	       $('.geobylatlng').removeClass('hide');
	       $('#geolocation').removeAttr('required');
	       $('#geolat').attr('required','required');
	       $('#geolong').attr('required','required');
	    <?php } ?>
	 });
	   function initialize() {
	  
	      var input = document.getElementById('geolocation');
	      //var options = {componentRestrictions: {country: 'in'}};
	      var autocomplete = new google.maps.places.Autocomplete(input);
	      google.maps.event.addListener(autocomplete, 'place_changed', function () {
		  var place = autocomplete.getPlace();
		  document.getElementById('lat').value = place.geometry.location.lat().toFixed(6);
		  document.getElementById('long').value = place.geometry.location.lng().toFixed(6);
		  document.getElementById('placeid').value = place.place_id;
		  $("#placeid").attr('class','mui--is-dirty valid mui--is-not-empty');
		  $("#lat").attr('class','mui--is-dirty valid mui--is-not-empty');
		  $("#long").attr('class','mui--is-dirty valid mui--is-not-empty');
	      });
	  }
	  
	 function reverse_geolocation() {
	    var geolat = $('#geolat').val();
	    var geolng = $('#geolong').val();
	    
	    if (geolat == '' && geolng == '') {
	       window.alert("Please enter Latitude & Longitude of the location."); 
	    }else{
	       var latlng = {lat: parseFloat(geolat), lng: parseFloat(geolng)};
	       var geocoder = new google.maps.Geocoder;
	       geocoder.geocode({'location': latlng}, function(results, status) {
		  if (status === 'OK') {
		     if (results[0]) {
			$('#geoaddress').val(results[0].formatted_address);
			$('#geoplaceid').val(results[0].place_id);
		     } else {
			window.alert('No results found');
		     }
		  } else {
		    window.alert('Geocoder failed due to: ' + status);
		  }
	       });
	    }
	 }
	 
	 google.maps.event.addDomListener(window, 'load', initialize);
      </script>
      <script type="text/javascript">
	 $(function() {
	    $('input[name="datefilter"]').daterangepicker({
	       locale: {
		  format: 'DD.MM.YYYY',
	       },
	       //startDate: moment().startOf('month').format('DD.MM.YYYY'),
	       //endDate: moment(),
	       "maxDate": moment(),
		}).on('change', function(e){
	       
		 usuage_logs();
	    });
	    
	    $('input[name="radius_datefilter"]').daterangepicker({
	       locale: {
		  format: 'DD.MM.YYYY',
	       },
	       //startDate: moment(),
	       //endDate: moment(),
	       "maxDate": moment(),
		}).on('change', function(e){
	       
		 radius_diagnostic();
	    });
	    
	    $('input[name="activitylogfilter"]').daterangepicker({
	       locale: {
		  format: 'DD.MM.YYYY',
	       },
	       startDate: moment().startOf('month').format('DD.MM.YYYY'),
	       endDate: moment(),
	       "maxDate": moment(),
	    }).on('change', function(e){
	       
	       activitylogs_pagination(1);
	    });
	    
	 });
	  
	 $("input[name='logs_activity_graph']").change(function() {
	 
	 var graph_type = $("input[name='logs_activity_graph']:checked").val();
	 if (graph_type == '1') {
	    usuage_logs();
	    $("#usages_graph_div").show();
	    $("#data_graph_div").hide();
	 }else{
	    data_logs();
	    $("#usages_graph_div").hide();
	    $("#data_graph_div").show();
	 }
      });
      
      function routerlogout(uuid) {
	 var c = confirm("Are you sure, you want to logout this user internet session ?");
	 if (c) {
	    $('.loading').removeClass('hide');
	    var subsid = $('.subscriber_userid').val();
	    $.ajax({
	       url: base_url+'user/disconnect_userineternet',
	       type: 'POST',
	       data: 'uuid='+uuid,
	       success: function(data){
		  window.location = base_url+'user/edit/'+subsid;
	       }
	    });
	 }
      }
 </script>


      <!--<script type="text/javascript">
	 $(document).ready(function(){
	    $('[data-toggle="popover"]').popover({
	       placement: 'right'
	    });
	 });
      </script>-->
   </body>
</html>

