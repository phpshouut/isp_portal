<?php
$isp_detail = $this->user_model->get_isp_name();
$isp_name = $isp_detail['isp_name'];
$fevicon = $isp_detail['fevicon_icon'];
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <link rel="icon" href="<?php echo base_url()?>assets/images/<?php echo $fevicon?>" type="image/x-icon"/>
      <title><?php echo $isp_name; ?> </title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
      <style type="text/css">
            .other_tabs__bar ul li{
                width: auto!important;
                height: auto!important;
                background-color:#ffffff !important;
                float: left;
                margin: 0px;
                padding: 0px;
                color: #b3b3b3 !important;
                cursor: pointer;
                font-size: 16px!important;
                font-family: 'Open Sans',sans-serif;
                font-weight: 500;
            }
      </style>
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading">
	 <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                     <?php
                     $isplogo = $this->user_model->get_ispdetail_info();
                     if($isplogo != 0){
                        echo '<img src="'.base_url()."ispmedia/logo/".$isplogo.'" class="img-responsive"/>';
                     }else{
                        echo '<img src="'.base_url().'assets/images/decibel.png" class="img-responsive"/>';
                     }
                     ?>
                     </span>
                  </div>
                  <?php
		     $leftperm['navperm']=$this->user_model->leftnav_permission();
		     $this->view('left_nav',$leftperm);
		  ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <span class="navbar-brand">Users</span>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
			      <?php if($this->user_model->is_permission(CREATEUSER,'ADD')){ ?>
                              <li>
                                 <a href="javascript:void(0)" onclick="setup_adduser()">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD USER
                                 </button>
                                 </a>
                              </li>
			      <?php } ?>
			      <?php if($this->user_model->is_permission(PLANS,'ADD')){ ?>
                              <li>
                                 <a href="javascript:void(0)" onclick="check_tax_plan()">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD PLAN
                                 </button>
                                 </a>
                              </li>
			      <?php } ?>
			      <?php if($this->user_model->is_permission(TOPUP,'ADD')){ ?>
			      <li>
                                 <a href="javascript:void(0)" onclick="check_tax_topup()">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD TOPUP
                                 </button>
                                 </a>
                              </li>
			      <?php }
			      $this->load->view('includes/global_setting',$leftperm);
			      ?>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px">
                              <ul>
                                 <li id="active_users">
                                    <h5>ACTIVE USERS</h5>
                                    <h4><i class="fa fa-user" aria-hidden="true"></i> <?php echo $active_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li id="inactive_users">
                                    <h5>INACTIVE USERS</h5>
                                    <h4><i class="fa fa-wifi" aria-hidden="true"></i> <?php echo $inactive_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li id="lead_enquiry">
                                    <h5>LEADS & ENQUIRIES</h5>
                                    <h4><i class="fa fa-question-circle" aria-hidden="true"></i> <?php echo $lead_enquiry_users?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li style="background-color:#f4474a"  id="user_complaints">
                                    <h5>COMPLAINTS</h5>
                                    <h4><i class="fa fa-exclamation-circle" aria-hidden="true"></i> <?php echo $complaints?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li style="background-color:#f4474a" id="payment_overdue">
                                    <h5>PAYMENT OVERDUE</h5>
                                    <h4><?php echo $ispcodet['currency'] ?> <?php echo $payment_dues?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                                 <li style="background-color:#f4474a" id="otherdues">
                                    <h5>OTHER DUES</h5>
                                    <h4><?php echo $ispcodet['currency'] ?> <?php echo $otherdues?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                 </li>
                              </ul>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:5px;">
                                      <div class="other_tabs__bar">  
                                         <ul class="mui-tabs__bar">
                                            <li class="mui--is-active">
                                               <a data-mui-toggle="tab" data-mui-controls="otc" onclick="getotcdues()">OTC</a></li>
                                            <li><a data-mui-toggle="tab" data-mui-controls="wallet_dues" onclick="getwalletdues()">Wallet Dues</a></li>
                                            <li><a data-mui-toggle="tab" data-mui-controls="custom_dues" onclick="getcustbilldues()">Custom Dues</a></li>
                                          </ul>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
                                              <div class="mui-tabs__pane mui--is-active" id="otc">
                                                <h2 style="font-size:15px;margin:0px;text-align:right;">Total OTC Due: <?php echo $ispcodet['currency']. ' <span id="tototcdue">'.$OTC['total_pendingAmt'] .'</span>' ?></h2>
                                                <div class="col-lg-12 col-md-12 col-sm-12" style="padding:0px;">
                                                    <div class="table-responsive">
                                                       <table class="table table-striped">
                                                          <thead style="font-size:11px">
                                                             <tr class="active">
                                                                <th>&nbsp;</th>
                                                                <th>Username</th>
                                                                <th>BillType</th>
                                                                <th>BillAmt</th>                                                                <th>Total Amount</th>
                                                                <th>AddedOn</th>
                                                             </tr>
                                                          </thead>
                                                          <tbody id="otclisting" style="font-size:11px">
                                                            <?php echo $OTC['otcdue_list']; ?>
                                                          </tbody>
                                                       </table>
                                                    </div>
                                                 </div>
                                              </div>
                                              <div class="mui-tabs__pane" id="wallet_dues">
                                                <h2 style="font-size:15px;margin:0px;text-align:right;">Total Wallet Due: <?php echo $ispcodet['currency']. ' '?><span id="totwaletdue"></span></h2>
                                                <div class="col-lg-12 col-md-12 col-sm-12" style="padding:0px;">
                                                    <div class="table-responsive">
                                                       <table class="table table-striped">
                                                          <thead style="font-size:11px">
                                                             <tr class="active">
                                                                <th>&nbsp;</th>
                                                                <th>Username</th>
                                                                <th>Total Amount</th>
                                                             </tr>
                                                          </thead>
                                                          <tbody id="walletduelisting" style="font-size:11px">                                                          </tbody>
                                                       </table>
                                                    </div>
                                                 </div>
                                              </div>
                                              <div class="mui-tabs__pane" id="custom_dues">
                                                <h2 style="font-size:15px;margin:0px;text-align:right;">Total CustomBill Due: <?php echo $ispcodet['currency']. ' '?><span id="totcustbilldue"></span></h2>
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                       <thead style="font-size:11px">
                                                          <tr class="active">
                                                             <th>&nbsp;</th>
                                                             <th>Username</th>
                                                             <th>Bill Details</th>
                                                             <th>Total Amount</th>
                                                          </tr>
                                                       </thead>
                                                       <tbody id="customduelisting" style="font-size:11px">                                                          </tbody>
                                                    </table>
                                                 </div>
                                              </div>
                                          </div>
                                      </div>  
                                    </div>
                                </div>
                            </div>
			</div>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/material.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/misc.js?version=3.1"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/crbnMenu.js"></script>
      <script type="text/javascript">
         if ($(window)) {
            $(function () {
               $('.menu').crbnMenu({
               hideActive: true
               });
            });
         }
      </script>
      <script type="text/javascript">
         $('body').on('click','.searchclear', function(){
            $('#search_user_complaint').val('');
            search_lefilter();
            
         });
         $('body').on('keyup', '#search_user_complaint', function(){
	    setTimeout(function (){
	       search_lefilter();
	    }, 2000);
         });
         
	 $('body').on('click', '#inactive_users', function(){
	    window.location.href = base_url+'user/status/inact';
	 });
	 
	 $('body').on('click', '#active_users', function(){
	    window.location.href = base_url+'user/status/act';
	 });
	 
         $('body').on('click', '#lead_enquiry', function(){
	    window.location.href = base_url+'user/lead_enquiry';
	 });
	 
	 $('body').on('click', '#user_complaints', function(){
	    window.location.href = base_url+'user/complaints';
	 });
	 $('body').on('click', '#payment_overdue', function(){
	    window.location.href = base_url+'user/pay_overdue'; 
	 });
	 
         $('body').on('click', '#otherdues', function(){
	    window.location.href = base_url+'user/otherdues'; 
	 });
	 
         $(document).ready(function() {
	    var height = $(window).height();
            $('#main_div').css('height', height);
	    //$('#right-container-fluid').css('height', height);
	    
	    $('.loading').addClass('hide');

         });
         function getotcdues() {
            $.ajax({
                url: base_url+"user/getotcdues",
                type:'POST',
                dataType: 'JSON',
                success: function(result){
                    $('#otclisting').html(result.otcdue_list);
                    $('#tototcdue').html(result.total_pendingAmt);
                }
            })
         }
         
         function getwalletdues() {
            $.ajax({
                url: base_url+"user/getwalletdues",
                type:'POST',
                dataType: 'JSON',
                success: function(result){
                    $('#walletduelisting').html(result.walletdue_list);
                    $('#totwaletdue').html(result.total_pendingAmt);
                }
            })
         }
         function getcustbilldues() {
            $.ajax({
                url: base_url+"user/getcustbilldues",
                type:'POST',
                dataType: 'JSON',
                success: function(result){
                    $('#customduelisting').html(result.custbilldue_list);
                    $('#totcustbilldue').html(result.total_pendingAmt);
                }
            })
         }
      </script>
      <script type="text/javascript">
	 /************** PERMISSION SETTING ***********/
	 $(document).ready(function() {
	    <?php
	       $superadmin = $this->session->userdata['isp_session']['super_admin'];
	       $active_hideperm = $this->user_model->is_permission('ACTIVEUSERS','HIDE');
	       $inactive_hideperm = $this->user_model->is_permission('INACTIVEUSERS','HIDE');
	       $leadenqry_hideperm = $this->user_model->is_permission('LEADENQUIRYUSERS','HIDE');
	       $complaint_hideperm = $this->user_model->is_permission('COMPLAINTSUSERS','HIDE');
	       $paymentdue_hideperm = $this->user_model->is_permission('PAYMENTDUEUSERS','HIDE');
	       $otherdue_hideperm = $this->user_model->is_permission('OTHERDUESUSERS','HIDE');
	    ?>   
	    <?php if(($active_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#active_users').addClass('hide');
	    <?php }else{ ?>
		  $('li#active_users').removeClass('hide');
	    <?php } ?>
	    <?php if(($inactive_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#inactive_users').addClass('hide');
	    <?php }else{ ?>
		  $('li#inactive_users').removeClass('hide');
	    <?php } ?>
	    <?php if(($leadenqry_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#lead_enquiry').addClass('hide');
	    <?php }else{ ?>
		  $('li#lead_enquiry').removeClass('hide');
	    <?php } ?>
	    <?php if(($complaint_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#user_complaints').addClass('hide');
	    <?php }else{ ?>
		  $('li#user_complaints').removeClass('hide');
	    <?php } ?>
	    <?php if(($paymentdue_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#payment_overdue').addClass('hide');
	    <?php }else{ ?>
		  $('li#payment_overdue').removeClass('hide');
	    <?php } ?>
	    <?php if(($otherdue_hideperm == false) && ($superadmin != '1')){ ?>
		  $('li#otherdues').addClass('hide');
	    <?php }else{ ?>
		  $('li#otherdues').removeClass('hide');
	    <?php } ?>
	 });
      </script>
   </body>
</html>