<?php
$subscriber_uid=''; $subscriber_id=''; $subscriber_username=''; $first_name=''; $middle_name=''; $last_name=''; $email=''; $phone=''; $dob=''; $flat_number=''; $address1=''; $address2=''; $state = ''; $city = ''; $zone = ''; $pancard = '';  $locality = ''; $priority_account = ''; $usertype = ''; $lechecked='checked="checked"'; $enquirychecked=''; $mac = ''; $ipaddr_type = ''; $maclockedstatus = ''; $subscriber_gstin_number = ''; $taxtype = ''; $altphone = ''; $geoaddr=""; $placeid=""; $lat=""; $long=""; $geoaddress_searchtype = '';

if($record != 0){
   
   foreach($record as $recordobj){
      //$subscriber_uid = $recordobj->useruid;
      $subscriber_id = $recordobj->id;
      $subscriber_username = $recordobj->username;
      $first_name = $recordobj->first_name;
      $middle_name = $recordobj->middle_name; 
      $last_name = $recordobj->last_name;
      $email = $recordobj->email;
      $phone = $recordobj->phone;
      $altphone = $recordobj->alt_mobile;
      $dob = $recordobj->dob;
      $flat_number = $recordobj->flat_number;
      $address1 = $recordobj->address1;
      $address2 = $recordobj->address2;
      $state = $recordobj->state;
      $city = $recordobj->city;
      $zone = $recordobj->zone;
      $pancard = $recordobj->pancard;
      $locality = $recordobj->usuage_locality;
      $priority_account = $recordobj->priority_account;
      $usertype = $recordobj->usertype;
      $mac = isset($recordobj->mac) ? $recordobj->mac : '';
      $ipaddr_type = isset($recordobj->ipaddr_type) ? $recordobj->ipaddr_type : '';
      $maclockedstatus = isset($recordobj->maclockedstatus) ? $recordobj->maclockedstatus : '';
      $geoaddr = $recordobj->geoaddress;
      $placeid = $recordobj->place_id;
      $lat = (round($recordobj->lat)==0)?"":$recordobj->lat;
      $long = (round($recordobj->longitude)==0)?"":$recordobj->longitude;
      $geoaddress_searchtype = $recordobj->geoaddress_searchtype;
      
      if($usertype == '1'){
         $lechecked = 'checked="checked"';
      }else{
         $enquirychecked = 'checked="checked"';
      }
   }
}

$isp_detail = $this->user_model->get_isp_name();
$isp_name = $isp_detail['isp_name'];
$fevicon = $isp_detail['fevicon_icon'];
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <link rel="icon" href="<?php echo base_url()?>assets/images/<?php echo $fevicon?>" type="image/x-icon"/>
      <title><?php echo $isp_name; ?> </title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-material-datetimepicker.css" />
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading">
	 <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                     <?php
                     $isplogo = $this->user_model->get_ispdetail_info();
                     if($isplogo != 0){
                        echo '<img src="'.base_url()."ispmedia/logo/".$isplogo.'" class="img-responsive"/>';
                     }else{
                        echo '<img src="'.base_url().'assets/images/decibel.png" class="img-responsive"/>';
                     }
                     ?>
                     </span>
                  </div>
                  <?php
		     $leftperm['navperm']=$this->user_model->leftnav_permission();
		     $this->view('left_nav',$leftperm);
		  ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">Add New User</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
                              <li style="margin: 10px" class="active_addc">
                                 <input data-toggle="toggle" data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger" type="checkbox" id="user_activate_check" value="0" style="height: 30px">
                              <input type="hidden" id="userstatus_changealert" value="0">
			      </li>
                              <li>
                                 <a href="<?php echo base_url().'user' ?>">
                                    <span class="mui-btn mui-btn--small mui-btn--accent">CANCEL AND EXIT</span>
                                 </a>
                              </li>
			      <?php
			      $this->load->view('includes/global_setting',$leftperm);
			      ?>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="add_user">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <ul class="mui-tabs__bar">
                                    <li class="mui--is-active"  id="personal_details">
                                       <a data-mui-toggle="tab" data-mui-controls="Personal_details" class="tab_menu" id="editclick_personal_details">
                                       User Details
                                       </a>
                                    </li>
                                    <li id="kyc_details">
                                       <a data-mui-toggle="tab" data-mui-controls="KYC_details" class="tab_menu" id="editclick_kyc_details">
                                       KYC
                                       </a>
                                    </li>
                                    <li>
                                       <!--class="tab_menu"-->
                                       <a data-mui-toggle="tab" data-mui-controls="Plan_details" class="tab_menu" id="editclick_plan_details">
                                       Plans
                                       </a>
                                    </li>
                                    <li>
                                       <a data-mui-toggle="tab" data-mui-controls="Topup" class="tab_menu" id="editclick_topup_details">
                                      Topup
                                       </a>
                                    </li>
                                    <li>
                                       <!--class="tab_menu"-->
                                       <a data-mui-toggle="tab" data-mui-controls="Billing" class="tab_menu" id="editclick_billing_details">
                                       Billing
                                       </a>
                                    </li>
                                    <li>
                                       <a data-mui-toggle="tab" data-mui-controls="Requests" class="tab_menu" onclick="fetch_allopenticket()">
                                       Requests / Complaints
                                       </a>
                                    </li>
                                    <li>
                                       <a data-mui-toggle="tab" data-mui-controls="Settings" class="tab_menu">
                                       Settings
                                       </a>
                                    </li>
                                    <!--<li>
                                       <a data-mui-toggle="tab" data-mui-controls="Logs" class="tab_menu">
                                       Logs & Activity
                                       </a>
                                    </li>-->
                                 </ul>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <input type="hidden" class="current_taxapplicable" id="current_taxapplicable" value="" />
			      <input type="hidden" id="ispcountry_currency" value="<?php echo $ispcodet['currency'] ?>" />
                              <div class="mui--appbar-height"></div>
                              <div class="mui-tabs__pane mui--is-active" id="Personal_details">
                                 <h2>Personal Details</h2>
                                 <div class="form-group">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <h4>STAGE</h4>
                                       <div class="row">
                                          <label class="radio-inline">
                                          <input type="radio" name="user_type_radio" value="lead" <?php echo $lechecked; ?> > Lead
                                          </label>
                                          <label class="radio-inline">
                                          <input type="radio" name="user_type_radio" value="enquiry"  <?php echo $enquirychecked; ?> > Enquiry
                                          </label>
                                          <label class="radio-inline">
                                          <input type="radio" name="user_type_radio" value="customer" id="usertype_customer"> Customer
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                                 <form action="<?php echo base_url().'user/add_lead_enquiry' ?>" method="post" id="user_type_lead_enquiry" autocomplete="off">
                                    <input type="hidden" name="subscriber_usertype" id="subscriber_usertypele" value='1'>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row">
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"  style="padding-left:0px;">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_fname" value="<?php echo $first_name; ?>" required>
                                                         <label>First Name<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_mname" value="<?php echo $middle_name; ?>">
                                                         <label>Middle Name</label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_lname" value="<?php echo $last_name; ?>" required>
                                                         <label>Last Name<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="email" name="subscriber_email" value="<?php echo $email; ?>" required>
                                                         <label>Email ID<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="tel" name="subscriber_phone" maxlength="10" minlength="10" value="<?php echo $phone; ?>" pattern="[6789][0-9]{9}" required>
                                                         <label>Mobile No.<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
                                                         <div class="input-group" style="margin-top:-5px">
                                                            <div class="input-group-addon" style="padding:6px 10px">
                                                               <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control dobdate" id="" placeholder="Date of Birth*" name="subscriber_dob" value="<?php echo $dob; ?>">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"  style="padding-left:0px;">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_flatno" value="<?php echo $flat_number; ?>" required>
                                                         <label>Flat / Door No.<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_address1" value="<?php echo $address1; ?>" required>
                                                         <label>Address Line 1<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_address2" value="<?php echo $address2; ?>">
                                                         <label>Address Line 2</label>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display: block; font-size: 16px; margin-bottom: 20px; padding-left: 0; padding-top: 25px;">
                                                      <div class=""><!--mui-select-->
                                                         <select name="state" id="state_leadenquiry" onchange="getcitylist(this.value)" required style="width:100%">
                                                            <option value="">State*</option>
                                                            <?php $this->user_model->state_list($state); ?>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display: block; font-size: 16px; margin-bottom: 20px; padding-left: 0; padding-top: 25px;">
                                                      <div class="">
                                                         <select name="city" id="city_leadenquiry" class="citylist" onchange="getzonelist(this.value)" style="width:100%" required>
                                                            <option value="">City*</option>
                                                            <?php
                                                            if($city != ''){
                                                               $this->user_model->getcitylist($state,$city);
                                                            }
                                                            ?>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display: block; font-size: 16px; margin-bottom: 20px; padding-left: 0; padding-top: 25px;">
                                                      <div class="">
                                                         <select name="zone" class="zonelist" style="width:100%" onchange="check_toadd_newzone(this.value, 'leadenquiry')" required>
                                                            <option value="">Zone*</option>
                                                            <?php
                                                            if($zone != ''){
                                                               $this->user_model->zone_list($city, $zone, $state);
                                                            }
                                                            ?>
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                      <div class="mui-select">
                                                         <select name="usuage_locality" required>
                                                            <option value="">User Category*</option>
                                                            <?php $this->user_model->usage_locality($locality); ?>
                                                         </select>
                                                      </div>
                                                   </div>
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center enquiry_status hide">
                                                      <a href="javascript:void(0)" onclick="showenquiryfeedback()" style="font-size:15px">Add Enquiry Status</a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
					  
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block" value="SAVE">
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                                 <form action="" method="post" id="user_type_customer" style="display: none" autocomplete="off" onsubmit="add_customer(); return false;">
                                    <input type="hidden" name="letoc_dataid" value="<?php echo $letoc_dataid ?>" />
                                    <input type="hidden" name="subscriber_usertype" id="subscriber_usertype" value=''>
                                    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value=''>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row">
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"  style="padding-left:0px;">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_fname"  value="<?php echo $first_name; ?>" required>
                                                         <label>First Name<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_mname" value="<?php echo $middle_name; ?>">
                                                         <label>Middle Name</label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_lname" value="<?php echo $last_name; ?>" required>
                                                         <label>Last Name<sup>*</sup></label>
                                                      </div>
                                                   </div>
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="email" name="subscriber_email" value="<?php echo $email; ?>" required>
                                                         <label>Email ID<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="tel" name="subscriber_phone" maxlength="10" minlength="10" value="<?php echo $phone; ?>"  pattern="[6789][0-9]{9}" required>
                                                         <label>Mobile No.<sup>*</sup></label>
                                                      </div>
                                                   </div>
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="tel" name="subscriber_altphone" maxlength="10" minlength="10" value="<?php echo $altphone; ?>" pattern="[6789][0-9]{9}">
                                                         <label>Alternate Mobile No.</label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
                                                         <div class="input-group" style="margin-top:-5px">
                                                            <div class="input-group-addon" style="padding:6px 10px">
                                                               <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control dobdate" placeholder="Date of Birth*" name="subscriber_dob" value="<?php echo $dob; ?>">
                                                         </div>
                                                      </div>
                                                   </div>
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-select">
                                                         <select name="usuage_locality" required>
                                                            <option value="">User Category*</option>
                                                            <?php $this->user_model->usage_locality($locality); ?>
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"  style="padding-left:0px;">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_flatno" value="<?php echo $flat_number; ?>" required>
                                                         <label>Flat / Door No.<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_address1" value="<?php echo $address1; ?>" required>
                                                         <label>Address Line 1<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_address2" value="<?php echo $address2; ?>">
                                                         <label>Address Line 2</label>
                                                      </div>
                                                   </div>
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display: block; font-size: 16px; margin-bottom: 20px; padding-left: 0; padding-top: 25px;">
                                                      <div class="">
                                                         <select name="state" id="state_customer" onchange="getcitylist(this.value, 'subscriber')" required style="width:100%">
                                                            <option value="">State*</option>
                                                            <?php $this->user_model->state_list($state); ?>
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display: block; font-size: 16px; margin-bottom: 20px; padding-left: 0; padding-top: 25px;">
                                                      <div class="">
                                                         <select name="city" id="city_customer" class="citylist" onchange="getzonelist(this.value, 'subscriber')" style="width:100%" required>
                                                            <option value="">City*</option>
                                                            <?php
                                                            if($city != ''){
                                                               $this->user_model->getcitylist($state,$city);
                                                            }
                                                            ?>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display: block; font-size: 16px; margin-bottom: 20px; padding-left: 0; padding-top: 25px;">
                                                      <div class="">
                                                         <select name="zone" class="zonelist" style="width:100%" onchange="check_toadd_newzone(this.value, 'customer')" required>
                                                            <option value="">Zone*</option>
                                                            <?php
                                                            if($zone != ''){
                                                               $this->user_model->zone_list($city, $zone, $state);
                                                            }
                                                            ?>
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
					  <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
						      <div class="mui-textfield mui-textfield--float-label">
							 <input type="text" name="gstin_number" >
							 <label>GSTIN Number<sup>*</sup></label>
						      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                      <h4>Tax Type</h4>
						      <label class="radio-inline">
						      <input type="radio" name="taxtype" value="1" required > CGST/SGST
						      </label>
						      <label class="radio-inline">
						      <input type="radio" name="taxtype" value="2" required > IGST
						      </label>
                                                   </div>
						   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                      <div class="mui-textfield">
                                                         <input type="text" name="subscriber_username" required>
                                                         <label>Username<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="subscriber_uuid" readonly="readonly" class="mui--is-untouched mui--is-dirty mui--is-not-empty" style="color:#acacac">
                                                         <label>UID<sup>*</sup></label>
                                                      </div>
                                                   </div>
						</div>
					     </div>
					   </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px">
                                                      <div class="row" style="margin-top:25px">
                                                         <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <h4 class="toggle_heading"> Priority Account</h4>
                                                         </div>
                                                         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <label class="switch">
                                                               <input type="checkbox" name="priority_account" id="priority_account" value="0">
                                                               <div class="slider round"></div>
                                                            </label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
					  <?php
					  $byaddrchk=''; $bylatlngchk='';
					  if($geoaddress_searchtype == 'byaddr'){
					     $byaddrchk = 'checked="checked"';
					  }else if($geoaddress_searchtype == 'bylatlng'){
					     $bylatlngchk = 'checked="checked"';
					  }
					  ?>
					  
					  <div class="form-group">
					     <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                   <div class="mui-radio">
						      <label class="nopadding">Choose Geolocation Type <sup>*</sup> </label> &nbsp;
						      <input type="radio" name="geoaddress_searchtype" required="required" value="byaddr" <?php echo $byaddrchk ?>>By Address &nbsp;
						      <input type="radio" name="geoaddress_searchtype" required="required" value="bylatlng" <?php echo $bylatlngchk ?>>By Latitude/Longitude
						   </div>
                                                </div>
                                             </div>
					  </div>
					  <div class="geobyaddr hide">
					     <div class="form-group">
						<div class="row">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text" id="geolocation" name="geoaddr" value="<?php echo $geoaddr; ?>" required placeholder="">
							    <label>GeoAddress</label>
							</div>
						      </div>
						   </div>
						</div>
					     </div>
					     <div class="form-group">
						<div class="row">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text"  id="placeid"  value="<?php echo $placeid; ?>" name="placeid"  readonly>
							    <label>Placeid</label>
							  </div>
						      </div>
						      
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text"  id="lat" value="<?php echo $lat; ?>" name="lat" readonly>
							    <label>Lat</label>
							 </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">   
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text" id="long" name="long" value="<?php echo $long; ?>"  readonly>
							    <label>Long</label>
							</div>
						      </div>
						   </div>
						</div>
					      </div>
					  </div>
					  <div class="geobylatlng hide">
					     <div class="form-group">
						<div class="row">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding">         
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text" name="geolat" id="geolat" required value="<?php echo $lat; ?>">
							    <label>Lat</label>
							 </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">   
							 <div class="mui-textfield mui-textfield--float-label">
							    <input type="text" name="geolong" id="geolong" required value="<?php echo $long; ?>">
							    <label>Long</label>
							</div>
						      </div>
						      <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="margin-top:20px; background-color:#a1a1a1; text-align:center; padding:5px; color:#ffffff;">
							 <span onclick="reverse_geolocation()" style="cursor:pointer">GET Address</span>
						      </div>
						   </div>
						</div>
					     </div>
					     <div class="form-group">
						<div class="row">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
							 <div class="mui-textfield">
							    <input type="text" id="geoaddress" name="geoaddress"  value="<?php echo $geoaddr; ?>" readonly>
							    <label>GeoAddress</label>
							  </div>
						      </div>
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
							 <div class="mui-textfield">
							    <input type="text" id="geoplaceid" name="geoplaceid"  value="<?php echo $placeid; ?>" readonly>
							    <label>Placeid</label>
							  </div>
						      </div>
						   </div>
						</div>
					     </div>
					  </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="NEXT" >
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                              <div class="mui-tabs__pane" id="KYC_details">
                                 <!--<h2>KYC Details</h2>-->
                                 <form action="" method="post" enctype="multipart/form-data" autocomplete="off" onsubmit="add_subscriber_documents(); return false;" id="subscriber_documents_form">
                                    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value=''>
				    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value=''>
				    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row">
					  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
					     <div class="row">
						<div class="form-group">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <h4>Photo ID Documents<sup>*</sup></h4>
						      
						      <div class="idproof_divmaker">
							 <div class="row">
							    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							       <div class="mui-select" style="margin-bottom:5px">
								  <select name="idproof_doctype" onchange="activate_iddocupload()" required>
								     <option value="">Select Document<sup>*</sup></option>
								     <option value="pan_card">Pan Card</option>
								     <option value="voterid_card">VoteriId Card</option>
								     <option value="aadhar_card">Aadhar Card</option>
								     <option value="passport">Passport</option>
								     <option value="driving_license">Driving License</option>
								     <option value="identity_card">Identity Card</option>
								  </select>
								  <input type="hidden" name="idproof_doctype_arr[]" id="idproof_doctype_arr"/>
							       </div>						 
							    </div>
							    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input type="text" maxlength="15" class="idproof_docnumber" required>
								  <input type="hidden" name="idproof_docnumber_arr[]" id="idproof_docnumber_arr">
								  <label>Enter Document Number<sup>*</sup></label>
							       </div>
							    </div>
							    <div class="idpfileperoption">
							       <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" id="default_iddocdiv">
								  <span class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600">CHOOSE FILE</span>
							       </div>
							    </div>
							 </div>
							 <div class="row"><div id="idprooflisting"></div></div>
						      </div>
						   </div>
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px">&nbsp;</div>
						</div>
						
						<div class="form-group">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <h4>Address Proof Documents<sup>*</sup></h4>
						      <div class="row">
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <div class="mui-select" style="margin-bottom:5px">
							       <select name="kyc_documents" onchange="activate_docupload()" required>
								  <option value="">Select Document<sup>*</sup></option>
								  <option value="pan_card">Pan Card</option>
								  <option value="aadhar_card">Aadhar Card</option>
								  <option value="bank_statement">Bank Statement</option>
								  <option value="rent_agreement">Rent Agreement</option>
								  <option value="electricity_bill">Electricity Bill</option>
								  <option value="ration_card">Ration Card</option>
								  <option value="driving_license">Driving License</option>
								  <option value="passport">Passport</option>
								  <option value="identity_card">Identity Card</option>
								  <option value="voterid_card">VoteriId Card</option>
								  <option value="registration_certificate">Registration Certificate</option>
							       </select>
							    <input type="hidden" name="kyc_documents_arr[]" id="kyc_documents_arr"/>
							    </div>
							 </div>
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <div class="mui-textfield mui-textfield--float-label">
							       <input type="text" class="kycdocnumber" maxlength="15"  required>
							       <input type="hidden" name="kycdocnumber_arr[]" id="kycdocnumber_arr">
							       <label>Enter Document Number<sup>*</sup></label>
							    </div>
							 </div>
							 <div class="fileperoption">
							    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" id="defaultdocdiv">
							       <span class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600">CHOOSE FILE</span>
							    </div>
							 </div>
						      </div>
						      <div class="row"><div id="kyclisting"></div></div>
						   </div>
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px">&nbsp;</div>
						</div>
						
						<div class="form-group">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <h4>Corporate Documents</h4>
						      <div class="row">
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <div class="mui-select" style="margin-bottom:5px">
							       <select name="corporate_doctype" onchange="activate_corpdocupload()">
								  <option value="">Select Document</option>
								  <option value="electricity_bill">Electricity Bill</option>
								  <option value="authority_letter">Authority Letter</option>
								  <option value="registration">Registration</option>
							       </select>
							    <input type="hidden" name="corporate_doctype_arr[]" id="corporate_doctype_arr"/>
							    </div>
							 </div>
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <div class="mui-textfield mui-textfield--float-label">
							       <input type="text" class="corporate_docnumber">
							       <input type="hidden" name="corporate_docnumber_arr[]" id="corporate_docnumber_arr">
							       <label>Enter Document Number</label>
							    </div>
							 </div>
							 <div class="corpfileperoption">
							    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" id="default_corporatedocdiv">
							       <span class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600">CHOOSE FILE</span>
							    </div>
							 </div>
						      </div>
						      <div class="row"><div id="corporatelisting"></div></div>
						   </div>
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px">&nbsp;</div>
						</div>
						
						<div class="form-group">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="row">
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <h4>User Image<sup>*</sup></h4>
							    <span class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600" id="trigger_userimage">CHOOSE FILE</span>
							    <input type="file" id="user_profile_image" class="hide" name="user_profile_image" accept="image/*" />
                                                            <div id="user_image"></div>
							 </div>
							 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							    <h4>User Signature</h4>
							    <span class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600" id="trigger_signatureimage">CHOOSE FILE</span>
							    <input type="file" id="user_signature_image" class="hide" name="user_signature_image" accept="image/*" />
                                                            <div id="user_signature"></div>
							 </div>
						      </div>
						   </div>
						</div>
						<div class="form-group">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:25px">
						   <div class="row">
						      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							 <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block" value="SAVE" >
						      </div>
						   </div>
						</div>
					     </div>
					     </div>
					  </div>
					  
					  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					     <div class="row">
						<div class="form-group">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						      <div class="row">
							 <div class="mui-textfield mui-textfield--float-label">
							    <label>Uploaded Files</label>
							    <a href="<?php echo base_url().'user/downloadkyc/'.$subscriber_id ?>" style="float:right;cursor:pointer;" >Download <i class="fa fa-download" aria-hidden="true"></i></a>
							 </div>
						      </div>
						   </div>
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="uploaded_docs"></div>
						</div>
					     </div>
					  </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                              <div class="mui-tabs__pane" id="Plan_details">
                                 <form action="" method="post" autocomplete="off" onsubmit="add_subscriber_plan(); return false;" id="subscriber_plandetail_form">
                                    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value=''>
                                    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value=''>
				    <input type="hidden" name="plan_duration" class="plan_duration" value='1'>
				    <input type="hidden" name="custom_planid" class="custom_planid" value="" />
				    <input type="hidden" name="custom_planprice" class="custom_planprice" value="" />
				    
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                       <div class="row">
                                          <h2>Active Plan</h2>
                                          <div class="form-group">
					     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="row">
						   <input type="radio" name="user_plan_type" value="prepaid" required /> Prepaid Plan User
						   &nbsp; &nbsp;
						   <input type="radio" name="user_plan_type" value="postpaid" required /> Postpaid Plan User
						</div>
					     </div>
					     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hide" style="margin:10px 0px;color:#aeaeae; font-size:14px" id="prebillmonthlyopt">
						<div class="row">
						   <input type="checkbox" name="prebill_oncycle" value="1" /> Monthly Bill according to BillingCycle
						</div>
					     </div>
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left:0px;">
                                                      <div class="mui-select">
                                                         <select name="assign_plan" id="assign_plan" onchange="getplandetails(this.value)" required>
							 </select>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						      <a href="<?php echo base_url().'plan/add_plan' ?>" style="display:inline-block; margin-top: 20px;" target="_blank"> Create New Plan </a>
						   </div>
						   <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="padding: 20px 0px 0px">
						      AutoRenew at Every Cycle: <a class="planautorenewal" onclick="change_planautorenewal()" href="javascript:void(0)"><img src="<?php echo base_url() ?>assets/images/on2.png" rel="disable"></a>
						   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <div class="row hide" id="plan_details">
                                                         <?php $this->view('users/Plan'); ?>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:25px">
                                                   <div class="row">
                                                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                         <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block" value="SAVE" >
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                       <div class="row mui--text-right">
                                          <p><strong class="customer_name">Praveer Kochhar (123456789012)</strong></p>
                                          <p><small class="customer_email">praveer.k@gmail.com</small></p>
                                          <p><small class="customer_mobile">+91 9876543210</small></p>
                                          <br/>
                                       </div>
                                    </div>
                                 </form>
                              </div>
			      <div class="mui-tabs__pane" id="Topup">
				 <div class="col-lg-12 col-md-12 col-sm-12">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12">
					  <div class="row">
					     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-left">
						<h2>TOPUP</h2>
					     </div>
					     <!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 onauthorized">
						<button class="mui-btn mui-btn--small mui-btn--accent pull-right" id="add_datatopup">
						TOPUP
						</button>
					     </div>-->
					  </div>
				       </div>
				    </div>
				    <div class="row unauthorized hide" >
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <h3>User may be INACTIVE or No plan associated with this user</h3>
				       </div>
				    </div>
				       
				    <div class="onauthorized">
				       <div class="row">
					  <div class="mui-panel">
					     <div class="row">
						<form action="" method="post" id="addtopup_form" autocomplete="off" onsubmit="assingtopup_touser(); return false;">
						   <div class="form-group">
						      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:10px ">
							 <label class="radio-inline">
							 <input type="radio" name="topuptype" value="1" checked> Data Top up
							 </label>
							 <label class="radio-inline">
							 <input type="radio" name="topuptype" value="3"> Speed Top up
							 </label>
							 <label class="radio-inline">
							 <input type="radio" name="topuptype" value="2"> Data Unaccountancy Top up
							 </label>
						      </div>
						   </div>
						   <div class="form-group">
						      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							 <div class="row">
							    <div class="col-sm-3 col-xs-3">
							       <div class="mui-select">
								  <select name="active_topups" onchange="topup_pricing(this.value)" required>
								  </select>
								  <label>Active Topups<sup>*</sup></label>
							       </div>
							    </div>
							    <div class="col-sm-7 col-xs-7 hide" id="topuptimer">
							       <div class="row">
								  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
								     <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
									<div class="input-group" style="margin-top:-5px">
									   <div class="input-group-addon" style="padding:6px 10px">
									      <i class="fa fa-calendar"></i>
									   </div>
									   <input type="text" class="form-control top_startdate topup_validate"  name="topup_startdate" placeholder="TopUp Start Date*" >
									</div>
								     </div>
								  </div>
								  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
								     <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
									<div class="input-group" style="margin-top:-5px">
									   <div class="input-group-addon" style="padding:6px 10px">
									      <i class="fa fa-calendar"></i>
									   </div>
									   <input type="text" class="form-control top_enddate topup_validate" name="topup_enddate" placeholder="TopUp End Date*"  >
									</div>
								     </div>
								  </div>
							       </div>
							    </div>
							 </div>
						      </div>
						   </div>
						   <div class="form-group">
						      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							 <input type="hidden" id="topup_actualcost" />
							 <input type="hidden" name="topup_totalcost" value="" />
							 <input type="hidden" id="topup_dayscount" name="topup_dayscount" value="0" />
							 Daily TopUp Cost: <span id="topup_price"></span> &nbsp;&nbsp;&nbsp;
							 Total Cost: <span id="topup_totalcost"></span>
						      </div>
						   </div>
						   <div class="form-group">
						      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							 <input type="submit" class="mui-btn mui-btn--small mui-btn--accent" value="Apply TopUp" />
						      </div>
						      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 hide" id="topup_err" style="color:#f00;"></div>
						   </div>
						</form>
					     </div>
					  </div>
				       </div>
				       <div class="row" >
					  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					     <div class="table-responsive">
						<table class="table table-striped">
						   <thead>
						      <tr class="active">
							 <th>&nbsp;</th>
							 <th>APPLIED ON</th>
							 <th>TOPUP TYPE</th>
							 <th>TOPUP NAME</th>
							 <th>TOPUP PRICE</th>
							 <th>TOPUP START DATE</th>
							 <th>TOPUP END DATE</th>
							 <th>STATUS</th>
							 <th>ACTIONS</th>
						      </tr>
						   </thead>
						   <tbody id="userassoc_topuplist"></tbody>
						</table>
					     </div>
					  </div>
				       </div>
				    </div>
				 </div>
			      </div>
                              <div class="mui-tabs__pane" id="Billing">
				 <div class="row">
				    <div class="col-sm-12 col-xs-12">
				       <ul class="mui-tabs__bar">
					  <li class="mui--is-active">
					     <a data-mui-toggle="tab" data-mui-controls="setup_billing_pane">Setup Billing</a>
					  </li>
					  <li>
					     <a data-mui-toggle="tab" data-mui-controls="active_billing_pane">Monthly Billing</a>
					  </li>
					  <!--<li>
					     <a data-mui-toggle="tab" data-mui-controls="custom_billing_pane" id="click_custombillingtab">Custom Billing</a>
					  </li>-->
				       </ul>
				    </div>
				    <div class="col-sm-12 col-xs-12">
				       <div class="mui-tabs__pane" id="active_billing_pane">
					  <div class="row">
					     <div class="col-sm-12 col-xs-12">
						<h2>&nbsp;</h2>
						<div id="inactive_billing"  class="hide">
						   No Active Plan Associated With This User.
						</div>
						<div id="active_billing" class="hide">
						   <h2 style="font-size:15px; margin:15px 0px;">
						   Wallet Balance: <span id="user_wallet_amt"></span> |
						   Wallet Credit Limit: <span id="wallet_credit_limit"><?php echo $ispcodet['currency'] ?> 0.00</span> |
						   Blocked Balance: <span id="plan_blocked_amt"><?php echo $ispcodet['currency'] ?> 0.00</span>&nbsp;<a href="javascript:void(0)" id="_advanced_prepaydetails" onclick="showadvancedpayment_details()" title="Advanced Payment Details"><i class="fa fa-info-circle" aria-hidden="true"></i></a> &nbsp; <a href="javascript:void(0)" id="_advance_prepayModal">Advance Prepay</a>
						   | <a href="javascript:void(0)" id="_addtowallet">Add to Wallet</a>
						   </h2>
						   <input type="hidden" id="net_walletamt" />
						   
						   <div class="row" style="clear:both">
						      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							 <div class="row">
							    <div class="col-sm-6 col-xs-6">
							       <div class="table-responsive">
								  <table class="table table-striped">
								     <thead>
									<tr class="active">
									   <th>&nbsp;</th>
									   <th>
									      <div class="checkbox" style="margin-top:0px; margin-bottom:0px;"><label><input type="checkbox" class="collapse_allbillcheckbox"></label></div>
									   </th>
									   <th>BillNo.</th>
									   <th>BillType</th>
									   <th>Amount</th>
									   <th>DateAdded</th>
									   <th colspan="3">Action</th>
									</tr>
								     </thead>
								     <tbody id="debitbill_summary"></tbody>
								  </table>
							       </div>
							       <center>
								  <input type='hidden' id="bill_limit" value="" />
								  <input type='hidden' id="bill_offset" value="" />
								  <div class="bill_loadmore hide">
								     <span style="padding:5px; border:1px solid; cursor:pointer" onclick="loadmore_billing('user','loadmore_content','customerbilling','')">Load More</span>
								  </div>
								  <div class="bill_loadmore_loader hide">
								     <img src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
								  </div>
							       </center>
							    </div>
							    <div class="col-sm-6 col-xs-6 nopadding-left">
							       <div style="font-size:15px; text-align:right;">
								  <span id="totalwallet_amt"></span>
							       </div>
							       <div class="table-responsive">
								  <table class="table table-striped">
								     <thead>
									<tr class="active">
									   <th>&nbsp;</th>
									   <th>
									      <!--<div class="checkbox" style="margin-top:0px; margin-bottom:0px;"><label><input type="checkbox" class="collapse_allrcptcheckbox"></label></div>-->
									      &nbsp;
									   </th>
									   <th>RcptNo</th>
									   <th>Amount</th>
									   <th>PayMethod</th>
									   <th>RcptDate</th>
									   <th colspan="2">Action</th>
									</tr>
								     </thead>
								     <tbody id="creditbill_summary"></tbody>
								  </table>
							       </div>
							       <center>
								  <input type='hidden' id="rcptbill_limit" value="" />
								  <input type='hidden' id="rcptbill_offset" value="" />
								  <div class="rcptbill_loadmore hide">
								     <span style="padding:5px; border:1px solid; cursor:pointer" onclick="loadmore_billing('user','loadmore_content','customerbilling','')">Load More</span>
								  </div>
								  <div class="rcptbill_loadmore_loader hide">
								     <img src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
								  </div>
							       </center>
							    </div>
							 </div>
						      </div>
						   </div>
						</div>
					     </div>
					  </div>
				       </div>
				       <div class="mui-tabs__pane mui--is-active" id="setup_billing_pane">
					  <div class="row" style="clear:both">
					     <div class="billing_list" style="margin:5px 0px">
						<ul style="float:right">
						   <li style="margin:0px 5px"><button class="mui-btn mui-btn--small mui-btn--accent diag_readable" style="height:36px; line-height:35px; font-weight:600" id="_installationModal">Add Installation Cost</button></li>
						   <li style="margin:0px 5px"><button class="mui-btn mui-btn--small mui-btn--accent diag_readable" style="height:36px; line-height:35px; font-weight:600" id="_securityModal">Add Security Cost</button></li>
						</ul>
					     </div>
					     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="table-responsive">
						   <table class="table table-striped">
						      <thead>
							 <tr class="active">
							    <th colspan="7" style="color: #f1f1f1">Installation & Security Charges</th>
							 </tr>
						      </thead>
						      <tbody id="instsecu_bill_summary"></tbody>
						   </table>
						</div>
					     </div>
					  </div>
				       </div>
				    </div>
				 </div>
                                 
                              </div>
			      <div class="mui-tabs__pane" id="Settings">
				 <h2>SETTINGS</h2>
                                 <form action="" method="post" id="syssetting_form" autocomplete="off" onsubmit="system_setting(); return false;">
				    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
				    <div class="form-group">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-bottom:15px">
					  <h4>LOGIN TYPE</h4>
					  <div class="row" style="margin-top:5px;">
					     <span style="padding-left:15px">
						<input name="logintype" class="logintype" value="pppoe" type="radio" checked required><span class="circle"></span><span class="check"></span> PPPOE &nbsp; &nbsp;
						<input name="logintype" class="logintype" value="hotspot" type="radio" required><span class="circle"></span><span class="check"></span> HOTSPOT &nbsp; &nbsp;
                                                <input name="logintype" class="logintype" value="ill_ipuser" type="radio" required><span class="circle"></span><span class="check"></span> IP User
					     </span>
					  </div>
					  <div class="row nomargin-left addhotspotMacID hide">
					     <input type="hidden" name="radchk_clearid" id="radchk_clearid" value="" />
					     <input type="hidden" name="radchk_simuseid" id="radchk_simuseid" value="" />
					     <input type="text" name="hotspotMacID" id="hotspotMacID" />
					     <label id="hotspotMacID_err" style="color:#f00"></label>
					  </div>
				       </div>
				    </div>
				    <div class="form-group ill_userpanel hide">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-bottom:15px">
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="mui-select">
                                                   <select name="nas_illoptions" required>
                                                      <option value="">Select Nas</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="mui-radio">
                                                   <label class="nopadding">IP Address Type <sup>*</sup> </label> <br/>
                                                   <input type="radio" name="ill_iptype" value="public" checked required> Public &nbsp;
                                                   <input type="radio" name="ill_iptype" value="private" required> Private
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="">
                                                   <select class="form-control" name="nas_iprange[]" multiple required>
                                                      <option value="">Select Available IP</option>
                                                   </select>
                                                   <!--<select class="form-control" name="nas_iprange" required>
                                                      <option value="">Select Available IP</option>
                                                   </select>-->
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
				    <div class="form-group not_ill_userpanel">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
					  <h4>IP ADDRESS TYPE</h4>
					  <div class="row nomargin-left">
					     <label>
					     <input name="ipaddrtype" class="ipaddrtype" value="0" type="radio" required><span class="circle"></span><span class="check"></span> Static IP
					     </label>
					     <label style="padding-left:30px">
					     <input name="ipaddrtype" class="ipaddrtype" value="1" type="radio" required><span class="circle"></span><span class="check"></span> Dynamic IP
					     </label>
					     <label style="padding-left:30px">
					     <input name="ipaddrtype" class="ipaddrtype" value="2" type="radio" required><span class="circle"></span><span class="check"></span> IP Pool
					     </label>
					  </div>
					  <div class="row nomargin-left addstaticip hide">
					     <input type="text" name="ipaddress" id="ipaddress" />
					     <label id="staticipaddr_err" style="color:#f00"></label>
					  </div>
					  <div class="row addippoolip hide">
					     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<strong>Available IPs:</strong> <span id="total_poolips"></span> <br/>
						<strong>Assigned IPAddress:</strong> <span id="poolip_assigned"></span>
						<div class="mui-select">
						   <select name="ipaddress_pool" id="ipaddress_pool" >
						      <option value="">Select IP Pool</option>
						   </select>
						</div>
						<label id="poolipaddr_err" style="color:#f00"></label>
					     </div>
					  </div>
				       </div>
				    </div>
				   
				    <div class="form-group">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-bottom:15px">
					  <h4>MAC ID BINDING</h4>
					  <div class="row nomargin-left">
					     <strong>Current MAC ID: </strong>
					     <label id="curr_macid"><?php echo $mac; ?></label>
					  </div>
					  <div class="row nomargin-left" style="margin-top:5px;">
					     <strong>Bind Mac: </strong>
					     <span style="padding-left:30px">
						<input name="bindmaclock" value="1" type="radio" required><span class="circle"></span><span class="check"></span> Yes &nbsp; &nbsp;
						<input name="bindmaclock" value="0" type="radio" required><span class="circle"></span><span class="check"></span> No
					     </span>
					     <a href="javascript:void(0)" style="padding-left:30px" data-toggle="modal" data-target="#resetMacModal" >Reset Mac</a>
					  </div>
				       </div>
				    </div>
				    
				    <div class="form-group">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <div class="row">
					     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block" value="SAVE" />
					     </div>
					  </div>
				       </div>
				    </div>
                                 </form>
                              </div>
			      <div class="mui-tabs__pane" id="Requests">
                                 <!--<h2 class="isticketperm">
                                    <a href="javascript:void(0)" id="create_newticket">Create New Service Request</a>
                                 </h2>-->
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                       <h2>Service Requests</h2>
                                    </div>
                                 </div>
				 <div class="col-sm-12 col-xs-12">
				    <ul class="mui-tabs__bar">
				       <li class="mui--is-active" >
					  <a data-mui-toggle="tab" data-mui-controls="opentickets_pane" onclick="fetch_allopenticket()">Open Tickets</a>
				       </li>
				       <li>
					  <a data-mui-toggle="tab" id="ticketclosedli" data-mui-controls="closetickets_pane">Tickets History</a>
				       </li>
				       <li>
					  <a data-mui-toggle="tab" id="tickettypeli" data-mui-controls="addtickettype_pane">Tickets Types</a>
				       </li>
				       <li>
					  <a data-mui-toggle="tab" id="create_newticket" data-mui-controls="addnewtickets_pane">Add New Ticket</a>
				       </li>
				    </ul>
				 </div>
				 <div class="mui-tabs__pane mui--is-active" id="opentickets_pane">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:50px 0px;">
					  <div class="table-responsive">
					     <table class="table table-striped">
						<thead>
						   <tr class="active">
						      <th>&nbsp;</th>
						      <th>REQUEST ID</th>
						      <th>DATE</th>
						      <th>TYPE</th>
						      <th colspan="2">STATUS</th>
						      <th style="text-align:center">TAT DAYS</th>
						      <th>PRIORITY</th>
						      <th>ACTIONS</th>
						      <th>CREATED BY</th>
						      <th style="text-align:center">ASSIGN TO</th>
						   </tr>
						</thead>
						<tbody id="allticketdiv"></tbody>
					     </table>
					  </div>
				       </div>
				    </div>
				 </div>
				 <div class="mui-tabs__pane" id="closetickets_pane">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:50px 0px;">
					  <div class="table-responsive">
					     <table class="table table-striped">
						<thead>
						   <tr class="active">
						      <th>&nbsp;</th>
						      <th>REQUEST ID</th>
						      <th>DATE</th>
						      <th>TYPE</th>
						      <th colspan="2">STATUS</th>
						      <th style="text-align:center">TAT DAYS</th>
						      <th>PRIORITY</th>
						      <th>CREATED BY</th>
						      <th style="text-align:center">ASSIGN TO</th>
						      <th>CLOSED ON</th>
						      <th>ACTIONS</th>
						   </tr>
						</thead>
						<tbody id="allclosedticketdiv"></tbody>
					     </table>
					  </div>
				       </div>
				    </div>
				 </div>
				 <div class="mui-tabs__pane" id="addtickettype_pane">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:50px 0px;">
					  <form action="" method="post" id="ticket_types_form" autocomplete="off" onsubmit="add_ticket_types(); return false;">
					     <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
					     <input type="hidden" name="tickettypeid" value=''>
					     <div class="col-sm-5 col-xs-5">
						<div class="row" style="margin-bottom:20px;">
						   <div class="mui-textfield">
						      <input type="text" name="tickettype_name" value="" required>
						      <label>Ticket Type <sup>*</sup></label>
						   </div>
						</div>
						<input type="submit" class="mui-btn mui-btn--primary mui-btn--flat" style="background-color:#f00f64; color:#fff" value="SUBMIT" >
					     </div>
					  </form>
					  <div class="col-sm-5 col-xs-5" style="border-left:1px solid #f1f1f1">
					     <div class="table-responsive">
						<table class="table table-striped">
						   <thead>
						      <tr class="active">
							 <th>&nbsp;</th>
							 <th>TYPE</th>
							 <th colspan="2" style="text-align:center">ACTIONS</th>
						      </tr>
						   </thead>
						   <tbody id="alltickettypesdiv"></tbody>
						</table>
					     </div>
					  </div>
				       </div>
				    </div>
				 </div>
				 <div class="mui-tabs__pane" id="addnewtickets_pane">
				    <div class="row">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:50px 0px;">
					  <form action="" method="post" id="ticket_request_form" autocomplete="off" onsubmit="add_ticket_request(); return false;">
					     <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
					     <div class="col-sm-7 col-xs-7">
						<div class="row" style="margin-bottom:20px;">
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						      <div class="mui-textfield">
							 <input type="text" name="ticketid" value="" readonly="readonly"  required>
							 <label>Ticket ID <sup>*</sup></label>
						      </div>
						   </div>
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						      <div class="mui-textfield" id="tktdtme">
							 <label>Ticket DateTime<sup>*</sup></label>
						      </div>
						   </div>
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						      <div class="mui-textfield">
							 <input type="text" name="customer_uuid" value="" readonly="readonly" required>
							 <label>Customer ID <sup>*</sup></label>
						      </div>
						   </div>
						</div>
						<div class="row">
						   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						      <div class="mui-select">
							 <select name="ticket_type" required>
							    <?php //echo $this->user_model->ticketstype_optionslist(); ?>
							 </select>
							 <label>Ticket Types <sup>*</sup></label>
						      </div>
						   </div>
						   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						      <div class="mui-select">
							 <select name="ticket_priority" required>
							    <option value="">Select Ticket Priority</option>
							    <option value="low">Low</option>
							    <option value="medium">Medium</option>
							    <option value="high">High</option>
							 </select>
							 <label>Priority<sup>*</sup></label>
						      </div>
						   </div>
						</div>
						<div class="row">
						   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						      <div class="mui-select">
							<select name="ticket_assignto">
							    <option value="">Assign Ticket</option>
							 </select>
							 <label>Assign Ticket<sup></sup></label>
						      </div>
						   </div>
						   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						      <div class="mui-textfield">
							 <textarea style="resize: vertical;" rows="4" name="ticket_description" required></textarea>
							 <label>Ticket Description<sup>*</sup></label>
						      </div>
						   </div>
						</div>
						<div class="row">
						   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						      <div class="mui-textfield mui-textfield--float-label">
							 <input type="text" name="ticket_comment">
							 <label>Comments (If any)</label>
						      </div>
						   </div>
						</div>
						<button type="button" class="mui-btn mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
						<input type="submit" class="mui-btn mui-btn--primary mui-btn--flat" style="background-color:#f00f64; color:#fff" value="ADD" >
					     </div>
					  </form>
				       </div>
				    </div>
				 </div>
			      </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /Start your project here-->

      <div class="modal fade" tabindex="-1" role="dialog" id="installationModal" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="installation_billing_form" autocomplete="off" onsubmit="installation_charges(); return false;">
	    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <input type="hidden" name="connection_type" class="connection_type" value="" />
            <input type="hidden" name="install_bill_transaction_type" value="installation" />
	    <input type="hidden" name="installation_billid" value="" />
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <h4 class="modal-title">
			<strong>INSTALLATION BILLING</strong>
		     </h4>
		  </div>
		  
		  <div class="modal-body">
		     <div class="row" style="margin-bottom:10px">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <input type="radio" name="installcharges_applicable" value="waivedoff" required /> Waived Off &nbsp; &nbsp;
			   <input type="radio" name="installcharges_applicable" value="applycharges" required /> Chargeable
			</div>
		     </div>
		     <div class="visible_oncharge">
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield" id="install_billdtme">
				 <label>Bill DateTime<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input type="text" name="install_bill_number" id="install_bill_number" readonly>
				 <label>Bill Number<sup>*</sup></label>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-select">
				 <select name="install_bill_payment_mode" required>
				    <option value="">Select Payment Mode</option>
				    <option value="cash">Cash</option>
				    <option value="cheque">Cheque</option>
				    <option value="dd">Demand Draft</option>
				 </select>
				 <label>Mode of Payment<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield" style="padding-top: 15px">
				 <input type="text" name="install_chqddrecpt">
				 <label>Cheque/DD/Receipt Number</label>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield" style="padding-top: 25px">
				 <input type="text" name="install_bill_amount" data-billtype="installation" class="bill_amount" id="install_bill_amount" required>
				 <label>Amount Received<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="">
				 <label style="display:block;">Discount (If Any)</label>
				 <input type="radio" name="install_discounttype" value="percent" checked/> Percent Discount &nbsp;&nbsp;
				 <input type="radio" name="install_discounttype" value="flat" /> Flat Discount
				 <select name="install_bill_percentdiscount" class="form-control" style="font-size:12px">
				    <option value="0" selected>Select Discount %</option>
				    <?php
				    for($d=1; $d<=100; $d++){
				       echo '<option value="'.$d.'">'.$d.'%</option>';
				    }
				    ?>
				 </select>
				 <input type="number" name="install_bill_flatdiscount" min="0" value="0" class="form-control hide"  style="font-size:12px"/>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-radio">
				 <label class="nopadding">Include Tax </label> <br/>
				 <input type="radio" name="install_bill_withtax" value="1" checked> Yes &nbsp;&nbsp;
				 <input type="radio" name="install_bill_withtax" value="0"> No
			      </div>
			   </div>
			   <input type="hidden" name="install_send_copy_to_customer" value="0" >
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-radio">
				 <label class="nopadding">Payment Received </label> <br/>
				 <input type="radio" name="install_bill_payment_received" value="1"> Yes &nbsp;&nbsp;
				 <input type="radio" name="install_bill_payment_received" value="0" checked> No
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input type="text" name="install_bill_total_amount" class="bill_total_amount" readonly="readonly" required >
				 <label>Total Amount</label>
			      </div>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <textarea name="install_bill_comments"></textarea>
			      <label>Comments</label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <textarea name="install_bill_remarks"></textarea>
			      <label>Remarks</label>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="formsubmit mui-btn mui-btn--large mui-btn--accent" value="Save" />
                     <img class="formloader hide" src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
		  </div>
	       </div>
	       <!-- /.modal-content -->
	    </div>
	 </form>
      </div>
      
      <div class="modal fade" tabindex="-1" role="dialog" id="securityModal" data-backdrop="static" data-keyboard="false">
         <form action="" method="post" id="security_billing_form" autocomplete="off" onsubmit="security_charges(); return false;">
	    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <input type="hidden" name="connection_type" class="connection_type" value="" />
            <input type="hidden" name="security_bill_transaction_type" value="security" />
	    <input type="hidden" name="security_billid" value="" />
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <h4 class="modal-title">
			<strong>SECURITY BILLING</strong>
		     </h4>
		  </div>
		  <div class="modal-body">
		     <div class="row" style="margin-bottom:10px">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <input type="radio" name="securitycharges_applicable" value="waivedoff" required /> Waived Off &nbsp; &nbsp;
			   <input type="radio" name="securitycharges_applicable" value="applycharges" required /> Chargeable
			</div>
		     </div>
		     <div class="security_visible_oncharge">
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield" id="security_billdtme">
				 <label>Bill DateTime<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input type="text" name="security_receipt_number" id="security_receipt_number">
				 <label>Cheque/DD/Receipt Number<sup></sup></label>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-select">
				 <select name="security_bill_transaction_type" required>
				    <option value="">Select Transaction Type</option>
				    <option value="security" selected>Security Cost</option>
				 </select>
				 <label>Transaction Type<sup>*</sup></label>
			      </div>
			   </div>-->
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-select">
				 <select name="security_bill_payment_mode" required>
				    <option value="">Select Payment Mode</option>
				    <option value="cash">Cash</option>
				    <option value="cheque">Cheque</option>
				    <option value="dd">Demand Draft</option>
				 </select>
				 <label>Mode of Payment<sup>*</sup></label>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield mui-textfield--float-label" style="padding-top: 25px">
				 <input type="text" name="security_bill_amount" data-billtype="security" class="bill_amount" id="security_bill_amount" required>
				 <label>Amount Received<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="">
				 <label style="display:block;">Discount (If Any)</label>
				 <input type="radio" name="security_discounttype" value="percent" checked/> Percent Discount &nbsp;&nbsp;
				 <input type="radio" name="security_discounttype" value="flat" /> Flat Discount
				 <select name="security_bill_percentdiscount" class="form-control" style="font-size:12px">
				    <option value="0" selected>Select Discount %</option>
				    <?php
				    for($d=1; $d<=100; $d++){
				       echo '<option value="'.$d.'">'.$d.'%</option>';
				    }
				    ?>
				 </select>
				 <input type="number" name="security_bill_flatdiscount" min="0" value="0" class="form-control hide"  style="font-size:12px"/>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-radio">
				 <label class="nopadding">Include Tax </label> <br/>
				 <input type="radio" name="security_bill_withtax" value="1" checked> Yes &nbsp;&nbsp;
				 <input type="radio" name="security_bill_withtax" value="0"> No
			      </div>
			   </div>
			   <input type="hidden" name="security_send_copy_to_customer" value="0">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-radio">
				 <label class="nopadding">Payment Received </label> <br/>
				 <input type="radio" name="security_bill_payment_received" value="1"> Yes &nbsp;&nbsp;
				 <input type="radio" name="security_bill_payment_received" value="0" checked> No
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input type="text" name="security_bill_total_amount" class="bill_total_amount" readonly="readonly" required>
				 <label>Total Amount</label>
			      </div>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <textarea name="security_bill_comments"></textarea>
			      <label>Comments</label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <textarea name="security_bill_remarks"></textarea>
			      <label>Remarks</label>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="formsubmit mui-btn mui-btn--large mui-btn--accent" value="Save" />
                     <img class="formloader hide" src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
		  </div>
	       </div>
	       <!-- /.modal-content -->
	    </div>
	 </form>
      </div>
      
      <div class="modal fade" tabindex="-1" role="dialog" id="advance_prepayModal" data-backdrop="static" data-keyboard="false" style="z-index:9999999">
         <form action="" method="post" id="advancedpay_billing_form" autocomplete="off" onsubmit="advance_prepaid(); return false;">
	    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
            <input type="hidden" name="advprepay_bill_transaction_type" value="advprepay" />
	    <input type="hidden" name="connection_type" class="connection_type" value="" />
            <input type="hidden" name="plan_monthly_cost" id="plan_monthly_cost" value="" />
	    <input type="hidden" name="userassoc_planid" id="userassoc_planid" value="" />
	    <input type="hidden" name="prevadvpayid" id="prevadvpayid" value="" />
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <h4 class="modal-title">
			<strong>ADD ADVANCED PREPAY BILLING</strong>
		     </h4>
		  </div>
		  <div class="modal-body">
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield" id="advprepay_billdtme">
			      <label>Bill DateTime<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="advprepay_bill_number" id="advprepay_bill_number" readonly>
			      <label>Bill Number<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="advprepay_bill_payment_mode" required>
				 <option value="">Select Payment Mode</option>
				 <option value="cash">Cash</option>
				 <option value="cheque">Cheque</option>
				 <option value="dd">Demand Draft</option>
				 <option value="viapaytm">Paytm</option>
				 <option value="netbanking">NetBanking</option>
			      </select>
			      <label>Mode of Payment<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield" style="padding-top: 15px">
			      <input type="text" name="advprepay_chqddrecpt" required>
			      <label>Cheque/DD/Paytm No./TranscID/Rcpt No.<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="">
                              <label>Select Number Of Month</label>
			      <select name="advprepay_month_count" class="form-control advprepay_month_count" required>
				 <option value="">Select Number Of Month</option>
				 <?php
				 for($d=1; $d<=12; $d++){
				    echo '<option value="'.$d.'">'.$d.'</option>';
				 }
				 ?>
			      </select>
			      
			   </div>
			</div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="advprepay_freemonth_count">
				 <option value="0" selected>Select Number Of Free Month</option>
				 <?php
				 for($d=1; $d<=12; $d++){
				    echo '<option value="'.$d.'">'.$d.'</option>';
				 }
				 ?>
			      </select>
			      <label>Select Number Of Free Month</label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="advprepay_bill_amount" readonly="readonly" required>
			      <label>Calculated Amount<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="">
			      <label style="display:block;">Discount (If Any)</label>
			      <input type="radio" name="advprepay_discounttype" value="percent" checked/> Percent Discount &nbsp;&nbsp;
			      <input type="radio" name="advprepay_discounttype" value="flat" /> Flat Discount
			      <select name="advprepay_bill_percentdiscount" class="form-control" style="font-size:12px">
				 <option value="0" selected>Select Discount %</option>
				 <?php
				 for($d=1; $d<=100; $d++){
				    echo '<option value="'.$d.'">'.$d.'%</option>';
				 }
				 ?>
			      </select>
			      <input type="number" name="advprepay_bill_flatdiscount" min="0" value="0" class="form-control hide"  style="font-size:12px"/>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="advprepay_bill_total_amount" readonly="readonly" required>
			      <label>Total Amount</label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-radio">
			      <label class="nopadding">Send Copy To Customer</label> <br/>
			      <input type="radio" name="advprepay_send_copy_to_customer" value="1" checked> Yes &nbsp;&nbsp;
			      <input type="radio" name="advprepay_send_copy_to_customer" value="0"> No
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
                     <span class="adverr hide" style="color:#FF4081">You can't choose Free Months & Discount both at the same time.</span> <br/>
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="SAVE" />
		  </div>
	       </div>
	       <!-- /.modal-content -->
	    </div>
	 </form>
      </div>
      
      <div class="modal fade" id="delete_advpaymentrcptModal" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:9999999">
         <div class="modal-dialog modal-sm">
            <form action="" id="deladvpayform" method="post" autocomplete="off" onsubmit="delete_advprepaid_rcpt(); return false;">
	       <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	       <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	       <input type="hidden" name="advprepayid" value=''>
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title">
			<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
		     </h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <p>How do you want to manage this receipt amount ?</p>
		     <div class="row" style="margin-bottom:10px">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <input type="radio" name="advprepay_addtowallet" value="1" required /> Delete & Add to Wallet
			   <br/>
			   <input type="radio" name="advprepay_addtowallet" value="0" required /> Delete Only
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--large mui-btn--accent del_advpaybtn" value="Submit" />
		  </div>
	       </div>
	    </form>
         </div>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="addtowalletModal" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="addtowallet_billing_form" autocomplete="off" onsubmit="addtowallet(); return false;">
	    <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <input type="hidden" name="addtowallet_bill_transaction_type" value="addtowallet" />
	    <input type="hidden" name="walletid_foredit"  value="" />
	    <input type="hidden" name="walletbillid_foredit" value="" />
	    
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <h4 class="modal-title">
			<strong>ADD TO WALLET</strong>
		     </h4>
		  </div>
		  
		  <div class="modal-body">
		     <div class="row">
			<!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield" id="addtowallet_billdtme">
			      <label>Bill DateTime<sup>*</sup></label>
			   </div>
			</div>-->
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
			      <div class="input-group" style="margin-top:-5px">
				 <div class="input-group-addon" style="padding:6px 10px">
				    <i class="fa fa-calendar"></i>
				 </div>
				 <input type="text" class="form-control walletamt_receivedate" id="" placeholder="Amount Received On*" name="addtowallet_bill_datetimeformat" required>
				 <input name="addtowallet_bill_datetime" type="hidden" value="">
			      </div>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="addtowallet_receipt_number" id="addtowallet_bill_number" required>
			      <label>Cheque/DD/Receipt Number<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="addtowallet_payment_mode" required>
				 <option value="">Select Payment Mode</option>
				 <option value="cash">Cash</option>
				 <option value="cheque">Cheque</option>
				 <option value="dd">Demand Draft</option>
				 <option value="viapaytm">Paytm</option>
				 <option value="netbanking">NetBanking</option>
			      </select>
			      <label>Mode of Payment<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield mui-textfield--float-label">
			      <input type="text" name="addtowallet_amount" required>
			      <label>Amount Received<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 chqddpaytm hide">
			   <div class="mui-textfield">
			      <input type="text" name="update_cheque_dd_paytm" required="required" pattern="^[0-9]\d*$">
			      <label>Cheque/DD/Paytm No./Transaction ID<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="bill_bankdetails hide">
			<div class="row">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding-left">
				 <div class="mui-textfield">
				    <input type="tel" name="bill_bankaccount_number" maxlength="20">
				    <label>Account Number</label>
				 </div>
			      </div>
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding-left">
				 <div class="mui-textfield">
				    <!--<input type="tel" class="req_cheque_dd" name="bill_bankissued_date">-->
				    <div class="input-group" style="margin-top:-5px">
				       <div class="input-group-addon" style="padding:6px 10px">
					  <i class="fa fa-calendar"></i>
				       </div>
				       <input type="text" class="form-control receiptdate" id="" placeholder="Issue Date" name="bill_bankissued_date" value="">
				    </div>
				    <label>Issued Date</label>
				 </div>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding-left">
				 <div class="mui-textfield">
				    <input type="text" name="bill_bankname">
				    <label>Bank Name</label>
				 </div>
			      </div>
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				 <div class="mui-textfield">
				    <input type="text" name="bill_bankaddress" class="nopadding-left">
				    <label>Bank Address</label>
				 </div>
			      </div>
			   </div>
			</div>
			<!--<div class="row">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				 <div class="mui-textfield">
				    <input type="file" name="bill_slipimage">
				    <label>Slip Image</label>
				 </div>
			      </div>
			   </div>
			</div>-->
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-radio">
			      <label class="nopadding">Amout Received</label> <br/>
			      <input type="radio" name="wallet_amount_received" value="1" required> Confirmed &nbsp;&nbsp;
			      <input type="radio" name="wallet_amount_received" value="0" required> Pending
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-radio">
			      <label class="nopadding">Send Copy To Customer</label> <br/>
			      <input type="radio" name="addtowallet_send_copy_to_customer" value="1" checked> Yes &nbsp;&nbsp;
			      <input type="radio" name="addtowallet_send_copy_to_customer" value="0"> No
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="Submit" />
		  </div>
	       </div>
	       <!-- /.modal-content -->
	    </div>
	 </form>
      </div>
      
      <div class="modal fade" tabindex="-1" role="dialog" id="documentModal" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">
                     <strong>DOCUMENTS PREVIEW</strong>
                  </h4>
               </div>
               <div class="modal-body">
                  <div class="row" id="docpreview"></div>
               </div>
               <div class="modal-footer">&nbsp;</div>
            </div>
         </div>
      </div>
      
   
      <div class="modal fade" tabindex="-1" role="dialog" id="editserviceRequestModal" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog" style="margin-top:4%">
            <form action="" method="post" id="edit_ticket_request_form" autocomplete="off" onsubmit="edit_ticket_request(); return false;">
	       <input type="hidden" name="edcustomer_id" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	       <input type="hidden" name="edtktid" value="" />
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h4 class="modal-title">
                        <strong>EDIT SERVICE REQUEST</strong>
                     </h4>
                  </div>
                  <div class="modal-body">
                     <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-textfield">
                              <input type="text" name="edticketid" value="" readonly  required>
                              <label>Ticket ID <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-textfield" id="edtktdtme">
			      <input name="edticket_datetime" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" readonly required/>
                              <label>Ticket DateTime<sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-textfield">
                              <input type="text" name="edcustomer_uuid" value="" readonly required>
                              <label>Customer ID <sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="edticket_type" required disabled></select>
                              <label>Ticket Types <sup>*</sup></label>
                           </div>
                        </div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield">
                              <textarea style="resize: vertical;" rows="2" name="edticket_description" required readonly></textarea>
                              <label>Ticket Description<sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="edticket_assignto" required disabled>
                                 <option value="">Assign Ticket</option>
                              </select>
                              <label>Assign Ticket<sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="edticket_priority" required>
                                 <option value="">Select Ticket Priority</option>
                                 <option value="low">Low</option>
                                 <option value="medium">Medium</option>
                                 <option value="high">High</option>
                              </select>
                              <label>Priority<sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label">
                              <input type="text" name="edticket_comment">
                              <label>Comments (If any)</label>
                           </div>
                        </div>
                     </div>
		     <div class="row">
			<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			   <div class='mui-textfield mui-textfield--float-label' id="edit_tcomment_list">
			   </div>
			</div>
		     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="mui-btn mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                     <input type="submit" class="mui-btn mui-btn--primary mui-btn--flat" style="background-color:#f00f64; color:#fff" value="SAVE" >
                  </div>
               </div>
            </form>
         </div>
      </div>
      <div class="modal fade" id="assignTicketModal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="assignTicketMember_form" autocomplete="off" onsubmit="updateTicketMember(); return false;">
	    <input type="hidden" name="tktid_toupdate" id="tktid_toupdate" />
            <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title"><strong>ASSIGN TICKET MEMBER</strong></h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="mui-select">
			      <select name="team_member" required>
				 <option value="">Select Team Member</option>
			      </select>
			      <label>Assign Ticket<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="mui-textfield">
			      <textarea style="resize: vertical;" rows="1" name="chngtktstatus_description" required></textarea>
			      <label>Comment<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn mui-btn--small mui-btn--accent" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--small mui-btn--accent" value="Save" />
		  </div>
	       </div>
	    </div>
	 </form>
      </div>
      <div class="modal fade" id="changeticket_statusconfirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="changeticket_statusform" autocomplete="off" onsubmit="changeticket_statusconfirmation(); return false;">
	    <input type="hidden" name="chngtktstatus" value="" />
	    <input type="hidden" name="chngtktstatusid" value="" />
	    <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title">
			<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
		     </h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <p>Are you sure you want to change the ticket status ?</p><br/>
		     <div class="mui-textfield">
			<textarea style="resize: vertical;" rows="2" name="chngtktstatus_description" required></textarea>
			<label>Comment<sup>*</sup></label>
		     </div>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal" onclick="fetch_allopenticket()">CANCEL</button>
		  <input type="submit" class="mui-btn mui-btn--small mui-btn--accent" value="YES" />
		  </div>
	       </div>
	    </div>
	 </form>
      </div>
      <div class="modal fade" id="ticket_closedModal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <input type="hidden" name="tktcloseid" value="">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
		     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CLOSE TICKET ALERT</strong>
		  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>To close the ticket of <strong class="customer_name"></strong>, please enter the OTP sent to regestiered mobile no. <strong class="customer_phone"></strong>
                  </p>
                  <div class="mui-textfield mui-textfield--float-label" style="margin-bottom:0px">
                     <input type="text" name="tktclose_otp" maxlength="4" minlength="4" required>
                     <label>Enter 4-Digit Mobile No.</label>
                  </div>
		  <label id="tktclose_otp_err" style="color:#f00; font-size:12px;"></label>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="verify_confirm_tktclose()">CLOSE TICKET</button>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade" id="ticketsummary_modal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog">
	     <div class="modal-content">
		 <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title">
			 <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>Ticket Summary</strong>
		     </h4>
		 </div>
		 <div class="modal-body" style="padding-bottom:5px">
		     <ul id="tktsummarylist"></ul>
		 </div>
		 <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
		 </div>
	     </div>
	 </div>
      </div> 
      <div class="modal fade" tabindex="-1" role="dialog" id="edit_documentModal" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">
                     <strong>DOCUMENTS PREVIEW</strong>
                  </h4>
               </div>
               <div class="modal-body">
                  <div class="row" id="edit_docpreview"></div>
               </div>
               <div class="modal-footer">&nbsp;</div>
            </div>
         </div>
      </div>
      <div class="modal fade" id="user_activatedModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><strong>ACTIVATE USER?</strong></h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
		  <input type="hidden" id="otp_to_activate_user" value="" />
		  <input type="hidden" id="phone_to_activate_user" value="" />
                  <p>To Activate <span id="activate_username"></span>, please enter the OTP sent to your regestiered mobile no. <span id="activate_userphone"></span>
                  </p>
                  <div class="mui-textfield mui-textfield--float-label" style="margin-bottom:0px">
                     <input type="text" name="enter_otp_to_activate_user" id="enter_otp_to_activate_user">
                     <label>Enter 4-Digit Mobile No.</label>
                  </div>
		  <label id="activate_otp_err"></label>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                  <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="active_user_confirmation()">ACTIVATE</button>
               </div>
            </div>
         </div>
      </div>
      
        <!--user verifaication modal-->
        <div class="modal fade" id="userverifcation_toActiveModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
            <form action="" method="post" autocomplete="off"  return false;">
	       <input type="hidden" name="subscriber_userid" class="subscriber_userid" value='<?php echo $subscriber_id ?>'>
	       <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title">
			<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
		     </h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <p>Do You want to verify this user ?</p>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn  mui-btn--small mui-btn--accent notverifyuser" style="background-color:#4D4D4D" >NO</button>
		     <button type="button" class="mui-btn  mui-btn--small mui-btn--accent verifyuser">YES</button>
		  </div>
	       </div>
	    </form>
         </div>
      </div>
      <div class="modal fade" id="user_inactiveAlert" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm" style="width:500px">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>USER CANNOT BE MADE ACTIVE</strong>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>Please check these details to make User Active</p>
		  <ul style="margin:15px; list-style-type: none">
		     <li><span id="pedactive"></span> &nbsp; Complete Personal Details</li>
		     <li><span id="kycactive"></span> &nbsp; Upload atleast one KYC document</li>
		     <li><span id="planactive"></span> &nbsp; Plan is associated with the user</li>
		     <li><span id="inscactive"></span> &nbsp; Installation & Security Charges paid by the User</li>
		  </ul>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="enterReceiptModal" role="dialog" data-backdrop="static" data-keyboard="false">
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title"><strong>Bill Receipt</strong> (Pending Amount: <span id="billpendingcost"></span>)</h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <div class="row">
			<div class="col-sm-12 col-xs-12">
			   <ul class="mui-tabs__bar">
			      <li class="mui--is-active hideifbillpaid">
				 <a data-mui-toggle="tab" data-mui-controls="add_receipt_pane">Add Receipt</a>
			      </li>
			      <li>
				 <a data-mui-toggle="tab" data-mui-controls="past_receipt_pane" onclick="getbillpastreceipts()">Past Receipts</a>
			      </li>
			   </ul>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="mui-tabs__pane mui--is-active hideifbillpaid" id="add_receipt_pane">
			      <div class="mui--appbar-height"></div>
			      <form action="" method="post" id="receiptupdate_form" autocomplete="off" onsubmit="update_receiptform(); return false;">
				 <input type="hidden" name="billid_toupdate" id="billid_toupdate" />
				 <input type="hidden" name="billtype_toupdate" id="billtype_toupdate" />
				 <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
				 <input type="hidden" id="billpendingamount" value=""/>
				 <div class="row">
				    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px;">
					  <div class="mui-textfield">
					     <input type="tel" name="update_receipt_number" required="required" maxlength="20">
					     <label>Receipt Number<sup>*</sup></label>
					  </div>
				       </div>
				       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px;">
					  <div class="mui-textfield">
					     <input type="tel" id="update_receipt_amount" name="update_receipt_amount" required="required" pattern="^[1-9]\d*$">
					     <label>Receipt Amount<sup>*</sup></label>
					     <span id="receiptamount_errtxt"></span>
					     <input type="hidden" id="receiptamount_err" value="0" />
					  </div>
				       </div>
				    </div>
				 </div>
				 <div class="row">
				    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px;">
					  <div class="mui-select">
					     <select name="update_payment_mode" required>
						<option value="">Select Payment Mode</option>
						<option value="cash">Cash</option>
						<option value="cheque">Cheque</option>
						<option value="dd">Demand Draft</option>
						<option value="viapaytm">via Paytm</option>
						<option value="viawallet">via Wallet</option>
					     </select>
					     <label>Mode of Payment<sup>*</sup></label>
					  </div>
				       </div>
				       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 chqddpaytm hide">
					  <div class="mui-textfield">
					     <input type="text" name="update_cheque_dd_paytm" required="required" pattern="^[1-9]\d*$">
					     <label>Cheque/DD/Paytm Number<sup>*</sup></label>
					  </div>
				       </div>
				    </div>
				    
				    <div class="bill_bankdetails hide">
				       <div class="row">
					  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="mui-textfield">
						   <input type="tel" class="req_cheque_dd" name="bill_bankaccount_number" maxlength="20">
						   <label>Account Number<sup>*</sup></label>
						</div>
					     </div>
					     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="mui-textfield">
						   <!--<input type="tel" class="req_cheque_dd" name="bill_bankissued_date">-->
						   <div class="input-group" style="margin-top:-5px">
						      <div class="input-group-addon" style="padding:6px 10px">
							 <i class="fa fa-calendar"></i>
						      </div>
						      <input type="text" class="form-control req_cheque_dd receiptdate" id="" placeholder="Date of Birth*" name="bill_bankissued_date" value="">
						   </div>
						   <label>Issued Date<sup>*</sup></label>
						</div>
					     </div>
					  </div>
				       </div>
				       <div class="row">
					  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="mui-textfield">
						   <input type="text" class="req_cheque_dd" name="bill_bankname">
						   <label>Bank Name<sup>*</sup></label>
						</div>
					     </div>
					     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="mui-textfield">
						   <input type="text" class="req_cheque_dd" name="bill_bankaddress">
						   <label>Bank Address<sup>*</sup></label>
						</div>
					     </div>
					  </div>
				       </div>
				       <div class="row">
					  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="mui-textfield">
						   <input type="file" name="bill_slipimage">
						   <label>Slip Image</label>
						</div>
					     </div>
					  </div>
				       </div>
				    </div>

				    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:5px">
				       <button type="button" class="receiptbtn mui-btn mui-btn--small mui-btn--accent" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
				       <input type="submit" class="receiptbtn mui-btn mui-btn--small mui-btn--accent" value="Save" />
				    </div>
				    
				 </div>
			      </form>
			   </div>
			   <div class="mui-tabs__pane" id="past_receipt_pane">
			      <div class="row">
				 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="pastbill_listingdiv">
				    
				 </div>
			      </div>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer" style="text-align: right">&nbsp;</div>
	       </div>
	    </div>
      </div>

      <div class="modal fade" id="assignTicketModal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="assignTicketMember_form" autocomplete="off" onsubmit="updateTicketMember(); return false;">
	    <input type="hidden" name="tktid_toupdate" id="tktid_toupdate" />
            <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title"><strong>ASSIGN TICKET MEMBER</strong></h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="mui-select">
			      <select name="team_member" required>
				 <option value="">Select Team Member</option>
			      </select>
			      <label>Assign Ticket<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="mui-btn mui-btn--small mui-btn--accent" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--small mui-btn--accent" value="Save" />
		  </div>
	       </div>
	    </div>
	 </form>
      </div>


      
      <div class="modal fade" id="add_zoneModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" onclick="resetzonevalue()">&times;</button>
                     <h4 class="modal-title"><strong>ADD ZONE</strong></h4>
                 </div>
                 <form action="" method="post" id="addzone_fly" autocomplete="off" onsubmit="add_newzone(); return false;">
                     <div class="modal-body" style="padding-bottom:5px">
                         <p>Add a new zone to your location listing.</p>
                         <div class="mui-textfield mui-textfield--float-label">
                             <input type="text" name="zone_name" class="zone_text" required>
                             <label>Zone Name</label>
                         </div>
                         <input type="hidden" class="state_id" name="state_id" value="">
                          <input type="hidden" class="city_id" name="add_zone_city" value="">
                     </div>
                     <div class="modal-footer" style="text-align: right">
                         <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff " onclick="resetzonevalue()">CANCEL</button>
                           <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="ADD ZONE">
                     </div>
                 </form>
     
             </div>
         </div>
     </div>
     
     
      <div class="modal fade" id="add_cityModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" onclick="resetcityvalue()">&times;</button>
                     <h4 class="modal-title"><strong>ADD CITY</strong></h4>
                 </div>
                 <form action="" method="post" id="addcity_fly" autocomplete="off" onsubmit="add_newcity(); return false;">
                     <div class="modal-body" style="padding-bottom:5px">
                         <p>Add a new City to your location listing.</p>
                         <div class="mui-textfield mui-textfield--float-label">
                             <input type="text" name="city_add" class="city_text" required>
                             <label>City Name</label>
                         </div>
                         <input type="hidden" class="state_id" name="add_city_state" value="">
                       
                     </div>
                     <div class="modal-footer" style="text-align: right">
                         <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff " onclick="resetcityvalue()">CANCEL</button>
                           <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="ADD CITY">
                     </div>
                 </form>
     
             </div>
         </div>
      </div>
      
      <div class="modal fade" id="resetMacModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION RESET MAC</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p class="resetsucctxt">Are you sure you want to reset Mac for this user ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent resetsuccbtn" onclick="resetUserMac()">YES</button>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade" id="alertModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		  <h4 class="modal-title">
		     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>ALERT</strong>
		  </h4>
	       </div>
	       <div class="modal-body" style="padding-bottom:5px">
		  <p id="alertMsgTxt"></p>  
	       </div>
	       <div class="modal-footer" style="text-align: right">
		  <div class="form-group" style="overflow:hidden;">
		     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right">
			      <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
			   </div>
			</div>
		     </div>
		  </div>
	       </div>
	    </div>
         </div>
      </div>
      <div class="modal fade" id="customPlanPriceModal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="customplanprice_form" autocomplete="off" onsubmit="addcustom_planprice(); return false;">
	    <input type="hidden" name="custom_srvid" id="custom_srvid" />
	    <input type="hidden" name="custom_srvid_duration" id="custom_srvid_duration" />
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title"><strong>CUSTOMIZE PLAN PRICE</strong></h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <label>Current Plan Price(pm) :</label>&nbsp;&nbsp;<span id="displaycurr_plan_price"></span>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="mui-textfield">
			      <input type="number" id="custom_plan_price" name="custom_plan_price" min="36" required="required" pattern="^[1-9]\d*$">
			      <label>Custom Plan Price<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="custplanbtn mui-btn mui-btn--small mui-btn--accent" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="custplanbtn mui-btn mui-btn--small mui-btn--accent" value="Save" />
		  </div>
	       </div>
	    </div>
	 </form>
      </div>
      

      <div class="modal fade" id="enquiryFeedbackModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	 <input type="hidden" name="subscriber_userid" class="enq_userid" value='<?php echo $subscriber_id ?>'>
	 <div class="modal-dialog" role="document">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#f00f64; opacity: 1;">
		  <span aria-hidden="true">&times;</span>
		  </button>
		  <center>
		     <h4 class="modal-title" style="font-size:20px; font-weight:600;color:#f00f64">Enquiry Feedback</h4>
		  </center>
	       </div>
	       <div class="modal-body" style="padding-top:0px">
		  <div class="row">
		     <div class="col-sm-12 col-xs-12" style="margin-bottom:20px;">
			<div class="col-sm-8 col-xs-8">
			   <div class="mui-textfield mui-textfield--float-label" style="width:100%">
			      <input type="text" name="feedback_message" id="feedback_message" value="" required="required">
			      <label>Add Message Here</label>
			   </div>
			</div>
			<div class="col-sm-4 col-xs-4">
			   <button class="mui-btn mui-btn--accent" onclick="addenquiryfeedback()">Add Message</button>
			</div>
		     </div>
		     <div class="col-sm-12 col-xs-12">
			<div class="table-responsive" style="height:250px; overflow-x:auto;">
			   <table class="table table-striped">
			      <thead>
				 <tr class="active">
				   <th class="col-sm-2 col-xs-2">DATE</th>
				   <th class="col-sm-2 col-xs-2">TIME</th>
				   <th class="col-sm-3 col-xs-3">USER</th>
				   <th class="col-sm-5 col-xs-5">MESSAGE</th>
				 </tr>
                              </thead>
			      <tbody id="feedback_history">
			      </tbody>
			   </table>
			</div>
		     </div>
		  </div>
	       </div>
	    </div>
	 </div>
      </div>
      <!----------------------------------------------------------------------------------------------------------->
      <!-- CUSTOM BILLING SECTION -->
      <div class="modal fade" id="customBillModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close closeBillModal" aria-label="Close" style="color:#f00f64; opacity: 1;" onclick="closeCustomBillHandler(0)">
		  <span aria-hidden="true">&times;</span>
		  </button>
		  <center>
		     <h4 class="modal-title" style="font-size:20px; font-weight:600;color:#f00f64">Generate Custom Invoice</h4>
		  </center>
	       </div>
	       <div class="modal-body" style="padding-bottom:25px">
		  <div class="row">
		     <div class="col-sm-11 col-xs-11 text-right">
			<label style="font-weight:600"><span>Invoice Number :</span><span class="custom_billnumber"></span></label>
		     </div>
		     <form method="post" action="" id="custombillingform" autocomplete="off" onsubmit="custombill_invoicelist(); return false;">
			<input type="hidden" class="custom_billnumber" name="custom_billnumber" value="" />
			<div class="col-sm-12 col-xs-12">
			   <div class="row">
			      <div class="col-sm-6 col-xs-6">
				 <div class="row">
				    <div class="col-sm-12 col-xs-12">
				       <div class="col-sm-3 col-xs-3">
					  <label style="font-weight:600; font-size: 14px; margin-top: 15px">Company</label>
				       </div>
				       <div class="col-sm-5 col-xs-5">
					  <select class="form-control" name="billcompany_name" id="company_namelist" required>
					     <option value="">Select Company</option>
					  </select>
				       </div>
				       <div class="col-sm-4 col-xs-4">
					  <button type="button" class="btn btn-link" style="color:#27aae1; font-weight:600" onclick="showadd_companyModal()">
					  Add Company</button>
				       </div>
				    </div>
				    <div class="col-sm-12 col-xs-12">
				       <div class="col-sm-3 col-xs-3">
					  <label style="font-weight:600; font-size: 14px; margin-top: 15px" >Topic</label>
				       </div>
				       <div class="col-sm-5 col-xs-5">
					  <select class="form-control" name="billcompany_topics" id="topics_namelist" required onchange="gettopicdetails(this.value)">
					     <option value="">Select Topic</option>
					  </select>
				       </div>
				       <div class="col-sm-4 col-xs-4">
					  <button type="button" class="btn btn-link" style="color:#27aae1;font-weight:600" onclick="showadd_topicModal()">
					  Add Topic</button>
				       </div>
				    </div>
				 </div>
			      </div>
			      <div class="col-sm-6 col-xs-6">
				 <div class="row">
				    <div class="col-sm-12 col-xs-12">
				       <div class="row" style="margin-top:35px">
					  <div class="col-sm-4 col-xs-4">
					     <div class="mui-textfield mui-textfield--float-label">
						<input type="tel" name="billtopic_rates">
						<label>Rate</label>
					     </div>
					  </div>
					  <div class="col-sm-3 col-xs-3">
					     <div class="mui-textfield mui-textfield--float-label">
						<input type="text" name="billtopics_qty" min="1" required pattern="^[1-9]\d*$">
						<label>Qty</label>
					     </div>
					  </div>
					  <div class="col-sm-4 col-xs-4">
					     <input type="submit" class="mui-btn mui-btn--small mui-btn--danger mui-btn--raised" style="margin-top: 20px" value="Add to Invoice" />
					  </div>
				       </div>
				    </div>
				 </div>
			      </div>
			   </div>
			</div>
		     </form>
		     <div class="col-sm-6 col-xs-6">
			<div class="table-responsive" style="height:150px; overflow-x:auto;">
			   <table class="table table-striped" style="margin-bottom:0px">
			      <thead>
				 <tr class="active">
				    <th class="col-sm-4 col-xs-4">Details </th>
				    <th class="col-sm-2 col-xs-2">Rate </th>
				    <th class="col-sm-3 col-xs-3">Qty </th>
				    <th class="col-sm-4 col-xs-4">Total</th>
				 </tr>
			      </thead>
			      <tbody id="custinvoicelist">
				 
			      </tbody>
			   </table>
			</div>
		     </div>
		     <div class="col-sm-12 col-xs-12">
			<button class="mui-btn mui-btn--accent mui-btn--raised pull-right cust_billbtn" onclick="generate_custombilling()">
			Generate Bill</button>
		     </div>
		  </div>
	       </div>
	    </div>
	 </div>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="add_companyModal" data-backdrop="static" data-keyboard="false">
	 <form class="mui-form" action="" method="post" id="addispcompany_form" autocomplete="off" onsubmit="addisp_company(); return false;">
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <center>
			<h4 class="modal-title" style="font-size:20px; font-weight:600;color:#f00f64">Add Company</h4>
		     </center>
		  </div>
		  <div class="modal-body">
		     <div class="row">
			   <div class="row">
			      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
			      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				 <h2 style="font-size: 16px; margin-top:15px">Add Company</h2>
			      </div>
			      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				 <div class="mui-textfield mui-textfield--float-label">
				    <input type="text" name="bill_company_name" value="" required>
				    <label>Add Company</label>
				 </div>
			      </div>
			      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
			   </div>
			   <div class="row">
			      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
			      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				 <h2 style="font-size: 16px; margin-top:15px">Taxes <small>(IGST,SGST,CGST)</small></h2>
			      </div>
			      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				 <div class="mui-textfield mui-textfield--float-label">
				    <input type="text" name="bill_company_tax" value="" min="1" required="required" pattern="^[1-9]\d*$">
				    <label>Taxes (IGST,SGST,CGST) </label>
				 </div>
			      </div>
			      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
			   </div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <center>
			<input type="submit" class="mui-btn mui-btn--accent" value="ADD" />
		     </center>
		  </div>
	       </div>
	    </div>
	 </form>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="add_topicModal" data-backdrop="static" data-keyboard="false">
	 <form class="mui-form" action="" method="post" id="addisptopic_form" autocomplete="off" onsubmit="addisp_topics(); return false;">
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <center>
			<h4 class="modal-title" style="font-size:20px; font-weight:600;color:#f00f64">Add Topic</h4>
		     </center>
		  </div>
		  <div class="modal-body">
		     <div class="row">
			<!--<div class="col-sm-12 col-xs-12 text-right">
			   <label style="font-weight:600"><span>Invoice Number :</span><span>1202030</span></label>
			</div>-->
			<div class="col-sm-12 col-xs-12">
			   <div class="col-sm-12 col-xs-12">
			      <div class="row">
				 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				    <h2 style="font-size: 16px; margin-top:15px">Topic Details </h2>
				 </div>
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="text" name="topic_name" required>
				       <label>Topic Details</label>
				    </div>
				 </div>
			      </div>
			      <div class="row">
				 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				    <h2 style="font-size: 16px; margin-top:20px">Topic Details </h2>
				 </div>
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="row" style="margin-top:25px">
				       <label class="radio-inline">
				       <input name="bill_topictype" value="standard" type="radio" required> Standard 
				       </label>
				       <label class="radio-inline">
				       <input name="bill_topictype" value="dynamic" type="radio" required> Dynamic
				       </label>
				    </div>
				 </div>
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="text" name="topic_rate">
				       <label>Rate</label>
				    </div>
				 </div>
			      </div>
			      <div class="row">
				 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				    <h2 style="font-size: 16px; margin-top:20px">Tax Type</h2>
				 </div>
				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				    <div class="row" style="margin-top:25px">
				       <label class="radio-inline">
				       <input type="radio" name="taxtypes" value="igst" required /> IGST  
				       </label>
				       <label class="radio-inline">
				       <input type="radio" name="taxtypes" value="sgst_cgst" required /> SGST / CGST
				       </label>
				    </div>
				 </div>
			      </div>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <center>
			<input type="submit" class="mui-btn mui-btn--accent" value="Add Topic Details" />
		     </center>
		  </div>
	       </div>
	    </div>
	 </form>
      </div>
      
      <div class="modal fade" id="confirm_cancel_custombill" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CANCEL CONFIRMATION </strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p class="resetsucctxt">Are you sure you want to cancel added custom bills ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" onclick="closeCustomBillHandler(1)">YES</button>
               </div>
            </div>
         </div>
      </div>


      <div class="modal fade" id="editcustomBillModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	 <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" style="color:#f00f64; opacity: 1;">&times;</button>
		  <center>
		     <h4 class="modal-title" style="font-size:20px; font-weight:600;color:#f00f64">Edit Custom Invoice</h4>
		  </center>
	       </div>
	       <div class="modal-body" style="padding-bottom:25px">
		  <div class="row">
		     <div class="col-sm-11 col-xs-11 text-right">
			<label style="font-weight:600"><span>Invoice Number :</span>
			<span class="custom_billnumber_toedit"></span></label>
		     </div>
		     <form method="post" action="" id="custombillingform_toedit" autocomplete="off" onsubmit="custombill_editinvoice(); return false;">			
			<input type="hidden" class="custom_billid_toedit" name="custom_billid_toedit" value="" />
			<input type="hidden" name="custom_billnumber_toedit" value="" />
			<div class="col-sm-12 col-xs-12">
			   <div class="row">
			      <div class="col-sm-6 col-xs-6">
				 <div class="row">
				    <div class="col-sm-12 col-xs-12">
				       <div class="col-sm-3 col-xs-3">
					  <label style="font-weight:600; font-size: 14px; margin-top: 15px">Company</label>
				       </div>
				       <div class="col-sm-5 col-xs-5">
					  <select class="form-control" name="billcompany_name" id="company_namelist_toedit" required>
					     <option value="">Select Company</option>
					  </select>
				       </div>
				       <div class="col-sm-4 col-xs-4">
					  <button type="button" class="btn btn-link" style="color:#27aae1; font-weight:600" onclick="showadd_companyModal()">
					  Add Company</button>
				       </div>
				    </div>
				    <div class="col-sm-12 col-xs-12">
				       <div class="col-sm-3 col-xs-3">
					  <label style="font-weight:600; font-size: 14px; margin-top: 15px" >Topic</label>
				       </div>
				       <div class="col-sm-5 col-xs-5">
					  <select class="form-control" name="billcompany_topics" id="topics_namelist_toedit" required onchange="gettopicdetails(this.value)">
					     <option value="">Select Topic</option>
					  </select>
				       </div>
				       <div class="col-sm-4 col-xs-4">
					  <button type="button" class="btn btn-link" style="color:#27aae1;font-weight:600" onclick="showadd_topicModal()">
					  Add Topic</button>
				       </div>
				    </div>
				 </div>
			      </div>
			      <div class="col-sm-6 col-xs-6">
				 <div class="row">
				    <div class="col-sm-12 col-xs-12">
				       <div class="row" style="margin-top:35px">
					  <div class="col-sm-4 col-xs-4">
					     <div class="mui-textfield mui-textfield--float-label">
						<input type="tel" name="billtopic_rates">
						<label>Rate</label>
					     </div>
					  </div>
					  <div class="col-sm-3 col-xs-3">
					     <div class="mui-textfield mui-textfield--float-label">
						<input type="text" name="billtopics_qty" min="1" required pattern="^[1-9]\d*$">
						<label>Qty</label>
					     </div>
					  </div>
					  <div class="col-sm-4 col-xs-4">
					     <input type="submit" class="mui-btn mui-btn--small mui-btn--danger mui-btn--raised" style="margin-top: 20px" value="Add to Invoice" />
					  </div>
				       </div>
				    </div>
				 </div>
			      </div>
			   </div>
			</div>
		     </form>
		     <div class="col-sm-6 col-xs-6">
			<div class="table-responsive" style="height:150px; overflow-x:auto;">
			   <table class="table table-striped" style="margin-bottom:0px">
			      <thead>
				 <tr class="active">
				    <th class="col-sm-4 col-xs-4">Details </th>
				    <th class="col-sm-2 col-xs-2">Rate </th>
				    <th class="col-sm-3 col-xs-3">Qty </th>
				    <th class="col-sm-4 col-xs-4">Total</th>
				 </tr>
			      </thead>
			      <tbody id="custinvoicelist_toedit">
				 
			      </tbody>
			   </table>
			</div>
		     </div>
		     <div class="col-sm-12 col-xs-12">
			<span class="custbill_errortext"></span>
			<button class="mui-btn mui-btn--accent mui-btn--raised pull-right cust_billbtn" onclick="update_custombilling()">
			Update Bill</button>
		     </div>
		  </div>
	       </div>
	    </div>
	 </div>
      </div>
      <!-- BILLING SECTION EDITABLE MODALS -->
      <div class="modal fade" tabindex="-1" role="dialog" id="editbillingModal" data-backdrop="static" data-keyboard="false">
         <form action="" method="post" id="editbilling_form" autocomplete="off" onsubmit="updatebilldetails(); return false;">
	    <input type="hidden" name="subscriber_uuid" class="subscriber_uuid" value='<?php echo $subscriber_uid ?>'>
	    <input type="hidden" name="editbillid" id="editbillid" value="" />
	    <input type="hidden" name="editbilltype" id="editbilltype" value="" />
	    <input type="hidden" name="editbill_receiptamount" id="editbill_receiptamount" value="" />
	    <input type="hidden" name="editbill_prevbillamount" id="editbill_prevbillamount" value="" />
	    <input type="hidden" name="editbillpaid" id="editbillpaid" value="" />
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <h4 class="modal-title">
			<strong>EDIT <span id="billtypetext"></span> BILL</strong>
			<span class="hide" id="billplanname"></span>
		     </h4>
		  </div>
		  <div class="modal-body">
		     <div class="disabledwhenpayadvanced">
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input id="editdbbilldtme" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="">
				 <label>Bill DateTime<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input type="text" id="editdbbill_number" readonly>
				 <label>Bill Number<sup>*</sup></label>
			      </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="mui-textfield">
				 <input type="text" name="editdbbill_billamount" id="editdbbill_billamount" required>
				 <label>Total Amount<sup>*</sup></label>
				 <span id="billamount_errtxt"></span>
				 <input type="hidden" id="billamount_err" value="0" />
			      </div>
			   </div>
			   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			      <div class="">
				 <label style="display:block;">Discount (If Any)</label>
				 <input type="radio" name="discounttype" value="percent" /> Percent Discount &nbsp;&nbsp;
				 <input type="radio" name="discounttype" value="flat" /> Flat Discount
				 <select name="editdbbill_percentdiscount" class="form-control" style="font-size:12px">
				    <option value="0" selected>Select Discount %</option>
				    <?php
				    for($d=1; $d<=100; $d++){
				       echo '<option value="'.$d.'">'.$d.'%</option>';
				    }
				    ?>
				 </select>
				 <input type="number" name="editdbbill_flatdiscount" min="0" value="0" class="form-control hide" style="font-size:12px"/>
			      </div>
			   </div>
			</div>
			<div class="row" style="margin-bottom:20px">
			   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <h3 style="margin:0px">Additional Charges</h3>
			      <div class="addichrgrow row">
				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				    <input type="text" name="additional_chrgtxt[]" class="form-control" value="" placeholder="Enter Charges Details" style="font-size:12px"/>
				 </div>
				 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
				    <input type="number" name="additional_chrgval[]" class="form-control" value="" style="font-size:12px" />
				 </div>
				 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 addrmvclass">
				    <a href="javascript:void(0)" onclick="addmorecharges()"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add</a>
				 </div>
			      </div>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="editdbbill_netamount" id="editdbbill_netamount" readonly required  style="font-size:16px"/>
			      <label>Net Amount (inclusive taxes)<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input type="text" name="editdbbill_hsncode" id="editdbbill_hsncode">
			      <label>HSN Code</label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <textarea name="editdbbill_comments"></textarea>
			      <label>Comments</label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <textarea name="editdbbill_remarks"></textarea>
			      <label>Remarks</label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="">
                              <label style="display:block;">is advanced paid</label>
			      <input type="radio" name="is_advanced_paid" value="1" /> Yes &nbsp;&nbsp;
			      <input type="radio" name="is_advanced_paid" value="0" checked/> No
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="Save" />
		  </div>
	       </div>
	       <!-- /.modal-content -->
	    </div>
	 </form>
      </div>
      
      
      <div class="modal fade" tabindex="-1" role="dialog" id="editplanspeedModal" data-backdrop="static" data-keyboard="false">
         <form action="" method="post" id="editplanspeed_form" autocomplete="off" onsubmit="update_planspeed(); return false;">
	    <input name="assigned_activeplan" id="assigned_activeplan" type="hidden" value="">
	    <div class="modal-dialog">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		     <h4 class="modal-title">
			<strong>Edit PlanSpeed</strong>
		     </h4>
		  </div>
		  <div class="modal-body">
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input name="subscriber_uuid" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="<?php echo $subscriber_uid ?>" required>
			      <label>UID<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input name="editplan_uploadspeed" type="text" value="" required>
			      <label>Upload Speed (in Kbps)<sup>*</sup></label>
			   </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-textfield">
			      <input name="editplan_downloadspeed" type="text" value="" required>
			      <label>Download Speed (in Kbps)<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		     <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			   <div class="mui-radio">
			      <label class="nopadding">Send Alert To Customer</label> <br/>
			      <input type="radio" name="editplan_speedalert" value="1"> Yes &nbsp;&nbsp;
			      <input type="radio" name="editplan_speedalert" value="0" checked> No
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer">
		     <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="Save" />
		  </div>
	       </div>
	       <!-- /.modal-content -->
	    </div>
	 </form>
      </div>
      
      <div class="modal fade" id="payinstsecurityModal" role="dialog" data-backdrop="static" data-keyboard="false">
	 <form action="" method="post" id="payinstsecurity_form" autocomplete="off" onsubmit="payinstsecuritybill(); return false;">
	    <input type="hidden" name="billid" value="" />
	    <div class="modal-dialog modal-sm">
	       <div class="modal-content">
		  <div class="modal-header">
		     <button type="button" class="close" data-dismiss="modal">&times;</button>
		     <h4 class="modal-title"><strong>Enter Receipt to Pay</strong></h4>
		  </div>
		  <div class="modal-body" style="padding-bottom:5px">
		     <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="mui-textfield">
			      <input type="tel" name="update_receipt_number" required="required" maxlength="20">
			      <label>Receipt Number<sup>*</sup></label>
			   </div>
			</div>
		     </div>
		  </div>
		  <div class="modal-footer" style="text-align: right">
		     <button type="button" class="custplanbtn mui-btn mui-btn--small mui-btn--accent" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
		     <input type="submit" class="custplanbtn mui-btn mui-btn--small mui-btn--accent" value="Save" />
		  </div>
	       </div>
	    </div>
	 </form>
      </div>
      <div class="modal fade" id="walletbalhistoryModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		  <h4 class="modal-title">
		     <i class="fa fa-money" aria-hidden="true"></i>&nbsp;<strong>Wallet Balance History</strong>
		  </h4>
	       </div>
	       <div class="modal-body" style="padding-bottom:5px">
		  <p id="walletbalhistory"></p>  
	       </div>
	       <div class="modal-footer"></div>
	    </div>
         </div>
      </div>

      <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/material.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/misc.js?version=13.6"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/billing.js?version=13.5"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/docupload.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/kycupload.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/corpupload.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/data_topup.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/moment-with-locales.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap-material-datetimepicker.js"></script>
      <link href="<?php echo base_url() ?>assets/css/bootstrap-toggle.min.css" rel="stylesheet">
      <script src="<?php echo base_url() ?>assets/js/bootstrap-toggle.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/crbnMenu.js"></script>
      <script type="text/javascript">
         if ($(window)) {
            $(function () {
               $('.menu').crbnMenu({
               hideActive: true
               });
            });
         }
      </script>
      <script type="text/javascript">
         $('body').on('click', '.slider', function(){
            var priority = ($('#priority_account').val() == 0) ? 1 : 0;
            $('#priority_account').val(priority);
         });
         $(document).ready(function() {
            var height = $(window).height();
            $('#main_div').css('height', height);
            $('#right-container-fluid').css('height', height);
	    setTimeout(function (){ $('.loading').addClass('hide') },1000);
	    $('.enquiry_status').addClass('hide');
	    
	    <?php if($usertype == 1){ ?>
	    $('.enquiry_status').removeClass('hide');
	    $('input[name="user_type_radio"][value="enquiry"]').attr('disabled','disabled');
	    <?php }elseif($usertype == 2){ ?>
	    $('.enquiry_status').removeClass('hide');
	    $('input[name="user_type_radio"][value="lead"]').attr('disabled','disabled');
	    <?php } ?>
	    
	    $.ajax({
	       url: base_url+'user/taxapplicable',
	       type: 'POST',
	       async: false,
	       success: function(datatax){
		  $('.current_taxapplicable').val(datatax);
	       }
	    });
         
            // on user type change change page(form)
            var defaultchked = $('input[type=radio][name=user_type_radio]:checked').val();
            if ((defaultchked == 'lead') || (defaultchked == 'enquiry')) {
               $('.tab_menu').css({'cursor' : 'not-allowed'});
               $('.tab_menu').attr('disabled','disabled');
            }
            $('input[type=radio][name=user_type_radio]').change(function() {
               <?php if($record == 0){ ?>
                  $('.citylist').empty().append('<option value="">City*</option>');
               <?php } ?>
               if (this.value == 'lead' || this.value == 'enquiry') {
                  if (this.value == 'lead') {
                     $('#subscriber_usertypele').val(1);
                  }else{
                     $('#subscriber_usertypele').val(2);
                  }
                  $('.tab_menu').css({'cursor' : 'not-allowed'});
                  $('.tab_menu').attr('disabled','disabled');
                  $("#user_type_lead_enquiry")[0].reset();
                  $("#user_type_lead_enquiry").show();
                  $("#user_type_customer").hide();
               }else if (this.value == 'customer') {
                  $('#subscriber_usertype').val(3);
                  $('.tab_menu').css({'cursor' : 'not-allowed'});
                  $('.tab_menu').attr('disabled','disabled');
                  $("#user_type_customer")[0].reset();
                  /*
                  var date = new Date();
                  var components = [
                      date.getYear(),
                      date.getMonth(),
                      date.getDate(),
                      date.getHours(),
                      date.getMinutes(),
                      date.getSeconds(),
                      date.getMilliseconds()
                  ];
                  var uuid = components.join("");
                  
                  var uuid = "<?php //echo random_string('alnum', 6); ?>";
                  //$('input[name="subscriber_uuid"]').val(uuid);
                  //$('input[name="subscriber_username"]').val(uuid);*/
                  
                  <?php if($record != 0){ ?>
                     getcitylist(<?php echo $state ?>, 'subscriber', <?php echo $city ?>);
                     getzonelist(<?php echo $city ?>, 'subscriber', <?php echo $zone ?>)
                  <?php } ?>
                  
                  $("#user_type_customer").show();
                  $("#user_type_lead_enquiry").hide();
               }
            });
            
         });
      </script>
      <script type="text/javascript" > 
         $(document).ready(function(){
            $("#create_flip").click(function(){
               $("#create_panel").toggle();
            });
         });
      </script>
      <script type="text/javascript">
         $('body').on('click','#create_newticket', function(){
            $('select[name="ticket_type"]').val('');
	    $('select[name="ticket_priority"]').val('');
	    $('select[name="ticket_assignto"]').val('');
	    $('textarea[name="ticket_description"]').val('');
	    $('input[name="ticket_comment"]').val('');
            var cust_uuid = $('input[name="subscriber_uuid"]').val();
            $.ajax({
               url: base_url+'user/ticket_sorters_list',
               type: 'POST',
               async: false,
	       data: 'uuid='+cust_uuid,
               success: function(data){
		  $('select[name="ticket_assignto"]').empty().append('<option value="">Assign Ticket</option>');
                  $('select[name="ticket_assignto"]').append(data);
               }
            });
            $('input[name="customer_uuid"]').val(cust_uuid);
            var d = new Date();
            var time = ' '+d.toLocaleTimeString();
            var month = d.getMonth()+1;
            var day = d.getDate();
            var currdate = ((''+day).length<2 ? '0' : '')+day + '-' +
                         ((''+month).length<2 ? '0' : '')+month + '-' +
                         d.getFullYear() ;
            var seconds = d.getSeconds();
            var tkttime = d.getHours() +""+ d.getMinutes() +""+ ((''+seconds).length<2 ? '0' : '')+seconds ;
            var tktdate = day +""+ month +""+ d.getFullYear().toString().substr(-2) ;
            var dbtktdatetime =  d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + d.getHours() +":"+ d.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;;
            $('input[name="ticket_datetime"]').remove();
            $('input[name="ticket_datetimeformat"]').remove();
            $('input[name="ticketid"]').val(tktdate+tkttime);
            $('#tktdtme').append('<input name="ticket_datetimeformat" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="'+currdate+time+'">');
            $('#tktdtme').append('<input name="ticket_datetime"  class="mui--is-empty mui--is-untouched mui--is-pristine" type="hidden" value="'+dbtktdatetime+'">');
            $('#serviceRequestModal').modal('show');
         });
         
         $(document).ready(function(){
            var d = new Date(); var currY = d.getFullYear();
            var validY = currY - 18;
            $('.dobdate').bootstrapMaterialDatePicker({
               maxDate: moment("12/31/"+validY),
               format: 'DD-MM-YYYY',
               time: false,
               //clearButton: true
            });
            
            $('.date').bootstrapMaterialDatePicker({
               format: 'DD-MM-YYYY',
               time: false,
               //clearButton: true
            });
            
            
            $('.date-start').bootstrapMaterialDatePicker({
               weekStart: 0, format: 'DD-MM-YYYY', shortTime : true
            }).on('change', function(e, date){
                  $('.date-end').bootstrapMaterialDatePicker('setMinDate', date);
               });
            
            $('.min-date').bootstrapMaterialDatePicker({ format : 'DD-MM-YYYY', minDate : new Date() });
            
	    $('.walletamt_receivedate').bootstrapMaterialDatePicker({
               weekStart: 0,
	       format: 'DD-MM-YYYY HH:mm',
	       //shortTime : true,
	       maxDate: moment()
            }).on('change', function(e, date){

	       var dateObj = new Date(date);
	       var month = (dateObj.getMonth()+1).toString();
	       var day = dateObj.getDate().toString();
	       var seconds = dateObj.getSeconds();
	       var year = dateObj.getFullYear();
	       var dbtktdatetime =  year + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + dateObj.getHours() +":"+ dateObj.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;;
               $('input[name="addtowallet_bill_datetime"]').val(dbtktdatetime);
            });
	    
            $.material.init();
            $('.active_addc').addClass('hide');
         });
      </script>
      <!-- script to get the geolocation and set lat and long -->
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA61iANDo6jqGYQm0SjXzIvmOEnks-MpG0&amp;libraries=places"  type="text/javascript"></script>
      <script type="text/javascript">
	 $('body').on('change', 'input[type=radio][name=geoaddress_searchtype]', function(){
	    var searchtype = $(this).val();
	    if (searchtype == 'byaddr') {
	       $('.geobyaddr').removeClass('hide');
	       $('.geobylatlng').addClass('hide');
	       $('#geolocation').attr('required','required');
	       $('#geolat').removeAttr('required');
	       $('#geolong').removeAttr('required');
	    }else if (searchtype == 'bylatlng') {
	       $('.geobyaddr').addClass('hide');
	       $('.geobylatlng').removeClass('hide');
	       $('#geolocation').removeAttr('required');
	       $('#geolat').attr('required','required');
	       $('#geolong').attr('required','required');
	    }
	 });
	 $('document').ready(function(){
	    <?php if($geoaddress_searchtype == 'byaddr'){ ?>
	       $('.geobyaddr').removeClass('hide');
	       $('.geobylatlng').addClass('hide');
	       $('#geolocation').attr('required','required');
	       $('#geolat').removeAttr('required');
	       $('#geolong').removeAttr('required');
	    <?php }elseif($geoaddress_searchtype == 'bylatlng'){ ?>
	       $('.geobyaddr').addClass('hide');
	       $('.geobylatlng').removeClass('hide');
	       $('#geolocation').removeAttr('required');
	       $('#geolat').attr('required','required');
	       $('#geolong').attr('required','required');
	    <?php } ?>
	 });
	 
	 function initialize() {
	      var input = document.getElementById('geolocation');
	      //var options = {componentRestrictions: {country: 'in'}};
	      var autocomplete = new google.maps.places.Autocomplete(input);
	      google.maps.event.addListener(autocomplete, 'place_changed', function () {
		  var place = autocomplete.getPlace();
		  document.getElementById('lat').value = place.geometry.location.lat().toFixed(6);
		  document.getElementById('long').value = place.geometry.location.lng().toFixed(6);
		  document.getElementById('placeid').value = place.place_id;
		  $("#placeid").attr('class','mui--is-dirty valid mui--is-not-empty');
		  $("#lat").attr('class','mui--is-dirty valid mui--is-not-empty');
		  $("#long").attr('class','mui--is-dirty valid mui--is-not-empty');
	      });
	  }
	  
	 function reverse_geolocation() {
	    var geolat = $('#geolat').val();
	    var geolng = $('#geolong').val();
	    
	    if (geolat == '' && geolng == '') {
	       window.alert("Please enter Latitude & Longitude of the location."); 
	    }else{
	       var latlng = {lat: parseFloat(geolat), lng: parseFloat(geolng)};
	       var geocoder = new google.maps.Geocoder;
	       geocoder.geocode({'location': latlng}, function(results, status) {
		  if (status === 'OK') {
		     if (results[0]) {
			$('#geoaddress').val(results[0].formatted_address);
			$('#geoplaceid').val(results[0].place_id);
		     } else {
			window.alert('No results found');
		     }
		  } else {
		    window.alert('Geocoder failed due to: ' + status);
		  }
	       });
	    }
	 }
    
      google.maps.event.addDomListener(window, 'load', initialize);
    
   </script>

   </body>
</html>