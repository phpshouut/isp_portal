<div class="col-sm-12 col-xs-12" style="margin-bottom: 15px;">
      <div class="row">
         <div class="col-sm-12 col-xs-12">
            <h2>Sys Log</h2>
            <?php //$conn=$this->user_model->check_sysdatabase();?>
            
            <div class="row dberr" style="color:red;"></div>
      
               <div class="row">
                   <form action="" method="post" id="syslog_form" autocomplete="off" onsubmit="syslog_connection(); return false;">
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                     <div class="row" style="margin-top:25px">
                        <div class="col-sm-12 col-xs-12">
                           <h4 class="toggle_heading">Setup Sys Log</h4>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-2 col-xs-2">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input class="mui--is-empty mui--is-untouched mui--is-pristine" name="hostname" type="text">
                        <label>HostName</label>
                     </div>
                  </div>
                  <div class="col-sm-2 col-xs-2">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input class="mui--is-empty mui--is-untouched mui--is-pristine" name="username" type="text">
                        <label>Username</label>
                     </div>
                  </div>
                   <div class="col-sm-2 col-xs-2">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input class="mui--is-empty mui--is-untouched mui--is-pristine" name="password" type="text">
                        <label>Password</label>
                     </div>
                  </div>
                    <div class="col-sm-2 col-xs-2">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input class="mui--is-empty mui--is-untouched mui--is-pristine" name="dbname" type="text">
                        <label>Database Name</label>
                     </div>
                  </div>
                  <div class="col-sm-3 col-xs-3">
                        <div class="col-sm-12 col-xs-12">
                              <div class="col-sm-9 col-xs-9">
                     <button class="mui-btn mui-btn--accent dbconnect" style="height:30px; line-height: 30px; margin-top:15px">Connecting...</button>
                              </div>
                               <div class="col-sm-3 col-xs-3 ">
                  <div class="loadmore_loader hide" style="margin-top:15px;">
                                       <img src="<?php echo base_url() ?>assets/images/loader.svg" width="100%"/>
                                    </div></div>
                        </div>
                  </div>
                     
                   </form>
               </div>
            
               <div class="row">
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                     <div class="row" style="margin-top:25px">
                        <div class="col-sm-12 col-xs-12">
                           <h4 class="toggle_heading">Start Date</h4>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-2 col-xs-2">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input class="is_readonly mui--is-empty mui--is-untouched mui--is-pristine date-start startdate" name="startdate" type="text">
                       
                     </div>
                     <span class="sdateerr" style="color:red;"></span>
                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                     <div class="row" style="margin-top:25px">
                        <div class="col-sm-12 col-xs-12">
                           <h4 class="toggle_heading">End Date</h4>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-2 col-xs-2">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input class="is_readonly mui--is-empty mui--is-untouched mui--is-pristine date-end enddate" name="enddate" type="text">
                        
                     </div>
                     <span class="edateerr" style="color:red;"></span>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                     <div class="row" style="margin-top:25px">
                        <div class="col-sm-12 col-xs-12">
                           <h4 class="toggle_heading">Start Time</h4>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-2 col-xs-2">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input class="is_readonly mui--is-empty mui--is-untouched mui--is-pristine timehh starttime" name="starttime" type="text">
                      
                     </div>
                     <span class="stimeerr" style="color:red;"></span>
                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                     <div class="row" style="margin-top:25px">
                        <div class="col-sm-12 col-xs-12">
                           <h4 class="toggle_heading" >End Time</h4>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-2 col-xs-2">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input class="is_readonly mui--is-empty mui--is-untouched mui--is-pristine timehh endtime" name="endtime" type="text">
                       
                     </div>
                       <span class="stimeerr" style="color:red;"></span>
                  </div>
                  <input type="hidden" id="user" name="user" value="<?php echo $uid;?>">
                   <div class="col-sm-3 col-xs-3">
                     <button class="is_readonly mui-btn mui-btn--accent syslog"  style="height:30px; line-height: 30px; margin-top:15px">Search</button>
                  </div>
               </div>
            
         </div>
      </div>
   </div>
   <div class="col-sm-12 col-xs-12">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="table-responsive">
               <table id="syslog_table" class="table table-striped">
                  <thead>
                     <tr class="active">
                        
                        <th>NasIP</th>
                        <th>Nasname</th>
                        <th>Connection Type</th>
                        <th>IP Protocol</th>
                        <th>Src-IP</th>
                        <th>Src-Port</th>
                        <th>Dst-ip</th>
                        <th>Dst-port</th>
                        <th>Datetime</th>
                     </tr>
                  </thead>
                  <tbody id="sysview">
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="row" style="text-align:center">
                                     <input type='hidden' id="limit" value="<?php echo LIMIT;?>" />
                                    <input type='hidden' id="offset" value="<?php echo OFFSET;?>" />
                                    <input type='hidden' id="soffset" value="<?php echo SOFFSET;?>" />
                                    <div class="loadmore hide">
                                       <span style="padding:5px; border:1px solid; cursor:pointer" onclick="loadmore_syslog('plan','syslogdata','','')">Load More</span>
                                    </div>
                                    <div class="loadmore_loader hide">
                                       <img src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
                                    </div>
                                 </div>
                              </div>
   
   
  
   
   <script type="text/javascript">
    $(document).ready(function () {
        $('.date').bootstrapMaterialDatePicker({
            format: 'DD-MM-YYYY',
            time: false,
            clearButton: true
        });

        
        $('.timehh').bootstrapMaterialDatePicker
                ({
                    date: false,
                    shortTime: false,
                    format: 'HH:mm'
                });

         $('.date-end').bootstrapMaterialDatePicker({
            weekStart: 0, format: 'DD-MM-YYYY',time: false
        })         
        $('.date-start').bootstrapMaterialDatePicker({
            weekStart: 0, format: 'DD-MM-YYYY',time: false
        })
                .on('change', function (e, date) {
                    $('.date-end').bootstrapMaterialDatePicker('setMinDate', date);
                });

        $('.min-date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY', minDate: new Date()});

        $.material.init()
        
      
    });
    
    $(document).on('click','#syslogtab',function(){
       $('.loadmore_loader').removeClass('hide');
      $.ajax({
        url: base_url+'user/check_sysdatabase',
        type: 'POST',
        dataType: 'json',
        data: {id:1},
        success: function(data){
            
            if (data.connected=="1") {
                 $('.dbconnect').text('Connected');
                 $('.dbconnect').prop('disabled',true);
                 $('.dberr').html('');
                 $('.syslog').prop('disabled',false);
                 
            }
            else{
                 $('.dbconnect').text('Connect');
                 $('.dbconnect').prop('disabled',false);
                 $('.dberr').html('Syslog Db not Connected');
                 $('.syslog').prop('disabled',true);
            }
            
            if (data.isreadonly_permission == '1') {
                  $('#syslog_form input').prop('disabled', true);
		  $('#syslog_form button').prop('disabled', true);
                  $('.is_readonly').prop('disabled', true);
            }else{
                  $('#syslog_form input').prop('disabled', false);
		  $('#syslog_form button').prop('disabled', false);
                  $('.is_readonly').prop('disabled', false);
            }
            
            $('.loadmore_loader').addClass('hide');
          
           
        }
    });
    });
    
    function getsyslogdetails() {
      $('.loadmore_loader').removeClass('hide');
      $.ajax({
        url: base_url+'user/check_sysdatabase',
        type: 'POST',
        dataType: 'json',
        data: {id:1},
        success: function(data){
            
            if (data.connected=="1") {
                 $('.dbconnect').text('Connected');
                 $('.dbconnect').prop('disabled',true);
                 $('.dberr').html('');
                 $('.syslog').prop('disabled',false);
                 
            }
            else{
                 $('.dbconnect').text('Connect');
                 $('.dbconnect').prop('disabled',false);
                 $('.dberr').html('Syslog Db not Connected');
                 $('.syslog').prop('disabled',true);
            }
            
            if (data.isreadonly_permission == '1') {
                  $('#syslog_form input').prop('disabled', true);
		  $('#syslog_form button').prop('disabled', true);
                  $('.is_readonly').prop('disabled', true);
            }else{
                  $('#syslog_form input').prop('disabled', false);
		  $('#syslog_form button').prop('disabled', false);
                  $('.is_readonly').prop('disabled', false);
            }
            
            $('.loadmore_loader').addClass('hide');
          
           
        }
      });
    }
    
    $(document).on('click','.syslog',function(){
     
      var startdate=$('.startdate').val();
      var endate=$('.enddate').val();
      var starttime = $('.starttime').val();
      var endtime = $('.endtime').val();
       var limit = $('#limit').val();
    var offset = $('#soffset').val();
      var user=$('#user').val();
      if (startdate=="" || endate=="") {
            if (startdate=="" && endate=="") {
                  
            }
            else{
                  if (startdate=="") {
                       $('.sdateerr').html("Enter the start date");
                       return false;
                 }
                 else{
                  $('.sdateerr').html("");
                 }
                 
                 if (endate=="") {
                       $('.edateerr').html("Enter the End date");
                       return false;
                 }
                 else{
                 
                  $('.edateerr').html("");
                 }
            }
           
      }
      else{
       $('.sdateerr').html("");
        $('.edateerr').html("");
      }
      
       if (starttime=="" || endtime=="") {
            if (starttime=="" && endtime=="") {
                  
            }
            else{
                  if (starttime=="") {
                       $('.stimeerr').html("Enter the start time");
                       return false;
                 }
                 else{
                  $('.stimeerr').html("");
                 }
                 
                 if (endtime=="") {
                       $('.etimeerr').html("Enter the End time");
                       return false;
                 }
                 else{
                  $('.etimeerr').html("");
                 }
            }
           
      }
      else
      {
            $('.stimeerr').html("");
            $('.etimeerr').html(""); 
      }
      $('.loading').removeClass('hide');
      $.ajax({
        url: base_url+'plan/syslogdata',
        type: 'POST',
        dataType: 'json',
        data: {startdate:startdate,endate:endate,starttime:starttime,endtime:endtime,user:user,limit:limit,offset:offset},
        success: function(data){
            
            var nxtlimit = data.limit;
            var nxtofset = data.offset;
             $('#limit').val(nxtlimit); $('#offset').val(nxtofset);
           $('#sysview').html(data.html);
           $('.loading').addClass('hide');
           $('.loadmore').removeClass('hide');
          
           
        }
    });
      
      
      
     
      
    });
    
    function loadmore_syslog(controller,method,pageinfo='', param='') {
    $('.loadmore').addClass('hide');
    $('.loadmore_loader').removeClass('hide');
   var startdate=$('.startdate').val();
      var endate=$('.enddate').val();
      var starttime = $('.starttime').val();
      var endtime = $('.endtime').val();
       var limit = $('#limit').val();
    var offset = $('#offset').val();
      var user=$('#user').val();
  
   
    
    $.ajax({
        url : base_url+controller+'/'+method,
        type : 'POST',
       data: {startdate:startdate,endate:endate,starttime:starttime,endtime:endtime,user:user,limit:limit,offset:offset},
        dataType: 'json',
        success: function(data){
              var nxtlimit = data.limit;
            var nxtofset = data.offset;
             $('#limit').val(nxtlimit); $('#offset').val(nxtofset);
            $('#sysview').append(data.html);
           $('.loading').addClass('hide');
             $('.loadmore_loader').addClass('hide');
             $('.loadmore').removeClass('hide');
             
        }
    })
}

function syslog_connection()
{
var formdata = $("#syslog_form").serialize();
     
    $('.loadmore_loader').removeClass('hide');
     $.ajax({
        url: base_url+'plan/add_syslogconn',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
         
         if (data.resultcode==1) {
             $('.loadmore_loader').addClass('hide');
            $('.dberr').addClass('hide');
            $('.dbconnect').removeAttr('disabled');
            $('.syslog').removeAttr('disabled');
            $('.dbconnect').html('connected');
          
            
         }
         else{
             $('.loadmore_loader').addClass('hide');
            $('.dberr').removeClass('hide');
            $('.dbconnect').addAttr('disabled');
            $('.dbconnect').html('connect');
           
            $('.syslog').addAttr('disabled');
         }
        }
    });
}

function check_configuration(uid) {
      
	 $(".loading").removeClass('hide');
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>plan/check_configuration",
	    data: "uid="+uid,
	    dataType: 'json',
	    success: function(data){
	       if(data.is_nas_setup == '1'){
		  $("#check_conf_nas_setup_yes").show();
		  $("#check_conf_nas_setup_no").hide();
	       }else{
		  $("#check_conf_nas_setup_yes").hide();
		  $("#check_conf_nas_setup_no").show();
	       }
               
                if(data.is_plan_setup == '1'){
		  $("#check_plan_setup_yes").show();
		  $("#check_plan_setup_no").hide();
	       }else{
		  $("#check_plan_setup_yes").hide();
		  $("#check_plan_setup_no").show();
	       }
	       
	       if(data.is_plan_nas_setup == '1'){
		  $("#check_conf_nas_plan_association_yes").show();
		  $("#check_conf_nas_plan_association_no").hide();
	       }else{
		  $("#check_conf_nas_plan_association_yes").hide();
		  $("#check_conf_nas_plan_association_no").show();
	       }
	       $(".loading").addClass('hide');
              
	       $("#is_config_proper_setup").modal("show");
	    }
         });
}
</script>
   
   