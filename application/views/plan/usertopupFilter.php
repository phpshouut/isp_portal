<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>SHOUUT | ISP</title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading hide">
	 <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                      <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                     <img src="<?php echo $img ?>" class="img-responsive" />
                     </span>
                  </div>
                  <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
                 $this->view('left_nav',$data); ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <span class="navbar-brand">Users</span>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <?php $this->load->view('plan/plan_headerview',$data);?>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-right">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-4 col-md-4 plans-padding">
                                            <div class="plans-thumbnail" style="background-color:#ffbc00;">
                                                <div class="caption">
                                                    <h5>TOP Speed Top-Up</h5>
                                                     <h4>
                                       <?php echo $topup_count['speed']['topupname']?>
                                    </h4>
                                               <h6><a href="<?php echo ($topup_count['speed']['usercount']>0)?base_url().'plan/topupuser_stat/'.$topup_count['speed']['topupid']:'#'; ?>" style=" text-decoration:underline; color:white;"> <?php echo $topup_count['speed']['usercount'] ?> users using it  </a></h6>
                                    <h6><a href="<?php echo base_url().'plan/topup_stat/speed' ?>" style=" text-decoration:underline; color:white;"><?php echo $topup_count['speed']['topupcount'] ?> Top-Up</a></h6>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-sm-4 col-md-4 plans-padding">
                                            <div class="plans-thumbnail" style="background-color:#F4474A;">
                                                <div class="caption">
                                                 <h5>TOP Data Top-Up</h5>
                                                   <h4>
                                       <?php echo $topup_count['datatopup']['topupname']?> 
                                    </h4>
                                                 <h6><a href="<?php echo ($topup_count['datatopup']['usercount']>0)?base_url().'plan/topupuser_stat/'.$topup_count['datatopup']['topupid']:'#'; ?>" style=" text-decoration:underline; color:white;"> <?php echo $topup_count['datatopup']['usercount'] ?> users using it</a></h6>
                                    <h6><a href="<?php echo base_url().'plan/topup_stat/datatopup' ?>" style=" text-decoration:underline; color:white;"><?php echo $topup_count['datatopup']['topupcount'] ?> Top-Up</a></h6>
                                                </div>
                                            </div>
                                        </div>

                                     <div class="col-sm-6 col-sm-4 col-md-4 plans-padding">
                                            <div class="plans-thumbnail" style="background-color:#29ABE2;">
                                                <div class="caption">
                                                   <h5>TOP Data un-accountancy Top-Up</h5>
                                                       <h4>
                                       <?php echo $topup_count['unacctncy']['topupname']?>
                                    </h4>
                                                  <h6><a href="<?php echo ($topup_count['unacctncy']['usercount']>0)?base_url().'plan/topupuser_stat/'.$topup_count['unacctncy']['topupid']:'#'; ?>" style=" text-decoration:underline; color:white;"> <?php echo $topup_count['unacctncy']['usercount'] ?> users using it </a></h6>
                                    <h6><a href="<?php echo base_url().'plan/topup_stat/unacctncy' ?>" style=" text-decoration:underline; color:white;"><?php echo $topup_count['unacctncy']['topupcount'] ?> Top-Up</a></h6>
                                                </div>
                                            </div>
                                        </div>
                                      
                                    </div>
                                </div>
                           </div>
			   <div id="active_inactive_results">
                              <input type="hidden" id="filtertype" value="<?php echo $filtertype; ?>" >
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                       <h1 id="search_count"></h1>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 pull-right">
                                       <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 14px;">
                                          <div class="form-group">
                                             <div class="input-group">
                                                <div class="input-group-addon">
                                                   <i class="fa fa-search" aria-hidden="true"></i>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Search Logs" onBlur="this.placeholder='Search Logs'" onFocus="this.placeholder=''"  id="searchtext">
                                                <span class="searchclear" id="searchclear">
                                                <img src="<?php echo base_url() ?>assets/images/clear.svg"/>
                                                </span>
                                             </div>
                                             <label class="search_label">You can look up by name, email, UID or mobile</label>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="row" id="onefilter">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                       <div class="row">
                                          <div class="col-lg-3 col-md-3 col-sm-3">
                                             <div class="mui-select">
                                                <select name="filter_locality" onchange="search_filter()">
                                                   <option value="">User Category*</option>
                                                   <?php $this->user_model->usage_locality(); ?>
                                                </select>
                                                <label>User Category</label>
                                             </div>
                                          </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select name="filter_state" class='statelist' onchange="search_filter(this.value, 'state')">
                     <option value="all">All States</option>
                     <?php $this->plan_model->state_list(); ?>
                  </select>
                  <label>State</label>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select class="search_citylist"  onchange="search_filter(this.value, 'city')"  name="filter_city">
                     <option value="all">All Cities</option>
                  </select>
                  <label>City</label>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select  onchange="search_filter()" class="search_zonelist"  name="filter_zone">
                        <option value="all">All Zones</option>
                     <?php //$this->user_model->zone_list(); ?>
                  </select>
                  <label>Zone</label>
               </div>
            </div>
                               
                                       </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                       <div class="row pull-right">
                                          <div class="col-lg-12 col-md-12 col-sm-12">
                                             <!--<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <div class="row">
                                                   <h4 class="toggle_heading">Show Priority</h4>
                                                </div>
                                             </div>-->
                                            <!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                                <label class="switch">
                                                   <input type="checkbox">
                                                   <div class="slider round"></div>
                                                </label>
                                             </div>-->
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row" id="secondfilter" style="display:none">
                                    <div class="col-lg-5 col-md-5 col-sm-5">
                                       <div class="row">
                                          <div class="col-lg-6 col-md-6 col-sm-6 nopadding-right">
                                             <div class="switch-field">
                                                <input type="radio" id="switch_left" name="switch_2" value="active" checked/>
                                                <label for="switch_left">ACTIVE</label>
                                                <input type="radio" id="switch_right" name="switch_2" value="inactive" />
                                                <label for="switch_right" data-toggle="modal" data-target="#inactive">INACTIVE</label>
                                             </div>
                                          </div>
                                          <div class="col-lg-6 col-md-6 col-sm-6">
                                             <div class="mui-textfield">
                                                <input type="text" class="date" placeholder="dd.mm.yyyy">
                                                <label>Set Expiry</label>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                       <div class="view_user_right_list">
                                          <ul>
                                             <li><a href="#">Email Users</a></li>
                                             <li><a href="#">Reset password</a></li>
                                             <li><a href="#">Reset MAC</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="table-responsive">
                                    <table class="table table-striped">
                                       <thead>
                                          <tr class="active">
                                             <th>&nbsp;</th>
                                             <th>
                                                <div class="checkbox" style="margin-top:0px; margin-bottom:0px;">
                                                   <label>
                                                   <input type="checkbox" class="collapse_allcheckbox"> 
                                                   </label>
                                                </div>
                                             </th>
                                             <th>USERNAME</th>
                                             <th>FULL NAME</th>
                                             <th>STATE</th>
                                             <th>CITY</th>
                                             <th>ZONE</th>
                                             <th>PLAN</th>
                                             <th>ACTIVE FROM</th>
                                             <th>RENEWAL DATE</th>
                                             <th>EXPIRY</th>
                                             <th>STATUS</th>
                                             <th>BALANCE</th>
                                             <th>TICKETS</th>
                                          </tr>
                                       </thead>
                                       <tbody id="search_gridview"></tbody>
                                    </table>
                                 </div>
                              </div>
                              
                               <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="row" style="text-align:center">
                                     <input type='hidden' id="limit" value="<?php echo LIMIT;?>" />
                                    <input type='hidden' id="offset" value="<?php echo OFFSET;?>" />
                                    <input type='hidden' id="soffset" value="<?php echo SOFFSET;?>" />
                                    <div class="loadmore ">
                                       <span style="padding:5px; border:1px solid; cursor:pointer" onclick="loadmore_user('plan','active_inactive_topupusersfilter','','<?php echo $filtertype; ?>')">Load More</span>
                                    </div>
                                    <div class="loadmore_loader hide">
                                       <img src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
                                    </div>
                                 </div>
                              </div>
                           </div>
			</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/material.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
       <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plan_misc.js"></script>
      <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/misc.js?version=3.1"></script>
      <script type="text/javascript">
          $('body').on('click', '#lead_enquiry', function(){
	    window.location.href = base_url+'user/lead_enquiry';
	 });
	 
	 $('body').on('click', '#user_complaints', function(){
	    window.location.href = base_url+'user/complaints';
	 });
	 
	 $('body').on('click', '#inactive_users', function(){
	    window.location.href = base_url+'user/status/inact';
	 });
	 
	 $('body').on('click', '#active_users', function(){
	    window.location.href = base_url+'user/status/act';
	 });
         
         $('body').on('click','#searchclear', function(){
            $('#searchtext').val('');
           $('#searchtext').addClass('serch_loading');
            search_filter('','','1');
            
         });
         $('body').on('keyup', '#searchtext', function(){
            $('#searchtext').addClass('serch_loading');
            search_filter('','','1');
         });
         
         $('body').on('change', '.collapse_allcheckbox', function(){
            var status = this.checked;
            $('.collapse_checkbox').each(function(){ 
               this.checked = status;
            });
            if (status) {
               $('#onefilter').css('display', 'none');
               $('#secondfilter').css('display', 'block');
            }else{
               $('#onefilter').css('display', 'block');
               $('#secondfilter').css('display', 'none');
            }
         });
         
          $('body').on('change', '.collapse_checkbox', function(){
            if(this.checked == false){ 
               $(".collapse_allcheckbox")[0].checked = false; 
            }
            if ($('.collapse_checkbox:checked').length == $('.collapse_checkbox').length ){
               $(".collapse_allcheckbox")[0].checked = true; 
            }
            
            if ($('.collapse_checkbox:checked').length > 0) {
               $('#onefilter').css('display', 'none');
               $('#secondfilter').css('display', 'block');
            }else{
               $('#onefilter').css('display', 'block');
               $('#secondfilter').css('display', 'none');
            }
         });
         
         
            function getcitylist(stateid) {
    $.ajax({
        url: base_url+'plan/getcitylist',
        type: 'POST',
        dataType: 'text',
        data: 'stateid='+stateid+'&is_addable=0',
        success: function(data){
           
            $('.search_citylist').empty();
            $('.search_citylist').append(data);
        }
    });
}

 function getzonelist(cityid) {
      var stateid=$('.statelist').val();
    $.ajax({
        url: base_url+'plan/getzonelist'+'&is_addable=0',
        type: 'POST',
        dataType: 'text',
        data: 'cityid='+cityid+'&stateid='+stateid,
        success: function(data){
           
            $('.search_zonelist').empty();
            $('.search_zonelist').append(data);
        }
    });
}
         
         function search_filter(data='', filterby='',keyup=''){
            var searchtext = $('#searchtext').val();
            $('#search_panel').addClass('hide');
            $('#allsearch_results').removeClass('hide');
             if(keyup=="")
            {
                 $('.loading').removeClass('hide');
            }
            var limit = $('#limit').val();
    var offset = $('#soffset').val();
            var formdata = '';
            var city = $('select[name="filter_city"]').val();
            var zone = $('select[name="filter_zone"]').val();
            var locality = $('select[name="filter_locality"]').val();
            if (filterby == 'state') {
               getcitylist(data);
               formdata += '&state='+data+ '&city=&zone='+zone+'&locality='+locality;
            }else{
                  if (filterby == 'city')
               {
                   getzonelist(data);
               }
               var state = $('select[name="filter_state"]').val();
               formdata += '&state='+state+ '&city='+city+'&zone='+zone+'&locality='+locality;
            }
            
            formdata += '&filtertype='+$('#filtertype').val();
            //alert(formdata);
            $.ajax({
               url: base_url+'plan/active_inactive_topupusersfilter',
               type: 'POST',
               dataType: 'json',
               data: 'search_user='+searchtext+formdata+'&limit='+limit+'&offset='+offset,
               success: function(data){
                    var nxtlimit = data.limit;
            var nxtofset = data.offset;
             $('#limit').val(nxtlimit); $('#offset').val(nxtofset);
                    $('.loading').addClass('hide');
                  $('#search_count').html(data.total_results+' <small>Results</small>');
                  $('#search_gridview').html(data.search_results);
                  $('#searchtext').removeClass('serch_loading');
               }
            });
         }
         
         $(document).ready(function() {
	    var height = $(window).height();
            $('#main_div').css('height', height);
	    //$('#right-container-fluid').css('height', height);
	    
            var total_results = "<?php echo $active_inactive['total_results']; ?>";
            var search_results = "<?php echo $active_inactive['search_results']; ?>" ;
            var filtertype = $('#filtertype').val();
            if (filtertype == 'inact') {
               $('#search_count').html('Inactive Users ('+total_results+' <small>Results</small>)');
            }else{
               $('#search_count').html(total_results+' <small>Results</small>');
            }
	    $('#search_gridview').html(search_results);
	    $('#active_inactive_results').removeClass('hide');
	    $('.loading').addClass('hide');

         });
      </script>
      <script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
        

      </script>
   </body>
</html>