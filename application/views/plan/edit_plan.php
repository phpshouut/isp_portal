<?php $this->load->view('includes/header'); ?>
<div class="loading hide" >
    <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="wapper">
            <div id="sidedrawer" class="mui--no-user-select">
                <div id="sidedrawer-brand" class="mui--appbar-line-height">
                    <span class="mui--text-title">
                           <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                        <img src="<?php echo $img ?>" class="img-responsive" />
                    </span>
                </div>
                 <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
                 $this->view('left_nav',$data); ?>

            </div>
            <header id="header">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">Plans</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                             <?php $this->load->view('plan/plan_headerview',$data);?>
                        </div>
                    </div>
                </nav>
            </header>
            <div id="content-wrapper">
                <div class="mui--appbar-height"></div>
                <div class="mui-container-fluid" id="right-container-fluid">
                    <div class="add_user">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <ul class="mui-tabs__bar plan_mui-tabs__bar">
                                            <li class="mui--is-active Plan-details limiui">
                                                <a data-mui-toggle="tab" data-mui-controls="Plan-details">
                                                    Plan Details
                                                </a>
                                            </li>
                                            <li class="Plan-settings limiui">
                                                <a data-mui-toggle="tab" data-mui-controls="Plan-settings" class="tab_menu">
                                                    Plan Settings
                                                </a>
                                            </li>
                                            <li class="  Pricing limiui">
                                                <a data-mui-toggle="tab" data-mui-controls="Pricing" class="tab_menu">
                                                    Pricing
                                                </a>
                                            </li>
                                             <li class="  Pricing limiui">
                                                <a data-mui-toggle="tab" data-mui-controls="add-region" >
                                                    Region
                                                </a>
                                            </li>
                                            <li class="Usage-stats limiui">
                                                <a data-mui-toggle="tab" data-mui-controls="plan-nas-assoc" class="tab_menu plan_nas_assoc">
                                                    Nas Association
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="mui--appbar-height"></div>
                                        <div class="mui-tabs__pane mui--is-active " id="Plan-details">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <form action="" method="post" id="add_plan" autocomplete="off" onsubmit="add_plan(); return false;">
                                                        <h2>Plan Details</h2>
                                                      <?php   if($plan['plantype']==1)
                                                     {
                                                        $plandata="UP"; 
                                                     }
                                                    else   if($plan['plantype']==2)
                                                     {
                                                         $plandata="TP"; 
                                                     }
                                                      else  if($plan['plantype']==3)
                                                     {
                                                         $plandata="FP"; 
                                                     }
                                                       else
                                                     {
                                                         $plandata="DP"; 
                                                     }
                                                     ?>
                                                     <input type="hidden" id="prvdatalimit" name="prvdatalimit" value="<?php echo $plan['datalimit'];?>">
                                                        <input type="hidden" id="plan_id" class="plan_id" name="plan_id" value="<?php echo $plan['srvid'];?>">
                                                   <input type="hidden" id="plantype_id" class="plantype_id" name="plantype_id" value="<?php echo $plandata;?>">
                                                     <input type="hidden" id="" class="is_frmedit" name="is_frmedit" value="1">
                                                        <div class="form-group">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="text" maxlength="80" name="plan_name" <?php echo (isset($ro) && $ro==1)?"readonly":"";?> value="<?php echo $plan['srvname'];?>" required>
                                                                            <label>Plan Name<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="text" name="plan_desc" <?php echo (isset($ro) && $ro==1)?"readonly":"";?> value="<?php echo $plan['descr'];?>" >
                                                                            <label>Plan Description</label>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-top:10px;">
                                                                         <select name="plan_duration"  <?php echo (isset($ro) && $ro==1)?"readonly":"";?> class=" form-control" id=""    required>
                                                                            <option value="">Select Duration</option>
                                                                           <?php for($i=1;$i<=12;$i++){
                                                                            $selected=( $plan['plan_duration']==$i)?"selected":"";
                                                                            ?>
                                                                           <option value="<?php echo $i;?>" <?php echo $selected; ?>><?php echo $i." months"?></option>
                                                                           <?php } ?>
                                                                         </select>
                                                                        
                                                                    </div>
                                                                        
                                                                       
                                                                      
                                                                  <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <div class="row" style="margin-top:24px">
                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                                <h4 class="toggle_heading"> Enable for Use</h4>
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                                <label class="switch">
                                                                                    <input type="checkbox" <?php echo ($plan['enableplan']==1)?"checked":"";?>>
                                                                                    <div class="slider round slide_usable"></div>
                                                                                </label>
                                                                            </div>
                                                                            <input type="hidden" name="is_usable" id="is_usable" value="<?php echo $plan['enableplan'];?>">
                                                                        </div>
                                                                    </div>-->
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:5px">
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" > 
                                                             <div class="row">
                                                                   
                                                                         <label class="radio-inline">
                                                                        <input type="radio" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> required name="access_type"  value="0" <?php echo ($plan['is_private']==0)?"checked":"";?>> Public Plan 
                                                                    </label>
                                                                 <label class="radio-inline">
                                                                        <input type="radio" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> required name="access_type"  value="1" <?php echo ($plan['is_private']==1)?"checked":"";?>> private Plan 
                                                                    </label>
                                                                    
                                                                        </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" > 
                                                             <div class="row">
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left:0px">
                                                                    <h2 class="text-right">Plan Change</h2>
                                                                </div>
                                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right:5px">
                                                                        <select name="change_cycle"  <?php echo (isset($ro) && $ro==1)?"disabled":"";?> required class=" form-control" id=""    required>
                                                                            <option value="">Select</option>
                                                                             <option value="0" <?php echo ($plan['planchange_isnextcycle']==0 && $plan['planchange_isnextcycle']!='' )?"selected":"";?>>Now</option>
                                                                             <option value="1" <?php echo ($plan['planchange_isnextcycle']==1 && $plan['planchange_isnextcycle']!='')?"selected":"";?>>Next Cycle</option>
                                                                          
                                                                         </select>
                                                                    </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                       
                                                        <div class="form-group">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:5px;">
                                                                <h4>TYPE OF PLAN</h4>
                                                                <div class="row" style="margin-top: 10px;">
                                                                   
                                                                   <?php
                                                                   if($plan['plantype']==1)
                                                                   {
                                                                    $pt="UP";
                                                                   }
                                                                   else if($plan['plantype']==2)
                                                                   {
                                                                    $pt="TP";
                                                                   }
                                                                   else if($plan['plantype']==3)
                                                                   {
                                                                     $pt="FP";
                                                                   }
                                                                   else
                                                                   {
                                                                     $pt="DP";
                                                                   }
                                                                   ?> 
                                                                     <label class="radio-inline">
                                                                        <input type="radio" disabled required name="<?php echo (isset($ro) && $ro==1)?"plan_type_radio1":"plan_type_radio1";?>"  value="UP" <?php echo ($plan['plantype']==1)?"checked":"";?>> Unlimited Plan 
                                                                    </label>
                                                                 <!--<label class="radio-inline">
                                                                        <input type="radio" readonly required name="<?php echo (isset($ro) && $ro==1)?"plan_type_radio1":"plan_type_radio1";?>"  value="TP" <?php echo ($plan['plantype']==2)?"checked":"";?>> Time Plan 
                                                                    </label>-->
                                                                 <label class="radio-inline">
                                                                        <input type="radio" disabled required name="<?php echo (isset($ro) && $ro==1)?"plan_type_radio1":"plan_type_radio1";?>"  value="FP" <?php echo ($plan['plantype']==3)?"checked":"";?>>FUP Plan 
                                                                    </label>
                                                                 <label class="radio-inline">
                                                                        <input type="radio" disabled required name="<?php echo (isset($ro) && $ro==1)?"plan_type_radio1":"plan_type_radio1";?>"  value="DP" <?php echo ($plan['plantype']==4)?"checked":"";?>>  Data Plan 
                                                                    </label>
                                                                 <input type="hidden" name="plan_type_radio" value="<?php echo $pt;?>">
                                                                 <?php if(isset($ro) && $ro==1){?>
                                                                    <input type="hidden" name="plan_type_radio" value="<?php echo $pt;?>">
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" id="user_type_ful_plan_enquiry" style="display: block">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:20px;">
                                                                <div class="row">
                                                                    
                                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding-left">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" name="dwnld_rate" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> value="<?php echo ($plan['downrate']==0)?"":$this->plan_model->convertTodata($plan['downrate']."KB");?>" required>
                                                                            <span class="title_box">Kbps</span>
                                                                            <label>Download Rate<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" name="upld_rate" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> value="<?php echo ($plan['uprate']==0)?"":$this->plan_model->convertTodata($plan['uprate']."KB");?>" required>
                                                                             <span class="title_box">Kbps</span>
                                                                            <label>Upload Rate<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class ="row timeplan" style="margin-top:15px; display:none;">
                                                                    
                                                                 
                                                                    
                                                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding-left">
                                                                        <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
                                                                            <div class="input-group" style="margin-top:-5">
                                                                                
                                                                                <input type="text" <?php echo (isset($ro) && $ro==1)?"readonly":"";?> class="form-control  timelimit" pattern="([01]?[0-9]{2})(:[0-5][0-9]){1}" name="timelimit" value="<?php echo ($plan['timelimit']==0 || $plan['timelimit']=="")?"":$this->plan_model->sec2hms($plan['timelimit']);?>" id=""  placeholder="Time Limit HH:mm*"  >
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    
                                                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
                                                                <h4>Time Calculated on</h4>
                                                                    
                                                                <?php
                                                                $tc="";
                                                                if($plan['timecalc']==1)
                                                                {
                                                                  $tc="OT" ; 
                                                                }
                                                                else if($plan['timecalc']==2)
                                                                {
                                                                  $tc="LT" ;  
                                                                }
                                                                ?>
                                                                 <label class="radio-inline">
                                                                        <input type="radio" class="time_calcn" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> name="<?php echo (isset($ro) && $ro==1)?"time_calcn_radio1":"time_calcn_radio";?>" <?php echo ($plan['timecalc']==1)?"checked":"";?>  value="OT">Online Time 
                                                                    
                                                                    </label>
                                                                <label class="radio-inline">
                                                                        <input type="radio" class="time_calcn" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> name="<?php echo (isset($ro) && $ro==1)?"time_calcn_radio1":"time_calcn_radio";?>" <?php echo ($plan['timecalc']==2)?"checked":"";?>  value="LT"> Login Time 
                                                                    
                                                                    </label>
                                                                <?php if(isset($ro) && $ro==1){?>
                                                                <input type="hidden" name="time_calcn_radio" value="<?php echo $tc; ?>">
                                                                  <?php } ?>

                                                            </div>
                                                                </div>
                                                                <div class="row dataplan" style="margin-top: 15px; display:none;">
                                                                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding-left">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" step="0.001"  class="data_limit" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> name="data_limit" value="<?php echo ($plan['datalimit']==0 || $plan['datalimit']=="")?"":round($this->plan_model->convertTodata($plan['datalimit']."GB"),3);?>" placeholder="Data Limit in GBPS*" >
                                                                            <span class="title_box">Gbps</span>
                                                                            <label>Data Limit<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                     
                                                                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <h4>Date Calculated on</h4>
                                                               
                                                              <?php
                                                              $dc="";
                                                                if($plan['datacalc']==1)
                                                                {
                                                                  $dc="DO" ; 
                                                                }
                                                                else if($plan['datacalc']==2)
                                                                {
                                                                  $dc="UDC" ;  
                                                                }
                                                                ?>
                                                                   
                                                                 <label class="radio-inline">
                                                                     <input type="radio" disabled <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="data_calcn" name="<?php echo (isset($ro) && $ro==1)?"data_calcn_radio1":"data_calcn_radio1";?>" <?php echo ($plan['datacalc']==1)?"checked":"";?> value="DO"> Download Only 
                                                                    </label>
                                                                <label class="radio-inline">
                                                                     <input type="radio" disabled <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="data_calcn" name="<?php echo (isset($ro) && $ro==1)?"data_calcn_radio1":"data_calcn_radio1";?>" <?php echo ($plan['datacalc']==2)?"checked":"";?> value="UDC">Upload Download Combined
                                                                    </label>
                                                                 <input type="hidden" name="data_calcn_radio" value="<?php echo $dc; ?>">
                                                                 <?php if(isset($ro) && $ro==1){?>
                                                                <input type="hidden" name="data_calcn_radio" value="<?php echo $dc; ?>">
                                                            <?php } ?>
                                                                     </div>
                                                                </div>
                                                                
                                                                 <div class="row fupclass" style="display:none;">
                                                                    <div class="row">
                                                                         <div class="col-sm-12 col-xs-12"> <h4>Post FUP limit</h4></div>
                                                                    </div>
                                                                    <div class="row">
                                                                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="fup_dwnl_rate" value="<?php  echo ($plan['fupdownrate']==0 || $plan['fupdownrate']=="")?"":round($this->plan_model->convertTodata($plan['fupdownrate']."KB")); ?>" name="fup_dwnl_rate">
                                                                            <span class="title_box">Kbps</span>
                                                                            <label>Download Rate<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                     
                                                                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="fup_upld_rate" value="<?php  echo ($plan['fupuprate']==0 || $plan['fupuprate']=="")?"":round($this->plan_model->convertTodata($plan['fupuprate']."KB")); ?>" name="fup_upld_rate">
                                                                            <span class="title_box">Kbps</span>
                                                                            <label>Upload Rate<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                     </div>
                                                                     
                                                                   
                                                                </div>
                                                                    
                                                                  
                                                                   
                                                                </div>
                                                            </div>
                                                           
                                                           
                                                           
                                                           
                                                           
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px;">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                         <input type="submit" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="mui-btn mui-btn--accent btn-lg btn-block btn_submit"  value="SAVE & EXIT" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                     
                                        <div class="mui-tabs__pane " id="Plan-settings">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                      <form action="" method="post" id="add_plan_setting" autocomplete="off" onsubmit="add_plan_setting(); return false;">
                                                        <input type="hidden"  class="plan_id" name="plan_id" value="<?php echo $plan['srvid'];?>">
                                                         <input type="hidden" id="" class="is_frmedit" name="is_frmedit" value="1">
                                                          
                                                        <div class="form-group">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <h2>Plan Settings</h2>
                                                                <div class="row">
                                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                        <div class="row">
                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:5px">
                                                                                <h4 class="toggle_heading"> Enable Burst Mode</h4>
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                                <label class="switch">
                                                                                      <input type="checkbox" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> <?php echo  ($plan['enableburst']==1)?"checked":"";?>>
                                                                                    <div class="slider round sliderburst"></div>
                                                                                </label>
                                                                            </div>
                                                                            <input type="hidden" name="is_burst_enable" id="is_burst_enable" value="<?php echo $plan['enableburst'];?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"
                                                            <h5>Burst speed and burst threshhold should be greater than the plan main speed</h5>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0"  <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="dwnld_burst" name="dwnld_burst" value="<?php  echo ($plan['dlburstlimit']==0 || $plan['dlburstlimit']=="")?"":round($this->plan_model->convertTodata($plan['dlburstlimit']."KB")); ?>" >
                                                                            <span class="title_box">Kbps</span>
                                                                            <label>Download Burst Speed<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="upld_burst" name="upld_burst" value="<?php  echo ($plan['ulburstlimit']==0 || $plan['ulburstlimit']=="")?"":round($this->plan_model->convertTodata($plan['ulburstlimit']."KB")); ?>" >
                                                                            <span class="title_box">Kbps</span>
                                                                            <label>Upload Burst Speed<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="dwnld_threshold" name="dwnld_threshold" value="<?php  echo ($plan['dlburstthreshold']==0 || $plan['dlburstthreshold']=="")?"":round($this->plan_model->convertTodata($plan['dlburstthreshold']."KB")); ?>" >
                                                                            <span class="title_box">Kbps</span>
                                                                            <label>Download Burst Threshold<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="upld_threshold" name="upld_threshold" value="<?php  echo ($plan['ulburstthreshold']==0 || $plan['ulburstthreshold']=="")?"":round($this->plan_model->convertTodata($plan['ulburstthreshold']."KB")); ?>" >
                                                                            <span class="title_box">Kbps</span>
                                                                            <label>Upload Burst Threshold*<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="burst_time" name="burst_time" value="<?php  echo ($plan['bursttime']==0 || $plan['bursttime']=="")?"":$plan['bursttime']; ?>" >
                                                                             <span class="title_box">(seconds)</span>
                                                                            <label>Burst Time<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="burst_priority" name="burst_priority"  value="<?php echo ($plan['priority']==0 || $plan['priority']=="")?"":$plan['priority'];?>" >
                                                                            <label>Burst Priority<sup>*</sup></label>
                                                                             <span class="brst_prerr" style="color:red;"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                           
                                                        
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px;">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <input type="submit" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE & EXIT" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mui-tabs__pane " id="Pricing">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <form action="" method="post" id="add_pricing" autocomplete="off" onsubmit="add_pricing(); return false;">
                                                         <input type="hidden"  class="plan_id" name="plan_id"  value="<?php echo $plan['srvid'];?>">
                                                          <input type="hidden" id="" class="is_frmedit" name="is_frmedit" value="1">
                                                        <h2>Plan Pricing</h2>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="gross_amt" disabled <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="gross_amt" name="gross_amt" value="<?php echo round($plan['gross_amt']);?>" required>
                                                                        <span class="title_box_left"><?php echo $ispcodet['currency'] ?></span>
                                                                        <label>Gross Amount<sup>*</sup></label>
                                                                        <span class="amterror" style="color:red;"></span>
                                                                    </div>
                                                                </div>
                                                                 <input type="hidden" name="tax" class="tax" value="<?php echo $tax['tax']?>">
                                                            </div>
                                                        </div>

                                          

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px">
                                                                    <div class="mui-pricing">
                                                                        <h3>TOTAL AMOUNT</h3>
                                                                        <?php $net_amt=$plan['gross_amt']+($tax['tax']/100*$plan['gross_amt']);?>
                                                                        <h2><small><?php echo $ispcodet['currency'] ?></small><span id="net_amt"> <?php echo ceil($net_amt);?></span>.00</h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                        <input type="hidden" name="net_amount" id="net_amount" value="<?php echo ceil($net_amt);?>">

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px; margin-top: 20px;">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                   <input type="submit" disabled <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="btn_pricing mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE & EXIT" >
                                                                </div>
                                                            </div>
                                                        </div>      
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="mui-tabs__pane" id="add-region">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <form action="" method="post" id="add_region" autocomplete="off" onsubmit="add_region(); return false;">
                                                  <input type="hidden"  class="plan_id" name="plan_id" value="<?php echo $plan['srvid'];?>">
                                                   <input type="hidden" id="" class="is_frmedit" name="is_frmedit" value="1">
                                                    <div class="row">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                     <h4>Region</h4>
                     
                   <!--  <label class="radio-inline">
                     <input type="radio" required <?php echo (isset($ro) && $ro==1)?"disabled":"";?>  <?php echo ($deptregion_type=='allindia')?"":"disabled";?> name="region" <?php echo ($plan['region_type']=="allindia")?"checked":""; ?>  value="allindia" > All india
                     </label>-->
                     <label class="radio-inline">
                     <input type="radio" required <?php echo (isset($ro) && $ro==1)?"disabled":"";?>   name="region" <?php echo ($plan['region_type']=="region")?"checked":""; ?> value="region" > Region wise
                     </label>
                  </div>
                                                </div>
                                                <div class='row '>
                                                    <input type="hidden" name="regiondat" id="regiondat" value="">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo ($plan['region_type']=="region")?"":"hide"; ?> regionappend">
                                                       
                               <?php
                               $i=0;
                               foreach($plan_region as $vald){       ?>                 
                     <div class="row xxxx">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                           <div class="">
                               
                              <!-- onchange="search_filter_city(this.value)"-->
                              <select name="state" disabled <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="statelist form-control rmsel" id="statelist"    required>
                                 <option value="">Select States</option>
                                 <?php $this->plan_model->state_list($vald->state_id); ?>
                              </select>
                                <input type="hidden" class="statesel" name="statesel" value="<?php echo $vald->state_id;?>">
                              <!--<label>State</label>-->
                          
                        </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                           <div class="">
                              <select name="city" disabled <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="search_citylist form-control rmsel" required>
                                
                                 <?php $this->plan_model->getcitylist($vald->state_id,$vald->city_id); ?>
                              </select>
                                <input type="hidden" class="citysel" name="citysel" value="<?php echo $vald->city_id;?>">
                              <!--<label>City</label>-->
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                           <div class="">
                              <select name="zone" disabled <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="zone_list form-control rmsel" required>
                              
                                <?php $this->plan_model->getzonelist($vald->city_id,$vald->zone_id,$vald->state_id); ?>
                              </select>
                              <!--<label>Zone</label>-->
                           </div>
                        </div>
                          <div class="col-lg-3 col-md-3 col-sm-3 btnreg">
                           <button <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="mui-btn mui-btn--small mui-btn--accent addregion">
                                 + ADD Region
                                 </button>
                              <?php if($i>0){ ?>
                              <span class="rem"></span>
                              <?php }
                              else{?>
                              <span class="rem" <?php echo (isset($ro) && $ro==1)?"disabled":"";?>></span>
                              <?php } ?>
                            
                        </div>
                         
                        <input type="hidden" class="valregion" rel="" data-stateid="<?php echo $vald->state_id;?>" data-cityid="<?php echo ($vald->city_id!=0 && $vald->city_id!='')?$vald->city_id:'all';?>" data-zoneid="<?php echo ($vald->zone_id!=0 && $vald->zone_id!='')?$vald->zone_id:'all';?>" data-mapid="<?php echo $vald->id;?>">
                     </div>
                               <?php
                               
                               $i++;
                               
                               } ?>
                                     
                                                       <?php if($plan['region_type']=="allindia" || count($plan_region)==0){?>
                                    <div class="row xxxx">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                           <div class="">
                               
                              <!-- onchange="search_filter_city(this.value)"-->
                              <select name="state" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="statelist form-control rmsel" id="statelist"    required>
                                 <option value="">Select States</option>
                                 <?php $this->plan_model->state_list(); ?>
                              </select>
                              <input type="hidden" class="statesel" name="statesel" value="">
                              <!--<label>State</label>-->
                          
                        </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                           <div class="">
                              <select name="city" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="search_citylist form-control rmsel" >
                                 <option value="all">All Cities</option>
                                 
                              </select>
                                <input type="hidden" class="citysel" name="citysel" value="">
                              <!--<label>City</label>-->
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                           <div class="">
                              <select name="zone" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="zone_list form-control rmsel" >
                                 <option value="all">All Zones</option>
                                
                              </select>
                              <!--<label>Zone</label>-->
                           </div>
                        </div>
                          <div class="col-lg-3 col-md-3 col-sm-3 btnreg">
                           <button <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="mui-btn mui-btn--small mui-btn--accent addregion">
                                 + ADD Region
                                 </button>
                              <span class="rem" <?php echo (isset($ro) && $ro==1)?"disabled":"";?>></span>
                            
                        </div>
                         
                        <input type="hidden" class="valregion" rel="" data-stateid="all" data-cityid="all" data-zoneid="all" data-mapid="">
                     </div>   
                                                       
                                                       <?php } ?>
                                                       
                                                       
                  </div>  
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px; margin-top: 20px;">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                   <input type="submit" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE & EXIT" >
                                                                </div>
                                                            </div>
                                                    </div>   
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="mui-tabs__pane" id="plan-nas-assoc">
                                            <form action="" method="post" id="add_plannasform" autocomplete="off" onsubmit="plannas_configuration(); return false;">
                                                <input type="hidden"  class="plan_id" name="plan_id" value="<?php echo $plan['srvid'];?>">
                                                <input type="hidden" id="" class="is_frmedit" name="is_frmedit" value="1">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="row" id="plan_nas_listing">
                                                        Usage Stats
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px; margin-top: 20px;">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                            <input type="submit" <?php echo (isset($ro) && $ro==1)?"disabled":"";?> class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE & EXIT" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        
       // $('.tab_menu').css({'cursor' : 'not-allowed'});
            //   $('.tab_menu').attr('disabled','disabled');
        
        var height = $(window).height();
        $('#main_div').css('height', height);
        $('#right-container-fluid').css('height', height);

        // on user type change change page(form)
        $('input[type=radio][name=plan_type_radio]').change(function () {
            if (this.value == 'FUP_Plan') {
                $("#user_type_ful_plan_enquiry").show();
            } else if (this.value == 'Unlimited') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Data_Usage') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Time_Usage') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Others') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Others') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Offer_Plan') {
                $("#user_type_ful_plan_enquiry").hide();
            }
        });
        
        
        
        
        $(document).on('click','.addregion',function(){
           
          var $block = $(this).closest('.row');

            var $clone = $block.clone();

      $clone.find('.statelist ').prop("disabled", false);
       $clone.find('.search_citylist').prop("disabled", false);
        $clone.find('.zone_list').prop("disabled", false);
    $clone.find('option:selected').prop("selected", false);
    $clone.find('.search_citylist').html('').append('<option  value="all">All Cities</option>');
    $clone.find('.zone_list').html('').append('<option  value="all">All Zones</option>');
    $clone.find('.valregion').attr('data-zoneid','');
    $clone.find('.valregion').attr('data-mapid','');
    $clone.find('.valregion').attr('data-cityid','');
    $clone.find('.valregion').attr('data-stateid','');
    $clone.appendTo('.regionappend').find('.rem').html("<button class='mui-btn mui-btn--small mui-btn--accent remregion'>-</button>");  
          return false;
        });
        
        $(document).on('click','.remregion',function(){
            
            $(this).closest('.row').remove();
            var id=$(this).attr('rel');
            if(id!=="" && id!==undefined){
                    $.ajax({
             url: base_url+'plan/delete_region_plan',
             type: 'POST',
             dataType: 'text',
             data: 'id='+id,
             success: function(data){

                
             }


         });
    }
        });
        
         $(document).on('change','input:radio[name="region"]',function(){
         
         if($(this).val()==="region")
         {
             $('.regionappend').removeClass('hide');
              $('.statelist').attr('required',true);
              $('.search_citylist').attr('required',true);
              $('.zone_list').attr('required',true);
         }
         else
         {
              $('.regionappend').addClass('hide');
               $('.statelist').attr('required',false);
               $('.search_citylist').attr('required',false);
              $('.zone_list').attr('required',false);
         }
         
        });
        
      $(document).on('change','.statelist',function(){
          var stateid=$(this).val();
          var $this=$(this);
           $(this).closest('.row').find('.statesel').val(stateid);
          $this.closest('.row').find('.valregion').data('stateid',stateid);
          $this.closest('.row').find('.valregion').data('cityid','all');
            $this.closest('.row').find('.valregion').data('zoneid','all');
          $.ajax({
        url: base_url+'plan/getcitylist',
        type: 'POST',
        dataType: 'text',
        data: 'stateid='+stateid,
        success: function(data){
        
           $this.closest('.row').find('.search_citylist').empty();
             $this.closest('.row').find('.search_citylist').append(data);
        }
        
        
    });
         
      });
      
       $(document).on('change','.search_citylist',function(){
            
	     if($(this).val()=="addc")
          {
            $('.city_text').val('');
            var state_id=$(this).closest('.row').find('.statelist').val();
            $('.state_id').val(state_id);
            $('#add_city').modal('show'); 
            return false;
          }
          var cityid=$(this).val();
          var stateid=$(this).closest('.row').find('.statelist').val();
          var $this=$(this);
          $(this).closest('.row').find('.citysel').val(cityid);
          
       
          $this.closest('.row').find('.valregion').data('cityid',cityid);
          $.ajax({
        url: base_url+'plan/getzonelist',
        type: 'POST',
        dataType: 'text',
        data: 'cityid='+cityid+'&stateid='+stateid,
        success: function(data){
         
           $this.closest('.row').find('.zone_list').empty();
             $this.closest('.row').find('.zone_list').append(data);
        }
        
        
    });
         
      });
      
      
       $(document).on('change','.zone_list',function(){
           if($(this).val()=="addz"){
                        $('.zone_text').val('');
                 var state_id=$(this).closest('.row').find('.search_citylist').val();
                 var city_id=$(this).closest('.row').find('.search_citylist').val();
                 $('.state_id').val(state_id);
                 $('.city_id').val(city_id);
                $('#add_zone').modal('show'); 
                return false;
           }
          var zoneid=$(this).val();
          var $this=$(this);
            $this.closest('.row').find('.valregion').data('zoneid',zoneid);
         
      });

    });
    
    
       
function add_region()
{
    	var regionarr=[];
        $( ".valregion" ).each(function() {
            var stateid=$(this).data('stateid');
            var cityid=$(this).data('cityid');
            var zoneid=$(this).data('zoneid');
             var mapid=$(this).data('mapid');
            var fd=stateid+"::"+cityid+"::"+zoneid+"::"+mapid;
            	regionarr.push(fd);
});
$('#regiondat').val(regionarr);
      var formdata = $("#add_region").serialize();
      
      
      
   // alert(data);
    $('.loading').removeClass('hide');
     $.ajax({
        url: base_url+'plan/add_region',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
          //  alert(data);
               window.location = base_url+"plan";
           $('.loading').addClass('hide');
           /*  mui.tabs.activate('Plan-settings');
             
            $('.plan_id').val(data);
          $('.tab_menu').css({'cursor' : 'pointer'});
            $('.tab_menu').removeAttr('disabled');*/
           /* $('#usertype_customer').attr('checked','checked');
            $('.radio-inline').css('color', '#b3b3b3');
            $('input[type="radio"][name="user_type_radio"]').attr('disabled','disabled');
            mui.tabs.activate('KYC_details');*/
        }
  });
}
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date').bootstrapMaterialDatePicker({
            format: 'DD-MM-YYYY',
            time: false,
            clearButton: true
        });
        
         $('.time').bootstrapMaterialDatePicker
   ({
    date: false,
    shortTime: false,
    format: 'HH:mm:ss'
   });

        $('.date-start').bootstrapMaterialDatePicker({
            weekStart: 0, format: 'DD-MM-YYYY HH:mm', shortTime: true
        })
                .on('change', function (e, date) {
                    $('.date-end').bootstrapMaterialDatePicker('setMinDate', date);
                });

        $('.min-date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY HH:mm', minDate: new Date()});

        $.material.init()
    });
</script>
<script type="text/javascript">
    function toggleChevron(e) {
        $(e.target)
                .prev('.panel-heading')
                .find("i.")
                .toggleClass('fa fa-caret-down fa fa-caret-right');
    }
    $('#accordion').on('hidden.bs.collapse', toggleChevron);
    $('#accordion').on('shown.bs.collapse', toggleChevron);
</script>
<script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
        

      </script>

<script type="text/javascript">
    $('body').on('click', '.plan_nas_assoc', function(){
        $('#plan_nas_listing').html('');
        var planid = $('#add_plannasform .plan_id').val();
        $.ajax({
            url: base_url+'plan/listing_nas',
            type: 'POST',
            dataType: 'json',
            data: 'planid='+planid,
            async: false,
            success: function(data){
                $('#plan_nas_listing').html(data.nas_listing);
            }
        });
        nas_status();
    });
    function nas_status() {
        $( ".nasnameip" ).each(function( ) {
            var $this=$(this);
            var nasip=$(this).val();
            $.ajax({
                url: base_url+'nas/status_nasip',
                type: 'POST',
                dataType: 'text',
                data: 'nasip='+nasip,
                success: function(data){
                    var active= base_url+"assets/images/green.png";
                    var inactive= base_url+"assets/images/red.png";
                    if(data==0){
                        $this.closest('li').find('.nasimg').attr('src', active);
                    }else{
                        $this.closest('li').find('.nasimg').attr('src', inactive);
                    }
                }
            });
        });
    }
    
    $('body').on('change', '.allnasids', function(){
        var status = this.checked;
        $('.nasidsopt').each(function(){ 
           this.checked = status;
        });
    });
     
    $('body').on('change', '.nasidsopt', function(){
        if(this.checked == false){ 
            $(".allnasids")[0].checked = false; 
        }
        if ($('.nasidsopt:checked').length == $('.nasidsopt').length ){
            $(".allnasids")[0].checked = true; 
        }
    });
    
    function plannas_configuration(){
        seluids = [];
        $("input:checkbox[name=plannasids]:checked").each(function(){
           seluids.push($(this).val());
        });
        if (seluids.length > 0) {
            var planid = $('#add_plannasform .plan_id').val();
           $.ajax({
                url: base_url+'plan/plannas_configuration',
                type: 'POST',
                data: 'selnasids='+seluids+'&planid='+planid,
                success: function(result){
                    window.location = base_url+"plan/edit_plan/"+planid;
                }
           });
        }
    }
</script>
<?php $this->load->view('includes/footer'); ?>