<?php $this->load->view('includes/header'); ?>
<div class="loading" style="display:none">
   <img src="assets/images/loader.svg"/>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="wapper">
         <div id="sidedrawer" class="mui--no-user-select">
            <div id="sidedrawer-brand" class="mui--appbar-line-height">
               <span class="mui--text-title">
               <img src="<?php echo base_url() ?>assets/images/ozone-logo.png" class="img-responsive" width="80%"/>
               </span>
            </div>
            <?php $this->view('left_nav'); ?>
         </div>
         <header id="header">
            <nav class="navbar navbar-default">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <a class="navbar-brand" href="#">Top Up</a>
                  </div>
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                           <a href="add_user.html">
                           <button class="mui-btn mui-btn--small mui-btn--accent">
                           + ADD USER
                           </button>
                           </a>
                        </li>
                        <li>
                           <a href="add_plan.html">
                           <button class="mui-btn mui-btn--small mui-btn--accent">
                           + ADD Topup
                           </button>
                           </a>
                        </li>
                        <!--<li>
                           <a href="#">
                           <button class="mui-btn mui-btn--small mui-btn--accent">
                           + ADD LOCATION
                           </button>
                           </a>
                           </li>-->
                     </ul>
                  </div>
               </div>
            </nav>
         </header>
         <div id="content-wrapper">
            <div class="mui--appbar-height"></div>
            <div class="mui-container-fluid">
               <div class="add_user">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <div class="row">
                              <ul class="mui-tabs__bar plan_mui-tabs__bar">
                                 <li class="mui--is-active Plan-details limiui">
                                    <a data-mui-toggle="tab" data-mui-controls="topup-details">
                                    TopUp Details
                                    </a>
                                 </li>
                                 <li class="  Pricing limiui">
                                    <a data-mui-toggle="tab" data-mui-controls="Pricing" class="tab_menu">
                                    Pricing
                                    </a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <div class="row">
                              <div class="mui--appbar-height"></div>
                              <div class="mui-tabs__pane mui--is-active " id="topup-details">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                       <form action="" method="post" id="add_topup" autocomplete="off" onsubmit="add_topup(); return false;">
                                          <h2>TopUp Details</h2>
                                          <input type="hidden" id="topup_id" class="topup_id" name="topup_id" value="">
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px">
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="topup_name" required>
                                                         <label>Top Up Name<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="text" name="topup_desc" required>
                                                         <label>Top Up  Description</label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="number"  name="topup_validity" required>
                                                         <label>Top Up Validity</label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <div class="row" style="margin-top:24px">
                                                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                            <h4 class="toggle_heading"> Enable for Use</h4>
                                                         </div>
                                                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                            <label class="switch">
                                                               <input type="checkbox" >
                                                               <div class="slider round slide_usable"></div>
                                                            </label>
                                                         </div>
                                                         <input type="hidden" name="is_usable" id="is_usable" value="0">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:5px;">
                                                <h4>TYPE OF TopUp</h4>
                                                <div class="row" style="margin-top: 10px;">
                                                   <?php foreach($topup_type as $valplan){
                                                      $checked=($valplan->id==9)?"checked":"";
                                                      ?>
                                                   <label class="radio-inline">
                                                   <input type="radio" required name="topup_type_radio"  value="<?php echo $valplan->id;?>" <?php echo $checked;?>> <?php echo $valplan->misc_name;?>
                                                   </label>
                                                   <?php } ?>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group" id="user_type_ful_plan_enquiry" style="display: block">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:20px;">
                                                <div class="row data_topup">
                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                      <div class="mui-textfield mui-textfield--float-label">
                                                         <input type="number" class="topup_data" name="topup_data" required  />
                                                         <span class="title_box">GB</span>
                                                         <label>Data<sup>*</sup></label>
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
                                                      <h4>Data to be added</h4>
                                                      <label class="radio-inline">
                                                      <input type="radio" class="data_added" required name="data_added"  value="Download Only"> Download Only
                                                      </label>
                                                      <label class="radio-inline">
                                                      <input type="radio" class="data_added" required name="data_added"  value="Download Upload Both"> Download + Upload Both
                                                      </label>
                                                   </div>
                                                </div>
                                                <div class="unaccountancy" style="margin-top:15px; display:none;">
                                                   <div class ="row " >
                                                      <div class="mui-select col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <select class="unaccounting" name="unaccounting" required>
                                                            <?php for($i=0; $i<=300; $i++ ){ if($i%10==0){ ?>
                                                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                            <?php }
                                                               }
                                                               ?>
                                                         </select>
                                                         <label>Un-accounting %<sup>*</sup></label>
                                                      </div>
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <div class="mui-textfield mui-textfield--float-label">
                                                            <input type="text" class='timehh unacctstart_time' name="unacctstart_time" required />
                                                            <span class="title_box">GB</span>
                                                            <label>Un-accounting Time Start<sup>*</sup></label>
                                                         </div>
                                                      </div>
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <div class="mui-textfield mui-textfield--float-label">
                                                            <input type="text" class='timehh unacctstop_time' name="unacctstop_time" required />
                                                            <span class="title_box">GB</span>
                                                            <label>Un-accounting Time Stop<sup>*</sup></label>
                                                         </div>
                                                      </div>
                                                       
                                                     
                                                           
                                                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                           <div class="form-group">
                                                               <div class="col-lg-4 col-md-4 col-sm-4">
                                                                   <select id="dates-field2" required class="multiselect-ui form-control unacct_days" multiple="multiple">
                                                                       <option value="Monday">Monday</option>
                                                                       <option value="Tuesday">Tuesday</option>
                                                                       <option value="Wednesday">Wednesday</option>
                                                                       <option value="Thursday">Thursday</option>
                                                                       <option value="Friday">Friday</option>
                                                                       <option value="Saturday">Saturday</option>
                                                                       <option value="Sunday">Sunday</option>
                                                                   </select>
                                                               </div>
                                                           </div>
                                                           
                                                          
                                                           
                                                      
                                                           
                                                        
                                                           
                                                       </div>
                                                       
                                                   </div>
                                                    
                                                    
                                                   
                                                </div>
                                                <div class="speed_topup" style="display:none;">
                                                   <div class="row " style="margin-top: 15px; ">
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <div class="mui-textfield mui-textfield--float-label">
                                                            <input type="number" class="dwnld_speed" name="dwnld_speed" placeholder="" required>
                                                            <label>Download Speed<sup>*</sup></label>
                                                         </div>
                                                      </div>
                                                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                         <label class="radio-inline">
                                                         <input type="radio" class="dwnld_speed_multiple" name="dwnld_speed_multiple"  value="1x"> 1x
                                                         </label>
                                                         <label class="radio-inline">
                                                         <input type="radio" class="dwnld_speed_multiple" name="dwnld_speed_multiple"  value="2x"> 2x 
                                                         </label>
                                                         <label class="radio-inline">
                                                         <input type="radio" class="dwnld_speed_multiple" name="dwnld_speed_multiple"  value="3x"> 3x 
                                                         </label>  
                                                      </div>
                                                   </div>
                                                   <div class="row " style="margin-top: 15px; display:block;">
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <div class="mui-textfield mui-textfield--float-label">
                                                            <input type="number" class="upld_speed" name="upld_speed" required placeholder="" >
                                                            <label>Upload Speed<sup>*</sup></label>
                                                         </div>
                                                      </div>
                                                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                         <label class="radio-inline">
                                                         <input type="radio" class="upld_speed_multiple" name="upld_speed_multiple"  value="1x"> 1x
                                                         </label>
                                                         <label class="radio-inline">
                                                         <input type="radio" class="upld_speed_multiple" name="upld_speed_multiple"  value="2x"> 2x 
                                                         </label>
                                                         <label class="radio-inline">
                                                         <input type="radio" class="upld_speed_multiple" name="upld_speed_multiple"  value="3x"> 3x 
                                                         </label>  
                                                      </div>
                                                   </div>
                                                   <div class="row " style="margin-top: 15px; display:block;">
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <div class="mui-textfield mui-textfield--float-label">
                                                            <input type="text" class="speed_boost_start timehh" required   name="speed_boost_start" placeholder="" >
                                                            <label>Speed Boost Start<sup>*</sup></label>
                                                         </div>
                                                      </div>
                                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <div class="mui-textfield mui-textfield--float-label">
                                                            <input type="text" class="speed_boost_stop timehh" required name="speed_boost_stop" placeholder="" >
                                                            <label>Speed Boost Stop<sup>*</sup></label>
                                                         </div>
                                                      </div>
                                                       <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                           <div class="form-group">
                                                               <div class="col-lg-4 col-md-4 col-sm-4">
                                                                   <select id="dates-field2" class="multiselect-ui form-control speedboost_days" multiple="multiple">
                                                                       <option value="Monday">Monday</option>
                                                                       <option value="Tuesday">Tuesday</option>
                                                                       <option value="Wednesday">Wednesday</option>
                                                                       <option value="Thursday">Thursday</option>
                                                                       <option value="Friday">Friday</option>
                                                                       <option value="Saturday">Saturday</option>
                                                                       <option value="Sunday">Sunday</option>
                                                                   </select>
                                                               </div>
                                                           </div>
                                                           
                                                          
                                                           
                                                       </div>
                                                   </div>

                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;margin-top: 15px;">
                                            
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block btn_submit"  value="NEXT">
                                                </div>
                                             
                                          </div>
                                    </div>
                                    </form>
                                 </div>
                              </div>
                              <div class="mui-tabs__pane " id="Pricing">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                       <form action="" method="post" id="add_pricing" autocomplete="off" onsubmit="add_topuppricing(); return false;">
                                          <input type="hidden"  class="topup_id" name="topup_id" value="">
                                          <h2>Plan Pricing</h2>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                             <div class="row">
                                                <label class="radio-inline">
                                                <input type="radio" class="data_calcn" name="data_calcn_radio"  value=""> 1x
                                                </label>
                                                <label class="radio-inline">
                                                <input type="radio" class="data_calcn" name="data_calcn_radio"  value=""> 2x 
                                                </label>
                                             </div>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                             <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="gross_amt" class="gross_amt" name="gross_amt" required>
                                                      <label>₹ Gross Amount<sup>*</sup></label>
                                                      <span class="amterror"></span>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="number" class="tax" name="tax" required>
                                                      <label>% Tax<sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px">
                                                   <div class="mui-pricing">
                                                      <h3>TOTAL AMOUNT</h3>
                                                      <h2><small>₹</small><span id="net_amt"> 00</span>.00</h2>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <input type="hidden" name="net_amount" id="net_amount" value="">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px; margin-top: 20px;">
                                             <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="DONE" >
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   $(document).ready(function () {
       
       $('.tab_menu').css({'cursor' : 'not-allowed'});
              $('.tab_menu').attr('disabled','disabled');
       
       var height = $(window).height();
       $('#main_div').css('height', height);
       $('#right-container-fluid').css('height', auto);
   
       // on user type change change page(form)
       $('input[type=radio][name=plan_type_radio]').change(function () {
           if (this.value == 'FUP_Plan') {
               $("#user_type_ful_plan_enquiry").show();
           } else if (this.value == 'Unlimited') {
               $("#user_type_ful_plan_enquiry").hide();
           } else if (this.value == 'Data_Usage') {
               $("#user_type_ful_plan_enquiry").hide();
           } else if (this.value == 'Time_Usage') {
               $("#user_type_ful_plan_enquiry").hide();
           } else if (this.value == 'Others') {
               $("#user_type_ful_plan_enquiry").hide();
           } else if (this.value == 'Others') {
               $("#user_type_ful_plan_enquiry").hide();
           } else if (this.value == 'Offer_Plan') {
               $("#user_type_ful_plan_enquiry").hide();
           }
       });
   
   });
</script>
<script type="text/javascript">
   $(document).ready(function () {
       $('.date').bootstrapMaterialDatePicker({
           format: 'DD-MM-YYYY',
           time: false,
           clearButton: true
       });
       
        $('.timehh').bootstrapMaterialDatePicker
   ({
   date: false,
   shortTime: false,
   format: 'HH'
   });
   
       $('.date-start').bootstrapMaterialDatePicker({
           weekStart: 0, format: 'DD-MM-YYYY HH:mm', shortTime: true
       })
               .on('change', function (e, date) {
                   $('.date-end').bootstrapMaterialDatePicker('setMinDate', date);
               });
   
       $('.min-date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY HH:mm', minDate: new Date()});
   
       $.material.init()
   });
</script>
<script type="text/javascript">
   function toggleChevron(e) {
       $(e.target)
               .prev('.panel-heading')
               .find("i.")
               .toggleClass('fa fa-caret-down fa fa-caret-right');
   }
   $('#accordion').on('hidden.bs.collapse', toggleChevron);
   $('#accordion').on('shown.bs.collapse', toggleChevron);
</script>
<script type="text/javascript">
  $(function() {
   $('.multiselect-ui').multiselect({
    includeSelectAllOption: true
   });
  });
    </script>
<?php $this->load->view('includes/footer'); ?>