<?php $this->load->view('includes/header'); ?>
<div class="loading hide" >
    <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="wapper">
            <div id="sidedrawer" class="mui--no-user-select">
                <div id="sidedrawer-brand" class="mui--appbar-line-height">
                    <span class="mui--text-title">
                           <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                        <img src="<?php echo $img ?>" class="img-responsive" />
                    </span>
                </div>
                <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
                 $this->view('left_nav',$data); ?>
            </div>
            <header id="header">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">Top-Up</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <?php $this->load->view('plan/plan_headerview',$data); ?>
                        </div>
                    </div>
                </nav>
            </header>
            <div id="content-wrapper">
                <div class="mui--appbar-height"></div>
                <div class="mui-container-fluid" id="right-container-fluid">
                    <div class="add_user">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <ul class="mui-tabs__bar plan_mui-tabs__bar">
                                            <li class="mui--is-active Plan-details limiui">
                                                <a data-mui-toggle="tab" data-mui-controls="topup-details">
                                                    Top-Up Details
                                                </a>
                                            </li>
                                            <li class="  Pricing limiui">
                                                <a data-mui-toggle="tab" data-mui-controls="Pricing" class="" >
                                                    Pricing
                                                </a>
                                            </li>
                                            <li class="  Pricing limiui">
                                                <a data-mui-toggle="tab" data-mui-controls="add-region" class="">
                                                    Region
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="mui--appbar-height"></div>
                                        <div class="mui-tabs__pane mui--is-active " id="topup-details">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <form action="" method="post" id="add_topup" autocomplete="off" onsubmit="add_topup(); return false;">

                                                        <input type="hidden" id="topup_id" class="topup_id" name="topup_id" value="">
                                                        <input type="hidden" id="" class="is_formedit" name="is_formedit" value="0">
                                                        <div class="form-group">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="text" maxlength="20"  name="topup_name" required>
                                                                            <label>Top-Up Name<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="text" name="topup_desc" >
                                                                            <label>Top-Up  Description</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="margin-top:25px">
                                                                        <label class="radio-inline">
                                                                        <input type="radio"  required name="access_type"  value="0" checked > Public Topup 
                                                                    </label>
                                                                 <label class="radio-inline">
                                                                        <input type="radio"  required name="access_type"  value="1" > private Topup 
                                                                    </label>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                                                <!--<div class="row">
                                                                    
                                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                    <div class="row" style="margin-top:24px">
                                                                       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                          <h4 class="toggle_heading"> Enable for Use</h4>
                                                                       </div>
                                                                       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                          <label class="switch">
                                                                             <input type="checkbox" >
                                                                             <div class="slider round slide_usable"></div>
                                                                          </label>
                                                                       </div>
                                                                       <input type="hidden" name="is_usable" id="is_usable" value="0">
                                                                    </div>
                                                                 </div>  
                                                                    
                                                                </div>-->
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px" style="padding-left:5px;">
                                                                <h4>Type of Top-Up</h4>
                                                                <div class="row" style="margin-top: 10px;">
                                                                    <label class="radio-inline">
                                                                        <input type="radio" required name="topup_type_radio"  value="1" checked> Data Top-Up 
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" required name="topup_type_radio"  value="2" > Data un-accountancy 
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" required name="topup_type_radio"  value="3" >  Speed Top-Up 
                                                                    </label>


                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" id="user_type_ful_plan_enquiry" style="display: block">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:20px;">
                                                                <div class="row data_topup">
                                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" class="topup_data" step="0.001" name="topup_data" required  />
                                                                            <span class="title_box">GB</span>
                                                                            <label>Data<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
                                                                        <h4>Data to be added</h4>
                                                                        <label class="radio-inline">
                                                                        <input type="radio" class="data_added" required name="data_added"  value="1"> Download Only
                                                                        </label>
                                                                        <label class="radio-inline">
                                                                        <input type="radio" class="data_added" required name="data_added"  value="2"> Download + Upload Both
                                                                        </label>
                                                                     </div>-->
                                                                </div>
                                                                <div class="unaccountancy" style="margin-top:15px; display:none;">
                                                                    <div class ="row " >
                                                                        <div class="mui-select col-lg-4 col-md-4 col-sm-4 col-xs-4" style="height:100px;">
                                                                            <select class="unaccounting"   name="unaccounting" >
                                                                                <?php for ($i = 0; $i <= 100; $i++) {
                                                                                    if ($i % 10 == 0 && $i != 0) { ?>
                                                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                                    <?php
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                            <label>Un-accounting %<sup>*</sup></label>
                                                                        </div>
                                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                            <div class="mui-textfield ">
                                                                                <input type="text" class='timehh unacctstart_time' name="unacctstart_time"  />

                                                                                <label>Un-accounting Time Start<sup>*</sup></label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                            <div class="mui-textfield ">
                                                                                <input type="text" class='timehh unacctstop_time' name="unacctstop_time"  />

                                                                                <label>Un-accounting Time Stop<sup>*</sup></label>
                                                                            </div>
                                                                        </div>



                                                                        <div class="form-group">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:5px;">
                                                                                <h4>Select Unaccounting Days</h4>
                                                                                <div class="row" style="margin-top: 10px;">
                                                                                    <label class="checkbox-inline ">
                                                                                        <input type="checkbox" class=""  id="inlineCheckbox1" name="unacctng_days[]" value=""> All Days
                                                                                    </label>
                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="unacct_days" required  id="inlineCheckbox2" name="unacctng_days[]" value="Mon"> Monday
                                                                                    </label>
                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="unacct_days"  id="inlineCheckbox3" name="unacctng_days[]" value="Tue"> Tuesday
                                                                                    </label>
                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="unacct_days"  id="inlineCheckbox4" name="unacctng_days[]" value="Wed"> Wednesday
                                                                                    </label>

                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="unacct_days"  id="inlineCheckbox5" name="unacctng_days[]" value="Thu"> Thursday
                                                                                    </label>

                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="unacct_days"  id="inlineCheckbox6" name="unacctng_days[]" value="Fri"> Friday
                                                                                    </label>

                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="unacct_days"  id="inlineCheckbox7" name="unacctng_days[]" value="Sat"> Saturday
                                                                                    </label>

                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="unacct_days"  id="inlineCheckbox8" name="unacctng_days[]" value="Sun">  Sunday
                                                                                    </label>

                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>



                                                                </div>
                                                                <div class="speed_topup" style="display:none;">
                                                                    <div class="row " style="margin-top: 15px; ">
                                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                            <div class="mui-textfield mui-textfield--float-label">
                                                                                <input type="number" min="0" class="dwnld_speed" name="dwnld_speed" placeholder="" >
                                                                                <span class="title_box">Kbps</span>
                                                                                <label>Download Speed<sup>*</sup></label>
                                                                            </div>
                                                                        </div>
                                                                        <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                            <label class="radio-inline">
                                                                            <input type="radio" class="dwnld_speed_multiple" name="dwnld_speed_multiple"  value="2x"> 2x
                                                                            </label>
                                                                            <label class="radio-inline">
                                                                            <input type="radio" class="dwnld_speed_multiple" name="dwnld_speed_multiple"  value="3x"> 3x 
                                                                            </label>
                                                                            <label class="radio-inline">
                                                                            <input type="radio" class="dwnld_speed_multiple" name="dwnld_speed_multiple"  value="4x"> 4x 
                                                                            </label>  
                                                                         </div>-->
                                                                    </div>
                                                                    <div class="row " style="margin-top: 15px; display:block;">
                                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                            <div class="mui-textfield mui-textfield--float-label">
                                                                                <input type="number" min="0" class="upld_speed" name="upld_speed"  placeholder="" >
                                                                                <span class="title_box">Kbps</span>
                                                                                <label>Upload Speed<sup>*</sup></label>
                                                                            </div>
                                                                        </div>
                                                                        <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                            <label class="radio-inline">
                                                                            <input type="radio" class="upld_speed_multiple" name="upld_speed_multiple"  value="2x"> 2x
                                                                            </label>
                                                                            <label class="radio-inline">
                                                                            <input type="radio" class="upld_speed_multiple" name="upld_speed_multiple"  value="3x"> 3x 
                                                                            </label>
                                                                            <label class="radio-inline">
                                                                            <input type="radio" class="upld_speed_multiple" name="upld_speed_multiple"  value="4x"> 4x 
                                                                            </label>  
                                                                         </div>-->
                                                                    </div>
                                                                    <div class="row " style="margin-top: 15px; display:block;">
                                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                            <div class="mui-textfield ">
                                                                                <input type="text" class="speed_boost_start timehh"    name="speed_boost_start" placeholder="" >
                                                                                <label>Speed Boost Start<sup>*</sup></label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                            <div class="mui-textfield ">
                                                                                <input type="text" class="speed_boost_stop timehh"  name="speed_boost_stop" placeholder="" >
                                                                                <label>Speed Boost Stop<sup>*</sup></label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:5px;">
                                                                                <h4>Select Speed Boost Days</h4>
                                                                                <div class="row" style="margin-top: 10px;">
                                                                                    <label class="checkbox-inline ">
                                                                                        <input type="checkbox" class=""  id="spinlineCheckbox1" name="speedboost_days[]" value=""> All Days
                                                                                    </label>
                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="speedboost_days" required  id="spinlineCheckbox2" name="speedboost_days[]" value="Mon"> Monday
                                                                                    </label>
                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="speedboost_days"  id="spinlineCheckbox3" name="speedboost_days[]" value="Tue"> Tuesday
                                                                                    </label>
                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="speedboost_days"  id="spinlineCheckbox4" name="speedboost_days[]" value="Wed"> Wednesday
                                                                                    </label>

                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="speedboost_days"  id="spinlineCheckbox5" name="speedboost_days[]" value="Thu"> Thursday
                                                                                    </label>

                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="speedboost_days"  id="spinlineCheckbox6" name="speedboost_days[]" value="Fri"> Friday
                                                                                    </label>

                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="speedboost_days"  id="spinlineCheckbox7" name="speedboost_days[]" value="Sat"> Saturday
                                                                                    </label>

                                                                                    <label class="checkbox-inline">
                                                                                        <input type="checkbox" class="speedboost_days"  id="spinlineCheckbox8" name="speedboost_days[]" value="Sun">  Sunday
                                                                                    </label>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;margin-top: 15px;">

                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block btn_submit"  value="NEXT">
                                                            </div>

                                                        </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="mui-tabs__pane " id="Pricing">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <form action="" method="post" id="add_pricing" autocomplete="off" onsubmit="add_topuppricing(); return false;">
                                                        <input type="hidden"  class="topup_id" name="topup_id" value="">
                                                        <input type="hidden" id="" class="is_formedit" name="is_formedit" value="0">
                                                        <h2>Top-Up Daily Cost</h2>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                            <div class="row">
                                                                <label class="radio-inline">
                                                                    <input type="radio" class="plan_pricing" name="plan_pricing_radio"  value="Paid" required > Paid
                                                                </label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" class="plan_pricing" name="plan_pricing_radio"  value="Free" required> Free 
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px">
                                                                    <div class="mui-textfield mui-textfield--float-label">
                                                                        <input type="gross_amt" class="gross_amt" name="gross_amt" required>
                                                                        <label><?php echo $ispcodet['currency'] ?> Gross Amount<sup>*</sup></label>
                                                                        <span class="amterror" style="color:red;"></span>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="tax" class="tax" value="<?php echo $tax['tax']?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px">
                                                                    <div class="mui-pricing">
                                                                        <h3>TOTAL AMOUNT</h3>
                                                                        <h2><small><?php echo $ispcodet['currency'] ?></small><span id="net_amt"> 00</span>.00</h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="net_amount" id="net_amount" value="">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px; margin-top: 20px;">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                    <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block btn_toppricing" disabled  value="NEXT" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mui-tabs__pane" id="add-region">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <form action="" method="post" id="add_region" autocomplete="off" onsubmit="add_region(); return false;">
                                                    <input type="hidden"  class="topup_id" name="topup_id" value="1">
                                                    <input type="hidden" id="" class="is_formedit" name="is_formedit" value="0">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                                                            <h4>Region</h4>
                                                        <!--    <label class="radio-inline">
                                                                <input type="radio" required name="region"  <?php echo ($region_type == "allindia") ? "" : "disabled"; ?> value="allindia" > All india
                                                            </label>-->
                                                            <label class="radio-inline">
                                                                <input type="radio" required name="region"  value="region" > Region wise
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class='row '>
                                                        <input type="hidden" name="regiondat" id="regiondat" value="">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hide regionappend">
                                                            <div class="row xxxx">
                                                                <div class="col-lg-3 col-md-3 col-sm-3">
                                                                    <div class="">

                                                                        <!-- onchange="search_filter_city(this.value)"-->
                                                                        <select name="state" class="statelist form-control" id="statelist"    required>
                                                                            <option value="">Select States</option>
<?php $this->plan_model->state_list(); ?>
                                                                        </select>
                                                                        <input type="hidden" class="statesel" name="statesel" value="">
                                                                        <!--<label>State</label>-->

                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-3">
                                                                    <div class="">
                                                                        <select name="city" class="search_citylist form-control" required>
                                                                            <option value="all">All Cities</option>

                                                                        </select>
                                                                        <input type="hidden" class="citysel" name="citysel" value="">
                                                                        <!--<label>City</label>-->
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-3">
                                                                    <div class="">
                                                                        <select name="zone" class="zone_list form-control" required>
                                                                            <option value="all">All Zone</option>

                                                                        </select>
                                                                        <!--<label>Zone</label>-->
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-3 btnreg">
                                                                    <button class="mui-btn mui-btn--small mui-btn--accent addregion">
                                                                        + ADD Region
                                                                    </button>
                                                                    <span class="rem"></span>

                                                                </div>

                                                                <input type="hidden" class="valregion" rel="" data-stateid="all" data-cityid="all" data-zoneid="all" data-mapid="">
                                                            </div>


                                                        </div>  
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px; margin-top: 20px;">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                    <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block btn_topregion" disabled value="SAVE" >
                                                                </div>
                                                            </div>
                                                        </div>   
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function () {

        $('.unacct_days').attr('required', false);
        $('.speedboost_days').attr('required', false);


        $('.tab_menu').css({'cursor': 'not-allowed'});
        $('.tab_menu').attr('disabled', 'disabled');

        var height = $(window).height();
        var herader_height = $("#header").height();
        height = height - herader_height;
        //$('#main_div').css('height', height);
        $('#right-container-fluid').css('height', height);

        // on user type change change page(form)
        $('input[type=radio][name=plan_type_radio]').change(function () {
            if (this.value == 'FUP_Plan') {
                $("#user_type_ful_plan_enquiry").show();
            } else if (this.value == 'Unlimited') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Data_Usage') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Time_Usage') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Others') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Others') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Offer_Plan') {
                $("#user_type_ful_plan_enquiry").hide();
            }
        });



        $(document).on('click', '.addregion', function () {

            var $block = $(this).closest('.row');

            var $clone = $block.clone();


            $clone.find('option:selected').prop("selected", false);
            $clone.find('.search_citylist').html('').append('<option  value="all">All Cities</option>');
            $clone.find('.zone_list').html('').append('<option  value="all">All Zones</option>');
            $clone.find('.valregion').attr('data-zoneid', '');
            $clone.find('.valregion').attr('data-mapid', '');
            $clone.find('.valregion').attr('data-cityid', '');
            $clone.find('.valregion').attr('data-stateid', '');
            $clone.appendTo('.regionappend').find('.rem').html("<button class='mui-btn mui-btn--small mui-btn--accent remregion'>-</button>");

            return false;
        });

        $(document).on('click', '.remregion', function () {

            $(this).closest('.row').remove();
        });

        $(document).on('change', 'input:radio[name="region"]', function () {

            if ($(this).val() === "region")
            {
                $('.regionappend').removeClass('hide');
                $('.statelist').attr('required', true);
                $('.search_citylist').attr('required', true);
                $('.zone_list').attr('required', true);

            } else
            {
                $('.regionappend').addClass('hide');
                $('.statelist').attr('required', false);
                $('.search_citylist').attr('required', false);
                $('.zone_list').attr('required', false);
            }

        });


        $(document).on('change', '.statelist', function () {
            var stateid = $(this).val();
            var $this = $(this);
            $(this).closest('.row').find('.statesel').val(stateid);
            $this.closest('.row').find('.valregion').data('stateid', stateid);
            $this.closest('.row').find('.valregion').data('cityid', 'all');
            $this.closest('.row').find('.valregion').data('zoneid', 'all');
            $.ajax({
                url: base_url + 'plan/getcitylist',
                type: 'POST',
                dataType: 'text',
                data: 'stateid=' + stateid,
                success: function (data) {

                    $this.closest('.row').find('.search_citylist').empty();
                    $this.closest('.row').find('.search_citylist').append(data);
                }


            });

        });

        $(document).on('change', '.search_citylist', function () {

            if ($(this).val() == "addc")
            {
                $('.city_text').val('');
                var state_id = $(this).closest('.row').find('.statelist').val();
                $('.state_id').val(state_id);
                $('#add_city').modal('show');
                return false;
            }
            var cityid = $(this).val();
            var stateid = $(this).closest('.row').find('.statelist').val();
            var $this = $(this);
            $(this).closest('.row').find('.citysel').val(cityid);

            $this.closest('.row').find('.valregion').data('cityid', cityid);
            $.ajax({
                url: base_url + 'plan/getzonelist',
                type: 'POST',
                dataType: 'text',
                data: 'cityid=' + cityid + '&stateid=' + stateid,
                success: function (data) {

                    $this.closest('.row').find('.zone_list').empty();
                    $this.closest('.row').find('.zone_list').append(data);
                }


            });

        });


        $(document).on('change', '.zone_list', function () {
            if ($(this).val() == "addz") {
                $('.zone_text').val('');
                var state_id = $(this).closest('.row').find('.search_citylist').val();
                var city_id = $(this).closest('.row').find('.search_citylist').val();
                $('.state_id').val(state_id);
                $('.city_id').val(city_id);
                $('#add_zone').modal('show');
                return false;
            }
            var zoneid = $(this).val();
            var $this = $(this);
            $this.closest('.row').find('.valregion').data('zoneid', zoneid);

        });

        /*  $(document).on('change','.search_citylist',function(){
         
         // $('.zncity').change(function(){
         alert('xxxxxx');
         var cityid=$(this).val(); 
         getZonelist(cityid) ;
         });*/





    });

    function add_region()
    {
        var regionarr = [];
        $(".valregion").each(function () {
            var stateid = $(this).data('stateid');
            var cityid = $(this).data('cityid');
            var zoneid = $(this).data('zoneid');
            var mapid = $(this).data('mapid');
            var fd = stateid + "::" + cityid + "::" + zoneid + "::" + mapid;
            regionarr.push(fd);
        });
        $('#regiondat').val(regionarr);
        var formdata = $("#add_region").serialize();



        // alert(data);
        $('.loading').removeClass('hide');
        $.ajax({
            url: base_url + 'plan/add_region_topup',
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function (data) {
                //  alert(data);
                window.location = base_url + "plan/topup";
                $('.loading').addClass('hide');
                /*  mui.tabs.activate('Plan-settings');
                 
                 $('.plan_id').val(data);
                 $('.tab_menu').css({'cursor' : 'pointer'});
                 $('.tab_menu').removeAttr('disabled');*/
                /* $('#usertype_customer').attr('checked','checked');
                 $('.radio-inline').css('color', '#b3b3b3');
                 $('input[type="radio"][name="user_type_radio"]').attr('disabled','disabled');
                 mui.tabs.activate('KYC_details');*/
            }
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date').bootstrapMaterialDatePicker({
            format: 'DD-MM-YYYY',
            time: false,
            clearButton: true
        });

        $('.timehh').bootstrapMaterialDatePicker
                ({
                    date: false,
                    shortTime: false,
                    format: 'HH'
                });

        $('.date-start').bootstrapMaterialDatePicker({
            weekStart: 0, format: 'DD-MM-YYYY HH:mm', shortTime: true
        })
                .on('change', function (e, date) {
                    $('.date-end').bootstrapMaterialDatePicker('setMinDate', date);
                });

        $('.min-date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY HH:mm', minDate: new Date()});

        $.material.init()
    });
</script>
<script type="text/javascript">
    function toggleChevron(e) {
        $(e.target)
                .prev('.panel-heading')
                .find("i.")
                .toggleClass('fa fa-caret-down fa fa-caret-right');
    }
    $('#accordion').on('hidden.bs.collapse', toggleChevron);
    $('#accordion').on('shown.bs.collapse', toggleChevron);
</script>
<script type="text/javascript">
    $(function () {
        $('.multiselect-ui').multiselect({
            includeSelectAllOption: true
        });
    });
</script>
<script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
        

      </script>
<?php $this->load->view('includes/footer'); ?>