 <!-- Start your project here-->
 <?php $this->load->view('includes/header');?>
      <div class="loading hide">
	   <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  <div id="sidedrawer-brand" class="mui--appbar-line-height">
                     <span class="mui--text-title">
                       <?php $img=(isset($ispdetail->logo_image) && $ispdetail->logo_image!='')?base_url()."ispmedia/logo/".$ispdetail->logo_image:base_url()."assets/images/decibel.png"?>
                        <img src="<?php echo $img ?>" class="img-responsive" />
                     </span>
                  </div>
                 <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
                 $this->view('left_nav',$data); ?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">Plans</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                          <?php $this->load->view('plan/plan_headerview',$data);?>
                        </div>
                     </div>
                  </nav>
               </header>
                
                
                <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                       <div class="right_side" style="height:auto; padding-bottom: 0px;">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-right">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-3 col-md-3 plans-padding">
                                            <div class="plans-thumbnail" style="background-color:#ffbc00;">
                                                <div class="caption">
                                                    <h5>Top Unlimited Plan</h5>
                                                     <h4><?php echo $plan_count['unlimited']['planname'];?></h4>
                                                 <h6>   <a href="<?php echo ($plan_count['unlimited']['usercount']>0)?base_url().'plan/user_stat/'.$plan_count['unlimited']['planid']:'#'; ?>" style=" text-decoration:underline; color:white;">
                                                        
                                       <?php echo $plan_count['unlimited']['usercount'];?> users using it 
                                        
                                                    </a></h6>
                                                      <h6><a href="<?php echo base_url()?>plan/plan_stat/unlimited" style=" text-decoration:underline; color:white;"><?php echo $plan_count['unlimited']['plancount'];?> Plans</a></h6>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-sm-3 col-md-3 plans-padding">
                                            <div class="plans-thumbnail" style="background-color:#F4474A;">
                                                <div class="caption">
                                                   <h5>Top Data Plan</h5>
                                                    <h4><?php echo $plan_count['Data']['planname'];?></h4>
                                                    <h6><a href="<?php echo ($plan_count['Data']['usercount']>0)?base_url().'plan/user_stat/'.$plan_count['Data']['planid']:'#'; ?>" style=" text-decoration:underline; color:white;"> 
                                      <?php echo $plan_count['Data']['usercount'];?> users using it
                                        </a></h6>
                                    <h6><a href="<?php echo base_url()?>plan/plan_stat/data" style=" text-decoration:underline; color:white;"><?php echo $plan_count['Data']['plancount'];?> Plans</a></h6>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-sm-3 col-md-3 plans-padding">
                                            <div class="plans-thumbnail" style="background-color:#29ABE2;">
                                                <div class="caption">
                                                   <h5>Top Fup Plan</h5>
                                                     <h4><?php echo $plan_count['FUP']['planname'];?></h4>
                                                     <h6> <a href="<?php echo($plan_count['FUP']['usercount']>0)?base_url().'plan/user_stat/'.$plan_count['FUP']['planid']:'#'; ?>" style=" text-decoration:underline; color:white;"> 
                                       <?php echo $plan_count['FUP']['usercount'];?> users using it</a> </h6>
                                   <h6> <a href="<?php echo base_url()?>plan/plan_stat/fup" style=" text-decoration:underline; color:white;"><?php echo $plan_count['FUP']['plancount'];?> Plans</a></h6>
                                                </div>
                                            </div>
                                        </div>
                                       <!-- <div class="col-sm-6 col-sm-3 col-md-3 plans-padding">
                                            <div class="plans-thumbnail" style="background-color:#29ABE2;">
                                                <div class="caption">
                                                    <h5>Top Time Plan</h5>
                                                     <h4><?php echo $plan_count['Time']['planname'];?></h4>
                                                    <h6> <a href="<?php echo($plan_count['Time']['usercount']>0)?base_url().'plan/user_stat/'.$plan_count['Time']['planid']:'#'; ?>" style=" text-decoration:underline; color:white;">
                                      <?php echo $plan_count['Time']['usercount'];?> users using it </a></h6>
                                <h6>  <a href="<?php echo base_url()?>plan/plan_stat/time" style=" text-decoration:underline; color:white;">  <?php echo $plan_count['Time']['plancount'];?> Plans</a></h6>
                                                </div>
                                            </div>
                                        </div>-->
                                    </div>
                                </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <!--<h1>1285 <small>Users</small></h1>-->
                           </div>
						   </div>
					  </div>
                     <div class="add_user" style="padding-top:0px;">
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <input type="hidden" name="filtertype" id="plantype" value="<?php echo $filtertype; ?>">
                           <div class="row">
                               
                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12">
   <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8">
         <h1 id="search_count"></h1>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 pull-right">
         <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 14px;">
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon">
                     <i class="fa fa-search" aria-hidden="true"></i>
                  </div>
                  <input type="text" class="form-control" placeholder="Search Plan" onBlur="this.placeholder='Search nas'" onFocus="this.placeholder=''"  id="searchtext">
                  <span class="searchclear" id="searchclear">
                  <img src="<?php echo base_url() ?>assets/images/clear.svg"/>
                  </span>
               </div>
               <label class="search_label">You can look up by Plan Name</label>
            </div>
         </div>
      </div>
   </div>
                                  
                                   <div class="row" id="onefilter">
      <div class="col-lg-8 col-md-8 col-sm-8">
         <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select name="filter_state" class='statelist' onchange="search_filter(this.value, 'state')">
                     <option value="all">All States</option>
                     <?php $this->plan_model->state_list(); ?>
                  </select>
                  <label>State</label>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select class="search_citylist"  onchange="search_filter(this.value, 'city')"  name="filter_city">
                     <option value="all">All Cities</option>
                  </select>
                  <label>City</label>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
               <div class="mui-select">
                  <select  onchange="search_filter()" class="search_zonelist"  name="filter_zone">
                        <option value="all">All Zones</option>
                     <?php //$this->user_model->zone_list(); ?>
                  </select>
                  <label>Zone</label>
               </div>
            </div>
         </div>
      </div>
     
   </div>
</div>
                              </div>
                           </div>
                               
                           </div>
                           
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                               <!-- <ul class="plan_mui-tabs__bar">
								  <li class="plan_mui--is-active">
								  <a data-mui-toggle="tab" data-mui-controls="pane-default-1" >PPPoE Plans</a>
								  </li>
								 
								</ul>-->
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                             <div class="row">
                              <div class="mui--appbar-height"></div>
                          		<div class="mui-tabs__pane mui--is-active" id="pane-default-1">
							      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                      <div class="row">
                                       <div class="table-responsive">
                                          <table class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>PLAN NAME</th>
                                                   <th>PLAN TYPE</th>
						   <th>ACCESS TYPE</th>
                                                   <th>DOWNLOAD </th>
                                                   <th>UPLOAD</th>
                                                   <th>DATA</th>
                                                   <th>PRICE</th>
                                                   <th>USERS</th>
                                                  <th>STATUS</th>
                                                   <th colspan="2" class="mui--text-right">ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody id='search_gridview'>
                                                
                                                 <?php 
                                                  $i=1;
                                                  $is_editable=$this->plan_model->is_permission(PLANS,'EDIT');
                                                    $is_deletable=$this->plan_model->is_permission(PLANS,'DELETE');
                                                 foreach($plan_listing as $valdata){
                                                    
                                                     $status = ($valdata->enableplan==1)?"Active":"Deactive";                                                            
                                                             
                                                     $plandata="";
                                                     if($valdata->plantype==1)
                                                     {
                                                        $plandata="Unlimited"; 
                                                         $plantype="Unlimited Plan";
                                                     }
                                                    else if($valdata->plantype==2)
                                                     {
                                                         $plandata="Timeplan";
                                                         $plantype="Time Plan";
                                                     }
                                                      else if($valdata->plantype==3)
                                                     {
                                                         $plandata= round($this->plan_model->convertTodata($valdata->datalimit."GB"))." GB";
                                                         $plantype="Fup Plan";
                                                        
                                                     }
                                                       else
                                                     {
                                                         $plandata= round($this->plan_model->convertTodata($valdata->datalimit."GB"))." GB";
                                                           $plantype="Data Plan";
                                                     }
						     $net_total=$valdata->gross_amt+($tax['tax']/100*$valdata->gross_amt);
                                                     ?>
						     
                                               <tr>
												   <td><?php echo $i;?>.</td>
                                                                                                   <td><a href="<?php echo base_url()."plan/edit_plan/".$valdata->srvid;?>"><?php echo $valdata->srvname;?></a></td>
												   <td><?php echo $plantype;?></td>
												    <td><?php echo ($valdata->is_private==1)?"Private":"Public"; ?></td>
                                                                                                   <td><?php echo $this->plan_model->convertTodata($valdata->downrate."KB");?></td>
												   <td><?php echo $this->plan_model->convertTodata($valdata->uprate."KB");?></td>
												   <td><?php echo $plandata;?></td>
												   <td>₹ <?php echo round($net_total);?> </td>
												     <td><a href="<?php echo base_url()."plan/user_stat/".$valdata->srvid;?>"><?php echo $valdata->usercount;?> </a></td>
												 
												    <?php 
									 $status = ''; $class = '';
							if($valdata->enableplan==1){
							$class = 'class="inactive delete"';
							$status = '<img src="'.base_url().'assets/images/on2.png" rel="disable">';
                                                        $stat1="Active";
                                                       // $task="disable";
							}else{
							$class = 'class=" delete"';
							$status = '<img src="'.base_url().'assets/images/off2.png" rel="enable">';
                                                        $stat1="Inctive";
                                                       // $task="enable";
							}
							
								
                                                                                                     
                                                                                                      if($is_editable){
								echo '<td><a ' . $class . '  onclick = "change_planstatus(\''.$valdata->srvid.'\')" href="javascript:void(0)" id="' . $valdata->srvid . '"> ' . $status . ' </a></td>';
                                                         }
                                                         else
                                                         {
                                                             echo '<td> ' . $stat1 . '</td>';
                                                         }
								
									 ?>
                                                                                       	   <td> <a href="<?php echo base_url()."plan/edit_plan/".$valdata->srvid;?>">Edit</a></td> 
                                                                                        	  <td><?php if($is_deletable){ ?><a href="javascript:void(0);" class="<?php echo($is_deletable)?'deletplan':'' ?>"  rel="<?php echo $valdata->srvid;?>">Delete</a><?php } ?></td> 
												  
                                                </tr>
                                                 <?php
                                                 
                                                 $i++;
                                                 } ?>
                                               
                                             </tbody>
                                          </table>
                                       </div>
									  </div>
                                    </div>
							    </div>
							
							   </div>
                           </div>
                            
                             <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="row" style="text-align:center">
                                    <input type='hidden' id="limit" value="<?php echo LIMIT;?>" />
                                    <input type='hidden' id="offset" value="<?php echo OFFSET;?>" />
                                    <input type='hidden' id="soffset" value="<?php echo SOFFSET;?>" />
                                    <div class="loadmore">
                                      
                                       <span style="padding:5px; border:1px solid; cursor:pointer" onclick="loadmore_plantopup('plan','viewplan_searchfilter','','<?php echo $filtertype; ?>')">Load More</span>
                                       
                                    </div>
                                    <div class="loadmore_loader hide">
                                       <img src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
                                    </div>
                                 </div>
                              </div>
                        </div>
					   </div> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
 
   <div class="modal fade" id="Deleteplan_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
          <input type="hidden" id="delplanid" value="">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                     <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                  </h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p id="erromsg">Are you sure you want to Delete Plan ?</p>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyes" >YES</button>
               </div>
            </div>
         </div>
      </div>
 <script type="text/javascript">
 $(document).ready(function(){
     
      $('body').on('click','#searchclear', function(){
      $('#searchtext').val('');
       $('#searchtext').addClass('serch_loading');
     search_filter('','','1');
      
   });
   
    $('body').on('keyup', '#searchtext', function(){
       $('#searchtext').addClass('serch_loading');
     search_filter('','','1');
   });
   
    $(document).on('click','.deletplan',function(){
           $('#erromsg').html("Are you sure you want to Delete Plan ?" );
                $('.delyes').removeClass('hide');
             var delplanid=$(this).attr('rel');
             $('#delplanid').val(delplanid);
                $.ajax({
        url: base_url+'plan/check_plan_deletable',
        type: 'POST',
        dataType: 'json',
        data: {delplanid:delplanid},
        success: function(data){
            if(data==1)
            {
               $('#Deleteplan_confirmation_alert').modal('show'); 
            }
            else
            {
                  $('#Deleteplan_confirmation_alert').modal('show'); 
                  $('#erromsg').html("Plan Can't be deleted already assigned to user" );
                $('.delyes').addClass('hide');
            }
         
           // $('#Deletenas_confirmation_alert').modal('hide');
      
      //  location.reload();
           
        
        }
    });
             
             
             
         });
   
      $(document).on('click','.delyes',function(){
           
           var planid=$('#delplanid').val();
             $.ajax({
        url: base_url+'plan/delete_plan',
        type: 'POST',
        dataType: 'json',
        data: {planid:planid},
        success: function(data){
         
            $('#Deleteplan_confirmation_alert').modal('hide');
      
        location.reload();
           
        
        }
    });
             
         });
     
 });
 
 
  
 function getcitylist(stateid) {
    $.ajax({
        url: base_url+'plan/getcitylist',
        type: 'POST',
        dataType: 'text',
        data: 'stateid='+stateid+'&is_addable=0',
        success: function(data){
           
            $('.search_citylist').empty();
            $('.search_citylist').append(data);
        }
    });
}

 function getzonelist(cityid) {
      var stateid=$('.statelist').val();
    $.ajax({
        url: base_url+'plan/getzonelist',
        type: 'POST',
        dataType: 'text',
        data: 'cityid='+cityid+'&stateid='+stateid+'&is_addable=0',
        success: function(data){
           
            $('.search_zonelist').empty();
            $('.search_zonelist').append(data);
        }
    });
}

function search_filter(data='', filterby='',keyup=''){
    $('.loadmore').removeClass('hide');
      var searchtext = $('#searchtext').val();
       if(keyup=="")
      {
           $('.loading').removeClass('hide');
      }
      $('#search_panel').addClass('hide');
      $('#allsearch_results').removeClass('hide');
       
 var limit = $('#limit').val();
    var offset = $('#soffset').val();
     // $('.loading').css('display', 'block');
      var plantype=$('#plantype').val();
      var formdata = '';
    //  var city = $('select[name="filter_city"]').val();
     // var zone = $('select[name="filter_zone"]').val();
      if (filterby == 'state') {
          $('.search_citylist').find('option:selected').prop("selected", false);
           $('.search_zonelist').find('option:selected').prop("selected", false);
         getcitylist(data);
           var city = "all";
      var zone = "all";
         formdata += '&state='+data+ '&city='+city+'&zone='+zone+'&filtertype='+plantype;
      }
      else if(filterby == 'city')
      {
          $('.search_zonelist').find('option:selected').prop("selected", false);
          getzonelist(data);
          var state = $('select[name="filter_state"]').val();
          var zone = "all";
         formdata += '&state='+state+ '&city='+data+'&zone='+zone+'&filtertype='+plantype;
      }
        else{
         var state = $('select[name="filter_state"]').val();
         var city = $('select[name="filter_city"]').val();
          var zone = $('select[name="filter_zone"]').val();
         formdata += '&state='+state+ '&city='+city+'&zone='+zone+'&filtertype='+plantype;
      }

      //alert(formdata);
      $.ajax({
         url: base_url+'plan/viewplan_searchfilter',
         type: 'POST',
         dataType: 'json',
         data: 'search_user='+searchtext+'&limit='+limit+'&offset='+offset+formdata,
         success: function(data){
              var nxtlimit = data.limit;
            var nxtofset = data.offset;
             $('#limit').val(nxtlimit); $('#offset').val(nxtofset);
            $('.loading').addClass('hide');
            $('#search_count').html(data.total_results+' <small>Results</small>');
            $('#search_gridview').html(data.search_results);
               $('#searchtext').removeClass('serch_loading');
         }
      });
   }
 </script>
 <script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
        

      </script>
 
 <?php $this->load->view('includes/footer');?>