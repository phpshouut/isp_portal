<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomFunc_model extends CI_Model{
    
    public function __construct(){
        
    }

    public function current_taxapplicable($isp_uid){
        $query = $this->db->query("SELECT tax FROM sht_tax WHERE isp_uid='".$isp_uid."'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return $rowdata->tax;
        }
    }
    
    public function userbalance_amount($uuid){
        $wallet_amt = 0;
        $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
        if($walletQ->num_rows() > 0){
            $wallet_amt = $walletQ->row()->wallet_amt;
        }
        
        $passbook_amt = 0;
        $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
        if($passbookQ->num_rows() > 0){
            $passbook_amt = $passbookQ->row()->plan_cost;
        }
        $balanceamt = $wallet_amt - $passbook_amt;
        return $balanceamt;
    }

    
    public function userassoc_plandata($isp_uid, $baseplanid){
        $data = array();
        $user_currplanQ = $this->db->query("SELECT tb2.srvid, tb2.datacalc, tb2.datalimit, tb2.plantype, tb2.plan_duration, tb3.net_total, tb3.gross_amt FROM sht_services as tb2 INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb2.srvid='".$baseplanid."'");
        
        $current_planrow = $user_currplanQ->row();
        $tax = $this->current_taxapplicable($isp_uid);
        
        $plan_duration = $current_planrow->plan_duration;
        $srvid = $current_planrow->srvid;
        $gross_amt = $current_planrow->gross_amt;
        if($gross_amt == '1.00'){
            $gross_amt = '30';
            $update_planprice = round($gross_amt + (($gross_amt * $tax)/100));
            $this->db->update('sht_plan_pricing', array('gross_amt' => $gross_amt, 'net_total' => $update_planprice), array('srvid' => $srvid));
        }
        
        $gross_amt = ($gross_amt * $plan_duration);
        $plan_price = round($gross_amt + (($gross_amt * $tax)/100));
        $plan_tilldays = ($plan_duration * 30);

        $data['plan_duration'] = $plan_duration;
        $data['datacalc'] = $current_planrow->datacalc;
        $data['datalimit'] = $current_planrow->datalimit;
        $data['planid'] = $srvid;
        $data['plan_price'] = $plan_price;
        $data['plantype'] = $current_planrow->plantype;
        $data['plan_tilldays'] = $plan_tilldays;
        $data['plan_cost_perday'] = round($plan_price/$plan_tilldays);
        
        return $data;
    }
    
    
    public function plan_billing_cycle($isp_uid){
        $planbillingQ = $this->db->get_where('sht_billing_cycle', array('isp_uid' => $isp_uid));
        $numrows = $planbillingQ->num_rows();
        return $planbillingQ->row();
    }
    public function activate_users_manually($isp_uid){
        $userQ = $this->db->query("SELECT tb1.id, tb1.uid, tb1.baseplanid, tb1.user_plan_type FROM sht_users as tb1 LEFT JOIN sht_subscriber_activation_panel as tb2 ON(tb1.id=tb2.subscriber_id) WHERE tb1.account_activated_on = '0000-00-00 00:00:00' AND tb1.enableuser='0' AND tb2.kyc_details='0' AND tb1.isp_uid='".$isp_uid."' AND tb1.baseplanid != '0'");
        if($userQ->num_rows() > 0){
            foreach($userQ->result() as $userobj){
                $subscid = $userobj->id;
                $subs_uuid = $userobj->uid;
                $srvid = $userobj->baseplanid;
                $user_plan_type = $userobj->user_plan_type;
                $cust_pwd = random_string('alnum', 7);
                $password = md5($cust_pwd);
                $radchk_pwd = '123456';
                
                $this->db->update("sht_subscriber_activation_panel", array('kyc_details' => '1'), array('subscriber_id' => $subscid));
                $radchkQ = $this->db->get_where('radcheck', array('username' => $subs_uuid));
                if($radchkQ->num_rows() == 0){
                    $this->db->query("INSERT INTO radcheck SET username='".$subs_uuid."', attribute = 'Cleartext-Password', op = ':=', value = '".$radchk_pwd."'");
                    $this->db->query("INSERT INTO radcheck SET username='".$subs_uuid."', attribute = 'Simultaneous-Use', op = ':=', value = '1'");
                }
                
                /* --------- TO DEDUCT MONEY FROM ISP WALLET -----------*/
                    $checkUserQ = $this->db->query("SELECT uid FROM sht_isp_users_track WHERE uid='".$subs_uuid."' AND  YEAR(`added_on`) = YEAR(CURRENT_DATE()) AND MONTH(`added_on`) = MONTH(CURRENT_DATE())");
                    if($checkUserQ->num_rows() == 0){
                        $this->db->insert('sht_isp_users_track', array('isp_uid' => $isp_uid, 'uid' => $subs_uuid, 'added_on' => date('Y-m-d H:i:s')));
                        $this->db->insert('sht_isp_passbook', array('isp_uid' => $isp_uid, 'total_active_users' => '1', 'cost' => '10', 'added_on' => date('Y-m-d H:i:s')));
                    }
                /* -------- END -----------*/
                
                $userassoc_plandata = $this->userassoc_plandata($isp_uid, $srvid);
                $datacalc = $userassoc_plandata['datacalc'];
                $plantype = $userassoc_plandata['plantype'];
                $plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
                $plan_price = $userassoc_plandata['plan_price'];
                $baseplanid = $userassoc_plandata['planid'];
                $plan_duration = $userassoc_plandata['plan_duration'];
                $plan_tilldays = $userassoc_plandata['plan_tilldays'];
                $user_credit_limit = round($plan_price + ($plan_price / 2));
                $user_wallet_balance = $this->userbalance_amount($subs_uuid);
                $total_advpay = 0;
                
                $this->db->update('sht_users', array('user_credit_limit' => $user_credit_limit), array('isp_uid' => $isp_uid, 'uid' => $subs_uuid));
                $user_updatedatarr = array('enableuser' => '1', 'orig_pwd' => $cust_pwd, 'password' => $password, 'account_activated_on' => date('Y-m-d H:i:s'), 'plan_activated_date' => date('Y-m-d'), 'plan_changed_datetime' => date('Y-m-d H:i:s'));
                
                /*if($user_plan_type == 'prepaid'){
                    $prebillingarr = array(
                        'subscriber_uuid' => $subs_uuid,
                        'plan_id' => $baseplanid,
                        'user_plan_type' => 'prepaid',
                        'bill_added_on' => date('Y-m-d H:i:s'),
                        'bill_starts_from' => date('Y-m-d'),
                        'bill_number' => date('jnyHis'),
                        'bill_type' => 'montly_pay',
                        'payment_mode' => '',
                        'receipt_received' => '0',
                        'actual_amount' => $plan_price,
                        'discount' => '0',
                        'total_amount' => $plan_price,
                        'isp_uid' => $isp_uid
                    );
                    
                    if($user_wallet_balance < $plan_price){
                        $prebillingarr['adjusted_amount'] = $user_wallet_balance;
                    }elseif($user_wallet_balance >= $plan_price){
                        $prebillingarr['payment_mode'] = 'wallet';
                        $prebillingarr['adjusted_amount'] = '0.00';
                        $prebillingarr['receipt_received'] = '1';
                    }
                    
                    $this->db->insert('sht_subscriber_billing', $prebillingarr);
                    $prebillid = $this->db->insert_id();
                    $prePassbookArr = array(
                        'isp_uid' => $isp_uid,
                        'subscriber_uuid' => $subs_uuid,
                        'billing_id' => $prebillid,
                        'srvid' => $baseplanid,
                        'plan_cost' => $plan_price,
                        'added_on'	=> date('Y-m-d H:i:s')
                    );
                    $this->db->insert('sht_subscriber_passbook', $prePassbookArr);
                    $expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
                
                }else{
                    $expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
                }*/
                
                $expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
                $pbcycle_rowdata = $this->plan_billing_cycle($isp_uid);
                $billing_cycle_date = $pbcycle_rowdata->billing_cycle;
                $today = date('d');
                if($today > $billing_cycle_date){
                    $nextdate = date('Y-m-d', strtotime('+1 month'));
                    $nextdate = new DateTime($nextdate);
                    $nextdate->setDate($nextdate->format('Y'), $nextdate->format('m'), $pbcycle_rowdata->billing_cycle);
                    $next_month = $nextdate->format('Y-m-d');
                    $user_updatedatarr['next_bill_date'] = $next_month;
                }else{
                    $user_updatedatarr['next_bill_date'] = date('Y-m').'-'.$pbcycle_rowdata->billing_cycle;
                }
                
                if($user_plan_type == 'prepaid'){
                    $pnext_bill_date = date('Y-m-d',strtotime("+$plan_tilldays days"));
                    $user_updatedatarr['next_bill_date'] = $pnext_bill_date;
                }
                
                $paidTilldate = $user_updatedatarr['plan_activated_date'];
                
                $expiration_date = date('Y-m-d', strtotime($paidTilldate . " +".$expiration_acctdays." days"));
                
                $user_updatedatarr['expiration'] = $expiration_date.' 00:00:00';
                $user_updatedatarr['paidtill_date'] = $paidTilldate;
                if($datacalc == 1){
                    $user_updatedatarr['downlimit'] = $userassoc_plandata['datalimit'];
                }elseif($datacalc == 2){
                    $user_updatedatarr['comblimit'] = $userassoc_plandata['datalimit'];
                }
                
                
                $this->db->update('sht_users', $user_updatedatarr , array('uid' => $subs_uuid));
                
                echo $isp_uid.'=>'.$subs_uuid. '<br/>'; 
            }
        }
    }
    
    
    public function update_stpl_uid(){
        $isp_uid = '129';
        $userQuery = $this->db->query("SELECT id,username, uid FROM sht_users WHERE isp_uid='".$isp_uid."' AND username != ''");
        if($userQuery->num_rows() > 0){
            $i = 1;
            foreach($userQuery->result() as $userobj){
                $id = $userobj->id;
                $username = $userobj->username;
                $uid = $userobj->uid;
                
                $chkduplicacyQuery = $this->db->query("SELECT username, uid FROM sht_users WHERE username = '".$username."' AND id != '".$id."'");
                if($chkduplicacyQuery->num_rows() == 0){
                    
                    //update billing
                    $billingQ = $this->db->query("SELECT id,subscriber_uuid FROM sht_subscriber_billing WHERE subscriber_uuid='".$uid."'");
                    if($billingQ->num_rows() > 0){
                    $this->db->update("sht_subscriber_billing", array('subscriber_uuid' => $username), array('subscriber_uuid' => $uid));
                    }
                    
                    //passbook
                    $passbookQ = $this->db->query("SELECT id,subscriber_uuid FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uid."'");
                    if($passbookQ->num_rows() > 0){
                    $this->db->update("sht_subscriber_passbook", array('subscriber_uuid' => $username, 'isp_uid' => $isp_uid), array('subscriber_uuid' => $uid));
                    }
                    
                    //isp user track record
                    $trackQ = $this->db->query("SELECT id FROM sht_isp_users_track WHERE uid='".$uid."'");
                    if($trackQ->num_rows() > 0){
                    $this->db->update("sht_isp_users_track", array('uid' => $username), array('uid' => $uid));
                    }
                    
                    //radcheck
                    $radcheckQ = $this->db->query("SELECT username FROM radcheck WHERE username='".$uid."'");
                    if($radcheckQ->num_rows() > 0){
                    $this->db->update("radcheck", array('username' => $username), array('username' => $uid));
                    }
                    
                    //update sht_users
                    $this->db->update("sht_users", array('uid' => $username), array('uid' => $uid));
                }
                echo $i.'. username: '.$username. '<br/>';
                $i++;
            }
        }
        
    }
    public function disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret){
        $data = array("apikey" => "n|A~ok4Y3D>&{U&S(A@G", 'username' => $username, 'userframedipaddress' => $userframedipaddress, 'usernasipaddress' => $usernasipaddress, 'secret' => $secret);
        $data_string = array('requestData' => json_encode($data));
        $ch = curl_init('http://103.20.213.150/decibel/decibelapis/disconnect_user_session');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($ch);
        if ($curl_response === false) {
            $info = curl_getinfo($ch);
            curl_close($ch);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($ch);
        return $curl_response;
    }
    public function disconnect_userineternet($uuid){
        $nasipQ = $this->db->query("SELECT tb1.radacctid, tb1.username, tb1.nasipaddress, tb1.framedipaddress, tb2.secret FROM radacct as tb1 INNER JOIN nas as tb2 ON(tb1.nasipaddress=tb2.nasname) WHERE tb1.username='".$uuid."' ORDER BY radacctid DESC LIMIT 1");
        if($nasipQ->num_rows() > 0){
            $nasrowdata = $nasipQ->row();
            $radacctid = $nasrowdata->radacctid;
            $username = $nasrowdata->username;
            $userframedipaddress = $nasrowdata->framedipaddress;
            $usernasipaddress = $nasrowdata->nasipaddress;
            $secret = $nasrowdata->secret;
            //@exec("echo User-Name:=$username,Framed-Ip-Address=$userframedipaddress | radclient -x $usernasipaddress:3799 disconnect $secret");
            $this->disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret);
            $this->db->update('radacct', array('acctstoptime' => date('Y-m-d H:i:s')), array('username' => $uuid, 'radacctid' => $radacctid));
        }
        return 1;
    }
    public function upload_billing_invoice($header,$arr_data, $isp_uid){
        //DELETE ALL previous bills invoices
        //$this->db->delete('sht_subscriber_billing', array('isp_uid' => $isp_uid));
        //$this->db->delete('sht_subscriber_passbook', array('isp_uid' => $isp_uid));
        //$this->db->delete('sht_subscriber_wallet', array('isp_uid' => $isp_uid));
        
        
        $headerArr = $header[1];
        $i=2;
        foreach($arr_data as $valueArr){
            $username = ''; $baseplanid = '0'; $bill_number = ''; $bill_added_on = ''; $expiration_date = '';
            $payment_mode = ''; $receipt_received = '0'; $adjusted_amount = 0;
            $plan_price = 0; $bill_amount = 0;
            $service_tax = 0; $swachh_bharat_tax = 0; $krishi_kalyan_tax = 0; $cgst_tax = 0; $sgst_tax = 0; 
            
            foreach($valueArr as $vcolname => $vcolval){
                if(array_key_exists($vcolname, $headerArr)){
                    if($headerArr["$vcolname"] == 'username'){
                        $username = trim($vcolval);
                    }
                    if($headerArr["$vcolname"] == 'plan_name'){
			$planname = trim($vcolval);
                        if($planname != 'installation'){
                            $planQ = $this->db->query("SELECT srvid FROM sht_services WHERE srvname='".$planname."' AND enableplan='1' AND isp_uid='".$isp_uid."' AND is_deleted='0'");
                            if($planQ->num_rows() > 0){
                                $baseplanid = $planQ->row()->srvid;
                            }
                        }else{
                            $baseplanid = 'installation';
                        }
		    }
                    if($headerArr["$vcolname"] == 'bill_number'){
                        $bill_number = trim($vcolval);
                    }
                    if($headerArr["$vcolname"] == 'bill_datetime'){
                        $bill_added_on = date('Y-m-d H:i:s', strtotime($vcolval));
                    }
                    if($headerArr["$vcolname"] == 'plan_amount'){
                        $plan_price = trim($vcolval);
                    }
                    if($headerArr["$vcolname"] == 'service_tax'){
                        $service_tax = trim($vcolval);
                    }
                    if($headerArr["$vcolname"] == 'Swachh_Bharat_Cess'){
                        $swachh_bharat_tax = trim($vcolval);
                    }
                    if($headerArr["$vcolname"] == 'Krishi_Kalyan_Cess'){
                        $krishi_kalyan_tax = trim($vcolval);
                    }
                    if($headerArr["$vcolname"] == 'SGST'){
                        $sgst_tax = trim($vcolval);
                    }
                    if($headerArr["$vcolname"] == 'CGST'){
                        $cgst_tax = trim($vcolval);
                    }
                    if($headerArr["$vcolname"] == 'bill_amount'){
                        $bill_amount = trim($vcolval);
                    }
                }
            }
            
            $billdate = date('Y-m-d', strtotime($bill_added_on));
            $bill_starts_from = date('Y-m-d', strtotime($billdate. " -30 days"));
            
            $chkuserQ = $this->db->query("SELECT uid FROM sht_users WHERE (uid='".$username."' OR username='".$username."') AND enableuser = '1'");
	    if($chkuserQ->num_rows() != 0){
                
                if($baseplanid == 'installation'){
                    $installarr = array(
                        'isp_uid' => $isp_uid,
                        'subscriber_uuid' => $username,
                        'connection_type' => '',
                        'bill_added_on' => $bill_added_on,
                        'bill_number' => $bill_number,
                        'receipt_number' => '',
                        'bill_type' => 'installation',
                        'payment_mode' => $payment_mode,
                        'actual_amount' => $plan_price,
                        'discount' => '0',
                        'total_amount' => $bill_amount,
                        'receipt_received' => $receipt_received,
                        'alert_user' => '1'
		    );
                    //$this->db->insert('sht_subscriber_billing', $installarr);
                }else{
                    echo 'Attached Bill: '.$username. '=>'. $bill_number. '=>'. $bill_added_on. '<br/>';
                    $this->disconnect_userineternet($username);
                    
                    $prebillingarr = array(
                        'subscriber_uuid' => $username,
                        'plan_id' => $baseplanid,
                        'user_plan_type' => 'prepaid',
                        'bill_starts_from' => $bill_starts_from,
                        'bill_added_on' => $bill_added_on,
                        'bill_number' => $bill_number,
                        'bill_type' => 'montly_pay',
                        'payment_mode' => $payment_mode,
                        'receipt_received' => $receipt_received,
                        'actual_amount' => $plan_price,
                        'discount' => '0',
                        'total_amount' => $bill_amount,
                        'adjusted_amount' => $adjusted_amount,
                        'isp_uid' => $isp_uid,
                        'service_tax' => $service_tax,
                        'swachh_bharat_tax' => $swachh_bharat_tax,
                        'krishi_kalyan_tax' => $krishi_kalyan_tax,
                        'cgst_tax' => $cgst_tax,
                        'sgst_tax' => $sgst_tax
                    );
                    //$this->db->insert('sht_subscriber_billing', $prebillingarr);
                    $prebillid = $this->db->insert_id();
                    $prePassbookArr = array(
                        'isp_uid' => $isp_uid,
                        'subscriber_uuid' => $username,
                        'billing_id' => $prebillid,
                        'srvid' => $baseplanid,
                        'plan_cost' => $bill_amount,
                        'added_on'	=> $bill_added_on
                    );
                    //$this->db->insert('sht_subscriber_passbook', $prePassbookArr);
                    
                    $userassoc_plandata = $this->userassoc_plandata($isp_uid, $baseplanid);
                    $datacalc = $userassoc_plandata['datacalc'];
                    $plantype = $userassoc_plandata['plantype'];
                    $plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
                    $plan_price = $userassoc_plandata['plan_price'];
                    $baseplanid = $userassoc_plandata['planid'];
                    $plan_duration = $userassoc_plandata['plan_duration'];
                    $plan_tilldays = $userassoc_plandata['plan_tilldays'];
                    $user_credit_limit = round($plan_price + ($plan_price / 2));
                    $user_wallet_balance = $this->userbalance_amount($username);
                    $total_advpay = 0;
                    
                    $user_updatedatarr = array('user_plan_type' => 'prepaid', 'baseplanid' => $baseplanid);
                    $expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
                    if($datacalc == 1){
                        $user_updatedatarr['downlimit'] = $userassoc_plandata['datalimit'];
                    }elseif($datacalc == 2){
                        $user_updatedatarr['comblimit'] = $userassoc_plandata['datalimit'];
                    }
                    
                    $paidTilldate = date('Y-m-d', strtotime($bill_added_on));
                    $next_bill_date = date('Y-m-d', strtotime($paidTilldate. " +30 days"));
                    $expiration_date = date('Y-m-d', strtotime($paidTilldate . " +".$expiration_acctdays." days"));
                    echo $paidTilldate. '=>'. $expiration_acctdays. '=>'. $expiration_date. '<br/><br/>';
                    
                    $user_updatedatarr['next_bill_date'] = $next_bill_date;
                    $user_updatedatarr['expiration'] = $expiration_date.' 00:00:00';
                    $user_updatedatarr['paidtill_date'] = $paidTilldate;
                    $user_updatedatarr['user_plan_type'] = 'prepaid';
                    $user_updatedatarr['plan_activated_date'] = $bill_added_on;
                    
                    //$this->db->update('sht_users', $user_updatedatarr, array('uid' => $username));
                }
            }else{
                echo 'Pending Bill: '.$username. '=>'. $bill_number. '=>'. $bill_added_on. '<br/>';
            }
        }
    }
    
    public function delete_activation_records(){
        $query = $this->db->query("SELECT id, subscriber_id FROM sht_subscriber_activation_panel GROUP BY subscriber_id HAVING COUNT(*) > 1");
        if($query->num_rows() > 0){
            foreach($query->result() as $obj){
                $recid = $obj->id;
                $subsid = $obj->subscriber_id;
                $this->db->query("DELETE FROM sht_subscriber_activation_panel WHERE subscriber_id='".$subsid."' AND id != '".$recid."'");
            }
        }
    }
    
    public function update_billingaddress(){
        $billaddrQ = $this->db->query("SELECT id, firstname, lastname, email, mobile, flat_number, address, address2, state, city, zone FROM sht_users WHERE billaddr='1'");
        if($billaddrQ->num_rows() > 0){
            foreach($billaddrQ->result() as $rowdata){
                $id = $rowdata->id;
                $username = $rowdata->firstname .' '. $rowdata->lastname;
                $email = $rowdata->email;
                $mobile = $rowdata->mobile;
                $flat_number = $rowdata->flat_number;
                $address = $rowdata->address;
                $address2 = $rowdata->address2;
                $state = $rowdata->state;
                $city = $rowdata->city;
                $zone = $rowdata->zone;
                
                $this->db->update("sht_users", array('billing_username' => $username, 'billing_email' => $email, 'billing_mobileno' => $mobile, 'billing_flat_number' => $flat_number, 'billing_address' => $address, 'billing_address2' => $address2, 'billing_state' => $state, 'billing_city' => $city, 'billing_zone' => $zone), array('id' => $id));
            }
        }
    }
    
/***************************************************************************************************************************/
    public function user_advpayments($uid){
        $query = $this->db->query("SELECT COALESCE(SUM(balance_left),0) as balance_left FROM sht_advance_payments WHERE uid='".$uid."'");
        $advobj = $query->row();
        $total_advpay = $advobj->balance_left;
        return $total_advpay;
    }

    public function plandata_records($subscriber_uuid, $isp_uid){
        $data = array();
        $user_currplanQ = $this->db->query("SELECT tb2.datacalc, tb2.datalimit, tb2.plantype, tb2.plan_duration, tb1.baseplanid, tb1.user_plan_type, tb1.user_credit_limit, DATE(tb1.account_activated_on) as account_activated_on, tb1.paidtill_date, DATE(tb1.expiration) as expiration_date, tb3.net_total, tb3.gross_amt FROM sht_users as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid=tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb1.uid='".$subscriber_uuid."'");
        $current_planrow = $user_currplanQ->row();
        $tax = $this->current_taxapplicable($isp_uid);
        $plan_duration = $current_planrow->plan_duration;
        $srvid = $current_planrow->baseplanid;
        
        $gross_amt = ($current_planrow->gross_amt * $plan_duration);
        $plan_price = round($gross_amt + (($gross_amt * $tax)/100));
        
        $data['plan_duration'] = $plan_duration;
        $data['datacalc'] = $current_planrow->datacalc;
        $data['datalimit'] = $current_planrow->datalimit;
        $data['planid'] = $current_planrow->baseplanid;
        $data['plan_price'] = $plan_price;
        $data['plantype'] = $current_planrow->plantype;
        $data['user_credit_limit'] = $current_planrow->user_credit_limit;
        $data['account_activated_on'] = $current_planrow->account_activated_on;
        //$data['paidtill_date'] = $current_planrow->paidtill_date;
        
        $plan_tilldays = ($plan_duration * 30);
        $data['plan_tilldays'] = $plan_tilldays;
        $data['plan_cost_perday'] = round($plan_price/$plan_tilldays);
        $data['expiration_date'] = $current_planrow->expiration_date;
        $data['user_plan_type'] = $current_planrow->user_plan_type;
        $data['total_advpay'] = $this->user_advpayments($subscriber_uuid);
        return $data;
    }
    
    public function generate_billinvoices(){
        //$invoiceQ = $this->db->query("SELECT * FROM `sht_users` WHERE `next_bill_date` LIKE '%-10-%' AND inactivate_type != 'terminate' AND isp_uid='129'");
        $invoiceQ = $this->db->query("SELECT *  FROM `sht_users` WHERE `next_bill_date` BETWEEN '2017-11-01' AND '2017-11-15' AND inactivate_type != 'terminate' AND isp_uid='129'");
        if($invoiceQ->num_rows() > 0){
            foreach($invoiceQ->result() as $invoiceobj){
                $subs_uuid = $invoiceobj->uid;
                $isp_uid = $invoiceobj->isp_uid;
                $user_plan_type = $invoiceobj->user_plan_type;
                $baseplanid = $invoiceobj->baseplanid;
                $next_bill_date = $invoiceobj->next_bill_date;
                $bill_starts_from = $invoiceobj->plan_activated_date;
                
                echo 'uuid : '.$subs_uuid. '<br/>';
                
                $this->disconnect_userineternet($subs_uuid);
                
                $userassoc_plandata = $this->plandata_records($subs_uuid, $isp_uid);
                $datacalc = $userassoc_plandata['datacalc'];
                $plantype = $userassoc_plandata['plantype'];
                $user_credit_limit = $userassoc_plandata['user_credit_limit'];
                $plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
                $user_plan_type = $userassoc_plandata['user_plan_type'];
                $plan_price = $userassoc_plandata['plan_price'];
                $baseplanid = $userassoc_plandata['planid'];
                $total_advpay = $userassoc_plandata['total_advpay'];
                $plan_duration = $userassoc_plandata['plan_duration'];
                $plan_tilldays = $userassoc_plandata['plan_tilldays'];
                
                $prebillingarr = array(
                    'subscriber_uuid' => $subs_uuid,
                    'plan_id' => $baseplanid,
                    'user_plan_type' => $user_plan_type,
                    'bill_added_on' => "$next_bill_date 11:59:12",
                    'bill_starts_from' => $bill_starts_from,
                    'bill_number' => date('jnyHis'),
                    'bill_type' => 'montly_pay',
                    'payment_mode' => '',
                    'receipt_received' => '0',
                    'actual_amount' => $plan_price,
                    'discount' => '0',
                    'total_amount' => $plan_price,
                    'isp_uid' => $isp_uid
                );
                
                $this->db->insert('sht_subscriber_billing', $prebillingarr);
                $prebillid = $this->db->insert_id();
                $prePassbookArr = array(
                        'isp_uid' => $isp_uid,
                        'subscriber_uuid' => $subs_uuid,
                        'billing_id' => $prebillid,
                        'srvid' => $baseplanid,
                        'plan_cost' => $plan_price,
                        'added_on'	=> date('Y-m-d H:i:s')
                );
                $this->db->insert('sht_subscriber_passbook', $prePassbookArr);
                
                $user_wallet_balance = $this->userbalance_amount($subs_uuid);
                $expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
                $expiration_date = date('Y-m-d', strtotime($next_bill_date . " +".$expiration_acctdays." days"));
                
                $pnext_bill_date = date('Y-m-d',strtotime("$next_bill_date +$plan_tilldays days"));
		$user_updatedatarr['next_bill_date'] = $pnext_bill_date;
                $user_updatedatarr['plan_activated_date'] = $next_bill_date;
                $user_updatedatarr['expiration'] = $expiration_date.' 00:00:00';
                $user_updatedatarr['paidtill_date'] = $next_bill_date;
                if($datacalc == 1){
                    $user_updatedatarr['downlimit'] = $userassoc_plandata['datalimit'];
                }elseif($datacalc == 2){
                    $user_updatedatarr['comblimit'] = $userassoc_plandata['datalimit'];
                }
		$this->db->update('sht_users', $user_updatedatarr , array('uid' => $subs_uuid));
            }
        }
    }

    public function wallet_updation(){
        $shtusersQ = $this->db->query("SELECT uid, isp_uid FROM sht_users");
        if($shtusersQ->num_rows() > 0){
            foreach($shtusersQ->result() as $shtuserobj){
                $uuid = $shtuserobj->uid;
                $isp_uid = $shtuserobj->isp_uid;
                
                $wallet_amt = 0; $passbook_amt = 0;
                $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
                if($walletQ->num_rows() > 0){
                    $wallet_amt = $walletQ->row()->wallet_amt;
                }
                
                $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
                if($passbookQ->num_rows() > 0){
                    $passbook_amt = $passbookQ->row()->plan_cost;
                }
                $balanceamt = $wallet_amt - $passbook_amt;
                
                if($balanceamt < 0){
                    $walletArr = array(
                        'isp_uid' => $isp_uid,
                        'subscriber_uuid' => $uuid,
                        'wallet_amount' => $wallet_amt,
                        'left_amount' => '0',
                        'added_on'	=> date('Y-m-d H:i:s')
                    );
                    $this->db->insert('sht_subscriber_wallet', $walletArr);
                    $wid = $this->db->insert_id();
                    $this->db->query("DELETE FROM sht_subscriber_wallet WHERE subscriber_uuid = '".$uuid."' AND id != '".$wid."'");
                }else{
                    $rcptwalletQ = $this->db->query("SELECT COALESCE(SUM(receipt_amount),0) as receipt_amount FROM sht_subscriber_receipt_history WHERE uid='".$uuid."' AND payment_mode='viawallet'");
                    if($rcptwalletQ->num_rows() > 0){
			$rcptrowdata = $rcptwalletQ->row();
                        $receipt_amount = $rcptrowdata->receipt_amount;
                        $balanceamt = ($wallet_amt - $receipt_amount);
                    }
                    
                    $walletArr = array(
                        'isp_uid' => $isp_uid,
                        'subscriber_uuid' => $uuid,
                        'wallet_amount' => $wallet_amt,
                        'left_amount' => $balanceamt,
                        'added_on'	=> date('Y-m-d H:i:s')
                    );
                    $this->db->insert('sht_subscriber_wallet', $walletArr);
                    $wid = $this->db->insert_id();
                    $this->db->query("DELETE FROM sht_subscriber_wallet WHERE subscriber_uuid = '".$uuid."' AND id != '".$wid."'");
                }
            }
        }
    }
    
    public function receipt_updation(){
        $billrcptQ = $this->db->query("SELECT tb1.id, tb1.bill_id FROM sht_subscriber_receipt_history as tb1 INNER JOIN sht_subscriber_billing as tb2 ON(tb1.bill_id=tb2.id) WHERE tb2.bill_type='addtowallet'");
        if($billrcptQ->num_rows() > 0){
            foreach($billrcptQ->result() as $brcptobj){
                $rcptid = $brcptobj->id;
                $this->db->delete("sht_subscriber_receipt_history", array('id' => $rcptid));
            }
        }
        
    }
    
    public function addreceipt_forbilling(){
        $billQ = $this->db->query("SELECT id,total_amount,adjusted_amount,receipt_received,bill_type,receipt_number,subscriber_uuid,isp_uid,payment_mode,bill_added_on FROM sht_subscriber_billing WHERE ((receipt_received='1' AND bill_type != '') OR (adjusted_amount != '0.00')) AND bill_type != 'installation' AND bill_type != 'security' AND bill_type != 'advprepay' AND bill_type != 'addtowallet' ");
        //echo $this->db->last_query(); die;
        if($billQ->num_rows() > 0){
            foreach($billQ->result() as $billobj){
                $billid = $billobj->id;
                $billreceipt_received = $billobj->receipt_received;
                $billadjusted_amount = $billobj->adjusted_amount;
                $rcptQ = $this->db->query("SELECT id FROM sht_subscriber_receipt_history WHERE bill_id='".$billid."'");
                $numrows = $rcptQ->num_rows();
                if($numrows == 0){
                    if(($billreceipt_received != '1') && ($billadjusted_amount != '0.00')){
                        $receipt_amount = $billadjusted_amount;
                    }else{
                        $receipt_amount = $billobj->total_amount;
                    }
                    $rcptarr = array('isp_uid' => $billobj->isp_uid, 'uid' => $billobj->subscriber_uuid, 'bill_id' => $billid, 'receipt_number' => $billobj->receipt_number, 'receipt_amount' => $receipt_amount, 'payment_mode' => $billobj->payment_mode,  'bill_type' => $billobj->bill_type, 'added_on' => $billobj->bill_added_on);
                    $this->db->insert("sht_subscriber_receipt_history", $rcptarr);
                }
                
            }
        }
    }
    
/****************************************************************************************************************************/    
    public function balance_billpassbook(){
        $passbookQ = $this->db->query("SELECT tb1.id, tb1.subscriber_uuid, tb1.plan_cost, tb2.total_amount FROM `sht_subscriber_passbook` as tb1 LEFT JOIN sht_subscriber_billing as tb2 ON(tb1.billing_id = tb2.id) WHERE (tb1.plan_cost != tb2.total_amount)");
        echo $this->db->last_query(); die;
        if($passbookQ->num_rows() > 0){
            foreach($passbookQ->result() as $passobj){
                $billamt = $passobj->total_amount;
                $passid = $passobj->id;
                
                $this->db->update("sht_subscriber_passbook", array('plan_cost' => $billamt), array('id' => $passid));
                
            }
        }
    }
    
    public function balance_billwallet(){
        $walletQ = $this->db->query("SELECT tb1.* FROM `sht_subscriber_wallet` as tb1 WHERE tb1.isp_uid != '129' ");
        if($walletQ->num_rows() > 0){
            $i = 1;
            foreach($walletQ->result() as $wallobj){
                $wlastid = $wallobj->id;
                $isp_uid = $wallobj->isp_uid;
                $uuid = $wallobj->subscriber_uuid;
                $wallet_amount = $wallobj->wallet_amount;
                $added_on = $wallobj->added_on;
                
                $billQ = $this->db->query("SELECT tb1.wallet_id FROM `sht_subscriber_billing` as tb1 WHERE tb1.isp_uid != '129' AND tb1. wallet_id = '".$wlastid."' AND tb1.bill_type='addtowallet'");
                if($billQ->num_rows() == 0){
                    echo $i.' '.$wlastid. '=>' .$uuid . '=>' .$isp_uid.'<br/>';
                    $i++;
                    /*$wallet_billarr = array(
                        'isp_uid' => $isp_uid,
                        'subscriber_uuid' => $uuid,
                        'wallet_id' => $wlastid,
                        'bill_added_on' => $added_on,
                        'receipt_number' => date('jnyHis'),
                        'bill_type' => 'addtowallet',
                        'payment_mode' => 'cash',
                        'actual_amount' => $wallet_amount,
                        'total_amount' => $wallet_amount,
                        'receipt_received' => '1',
                        'alert_user' => '0',
                        'bill_generate_by' => $isp_uid,
                        'bill_paid_on' => $added_on,
                        'wallet_amount_received' => '1'
                    );
                    $this->db->insert('sht_subscriber_billing', $wallet_billarr);
                    */
                }
            }
        }
    }
}
?>