<?php

class Nas_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function listing_nas() {
        //$condarr=array('status'=>1,"is_deleted"=>0);
        $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
          $this->db->order_by("id","desc");
          $condarr=array("isp_uid"=>$isp_uid);
          $query = $this->db->get_where(SHTNAS,$condarr);
         // echo $this->db->last_query(); die;
           return $query->result();
    }

    public function add_nas_data() {

        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $usergraph=(isset($postdata['usergrph']) && $postdata['usergrph'] != '' ) ? $postdata['usergrph'] : "0";
         $username=(isset($postdata['username']) && $postdata['username'] != '' ) ? $postdata['username'] : "";
          $userpwd=(isset($postdata['userpwd']) && $postdata['userpwd'] != '' ) ? $postdata['userpwd'] : "";
         $isp_uid = $sessiondata['isp_uid'];
        
        $nasfqdn = (isset($postdata['fqdn']) && $postdata['fqdn'] != '' ) ? $postdata['fqdn'] : "";
        // $nasip1=(isset($postdata['ip1']) && $postdata['ip1']!=''  )?$postdata['ip1'].".".$postdata['ip2'].".".$postdata['ip3'].".".$postdata['ip4']:"";
        $nasip = (isset($postdata['ip'])) ? $postdata['ip'] : '';
        
        // $tabledata=array('nasipformat'=>$postdata['nasipformat'],"nasfqdn"=>$nasfqdn,'nasipaddr'=>$nasip,'nasname'=>$postdata['nas_name'],'nastype'=>$postdata['nastype'],'nassecret'=>$postdata['nas_secret'], 'nasstate'=>$postdata['state'],'nascity'=>$postdata['city'],'naszone'=>$postdata['zone'],'created_on'=>date("Y-m-d H:i:s"));
        if (isset($postdata['nasid']) && $postdata['nasid'] != '') {
            $tabledata = array('nasname' => $nasip,"isp_uid"=>$isp_uid, "shortname" => $postdata['nas_name'], 'type' => 0, 'ports' => 0, 'secret' => $postdata['nas_secret'], 'nastype' => $postdata['nastype'], 'nasstate' => $postdata['state'], 'nascity' => $postdata['city'], 'naszone' => $postdata['zone'], 'created_on' => date("Y-m-d H:i:s"),
                               "geoaddress"=>$postdata['geoaddr'],"place_id"=>$postdata['placeid']
                                ,"lat"=>$postdata['lat'],"longitude"=>$postdata['long']);
            $this->db->update(SHTNAS, $tabledata, array('id' => $postdata['nasid']));
            $this->nas_plan_association($postdata, $postdata['nasid']);
            return $postdata['nasid'];
        } else {
             $tabledata = array('nasname' => $nasip,"isp_uid"=>$isp_uid, "shortname" => $postdata['nas_name'],
                                'type' => 0, 'ports' => 0, 'secret' => $postdata['nas_secret'],
                                'nastype' => $postdata['nastype'], 'nasstate' => $postdata['state'],
                                'nascity' => $postdata['city'], 'naszone' => $postdata['zone'],
                                'created_on' => date("Y-m-d H:i:s"),"graphuser"=>$usergraph,"apiusername"=>$username,
                                "apipassword"=>$userpwd,"geoaddress"=>$postdata['geoaddr'],"place_id"=>$postdata['placeid']
                                ,"lat"=>$postdata['lat'],"longitude"=>$postdata['long']);
            $this->db->insert(SHTNAS, $tabledata);
            $nasid = $this->db->insert_id();
            $this->nas_plan_association($postdata, $nasid);
            if($usergraph==1)
            {
                $this->graph_command($userpwd);
            }
            return $nasid;
        }
    }
    
    public function graph_command($password)
    {
    @exec("echo user add name=shouutAPI password=$password group=full");
    @exec("echo ip service set api disabled=no port=8728");
    }

    public function viewall_search_results() {
        $data = array();
        $gen = '';
        $total_search = 0;
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" isp_uid='".$isp_uid."'";
         $where ="$ispcond";
        $search_user = $this->input->post('search_user');
        $where.= "and ((nasname LIKE '%" . $search_user . "%') OR (shortname LIKE '%" . $search_user . "%') ) ";
        $user_state = $this->input->post('state');
        $user_city = $this->input->post('city');
        $user_zone = $this->input->post('zone');
        

        $condarr = array();
        if ($user_state != 'all' && $user_city != 'all' && $user_zone != 'all') {
            $condarr[] = "(nasstate='" . $user_state . "' and nascity='" . $user_city . "' and naszone='" . $user_zone . "')";
        } else if ($user_state != 'all' && $user_city == 'all') {
            $condarr[] = "(nasstate='" . $user_state . "')";
        } else if ($user_state != 'all' && $user_city != 'all' && $user_zone == 'all') {
            $condarr[] = "(nasstate='" . $user_state . "' and nascity='" . $user_city . "')";
        }
        $cond1 = implode('OR', $condarr);



        if ($cond1 != "") {
            $where .= "and ($cond1)";
        }

        if (isset($search_user) && $search_user != '') {
            $stateidarr = array();
            $cityidarr = array();
            $zoneidarr = array();
            $condarr = array();
            $stquery = $this->db->query("select id from sht_states where state like '%" . $search_user . "%'");
            if ($stquery->num_rows() > 0) {
                foreach ($stquery->result() as $val) {
                    $stateidarr[] = $val->id;
                }
                $sid = '"' . implode('", "', $stateidarr) . '"';
                $condarr[] = "(nasstate in ({$sid}))";
            }

            $ctquery = $this->db->query("select city_id from sht_cities where city_name like '%" . $search_user . "%'");
            if ($ctquery->num_rows() > 0) {
                foreach ($ctquery->result() as $val) {
                    $cityidarr[] = $val->city_id;
                }
                $cid = '"' . implode('", "', $cityidarr) . '"';
                $condarr[] = "(nascity in ({$cid}))";
            }

            $znquery = $this->db->query("select id from sht_zones where zone_name like '%" . $search_user . "%'");
            if ($znquery->num_rows() > 0) {
                foreach ($znquery->result() as $val) {
                    $zoneidarr[] = $val->id;
                }
                $zid = '"' . implode('", "', $zoneidarr) . '"';
                $condarr[] = "(naszone in ({$zid}))";
            }

            $cond = implode("OR", $condarr);
            if ($cond != '') {
                $cond = "OR ({$cond})";
            }
            $where .= $cond;
        }

        $this->db->select('nasstate,nascity,naszone,nastype,nasname,shortname,id');
        $this->db->from(SHTNAS);
        $this->db->where($where);
        $query = $this->db->get();
      //  echo $this->db->last_query(); die;
        $total_search = $query->num_rows();
        if ($total_search > 0) {
            $i = 1;
            foreach ($query->result() as $sobj) {
                $state = $this->user_model->getstatename($sobj->nasstate);
                $city = $this->user_model->getcityname($sobj->nasstate, $sobj->nascity);
                $zone = $this->user_model->getzonename($sobj->naszone);
                //$status = ($sobj->status == 1)?'active':'inactive';
                $nastype = ($sobj->nastype == 1) ? "Microtik" : "Others";
                $up = $this->ping($sobj->nasname);
                if ($up == 0) {

                    $nsimg = '<img src="' . base_url() . 'assets/images/green.png" > ';
                } else {
                    $nsimg = '<img src="' . base_url() . 'assets/images/red.png" >';
                }


                $gen .= '
				<tr>
					<td>' . $i . '.</td>
					<td>' . $sobj->shortname . '</td>
					<td>' . $nastype . '</td>
					<td>' . $sobj->nasname . '</td>
					<td>' . $state . '</td>
					<td>' . $city . '</td>
					<td>' . $zone . '</td>
                                            <td>' . $nsimg . '</td>
					
					<td><a href="javascript:void(0);" class="editnas" rel="' . $sobj->id . '">EDIT</a></td>
					
				</tr>
				';
                $i++;
            }
        }

        $data['total_results'] = $total_search;
        $data['search_results'] = $gen;

        echo json_encode($data);
    }

    // public function 

    public function delete_nas() {
        $postdata = $this->input->post();
        $tabledata = array("is_deleted" => 1);
        $this->db->update(SHTNAS, $tabledata, array('nasid' => $postdata['nasid']));
        return $postdata['nasid'];
    }
    
    
    

    public function edit_nas($is_ro) {
        $id = $this->input->post('nasid');
        $condarr = array('id' => $id);
        $disable = ($is_ro == 1) ? "disabled" : "";
        $query = $this->db->get_where(SHTNAS, $condarr);
        $rowarr = $query->row_array();
        $gen = "";
        // $checkst=($rowarr['nasipformat']=="Static")?"checked":"";
        //$chekdns=($rowarr['nasipformat']=="DDNS")?"checked":"";
        // $fqdn=$rowarr['nasfqdn'];
        // $fqdndis=($rowarr['nasipformat']=="Static")?"disabled":"";
        // $ipdis=($rowarr['nasipformat']=="DDNS")?"disabled":"";
        $gen .= ' 
            <input type="hidden" name="nasid" id="editnasid" value="' . $rowarr['id'] . '">
                <div class="row"><div id="ip1"></div></div>
               <div class="row">
                  
                    <input type="hidden" name="mobileno" value="9873834499">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <label>IP Address<sup>*</sup></label>
                           <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-right">';

        $iparr = explode(".", $rowarr['nasname']);

        $selother = ($rowarr['nastype'] == "0") ? "selected" : "";
        $selmicro = ($rowarr['nastype'] == "1") ? "selected" : "";
        $ip1 = $iparr[0];
        $ip2 = $iparr[1];
        $ip3 = $iparr[2];
        $ip4 = $iparr[3];
        $state = $this->state_list($rowarr['nasstate']);


        $gen .= '<div class="mui-textfield mui-textfield--float-label" style="padding-right:10px">
                                      
                                           <input type="text"  id="editip" name="ip" value="' . $rowarr['nasname'] . '" required>
                                      
                                    </div>
                                     <span class="errorip" style="color:red;">
                                 </div>
                              
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label">
                              <input type="text" name="nas_name" ' . $disable . ' id="editnasname" value="' . $rowarr['shortname'] . '" required>
                              <label>Nas Name</label>
                           </div>
                            <span class="errornas" style="color:red;"></span>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="nastype" ' . $disable . ' required>
                               
                                 <option value="1" ' . $selmicro . '>Microtik</option>
                              </select>
                              <label>NAS Type<sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label">
                              <input type="text" ' . $disable . ' name="nas_secret" id="editnas_secret" value="' . $rowarr['secret'] . '" required>
                              <label>Nas Secret</label>
                           </div>
                           <span class="errornassecret" style="color:red;"></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="">
                              <select name="state" ' . $disable . ' required class="statelist form-control">
                                 <option value="all">All States</option>
                               ' . $state . '
                            </select>
                              <input type="hidden" class="statesel" name="statesel" value="' . $rowarr['nasstate'] . '">
                              
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="">
                              <select name="city" ' . $disable . ' class="search_citylist form-control" required >
                                 ';
        $gen .= $this->getcitylist($rowarr['nasstate'], $rowarr['nascity']);
        $gen .= '    </select><input type="hidden" class="citysel" name="citysel" value="' . $rowarr['nascity'] . '">
                            
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="">
                              <select class="zone_list form-control" name="zone" ' . $disable . ' required>
                               ';
        $gen .= $this->zone_list($rowarr['nascity'], $rowarr['naszone'], $rowarr['nasstate']);
        $gen .= ' </select>
                              
                           </div>
                        </div>
                     </div>
                  ';
                  
        $data['html'] = $gen;
        $data['placeid']=$rowarr['place_id'];
        $data['lat']=$rowarr['lat'];
        $data['long']=$rowarr['longitude'];
        $data['address']=$rowarr['geoaddress'];
        $data['nasip'] = $rowarr['nasname'];
        echo json_encode($data);
        //  echo "<pre>"; print_R($rowarr); die;
    }

    public function state_list($stateid = '') {
        $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
        $isp_uid=$sessiondata['isp_uid'];
        $regiontype = $this->plan_model->dept_region_type();
        if ( $superadmin == 1) {
             $query = $this->db->query("select state from sht_isp_admin_region where isp_uid='" . $isp_uid . "'");
            $statearr = array();
            foreach ($query->result() as $val) {
                $statearr[] = $val->state;
            }
            if (in_array('all', $statearr)) {
                $stateQ = $this->db->get('sht_states');
            } else {
                $sid = '"' . implode('", "', $statearr) . '"';
                $stateQ = $this->db->query("select * from sht_states where id IN ({$sid}) ");
            }
            $num_rows = $stateQ->num_rows();
            if ($num_rows > 0) {
                $gen = '';
                foreach ($stateQ->result() as $stobj) {
                    $sel = '';
                    if ($stateid == $stobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $stobj->id . '" ' . $sel . '>' . $stobj->state . '</option>';
                }
            }
        } else {
            $query = $this->db->query("select state_id from sht_dept_region where dept_id='" . $dept_id . "'");
            $statearr = array();
            foreach ($query->result() as $val) {
                $statearr[] = $val->state_id;
            }
            if (in_array('all', $statearr)) {
                $stateQ = $this->db->get('sht_states');
            } else {
                $sid = '"' . implode('", "', $statearr) . '"';
                $stateQ = $this->db->query("select * from sht_states where id IN ({$sid}) ");
            }
            $num_rows = $stateQ->num_rows();
            if ($num_rows > 0) {
                $gen = '';
                foreach ($stateQ->result() as $stobj) {
                    $sel = '';
                    if ($stateid == $stobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $stobj->id . '" ' . $sel . '>' . $stobj->state . '</option>';
                }
            }
        }
        return $gen;
    }

    public function zone_list($cityid, $zoneid = '', $stateid = '') {

        $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
        $regiontype = $this->plan_model->dept_region_type();
        $isp_uid = $sessiondata['isp_uid'];
        // $stateid=$this->input->post('stateid');
        $allzone = '<option value="all">All Zone</option>';
        $gen = '';
        if ($superadmin == 1) {
            $zoneQ = $this->db->query("SELECT id,zone_name FROM `sht_zones` WHERE  (isp_uid='".$isp_uid."' AND city_id='".$cityid."') OR (isp_uid='0'  AND city_id='".$cityid."')");
            $gen .= $allzone;
            $num_rows = $zoneQ->num_rows();
            if ($num_rows > 0) {
                $gen .= $allzone;
                foreach ($zoneQ->result() as $znobj) {
                    $sel = '';
                    if ($zoneid == $znobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $znobj->id . '" ' . $sel . '>' . $znobj->zone_name . '</option>';
                }
            }
        } else {

            $query = $this->db->query("select zone_id from sht_dept_region where dept_id='" . $dept_id . "' and state_id='" . $stateid . "'");
            $zonearr = array();
            foreach ($query->result() as $val) {
                $zonearr[] = $val->zone_id;
            }
            //  echo "<pre>"; print_R($zonearr);
            if (in_array('all', $zonearr)) {
                //   echo "sssss"; die;
                $allzone = '<option value="all">All Zone</option>';
                 $zoneQ = $this->db->query("SELECT id,zone_name FROM `sht_zones` WHERE  (isp_uid='".$isp_uid."' AND city_id='".$cityid."') OR (isp_uid='0'  AND city_id='".$cityid."')");
            } else {
                $allzone = '<option value="">Select Zone</option>';
                $zid = '"' . implode('", "', $zonearr) . '"';
                $zoneQ = $this->db->query("select id,zone_name from sht_zones where id IN ({$zid})");
            }
            $num_rows = $zoneQ->num_rows();
            $gen .= $allzone;
            if ($num_rows > 0) {

                foreach ($zoneQ->result() as $znobj) {
                    $sel = '';
                    if ($zoneid == $znobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $znobj->id . '" ' . $sel . '>' . $znobj->zone_name . '</option>';
                }
            }
        }
        return $gen;
    }
	
	public function pool_listing()
	{
		 $sessiondata = $this->session->userdata('isp_session');
        $isp_uid=$sessiondata['isp_uid'];
		$query=$this->db->query("select `pool_name`,`start_ip`,`stop_ip`,`ipcount` from sht_ip_poolrange where isp_uid='".$isp_uid."'");
		return $query->result();
		
	}

    public function getcitylist($stateid, $cityid = '') {
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid=$sessiondata['isp_uid'];
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
        $regiontype = $this->plan_model->dept_region_type();
        $allcity = '<option value="all">All Cities</option>';
        $gen = '';
        if ($regiontype == "allindia" || $superadmin == 1) {


          $cityQ = $this->db->query("SELECT city_id,city_name FROM sht_cities WHERE  (isp_uid='".$isp_uid."' AND city_state_id='".$stateid."') OR (isp_uid='0'  AND  city_state_id='".$stateid."')");
            $num_rows = $cityQ->num_rows();
            if ($num_rows > 0) {
                $gen .= $allcity;
                foreach ($cityQ->result() as $ctobj) {
                    $sel = '';
                    if ($cityid == $ctobj->city_id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $ctobj->city_id . '" ' . $sel . '>' . $ctobj->city_name . '</option>';
                }
            }
        } else {
            $query = $this->db->query("select city_id from sht_dept_region where dept_id='" . $dept_id . "' and state_id='" . $stateid . "'");
            $cityarr = array();
            foreach ($query->result() as $val) {
                $cityarr[] = $val->city_id;
            }

            if (in_array('all', $cityarr)) {

                $allcity = '<option value="all">All Cities</option>';
               $cityQ = $this->db->query("SELECT city_id,city_name FROM sht_cities WHERE  (isp_uid='".$isp_uid."' AND city_state_id='".$stateid."') OR (isp_uid='0'  AND  city_state_id='".$stateid."')");
            } else {

                $allcity = '<option value="">Select City</option>';
                $cid = '"' . implode('", "', $cityarr) . '"';
                $cityQ = $this->db->query("select city_id,city_name from sht_cities where city_id IN ({$cid})");
            }
            //  $this->db->last_query(); die;
            $num_rows = $cityQ->num_rows();
            if ($num_rows > 0) {
                $gen .= $allcity;
                foreach ($cityQ->result() as $ctobj) {
                    $sel = '';
                    if ($cityid == $ctobj->city_id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $ctobj->city_id . '" ' . $sel . '>' . $ctobj->city_name . '</option>';
                }
            }
        }
        return $gen;
    }

    public function checknas_exist() {
        $postdata = $this->input->post();
        $nasname = $postdata['nasname'];
        if (isset($postdata['nasid']) && $postdata['nasid'] != '') {
            $this->db->where('id !=', $postdata['nasid']);
        }


        $this->db->where('shortname', $nasname);
        $query = $this->db->get(SHTNAS);
        //  echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function checknasip_exist() {
        $postdata = $this->input->post();
        $nasip = $postdata['nasip'];
        if (isset($postdata['nasid']) && $postdata['nasid'] != '') {
            $this->db->where('id !=', $postdata['nasid']);
        }
        $this->db->where('nasname', $nasip);
        //$condarr=array('nasname'=>$nasip);
        $query = $this->db->get(SHTNAS);
        //  echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function ping($host) {
        exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host)), $res, $rval);
        return $rval;
    }

    public function nas_plan_association($postdata, $nasid) {

         $condarr = array();
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $sczcond='';
       $permicond='';
                    $stateid = $postdata['state'];
                    $cityid = $postdata['city'];
                    $zoneid = $postdata['zone'];
                    if ($cityid == 'all') {
                        $permicond .= " (state_id='" . $stateid . "') ";
                    } elseif ($zoneid == 'all') {
                        $permicond .= " (state_id='" . $stateid . "' AND city_id='" . $cityid . "') ";
                    } else {
                        $permicond .= " (state_id='" . $stateid . "' AND city_id='" . $cityid . "' AND zone_id='" . $zoneid . "') ";
                    }
                   
                
            
            if($permicond!='')
            {
            $sczcond .= 'and (' . $permicond . ')';
            }
              $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
        
        
      


       

        $paramarr = array();
        $query = $this->db->query("SELECT distinct(plan_id) FROM sht_plan_region WHERE status='1'   AND is_deleted='0' {$ispcond} $sczcond");

        foreach ($query->result() as $val) {
            $paramarr[] = $val->plan_id;
        }

        $query2 = $this->db->query("select srvid from sht_plan_pricing where region_type='allindia'");
        $paramarr1 = array();
        if ($query2->num_rows() > 0) {
            foreach ($query2->result() as $val2) {
                $paramarr1[] = $val2->srvid;
            }
        }

        $paramarr = array_unique(array_merge($paramarr, $paramarr1));
        foreach ($paramarr as $pval) {
            $query1 = $this->db->query("select srvid from sht_plannasassoc where srvid='" . $pval . "' and nasid='" . $nasid . "'");
            if ($query1->num_rows() == 0) {
                $tabledata = array('srvid' => $pval, 'nasid' => $nasid);
                $this->db->insert('sht_plannasassoc', $tabledata);
            }
        }
    }
    
    public function add_pool_data()
    {
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
        $postdata=$this->input->post();
		
        $start = ip2long($postdata['startip']);
  $end = ip2long($postdata['stopip']);
  $poolarr= array_map('long2ip', range($start, $end));
  
  $query=$this->db->query("select ip from sht_ippool where isp_uid='".$isp_uid."'");
  $existingarr=array();
  if($query->num_rows()>0)
  {
      foreach($query->result() as $vald)
      {
         $existingarr[]= $vald->ip;
      }
  }
  
  $dataiparr=array_diff($poolarr,$existingarr);
  
  if(count($dataiparr)>0)
  {
  $ipcount=count($dataiparr);
  $tabledata=array('pool_name'=>$postdata['poolname'],"start_ip"=>$postdata['startip'],"stop_ip"=>$postdata['stopip'],
      "isp_uid"=>$isp_uid,"ipcount"=>$ipcount,"created_on"=>date("Y-m-d H:i:s"));
  $this->db->insert('sht_ip_poolrange',$tabledata);
  $id=$this->db->insert_id();
  
  foreach($dataiparr as $vcal)
  {
	  $tabledata1=array("ip"=>$vcal,"pool_id"=>$id,"isp_uid"=>$isp_uid,"is_assigned"=>0,"status"=>1);
	   $this->db->insert('sht_ippool',$tabledata1);
  }
  
  
	 $data['result']=1;
	 $gen="<tr><td></td><td>".$postdata['poolname']."</td><td>".$postdata['startip']."</td><td>".$postdata['stopip']."</td><td>".$ipcount."</td></tr>";
	 $data['html']=$gen;
  }
  else
  {
	  $data['result']=0;
	  $data['html']='';
  }
	 echo json_encode($data);
  
  
  
        
    }
    
    
    public function isp_license_data(){
		$wallet_amt = 0; $passbook_amt = 0;
		$walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='".ISPID."' AND is_paid='1'");
		if($walletQ->num_rows() > 0){
		    $wallet_amt = $walletQ->row()->wallet_amt;
		}
		
		$passbookQ = $this->db->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost FROM sht_isp_passbook WHERE isp_uid='".ISPID."'");
		if($passbookQ->num_rows() > 0){
		    $passrowdata = $passbookQ->row();
		    $passbook_amt = $passrowdata->activeusers_cost;
		}
		$balanceamt = $wallet_amt - $passbook_amt;
		if($balanceamt < 0){
			return 0;
		}else{
			return 1;
		}
	}

    /**********************
     *	IIL CONFIGURATION
     ***********************/
    public function getnasdetails(){
        $data = array();
        $id = $this->input->post('nasid');
        $query = $this->db->query("SELECT nasname FROM ".SHTNAS." WHERE id='".$id."'");
        if($query->num_rows() > 0){
            $data['error'] = '0';
            $data['nasname'] = $query->row()->nasname;
        }else{
            $data['error'] = '1';
        }
        echo json_encode($data);
    }
    public function test_ill_config(){
        $this->load->library('routerlib');
        $router_port = '8728';
        $router_id = '14.102.15.250';
        $router_user = 'admin';
        $router_password = 'shouut';
        
        //$ipranges = $this->getEachIpInRange('70.71.72.73/28');
        //print_r($ipranges); die;
        
        if($router_port != ''){
            $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
        }else{
            $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password");	
        }
        if($router_conn !== null){
            $check_queue = $router_conn->getall("/queue/simple");
            $check_interface = $router_conn->getall("/interface/ethernet");
            $check_iponrouter = $router_conn->getall("/ip/route", array('.id' => "14.102.15.240/28"));
            
            print_r($check_queue); die;
            //echo '<pre>'; print_r($check_interface); die;
            
            /*$input = new stdClass();
            $input->ip = "14.102.15.240";
            $input->netmask = "255.255.255.240";
            $input->ip_int = ip2long($input->ip);
            $input->netmask_int = ip2long($input->netmask);
            // Network is a logical AND between the address and netmask
            $input->network_int = $input->ip_int & $input->netmask_int;
            $input->network = long2ip($input->network_int);
            // Broadcast is a logical OR between the address and the NOT netmask
            $input->broadcast_int = $input->ip_int | (~ $input->netmask_int);
            $input->broadcast = long2ip($input->broadcast_int);
            
            //print_r($input); die;
            $network = $input->network;

            //check the IP address that the user is entering actually is configured on the router
            $check_iponrouter = $router_conn->getall("/ip/route");
            echo '<pre>'; print_r($check_iponrouter);
            foreach($check_iponrouter as $chk_dstaddr){
                echo $chk_dstaddr['dst-address'] .'<br/>';
            }
            */
            $ipaddress = $router_conn->add("/ip/address",array("interface"=>"ether1","address"=>"192.168.12.1/24"));
            var_dump($ipaddress); die;
            
        }else{
            die("couldn't connect to router");
        }
    }
    public function start_configuration(){
        $data = array();
        $this->load->library('routerlib');
        $router_id = $this->input->post('router_ip');
        $router_user = $this->input->post('router_user');
        $router_password = $this->input->post('router_password');
        $router_port = $this->input->post('router_port');
        $nasid = $this->input->post('nasid');
        $tableid = $this->input->post('tableid');
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        
        $router_port = (isset($router_port) && ($router_port != '')) ? $router_port : '8728';
        
        if($router_port != ''){
            $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
        }else{
            $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password");	
        }
        
        if($router_conn !== null){
            $data['error'] = '0';
            $nasdata = array(
                'isp_uid' => $isp_uid,
                'nasid' => $nasid,
                'nasname' => $router_id,
                'username' => $router_user,
                'password' => $router_password,
                'port' => $router_port
            );
            
            if($tableid != ''){
                $nas_illQuery = $this->db->query("SELECT id FROM nas_ill WHERE nasid='".$nasid."' AND id='".$tableid."'");
                if($nas_illQuery->num_rows() > 0){
                    $this->db->update('nas_ill', $nasdata, array('nasid' => $nasid));
                }
            }else{
                $this->db->insert('nas_ill', $nasdata);
                $tableid = $this->db->insert_id();
            }

            $interfaces = '<option value="">Select Interface</option>';
            
            //1. check if there is any interface ethernet
            $check_interface = $router_conn->getall("/interface/ethernet");
            foreach($check_interface as $check_interface1){
                $interface_name = $check_interface1['name'];
                $interface_defaultname = $check_interface1['default-name'];
                $data['intername'] = $interface_name;
                $data['deintername'] = $interface_defaultname;
                
                if($interface_name != $interface_defaultname){
                    $router_conn->set('/interface/ethernet', array('.id' => "$interface_name", 'name' => "$interface_defaultname"));
                }
                $interfaces .= '<option value="'.$interface_defaultname.'">'.$interface_defaultname.'</option>';
            }
            $data['interfaces'] = $interfaces;
            $data['tableid'] = $tableid;
        }else{
            $data['error'] = '1';
        }
        
        echo json_encode($data);
    }
    
    public function interface_netmask_configuration(){
        $data = array();
        $this->load->library('routerlib');
        $interface_name = $this->input->post('interface_name');
        $ip_type = $this->input->post('ip_type');
        $natmask = $this->input->post('natmask');
        $nasid = $this->input->post('nasid');
        $gateway_ip = $this->input->post('gateway_ip');
        $tableid = $this->input->post('tableid');
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        
        //check gateway ip alreay exists
        
        $nasdetailsQ = $this->db->query("SELECT port,nasname,username,password FROM nas_ill WHERE nasid='".$nasid."'");
        if($nasdetailsQ->num_rows() > 0){
            $rawdata = $nasdetailsQ->row();
            $router_port = $rawdata->port;
            $router_port = (isset($router_port) && ($router_port != '')) ? $router_port : '8728';
            
            $router_id = $rawdata->nasname;
            $router_user = $rawdata->username;
            $router_password = $rawdata->password;
            
            if($router_port != ''){
                $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
            }else{
                $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password");	
            }
            
            if($router_conn !== null){
                $data['conn_error'] = '0';
            
                //check the IP address that the user is entering actually is configured on the router
                //$check_iponrouter = $router_conn->getall("/ip address print");
                //print_r($check_iponrouter); die;
                
                $netmaskip = '';
                $getnasmaskipQ = $this->db->query("SELECT netmask FROM netmasking_ip WHERE slash='".$natmask."'");
                if($getnasmaskipQ->num_rows() > 0){
                    $netmaskip = $getnasmaskipQ->row()->netmask;
                }
                
                if($netmaskip != ''){
                    $input = new stdClass();
                    $input->ip = "$gateway_ip";
                    $input->netmask = "$netmaskip";
                    $input->ip_int = ip2long($input->ip);
                    $input->netmask_int = ip2long($input->netmask);
                    // Network is a logical AND between the address and netmask
                    $input->network_int = $input->ip_int & $input->netmask_int;
                    $input->network = long2ip($input->network_int);
                    // Broadcast is a logical OR between the address and the NOT netmask
                    $input->broadcast_int = $input->ip_int | (~ $input->netmask_int);
                    $input->broadcast = long2ip($input->broadcast_int);
                    
                    //print_r($input); die;
                    $subnetID = $input->network;
                    
                    $ip_network_exists = 0;
                    //check the IP address that the user is entering actually is configured on the router
                    $check_iponrouter = $router_conn->getall("/ip/route");
                    if(count($check_iponrouter) > 0){
                        foreach($check_iponrouter as $chk_dstaddr){
                            $ipwithmask = $subnetID.'/'.$natmask;
                            if($chk_dstaddr['dst-address'] == "$ipwithmask"){
                                $ip_network_exists = '1';
                                break;
                            }
                        }
                    }
                    
                    if($ip_network_exists == 0){
                        $ip_address = $router_conn->add("/ip/address",array("interface"=>"$interface_name","address"=>$gateway_ip."/".$natmask));
                        $idle_time_out = $router_conn->add("/ip/firewall/nat",array("chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"NAT_STATIC"));
                        $ip_firewall_addresslist_add = $router_conn->add("/ip/firewall/address-list", array("list"=>"NAT_STATIC", "address"=>$gateway_ip."/".$natmask));
                    
                        $netmask_data = array(
                            'interface_name' => $interface_name,
                            'ip_type' => $ip_type,
                            'gateway_ip_address' => $gateway_ip,
                            'netmask' => $natmask,
                            'netmask_ip' => $netmaskip,
                            'subnetID' => $subnetID
                        );
                        if($tableid != ''){
                            $this->db->update('nas_ill', $netmask_data, array('nasid' => $nasid,  'id' => $tableid));
                        }else{
                            $nasdata = array(
                                'isp_uid' => $isp_uid,
                                'nasid' => $nasid,
                                'nasname' => $router_id,
                                'username' => $router_user,
                                'password' => $router_password,
                                'port' => $router_port,
                                'interface_name' => $interface_name,
                                'ip_type' => $ip_type,
                                'gateway_ip_address' => $gateway_ip,
                                'netmask' => $natmask,
                                'netmask_ip' => $netmaskip,
                                'subnetID' => $subnetID
                            );
                            $this->db->insert('nas_ill', $nasdata);
                            $tableid = $this->db->insert_id();
                        }
                        $data['message'] = 'success';
                    }else{
                        $data['message'] = 'Network '.$gateway_ip.' already exist please enter some other IP Address';
                    }
                }
                else{
                    $data['message'] = 'Invalid Netmask Value';
                }
            }else{
                $data['conn_error'] = '1';
            }
        }
        echo json_encode($data);
    }

    public function add_ill_iprange(){
        $data = array();
        $nasid = $this->input->post('nasid');
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        
        $this->load->library('routerlib');
        $nasdetailsQ = $this->db->query("SELECT * FROM nas_ill WHERE nasid='".$nasid."'");
        if($nasdetailsQ->num_rows() > 0){
            foreach($nasdetailsQ->result() as $nasobj){
                $gateway_ip = $nasobj->gateway_ip_address;
                $netmaskip = $nasobj->netmask_ip;
                $netmask = $nasobj->netmask;
                $tableid = $nasobj->id;
                $router_port = $nasobj->port;
                $interface = $nasobj->interface_name;
                $router_port = (isset($router_port) && ($router_port != '')) ? $router_port : '8728';
                
                $router_id = $nasobj->nasname;
                $router_user = $nasobj->username;
                $router_password = $nasobj->password;
                
                if($router_port != ''){
                    $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
                }else{
                    $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password");	
                }
                
                if($router_conn !== null){
                    $gethost_ipranges = $this->getEachIpInRange($gateway_ip.'/'.$netmask);
                    
                    $input = new stdClass();
                    $input->ip = "$gateway_ip";
                    $input->netmask = "$netmaskip";
                    $input->ip_int = ip2long($input->ip);
                    $input->netmask_int = ip2long($input->netmask);
                    // Network is a logical AND between the address and netmask
                    $input->network_int = $input->ip_int & $input->netmask_int;
                    $input->network = long2ip($input->network_int);
                    // Broadcast is a logical OR between the address and the NOT netmask
                    $input->broadcast_int = $input->ip_int | (~ $input->netmask_int);
                    $input->broadcast = long2ip($input->broadcast_int);
                    
                    //print_r($input); die;
                    $subnetID = $input->network;
                    $broadcastIP = $input->broadcast;
                    
                    $extractips = array("$gateway_ip", "$subnetID", "$broadcastIP");
                    $dataiparr = array_diff($gethost_ipranges, $extractips);
                    if(count($dataiparr) > 0){
                        foreach($dataiparr as $ip_addr){
                            $iprangearr = array(
                                'nas_ill_id' => $tableid,
                                'ip_address' => $ip_addr,
                                'isp_uid' => $isp_uid
                            );
                            $checkipQ = $this->db->query("SELECT id FROM nas_ill_iprange WHERE isp_uid='".$isp_uid."' AND ip_address='".$ip_addr."'");
                            if($checkipQ->num_rows() == 0){
                                $this->db->insert('nas_ill_iprange', $iprangearr);
                            }
                        }
                    }
                    
                    $start_ip = reset($gethost_ipranges);
                    $end_ip = end($gethost_ipranges);
                    $this->db->update('nas_ill', array('start_ip' => "$start_ip", 'end_ip' => "$end_ip"), array('id' => $tableid));
                    //$user_add = $router_conn->add("/user",array("disabled"=>"no", "name"=>"$router_user", "password"=>"$router_password","group"=>"full"));
                    
                }
            }
            
            $this->db->update('nas', array('is_ill_configured' => '1'), array('id' => $nasid));
            
            echo json_encode(true);
        }
    }
    function getIpRange(  $cidr) {
        list($ip, $mask) = explode('/', $cidr);    
        $maskBinStr =str_repeat("1", $mask ) . str_repeat("0", 32-$mask );      //net mask binary string
        $inverseMaskBinStr = str_repeat("0", $mask ) . str_repeat("1",  32-$mask ); //inverse mask
    
        $ipLong = ip2long( $ip );
        $ipMaskLong = bindec( $maskBinStr );
        $inverseIpMaskLong = bindec( $inverseMaskBinStr );
        $netWork = $ipLong & $ipMaskLong; 
    
        $start = $netWork+1;//ignore network ID(eg: 192.168.1.0)
    
        $end = ($netWork | $inverseIpMaskLong) -1 ; //ignore brocast IP(eg: 192.168.1.255)
        return array('firstIP' => $start, 'lastIP' => $end );
    }
    
    function getEachIpInRange ( $cidr) {
        $ips = array();
        $range = $this->getIpRange($cidr);
        for ($ip = $range['firstIP']; $ip <= $range['lastIP']; $ip++) {
            $ips[] = long2ip($ip);
        }
        return $ips;
    }

    public function getill_iprange(){
        $data = array();
        $nasid = $this->input->post('nasid');
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        
        $gen = '<ul>';
        $gen .= '<li style="width:49%; display:inline-block; font-size:18px; font-weight:bold;">IP ADDRESS</li><li style="width:49%; display:inline-block; font-size:18px; font-weight:bold;float:right;">ASSIGNED TO</li>';
        $nasdetails = $this->db->query("SELECT ip_address, uid FROM `nas_ill_iprange` WHERE `isp_uid` = '".$isp_uid."'");
        if($nasdetails->num_rows() > 0){
            foreach($nasdetails->result() as $nasobj){
                $ip_address = $nasobj->ip_address;
                $uuid = $nasobj->uid;
                if(($uuid == NULL) || ($uuid == '')){
                    $uuid = '&nbsp;';
                }
                $gen .= '<li style="width:49%; display:inline-block;">'.$ip_address.'</li><li style="width:49%;float:right;display:inline-block;">'.$uuid.'</li>';
            }
        }
        $gen .= '</ul>';
        
        $data['ill_iplists'] = $gen;
        echo json_encode($data);
        
    }   
}

?>