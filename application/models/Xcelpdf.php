<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Xcelpdf extends CI_Model{
    
    public function __construct(){
        
    }
    public function getcustomer_data($uuid){
        $userQuery = $this->db->get_where('sht_users', array('uid' => $uuid));
        $num_rows = $userQuery->num_rows();
        return $userQuery->row();
    }
    public function user_billing_listing($uuid, $billid){
        $query = $this->db->query('SELECT * FROM sht_subscriber_billing WHERE subscriber_uuid="'.$uuid.'" AND status="1" AND is_deleted="0" AND id="'.$billid.'" ORDER BY id DESC LIMIT 1');
        $num_rows = $query->num_rows();
        return $query->row();
    }

    public function getplandetail($srvid){
        $data = array();
        $srvQuery = $this->db->query("SELECT srvname FROM sht_services WHERE srvid='".$srvid."'");
        if($srvQuery->num_rows() > 0){
            $rowdata = $srvQuery->row();
            
            $data['planname'] = $rowdata->srvname;
        }
        return $data;
    }
    public function userbalance_amount($uuid){
        $wallet_amt = 0;
        $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
        if($walletQ->num_rows() > 0){
                $wallet_amt = $walletQ->row()->wallet_amt;
        }
        
        $passbook_amt = 0;
        $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
        if($passbookQ->num_rows() > 0){
                $passbook_amt = $passbookQ->row()->plan_cost;
        }
        $balanceamt = $wallet_amt - $passbook_amt;
        return $balanceamt;
    }


    public function getstatename($stateid){
        $name = '';
        $stateQ = $this->db->get_where('sht_states', array('id' => $stateid));
        $num_rows = $stateQ->num_rows();
        if($num_rows > 0){
                $data = $stateQ->row();
                $name = ucwords($data->state);
        }
        return $name;
    }
    public function getcityname($stateid, $cityid){
        $name = '';
        $cityQ = $this->db->get_where('sht_cities', array('city_state_id' => $stateid, 'city_id' => $cityid));
        $num_rows = $cityQ->num_rows();
        if($num_rows > 0){
                $data = $cityQ->row();
                $name = ucwords($data->city_name);
        }
        return $name;
    }
    public function getzonename($zoneid){
        $name = '';
        $zoneQ = $this->db->get_where('sht_zones', array('id' => $zoneid));
        $num_rows = $zoneQ->num_rows();
        if($num_rows > 0){
                $data = $zoneQ->row();
                $name = ucwords($data->zone_name);
        }
        return $name;
    }

    public function isp_details($isp_uid){
        $query = $this->db->query("SELECT * FROM sht_isp_detail WHERE isp_uid='".$isp_uid."' AND status='1'");
        if($query->num_rows() > 0){
            return  $query->row_array();
        }
    }
    
    public function getplan_details($srvid, $uuid=''){
        $data = array();
        $planQ = $this->db->query("SELECT tb1.srvname, tb1.descr, tb2.gross_amt, tb1.plan_duration, tb1.isp_uid, tb1.plantype FROM sht_services as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb1.srvid='".$srvid."' AND tb1.enableplan='1'");
        if($planQ->num_rows() > 0){
            $rowdata = $planQ->row();
            $isp_uid = $rowdata->isp_uid;
            $gross_amt = $rowdata->gross_amt;
	    $actual_amt = 0;
            
            $custom_planpriceArr = $this->getcustom_planprice($isp_uid, $uuid, $srvid);
            $custom_planprice = $custom_planpriceArr['gross_amt'];
            if($custom_planprice != ''){
		$actual_amt = $custom_planprice;
                $planprice = round($custom_planprice);
                $plan_duration = $rowdata->plan_duration;
                $plan_tilldays = ($plan_duration * 30);
            }else{
		$actual_amt = $gross_amt;
                $tax = $this->current_taxapplicable($isp_uid);
                $plan_duration = $rowdata->plan_duration;
                $gross_amt = ($gross_amt * $plan_duration);
                $planprice = round($gross_amt + (($gross_amt * $tax)/100));
                $plan_tilldays = ($plan_duration * 30);
            }
            
            $planname = $rowdata->srvname;
            $plandesc = $rowdata->descr;
            
            $data['planname'] = $planname;
            $data['plandesc'] = $plandesc;
            $data['price'] = $planprice;
	    $data['actual_price'] = $actual_amt;
            $data['plantype'] = $rowdata->plantype;
            $data['plan_duration'] = $plan_duration;
            $data['plan_tilldays'] = $plan_tilldays;
            
            return $data;
        }
    }
    public function gettopup_details($srvid){
        $data = array();
        $planQ = $this->db->query("SELECT tb1.srvname, tb1.descr, tb2.gross_amt, tb1.isp_uid, tb1.topuptype FROM sht_services as tb1 INNER JOIN sht_topup_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb1.srvid='".$srvid."' AND tb1.enableplan='1'");
        if($planQ->num_rows() > 0){
            $rowdata = $planQ->row();
            $isp_uid = $rowdata->isp_uid;
            $gross_amt = $rowdata->gross_amt;
            $planname = $rowdata->srvname;
            $plandesc = $rowdata->descr;
            
            $data['topupname'] = $planname;
            $data['topupdesc'] = $plandesc;
            $data['topupprice'] = $gross_amt;
            $data['topuptype'] = $rowdata->topuptype;
            
            return $data;
        }
    }

    public function appliedtopup_details($srvid){
	$data = array();
        $planQ = $this->db->query("SELECT topup_startdate, topup_enddate, applicable_days, topuptype FROM sht_usertopupassoc WHERE topup_id='".$srvid."' AND status='1' AND terminate='0' ORDER BY id DESC LIMIT 1");
        if($planQ->num_rows() > 0){
            $rowdata = $planQ->row();
            
            $data['topup_startdate'] = $rowdata->topup_startdate;
	    $data['topup_enddate'] = $rowdata->topup_enddate;
            $data['applicable_days'] = $rowdata->applicable_days;
            $data['topuptype'] = $rowdata->topuptype;
        }
	return $data;
    }
    public function get_isplogo($isp_uid){
        $query = $this->db->query("select tb1.portal_url, tb2.logo_image from sht_isp_admin as tb1 INNER JOIN sht_isp_detail as tb2 ON(tb1.isp_uid=tb2.isp_uid) where tb1.is_activated='1' AND tb1.isp_uid='".$isp_uid."'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return "https://www.shouut.com/ispadmins/".$rowdata->portal_url.".shouut.com/ispmedia/logo/".$rowdata->logo_image;
        }else{
            return  0;
        }
    }
    
    public function get_ispsignaturelogo($isp_uid){
        $query = $this->db->query("select tb1.portal_url, tb2.logo_signature from sht_isp_admin as tb1 INNER JOIN sht_isp_detail as tb2 ON(tb1.isp_uid=tb2.isp_uid) where tb1.is_activated='1' AND tb1.isp_uid='".$isp_uid."'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return "https://www.shouut.com/ispadmins/".$rowdata->portal_url.".shouut.com/ispmedia/logo/".$rowdata->logo_signature;
        }else{
            return  0;
        }
    }


    
    
    public function getIndianCurrency($number){
        $number = (float) $number;
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $hundred = null;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred','thousand','lakh', 'crore');
        while( $i < $digits_length ) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str[] = null;
        }
        $Rupees = implode('', array_reverse($str));
        $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
        return ($Rupees ? ucwords($Rupees) . 'Rupees ' : '') . $paise ;
    }
    public function current_taxapplicable($isp_uid){
        $query = $this->db->query("SELECT tax FROM sht_tax WHERE isp_uid='".$isp_uid."'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return $rowdata->tax;
        }
    }
    public function getisp_accounttype($isp_uid){
        $query = $this->db->query("SELECT decibel_account FROM sht_isp_admin WHERE isp_uid='".$isp_uid."' AND is_activated='1'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return $rowdata->decibel_account;
        }
    }
    public function lastplan_activatedate($billid){
        $lastplan_activatedate = '';
        $lastplandateQ = $this->db->query("SELECT lastplan_activatedate FROM sht_userplan_datechange_records WHERE bill_id='".$billid."'");
        if($lastplandateQ->num_rows() > 0){
            $rowdata = $lastplandateQ->row();
            $lastplan_activatedate = $rowdata->lastplan_activatedate;
        }
        return $lastplan_activatedate;
    }
    public function country_currency($isp_uid){
	$currsymb = '';
	$ispcountryQ = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".$isp_uid."'");
	if($ispcountryQ->num_rows() > 0){
	    $crowdata = $ispcountryQ->row();
	    $countryid = $crowdata->country_id;
    
	    $countryQ = $this->db->query("SELECT currency_id FROM sht_countries WHERE id='".$countryid."'");
	    if($countryQ->num_rows() > 0){
		$rowdata = $countryQ->row();
		$currid = $rowdata->currency_id;
		
		$currencyQ = $this->db->get_where('sht_currency', array('currency_id' => $currid));
		if($currencyQ->num_rows() > 0){
		    $currobj = $currencyQ->row();
		    $currsymb = $currobj->currency_symbol;
		}
	    }
	}
	return $currsymb;
    }

    
    public function getcustom_planprice($isp_uid, $uuid, $srvid){
        $data = array();
        $customplanQ = $this->db->query("SELECT baseplanid,gross_amt FROM sht_custom_plan_pricing WHERE isp_uid='".$isp_uid."' AND uid='".$uuid."' AND baseplanid='".$srvid."' ORDER BY id DESC LIMIT 1");
        $num_rows = $customplanQ->num_rows();
        if($num_rows > 0){
                $rowdata = $customplanQ->row();
                $data['baseplanid'] = $rowdata->baseplanid;
                $data['gross_amt'] = $rowdata->gross_amt;
        }else{
                $data['baseplanid'] = '';
                $data['gross_amt'] = '';
        }
        return $data;
    }



/*-------------------------------------------------------------------------------------------------------------*/    
    public function excelexport_userlisting(){
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 11
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            )
        );
        $styleArray2 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F28A8C')
            )
        );
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
       
        $objPHPExcel->getActiveSheet()->setTitle("Users Listing");
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A1','UID');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B1','Username');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C1','First Name');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D1','Last Name');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E1','Email');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('F1','Mobile');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('G1','Alternate Mobile');
        $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('H1','GSTIN Number');
        $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('I1','Expired On');
        $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('J1','Wallet Amount');
        $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray);        
        $objPHPExcel->getActiveSheet()->setCellValue('K1','Active Plan');
        $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($styleArray);

        
        $objPHPExcel->getActiveSheet()->setCellValue('M1','Address');
        $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('N1','State');
        $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('O1','City');
        $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('P1','Zone');
        $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($styleArray);
        
        
        /*
        $objPHPExcel->getActiveSheet()->setCellValue('A1','Perspective User Report');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        */
        
        $filteruids = $this->input->post('seluids');
        $today_date = date('d-m-Y');
        $j = 2;
        $uidsarr = explode(',' , $filteruids);
        foreach($uidsarr as $uid_idno){
            $userQ = $this->db->query("SELECT uid, username, firstname, lastname, mobile, email, alt_mobile, flat_number, address, address2, city, state, zone, expiration, gstin_number, baseplanid, isp_uid FROM sht_users WHERE id='".$uid_idno."'");
            if($userQ->num_rows() > 0){
                $urowdata = $userQ->row();
                $isp_uid = $urowdata->isp_uid;
                $uuid = $urowdata->uid;
                $username = $urowdata->username;
                $firstname = $urowdata->firstname;
                $lastname = $urowdata->lastname;
                $mobile = $urowdata->mobile;
                $alt_mobile = $urowdata->alt_mobile;
                $flat_number = $urowdata->flat_number;
                $address = $urowdata->address;
                $address2 = $urowdata->address2;
                $state = $this->getstatename($urowdata->state);
                $city = $this->getcityname($urowdata->state,$urowdata->city);
                $zone = $this->getzonename($urowdata->zone);
                $expiration = date('d-m-Y', strtotime($urowdata->expiration));
                $gstin_number = $urowdata->gstin_number;
                $baseplanid = $urowdata->baseplanid;
                $email = $urowdata->email;
                $wallet_amount = $this->userbalance_amount($uuid);
                
                $plandetArr = $this->getplandetail($baseplanid);
                $planname = $plandetArr['planname'];
                
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $uuid);
                if(strtotime($expiration) > strtotime($today_date) ){
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
                }else{
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray2);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$j,$username);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$j,$firstname);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$j,$lastname);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$j,$email);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$j,$mobile);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$j,$alt_mobile);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$j,$gstin_number);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$j,$expiration);
                
                if(strtotime($expiration) > strtotime($today_date) ){
                    $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($styleArray1);
                }else{
                    $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($styleArray2);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$j,$wallet_amount);
                $objPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('K'.$j,$planname);
                $objPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray($styleArray1);
                
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$j, $flat_number.', '. $address.', '.$address2);
                $objPHPExcel->getActiveSheet()->getStyle('M'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('N'.$j,$state);
                $objPHPExcel->getActiveSheet()->getStyle('N'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('O'.$j,$city);
                $objPHPExcel->getActiveSheet()->getStyle('O'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('P'.$j,$zone);
                $objPHPExcel->getActiveSheet()->getStyle('P'.$j)->applyFromArray($styleArray1);
                
                $j++;
            }
        }
        
        $session_data = $this->session->userdata('isp_session');
	$isp_uid = $session_data['isp_uid'];
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('assets/media/pdfexcel_dwnld/'.$isp_uid.'_userlisting.xlsx');
       
    }
    public function pdfexport_userbilling(){
        $filteruids = $this->input->post('billids_toexport');
        $billidsarr = explode(',' , $filteruids);
        
        //$billidsarr = array('1', '2', '7955');
        foreach($billidsarr as $billid){
            $billquery = $this->db->query('SELECT * FROM sht_subscriber_billing WHERE status="1" AND is_deleted="0" AND id="'.$billid.'" ORDER BY id DESC LIMIT 1');
            $billnum_rows = $billquery->num_rows();
            $billingdata = $billquery->row();
            $uuid = $billingdata->subscriber_uuid;
        
            $apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
            $billtype = $billingdata->bill_type;
            $user_rowdata = $this->getcustomer_data($uuid);
            $state = $this->getstatename($user_rowdata->billing_state);
            $city = $this->getcityname($user_rowdata->billing_state,$user_rowdata->billing_city);
            $zone = $this->getzonename($user_rowdata->billing_zone);
            $mobileno = $user_rowdata->billing_mobileno;
            $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
            $gstin_number = $user_rowdata->gstin_number;
            $email = $user_rowdata->email;
            $isp_uid = $user_rowdata->isp_uid;
            $taxtype = $user_rowdata->taxtype;
            $tax = $this->current_taxapplicable($isp_uid);
            if($taxtype == '1'){
                $apply_cgst_sgst_tax = 1;
            }else{
                $apply_igst_tax = 1;
            }
    
            $next_bill_date = date('d-m-Y', strtotime($user_rowdata->next_bill_date));
            $billingdata = $this->user_billing_listing($uuid, $billid);
            $bill_number = $billingdata->bill_number;
            $payment_mode = $billingdata->payment_mode;
            $amount = $billingdata->actual_amount;
            $discount = $billingdata->discount;
            if($discount != 0){
                $discount_amt = ($amount * $billingdata->discount)/100;
            }else{
                $discount_amt = 0;
            }
            $total_amount = $billingdata->total_amount;
            $service_tax = $billingdata->service_tax;
            $swachh_bharat_tax = $billingdata->swachh_bharat_tax;
            $krishi_kalyan_tax = $billingdata->krishi_kalyan_tax;
            $cgst_tax = $billingdata->cgst_tax;
            $sgst_tax = $billingdata->sgst_tax;
            $igst_tax = $billingdata->igst_tax;
            $amount_inwords = $this->getIndianCurrency($total_amount);
            
            $bill_comments = $billingdata->bill_comments;
            if($bill_comments != ''){
                $bill_comments = 'Other Details: '.$bill_comments;
            }
            $bill_remarks = $billingdata->bill_remarks;
            if($bill_remarks != ''){
                $bill_remarks = 'Remarks: '.$bill_remarks;
            }
            $currsymbol = $this->country_currency($isp_uid);
            
            //<td colspan="2" rowspan="5" valign="top">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
            if($service_tax != '0.00'){
                $actual_billamt = $billingdata->actual_amount;
                $tax_msg .= '
                    <tr>
                        <td>
                           <p style="margin:5px 0px">
                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
                              &nbsp;
                              </span>
                           </p>
                        </td>
                        <td  style="border-bottom: 1px solid #f1f2f2">
                           <p style="margin:5px 0px; text-align: center">
                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Service Tax</span>
                           </p>
                        </td>
                        <td style="border-bottom: 1px solid #f1f2f2">
                           <p style="margin:5px 0px; text-align: center">
                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
                              &nbsp;
                              </span>
                           </p>
                        </td>
                        <td style="border-bottom: 1px solid #f1f2f2">
                           <p style="margin:5px 0px; text-align: right">
                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($service_tax,2).'</span>
                           </p>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <p style="margin:5px 0px">
                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
                              &nbsp;
                              </span>
                           </p>
                        </td>
                        <td  style="border-bottom: 1px solid #f1f2f2">
                           <p style="margin:5px 0px; text-align: center">
                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Krishi Kalyan Tax</span>
                           </p>
                        </td>
                        <td style="border-bottom: 1px solid #f1f2f2">
                           <p style="margin:5px 0px; text-align: center">
                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
                              &nbsp;
                              </span>
                           </p>
                        </td>
                        <td style="border-bottom: 1px solid #f1f2f2">
                           <p style="margin:5px 0px; text-align: right">
                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($krishi_kalyan_tax,2).'</span>
                           </p>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="4" style="text-align: right">
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2" rowspan="3" valign="top">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
                        <td colspan="2" style="text-align: right">
                           <p style="margin:5px 0px; text-align: right">
                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
                              NET AMOUNT (Including Tax)
                              </span>
                           </p>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2" style="text-align: right">
                           <p style="margin:5px 0px; text-align: right">
                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
                           </p>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2" style="text-align: right">
                           <p style="margin:5px 0px; text-align: right">
                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
                           </p>
                        </td>
                     </tr>
                ';
                
               
            }else{
                if($apply_cgst_sgst_tax == 1){
                    $tax = (100 + $tax);
                    $actual_billamt = round(($total_amount * 100) /  $tax);
                    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
                        $taxamt = ($total_amount - $actual_billamt);
                        $cgst_tax = round($taxamt/2);
                        $sgst_tax = round($taxamt/2);
                    }
                    
                    $tax_msg .= '
                        <tr>
                            <td>
                               <p style="margin:5px 0px">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                  &nbsp;
                                  </span>
                               </p>
                            </td>
                            <td  style="border-bottom: 1px solid #f1f2f2">
                               <p style="margin:5px 0px; text-align: center">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">CGST @9%</span>
                               </p>
                            </td>
                            <td style="border-bottom: 1px solid #f1f2f2">
                               <p style="margin:5px 0px; text-align: center">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                  &nbsp;
                                  </span>
                               </p>
                            </td>
                            <td style="border-bottom: 1px solid #f1f2f2">
                               <p style="margin:5px 0px; text-align: right">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($cgst_tax,2).'</span>
                               </p>
                            </td>
                         </tr>
                         <tr>
                            <td>
                               <p style="margin:5px 0px">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                  &nbsp;
                                  </span>
                               </p>
                            </td>
                            <td  style="border-bottom: 1px solid #f1f2f2">
                               <p style="margin:5px 0px; text-align: center">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">SGST @9%</span>
                               </p>
                            </td>
                            <td style="border-bottom: 1px solid #f1f2f2">
                               <p style="margin:5px 0px; text-align: center">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                  &nbsp;
                                  </span>
                               </p>
                            </td>
                            <td style="border-bottom: 1px solid #f1f2f2">
                               <p style="margin:5px 0px; text-align: right">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($sgst_tax,2).'</span>
                               </p>
                            </td>
                         </tr>
                         <tr>
                            <td colspan="4" style="text-align: right">
                            </td>
                         </tr>
                         <tr>
                            <td colspan="2" rowspan="3" valign="top">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
                            <td colspan="2" style="text-align: right">
                               <p style="margin:5px 0px; text-align: right">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
                                  NET AMOUNT (Including Tax)
                                  </span>
                               </p>
                            </td>
                         </tr>
                         <tr>
                            <td colspan="2" style="text-align: right">
                               <p style="margin:5px 0px; text-align: right">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
                               </p>
                            </td>
                         </tr>
                         <tr>
                            <td colspan="2" style="text-align: right">
                               <p style="margin:5px 0px; text-align: right">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
                               </p>
                            </td>
                         </tr>
                    ';
                }else{
                    $tax = (100 + $tax);
                    $actual_billamt = round(($total_amount * 100) /  $tax);
                    if($igst_tax == '0.00'){
                        $igst_tax = ($total_amount - $actual_billamt);
                    }
                    
                    $tax_msg .= '
                        <tr>
                            <td>
                               <p style="margin:5px 0px">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                  &nbsp;
                                  </span>
                               </p>
                            </td>
                            <td  style="border-bottom: 1px solid #f1f2f2">
                               <p style="margin:5px 0px; text-align: center">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">IGST @18%</span>
                               </p>
                            </td>
                            <td style="border-bottom: 1px solid #f1f2f2">
                               <p style="margin:5px 0px; text-align: center">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                  &nbsp;
                                  </span>
                               </p>
                            </td>
                            <td style="border-bottom: 1px solid #f1f2f2">
                               <p style="margin:5px 0px; text-align: right">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($igst_tax,2).'</span>
                               </p>
                            </td>
                         </tr>
                         <tr>
                            <td colspan="4" style="text-align: right">
                            </td>
                         </tr>
                         <tr>
                            <td colspan="2" rowspan="3" valign="top">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
                            <td colspan="2" style="text-align: right">
                               <p style="margin:5px 0px; text-align: right">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
                                  NET AMOUNT (Including Tax)
                                  </span>
                               </p>
                            </td>
                         </tr>
                         <tr>
                            <td colspan="2" style="text-align: right">
                               <p style="margin:5px 0px; text-align: right">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
                               </p>
                            </td>
                         </tr>
                         <tr>
                            <td colspan="2" style="text-align: right">
                               <p style="margin:5px 0px; text-align: right">
                                  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
                               </p>
                            </td>
                         </tr>
                    ';
                    
                }
    
            }
    
            
            $bill_date = date('d-m-Y', strtotime($billingdata->bill_added_on));
            $plandetails = $this->getplan_details($billingdata->plan_id, $uuid);
            $plan_duration = $plandetails['plan_duration'];
            $plan_tilldays = $plandetails['plan_tilldays'];
            
            $lastplan_activatedate = $this->lastplan_activatedate($billid);
            $bill_starts_from = $billingdata->bill_starts_from;
            if($bill_starts_from == '0000-00-00'){
                if($lastplan_activatedate == ''){
                    $plan_activation_date = date('d-m-Y', strtotime($user_rowdata->plan_activated_date));
                }else{
                    $plan_activation_date = date('d-m-Y', strtotime($lastplan_activatedate));
                }
            }else{
                $plan_activation_date = date('d-m-Y', strtotime($billingdata->bill_starts_from));
            }
            $user_plan_type = $billingdata->user_plan_type;
            if($user_plan_type == 'prepaid'){
                $bill_tilldate = date('d-m-Y', strtotime($plan_activation_date." +$plan_tilldays day"));
            }else{
                $bill_tilldate = $bill_date;
            }
            
            $ispdetailArr = $this->isp_details($isp_uid);
            $company_name = $ispdetailArr['company_name'];
            $address = $ispdetailArr['address1'];
            $helpline = $ispdetailArr['help_number1'];
            $support_email = $ispdetailArr['support_email'];
            $isp_name = ucwords($ispdetailArr['isp_name']);
            $ispgst_number = $ispdetailArr['gst_no'];
            $isp_service_tax_no = $ispdetailArr['service_tax_no'];
            $ispcin_number = $ispdetailArr['cin_number'];
            $isp_website = $ispdetailArr['website'];
            $isp_state = $this->getstatename($ispdetailArr['state']);
            $isplogo = $this->get_isplogo($isp_uid);
            $isp_signature = $this->get_ispsignaturelogo($isp_uid);
            $isppancard = $ispdetailArr['pancard'];
            $invoice_color = $ispdetailArr['invoice_color'];
            $invoice_banner1 = $ispdetailArr['invoice_banner1'];
            $invoice_banner2 = $ispdetailArr['invoice_banner2'];
            $invoice_statecode = $ispdetailArr['invoice_statecode'];
            
            $isptaxdet = '';
            $billmonth = date('m', strtotime($bill_date));
            $billyear = date('Y', strtotime($bill_date));
            if(($billmonth < 7) && ($billyear == '2017')){
                $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">Service Tax : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isp_service_tax_no.'</span>';
            }else{
                $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">GSTIN : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispgst_number.'</span>';
            }
            
            
            $message = '';
            
            $ispacctQ = $this->db->query("SELECT tb1.portal_url, tb1.decibel_account FROM sht_isp_admin as tb1 WHERE tb1.isp_uid='".$isp_uid."'");
            if($ispacctQ->num_rows() > 0){
                $isprowdata = $ispacctQ->row();
                $portal_url = $isprowdata->portal_url;
                $decibel_account = $isprowdata->decibel_account;
                if($decibel_account == 'paid'){
                    $ispurl = 'https://'.$portal_url.'.shouut.com/consumer/';
                }else{
                    $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/consumer/';
                }
            }
            
            if($billtype == 'topup'){
                $plandetails = $this->gettopup_details($billingdata->plan_id);
                $topuptype = $plandetails['topuptype'];
                if($topuptype == '1'){
                    $topuptype = 'Data Topup';
                }elseif($topuptype == '2'){
                    $topuptype = 'Data Unaccountancy Topup';
                }elseif($topuptype == '3'){
                    $topuptype = 'Speed Topup';
                }
                $topupname = $plandetails['topupname'];
                $topupprice = $plandetails['topupprice'];
                
                $topupdetails = '';
                if($topuptype != 'Data Topup'){
                    $appliedtopup_details = $this->appliedtopup_details($billingdata->plan_id);
                    $topup_startdate = date('d-m-Y', strtotime($appliedtopup_details['topup_startdate']));
                    $topup_enddate = date('d-m-Y', strtotime($appliedtopup_details['topup_enddate']));
                    $applicable_days = $appliedtopup_details['applicable_days'];
                    $total_topupamount = ($topupprice * $applicable_days);
                    $topupdetails = $topup_startdate.' to '.$topup_enddate.' ('.$applicable_days.' days)';
                }
            }
            
            $this->load->library('Dpdf');
            ini_set('memory_limit', '256M');
            
            $message .= '
            <!doctype html>
                <html>
                   <head>
                      <meta charset="utf-8">
                      <meta name="viewport" content="width=device-width, initial-scale=1.0">
                      <meta http-equiv="X-UA-Compatible" content="IE=edge">
                      <title></title>
                      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=greek" rel="stylesheet">
                      <link rel="stylesheet" href="'.base_url().'assets/media/invoicebanner/css/font-awesome.min.css">
                      <style type="text/css">
                         html,  body {
                         margin: 0 !important;
                         padding: 0 !important;
                         height: 100% !important;
                         width: 100% !important;
                         }
                         * {
                         -ms-text-size-adjust: 100%;
                         -webkit-text-size-adjust: 100%;
                         }
                         .ExternalClass {
                         width: 100%;
                         }
    
                         div[style*="margin: 10px 0"] {
                         margin: 0 !important;
                         }
    
                         table,  td {
                         mso-table-lspace: 0pt !important;
                         mso-table-rspace: 0pt !important;
                         }
                         
                         table {
                         border-spacing: 0 !important;
                         border-collapse: collapse !important;
                         table-layout: fixed !important;
                         margin: 0 auto !important;
                         }
                         table table table {
                         table-layout: auto;
                         }
    
                         img {
                         -ms-interpolation-mode: bicubic;
                         }
    
                         .yshortcuts a {
                         border-bottom: none !important;
                         }
    
                         a[x-apple-data-detectors] {
                         color: inherit !important;
                         }
                      </style>
                      <style type="text/css">
    
                         .button-td,
                         .button-a {
                         transition: all 100ms ease-in;
                         }
                         .button-td:hover,
                         .button-a:hover {
                         background: #2fbdf2 !important;
                         border-color: #2fbdf2 !important;
                         }
    
                         .button-get-td,
                         .button-get-a {
                         transition: all 100ms ease-in;
                         }
                         .button-get-td:hover,
                         .button-get-a:hover {
                         background: #545454 !important;
                         border-color: #545454 !important;
                         }
                         /* Media Queries */
                         @media screen and (max-width: 600px) {
                         .email-container {
                         width: 100% !important;
                         }
    
                         .fluid,
                         .fluid-centered {
                         max-width: 100% !important;
                         height: auto !important;
                         margin-left: auto !important;
                         margin-right: auto !important;
                         }
                         /* And center justify these ones. */
                         .fluid-centered {
                         margin-left: auto !important;
                         margin-right: auto !important;
                         }
                         /* What it does: Forces table cells into full-width rows. */
                         .stack-column,
                         .stack-column-center {
                         display: block !important;
                         width: 100% !important;
                         max-width: 100% !important;
                         direction: ltr !important;
                         }
                         /* And center justify these ones. */
                         .stack-column-center {
                         text-align: center !important;
                         }
                         /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
                         .center-on-narrow {
                         text-align: center !important;
                         display: block !important;
                         margin-left: auto !important;
                         margin-right: auto !important;
                         float: none !important;
                         }
                         table.center-on-narrow {
                         display: inline-block !important;
                         }
                         }
                      </style>
                   </head>
                   <body bgcolor="#ffffff"  style="margin: 0;">';
            
            if($billtype == 'installation'){
                $hsn_sac = (($billingdata->hsn_number) != '') ? $billingdata->hsn_number : '9987';
                $message .= '
                <div style="width:750px; padding: 0px; margin: 0px auto;">
                         <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
                            <tr>
                               <td valign="top" style="width: 100%">
                                  <center style="width:100%; border:none">
                                     <!-- Email Header : BEGIN -->
                                     <table width="100%" border="0">
                                        <tr>
                                           <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
                                            if($isplogo != '0'){
                                                $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
                                            }else{
                                                $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
                                            }
                            $message .=    '</td>
                                           <td style="width:70%;">
                                              <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
                                           </td>
                                        </tr>
                                        <tr>
                                           <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
                                        </tr>
                                     </table>
                                     <!-- Email Header : END --> 
                                     <!-- Email Body : BEGIN -->
                                     <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
                                        <tr>
                                           <td style="width:40%;" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:5px 10px" valign="top">
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color:'.$invoice_color.'; font-size:10px;font-family: FontAwesome, sans-serif">
                                                          <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color:'.$invoice_color.'; font-size:10px">
                                                          <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color:'.$invoice_color.'; font-size:10px">
                                                          <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">'.$isptaxdet.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          State Code : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$invoice_statecode.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          CIN : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          PAN : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                           <td style="width:60%" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
                                                          User ID :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Cust. Acc. No. :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Invoice No. :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Invoice Type : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Installation Charges</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px; padding-bottom: 5px;">
                                                          <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                          <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td colspan="2">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Name : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td colspan="2">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Address :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Place of Supply :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Zone : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Mobile : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
                                                       </p>
                                                    </td>';
                                                    if(($billmonth >= 7) && ($billyear >= '2017')){
                                    $message .=	'<td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          GSTIN :  
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
                                                       </p>
                                                    </td>';
                                                    }else{
                                        $message .=	'<td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
                                                       </p>
                                                    </td>';
                                                    }
                            $message .=	     '</tr>
                                              </table>
                                           </td>
                                        </tr>
                                     </table>
                                     <!-- Email Body : END --> 
                                     <!-- Email Billing Details --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
                                        <tr>
                                           <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr bgcolor="'.$invoice_color.'">
                                                    <td style="width:220px;">
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
                                                    </td>
                                                 </tr>
                                                 <tr  style="border-bottom: 1px solid #f1f2f2">
                                                    <td>
                                                       <p style="margin:5px 0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">1. Installation</span>
                                                       </p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: center">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
                                                       </p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: center">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
                                                       </p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: right">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 '.$tax_msg.'
                                              </table>
                                           </td>
                                           <td style="width:30%;padding:10px 5px;" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>';
                                                 if($invoice_banner1 != ''){
                                                    $message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
                                                 }else{
                                                    $message .= '<td>&nbsp;</td>';
                                                 }
                                    $message .=  '</tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: left">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                        </tr>
                                     </table>
                                     <!-- Email Billing Details END --->
                                     <!--- Email Add --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                           <td style="width:25%; padding: 5px 0px">
                                              <p style="margin: 0px">';
                                                if($isp_signature != '0'){
                                                    $message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
                                                }
                                    $message .= '</p>
                                              <p style="margin:0px;">
                                                 <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
                                              </p>
                                              <p style="margin:0px;">
                                                 <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
                                              </p>
                                           </td>
                                           <td style="width:75%; padding:5px 0px">
                                              <p style="padding:0px; margin: 10px;">';
                                                if($invoice_banner2 != ''){
                                                    $message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
                                                }
                            $message .=	 '</p>
                                           </td>
                                        </tr>
                                     </table>
                                     <!--- Email Add End --->
                                     <!--- invoice details --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                                        <tr bgcolor="'.$invoice_color.'">
                                           <td colspan="3" style="padding:5px 10px;">
                                              <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
                                           </td>
                                        </tr>
                                        <tr>
                                           <td colspan="3" style="padding:10px 0px;">
                                              <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                 <tr>
                                                    <td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
                                                       <p style="margin:0px">
                                                          <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
                                                       </p>
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
                                                          Pay securely using our online payment gateway using credit or debit cards.
                                                       </p>
                                                       <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
                                                          <a href="#" style="color: #ffffff;text-decoration: none;"> 
                                                          Click To Pay Now!
                                                          </a>
                                                       </p>
                                                       <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
                                                    </td>
                                                    <td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
                                                       <p style="margin:0px">
                                                          <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
                                                       </p>
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
                                                          Pay your bills via netbanking. Required details provided below:
                                                       </p>';
                                                       if($ispdetailArr['account_number'] != ''){ 
                                                        $message .= '
                                                            <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Acc. Name : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Bank Name : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Branch : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Account Number : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              IFSC Code : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
                                                           </p>';
                                                        }else{
                                                        $message .= '
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>';
                                                        }
                                    $message .=	'</td>
                                                    <td style="width:33%; padding:5px" valign="top">
                                                       <p style="margin:0px">
                                                          <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
                                                       </p>
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
                                                          Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
                                                       </p>
                                                       <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
                                                          <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<i class="fa fa-android" aria-hidden="true"></i> Get App </a>
                                                       </p>
                                                       <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                        </tr>
                                     </table>
                                     <!--- invoice details end --->
                                     <!-- Email Billing Details --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                           <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          1. In case of cheque bounce, Rs.100/ penalty will be levied.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          2. 18% interest will be levied on overdue payments
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          3. Late fee charge in case the bill is paid after the due date.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          4. In case of overdue / defaults, the right to deactivate your services, is reserved.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          5. All disputes are subject to '.$state.' jurisdiction.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          7. This Invoice is system generated hence signature and stamp is not required.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                           <td style="width:30%;padding:20px 5px;" valign="top">
                                              <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
                                           </td>
                                        </tr>
                                     </table>
                                  </center>
                               </td>
                            </tr>
                         </table>
                      </div>
                   </body>
                </html>';
                //echo $message; die;
                
                $dpdf = new dpdf();
                $dpdf->load_html($message);
                $dpdf->render();
                $output = $billid. '.pdf';
                //$finalpdf = $this->dpdf->stream($output);
                $finalpdf = $dpdf->output();
                file_put_contents('assets/media/pdfexcel_dwnld/'.$output, $finalpdf);
            }
            elseif($billtype == 'security'){
                $hsn_sac = (($billingdata->hsn_number) != '') ? $billingdata->hsn_number : '9987';
                $message = '
                <div style="width:750px; padding: 0px; margin: 0px auto;">
                         <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
                            <tr>
                               <td valign="top" style="width: 100%">
                                  <center style="width:100%; border:none">
                                     <!-- Email Header : BEGIN -->
                                     <table width="100%" border="0">
                                        <tr>
                                           <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
                                            if($isplogo != '0'){
                                                $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
                                            }else{
                                                $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
                                            }
                            $message .=    '</td>
                                           <td style="width:70%;">
                                              <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
                                           </td>
                                        </tr>
                                        <tr>
                                           <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
                                        </tr>
                                     </table>
                                     <!-- Email Header : END --> 
                                     <!-- Email Body : BEGIN -->
                                     <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
                                        <tr>
                                           <td style="width:40%;" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:5px 10px" valign="top">
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color:'.$invoice_color.'; font-size:10px">
                                                          <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color:'.$invoice_color.'; font-size:10px">
                                                          <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color:'.$invoice_color.'; font-size:10px">
                                                          <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">'.$isptaxdet.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          State Code : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          CIN : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          PAN : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                           <td style="width:60%" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
                                                          User ID :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Cust. Acc. No. :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Invoice No. :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Invoice Type : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Security Charges</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px; padding-bottom: 5px;">
                                                          <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                          <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td colspan="2">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Name : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td colspan="2">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Address :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Place of Supply :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Zone : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Mobile : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
                                                       </p>
                                                    </td>';
                                                    if(($billmonth >= 7) && ($billyear >= '2017')){
                                    $message .=	'<td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          GSTIN :  
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
                                                       </p>
                                                    </td>';
                                                    }else{
                                        $message .=	'<td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
                                                       </p>
                                                    </td>';
                                                    }
                            $message .=	     '</tr>
                                              </table>
                                           </td>
                                        </tr>
                                     </table>
                                     <!-- Email Body : END --> 
                                     <!-- Email Billing Details --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
                                        <tr>
                                           <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr bgcolor="'.$invoice_color.'">
                                                    <td style="width:220px;">
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
                                                    </td>
                                                 </tr>
                                                 <tr  style="border-bottom: 1px solid #f1f2f2">
                                                    <td>
                                                       <p style="margin:5px 0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">1. Security Charges</span>
                                                       </p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: center">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
                                                       </p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: center">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
                                                       </p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: right">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 '.$tax_msg.'
                                              </table>
                                           </td>
                                           <td style="width:30%;padding:10px 5px;" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>';
                                                 if($invoice_banner1 != ''){
                                                    $message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
                                                 }else{
                                                    $message .= '<td>&nbsp;</td>';
                                                 }
                                    $message .=  '</tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: left">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                        </tr>
                                     </table>
                                     <!-- Email Billing Details END --->
                                     <!--- Email Add --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                           <td style="width:25%; padding: 5px 0px">
                                              <p style="margin: 0px">';
                                                if($isp_signature != '0'){
                                                    $message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
                                                }
                                    $message .= '</p>
                                              <p style="margin:0px;">
                                                 <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
                                              </p>
                                              <p style="margin:0px;">
                                                 <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
                                              </p>
                                           </td>
                                           <td style="width:75%; padding:5px 0px">
                                              <p style="padding:0px; margin: 10px;">';
                                                if($invoice_banner2 != ''){
                                                    $message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
                                                }
                            $message .=	 '</p>
                                           </td>
                                        </tr>
                                     </table>
                                     <!--- Email Add End --->
                                     <!--- invoice details --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr bgcolor="'.$invoice_color.'">
                                           <td colspan="3" style="padding:5px 10px;">
                                              <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
                                           </td>
                                        </tr>
                                        <tr>
                                           <td colspan="3" style="padding:10px 0px;">
                                              <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                 <tr>
                                                    <td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
                                                       <p style="margin:0px">
                                                          <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
                                                       </p>
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
                                                          Pay securely using our online payment gateway using credit or debit cards.
                                                       </p>
                                                       <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
                                                          <a href="#" style="color: #ffffff;text-decoration: none;"> 
                                                          Click To Pay Now!
                                                          </a>
                                                       </p>
                                                       <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
                                                    </td>
                                                    <td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
                                                       <p style="margin:0px">
                                                          <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
                                                       </p>
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
                                                          Pay your bills via netbanking. Required details provided below:
                                                       </p>';
                                                       if($ispdetailArr['account_number'] != ''){ 
                                                        $message .= '
                                                            <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Acc. Name : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Bank Name : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Branch : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Account Number : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              IFSC Code : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
                                                           </p>';
                                                        }else{
                                                        $message .= '
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>';
                                                        }
                                    $message .=	'</td>
                                                    <td style="width:33%; padding:5px" valign="top">
                                                       <p style="margin:0px">
                                                          <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
                                                       </p>
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
                                                          Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
                                                       </p>
                                                       <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
                                                          <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<i class="fa fa-android" aria-hidden="true"></i> Get App </a>
                                                       </p>
                                                       <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                        </tr>
                                     </table>
                                     <!--- invoice details end --->
                                     <!-- Email Billing Details --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                           <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          1. In case of cheque bounce, Rs.100/ penalty will be levied.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          2. 18% interest will be levied on overdue payments
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          3. Late fee charge in case the bill is paid after the due date.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          4. In case of overdue / defaults, the right to deactivate your services, is reserved.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          5. All disputes are subject to '.$state.' jurisdiction.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          7. This Invoice is system generated hence signature and stamp is not required.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                           <td style="width:30%;padding:20px 5px;" valign="top">
                                              <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
                                           </td>
                                        </tr>
                                     </table>
                                  </center>
                               </td>
                            </tr>
                         </table>
                      </div>
                   </body>
                </html>';
                
                $dpdf = new dpdf();
                $dpdf->load_html($message);
                $dpdf->render();
                $output = $billid. '.pdf';
                //$finalpdf = $this->dpdf->stream($output);
                $finalpdf = $dpdf->output();
                file_put_contents('assets/media/pdfexcel_dwnld/'.$output, $finalpdf);
    
            }
            elseif($billtype == 'topup'){
                $hsn_sac = (($billingdata->hsn_number) != '') ? $billingdata->hsn_number : '9984';
                $message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
                         <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
                            <tr>
                               <td valign="top" style="width: 100%">
                                  <center style="width:100%; border:none">
                                     <!-- Email Header : BEGIN -->
                                     <table width="100%" border="0">
                                        <tr>
                                           <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
                                            if($isplogo != '0'){
                                                $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
                                            }else{
                                                $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
                                            }
                            $message .=    '</td>
                                           <td style="width:70%;">
                                              <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
                                           </td>
                                        </tr>
                                        <tr>
                                           <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
                                        </tr>
                                     </table>
                                     <!-- Email Header : END --> 
                                     <!-- Email Body : BEGIN -->
                                     <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
                                        <tr>
                                           <td style="width:40%;" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:5px 10px" valign="top">
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color:'.$invoice_color.'; font-size:10px">
                                                          <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color:'.$invoice_color.'; font-size:10px">
                                                          <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color:'.$invoice_color.'; font-size:10px">
                                                          <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">'.$isptaxdet.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          State Code : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          CIN : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          PAN : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                           <td style="width:60%" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
                                                          User ID :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Cust. Acc. No. :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Invoice No. :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Invoice Type : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Topups Billing</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px; padding-bottom: 5px;">
                                                          <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                          <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td colspan="2">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Name : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td colspan="2">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Address :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Place of Supply :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Zone : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Mobile : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
                                                       </p>
                                                    </td>';
                                                    if(($billmonth >= 7) && ($billyear >= '2017')){
                                    $message .=	'<td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          GSTIN :  
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
                                                       </p>
                                                    </td>';
                                                    }else{
                                        $message .=	'<td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
                                                       </p>
                                                    </td>';
                                                    }
                            $message .=	     '</tr>
                                              </table>
                                           </td>
                                        </tr>
                                     </table>
                                     <!-- Email Body : END --> 
                                     <!-- Email Billing Details --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
                                        <tr>
                                           <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr bgcolor="'.$invoice_color.'">
                                                    <td style="width:220px;">
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
                                                    </td>
                                                 </tr>
                                                 <tr  style="border-bottom: 1px solid #f1f2f2">
                                                    <td>
                                                       <p style="margin:5px 0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$topuptype.': '.$topupname.' <br/> '.$topupdetails.'</span>
                                                       </p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: center">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
                                                       </p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: center">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
                                                       </p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: right">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 '.$tax_msg.'
                                              </table>
                                           </td>
                                           <td style="width:30%;padding:10px 5px;" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>';
                                                 if($invoice_banner1 != ''){
                                                    $message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
                                                 }else{
                                                    $message .= '<td>&nbsp;</td>';
                                                 }
                                    $message .=  '</tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: left">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                        </tr>
                                     </table>
                                     <!-- Email Billing Details END --->
                                     <!--- Email Add --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                           <td style="width:25%; padding: 5px 0px">
                                              <p style="margin: 0px">';
                                                if($isp_signature != '0'){
                                                    $message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
                                                }
                                    $message .= '</p>
                                              <p style="margin:0px;">
                                                 <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
                                              </p>
                                              <p style="margin:0px;">
                                                 <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
                                              </p>
                                           </td>
                                           <td style="width:75%; padding:5px 0px">
                                              <p style="padding:0px; margin: 10px;">';
                                                if($invoice_banner2 != ''){
                                                    $message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
                                                }
                            $message .=	 '</p>
                                           </td>
                                        </tr>
                                     </table>
                                     <!--- Email Add End --->
                                     <!--- invoice details --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr bgcolor="'.$invoice_color.'">
                                           <td colspan="3" style="padding:5px 10px;">
                                              <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
                                           </td>
                                        </tr>
                                        <tr>
                                           <td colspan="3" style="padding:10px 0px;">
                                              <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                 <tr>
                                                    <td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
                                                       <p style="margin:0px">
                                                          <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
                                                       </p>
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
                                                          Pay securely using our online payment gateway using credit or debit cards.
                                                       </p>
                                                       <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
                                                          <a href="#" style="color: #ffffff;text-decoration: none;"> 
                                                          Click To Pay Now!
                                                          </a>
                                                       </p>
                                                       <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
                                                    </td>
                                                    <td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
                                                       <p style="margin:0px">
                                                          <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
                                                       </p>
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
                                                          Pay your bills via netbanking. Required details provided below:
                                                       </p>';
                                                       if($ispdetailArr['account_number'] != ''){ 
                                                        $message .= '
                                                            <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Acc. Name : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Bank Name : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Branch : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Account Number : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              IFSC Code : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
                                                           </p>';
                                                        }else{
                                                        $message .= '
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>';
                                                        }
                                    $message .=	'</td>
                                                    <td style="width:33%; padding:5px" valign="top">
                                                       <p style="margin:0px">
                                                          <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
                                                       </p>
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
                                                          Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
                                                       </p>
                                                       <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
                                                          <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<i class="fa fa-android" aria-hidden="true"></i> Get App </a>
                                                       </p>
                                                       <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                        </tr>
                                     </table>
                                     <!--- invoice details end --->
                                     <!-- Email Billing Details --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                           <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          1. In case of cheque bounce, Rs.100/ penalty will be levied.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          2. 18% interest will be levied on overdue payments
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          3. Late fee charge in case the bill is paid after the due date.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          4. In case of overdue / defaults, the right to deactivate your services, is reserved.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          5. All disputes are subject to '.$state.' jurisdiction.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          7. This Invoice is system generated hence signature and stamp is not required.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                           <td style="width:30%;padding:20px 5px;" valign="top">
                                              <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
                                           </td>
                                        </tr>
                                     </table>
                                  </center>
                               </td>
                            </tr>
                         </table>
                      </div>
                   </body>
                </html>';
                
                $dpdf = new dpdf();
                $dpdf->load_html($message);
                $dpdf->render();
                $output = $billid. '.pdf';
                //$finalpdf = $this->dpdf->stream($output);
                $finalpdf = $dpdf->output();
                file_put_contents('assets/media/pdfexcel_dwnld/'.$output, $finalpdf);
            }
            elseif(($billtype == 'montly_pay') || ($billtype == 'midchange_plancost')){
                $hsn_sac = (($billingdata->hsn_number) != '') ? $billingdata->hsn_number : '9984';
                $message .='
                
                <div style="width:750px; padding: 0px; margin: 0px auto;">
                         <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
                            <tr>
                               <td valign="top" style="width: 100%">
                                  <center style="width:100%; border:none">
                                     <!-- Email Header : BEGIN -->
                                     <table width="100%" border="0">
                                        <tr>
                                           <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
                                            if($isplogo != '0'){
                                                $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
                                            }else{
                                                $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
                                            }
                            $message .=    '</td>
                                           <td style="width:70%;">
                                              <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
                                           </td>
                                        </tr>
                                        <tr>
                                           <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
                                        </tr>
                                     </table>
                                     <!-- Email Header : END --> 
                                     <!-- Email Body : BEGIN -->
                                     <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
                                        <tr>
                                           <td style="width:40%;" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:5px 10px" valign="top">
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color:'.$invoice_color.'; font-size:10px">
                                                          <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color:'.$invoice_color.'; font-size:10px">
                                                          <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color:'.$invoice_color.'; font-size:10px">
                                                          <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">'.$isptaxdet.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          State Code : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          CIN : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px 10px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          PAN : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                           <td style="width:60%" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
                                                          User ID :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Cust. Acc. No. :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Invoice No. :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Invoice Type : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Monthly Plan Bill</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px; padding-bottom: 5px;">
                                                          <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                          <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td colspan="2">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Name : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td colspan="2">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Address :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Place of Supply :
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
                                                       </p>
                                                    </td>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Zone : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          Mobile : 
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
                                                       </p>
                                                    </td>';
                                                    if(($billmonth >= 7) && ($billyear >= '2017')){
                                    $message .=	'<td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
                                                          GSTIN :  
                                                          </span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
                                                       </p>
                                                    </td>';
                                                    }else{
                                        $message .=	'<td style="padding:0px" valign="top">
                                                       <p style="margin:0px;">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
                                                       </p>
                                                    </td>';
                                                    }
                            $message .=	     '</tr>
                                              </table>
                                           </td>
                                        </tr>
                                     </table>
                                     <!-- Email Body : END --> 
                                     <!-- Email Billing Details --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
                                        <tr>
                                           <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr bgcolor="'.$invoice_color.'">
                                                    <td style="width:220px;">
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
                                                    </td>
                                                 </tr>
                                                 <tr  style="border-bottom: 1px solid #f1f2f2">
                                                    <td>
                                                       <p style="margin:5px 0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">Package: '.$plandetails['planname'].'</span>
                                                       </p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: center">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
                                                       </p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: center">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
                                                       </p>
                                                    </td>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: right">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 '.$tax_msg.'
                                              </table>
                                           </td>
                                           <td style="width:30%;padding:10px 5px;" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>';
                                                 if($invoice_banner1 != ''){
                                                    $message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
                                                 }else{
                                                    $message .= '<td>&nbsp;</td>';
                                                 }
                                    $message .=  '</tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:5px 0px; text-align: left">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                        </tr>
                                     </table>
                                     <!-- Email Billing Details END --->
                                     <!--- Email Add --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                           <td style="width:25%; padding: 5px 0px">
                                              <p style="margin: 0px">';
                                                if($isp_signature != '0'){
                                                    $message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
                                                }
                                    $message .= '</p>
                                              <p style="margin:0px;">
                                                 <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
                                              </p>
                                              <p style="margin:0px;">
                                                 <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
                                              </p>
                                           </td>
                                           <td style="width:75%; padding:5px 0px">
                                              <p style="padding:0px; margin: 10px;">';
                                                if($invoice_banner2 != ''){
                                                    $message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
                                                }
                            $message .=	 '</p>
                                           </td>
                                        </tr>
                                     </table>
                                     <!--- Email Add End --->
                                     <!--- invoice details --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr bgcolor="'.$invoice_color.'">
                                           <td colspan="3" style="padding:5px 10px;">
                                              <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
                                           </td>
                                        </tr>
                                        <tr>
                                           <td colspan="3" style="padding:10px 0px;">
                                              <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                 <tr>
                                                    <td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
                                                       <p style="margin:0px">
                                                          <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
                                                       </p>
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
                                                          Pay securely using our online payment gateway using credit or debit cards.
                                                       </p>
                                                       <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
                                                          <a href="#" style="color: #ffffff;text-decoration: none;"> 
                                                          Click To Pay Now!
                                                          </a>
                                                       </p>
                                                       <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
                                                    </td>
                                                    <td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
                                                       <p style="margin:0px">
                                                          <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
                                                       </p>
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
                                                          Pay your bills via netbanking. Required details provided below:
                                                       </p>';
                                                       if($ispdetailArr['account_number'] != ''){ 
                                                        $message .= '
                                                            <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Acc. Name : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Bank Name : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Branch : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              Account Number : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
                                                           </p>
                                                           <p style="margin:0px;">
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
                                                              IFSC Code : 
                                                              </span> 
                                                              <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
                                                           </p>';
                                                        }else{
                                                        $message .= '
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>
                                                             <p style="margin:0px;">
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
                                                                <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
                                                             </p>';
                                                        }
                                    $message .=	'</td>
                                                    <td style="width:33%; padding:5px" valign="top">
                                                       <p style="margin:0px">
                                                          <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
                                                       </p>
                                                       <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
                                                          Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
                                                       </p>
                                                       <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
                                                          <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<i class="fa fa-android" aria-hidden="true"></i> Get App </a>
                                                       </p>
                                                       <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                        </tr>
                                     </table>
                                     <!--- invoice details end --->
                                     <!-- Email Billing Details --->
                                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                           <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          1. In case of cheque bounce, Rs.100/ penalty will be levied.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          2. 18% interest will be levied on overdue payments
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          3. Late fee charge in case the bill is paid after the due date.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          4. In case of overdue / defaults, the right to deactivate your services, is reserved.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          5. All disputes are subject to '.$state.' jurisdiction.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                       <p style="margin:0px">
                                                          <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
                                                          7. This Invoice is system generated hence signature and stamp is not required.
                                                          </span>
                                                       </p>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </td>
                                           <td style="width:30%;padding:20px 5px;" valign="top">
                                              <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
                                           </td>
                                        </tr>
                                     </table>
                                  </center>
                               </td>
                            </tr>
                         </table>
                      </div>
                    </body>
                </html>';
                
                $dpdf = new dpdf();
                $dpdf->load_html($message);
                $dpdf->render();
                $output = $billid. '.pdf';
                //$finalpdf = $this->dpdf->stream($output);
                $finalpdf = $dpdf->output();
                file_put_contents('assets/media/pdfexcel_dwnld/'.$output, $finalpdf);
    
            }
            elseif($billtype == 'addtowallet'){
                $hsn_sac = (($billingdata->hsn_number) != '') ? $billingdata->hsn_number : '9984';
                $current_wallet = $this->userbalance_amount($uuid);
                $message = '
                <table style="border:1px solid #A3A3A3;" width="850px" cellspacing="0px" cellpadding="0px" align="center">
                   <tbody>
                      <tr>
                         <td>
                            <table width="850px" cellspacing="0px" cellpadding="0px" border="0px" align="center">
                               <tbody>
                                  <tr>
                                     <td style="background-color:#F4F2F3;" width="360px" valign="top">
                                        <table style="border-bottom-right-radius:15px; background-color:#FFFFFF;"  border="0px" width="360px">
                                           <tbody>
                                              <tr>
                                                 <td width="50px" height="30"></td>
                                              </tr>
                                              <tr>
                                                 <td style="padding-left:20px" valign="top" align="left"><img src="'.$isplogo.'" class="img-responsive" alt="isplogo" width="160px"></td>
                                              </tr>
                                              <tr>
                                                 <td style="font-family: \'Open Sans\', sans-serif; font-size:22px; padding-left:20px;font-weight:bold;">
                                                    <span style="color:#F00F64;font-weight:bold;"><br>'.$company_name.'</span><br>
                                                    <table width="100%" border="0px">
                                                       <tbody>
                                                        <tr>
                                                          <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:bold;">'.$address.'</td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:bold;"> '.$ispdetailArr['city'].' - '.$ispdetailArr['pincode'].'</td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:bold;">Ph.No : +91 '.$ispdetailArr['help_number1'].'</td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:bold;">E-mail : '.$ispdetailArr['support_email'].' </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:bold;">GSTIN : '.$ispdetailArr['gst_no'].'</td>
                                                       </tr>
                                                       </tbody>
                                                    </table>
                                                    <!-- Address ENDS-->
                                                 </td>
                                              </tr>
                                           </tbody>
                                        </table>
                                     </td>
                                     <td style="background-color:#F4F2F3;" width="490px" valign="top">
                                        <table cellspacing="0px" width="486px" cellpadding="0px" border="0px">
                                           <tbody>
                                              <tr>
                                                 <td style="height:auto;" bgcolor="#FFFFFF">
                                                    <table style="border-top-left-radius:15px;background-color:#F4F2F3;" width="" border="0px">
                                                       <tbody>
                                                          <tr>
                                                             <td style="padding-left:35px;font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:600;" width="300px" height="195px">
                                                                <table border="0px">
                                                                   <tbody>
                                                                      <tr>
                                                                         <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:600;"><span style="line-height: 25px">Name</span> : <span style="line-height: 25px">'.ucfirst($user_rowdata->firstname).' '.ucfirst($user_rowdata->lastname).'</span></td>
                                                                      </tr>
                                                                      <tr>
                                                                         <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:600;">Address : '.ucfirst($user_rowdata->address).'</td>
                                                                      </tr>
                                                                      <tr>
                                                                         <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:600;">'.$state.'</td>
                                                                      </tr>
                                                                      <tr>
                                                                         <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:600;">India</td>
                                                                      </tr>
                                                                      <tr>
                                                                         <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:600;">Home : </td>
                                                                      </tr>
                                                                      <tr>
                                                                         <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:600;">Mobile : '.$user_rowdata->mobile.'</td>
                                                                      </tr>
                                                                      <tr>
                                                                         <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:600;">GSTIN : </td>
                                                                      </tr>
                                                                   </tbody>
                                                                </table>
                                                             </td>
                                                             <td width="250px">
                                                                <table border="0px">
                                                                   <tbody>
                                                                      <tr>
                                                                         <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:600;">User Id : '.$user_rowdata->uid.'</td>
                                                                      </tr>
                                                                      <tr>
                                                                         <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:600;">A/C No : '.$user_rowdata->uid.'</td>
                                                                      </tr>
                                                                      <tr>
                                                                         <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:600;">Receipt Number : '.$receipt_number.'</td>
                                                                      </tr>
                                                                      <tr>
                                                                         <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;font-weight:600;">Bill Date : '.$bill_date.'</td>
                                                                      </tr>
                                                                   </tbody>
                                                                </table>
                                                             </td>
                                                          </tr>
                                                       </tbody>
                                                    </table>
                                                 </td>
                                              </tr>
                                              <tr height="20px">
                                                 <td>&nbsp;</td>
                                              </tr>
                                           </tbody>
                                        </table>
                                     </td>
                                  </tr>
                                  <tr>
                                     <td colspan="2" style="background-color:#F4F2F3;"><br></td>
                                  </tr>
                                  <tr>
                                     <td valign="top" bgcolor="#F4F2F3"  width="385px">
                                        <table cellspacing="0px" cellpadding="0px" border="0px" style="margin-left:8px">
                                           <tbody>
                                              <tr>
                                                 <td valign="top" style="padding-top:20px">
                                                    &nbsp;
                                                 </td>
                                              </tr>
                                           </tbody>
                                        </table>
                                     </td>
                                     <td valign="top" width="460px" bgcolor="#F4F2F3">
                                        <table style="padding: 0px 5px" width="450px" cellspacing="0px" cellpadding="0px" border="0px">
                                           <tbody>
                                              <tr>
                                                 <td>
                                                    <table style="border: 1px solid #F00F64;border-radius:10px; background-color:#ffffff" width="450px" cellspacing="0px" cellpadding="10px" border="0px">
                                                       <tbody>
                                                          <tr>
                                                             <td colspan="2" style=" background-color:#F00F64;color:#fff; font-weight:600;font-family: \'Open Sans\', sans-serif; font-size:24px;border-bottom: 1px solid #F00F64;border-top-left-radius:8px; border-top-right-radius:8px">Wallet Summary</td>
                                                          </tr>
                                                          <tr bgcolor="#f9f9f9">
                                                            <td style="color:#36465f; font-weight:600;font-family: \'Open Sans\', sans-serif; font-size:20px; text-align: left;">Amount Added</td>
                                                            <td style="color:#36465f; font-weight:600;font-family: \'Open Sans\', sans-serif; font-size:20px; text-align: right;">Rs. '.number_format($amount, 2).'</td>
                                                         </tr>
                                                         <tr>
                                                            <td style="color:#36465f; font-weight:600;font-family: \'Open Sans\', sans-serif; font-size:20px; text-align: left;">Mode Of Payment</td>
                                                            <td style="color:#36465f; font-weight:600;font-family: \'Open Sans\', sans-serif; font-size:20px; text-align: right;">'.$payment_mode.'</td>
                                                         </tr>
                                                         <tr>
                                                            <td style="color:#36465f; text-align: left; font-weight:600;font-family: \'Open Sans\', sans-serif; font-size:20px">Status</td>
                                                            <td style="color:#36465f; text-align:right; font-weight:600;font-family: \'Open Sans\', sans-serif; font-size:20px">'.$billstatus.'</td>
                                                         </tr>';
                                                         if($current_wallet < 0){
                                                            $wstyle = '#E9A19F';
                                                         }else{
                                                            $wstyle = '#9FD438';
                                                         }
                                        $message .=    '<tr bgcolor="'.$wstyle.'">
                                                            <td style="color:#36465f; text-align: left; font-weight:600;font-family: \'Open Sans\', sans-serif; font-size:20px">Current Wallet Balance</td>
                                                            <td style="color:#36465f; text-align:right; font-weight:600;font-family: \'Open Sans\', sans-serif; font-size:20px">Rs. '.number_format($current_wallet, 2).'</td>
                                                         </tr>
                                                       </tbody>
                                                    </table>
                                                 </td>
                                              </tr>
                                           </tbody>
                                        </table>
                                     </td>
                                  </tr>
                                  <tr>
                                     <td colspan="2" style="color:#36465f; text-align: right; font-weight:600;font-family: \'Open Sans\', sans-serif; font-size:20px;padding:10px ">In words: '.$amount_inwords.'</td>
                                  </tr>
                                  <tr>
                                     <td colspan="2" style="color:#36465f; text-align: right; font-weight:600;font-family: \'Open Sans\', sans-serif; font-size:20px; padding:10px">
                                        <p style="margin:0px">For Legal Entity</p>
                                        <p style="margin:0px">&nbsp;</p>
                                        <p style="margin:0px">'.$isp_name.'</p>
                                     </td>
                                  </tr>
                                  <tr>
                                     <td colspan="2" style="background-color:#F4F2F3;"><br></td>
                                  </tr>
                                  <tr>
                                     <td colspan="2" style="background-color:#F4F2F3;"><br></td>
                                  </tr>
                                  <tr bgcolor="#F4F2F3">
                                     <td colspan="2" style="padding: 0px 15px">
                                        <table style="border-radius:15px;background-color:#CCCCCC;" width="850px" cellspacing="0px" cellpadding="0px" border="0px" align="center">
                                           <tbody>
                                              <tr style="font-weight:bold; font-size:20px" height="25px">
                                                 <td style="padding-left:20px; font-size:20px; color:#000" height="30px; font-family: \'Open Sans\', sans-serif;">Terms and Conditions </td>
                                              </tr>
                                              <tr>
                                                 <td style="font-family: \'Open Sans\', sans-serif; font-size:20px;color:#7A7A7A;font-weight:500; border-radius:15px">
                                                    <table style="font-family: \'Open Sans\', sans-serif; font-size:20px;border-bottom-right-radius:15px;border-bottom-left-radius:15px;background-color:#FFF; padding:0px 5px" width="800px" cellspacing="0px" cellpadding="0px" border="0px" align="center">
                                                       <tbody>
                                                          <tr>
                                                             <td>
                                                                <!-- start Terms and conditions!-->
                                                                <ol style="line-height:25px;padding-left:20px">
                                                                   <br>
                                                                    <li>Please make your payments before the due date to enjoy uninterrupted service of your internet connection.</li>
                                                                    <li>If the service is discontinued for non-payment of dues, then a reconnection charge may be applicable.</li>
                                                                    <li>Late payment charges may be applicable after the payment due date.</li>
                                                                    <li>All invoice disputes should be highlighted within 7 days of invoice generation.</li>
                                                                    <li>GSTIN No: '.$gst_number.'</li>
                                                                    <li>GST charges as applicable and notified by the Govt. of India would be applicable.</li>
                                                                    <li>All disputes are subject to the courts of '.$isp_state.'</li>
                                                                    <li>For any queries on the invoice please call '.$helpline.' or write at '.$support_email.'.</li>
                                                                </ol>
                                                                <!-- End Terms and conditions!-->
                                                             </td>
                                                          </tr>
                                                       </tbody>
                                                    </table>
                                                 </td>
                                              </tr>
                                           </tbody>
                                        </table>
                                     </td>
                                  </tr>
                                  <tr bgcolor="#F4F2F3">
                                     <td colspan="2" height="20px"></td>
                                  </tr>
                                  <tr bgcolor="#F4F2F3">
                                     <td colspan="2" height="20px"></td>
                                  </tr>
                                  <tr>
                                     <td colspan="2" height="5px"></td>
                                  </tr>
                               </tbody>
                            </table>
                         </td>
                      </tr>
                   </tbody>
                </table>
                ';
                
                $dpdf = new dpdf();
                $dpdf->load_html($message);
                $dpdf->render();
                $output = $billid. '.pdf';
                //$finalpdf = $this->dpdf->stream($output);
                $finalpdf = $dpdf->output();
                file_put_contents('assets/media/pdfexcel_dwnld/'.$output, $finalpdf);
            }
    
            
            elseif($billtype == 'advprepay'){
                $hsn_sac = (($billingdata->hsn_number) != '') ? $billingdata->hsn_number : '9984';
                $advpayfor = 0; $freemonths = 0;
                $advmonthpayQ = $this->db->query("SELECT advancepay_for_months, number_of_free_months FROM sht_advance_payments WHERE bill_id='".$billid."'");
                if($advmonthpayQ->num_rows() > 0){
                    $advrowdata = $advmonthpayQ->row();
                    $advpayfor = $advrowdata->advancepay_for_months;
                    $freemonths = $advrowdata->number_of_free_months;
                }
                $tax_msg = '';
                if($apply_cgst_sgst_tax == 1){
                    $tax = (100 + $tax);
                    $actual_billamt = round(($total_amount * 100) /  $tax);
                    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
                        $taxamt = ($total_amount - $actual_billamt);
                        $cgst_tax = round($taxamt/2);
                        $sgst_tax = round($taxamt/2);
                    }
                $tax_msg = '
                <tr>
                    <td style="border-bottom:1px solid #000000;">&nbsp;</td>
                    <td style="border-bottom:1px solid #000000;">&nbsp;</td>
                    <td style="border-bottom:1px solid #000000; border-right:1px solid #000000;padding-right:10px;text-align: right;">
                       <p style="color:#000; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">CGST @ 9 %</span></p>
                    </td>
                    <td style="border-bottom:1px solid #000000; border-right:none;padding-right:10px;text-align: right;">
                       <p style="color:#000; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">'.number_format($cgst_tax,2).'</p>
                    </td>
                 </tr>
                 <tr>
                    <td style="border-bottom:1px solid #000000;">&nbsp;</td>
                    <td style="border-bottom:1px solid #000000;">&nbsp;</td>
                    <td style="border-bottom:1px solid #000000; border-right:1px solid #000000;padding-right:10px;text-align: right;">
                       <p style="color:#000; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">SGST @ 9 %</span></p>
                    </td>
                    <td style="border-bottom:1px solid #000000; border-right:none; padding-right:10px;text-align: right;">
                       <p style="color:#000; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">'.number_format($sgst_tax,2).'</p>
                    </td>
                 </tr>';
                }else{
                    $tax = (100 + $tax);
                    $actual_billamt = round(($total_amount * 100) /  $tax);
                    if($igst_tax == '0.00'){
                        $igst_tax = ($total_amount - $actual_billamt);
                    }
                    $tax_msg = '
                    <tr>
                        <td style="border-bottom:1px solid #000000;">&nbsp;</td>
                        <td style="border-bottom:1px solid #000000;">&nbsp;</td>
                        <td style="border-bottom:1px solid #000000; border-right:1px solid #000000;padding-right:10px;text-align: right;">
                           <p style="color:#000; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">IGST @ 18 %</span></p>
                        </td>
                        <td style="border-bottom:1px solid #000000; border-right:none;padding-right:10px;text-align: right;">
                           <p style="color:#000; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">'.number_format($igst_tax, 2).'</p>
                        </td>
                    </tr>';
                }
                
                $tax_msg .= '
                 <tr>
                    <td style="border-bottom:1px solid #000000;">&nbsp;</td>
                    <td style="border-bottom:1px solid #000000;">&nbsp;</td>
                    <td style="border-bottom:1px solid #000000; border-right:1px solid #000000; padding-left:10px; padding-top:2px;text-align: right;">
                       <p style="color:#000; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">Net Amount</p>
                    </td>
                    <td style="border-bottom:1px solid #000000; border-right:none; padding-right:10px;padding-top:2px;text-align: right;">
                       <p style="color:#000;font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">'.number_format($total_amount, 2).'</p>
                    </td>
                 </tr>
                 <tr>
                    <td colspan="4" style="border-bottom:none; border-left:none; padding-top:2px; padding-left: 10px;text-align: right;">
                       <p style="color:#000; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Amount in words. : </span> '.$amount_inwords.'</p>
                    </td>
                 </tr>';
                
                $message = '
                <div style="max-width:840px; height: 100%; margin: auto; background-color:#fff; padding:10px">
                    <div style="width:790px; height:auto; margin: auto; border:1px solid #a3a3a3; padding: 0px;">
                       <table width="790" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                             <tr>
                                <td colspan="3" style="text-align:right">
                                   <img src="'.base_url().'/assets/images/emailer/Invoice.png" width="25%"/>
                                </td>
                             </tr>
                             <tr>
                                <td width="350" style="background-color:#f4f2f3" valign="top">
                                   <table width="350" border="0" cellspacing="0" cellpadding="0" style="background-color: #fff; border-bottom-right-radius:15px; padding:10px">
                                      <tbody>
                                         <tr>
                                            <td style="text-align: left;">';
                                            if($isplogo != '0'){
                                                $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" />';
                                            }else{
                                                $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" /> ';
                                            }
                            $message .='	</td>
                                         </tr>
                                         <tr>
                                            <td>
                                               <p style="color:#00a2e1; text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:5px 0px 0px 0px; padding:0px 0px 0px 10px;">'.$company_name.'</p>
                                            </td>
                                         </tr>
                                         <tr>
                                            <td>
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:5px 0px 0px 0px; padding:0px 0px 0px 10px;">'.$address.'</p>
                                            </td>
                                         </tr>
                                         <tr>
                                            <td>
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:5px 0px 0px 0px; padding:0px 0px 0px 10px;">Ph.No : +91 '.$ispdetailArr['help_number1'].'</p>
                                            </td>
                                         </tr>
                                         <tr>
                                            <td>
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:5px 0px 0px 0px; padding:0px 0px 0px 10px;">E-mail: '.$ispdetailArr['support_email'].'</p>
                                            </td>
                                         </tr>
                                         <tr>
                                            <td>
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:5px 0px 0px 0px; padding:0px 0px 0px 10px;">GSTIN: '.$ispgst_number.'</p>
                                            </td>
                                         </tr>
                                         <tr>
                                            <td>
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:5px 0px 0px 0px; padding:0px 0px 0px 10px;">CIN: '.$ispcin_number.'</p>
                                            </td>
                                         </tr>
                                      </tbody>
                                   </table>
                                </td>
                                <td width="440">
                                   <table width="440" border="0" cellspacing="0" cellpadding="0" style="padding:20px;border-top-left-radius:15px;background-color:#f4f2f3">
                                      <tbody>
                                         <tr>
                                            <td width="281" scope="col">
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;margin:2px 0px 0px 0px; padding:0px 10px">Name : <span>'.ucfirst($user_rowdata->billing_username).'</span></p>
                                            </td>
                                            <td width="159" scope="col">
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">User Id : '.$user_rowdata->uid.'</p>
                                            </td>
                                         </tr>
                                         <tr>
                                            <td>
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">
                                                  Address : '.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'	
                                               </p>
                                            </td>
                                            <td>&nbsp;</td>
                                         </tr>
                                         <tr>
                                            <td>
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">
                                                  GSTIN: '.$gstin_number.'
                                               </p>
                                            </td>
                                            <td>
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">Invoice No. : '.$bill_number.'</p>
                                            </td>
                                         </tr>
                                         <tr>
                                            <td>
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">
                                                  HSN/SAC : '.$hsn_sac.'
                                               </p>
                                            </td>
                                            <td>
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">Invoice Date : '.$bill_date.'</p>
                                            </td>
                                         </tr>
                                         <tr>
                                            <td>
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">Place of supply : '.$state.'</p>
                                            </td>
                                            <td>&nbsp;</td>
                                         </tr>
                                         <tr>
                                            <td>
                                               <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">Mobile : '.$mobileno.'</p>
                                            </td>
                                            <td>&nbsp;</td>
                                         </tr>
                                         <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                         </tr>
                                          <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                         </tr>
                                         <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                         </tr>
                                      </tbody>
                                   </table>
                                </td>
                             </tr>
                             <tr style="background-color:#f4f2f3">
                                <td style="padding:10px 10px 0px" colspan="2">
                                   <table width="770" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #000; padding:0px; margin:15px 0px; ">
                                      <tbody>
                                         <tr bgcolor="#f00f64">
                                            <td width="103" style="border-bottom:1px solid #f00f64; border-top:1px solid #f00f64; border-right:1px solid #ffffff; padding-top:5px; text-align: center;"><span style="font-weight: 600; color:#fff; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px; ">Sr. No.</span></td>
                                            <td width="338" style="border-bottom:1px solid #f00f64;border-top:1px solid #f00f64; border-right:1px solid #ffffff; padding-top:5px; text-align: center">
                                               <span style="font-weight: 600; color:#fff;text-align: center; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px;">Particulars</span>
                                            </td>
                                            <td width="238" style="border-bottom:1px solid #f00f64;border-top:1px solid #f00f64; border-right:1px solid #ffffff; text-align: center;padding-top:5px;">
                                               <span style="font-weight: 600; color:#fff;text-align: center; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px;">Plan Price</span>
                                            </td>
                                            <td width="238" style="border-bottom:1px solid #f00f64;border-top:1px solid #f00f64; border-right:1px solid #ffffff; text-align: center;padding-top:5px;">
                                               <span style="font-weight: 600; color:#fff;text-align: center; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px;">Advanced Pay(in months)</span>
                                            </td>
                                            <td width="238" style="border-bottom:1px solid #f00f64;border-top:1px solid #f00f64; border-right:1px solid #ffffff; text-align: center;padding-top:5px;">
                                               <span style="font-weight: 600; color:#fff;text-align: center; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px;">Free Months</span>
                                            </td>
                                            <td width="109" style="border-bottom:1px solid #f00f64;border-top:1px solid #f00f64; border-right:none;text-align: center;padding-top:5px;">
                                               <span style="font-weight: 600; color:#fff;text-align: center; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px;">Amount</span>
                                            </td>
                                         </tr>
                                         <tr>
                                            <td style="border-bottom:1px solid #000000; border-right:1px solid #000000; text-align: center ;padding-top:5px; ">
                                               <span style="font-weight: 600; color:#000; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px; ">1.</span>
                                            </td>
                                            <td style="border-bottom:1px solid #000000; border-right:1px solid #000000;padding-top:5px;">
                                               <span style="font-weight: 600; color:#000; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px; ">Package : '.$plandetails['planname'].'</span>
                                            </td>
                                            <td style="border-bottom:1px solid #000000; border-right:1px solid #000000;padding-top:5px; text-align: center">
                                               <span style="font-weight: 600; color:#000; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px; ">'.number_format($plandetails['actual_price'], 2).'</span>
                                            </td>
                                            <td style="border-bottom:1px solid #000000; border-right:1px solid #000000;padding-top:5px; text-align: center">
                                               <span style="font-weight: 600; color:#000; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px; ">'.$advpayfor.'</span>
                                            </td>
                                            <td style="border-bottom:1px solid #000000; border-right:1px solid #000000;padding-top:5px; text-align: center">
                                               <span style="font-weight: 600; color:#000; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px; ">'.$freemonths.'</span>
                                            </td>
                                            <td style="border-bottom:1px solid #000000; border-right:none; padding-top:5px; padding-right:5px; text-align: right">
                                               <span style="font-weight: 600; color:#000; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px; "> '.number_format($actual_billamt, 2).'</span>
                                            </td>
                                         </tr>';
                                        if($ispdetailArr['account_number'] != ''){ 
                            $message .=  '<tr>
                                            <td colspan="2" rowspan="5" style="border-right:1px solid #000000; padding: 5px">
                                               <table width="406" border="0" cellspacing="0" cellpadding="0">
                                                  <tbody>
                                                     <tr>
                                                        <td width="83">
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Bank Name :</span></p>
                                                        </td>
                                                        <td width="323">
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">'.$ispdetailArr['bank_name'].'</p>
                                                        </td>
                                                     </tr>
                                                     <tr>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Branch Add :</span></p>
                                                        </td>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">'.$ispdetailArr['branch_address'].'</p>
                                                        </td>
                                                     </tr>
                                                     <tr>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Account No :</span></p>
                                                        </td>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">'.$ispdetailArr['account_number'].'</p>
                                                        </td>
                                                     </tr>
                                                     <tr>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Acc. Name :</span></p>
                                                        </td>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">'.$ispdetailArr['account_holder_name'].'</p>
                                                        </td>
                                                     </tr>
                                                     <tr>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">IFSC CODE :</span></p>
                                                        </td>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">'.$ispdetailArr['ifsc_code'].'</p>
                                                        </td>
                                                     </tr>
                                                  </tbody>
                                               </table>
                                            </td>
                                            <td style="border-bottom:1px solid #000000; border-right:none;">&nbsp;</td>
                                            <td style="border-bottom:1px solid #000000; border-right:none;">&nbsp;</td>
                                            <td style="border-bottom:1px solid #000000; border-right:1px solid #000000;">&nbsp;</td>
                                            <td style="border-bottom:1px solid #000000; border-right:none;">&nbsp;</td>
                                         </tr>';
                                        }else{
                            $message .=  '<tr>
                                            <td colspan="2" rowspan="5" style="border-right:1px solid #000000; padding: 5px">
                                               <table width="406" border="0" cellspacing="0" cellpadding="0">
                                                  <tbody>
                                                     <tr>
                                                        <td width="83">
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">&nbsp;</span></p>
                                                        </td>
                                                        <td width="323">
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">&nbsp;</p>
                                                        </td>
                                                     </tr>
                                                     <tr>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">&nbsp;</span></p>
                                                        </td>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">&nbsp;</p>
                                                        </td>
                                                     </tr>
                                                     <tr>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">&nbsp;</span></p>
                                                        </td>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">&nbsp;</p>
                                                        </td>
                                                     </tr>
                                                     <tr>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">&nbsp;</span></p>
                                                        </td>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">&nbsp;</p>
                                                        </td>
                                                     </tr>
                                                     <tr>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">&nbsp;</span></p>
                                                        </td>
                                                        <td>
                                                           <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">&nbsp;</p>
                                                        </td>
                                                     </tr>
                                                  </tbody>
                                               </table>
                                            </td>
                                            <td style="border-bottom:1px solid #000000; border-right:none;">&nbsp;</td>
                                            <td style="border-bottom:1px solid #000000; border-right:none;">&nbsp;</td>
                                            <td style="border-bottom:1px solid #000000; border-right:1px solid #000000;">&nbsp;</td>
                                            <td style="border-bottom:1px solid #000000; border-right:none;">&nbsp;</td>
                                         </tr>';
                                        }
                            $message .= $tax_msg . '
                                      </tbody>
                                   </table>
                                   <table width="770" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px; margin:15px 0px 0px 0px; ">
                                      <tbody>
                                         <tr>
                                            <td width="331" scope="col">&nbsp;
                                            </td>
                                            <td width="437" scope="col" style="background-color: #fff; border-top-left-radius:15px ">
                                            </td>
                                         </tr>
                                         <tr>
                                            <td>&nbsp;</td>
                                            <td rowspan="4" style="background-color: #fff; text-align: right">';
                                                if($isp_signature != '0'){
                                                    $message .=  '<img src="'.$isp_signature.'" alt="isplogo" />';
                                                }
                                   $message .= '
                                            </td>
                                         </tr>
                                         <tr>
                                            <td style="padding-left:10px">
                                               <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">&nbsp;</span></p>
                                            </td>
                                         </tr>
                                         <tr>
                                            <td style="padding-left:10px">
                                               <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">&nbsp;</span></p>
                                            </td>
                                         </tr>
                                         <tr>
                                            <td>&nbsp;</td>
                                         </tr>
                                         <tr>
                                            <td>&nbsp;</td>
                                            <td style="padding-right:10px; background-color: #fff">
                                               <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">Authorised Signatory</p>
                                            </td>
                                         </tr>
                                         <tr>
                                            <td style="border-bottom-right-radius:15px">&nbsp;</td>
                                            <td style="background-color: #fff">
                                               <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:10px;">'.$company_name.'</p>
                                            </td>
                                         </tr>
                                      </tbody>
                                   </table>
                                </td>
                             </tr>
                          </tbody>
                       </table>
                       <table width="790" border="0" cellspacing="0" cellpadding="0" style="padding:10px 0px; margin:10px 0px; ">
                          <tbody>
                             <tr>
                                <td style="border-top-left-radius:4px; border-top-right-radius:4px; background-color: #c1c1c1; padding:10px">
                                   <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:14px; font-weight:600;  margin:0px; padding:0px;">Terms and conditions:</p>
                                </td>
                             </tr>
                             <tr>
                                <td style="padding:2px 10px">
                                   <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">1. Please make your payments before the due date to enjoy uninterrupted service of your internet connection. </p>
                                </td>
                             </tr>
                             <tr>
                                <td style="padding:2px 10px">
                                   <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">2. If the service is discontinued for non-payment of dues, then a reconnection charge may be applicable.</p>
                                </td>
                             </tr>
                             <tr>
                                <td style="padding:2px 10px">
                                   <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">3. Late payment charges may be applicable after the payment due date.</p>
                                </td>
                             </tr>
                             <tr>
                                <td style="padding:2px 10px">
                                   <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">4. All invoice disputes should be highlighted within 7 days of invoice generation.</p>
                                </td>
                             </tr>
                             <tr>
                                <td style="padding:2px 10px">
                                   <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">5. GST charges as applicable and notified by the Govt. of India would be applicable.</p>
                                </td>
                             </tr>
                             <tr>
                                <td style="padding:2px 10px">
                                   <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">6. All disputes are subject to the courts of '.$state.'</p>
                                </td>
                             </tr>
                             <tr>
                                <td style="padding:2px 10px">
                                   <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">7. For any queries on the invoice please call '.$helpline.' or write at '.$support_email.'.</p>
                                </td>
                             </tr>
                          </tbody>
                       </table>
                    </div>
                 </div>';
                 
                 //echo $message; die;
                $dpdf = new dpdf();
                $dpdf->load_html($message);
                $dpdf->render();
                $output = $billid. '.pdf';
                //$finalpdf = $this->dpdf->stream($output);
                $finalpdf = $dpdf->output();
                file_put_contents('assets/media/pdfexcel_dwnld/'.$output, $finalpdf);
            }
        
        
        }
        
        $zip = new ZipArchive();
        $zipname = time().".zip"; // Zip name
        $zip->open($zipname,  ZipArchive::CREATE);
        foreach($billidsarr as $billid){
            $path = 'assets/media/pdfexcel_dwnld/'.$billid.'.pdf';
            if(file_exists($path)){
                //$zip->addFromString(basename($path),  file_get_contents($path));
		$zip->addFile($path);
                @unlink($path);
            }else{
                echo"file does not exist";
            }
        }
        $zip->close();
        header ("Content-Type: application/octet-stream"); 
        header('Content-disposition: attachment; filename="'.$zipname.'"');
        header('Content-Length: ' . filesize($zipname));
        ob_clean();
        readfile($zipname);
    }

    public function excelexport_userbilling(){
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 11
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            )
        );

        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
       
        $objPHPExcel->getActiveSheet()->setTitle("Bill Listing");
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A1','UID');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B1','Username');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C1','First Name');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D1','Last Name');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E1','Email');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('F1','Mobile');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('G1','Alternate Mobile');
        $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('H1','GSTIN Number');
        $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);           
        $objPHPExcel->getActiveSheet()->setCellValue('I1','Active Plan');
        $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('K1','Bill Number');
        $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('L1','Bill Type');
        $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('M1','Amount');
        $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('N1','Discount');
        $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('O1','CGST');
        $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('P1','SGST');
        $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('Q1','IGST');
        $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('R1','Net Amount');
        $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('S1','Added On');
        $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('T1','State Code');
        $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('U1','SAC/HSN');
        $objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('V1','Place of supply');
        $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($styleArray);
        
        $j = 2;
	
	$filteruids = $this->input->post('billids_toexport');
        $billidsarr = explode(',' , $filteruids);
        
        //$billidsarr = array('5764', '6038', '5956', '6507');
        foreach($billidsarr as $billid){
            $billquery = $this->db->query('SELECT * FROM sht_subscriber_billing WHERE status="1" AND is_deleted="0" AND id="'.$billid.'" ORDER BY id DESC LIMIT 1');
            $billnum_rows = $billquery->num_rows();
            $billingdata = $billquery->row();
            $uuid = $billingdata->subscriber_uuid;
        
            $apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
            $billtype = $billingdata->bill_type;
            $user_rowdata = $this->getcustomer_data($uuid);
            $state = $this->getstatename($user_rowdata->billing_state);
            $city = $this->getcityname($user_rowdata->billing_state,$user_rowdata->billing_city);
            $zone = $this->getzonename($user_rowdata->billing_zone);
            $mobileno = $user_rowdata->billing_mobileno;
            $alt_mobileno = $user_rowdata->alt_mobile;
            $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
            $gstin_number = $user_rowdata->gstin_number;
            $email = $user_rowdata->email;
            $isp_uid = $user_rowdata->isp_uid;
            $taxtype = $user_rowdata->taxtype;
            $username = $user_rowdata->username;
            $firstname = $user_rowdata->firstname;
            $lastname = $user_rowdata->lastname;
            
            $tax = $this->current_taxapplicable($isp_uid);
            if($taxtype == '1'){
                $apply_cgst_sgst_tax = 1;
            }else{
                $apply_igst_tax = 1;
            }
    
            $next_bill_date = date('d-m-Y', strtotime($user_rowdata->next_bill_date));
            $billingdata = $this->user_billing_listing($uuid, $billid);
            $bill_number = $billingdata->bill_number;
            $payment_mode = $billingdata->payment_mode;
            $amount = $billingdata->actual_amount;
            $discount = $billingdata->discount;
            if($discount != 0){
                $discount_amt = ($amount * $billingdata->discount)/100;
            }else{
                $discount_amt = 0;
            }
            $total_amount = $billingdata->total_amount;
            $service_tax = $billingdata->service_tax;
            $swachh_bharat_tax = $billingdata->swachh_bharat_tax;
            $krishi_kalyan_tax = $billingdata->krishi_kalyan_tax;
            $cgst_tax = $billingdata->cgst_tax;
            $sgst_tax = $billingdata->sgst_tax;
            $igst_tax = $billingdata->igst_tax;
            $amount_inwords = $this->getIndianCurrency($total_amount);
            
            $bill_comments = $billingdata->bill_comments;
            if($bill_comments != ''){
                $bill_comments = 'Other Details: '.$bill_comments;
            }
            $bill_remarks = $billingdata->bill_remarks;
            if($bill_remarks != ''){
                $bill_remarks = 'Remarks: '.$bill_remarks;
            }
            $currsymbol = $this->country_currency($isp_uid);
            
            if($apply_cgst_sgst_tax == 1){
                $tax = (100 + $tax);
                $actual_billamt = round(($total_amount * 100) /  $tax);
                if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
                    $taxamt = ($total_amount - $actual_billamt);
                    $cgst_tax = round($taxamt/2);
                    $sgst_tax = round($taxamt/2);
                }
            }else{
                $tax = (100 + $tax);
                $actual_billamt = round(($total_amount * 100) /  $tax);
                if($igst_tax == '0.00'){
                    $igst_tax = ($total_amount - $actual_billamt);
                }
            }
            
            $bill_date = date('d-m-Y', strtotime($billingdata->bill_added_on));
            $plandetails = $this->getplan_details($billingdata->plan_id, $uuid);
            $plan_duration = $plandetails['plan_duration'];
            $plan_tilldays = $plandetails['plan_tilldays'];
            $planname = $plandetails['planname'];
            
            $lastplan_activatedate = $this->lastplan_activatedate($billid);
            $bill_starts_from = $billingdata->bill_starts_from;
            if($bill_starts_from == '0000-00-00'){
                if($lastplan_activatedate == ''){
                    $plan_activation_date = date('d-m-Y', strtotime($user_rowdata->plan_activated_date));
                }else{
                    $plan_activation_date = date('d-m-Y', strtotime($lastplan_activatedate));
                }
            }else{
                $plan_activation_date = date('d-m-Y', strtotime($billingdata->bill_starts_from));
            }
            $user_plan_type = $billingdata->user_plan_type;
            if($user_plan_type == 'prepaid'){
                $bill_tilldate = date('d-m-Y', strtotime($plan_activation_date." +$plan_tilldays day"));
            }else{
                $bill_tilldate = $bill_date;
            }
            
            $ispdetailArr = $this->isp_details($isp_uid);
            $company_name = $ispdetailArr['company_name'];
            $address = $ispdetailArr['address1'];
            $helpline = $ispdetailArr['help_number1'];
            $support_email = $ispdetailArr['support_email'];
            $isp_name = ucwords($ispdetailArr['isp_name']);
            $ispgst_number = $ispdetailArr['gst_no'];
            $isp_service_tax_no = $ispdetailArr['service_tax_no'];
            $ispcin_number = $ispdetailArr['cin_number'];
            $isp_website = $ispdetailArr['website'];
            $isp_state = $this->getstatename($ispdetailArr['state']);
            $isplogo = $this->get_isplogo($isp_uid);
            $isp_signature = $this->get_ispsignaturelogo($isp_uid);
            $isppancard = $ispdetailArr['pancard'];
            $invoice_color = $ispdetailArr['invoice_color'];
            $invoice_banner1 = $ispdetailArr['invoice_banner1'];
            $invoice_banner2 = $ispdetailArr['invoice_banner2'];
            $invoice_statecode = $ispdetailArr['invoice_statecode'];
            
            $isptaxdet = '';
            $billmonth = date('m', strtotime($bill_date));
            $billyear = date('Y', strtotime($bill_date));
            if(($billmonth < 7) && ($billyear == '2017')){
                $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">Service Tax : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isp_service_tax_no.'</span>';
            }else{
                $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">GSTIN : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispgst_number.'</span>';
            }
            
            
            if($billtype == 'topup'){
                $plandetails = $this->gettopup_details($billingdata->plan_id);
                $topuptype = $plandetails['topuptype'];
                if($topuptype == '1'){
                    $topuptype = 'Data Topup';
                }elseif($topuptype == '2'){
                    $topuptype = 'Data Unaccountancy Topup';
                }elseif($topuptype == '3'){
                    $topuptype = 'Speed Topup';
                }
                $topupname = $plandetails['topupname'];
                $topupprice = $plandetails['topupprice'];
                
                $topupdetails = '';
                if($topuptype != 'Data Topup'){
                    $appliedtopup_details = $this->appliedtopup_details($billingdata->plan_id);
                    $topup_startdate = date('d-m-Y', strtotime($appliedtopup_details['topup_startdate']));
                    $topup_enddate = date('d-m-Y', strtotime($appliedtopup_details['topup_enddate']));
                    $applicable_days = $appliedtopup_details['applicable_days'];
                    $total_topupamount = ($topupprice * $applicable_days);
                    $topupdetails = $topup_startdate.' to '.$topup_enddate.' ('.$applicable_days.' days)';
                }
            }
            
            if(($billtype == 'installation') || ($billtype == 'security')){
                $hsn_sac = '9987';
            }else{
                $hsn_sac = '9984';
            }
            
            
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $uuid);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $username);
            $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $firstname);
            $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $lastname);
            $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$j, $email);
            $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$j, $mobileno);
            $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$j, $alt_mobileno);
            $objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$j, $gstin_number);
            $objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($styleArray1);           
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$j, $planname);
            $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($styleArray1);
    
            
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$j, $bill_number);
            $objPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$j, $billtype);
            $objPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$j, $actual_billamt);
            $objPHPExcel->getActiveSheet()->getStyle('M'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('N'.$j, $discount_amt);
            $objPHPExcel->getActiveSheet()->getStyle('N'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('O'.$j, $cgst_tax);
            $objPHPExcel->getActiveSheet()->getStyle('O'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('P'.$j, $sgst_tax);
            $objPHPExcel->getActiveSheet()->getStyle('P'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('Q'.$j, $igst_tax);
            $objPHPExcel->getActiveSheet()->getStyle('Q'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('R'.$j, $total_amount);
            $objPHPExcel->getActiveSheet()->getStyle('R'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('S'.$j, $bill_date);
            $objPHPExcel->getActiveSheet()->getStyle('S'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('T'.$j, $invoice_statecode);
            $objPHPExcel->getActiveSheet()->getStyle('T'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('U'.$j, $hsn_sac);
            $objPHPExcel->getActiveSheet()->getStyle('U'.$j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('V'.$j, $state);
            $objPHPExcel->getActiveSheet()->getStyle('V'.$j)->applyFromArray($styleArray1);
            
            $j++;
        }
        
        $session_data = $this->session->userdata('isp_session');
	$isp_uid = $session_data['isp_uid'];
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('assets/media/pdfexcel_dwnld/'.$isp_uid.'_billlisting.xlsx');
	
	echo "<script type='text/javascript'>window.open('".base_url()."assets/media/pdfexcel_dwnld/".$isp_uid."_billlisting.xlsx','' ); </script>";
    }


}