<?php


class Ppoe_model extends CI_Model {
 public function __construct()
         {
            parent::__construct();
            	$this->load->library('routerlib');
         }
         
         public function add_connection()
         {
	  $data=array();
             $router_id=$_POST['ip'];
             $router_user=$_POST['username'];
             $router_password=$_POST['password'];
            $postdata=$this->input->post();
            $conn= RouterOS::connect("$router_id", "$router_user", "$router_password");
            if($conn)
            {
                $tabledata=array("ip"=>$postdata['ip'],"username"=>$postdata['username'],
                                 "password"=>$postdata['password'],"added_on"=>date("Y-m-d H:i:s"));
                $this->db->insert('conadetails',$tabledata);
                   $data['resultcode']=1;
		   $data['connid']=$this->db->insert_id();
            }
            else
            {
                $data['resultcode']=0;
		  
                  
            }
	    return $data;
         }
	 
	 
	 public function get_etherlist()
	 {
	  $data=array();
	   $router_id=$_POST['ip'];
             $router_user=$_POST['username'];
             $router_password=$_POST['password'];
            $postdata=$this->input->post();
	   
            $conn= RouterOS::connect("$router_id", "$router_user", "$router_password") or die("please check ur credentials");
            if($conn)
            {
	     
	     $already=array();
	  $ipaddress = $conn->getall("/ip/address");
	//  echo "<pre>"; print_R($ipaddress);die;
	  foreach($ipaddress as $valu)
	  {
	    $already[]= trim($valu['interface']);
	  }
	//  echo "<pre>"; print_R($already);
	     
               $interface = $conn->getall("/interface");
	      // echo "<pre>"; print_R($interface);//die;
	       $gen="";
	       $gen.=' <div class="mui-row"><div class="form-group"><div class="col-md-12">';
	       foreach($interface as $val)
	       {
		if($val['type']=="ether" || $val['type']=="wlan")
		{
			
			if (in_array($val['name'], $already))
			  {
			  continue;
			  }
			else
			  {
					  
					 $gen.='<div class="radio radio-primary"><label> <input type="radio"  name="interfaceether"  value="'.$val['name'].'" checked><span> '.$val['name'].' </label></div>' ;
			  }
		}
	       }
	       $gen.='  </div></div></div>';
	        $data['resultcode']=1;
		$data['interface']=$interface;
		$data['html']=$gen;
	       
            }
            else
            {
               $data['resultcode']=0;  
                  
            }
	    return $data;
	 }
	 
	 public function checkvlanid()
	 {
	  $data=array();
	   $router_id=$_POST['ip'];
             $router_user=$_POST['username'];
             $router_password=$_POST['password'];
	     $ether= $_POST['ether'];
	     $vlanid= $_POST['vlanid'];
            $postdata=$this->input->post();
            $conn= RouterOS::connect("$router_id", "$router_user", "$router_password") or die("please check ur credentials");
            if($conn)
            {
               $interface = $conn->getall("/interface/vlan");
	     //  echo "<pre>"; print_R($interface);
	       $valid=1;
	       foreach($interface as $val)
	       {
		if($ether==$val['interface'] && $vlanid==$val['vlan-id'])
		{
		 //echo "sssss";
		 $valid=0;
		 break;
		}
	       }
	       if($valid==1)
	       {
		 $data['resultcode']=1;  
	       }
	       else{
		 $data['resultcode']=0;  
	       }
	       
	       
            }
            else
            {
               $data['resultcode']=2;  
                  
            }
	    return $data;
	 }
	 
	  public function checkvlanname()
	 {
	  $data=array();
	   $router_id=$_POST['ip'];
             $router_user=$_POST['username'];
             $router_password=$_POST['password'];
	    
	     $vlanname= $_POST['vlanname'];
            $postdata=$this->input->post();
            $conn= RouterOS::connect("$router_id", "$router_user", "$router_password") or die("please check ur credentials");
            if($conn)
            {
               $interface = $conn->getall("/interface/vlan");
	       $valid=1;
	       foreach($interface as $val)
	       {
		if($vlanname==$val['name'])
		{
		 $valid=0;
		 break;
		}
	       }
	       if($valid==1)
	       {
		 $data['resultcode']=1;  
	       }
	       else{
		 $data['resultcode']=0;  
	       }
	       
	       
            }
            else
            {
               $data['resultcode']=2;  
                  
            }
	    return $data;
	 }
	 
	 public function check_nassetup()
	 {
	  $postdata=$this->input->post();
	   $sessiondata = $this->session->userdata('isp_session');
	     $isp_uid = $sessiondata['isp_uid'];
	     $query=$this->db->query("select id from nas where isp_uid='".$isp_uid."' and nasname='".$postdata['ip']."'");
	   if($query->num_rows()>0)
	   {
	    $data['resultcode']=1;  
	   }
	   else{
	   $data['resultcode']=0;  
	   }
	    return $data;
	 }
	 
	 public function setup_vlan()
	 {
	   $data=array();
	   $router_id=$_POST['ip'];
            $router_user=$_POST['username'];
            $router_password=$_POST['password'];
	    $vlanname= $_POST['vlanname'];
	    $vlanid=$_POST['vlanid'];
	    $intrface=$_POST['interface'];
            $postdata=$this->input->post();
            $conn= RouterOS::connect("$router_id", "$router_user", "$router_password") or die("please check ur credentials");
	   $interface = $conn->add("/interface/vlan",array("vlan-id"=>$vlanid,"name"=>$vlanname,"interface"=>$intrface));
	   if($interface)
	   {
	    $tabledata=array("router_ip"=>$router_id,"username"=>$router_user,"interface_name"=>$intrface,
			     "vlan"=>$vlanname,"vlan_id"=>$vlanid,"created_on"=>date("Y-m-d H:i:s"));
	    $this->db->insert('config_detail',$tabledata);
	    $data['resultcode']=1;
	   }
	   else
	   {
	    $data['resultcode']=0;
	   }
	   
	   return $data;
	 }
	 
	 
	  public function ppoe_listdata()
	 {
	  $data=array();
	   $router_id=$_POST['ip'];
             $router_user=$_POST['username'];
             $router_password=$_POST['password'];
            $postdata=$this->input->post();
	   
            $conn= RouterOS::connect("$router_id", "$router_user", "$router_password") or die("please check ur credentials");
            if($conn)
            {
               $interface = $conn->getall("/ip/address");
	      // echo "<pre>"; print_R($interface);die;
	       $gen="";
	        $gen.=' <div class="mui-row"><div class="form-group"><div class="col-md-12">';
	       foreach($interface as $val)
	       {
		
		$gen.='<div class="radio radio-primary"><label> <input type="radio"  name="ppoeip"  value="'.$val['address'].'" checked><span> '.$val['address'].' </label></div>' ;
		
	       }
	        $gen.='  </div></div></div>';
	        $data['resultcode']=1;
		$data['iplist']=$interface;
		$data['html']=$gen;
	       
            }
            else
            {
               $data['resultcode']=0;  
                  
            }
	    return $data;
	 }
	 
	 
	 public function execute_cmnd()
	 {
	   
	   $postdata=$this->input->post();
	   $router_id=$_POST['ip'];
             $router_user=$_POST['username'];
             $router_password=$_POST['password'];
	     $vlanid=$postdata['vlanid'];
	     $vlanname=$postdata['vlanname'];
	     $intrface=$postdata['interf'];
	    $iprange=$postdata['iprange'];
	    $ppoeip=$postdata['publicppoe'];
            $postdata=$this->input->post();
	   
            $conn= RouterOS::connect("$router_id", "$router_user", "$router_password") or die("please check ur credentials");
	    if($conn)
	    {
	     
	     $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	      if($postdata['hdnseltype']=="vlan"){
		  $interface = $conn->add("/interface/vlan",array("vlan-id"=>$vlanid,"name"=>$vlanname,"interface"=>$intrface));
		  if(!$interface)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Already have device with such name";
		  return $data;
		  }
		  //ip pool add
		  $ip_pool_add = $conn->add("/ip/pool", array("name"=>"decibelprivate", "ranges"=>$iprange));
		  if(!$ip_pool_add)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="IP pool Range already present";
		  return $data;
		  }
		  
		  //
		  $ip_firewall_nat_add = $conn->add("/ip/firewall/nat", array("disabled"=>"no", "chain"=>"srcnat", "src-address-list"=>"decibelprivate", "action"=>"masquerade"));
		   if(!$ip_firewall_nat_add)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Unable to Add Firewall";
		  return $data;
		  }
		  
		  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("disabled"=>"no", "list"=>"decibelprivate", "address"=>$iprange));
		   if(!$ip_firewall_addresslist_add)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Firewall Address List already have such entry";
		  return $data;
		  }
		  
		    $ip_firewall_addresslist_add_range = $conn->add("/ip/firewall/address-list", array("range"=>$iprange,"name"=>"ALLOW", "disabled"=>"no"));
		   if(!$ip_firewall_addresslist_add_range)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Firewall Address List Range not added";
		  return $data;
		  }
		  
		   $ip_firewall_addcomment = $conn->add("/ip/firewall", array("comment"=>"ALLOW","chain"=>"forward", "disabled"=>"no","src-address-list"=>"ALLOW","action"=>"accept","disabled"=>"no"));
		   if(!$ip_firewall_addcomment)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Firewall Comment not addded";
		  return $data;
		  }
		  
		  $ip_firewall_addchain = $conn->add("/ip/firewall/filter", array("chain"=>"forward","disabled"=>"no", "comment"=>"drop","action"=>"drop"));
		  if(!$ip_firewall_addchain)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Firewall Chain addded";
		  return $data;
		  }
		  
		  $ppp_profile_add = $conn->add("/ppp/profile", array("name"=>"decibelprofile1", "use-mpls"=>"default", "use-compression"=>"default", "use-encryption"=>"default", "only-one"=>"default", "change-tcp-mss"=>"yes", "use-upnp"=>"default", "local-address"=>$ppoeip, "remote-address"=>"decibelprivate", "dns-server"=>"8.8.8.8"));
		  if(!$ppp_profile_add)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="PPoe Profile Already Exist with This Name";
		  return $data;
		  }
		  else{
		  $data['resultcode']=1;
		  $data['resultmsg']="PPOE Succesfull";
		  $this->roterconfig_data($postdata);
		  return $data;
		  }
	      }
	      else{
		  //ip pool add
		  $ip_pool_add = $conn->add("/ip/pool", array("name"=>"decibelprivate", "ranges"=>$iprange));
		  if(!$ip_pool_add)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="IP pool Range already present";
		  return $data;
		  }
		  
		  //
		  $ip_firewall_nat_add = $conn->add("/ip/firewall/nat", array("disabled"=>"no", "chain"=>"srcnat", "src-address-list"=>"decibelprivate", "action"=>"masquerade"));
		   if(!$ip_firewall_nat_add)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Unable to Add Firewall";
		  return $data;
		  }
		  
		  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("disabled"=>"no", "list"=>"decibelprivate", "address"=>$iprange));
		   if(!$ip_firewall_addresslist_add)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Firewall Address List already have such entry";
		  return $data;
		  }
		  
		 /*  $ip_firewall_addresslist_add_range = $conn->add("/ip/firewall/address-list", array("range"=>$iprange,"name"=>"ALLOW", "disabled"=>"no"));
		   if(!$ip_firewall_addresslist_add_range)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Firewall Address List Range not added";
		  return $data;
		  }
		  
		   $ip_firewall_addcomment = $conn->add("/ip/firewall", array("comment"=>"ALLOW","chain"=>"forward", "disabled"=>"no","src-address-list"=>"ALLOW","action"=>"accept","disabled"=>"no"));
		   if(!$ip_firewall_addcomment)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Firewall Comment not addded";
		  return $data;
		  }
		  
		   $ip_firewall_addchain = $conn->add("/ip/firewall/filter", array("chain"=>"forward","disabled"=>"no", "comment"=>"drop","action"=>"drop"));
		  if(!$ip_firewall_addchain)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Firewall Chain addded";
		  return $data;
		  }*/
		  
		  //echo "<pre>"; print_R(array("name"=>"decibelprofile1", "use-mpls"=>"default", "use-compression"=>"default", "use-encryption"=>"default", "only-one"=>"default", "change-tcp-mss"=>"yes", "use-upnp"=>"default", "local-address"=>$ppoeip, "remote-address"=>"decibelprivate", "dns-server"=>"8.8.8.8"));//die;
		  $ppp_profile_add = $conn->add("/ppp/profile", array("name"=>"decibelprofile1", "use-mpls"=>"default", "use-compression"=>"default", "use-encryption"=>"default", "only-one"=>"default", "change-tcp-mss"=>"yes", "local-address"=>$ppoeip, "remote-address"=>"decibelprivate", "dns-server"=>"8.8.8.8"));
		  if(!$ppp_profile_add)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="PPoe Profile Already Exist with This Name";
		  return $data;
		  }
		  else{
		  $data['resultcode']=1;
		  $data['resultmsg']="PPOE Succesfull";
		  $this->roterconfig_data($postdata);
		  return $data;
		  }
	      }
	    }
	    else{
	    $data['resultcode']=0;
	    $data['resultmsg']="Connection Unsuccessfull";
	    return $data;
	    }
	 
	
	// echo "<pre>"; print_R($postdata); die;
	 }
	 
	 public function execute_cmnd_ppp()
	 {
	 
	   $postdata=$this->input->post();
	   $router_id=$_POST['ip'];
             $router_user=$_POST['username'];
             $router_password=$_POST['password'];
	     $vlanid=$postdata['vlanid'];
	     $vlanname=$postdata['vlanname'];
	     $intrface=$postdata['interf'];
	    $iprange=$postdata['iprange'];
	    $ppoeip=$postdata['publicppoe'];
	    $session_num=$postdata['session_num'];
	     $conn= RouterOS::connect("$router_id", "$router_user", "$router_password") or die("please check ur credentials");
	     if($conn)
	    {
	   
	     
	     $interface_wireless = $conn->add("/interface/pppoe-server/server",array("interface"=>$intrface, "service-name"=>"ppp", "default-profile"=>"decibelprofile1", "disabled"=>"no", "max-sessions"=>$session_num, "authentication"=>"pap,chap","max-mtu"=>"1480", "max-mru"=>"1480", "mrru"=>"1600"));
	      if(!$interface_wireless)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Unable to set Interface PPPoe server";
		  return $data;
		  }
		  
		  $ppp_aaa_set = $conn->set("/ppp/aaa",array("use-radius"=>"yes","accounting"=>"yes", "interim-update"=>"1m"));
		   if(!$ppp_aaa_set)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Unable to set PPP AAA Set";
		  return $data;
		  }
		  
		  $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"yes","port"=>"3799"));
		  if(!$radius_incoming_port)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Unable to set Radius Incoming Port";
		  return $data;
		  }
		  
		   $radius_address=RADIUSIP;
		   $secretkey='';
		   $secret=$this->db->query("select secret from nas where nasname='".$radius_address."'");
		   if($secret->num_rows()>0){
			   $secretarr=$secret->row_array();
			   $secretkey=$secretarr['secret'];
			}else{
			   $secretkey='testing123';
		   }
		   
  $radius_service_add = $conn->add("/radius",array("service"=>"ppp","disabled"=>"no","address"=>"$radius_address","secret"=>$secretkey,"timeout"=>"2000ms", "authentication-port"=>"1812", "accounting-port"=>"1813"));
  if(!$radius_service_add)
		  {
		   $data['resultcode']=0;
		  $data['resultmsg']="Unable to set Radius Service";
		  return $data;
		  }
		  else
		  {
		   $data['resultcode']=1;
		  $data['resultmsg']="Success";
		  return $data;
		  }
		  
		  
	    }
	      else{
	    $data['resultcode']=0;
	    $data['resultmsg']="Connection Unsuccessfull";
	    return $data;
	    }
	 
	    
	  
	 }
	 
	 
	 public function roterconfig_data($postdata)
	 {
	 //echo "<pre>"; print_R($postdata); die;
	 $tabledata=array("router_ip"=>$postdata['ip'],"type"=>$postdata['hdnseltype'],"username"=>$postdata['username'],
			  "interface_name"=>$postdata['interf'],"vlan"=>$postdata['vlanname'],"vlan_id"=>$postdata['vlanid'],
			  "publicppoe_ip"=>$postdata['publicppoe'],"ip_range"=>$postdata['iprange'],"conn_id"=>$postdata['connid'],"created_on"=>date("Y-m-d H:i:s"));
	 $this->db->insert('config_detail',$tabledata);
	 return true;
	 }
	
	
}