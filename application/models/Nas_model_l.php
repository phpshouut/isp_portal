<?php
class Nas_model extends CI_Model{
	
    public function __construct() {
        parent::__construct();
       
    }
    
    public function listing_nas()
    {
        //$condarr=array('status'=>1,"is_deleted"=>0);
        $query=$this->db->get_where(SHTNAS);
        return $query->result();
    }
    
    public function add_nas_data()
    {
        $postdata=$this->input->post();
      //  echo "<pre>"; print_R($postdata); die;
        $nasfqdn=(isset($postdata['fqdn']) && $postdata['fqdn']!='' )?$postdata['fqdn']:"";
        $nasip=(isset($postdata['ip1']) && $postdata['ip1']!=''  )?$postdata['ip1'].".".$postdata['ip2'].".".$postdata['ip3'].".".$postdata['ip4']:"";
          $tabledata=array('nasname'=>$nasip,"shortname"=>$postdata['nas_name'],'type'=>0,'ports'=>0,'secret'=>$postdata['nas_secret'],'nastype'=>$postdata['nastype'], 'nasstate'=>$postdata['state'],'nascity'=>$postdata['city'],'naszone'=>$postdata['zone'],'created_on'=>date("Y-m-d H:i:s"));
       // $tabledata=array('nasipformat'=>$postdata['nasipformat'],"nasfqdn"=>$nasfqdn,'nasipaddr'=>$nasip,'nasname'=>$postdata['nas_name'],'nastype'=>$postdata['nastype'],'nassecret'=>$postdata['nas_secret'], 'nasstate'=>$postdata['state'],'nascity'=>$postdata['city'],'naszone'=>$postdata['zone'],'created_on'=>date("Y-m-d H:i:s"));
        if(isset($postdata['nasid']) && $postdata['nasid']!='')
        {
         $this->db->update(SHTNAS, $tabledata, array('id' => $postdata['nasid']));
                        return  $postdata['nasid'];   
        }
 else {
     $this->db->insert(SHTNAS,$tabledata);
     return $this->db->insert_id();
 }
    
    }
    
    
   public function viewall_search_results(){
		$data = array();
		$gen = '';
		$total_search = 0;
		$search_user = $this->input->post('search_user');
		$where = " ((nasname LIKE '%".$search_user."%') OR (shortname LIKE '%".$search_user."%') ) ";
		$user_state = $this->input->post('state');
		$user_city = $this->input->post('city');
		$user_zone = $this->input->post('zone');
		
                
                  $condarr = array();
        if ($user_state != 'all' && $user_city != 'all' && $user_zone != 'all') {
            $condarr[] = "(nasstate='" . $user_state . "' and nascity='" . $user_city . "' and naszone='" . $user_zone . "')";
        } else if ($user_state != 'all' && $user_city == 'all') {
            $condarr[] = "(nasstate='" . $user_state . "')";
        } else if ($user_state != 'all' && $user_city != 'all' && $user_zone == 'all') {
            $condarr[] = "(nasstate='" . $user_state . "' and nascity='" . $user_city . "')";
        }
        $cond1 = implode('OR', $condarr);



        if ($cond1 != "") {
            $where.= "and ($cond1)";
        }
                
                if(isset($search_user) && $search_user!='')
                {
                        $stateidarr=array();$cityidarr=array();$zoneidarr=array();
                    $condarr=array();
                    $stquery=$this->db->query("select id from sht_states where state like '%".$search_user."%'");
                    if($stquery->num_rows()>0)
                    {
                       foreach($stquery->result() as $val)
                       {
                         $stateidarr[]= $val->id;
                       }
                       $sid = '"' . implode('", "', $stateidarr) . '"';
                       $condarr[]="(nasstate in ({$sid}))";
                    }
                    
                     $ctquery=$this->db->query("select city_id from sht_cities where city_name like '%".$search_user."%'");
                    if($ctquery->num_rows()>0)
                    {
                       foreach($ctquery->result() as $val)
                       {
                         $cityidarr[]= $val->city_id;
                       }
                       $cid = '"' . implode('", "', $cityidarr) . '"';
                       $condarr[]="(nascity in ({$cid}))";
                    }
                    
                     $znquery=$this->db->query("select id from sht_zones where zone_name like '%".$search_user."%'");
                    if($znquery->num_rows()>0)
                    {
                       foreach($znquery->result() as $val)
                       {
                         $zoneidarr[]= $val->id;
                       }
                       $zid = '"' . implode('", "', $zoneidarr) . '"';
                       $condarr[]="(naszone in ({$zid}))";
                    }
                    
                    $cond=implode("OR",$condarr);
                    if($cond!='')
                    {
                     $cond="OR ({$cond})";   
                    }
                    $where .=$cond;
                    
                }
		
		$this->db->select('*');
		$this->db->from(SHTNAS);
		$this->db->where($where);
		$query = $this->db->get();
               // echo $this->db->last_query(); die;
		$total_search = $query->num_rows();
		if($total_search > 0){
			$i = 1;
			foreach($query->result() as $sobj){
				$state = $this->user_model->getstatename($sobj->nasstate);
				$city = $this->user_model->getcityname($sobj->nasstate, $sobj->nascity);
				$zone = $this->user_model->getzonename($sobj->naszone);
				//$status = ($sobj->status == 1)?'active':'inactive';
                                $nastype=($sobj->nastype==1)?"Microtik":"Others";
                                 
                                  $nsimg='<img src="'.base_url().'assets/images/loader.svg" width="15%">';
                                                                                             
                                                                                              
				
				$gen .= '
				<tr>
					<td>'.$i.'.</td>
					<td>'.$sobj->shortname.'</td>
					<td>'.$nastype.'</td>
					<td>'.$sobj->nasname.'</td>
					<td>'.$state.'</td>
					<td>'.$city.'</td>
					<td>'.$zone.'</td>
                                            <td class="nasimg">'.$nsimg.'</td>
                                                <input type="hidden" class="nasnameip"  value="'.$valdata->nasname.'">
					
					<td><a href="javascript:void(0);" class="editnas" rel="'.$sobj->id.'">EDIT</a></td>
					
				</tr>
				';
				$i++;
			}
		}
		
		$data['total_results'] = $total_search;
		$data['search_results'] = $gen;
		
		echo json_encode($data);
	}
        
        
       // public function 
        
        public function delete_nas()
        {
            $postdata=$this->input->post();
            $tabledata=array("is_deleted"=>1);
           $this->db->update(SHTNAS, $tabledata, array('nasid' => $postdata['nasid']));
                        return  $postdata['nasid'];
        }
        
        public function edit_nas($is_ro)
        {
            $id=$this->input->post('nasid');
            $condarr=array('id'=>$id);
            $disable=($is_ro==1)?"disabled":"";
            $query=$this->db->get_where(SHTNAS,$condarr);
            $rowarr=$query->row_array();
            $gen="";
           // $checkst=($rowarr['nasipformat']=="Static")?"checked":"";
            //$chekdns=($rowarr['nasipformat']=="DDNS")?"checked":"";
           // $fqdn=$rowarr['nasfqdn'];
           // $fqdndis=($rowarr['nasipformat']=="Static")?"disabled":"";
           // $ipdis=($rowarr['nasipformat']=="DDNS")?"disabled":"";
            $gen.=' <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">
               <strong>Edit Nas</strong>
            </h4>
         </div>
         <div class="modal-body">
            <form action="" method="post" id="edit_nas" autocomplete="off" onsubmit="edit_nas(); return false;">
            <input type="hidden" name="nasid" value="'.$rowarr['id'].'">
               <div class="row">
                  
                    <input type="hidden" name="mobileno" value="9873834499">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <label>IP Address<sup>*</sup></label>
                           <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin-right">';
                                
            $iparr=explode(".",$rowarr['nasname']);
            
           $selother=($rowarr['nastype']=="0")?"selected":"";
           $selmicro=($rowarr['nastype']=="1")?"selected":"";
           $ip1=$iparr[0];
            $ip2=$iparr[1];
            $ip3=$iparr[2];
             $ip4=$iparr[3];
             $state=$this->state_list($rowarr['nasstate']);
             
           
                            $gen.=        '<div class="mui-textfield mui-textfield--float-label" style="padding-right:10px">
                                       <input type="number" '.$disable.' class="ip1 ipd" name="ip1" value="'.$ip1.'" required >
                                       <span class="ip_dot">.</span>
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin-right" style="padding-left:2px">
                                    <div class="mui-textfield mui-textfield--float-label" style="padding-right:10px">
                                       <input type="number" '.$disable.' class="ip2 ipd" name="ip2" value="'.$ip2.'" required>
                                       <span class="ip_dot">.</span>
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin">
                                    <div class="mui-textfield mui-textfield--float-label" style="padding-right:10px">
                                       <input type="number" '.$disable.' class="ip3 ipd" name="ip3" value="'.$ip3.'" required>
                                       <span class="ip_dot">.</span>
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin-left">
                                    <div class="mui-textfield mui-textfield--float-label">
                                       <input type="number" '.$disable.' class="ip4 ipd" name="ip4" value="'.$ip4.'" required>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label">
                              <input type="text" name="nas_name" '.$disable.' id="editnasname" value="'.$rowarr['shortname'].'" required>
                              <label>Nas Name</label>
                           </div>
                            <span class="errornas"></span>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="nastype" '.$disable.' required>
                               
                                 <option value="1" '.$selmicro.'>Microtik</option>
                              </select>
                              <label>NAS Type<sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label">
                              <input type="text" '.$disable.' name="nas_secret" value="'.$rowarr['secret'].'" required>
                              <label>Nas Secret</label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="">
                              <select name="state" '.$disable.' required class="statelist form-control">
                                 <option value="all">All States</option>
                               '.$state.'
                            </select>
                              <input type="hidden" class="statesel" name="statesel" value="'.$rowarr['nasstate'].'">
                              
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="">
                              <select name="city" '.$disable.' class="search_citylist form-control" required >
                                 ';
                         $gen.=  $this->getcitylist($rowarr['nasstate'],$rowarr['nascity']);
                          $gen.='    </select><input type="hidden" class="citysel" name="citysel" value="'.$rowarr['nascity'].'">
                            
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="">
                              <select class="zone_list form-control" name="zone" '.$disable.' required>
                               ';
                            $gen.=     $this->zone_list($rowarr['nascity'],$rowarr['naszone'],$rowarr['nasstate']);
                           $gen.=  ' </select>
                              
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="modal-footer">
                           <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                           <input type="submit" '.$disable.' class="mui-btn mui-btn--large mui-btn--accent" value="ADD NAS">
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <!-- /.modal-content -->
      </div>
   </div>';
                           $data['html']=$gen;
                   echo json_encode($data);        
          //  echo "<pre>"; print_R($rowarr); die;
            
        }
        
        
      
        
         public function state_list($stateid = '') {
         $sessiondata=$this->session->userdata('isp_session');
         $superadmin= $sessiondata['super_admin'];
          $dept_id= $sessiondata['dept_id'];
           $regiontype=$this->plan_model->dept_region_type();
       if($regiontype=="allindia" || $superadmin==1)
       {
        $stateQ = $this->db->get('sht_states');
        $num_rows = $stateQ->num_rows();
        if ($num_rows > 0) {
            $gen = '';
            foreach ($stateQ->result() as $stobj) {
                $sel = '';
                if ($stateid == $stobj->id) {
                    $sel = 'selected="selected"';
                }
                $gen .= '<option value="' . $stobj->id . '" ' . $sel . '>' . $stobj->state . '</option>';
            }
        }
       }
       else
       {
           $query=$this->db->query("select state_id from sht_dept_region where dept_id='".$dept_id."'");
           $statearr=array();
           foreach($query->result() as $val)
           {
               $statearr[]=$val->state_id;
           }
           if(in_array('all',$statearr))
           {
              $stateQ = $this->db->get('sht_states');
           }
           else
           {
                $sid = '"' . implode('", "', $statearr) . '"';
                $stateQ = $this->db->query("select * from sht_states where id IN ({$sid}) ");
           }
           $num_rows = $stateQ->num_rows();
        if ($num_rows > 0) {
            $gen = '';
            foreach ($stateQ->result() as $stobj) {
                $sel = '';
                if ($stateid == $stobj->id) {
                    $sel = 'selected="selected"';
                }
                $gen .= '<option value="' . $stobj->id . '" ' . $sel . '>' . $stobj->state . '</option>';
            }
        }
          
       }
       return $gen;
    }
	
	
	
	
         public function zone_list($cityid, $zoneid = '',$stateid='') {
          
            $sessiondata=$this->session->userdata('isp_session');
         $superadmin= $sessiondata['super_admin'];
          $dept_id= $sessiondata['dept_id'];
          $regiontype=$this->plan_model->dept_region_type();
         // $stateid=$this->input->post('stateid');
           $allzone='<option value="all">All Zone</option>';
          $gen = '';
           if($regiontype=="allindia" || $superadmin==1)
            {
                    $zoneQ = $this->db->get_where('sht_zones', array('city_id' => $cityid));
                     $num_rows = $zoneQ->num_rows();
                      $gen.=$allzone;
                     if ($num_rows > 0) {
                        
                         foreach ($zoneQ->result() as $znobj) {
                             $sel = '';
                             if ($zoneid == $znobj->id) {
                                 $sel = 'selected="selected"';
                             }
                             $gen .= '<option value="' . $znobj->id . '" ' . $sel . '>' . $znobj->zone_name . '</option>';
                         }
                     }
            }
            else
            {
                
                 $query=$this->db->query("select zone_id from sht_dept_region where dept_id='".$dept_id."' and state_id='".$stateid."'");
              $zonearr=array();
           foreach($query->result() as $val)
           {
               $zonearr[]=$val->zone_id;
           }
         //  echo "<pre>"; print_R($zonearr);
           if(in_array('all',$zonearr))
           {
            //   echo "sssss"; die;
               $allzone='<option value="all">All Zone</option>';
              $zoneQ = $this->db->get_where('sht_zones', array('city_id' => $cityid));
           }
           else
           {
             $allzone='<option value="">Select Zone</option>';
                $zid = '"' . implode('", "', $zonearr) . '"';
              $zoneQ = $this->db->query("select * from sht_zones where id IN ({$zid})");
           }
             $num_rows = $zoneQ->num_rows();
              $gen.=$allzone;
                if ($num_rows > 0) {
                   
                                foreach ($zoneQ->result() as $znobj) {
                                    $sel = '';
                                    if ($zoneid == $znobj->id) {
                                        $sel = 'selected="selected"';
                                    }
                                    $gen .= '<option value="' . $znobj->id . '" ' . $sel . '>' . $znobj->zone_name . '</option>';
                                }
                            }
                
            }
        return $gen;
    }
        
        
           public function getcitylist($stateid, $cityid = '') {
        $sessiondata=$this->session->userdata('isp_session');
         $superadmin= $sessiondata['super_admin'];
          $dept_id= $sessiondata['dept_id'];
          $regiontype=$this->plan_model->dept_region_type();
          $allcity='<option value="all">All Cities</option>';
          $gen = '';
           if($regiontype=="allindia" || $superadmin==1)
       {
               
           
        
        $cityQ = $this->db->get_where('sht_cities', array('city_state_id' => $stateid));
        $num_rows = $cityQ->num_rows();
        if ($num_rows > 0) {
            $gen.=$allcity;
            foreach ($cityQ->result() as $ctobj) {
                $sel = '';
                if ($cityid == $ctobj->city_id) {
                    $sel = 'selected="selected"';
                }
                $gen .= '<option value="' . $ctobj->city_id . '" ' . $sel . '>' . $ctobj->city_name . '</option>';
            }
        }
       }
       else
       {
            $query=$this->db->query("select city_id from sht_dept_region where dept_id='".$dept_id."' and state_id='".$stateid."'");
              $cityarr=array();
           foreach($query->result() as $val)
           {
               $cityarr[]=$val->city_id;
           }
         
           if(in_array('all',$cityarr))
           {
               
               $allcity='<option value="all">All Cities</option>';
              $cityQ = $this->db->get_where('sht_cities', array('city_state_id' => $stateid));
           }
           else
           {
              
               $allcity='<option value="">Select City</option>';
                $cid = '"' . implode('", "', $cityarr) . '"';
                $cityQ = $this->db->query("select * from sht_cities where city_id IN ({$cid})");
           }
         //  $this->db->last_query(); die;
             $num_rows = $cityQ->num_rows();
        if ($num_rows > 0) {
            $gen.=$allcity;
            foreach ($cityQ->result() as $ctobj) {
                $sel = '';
                if ($cityid == $ctobj->city_id) {
                    $sel = 'selected="selected"';
                }
                $gen .= '<option value="' . $ctobj->city_id . '" ' . $sel . '>' . $ctobj->city_name . '</option>';
            }
        }
           
           
           
       }
        return $gen;
    }
    
    
     public function ping($host)
    {
        exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host)), $res, $rval);
        return $rval;
    }
    
}


?>