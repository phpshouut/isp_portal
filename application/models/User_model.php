<?php
class User_model extends CI_Model{
	private $dbgraph;
	public function __construct(){
		parent::__construct();
		$this->dbgraph = $this->load->database('db3_decibelgraph', TRUE);
	}
	public function activate_emailer(){
		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'ssl://smtp.gmail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'talktous@shouut.com',
		    'smtp_pass' => 'G1@nt@sh0uut#',
		    'mailtype'  => 'html',
		    'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		
		$emailer = new CI_Email();
		$emailer->set_newline("\r\n");
		$emailer->from('talktous@shouut.com', 'SHOUUT');
		$emailer->to('vikas@shouut.com');
		$emailer->subject('Your DECIBEL Account Credentials');
		$emailer->message('Hi, Vikas');  
		$emailer->send();
		echo $emailer->print_debugger();
	}
	
	public function check_ispmodules(){
		$moduleArr = array();
		$query = $this->db->query("SELECT module FROM sht_isp_admin_modules WHERE isp_uid='".ISPID."' AND status='1' AND (module='1' OR module='7')");
		if($query->num_rows() > 0){
		    foreach($query->result() as $mobj){
			$moduleArr[] = $mobj->module;
		    }
		}
		if(count($moduleArr) > 0){
		    if((count($moduleArr) == 1) && (in_array('7' , $moduleArr))){
			redirect(base_url().'publicwifi');
		    }
		}
	}


/*------------------- PERMISSIONS FUNCTIONS --------------------------------------------------------------*/
	public function is_permission($slug, $permtype = "RO") {
		$userid = $this->session->userdata['isp_session']['userid'];
		$super_admin = $this->session->userdata['isp_session']['super_admin'];
		$query = $this->db->query("select id,super_admin,dept_id from sht_isp_users where id='" . $userid . "'");
		$rowarr = $query->row_array();
		if ($super_admin == 1) {
			return true;
		} else {
			$deptid = $rowarr['dept_id'];
			$query1 = $this->db->query("select id from sht_tab_menu where slug='" . $slug . "' ");
			//echo $this->db->last_query();
			$tabarr = $query1->row_array();
			$tabid = $tabarr['id'];
			//  echo $deptid."::".$tabid;
			$field = '';
			if ($permtype == "ADD") {
				$field = 'is_add';
			} else if ($permtype == "EDIT") {
				$field = 'is_edit';
			} else if ($permtype == "DELETE") {
				$field = 'is_delete';
			} else if ($permtype == "HIDE") {
				$field = 'is_hide';
			} else {
				$field = 'is_readonly';
			}
			$query2 = $this->db->query("select " . $field . " as perm from sht_isp_permission where isp_deptid='" . $deptid . "' and tabid='" . $tabid . "'");
			//echo $this->db->last_query();
			//  die;
			if ($query2->num_rows() > 0) {
				$permarr = $query2->row_array();
				if ($permarr['perm'] == 1) {
					if($permtype=="HIDE"){
						return false;
					}else{
						return true;
					}
				} else {
					if($permtype=="HIDE"){
						return true;
					}else{
						return false;
					}
					//return false;
				}
			} else {
				return false;
			}
		}
	}
	public function leftnav_permission() {
		$userid = $this->session->userdata['isp_session']['userid'];
		$query = $this->db->query("select id,super_admin,dept_id from sht_isp_users where id='" . $userid . "'");
		$tabarr = array();
		$rowarr = $query->row_array();
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$deptid = $rowarr['dept_id'];
		if ($sessiondata['super_admin'] == 1) {
		    $query = $this->db->query("select slug from sht_tab_menu where status='1'");
		    foreach ($query->result() as $val) {
			$tabarr[$val->slug] = 0;
		    }
		} else {
		    $query1 = $this->db->query("SELECT sip.is_hide,stm.slug FROM sht_isp_permission sip
	 INNER JOIN sht_tab_menu stm ON (stm.id=sip.tabid) WHERE isp_deptid='" . $deptid . "' ");
		    foreach ($query1->result() as $val) {
			$tabarr[$val->slug] = ($sessiondata['super_admin'] == 1) ? 0 : $val->is_hide;
		    }
		}
		//  echo $deptid."::".$tabid;
	      
		$tabarr['PUBLICWIFI']=$this->isp_publicwifi_perm();
		return $tabarr;
	}


	
	public function check_user_permission(){
		$userid = $this->session->userdata['isp_session']['userid'];
		$query = $this->db->query("SELECT tb1.menuid, tb2.menu_name FROM sht_isp_permission as tb1 INNER JOIN sht_menu as tb2 ON(tb1.menuid=tb2.id) WHERE isp_userid='".$userid."'");
		$num_rows = $query->num_rows();
		if($num_rows > 0){
			$icons = array('1' => 'fa-user', '2' => 'fa-wifi');
			echo '<ul>';
			foreach($query->result() as $dataobj){
				$menuid = $dataobj->menuid;
				$faicons = $icons["$menuid"];
				echo '
				<li>
				   <a href="#"><i class="fa '.$faicons.'" aria-hidden="true"></i> '.$dataobj->menu_name.'</a>
				</li>';
			}
			echo '</ul>';
		}
		
		
	}
	
	
	
	public function dept_region_type() {
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$region_type = '';
		if($superadmin==1){
		    $region_type = 'allindia';
		}else{
			$query = $this->db->get_where('sht_department', array('id' => $dept_id));
			if ($query->num_rows() > 0) {
				$rowarr = $query->row_array();
				$region_type = $rowarr['region_type'];
			}  
		}
	       
		return $region_type;
	}
	public function superadmin_regioninfo(){
		$data = array();
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$superadminQ = $this->db->get_where('sht_isp_admin_region', array('isp_uid' => $isp_uid, 'status' => '1'));
		if($superadminQ->num_rows() > 0){
			foreach($superadminQ->result() as $saregion){
				$data[] = $saregion->state;
			}
			
			$data = implode(',',$data);
		}
		return $data;
	}

	public function isp_publicwifi_perm(){
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$query=$this->db->query("select id from sht_isp_admin_modules where isp_uid='".$isp_uid."' and module='7' and status='1'");
		//echo $this->db->last_query();die;
		if($query->num_rows()>0){
			return 1;
		}else{
			return 0;
		}
	}

	public function getisp_countryid(){
		$query = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".ISPID."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			return $rowdata->country_id;
		}else{
			return '';
		}
		
	}
	public function countrydetails(){
		$data = array();
		$ispcountryQ = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".ISPID."'");
		if($ispcountryQ->num_rows() > 0){
			$crowdata = $ispcountryQ->row();
			$countryid = $crowdata->country_id;
			
			$data['countryid'] = $countryid;
			$countryQ = $this->db->query("SELECT * FROM sht_countries WHERE id='".$countryid."'");
			if($countryQ->num_rows() > 0){
				$rowdata = $countryQ->row();
				$currid = $rowdata->currency_id;
				
				$currencyQ = $this->db->get_where('sht_currency', array('currency_id' => $currid));
				if($currencyQ->num_rows() > 0){
				    $currobj = $currencyQ->row();
				    $currsymbol = $currobj->currency_symbol;
				    $data['currency'] = $currsymbol;
				}
				$data['demo_cost'] = $rowdata->demo_cost;
				$data['cost_per_user'] = $rowdata->cost_per_user;
				$data['cost_per_location'] = $rowdata->cost_per_location;
			}
		}
		return $data;
	}
	public function delete_temp_billnumber($isp_uid, $bill_number){
		$this->db->delete("sht_temp_billnumber",  array('isp_uid' => $isp_uid, 'bill_number' => $bill_number));
		return 1;
	}
	public function checkunique_billnumber($isp_uid, $bill_number){
		$today_date = date('jny');
		$checkbillQ = $this->db->query("SELECT bill_number FROM sht_temp_billnumber WHERE isp_uid='".$isp_uid."' AND bill_number='".$bill_number."'");
		if($checkbillQ->num_rows() == 0){
			$this->db->insert("sht_temp_billnumber",  array('isp_uid' => $isp_uid, 'bill_number' => $bill_number));
			return $bill_number;
		}else{
			$lastdigits = substr($bill_number, strpos($bill_number, $isp_uid)  + strlen($isp_uid));
			$lastdigits = ($lastdigits + 1);
			$bill_number = $today_date.$isp_uid.$lastdigits;
			return $this->checkunique_billnumber($isp_uid, $bill_number);
		}
	}
	public function generate_billnumber(){
		$bill_number = '';
		$session_data = $this->session->userdata('isp_session');;
		$isp_uid = $session_data['isp_uid'];
		$today_date = date('jny');
		
		$lastbillnumberQ = $this->db->query("SELECT bill_number FROM `sht_subscriber_billing` WHERE isp_uid='".$isp_uid."' AND (bill_type='montly_pay' OR bill_type='midchange_plancost' OR bill_type='topup') AND DATE(bill_added_on) > '2018-11-05' ORDER BY id DESC LIMIT 1");
		if($lastbillnumberQ->num_rows() > 0){
			$rawdata = $lastbillnumberQ->row();
			$bill_number = $rawdata->bill_number;
			if ((strpos($bill_number, $isp_uid)) !== FALSE) {
				$lastdigits = substr($bill_number, strpos($bill_number, $isp_uid)  + strlen($isp_uid));
				$lastdigits = ($lastdigits + 1);
				$bill_number = $today_date.$isp_uid.$lastdigits;
			}else{
				$bill_number = $today_date.$isp_uid.'00';
			}
		}else{
			$bill_number = $today_date.$isp_uid.'00';
		}
		
		$bill_number = $this->checkunique_billnumber($isp_uid, $bill_number);
		return $bill_number;
	}
/*----------------- FUNCTION TO ACTIVATE/DEACTIVATE USER START ---------------------------------------------*/	
	
	public function getisp_portalurl(){
		$query = $this->db->query("SELECT portal_url FROM sht_isp_admin WHERE isp_uid='".ISPID."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			return $rowdata->portal_url;
		}else{
			return '';
		}
		
	}
	
	public function disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret){
		$data = array("apikey" => "n|A~ok4Y3D>&{U&S(A@G", 'username' => $username, 'userframedipaddress' => $userframedipaddress, 'usernasipaddress' => $usernasipaddress, 'secret' => $secret);
		$data_string = array('requestData' => json_encode($data));
		$ch = curl_init('http://103.20.213.150/decibel/decibelapis/disconnect_user_session');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		$curl_response = curl_exec($ch);
		if ($curl_response === false) {
		    $info = curl_getinfo($ch);
		    curl_close($ch);
		    die('error occured during curl exec. Additioanl info: ' . var_export($info));
		}
		curl_close($ch);
		return $curl_response;
	}
	
	public function user_databalance($uuid){
		$data = array();
		//$query = $this->db->query("SELECT downlimit, comblimit FROM sht_users WHERE uid='".$uuid."' AND enableuser='1'");
		$query = $this->db->query("SELECT downlimit, comblimit FROM sht_users WHERE uid='".$uuid."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			$data['downlimit'] = $rowdata->downlimit;
			$data['comblimit'] = $rowdata->comblimit;
		}
		return $data;
	}
	public function user_activate_check(){
		$data = array();
		$subsid = $this->input->post('subsid');
		$is_user_activated = $this->input->post('is_user_activated');
		//$activQ = $this->db->get_where('sht_subscriber_activation_panel', array('subscriber_id' => $subsid, 'personal_details' => '1', 'kyc_details' => '1', 'plan_details' => '1', 'installation_details' => '1', 'security_details' => '1'));
		$activQ = $this->db->order_by('id', 'DESC')->limit(1)->get_where('sht_subscriber_activation_panel', array('subscriber_id' => $subsid));
		if($activQ->num_rows() > 0){
			$activeQrow = $activQ->row();
			$personal_details = $activeQrow->personal_details;
			$kyc_details = $activeQrow->kyc_details;
			$plan_details = $activeQrow->plan_details;
			$installation_details = $activeQrow->installation_details;
			$security_details = $activeQrow->security_details;
			
			if(($personal_details == 1) && ($kyc_details == 1) && ($plan_details == 1) && ($installation_details == 1) && ($security_details == 1)){	
				if($is_user_activated == '0'){
					$data['active_user'] = '0';
					$subsQ = $this->db->query("SELECT id, mobile,firstname,lastname FROM sht_users WHERE id='".$subsid."'");
					$subsQ_numrows = $subsQ->num_rows();
					if($subsQ_numrows > 0){
						$subs_rowdata = $subsQ->row();
						$phone = $subs_rowdata->mobile;
						$data['username'] = ucfirst($subs_rowdata->firstname).' '.ucfirst($subs_rowdata->lastname);
						if($phone != ''){
							$data['phone'] = $phone;
							$otp_verfiy=$this->input->post('otp_verify');
							if($otp_verfiy==1)
							{
								$data['useract_otp'] = $this->OTP_MSG($phone);
							}else
							{
								$data['useract_otp'] = 'xxxx';
							}
							//$data['useract_otp'] = $this->OTP_MSG($phone);
							
						}else{
							$data['phone'] = '0';
							$data['useract_otp'] = '0';
						}
					}
				}
			}else{
				$data['personal_details'] = $activeQrow->personal_details;
				$data['kyc_details'] = $activeQrow->kyc_details;
				$data['plan_details'] = $activeQrow->plan_details;
				$data['installation_details'] = $activeQrow->installation_details;
				$data['security_details'] = $activeQrow->security_details;
				$data['username'] = '';
				$data['phone'] = '0';
				$data['useract_otp'] = '0';
				$data['active_user'] = '0';
			}
		}else{
			$data['personal_details'] = 0;
			$data['kyc_details'] = 0;
			$data['plan_details'] = 0;
			$data['installation_details'] = 0;
			$data['security_details'] = 0;
			$data['username'] = '';
			$data['phone'] = '0';
			$data['useract_otp'] = '0';
			$data['active_user'] = '0';
		}
		
		echo json_encode($data);
	}
	
	public function active_user_confirmation(){
		/******************************************
		 * PSD, ED, NBD, PaidTill
		 *
		 * PTD = User Activation Date (If No Payment Done)
		 * PTD = UAD + No. of Months  (If Advance Pay)
		 * After First Billing Cycle Paid By User
		 * PTD = Last PTD
		 *
		 *******************************************/
		$subsid = $this->input->post('subsid');
		$subs_uuid = $this->input->post('subs_uuid');
		$send_otp = $this->input->post('send_otp');
		$enter_otp = $this->input->post('enter_otp');
		$phone = $this->input->post('phone');
		
		$isp_license_data = $this->isp_license_data(); 
		if($isp_license_data == 1){	
			//$password = md5($send_otp);
			$portalurl = $this->getisp_portalurl();
			if($portalurl == 'vsnt'){
				$radchk_pwd = '12345';
			}else{
				$radchk_pwd = random_string('alnum', 7);
			}
			$radchk_pwd = strtolower($radchk_pwd);
			$password = md5($radchk_pwd);
			$radchkQ = $this->db->get_where('radcheck', array('username' => $subs_uuid));
			if($radchkQ->num_rows() > 0){
				//$this->db->update('radcheck', array('value' => $radchk_pwd), array('username' => $subs_uuid, 'attribute' => 'Cleartext-Password'));
			}else{
				$this->db->query("INSERT INTO radcheck SET username='".$subs_uuid."', attribute = 'Cleartext-Password', op = ':=', value = '".$radchk_pwd."'");
				$this->db->query("INSERT INTO radcheck SET username='".$subs_uuid."', attribute = 'Simultaneous-Use', op = ':=', value = '1'");
				
			}
			
			$ispcodet = $this->countrydetails();
			$session_data = $this->session->userdata('isp_session');
			$session_data['active_user'] = "1";
			$this->session->set_userdata("isp_session", $session_data);
			$superadmin = $session_data['super_admin'];
			$isp_uid = $session_data['isp_uid'];
			
			$pbcycle_rowdata = $this->plan_billing_cycle($isp_uid);
			$billing_cycle_date = $pbcycle_rowdata->billing_cycle;
			$today = date('d');
			
			/* --------- TO DEDUCT MONEY FROM ISP WALLET -----------*/
			$checkUserQ = $this->db->query("SELECT uid FROM sht_isp_users_track WHERE uid='".$subs_uuid."' AND  YEAR(`added_on`) = YEAR(CURRENT_DATE()) AND MONTH(`added_on`) = MONTH(CURRENT_DATE())");
			if($checkUserQ->num_rows() == 0){
				$this->db->insert('sht_isp_users_track', array('isp_uid' => ISPID, 'uid' => $subs_uuid, 'added_on' => date('Y-m-d H:i:s')));
				$this->db->insert('sht_isp_passbook', array('isp_uid' => ISPID, 'total_active_users' => '1', 'cost' => $ispcodet['cost_per_user'], 'added_on' => date('Y-m-d H:i:s')));
				
			}
			/* -------- END -----------*/
			
			$user_updatedatarr = array('enableuser' => '1', 'orig_pwd' => $radchk_pwd, 'password' => $password, 'account_activated_on' => date('Y-m-d H:i:s'), 'plan_activated_date' => date('Y-m-d'), 'plan_changed_datetime' => date('Y-m-d H:i:s'), 'sms_alert' => '1', 'email_alert' => '1');
	
			$userassoc_plandata = $this->userassoc_plandata($subs_uuid);
			//print_r($userassoc_plandata); die;
			$datacalc = $userassoc_plandata['datacalc'];
			$plantype = $userassoc_plandata['plantype'];
			$user_credit_limit = $userassoc_plandata['user_credit_limit'];
			$plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
			$user_plan_type = $userassoc_plandata['user_plan_type'];
			$plan_price = $userassoc_plandata['plan_price'];
			$baseplanid = $userassoc_plandata['planid'];
			$total_advpay = $userassoc_plandata['total_advpay'];
			$plan_duration = $userassoc_plandata['plan_duration'];
			$plan_tilldays = $userassoc_plandata['plan_tilldays'];

			$prebill_oncycle = 0; $planautorenewal = 1;
			$prebill_oncycleQ = $this->db->query("SELECT prebill_oncycle, planautorenewal FROM sht_users WHERE uid='".$subs_uuid."'");
			if($prebill_oncycleQ->num_rows() > 0){
				$prowdata = $prebill_oncycleQ->row();
				$prebill_oncycle = $prowdata->prebill_oncycle;
				$planautorenewal = $prowdata->planautorenewal;
			}
			if($user_plan_type == 'prepaid'){
				if($prebill_oncycle == 1){
					$nextbdate = date('Y-m-d', strtotime('+1 month'));
					$nextbdate = new DateTime($nextbdate);
					$nextbdate->setDate($nextbdate->format('Y'), $nextbdate->format('m'), $billing_cycle_date);
					$next_month_billdate = $nextbdate->format('Y-m-d');
					
					$nxttimestamp = strtotime($next_month_billdate);
					$todtimestamp = strtotime(date('Y-m-d'));
					$netdays_alloted = round(abs($nxttimestamp - $todtimestamp) / 86400);
					
					$plancost_per_day = round(($plan_price/$plan_tilldays));
					$plancost_fornow = round($plancost_per_day * $netdays_alloted);
					
				}else{
					$plancost_fornow = $plan_price;
				}
				
				$billnumber = $this->generate_billnumber();
				$prebillingarr = array(
					'subscriber_uuid' => $subs_uuid,
					'plan_id' => $baseplanid,
					'user_plan_type' => 'prepaid',
					'bill_added_on' => date('Y-m-d H:i:s'),
					'bill_starts_from' => date('Y-m-d'),
					'bill_number' => $billnumber,
					'bill_type' => 'montly_pay',
					'payment_mode' => '',
					'receipt_received' => '0',
					'actual_amount' => round(($plancost_fornow * 100) / 118),
					'discount' => '0',
					'total_amount' => $plancost_fornow,
					'isp_uid' => $isp_uid
				);
				
				$this->db->insert('sht_subscriber_billing', $prebillingarr);
				$prebillid = $this->db->insert_id();
				$prePassbookArr = array(
					'isp_uid' => $isp_uid,
					'subscriber_uuid' => $subs_uuid,
					'billing_id' => $prebillid,
					'srvid' => $baseplanid,
					'plan_cost' => $plancost_fornow,
					'added_on'	=> date('Y-m-d H:i:s')
				);
				$this->db->insert('sht_subscriber_passbook', $prePassbookArr);
				
				$this->delete_temp_billnumber($isp_uid, $billnumber);
				$user_wallet_balance = $this->userbalance_amount($subs_uuid);
				/*if($user_wallet_balance < $plan_price){
					$prebillingarr['adjusted_amount'] = $user_wallet_balance;
				}elseif($user_wallet_balance >= $plan_price){
					$prebillingarr['payment_mode'] = 'wallet';
					$prebillingarr['adjusted_amount'] = '0.00';
					$prebillingarr['receipt_received'] = '1';
				}*/
				
				$expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
			}else{
				$user_wallet_balance = $this->userbalance_amount($subs_uuid);
				$expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
			}
			
			if($today >= $billing_cycle_date){
				$nextdate = date('Y-m-d', strtotime('+1 month'));
				$nextdate = new DateTime($nextdate);
				$nextdate->setDate($nextdate->format('Y'), $nextdate->format('m'), $pbcycle_rowdata->billing_cycle);
				$next_month = $nextdate->format('Y-m-d');
				$user_updatedatarr['next_bill_date'] = $next_month;
			}else{
				$user_updatedatarr['next_bill_date'] = date('Y-m').'-'.$pbcycle_rowdata->billing_cycle;
			}
			
			if($user_plan_type == 'prepaid'){
				if($prebill_oncycle == '1'){
					$nextbdate = date('Y-m-d', strtotime('+1 month'));
					$nextbdate = new DateTime($nextbdate);
					$nextbdate->setDate($nextbdate->format('Y'), $nextbdate->format('m'), $billing_cycle_date);
					$user_updatedatarr['next_bill_date'] = $nextbdate->format('Y-m-d');
				}else{
					$pnext_bill_date = date('Y-m-d',strtotime("+$plan_tilldays day"));
					$user_updatedatarr['next_bill_date'] = $pnext_bill_date;
				}
			}
			
			$advpayQ = $this->db->query("SELECT SUM(advancepay_for_months) as advancepay_for_months, SUM(number_of_free_months) as number_of_free_months FROM sht_advance_payments WHERE uid='".$subs_uuid."' AND balance_left != '0.00'");
			if($advpayQ->num_rows() > 0){
				$advrowdata = $advpayQ->row();
				$advancepay_for_months = $advrowdata->advancepay_for_months;
				$number_of_free_months = $advrowdata->number_of_free_months;
				$payformonths = $advancepay_for_months + $number_of_free_months;
				$paidTilldate = date("Y-m-d", strtotime(" +$payformonths months"));
			}else{
				$paidTilldate = $user_updatedatarr['plan_activated_date'];
			}
			
			$expiration_date = date('Y-m-d', strtotime($paidTilldate . " +".$expiration_acctdays." days"));
			
			$user_updatedatarr['expiration'] = $expiration_date.' 00:00:00';
			$user_updatedatarr['paidtill_date'] = $paidTilldate;
			if($datacalc == 1){
				$user_updatedatarr['downlimit'] = $userassoc_plandata['datalimit'];
			}elseif($datacalc == 2){
				$user_updatedatarr['comblimit'] = $userassoc_plandata['datalimit'];
			}
			
			$user_updatedatarr['prebill_oncycle'] = $prebill_oncycle;
			$this->db->update('sht_users', $user_updatedatarr , array('uid' => $subs_uuid));
                        $this->notify_model->user_activation_alert($subs_uuid, $radchk_pwd);
			$this->emailer_model->activate_user_emailer($subs_uuid, $radchk_pwd);
			
			if($planautorenewal == 1){
				$planname = $this->planname($baseplanid);
				$this->notify_model->planautorenewal_alert($subs_uuid, $planname);
				$this->emailer_model->planautorenewalemail_alert($subs_uuid, $planname);
			}
		}
		echo json_encode(1);
	}


	public function updateuser_profilestatus(){
		$login_userid = $this->session->userdata['isp_session']['userid'];
		$uuid = $this->input->post('uuid');
		$inactivate_type = $this->input->post('inactivate_type');
		$suspend_days = $this->input->post('suspend_days');
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		
		if(($inactivate_type == 'suspended') || ($inactivate_type == 'terminate')){ 
			$previouslogintype = $this->db->query("SELECT logintype FROM sht_users WHERE uid='".$uuid."'");
			if($previouslogintype->num_rows() > 0){
				$logintype = $previouslogintype->row()->logintype;
				if($logintype == 'ill_ipuser'){
					$this->load->library('routerlib');
					$chk_illassigned = $this->db->query("SELECT * FROM sht_users_ill_ips_assigned WHERE  isp_uid = '".$isp_uid."' AND uid='".$uuid."' AND status='1' LIMIT 1");
					if($chk_illassigned->num_rows() > 0){
						$nasobj = $chk_illassigned->row();
						$router_port = '8728';
						$router_id = $nasobj->nasname;
						$router_user = $nasobj->username;
						$router_password = $nasobj->password;
						
						if($router_port != ''){
						    $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
						}else{
						    $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password");	
						}
						
						if($router_conn !== null){
							$router_conn->set("/ip/firewall/address-list",array(".id"=>$uuid."_ILL", "disabled" => "yes"));
							$router_conn->set("/ip/firewall/filter",array(".id"=>$uuid."_ILL", "disabled" => "yes"));
						}
					}
				}
				
			}
		}
		
		$this->db->update('sht_users', array('enableuser' => '0', 'inactivate_type' => $inactivate_type, 'suspended_days' => $suspend_days, 'suspendedon' => date('Y-m-d')), array('uid' => $uuid));
		$this->db->insert('sht_users_status_report', array('uid' => $uuid, 'status_changeby' => $login_userid, 'inactivate_type' => $inactivate_type, 'suspended_days' => $suspend_days, 'added_on' => date('Y-m-d H:i:s')));
                
                $nasipQ = $this->db->query("SELECT tb1.radacctid,tb1.username, tb1.nasipaddress, tb1.framedipaddress, tb2.secret FROM radacct as tb1 INNER JOIN nas as tb2 ON(tb1.nasipaddress=tb2.nasname) WHERE tb1.username='".$uuid."' ORDER BY radacctid DESC LIMIT 1");
                if($nasipQ->num_rows() > 0){
                    $nasrowdata = $nasipQ->row();
                    $radacctid = $nasrowdata->radacctid;
                    $username = $nasrowdata->username;
                    $userframedipaddress = $nasrowdata->framedipaddress;
                    $usernasipaddress = $nasrowdata->nasipaddress;
                    $secret = $nasrowdata->secret;
                    //@exec("echo User-Name:=$username,Framed-Ip-Address=$userframedipaddress | radclient -x $usernasipaddress:3799 disconnect $secret");
                    $this->disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret);
                    $this->db->query("UPDATE radacct SET acctstoptime='".date('Y-m-d H:i:s')."' WHERE username='".$uuid."' AND acctstoptime IS NULL");
                }
		return true;
	}
	
	public function resetuser_toactivate(){
		$login_userid = $this->session->userdata['isp_session']['userid'];
		$uuid = $this->input->post('uuid');
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$pbcycle_rowdata = $this->plan_billing_cycle($isp_uid);
		$billing_cycle_date = $pbcycle_rowdata->billing_cycle;
		
		$inactivate_type = '';
		$userQuery = $this->db->query("SELECT user_plan_type,prebill_oncycle, baseplanid, inactivate_type, logintype FROM sht_users WHERE uid='".$uuid."'");
		if($userQuery->num_rows() > 0){
			$urowdata = $userQuery->row();
			$inactivate_type = $urowdata->inactivate_type;
			$baseplanid = $urowdata->baseplanid;
			$user_plan_type = $urowdata->user_plan_type;
			$prebill_oncycle = $urowdata->prebill_oncycle;
			$logintype = $urowdata->logintype;
			
			$user_creditlimit = $this->getuser_creditlimit($baseplanid, $uuid);
			$this->db->update('sht_users', array('user_credit_limit' => $user_creditlimit), array('uid' => $uuid));
			
			
			if($logintype == 'ill_ipuser'){
				$this->load->library('routerlib');
				$chk_illassigned = $this->db->query("SELECT * FROM sht_users_ill_ips_assigned WHERE  isp_uid = '".$isp_uid."' AND uid='".$uuid."' AND status='1' LIMIT 1");
				if($chk_illassigned->num_rows() > 0){
					$nasobj = $chk_illassigned->row();
					$router_port = '8728';
					$router_id = $nasobj->nasname;
					$router_user = $nasobj->username;
					$router_password = $nasobj->password;
					
					if($router_port != ''){
					    $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
					}else{
					    $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password");	
					}
					
					if($router_conn !== null){
						$router_conn->set("/ip/firewall/address-list",array(".id"=>$uuid."_ILL", "disabled" => "no"));
						$router_conn->set("/ip/firewall/filter",array(".id"=>$uuid."_ILL", "disabled" => "no"));
					}
				}
			}
			
			if($inactivate_type != 'terminate'){
				$this->db->update('sht_users', array('enableuser' => '1', 'inactivate_type' =>'', 'suspended_days' => '0', 'suspendedon' => '0000-00-00'), array('uid' => $uuid));
			}else{

				$userassoc_plandata = $this->userassoc_plandata($uuid);
				//print_r($userassoc_plandata); die;
				$datacalc = $userassoc_plandata['datacalc'];
				$plantype = $userassoc_plandata['plantype'];
				$user_credit_limit = $userassoc_plandata['user_credit_limit'];
				$plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
				$user_plan_type = $userassoc_plandata['user_plan_type'];
				$plan_price = $userassoc_plandata['plan_price'];
				$baseplanid = $userassoc_plandata['planid'];
				$total_advpay = $userassoc_plandata['total_advpay'];
				$plan_duration = $userassoc_plandata['plan_duration'];
				$plan_tilldays = $userassoc_plandata['plan_tilldays'];
	
				$user_updatedatarr = array('enableuser' => '1', 'inactivate_type' =>'', 'suspended_days' => '0', 'suspendedon' => '0000-00-00');
				
				if($user_plan_type == 'prepaid'){
					if($prebill_oncycle == 1){
						$nextbdate = date('Y-m-d', strtotime('+1 month'));
						$nextbdate = new DateTime($nextbdate);
						$nextbdate->setDate($nextbdate->format('Y'), $nextbdate->format('m'), $billing_cycle_date);
						$next_month_billdate = $nextbdate->format('Y-m-d');
						
						$nxttimestamp = strtotime($next_month_billdate);
						$todtimestamp = strtotime(date('Y-m-d'));
						$netdays_alloted = round(abs($nxttimestamp - $todtimestamp) / 86400);
						
						$plancost_per_day = round(($plan_price/$plan_tilldays));
						$plancost_fornow = round($plancost_per_day * $netdays_alloted);
						
					}else{
						$plancost_fornow = $plan_price;
					}
					
					$billnumber = $this->generate_billnumber();
					$prebillingarr = array(
						'subscriber_uuid' => $uuid,
						'plan_id' => $baseplanid,
						'user_plan_type' => 'prepaid',
						'bill_added_on' => date('Y-m-d H:i:s'),
						'bill_starts_from' => date('Y-m-d'),
						'bill_number' => $billnumber,
						'bill_type' => 'montly_pay',
						'payment_mode' => '',
						'receipt_received' => '0',
						'actual_amount' => $plancost_fornow,
						'discount' => '0',
						'total_amount' => $plancost_fornow,
						'isp_uid' => $isp_uid
					);
					
					$this->db->insert('sht_subscriber_billing', $prebillingarr);
					$prebillid = $this->db->insert_id();
					$prePassbookArr = array(
						'isp_uid' => $isp_uid,
						'subscriber_uuid' => $uuid,
						'billing_id' => $prebillid,
						'srvid' => $baseplanid,
						'plan_cost' => $plancost_fornow,
						'added_on'	=> date('Y-m-d H:i:s')
					);
					$this->db->insert('sht_subscriber_passbook', $prePassbookArr);
					
					$this->delete_temp_billnumber($isp_uid, $billnumber);
					$user_wallet_balance = $this->userbalance_amount($uuid);
					$expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
				}else{
					$user_wallet_balance = $this->userbalance_amount($uuid);
					$expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
				}
				$pbcycle_rowdata = $this->plan_billing_cycle($isp_uid);
				$billing_cycle_date = $pbcycle_rowdata->billing_cycle;
				$today = date('d');
				if($today >= $billing_cycle_date){
					$nextdate = date('Y-m-d', strtotime('+1 month'));
					$nextdate = new DateTime($nextdate);
					$nextdate->setDate($nextdate->format('Y'), $nextdate->format('m'), $pbcycle_rowdata->billing_cycle);
					$next_month = $nextdate->format('Y-m-d');
					$user_updatedatarr['next_bill_date'] = $next_month;
				}else{
					$user_updatedatarr['next_bill_date'] = date('Y-m').'-'.$pbcycle_rowdata->billing_cycle;
				}
				
				if($user_plan_type == 'prepaid'){
					if($prebill_oncycle == '1'){
						$nextbdate = date('Y-m-d', strtotime('+1 month'));
						$nextbdate = new DateTime($nextbdate);
						$nextbdate->setDate($nextbdate->format('Y'), $nextbdate->format('m'), $billing_cycle_date);
						$user_updatedatarr['next_bill_date'] = $nextbdate->format('Y-m-d');
					}else{
						$pnext_bill_date = date('Y-m-d',strtotime("+$plan_tilldays day"));
						$user_updatedatarr['next_bill_date'] = $pnext_bill_date;
					}
				}
				
				$advpayQ = $this->db->query("SELECT SUM(advancepay_for_months) as advancepay_for_months, SUM(number_of_free_months) as number_of_free_months FROM sht_advance_payments WHERE uid='".$uuid."' AND balance_left != '0.00'");
				if($advpayQ->num_rows() > 0){
					$advrowdata = $advpayQ->row();
					$advancepay_for_months = $advrowdata->advancepay_for_months;
					$number_of_free_months = $advrowdata->number_of_free_months;
					$payformonths = $advancepay_for_months + $number_of_free_months;
					$paidTilldate = date("Y-m-d", strtotime(" +$payformonths months"));
				}else{
					$paidTilldate = date('Y-m-d');
				}
				
				$expiration_date = date('Y-m-d', strtotime($paidTilldate . " +".$expiration_acctdays." days"));
				
				$user_updatedatarr['expiration'] = $expiration_date.' 00:00:00';
				$user_updatedatarr['paidtill_date'] = $paidTilldate;
				if($datacalc == 1){
					$user_updatedatarr['downlimit'] = $userassoc_plandata['datalimit'];
				}elseif($datacalc == 2){
					$user_updatedatarr['comblimit'] = $userassoc_plandata['datalimit'];
				}
				
				$user_updatedatarr['plan_activated_date'] = date('Y-m-d');
				$user_updatedatarr['plan_changed_datetime'] = date('Y-m-d H:i:s');
				
				$this->db->update('sht_users', $user_updatedatarr , array('uid' => $uuid));
				//$this->notify_model->user_activation_alert($subs_uuid, $radchk_pwd);
			}
		}
		
		$this->db->update('sht_users_status_report', array('activated_on' => date('Y-m-d H:i:s'), 'activated_by' => $login_userid), array('uid' => $uuid));
		return true;
	}
/*------------------ PAYTM OTP APIS BEGINS ----------------------------------------------------------------*/	
	
	private  $baseUrl = "http://sendotp.msg91.com/api";
	public function OTP_MSG($phone) {
	    $data = array("countryCode" => "91", "mobileNumber" => "$phone","getGeneratedOTP" => true);
	    $data_string = json_encode($data);
	    $ch = curl_init($this->baseUrl.'/generateOTP');
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($data_string),
		'application-Key: gGlhmtbDPqDEBNqIBINjXZfsLyVy5jOOszvb1Jy9SEHFN-HlARjLn-cGEsv2hZc9VBlyWSm2A9TaQDLzj2gejukAG0ZQrKsFynxW4s2NewHaPXbcU41WUEwiN7BJSjesbbjF4GdSvJ57rOo5yj1XcA=='
	    ));
	    $result = curl_exec($ch);
	    curl_close($ch);
	    //echo '<pre>'; print_r($result);
	    $response = json_decode($result,true);
	    if($response["status"] == "error"){
	       // return $response["response"]["code"];
		return 0;
	    }else{
		return $response["response"]["oneTimePassword"];
	    }
	}
	public function verifyBySendOtp($phone,$otp){
	    $data = array("countryCode" => "91", "mobileNumber" => "$phone", "oneTimePassword" => "$otp");
	    $data_string = json_encode($data);
	    $ch = curl_init($this->baseUrl . '/verifyOTP');
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	      'Content-Type: application/json',
	      'Content-Length: ' . strlen($data_string),
	      'application-Key: gGlhmtbDPqDEBNqIBINjXZfsLyVy5jOOszvb1Jy9SEHFN-HlARjLn-cGEsv2hZc9VBlyWSm2A9TaQDLzj2gejukAG0ZQrKsFynxW4s2NewHaPXbcU41WUEwiN7BJSjesbbjF4GdSvJ57rOo5yj1XcA=='
	    ));
	    $result = curl_exec($ch);
	    curl_close($ch);
	    $response = json_decode($result, true);
	    if ($response["status"] == "error") {
	     //customize this as per your framework
	     //$resp['message'] =  $response["response"]["code"];
	      return 0;
	    }else {
	      //$resp['message'] =  "NUMBER VERIFIED SUCCESSFULLY";
	      return 1;
	    }
	}


/*------------------ GENERAL FUNCTIONS STARTS ---------------------------------------------------------------*/
	
	public function get_ispdetail_info(){
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$query = $this->db->query("select logo_image from sht_isp_detail where status='1' AND isp_uid='".$isp_uid."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			return $rowdata->logo_image;
		}else{
			return  0;
		}
		
        }
	public function convertTodata($from){
		$number=substr($from,0,-2);
		switch(strtoupper(substr($from,-2))){
		    case "KB":
			return $number/1024;
		    case "MB":
			return $number/pow(1024,2);
		    case "GB":
			return round($number/pow(1024, 3), 2);
		    case "TB":
			return $number/pow(1024,4);
		    case "PB":
			return $number/pow(1024,5);
		    default:
			return $from;
		}
	}
	public function complaints_count(){
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$regiontype = $this->dept_region_type();
		$permicond = ''; $sczcond = '';
		if ($regiontype == "region") {
			$dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='".$dept_id."' AND status='1' AND is_deleted='0'");
			$total_deptregion = $dept_regionQ->num_rows();
			if($dept_regionQ->num_rows() > 0){
				$c = 1;
				foreach($dept_regionQ->result() as $deptobj){
					$stateid = $deptobj->state_id;
					$cityid = $deptobj->city_id;
					$zoneid = $deptobj->zone_id;
					if($cityid == 'all'){
						$permicond .= " (state='".$stateid."') ";
					}elseif($zoneid == 'all'){
						$permicond .= " (state='".$stateid."' AND city='".$cityid."') ";
					}else{
						$permicond .= " (state='".$stateid."' AND city='".$cityid."' AND zone='".$zoneid."') ";
					}
					if($c != $total_deptregion){
						$permicond .= ' OR ';
					}
					$c++;
				}
			}
			$sczcond .= ' AND ('.$permicond.')';
		}
		$userlistarr = array();
		$cquery = $this->db->query("SELECT uid FROM sht_users WHERE 1 $sczcond AND isp_uid='".$isp_uid."'");
		//echo $this->db->last_query(); die;
		$total_csearch = $cquery->num_rows();
		if($total_csearch > 0){
			foreach($cquery->result() as $sobj){
				$userlistarr[] = "'".$sobj->uid."'";
			}
			$userlistarr = implode(',', $userlistarr);
			$complaints = $this->db->query("SELECT count(id) as complaints FROM sht_subscriber_tickets WHERE status='0' AND subscriber_uuid IN($userlistarr)");
			return $complaints->row()->complaints;
		}else{
			return 0;
		}
	}
	public function isp_username($ispid){
	  $name = '';
	  $query = $this->db->query("SELECT name FROM sht_isp_users WHERE id='".$ispid."' AND status='1' AND is_deleted='0'");
	  $exists = $query->num_rows();
	  if($exists > 0){
		$name = $query->row()->name;
	  }
	  return $name;
	}
	
	public function tkt_sorter_username($ispid){
	  $name = '';
	  $query = $this->db->query("SELECT name FROM sht_isp_users WHERE id='".$ispid."' AND status='1' AND is_deleted='0'");
	  $exists = $query->num_rows();
	  if($exists > 0){
		$name = $query->row()->name;
	  }
	  return $name;
	}
	
	public function state_list($stateid=''){
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		//$regiontype = $this->dept_region_type();
		$ispstates = $this->superadmin_regioninfo();
		
		if ($superadmin == 1) {
			$stateQ = $this->db->query("SELECT * FROM sht_states WHERE id IN($ispstates)");
			//echo $this->db->last_query(); die;
			$num_rows = $stateQ->num_rows();
			if($num_rows > 0){
				$gen = '';
				foreach($stateQ->result() as $stobj){
					$sel = '';
					if($stateid == $stobj->id){
						$sel = 'selected="selected"';
					}
					$gen .= '<option value="'.$stobj->id.'" data-pincode="'.$stobj->state_pincode.'" '.$sel.'>'.$stobj->state.'</option>';
				}
			}
		}else{
			$query = $this->db->query("select state_id from sht_dept_region where dept_id='" . $dept_id . "'");
			$statearr = array();
			foreach ($query->result() as $val) {
			    $statearr[] = $val->state_id;
			}
			if (in_array('all', $statearr)) {
			    $stateQ = $this->db->get('sht_states');
			} else {
			    $sid = '"' . implode('", "', $statearr) . '"';
			    $stateQ = $this->db->query("select * from sht_states where id IN ({$sid}) ");
			}
			$num_rows = $stateQ->num_rows();
			if($num_rows > 0){
				$gen = '';
				foreach($stateQ->result() as $stobj){
					$sel = '';
					if($stateid == $stobj->id){
						$sel = 'selected="selected"';
					}
					$gen .= '<option value="'.$stobj->id.'" data-pincode="'.$stobj->state_pincode.'" '.$sel.'>'.$stobj->state.'</option>';
				}
			}
		}
		echo $gen;
	}
	
	public function zone_list($cityid, $zoneid='', $stateid=''){
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$ispstates = $this->superadmin_regioninfo();

		if ($superadmin == 1) {
			  $zonecond="";
	    if($sessiondata['is_franchise']==1)
	    {
		$franchiseQ=$this->db->query("select zone from sht_isp_admin_region where isp_uid='".$isp_uid."' and city='".$cityid."'");
		//echo $this->db->last_query();
		if($franchiseQ->num_rows()>0)
		{
		    $zoneidarr=array();
		    foreach($franchiseQ->result() as $franchO)
		    {
			$zoneidarr[]=$franchO->zone;
		    }
		 //   echo "<pre>"; print_R($cityidarr);die;
		    if(in_array('all',$zoneidarr))
		    {
			$zonecond="";
		    }
		    else
		    {
			 $zfrid = '"' . implode('", "', $zoneidarr) . '"';
			 $zonecond="and id IN ({$zfrid})";
		    }
		    $ispq=$this->db->query("select parent_isp from sht_isp_admin where isp_uid='".$isp_uid."'");
		$isparr=$ispq->row_array();
		$isp_uid=$isparr['parent_isp'];
			
		}
		
	    }
	    else
	    {
		$zonecond="";
	    }
			$zoneQ = $this->db->query("SELECT * FROM sht_zones WHERE ((isp_uid='".$isp_uid."' AND city_id='".$cityid."') OR (isp_uid='0'  AND city_id='".$cityid."')) {$zonecond}");
			$num_rows = $zoneQ->num_rows();
			$gen = '';
			if($num_rows > 0){
				foreach($zoneQ->result() as $znobj){
					$sel = '';
					if($zoneid == $znobj->id){
						$sel = 'selected="selected"';
					}
					$gen .= '<option value="'.$znobj->id.'" '.$sel.'>'.$znobj->zone_name.'</option>';
				}
			}
			$gen .= '<option class="addzonefly" value="addz">--Add Zone--</option>';
		}else{
			$query = $this->db->query("select zone_id from sht_dept_region where dept_id='" . $dept_id . "' and state_id='" . $stateid . "'");
			$zonearr = array();
			foreach ($query->result() as $val) {
			    $zonearr[] = $val->zone_id;
			}
			if (in_array('all', $zonearr)) {
			    $zoneQ = $this->db->query("SELECT * FROM sht_zones WHERE (isp_uid='".$isp_uid."' AND city_id='".$cityid."') OR (isp_uid='0'  AND city_id='".$cityid."')");
			} else {
			    $zid = '"' . implode('", "', $zonearr) . '"';
			    $zoneQ = $this->db->query("select * from sht_zones where id IN ({$zid})");
			}
			$num_rows = $zoneQ->num_rows();
			$gen = '';
			if($num_rows > 0){
				foreach($zoneQ->result() as $znobj){
					$sel = '';
					if($zoneid == $znobj->id){
						$sel = 'selected="selected"';
					}
					$gen .= '<option value="'.$znobj->id.'" '.$sel.'>'.$znobj->zone_name.'</option>';
				}
			}
			$gen .= '<option class="addzonefly" value="addz">--Add Zone--</option>';
		}
		echo $gen;
	}
	
	public function getcitylist($stateid, $cityid=''){
		$gen = '';
		$pin = $this->input->post('pinid');
		$scityid = $this->input->post('cityid');
		if($scityid != ''){ $cityid = $scityid; }
		
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		//$regiontype = $this->dept_region_type();
		$ispstates = $this->superadmin_regioninfo();
		
		if ($superadmin == 1) {
			 $citycond="";
	    if($sessiondata['is_franchise']==1)
	    {
		$franchiseQ=$this->db->query("select city from sht_isp_admin_region where isp_uid='".$isp_uid."' and state='".$stateid."'");
		//echo $this->db->last_query();
		if($franchiseQ->num_rows()>0)
		{
		    $cityidarr=array();
		    foreach($franchiseQ->result() as $franchO)
		    {
			$cityidarr[]=$franchO->city;
		    }
		 //   echo "<pre>"; print_R($cityidarr);die;
		    if(in_array('all',$cityidarr))
		    {
			$citycond="";
		    }
		    else
		    {
			 $cfrid = '"' . implode('", "', $cityidarr) . '"';
			 $citycond="and city_id IN ({$cfrid})";
		    }
			$ispq=$this->db->query("select parent_isp from sht_isp_admin where isp_uid='".$isp_uid."'");
		$isparr=$ispq->row_array();
		$isp_uid=$isparr['parent_isp'];	
		}
		
	    }
	    else
	    {
		$citycond="";
	    }
			
			$cityQ = $this->db->query("SELECT * FROM sht_cities WHERE ((isp_uid='".$isp_uid."' AND city_state_id='".$stateid."') OR (isp_uid='0'  AND city_state_id='".$stateid."')) {$citycond}");
			$num_rows = $cityQ->num_rows();
			if($num_rows > 0){
				foreach($cityQ->result() as $ctobj){
					$sel = '';
					if($cityid == $ctobj->city_id){
						$sel = 'selected="selected"';
					}
					$gen .= '<option value="'.$ctobj->city_id.'" '.$sel.'>'.$ctobj->city_name.'</option>';
				}
			}
			$gen .= '<option class="addcityfly" value="addc">--Add City--</option>';
		}else{
			$query = $this->db->query("select city_id from sht_dept_region where dept_id='" . $dept_id . "' and state_id='" . $stateid . "'");
			$cityarr = array();
			foreach ($query->result() as $val) {
			    $cityarr[] = $val->city_id;
			}
			$isaddcity=0;
			if (in_array('all', $cityarr)) {
			    $cityQ = $this->db->query("SELECT * FROM sht_cities WHERE (isp_uid='".$isp_uid."' AND city_state_id='".$stateid."') OR (isp_uid='0'  AND city_state_id='".$stateid."')");
			} else {
			    $cid = '"' . implode('", "', $cityarr) . '"';
			    $cityQ = $this->db->query("select * from sht_cities where city_id IN ({$cid})");
			}
			$num_rows = $cityQ->num_rows();
			if($num_rows > 0){
				foreach($cityQ->result() as $ctobj){
					$sel = '';
					if($cityid == $ctobj->city_id){
						$sel = 'selected="selected"';
					}
					$gen .= '<option value="'.$ctobj->city_id.'" '.$sel.'>'.$ctobj->city_name.'</option>';
				}
			}
			$gen .= '<option class="addcityfly" value="addc">--Add City--</option>';
		}
		
		$newavailableid = '';
		if(isset($pin) && $pin != ''){
			$subs_query = $this->db->select_max('id')->get('sht_users');
			$lastid = $subs_query->row()->id;
			$newid = $lastid + 1;
			$lencount = 8 - (strlen($isp_uid) + strlen($newid));
			$newavailableid = $isp_uid.str_repeat('0',$lencount).$newid;
			$login_isp = $this->session->userdata['isp_session']['userid'];
			
			$checkdbQ = $this->db->get_where("sht_users", array('uid' => $newavailableid));
			if($checkdbQ->num_rows() > 0){
				$newid = $newid + 1;
				$lencountt = 8 - (strlen($isp_uid) + strlen($newid));
				$newavailableid = $isp_uid.str_repeat('0',$lencountt).$newid;
			}
			
			$checkholdQ = $this->db->get_where("sht_temp_subscriber", array('subscriber_id' => $newavailableid));
			if($checkholdQ->num_rows() > 0){
				$nnewid = $newid + 1;
				$nlencount = 8 - (strlen($isp_uid) + strlen($nnewid));
				$newavailableid = $isp_uid.str_repeat('0',$nlencount).$nnewid;
				$this->db->insert('sht_temp_subscriber', array('isp_user_id' => $login_isp, 'subscriber_id' => $newavailableid)); 
			}else{
				$this->db->insert('sht_temp_subscriber', array('isp_user_id' => $login_isp, 'subscriber_id' => $newavailableid));
			}
		}
		
		if($newavailableid != ''){
			echo $newavailableid.'~~~'.$gen;
		}else{
			echo $gen;
		}
	}
	public function getstatename($stateid){
		$name = '';
		$stateQ = $this->db->get_where('sht_states', array('id' => $stateid));
		$num_rows = $stateQ->num_rows();
		if($num_rows > 0){
			$data = $stateQ->row();
			$name = ucwords($data->state);
		}
		return $name;
	}
	public function getcityname($stateid, $cityid){
		$name = '';
		$cityQ = $this->db->get_where('sht_cities', array('city_state_id' => $stateid, 'city_id' => $cityid));
		$num_rows = $cityQ->num_rows();
		if($num_rows > 0){
			$data = $cityQ->row();
			$name = ucwords($data->city_name);
		}
		return $name;
	}
	public function getzonename($zoneid){
		$name = '';
		$zoneQ = $this->db->get_where('sht_zones', array('id' => $zoneid));
		$num_rows = $zoneQ->num_rows();
		if($num_rows > 0){
			$data = $zoneQ->row();
			$name = ucwords($data->zone_name);
		}
		return $name;
	}
	public function convertbytestodata($live_usage){
		$live_usage_value = '';
		$live_usage_value = round($live_usage/(1024*1024*1024),2);
		if($live_usage_value < 1){
			//get in mb
			$live_usage_value = round($live_usage/(1024*1024),2);
			if($live_usage_value < 1){
				//get in kb
				$live_usage_value = round($live_usage/(1024),2)."KB";
			}else{
				$live_usage_value = $live_usage_value."MB" ;
			}
		}else{
			$live_usage_value = $live_usage_value."GB";
		}
		
		return $live_usage_value;
	}
	public function live_usage($useruid){
		$data = array();
		$live_usage =0;
		$percent = 0;
		$topupdata = 0; $topup_allocated = 0; $topup_added_on = ''; $topup_usuage = 0;
		
		// get topup limit
		$get_topup = $this->db->query("select ss.plantype, ss.datalimit, su.added_on from sht_usertopupassoc as su inner join sht_services as ss on(su.topup_id = ss.srvid) where su.uid = '$useruid' AND su.status='1' AND su.terminate='0' AND su.topuptype='1'");
		if($get_topup->num_rows() > 0){
			//$get_topupdata = $get_topup->row();
			//$topup_added_on = date('Y-m-d H:i:s', strtotime($get_topupdata->added_on." -5 minutes"));
			foreach($get_topup->result() as $tobj){
				$topupdata += $tobj->datalimit;
				$topup_allocated += $tobj->datalimit;
				$topup_added_on = date('Y-m-d H:i:s', strtotime($tobj->added_on." -2 minutes"));
			}
		}
		
		// get user plan limit
		$get_plan = $this->db->query("select su.comblimit, su.plan_changed_datetime, su.downlimit, ss.plantype, ss.datalimit, ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$useruid' AND su.baseplanid != '0'");
		$plan_msg = '';
		$data_allocated = 0;
		$monthly_data = 0;
		$plan_type = '';
		$plandata_allocated = 0;
		$plan_changed_datetime = '';
		if($get_plan->num_rows() > 0){
			$get_plan_row = $get_plan->row_array();
			$plan_type = $get_plan_row['plantype'];
			$data['datacalcon'] = $get_plan_row['datacalc'];
			//$plan_changed_datetime = date('Y-m-d', strtotime($get_plan_row['plan_changed_datetime']));
			if($get_plan_row['plantype'] == '1'){
				$plan_msg = "Unlimited plan";
                                $monthly_data = 'UL';
				$data['uses_limit_msg'] = "Total Data: ".$plan_msg;
			}elseif($get_plan_row['plantype'] == '3'){
				$plandata_allocated = $get_plan_row['datalimit'];
				$data_allocated = $get_plan_row['datalimit'] + $topupdata;
				$plan_msg = round(($data_allocated/(1024*1024*1024)),2)."GB";
				
				$monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
				$topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
				$data['uses_limit_msg'] = "Total Data: ".$plan_msg. "<br/> Monthly Data: ".$monthly_data."<br/> Topup Data: ".$topupdata ;
				
			}elseif($get_plan_row['plantype'] == '4'){
				$plandata_allocated = $get_plan_row['datalimit'];
				$data_allocated = $get_plan_row['datalimit'] + $topupdata;
				$plan_msg = round(($data_allocated/(1024*1024*1024)),2)."GB";
				
				$monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
				$topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
				$data['uses_limit_msg'] = "Total Data: ".$plan_msg. "<br/> Monthly Data: ".$monthly_data."<br/> Topup Data: ".$topupdata ;
			}
		}else{
			$data['uses_limit_msg'] = '';
		}
		
                $data['monthly_datalimit'] = $monthly_data;
		$data['plandatalimit'] = $plandata_allocated;
		$session_data = $this->session->userdata('isp_session');
		$superadmin = $session_data['super_admin'];
		$isp_uid = $session_data['isp_uid'];
		
		// get billing date
		$get_billing_date = $this->db->query("select plan_activated_date, next_bill_date from sht_users where uid = '$useruid'");

		if($get_billing_date->num_rows() > 0){
			$row_billing_date = $get_billing_date->row_array();
			$start_date = date('Y-m-d', strtotime($row_billing_date['plan_activated_date']));
			$end = $row_billing_date['next_bill_date'];
			$end_date = date('Y-m-d', strtotime($row_billing_date['next_bill_date']));
		}
		$query = $this->db->query("select acctinputoctets, acctoutputoctets from radacct where username = '$useruid' AND DATE(acctstarttime) between '$start_date' and '$end_date'");
		foreach($query->result() as $row){
			$get_plan_row = $get_plan->row_array();
			$datacalc = $get_plan_row['datacalc'];
			if(($datacalc == 2) || ($datacalc == 0)){
				$live_usage = $live_usage + $row->acctinputoctets + $row->acctoutputoctets;
			}else{
				$live_usage = $live_usage + $row->acctoutputoctets;
			}
		}
		$data['radactt_usage'] = $live_usage;
		$live_usage_value = round($live_usage/(1024*1024*1024),2);
		if($live_usage_value < 1){
			//get in mb
			$live_usage_value = round($live_usage/(1024*1024),2);
			if($live_usage_value < 1){
				//get in kb
				$live_usage_value = round($live_usage/(1024),2)."KB";
			}else{
				$live_usage_value = $live_usage_value."MB" ;
			}
		}else{
			$live_usage_value = $live_usage_value."GB";
		}
		
		
		// IN CASE OF FUP PLAN
		$data['plan_type'] = $plan_type;
		if($plan_type == '3'){
			//echo 'live_usuage: '.$this->convertbytestodata($live_usage);
			if($live_usage <= $plandata_allocated){
				$data['live_usage'] = $live_usage_value;
				$data['postfup_data'] = '0KB';
				$data['topup_data'] = '0KB';
				
				$percent = ($live_usage*100)/$plandata_allocated;
				if($percent > 100){
					$percent = 100;
				}
				$data['uses_percent'] = $percent;
				$data['postfup_percent'] = 0;
				$data['topup_percent'] = 0;
			}
			// fetch query after topup apply if limit goes in negative
			elseif(($topup_allocated != 0) && ($live_usage > $plandata_allocated)){
				$end_date = date('Y-m-d H:i:s');
				$query = $this->db->query("select acctinputoctets, acctoutputoctets from radacct where username = '$useruid' AND acctstarttime between '$topup_added_on' and '$end_date'");
				//echo $this->db->last_query();
				foreach($query->result() as $row){
					$get_plan_row = $get_plan->row_array();
					$datacalc = $get_plan_row['datacalc'];
					if(($datacalc == 2) || ($datacalc == 0)){
						$topup_usuage = $topup_usuage + $row->acctinputoctets + $row->acctoutputoctets;
					}else{
						$topup_usuage = $topup_usuage + $row->acctoutputoctets;
					}
				}
				
				
				$data['live_usage'] = $this->convertbytestodata($plandata_allocated);
				
				if($topup_usuage > $topup_allocated){
					$postfupdata = ($live_usage - ($plandata_allocated + $topup_allocated));
					$data['topup_data'] = $this->convertbytestodata($topup_allocated);
				}else{
					$postfupdata = ($live_usage - ($plandata_allocated + $topup_usuage));
					$data['topup_data'] = $this->convertbytestodata($topup_usuage);
				}
				$data['postfup_data'] = $this->convertbytestodata($postfupdata);
				
				$fup_percent = ($live_usage*100)/$plandata_allocated;
				$postfup_percent = (($live_usage - $plandata_allocated) / 1024);
				$topup_percent = ($topup_usuage*100)/$topup_allocated;

				if($fup_percent > 100){
					$fup_percent = 100;
				}
				if($postfup_percent > 100){
					$postfup_percent = 100;
				}
				if($topup_percent > 100){
					$topup_percent = 100;
				}
				$data['uses_percent'] = $fup_percent;
				$data['postfup_percent'] = $postfup_percent;
				$data['topup_percent'] = $topup_percent;
			}
			elseif($live_usage > $plandata_allocated){
				$postfupdata = ($live_usage - $plandata_allocated);
				$data['live_usage'] = $this->convertbytestodata($plandata_allocated);
				$data['postfup_data'] = $this->convertbytestodata($postfupdata);
				$data['topup_data'] = 0;

				$fup_percent = ($live_usage*100)/$plandata_allocated;
				$postfup_percent = (($live_usage - $plandata_allocated) / 1024);
				$topup_percent = 0;

				if($fup_percent > 100){
					$fup_percent = 100;
				}
				if($postfup_percent > 100){
					$postfup_percent = 100;
				}
				$data['uses_percent'] = $fup_percent;
				$data['postfup_percent'] = $postfup_percent;
				$data['topup_percent'] = 0;
			}
		
		}else{
			$data['live_usage'] = $live_usage_value;
			if($data_allocated > 0){
				$percent = ($live_usage*100)/$data_allocated;
			}else{
				$percent = $live_usage/1024;
			}
			if($percent > 100){
				$percent = 100;
			}
			$data['uses_percent'] = $percent;
		}
                
                $frameipQuery = $this->db->query("select framedipaddress from radacct where username = '$useruid' ORDER BY radacctid DESC LIMIT 1");
                if($frameipQuery->num_rows() > 0){
                    $frowdata = $frameipQuery->row();
                    $data['framedip'] = $frowdata->framedipaddress;
                }else{
                    $data['framedip'] = '-';
                }
		
		return $data;
	}

    
	public function add_newcity(){
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		
		$newcity_name = $this->input->post('city_add');
		$state_id = $this->input->post('add_city_state');
		$cityq=$this->db->query("select city_id,city_name from sht_cities where city_name='".$newcity_name."' and city_state_id ='".$state_id."' and (isp_uid='".$isp_uid."' or isp_uid='0')");
		// echo $cityq->num_rows();die;
		if($cityq->num_rows()>0){
			$rowarr=$cityq->row_array();
			$data['html']="<option value='".$rowarr['city_id']."' selected>".$rowarr['city_name']."</option>";
			$data['state_id']=$state_id;
		}else{
			$tabledata=array("city_name"=>$newcity_name,"city_state_id"=>$state_id, "isp_uid" => $isp_uid);
			$this->db->insert("sht_cities",$tabledata);
		       
			$id = $this->db->insert_id();
			$data=array();
			$data['html']="<option value='".$id."' selected>".$newcity_name."</option>";
			$data['state_id']=$state_id;
		}
		echo json_encode($data);
		
	}
	public function add_newzone(){
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		
		$newzone_name = $this->input->post('zone_name');
		$state_id = $this->input->post('state_id');
		$city_id = $this->input->post('add_zone_city');
		$zoneq=$this->db->query("select id,zone_name from sht_zones where zone_name='".$newzone_name."' and city_id ='".$city_id."' and (isp_uid='".$isp_uid."' or isp_uid='0')");
		if($zoneq->num_rows()>0){
			$rowarr=$zoneq->row_array();
			$data['html']="<option value='".$rowarr['id']."' selected>".$rowarr['zone_name']."</option>";
			$data['city_id']=$city_id;
		}else{
			$tabledata=array("zone_name"=>$newzone_name,"city_id"=>$city_id, "isp_uid" => $isp_uid);
			$this->db->insert("sht_zones",$tabledata);
			$id = $this->db->insert_id();
			$data=array();
			$data['html']="<option value='".$id."' selected>".$newzone_name."</option>";
			$data['city_id']=$city_id;
		}
		echo json_encode($data);
		
	}
	public function useralerts_status($uuid){
		$data = array();
		$notifyQ = $this->db->query("SELECT sms_alert, email_alert FROM sht_users WHERE uid='".$uuid."'");
		if($notifyQ->num_rows() > 0){
			$notifydata = $notifyQ->row();
			$data['sms_alert'] = $notifydata->sms_alert;
			$data['email_alert'] = $notifydata->email_alert;
		}
		return $data;
	}
/*------------------- USER DASHBOARD STARTS ----------------------------------------------------------------*/	
	public function check_taxfilled(){
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$query=$this->db->query("select tax from sht_tax where isp_uid='".$isp_uid."'");
		if($query->num_rows()>0){
			return 1;
		}else{
			return 0;
		}
	}
	public function check_userterminate($sid){
		$userQ = $this->db->query("SELECT id FROM sht_users WHERE id='".$sid."' AND inactivate_type='terminate'");
		if($userQ->num_rows() > 0){
			return 1;
		}else{
			return 0;
		}
	}
	public function setupfor_adduser(){
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$planbillingQ = $this->db->get_where('sht_billing_cycle', array('isp_uid' => $isp_uid));
		$numrows = $planbillingQ->num_rows();
		if($numrows > 0){
			return 1;
		}else{
			return 0;
		}
	}
	public function user_dashboard(){
		$data = array();
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		
		$regiontype = $this->dept_region_type();
		$permicond = ''; $where = '';
		if ($regiontype == "region") {
			$dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='".$dept_id."' AND status='1' AND is_deleted='0'");
			$total_deptregion = $dept_regionQ->num_rows();
			if($dept_regionQ->num_rows() > 0){
				$c = 1;
				foreach($dept_regionQ->result() as $deptobj){
					$stateid = $deptobj->state_id;
					$cityid = $deptobj->city_id;
					$zoneid = $deptobj->zone_id;
					if($cityid == 'all'){
						$permicond .= " (state='".$stateid."') ";
					}elseif($zoneid == 'all'){
						$permicond .= " (state='".$stateid."' AND city='".$cityid."') ";
					}else{
						$permicond .= " (state='".$stateid."' AND city='".$cityid."' AND zone='".$zoneid."') ";
					}
					if($c != $total_deptregion){
						$permicond .= ' OR ';
					}
					$c++;
				}
			}
			$where .= ' AND ('.$permicond.')';
		}
		
		$payoverdue_count = $this->paydue_userids($where);
		if(count($payoverdue_count) > 0){
			$balanceamt = number_format( array_sum($payoverdue_count['balanceamt']) );
		}else{
			$balanceamt = 0;
		}
		$inactive = "SELECT count(id) FROM sht_users WHERE enableuser='0' AND inactivate_type= '' $where AND isp_uid='".$isp_uid."'";
		$lead_enquiry = "SELECT count(id) FROM sht_subscriber WHERE status='1' AND is_deleted='0' AND (usertype='1' OR usertype='2') $where AND isp_uid='".$isp_uid."'";
		$query = $this->db->query("SELECT count(id) as active, ($inactive) as inactive, ($lead_enquiry) as lead_enquiry FROM `sht_users` WHERE ((enableuser='1') OR (enableuser='0' AND inactivate_type != '' AND inactivate_type != 'terminate')) $where AND isp_uid='".$isp_uid."'");
		//echo $this->db->last_query(); die;
		$num_rows = $query->num_rows();

		if($num_rows > 0){
			foreach($query->result() as $userobj){
				$data['active_users'] = $userobj->active;
				$data['inactive_users'] = $userobj->inactive;
				$data['lead_enquiry_users'] = $userobj->lead_enquiry;
				$data['complaints'] = $this->complaints_count();
				$data['payment_dues'] = $balanceamt;
				$data['total_users'] = $userobj->active + $userobj->inactive + $userobj->lead_enquiry;
				$data['otherdues'] = $this->totalotherdues_count();
			}
		}else{
			$data['total_users'] = 0;
			$data['active_users'] = 0;
			$data['inactive_users'] = 0;
			$data['lead_enquiry_users'] = 0;
			$data['complaints'] = 0;
			$data['payment_dues'] = 0;
			$data['otherdues'] = 0;
		}
		//echo '<pre>'; print_r($data); die;
		return $data;
	}
	public function search_user(){
		$data = array();
		$gen = '';
		$total_search = 0;
		$search_user = $this->input->post('search_user');
		
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$regiontype = $this->dept_region_type();
		
		
		$permicond = ''; $sczcond = '';
		if ($regiontype == "region") {
			$dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='".$dept_id."' AND status='1' AND is_deleted='0'");
			$total_deptregion = $dept_regionQ->num_rows();
			if($dept_regionQ->num_rows() > 0){
				$c = 1;
				foreach($dept_regionQ->result() as $deptobj){
					$stateid = $deptobj->state_id;
					$cityid = $deptobj->city_id;
					$zoneid = $deptobj->zone_id;
					if($cityid == 'all'){
						$permicond .= " (state='".$stateid."') ";
					}elseif($zoneid == 'all'){
						$permicond .= " (state='".$stateid."' AND city='".$cityid."') ";
					}else{
						$permicond .= " (state='".$stateid."' AND city='".$cityid."' AND zone='".$zoneid."') ";
					}
					if($c != $total_deptregion){
						$permicond .= ' OR ';
					}
					$c++;
				}
			}
			$sczcond .= ' AND ('.$permicond.')';
		}
		
		$maxcount = 4;
		$ccount = 0;
		
		if($search_user != ''){
			//LEAD & ENQUIRY SEARCH
			$where = " ((username LIKE '%".$search_user."%') OR (first_name LIKE '%".$search_user."%') OR (phone LIKE '%".$search_user."%')) AND status='1' AND is_deleted='0' $sczcond AND isp_uid='".$isp_uid."'";
			$this->db->select('id,first_name,last_name,state,city,zone,phone');
			$this->db->from('sht_subscriber');
			$this->db->where($where);
			$query = $this->db->get();
			$total_search = $query->num_rows();
			if($total_search > 0){
				foreach($query->result() as $sobj){
					$state = $this->getstatename($sobj->state);
					$city = $this->getcityname($sobj->state, $sobj->city);
					$zone = $this->getzonename($sobj->zone);
					$usertype = $sobj->usertype;
					if($usertype == '1' || $usertype == '2'){
						$link = base_url()."user/add/".$sobj->id;
						$user_uuid = ($sobj->usertype == 1) ? 'Lead' : 'Enquiry';
					}else{
						$link = base_url()."user/edit/".$sobj->id;
						$user_uuid = $sobj->useruid;
					}
					$gen .= '<a href="'.$link.'" class="search-dropdown">
						<h5><i class="fa fa-user" aria-hidden="true"></i> &nbsp;'.$sobj->first_name.' <small>'.$sobj->last_name.' ('.$user_uuid.')</small></h5>
						<samp>+91 '.$sobj->phone.', '.$city.', '.$state.', '.$zone.'</samp>
					      </a>';
					
					$ccount++;
					if($maxcount == $ccount){
						break;
					}
				}
			}
			
			//CUSTOMER SEARCH
			$cwhere = " ((username LIKE '%".$search_user."%') OR (firstname LIKE '%".$search_user."%') OR (mobile LIKE '%".$search_user."%')) $sczcond  AND isp_uid='".$isp_uid."' ";
			$this->db->select('id,uid,firstname,lastname,state,city,zone,mobile');
			$this->db->from('sht_users');
			$this->db->where($cwhere);
			$cquery = $this->db->get();
			//echo $this->db->last_query(); die;
			$total_csearch = $cquery->num_rows();
			if(($total_csearch > 0) && ($ccount < 4)){
				foreach($cquery->result() as $sobj){
					$state = $this->getstatename($sobj->state);
					$city = $this->getcityname($sobj->state, $sobj->city);
					$zone = $this->getzonename($sobj->zone);
					$link = base_url()."user/edit/".$sobj->id;
					$user_uuid = $sobj->uid;
					$gen .= '<a href="'.$link.'" class="search-dropdown">
						<h5><i class="fa fa-user" aria-hidden="true"></i> &nbsp;'.$sobj->firstname.' <small>'.$sobj->lastname.' ('.$user_uuid.')</small></h5>
						<samp>+91 '.$sobj->mobile.', '.$city.', '.$state.', '.$zone.'</samp>
					      </a>';
					
					$ccount++;
					if($maxcount == $ccount){
						break;
					}
				}
			}
		}
		
		$data['total_results'] = $total_search + $total_csearch;
		$data['search_results'] = $gen;
		
		echo json_encode($data);
	}
	
	public function customer_tickets_count($userid){
		$query = $this->db->query("SELECT id FROM sht_subscriber_tickets WHERE subscriber_id='".$userid."' AND status='0'");
		$num_rows = $query->num_rows();
		return $num_rows;
	}
	public function viewall_search_results(){
		$data = array();
		$gen = '';
		$total_search = 0;
		$search_user = $this->input->post('search_user');
		
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$regiontype = $this->dept_region_type();
		$ispcodet = $this->countrydetails();
		$cocurrency = $ispcodet['currency'];
		
		$permicond = ''; $sczcond = '';
		if ($regiontype == "region") {
			$dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='".$dept_id."' AND status='1' AND is_deleted='0'");
			$total_deptregion = $dept_regionQ->num_rows();
			if($dept_regionQ->num_rows() > 0){
				$c = 1;
				foreach($dept_regionQ->result() as $deptobj){
					$stateid = $deptobj->state_id;
					$cityid = $deptobj->city_id;
					$zoneid = $deptobj->zone_id;
					if($cityid == 'all'){
						$permicond .= " (state='".$stateid."') ";
					}elseif($zoneid == 'all'){
						$permicond .= " (state='".$stateid."' AND city='".$cityid."') ";
					}else{
						$permicond .= " (state='".$stateid."' AND city='".$cityid."' AND zone='".$zoneid."') ";
					}
					if($c != $total_deptregion){
						$permicond .= ' OR ';
					}
					$c++;
				}
			}
			$sczcond .= ' AND ('.$permicond.')';
		}
		
		$cwhere = " ((username LIKE '%".$search_user."%') OR (uid LIKE '%".$search_user."%') OR (firstname LIKE '%".$search_user."%') OR (mobile LIKE '%".$search_user."%')) $sczcond AND isp_uid='".$isp_uid."' ";
		$where = " ((username LIKE '%".$search_user."%') OR (first_name LIKE '%".$search_user."%') OR (phone LIKE '%".$search_user."%')) AND status='1' AND is_deleted='0' $sczcond  AND isp_uid='".$isp_uid."' ";
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$zone = $this->input->post('zone');
		$locality = $this->input->post('locality');
		if(isset($state) && $state != ''){
			$where .= " AND state='".$state."'";
			$cwhere .= " AND state='".$state."'";
		}
		if(isset($city) && $city != ''){
			$where .= " AND city='".$city."'";
			$cwhere .= " AND city='".$city."'";
		}
		if(isset($zone) && $zone != ''){
			$where .= " AND zone='".$zone."'";
			$cwhere .= " AND zone='".$zone."'";
		}
		if(isset($locality) && $locality != ''){
			$where .= " AND usuage_locality='".$locality."'";
			$cwhere .= " AND usuage_locality='".$locality."'";
		}
		
		$this->db->select('id,usertype,first_name,last_name,state,city,zone,status');
		$this->db->from('sht_subscriber');
		$this->db->where($where);
		$query = $this->db->get();
		$total_search = $query->num_rows();
		if($total_search > 0){
			$i = 1;
			foreach($query->result() as $sobj){
				$state = $this->getstatename($sobj->state);
				$city = $this->getcityname($sobj->state, $sobj->city);
				$zone = $this->getzonename($sobj->zone);
				$status = ($sobj->status == 1)?'active':'inactive';
				$user_uuid = ($sobj->usertype == 1) ? 'Lead' : 'Enquiry';
				
				$gen .= '
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>'.$user_uuid.'</td>
					<td><a href="'.base_url().'user/add/'.$sobj->id.'">'.$sobj->first_name.' '.$sobj->last_name.'</a></td>
					<td>'.$state.'</td>
					<td>'.$city.'</td>
					<td>'.$zone.'</td>
					<td> - </td>
					<td> - </td>
					<td> - </td>
					<td> - </td>
					<td>'.$status.'</td>
					<td><span>'.$cocurrency.'</span> 0</td>
					<td class="mui--text-right">0</td>
				</tr>
				';
				$i++;
			}
		}
		
		$this->db->select('id,uid,enableuser,firstname,lastname,state,city,zone,account_activated_on,paidtill_date,expiration');
		$this->db->from('sht_users');
		$this->db->where($cwhere);
		$cquery = $this->db->get();
		$total_csearch = $cquery->num_rows();
		if($total_csearch > 0){
			$i = 1;
			foreach($cquery->result() as $sobj){
				$ticket_count = $this->customer_tickets_count($sobj->id);
				$state = $this->getstatename($sobj->state);
				$city = $this->getcityname($sobj->state, $sobj->city);
				$zone = $this->getzonename($sobj->zone);
				$status = ($sobj->enableuser == 1)?'active':'inactive';
				$activeplan = $this->userassoc_planname($sobj->id);
				$active_from = ($sobj->account_activated_on != '0000-00-00 00:00:00') ? date('d-m-Y', strtotime($sobj->account_activated_on)) : '0000-00-00';
				$paidtill_date = ($sobj->paidtill_date != '0000-00-00') ? date('d-m-Y', strtotime($sobj->paidtill_date)) : '0000-00-00';
				$expiration = $sobj->expiration;
				if($expiration == '0000-00-00 00:00:00'){ $expiration = '-'; }
				else{ $expiration = date('d-m-Y', strtotime($sobj->expiration)); }
				
				$gen .= '
				<tr>
					<td>&nbsp;</td>
					<td>
					   <div class="checkbox" style="margin-top:0px; margin-bottom:0px;">
					      <label>
					      <input type="checkbox" class="collapse_checkbox" value="'.$sobj->id.'"> 
					      </label>
					   </div>
					</td>
					<td><a href="'.base_url().'user/edit/'.$sobj->id.'">'.$sobj->uid.'</a></td>
					<td>'.$sobj->firstname.' '.$sobj->lastname.'</td>
					<td>'.$state.'</td>
					<td>'.$city.'</td>
					<td>'.$zone.'</td>
					<td>'.$activeplan.'</td>
					<td>'.$active_from.'</td>
					<td>'.$paidtill_date.'</td>
					<td>'.$expiration.'</td>
					<td>'.$status.'</td>
					<td><span>'.$cocurrency.'</span> 0</td>
					<td class="mui--text-right">'.$ticket_count.'</td>
				</tr>
				';
				$i++;
			}
		}
		
		$total_results = $total_search + $total_csearch;
		if($total_results == 0){
			$gen .= "<tr><td style='text-align:center' colspan='10'> No Result Found !!</td></tr>";
		}
		
		$data['total_results'] = $total_results;
		$data['search_results'] = $gen;
		
		echo json_encode($data);
	}
	
	public function check_user_onlineoffline($uuid){
		$onoffQ = $this->db->query("SELECT radacctid FROM radacct WHERE username='".$uuid."' AND acctstoptime IS NULL ORDER BY radacctid DESC LIMIT 1");
		if($onoffQ->num_rows() > 0){
			return 'online';
		}else{
			return 'offline';
		}
	}
	public function check_user_profilestatus($uuid){
		$data = array();
		$upstatQ = $this->db->query("SELECT inactivate_type,suspended_days FROM sht_users WHERE uid='".$uuid."' AND inactivate_type != ''");
		if($upstatQ->num_rows() > 0){
			$rowdata = $upstatQ->row();
			$data['inactivate_type'] = $rowdata->inactivate_type;
			$data['suspended_days'] = $rowdata->suspended_days;
		}
		return $data;
	}
	public function postfup_userslisting(){
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$regiontype = $this->dept_region_type();
		
		$data = array();
		$gen = '';
		$total_search = 0;
		$cwhere = '1';
		$search_user = $this->input->post('search_user');
		//$searchtype = $this->input->post('searchtype');
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$zone = $this->input->post('zone');
		$locality = $this->input->post('locality');
		$netstatus = $this->input->post('netstatus');
		if(isset($state) && $state != ''){
			$cwhere .= " AND state='".$state."'";
		}
		if(isset($city) && $city != ''){
			$cwhere .= " AND city='".$city."'";
		}
		if(isset($zone) && $zone != ''){
			$cwhere .= " AND zone='".$zone."'";
		}
		if(isset($locality) && $locality != ''){
			$cwhere .= " AND usuage_locality='".$locality."'";
		}
		
		if(isset($search_user)){
			$cwhere .= " AND ((username LIKE '%".$search_user."%') OR (uid LIKE '%".$search_user."%') OR (firstname LIKE '%".$search_user."%') OR (mobile LIKE '%".$search_user."%')) ";
		}
		
		$permicond = '';
		if ($regiontype == "region") {
			$dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='".$dept_id."' AND status='1' AND is_deleted='0'");
			$total_deptregion = $dept_regionQ->num_rows();
			if($dept_regionQ->num_rows() > 0){
				$c = 1;
				foreach($dept_regionQ->result() as $deptobj){
					$stateid = $deptobj->state_id;
					$cityid = $deptobj->city_id;
					$zoneid = $deptobj->zone_id;
					if($cityid == 'all'){
						$permicond .= " (state='".$stateid."') ";
					}elseif($zoneid == 'all'){
						$permicond .= " (state='".$stateid."' AND city='".$cityid."') ";
					}else{
						$permicond .= " (state='".$stateid."' AND city='".$cityid."' AND zone='".$zoneid."') ";
					}
					if($c != $total_deptregion){
						$permicond .= ' OR ';
					}
					$c++;
				}
			}
			$cwhere .= ' AND ('.$permicond.')';
		}
		$cwhere .= " AND (enableuser='1' OR (enableuser='0' AND inactivate_type != '' AND inactivate_type != 'terminate' ))";
		
		$uonline = 0; $uoffline = 0; $fsucc = 0;
		$qlmt = 100; $qofset = 0;
		$limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
		
		$offlineuids = array(); $onlineuids = array();
		/************ FOR HEADER COUNT ************/
		$countQ = $this->db->query("SELECT uid FROM sht_users WHERE $cwhere AND isp_uid='".$isp_uid."'");
		$result_count = $countQ->num_rows();
		if($result_count > 0){
			foreach($countQ->result() as $cobj){
				$user_radactstat = $this->check_user_onlineoffline($cobj->uid);
				if($user_radactstat == 'online'){
					$uonline++;
					$onlineuids[] = $cobj->uid;
				}elseif($user_radactstat == 'offline'){
					$uoffline++;
					$offlineuids[] = $cobj->uid;
				}
			}
		}
		$ispcodet = $this->countrydetails();
		$gen = ''; $ispostfup = 0;
		$cquery = $this->db->query("SELECT tb1.id, tb1.uid, tb1.enableuser, tb1.firstname, tb1.lastname, tb1.state, tb1.city, tb1.zone, tb1.account_activated_on, tb1.paidtill_date, tb1.expiration, tb1.usuage_locality, tb1.inactivate_type, tb1.mobile, tb1.user_plan_type, tb1.plan_activated_date, tb1.next_bill_date, tb2.plantype, tb2.datalimit, tb2.datacalc FROM sht_users as tb1 LEFT JOIN sht_services as tb2 ON(tb1.baseplanid = tb2.srvid) WHERE $cwhere AND tb1.isp_uid='".$isp_uid."' AND tb2.plantype='3' AND tb1.inactivate_type != 'terminate' LIMIT $qofset,$qlmt");
		//echo $this->db->last_query(); die;
		$total_csearch = $cquery->num_rows();
		if($total_csearch > 0){
			foreach($cquery->result() as $sobj){
				
				$live_usage =0;
				$percent = 0;
				$topupdata = 0; $topup_allocated = 0; $topup_added_on = ''; $topup_usuage = 0;
				$framedip = '-';
				
				$start_date = date('Y-m-d', strtotime($sobj->plan_activated_date));
				$end_date = date('Y-m-d', strtotime($sobj->next_bill_date));
				$useruid = $sobj->uid;
				$datacalc = $sobj->datacalc;
				
				// get topup limit
				$get_topup = $this->db->query("select ss.plantype, ss.datalimit, su.added_on from sht_usertopupassoc as su inner join sht_services as ss on(su.topup_id = ss.srvid) where su.uid = '$useruid' AND su.status='1' AND su.terminate='0' AND su.topuptype='1'");
				if($get_topup->num_rows() > 0){
					foreach($get_topup->result() as $tobj){
						$topupdata += $tobj->datalimit;
						$topup_allocated += $tobj->datalimit;
					}
				}
				
				$state = $this->getstatename($sobj->state);
				$city = $this->getcityname($sobj->state, $sobj->city);
				$zone = $this->getzonename($sobj->zone);
				$status = $this->check_user_onlineoffline($sobj->uid);
				$activeplan = $this->userassoc_planname($sobj->id);
				$user_plan_type = $sobj->user_plan_type;
				$active_from = ($sobj->account_activated_on != '0000-00-00 00:00:00') ? date('d-m-Y', strtotime($sobj->account_activated_on)) : '0000-00-00';
				$paidtill_date = ($sobj->paidtill_date != '0000-00-00') ? date('d-m-Y', strtotime($sobj->paidtill_date)) : '0000-00-00';
				$expiration = $sobj->expiration;
				if($expiration == '0000-00-00 00:00:00'){ $expiration = '-'; }
				else{ $expiration = date('d-m-Y', strtotime($sobj->expiration)); }
				$wallet = $this->userbalance_amount($sobj->uid);
				
				$wstyle= ''; $onoff_style = ''; $tstyle = ''; $profstyle = '';
				if($wallet < 0){
					$wstyle = "<a href='javascript:void()' style='color:#f00; text-decoration: underline;' onclick='migrate_tobilling(".$sobj->id.")' target='_blank'>".$ispcodet['currency']." ".$wallet."</a>";
				}elseif(($wallet > 0) && ($wallet != 0)){
					$wstyle = "<a href='javascript:void()' style='color:#439B46; text-decoration: underline;' onclick='migrate_tobilling(".$sobj->id.")' target='_blank'>".$ispcodet['currency']." ".$wallet."</a>";
				}else{
					$wstyle = $ispcodet['currency']." ".$wallet;
				}
				if($status == 'online'){
                                        $logout_router = "<a href='javascript:void(0)' onclick='routerlogout(&apos;".$useruid."&apos;)' style='color:#000'><i class='fa fa-sign-out' aria-hidden='true'></i></a>";
					$onoff_style = "style='color:#439B46'";
				}elseif($status == 'offline'){
					$logout_router = "";
					$onoff_style = "style='color:#f00'";
				}
				
				$plandata_allocated = $sobj->datalimit;
				$data_allocated = $sobj->datalimit + $topupdata;
				
				$query = $this->db->query("select framedipaddress, acctinputoctets, acctoutputoctets from radacct where username = '$useruid' AND DATE(acctstarttime) between '$start_date' and '$end_date'");
				foreach($query->result() as $row){
					$framedip = $row->framedipaddress;
					if($datacalc == 2){
						$live_usage = $live_usage + $row->acctinputoctets + $row->acctoutputoctets;
					}else{
						$live_usage = $live_usage + $row->acctoutputoctets;
					}
				}
				
				$live_usage_value = round($live_usage/(1024*1024*1024),2);
				if($live_usage_value < 1){
					//get in mb
					$live_usage_value = round($live_usage/(1024*1024),2);
					if($live_usage_value < 1){
						//get in kb
						$live_usage_value = round($live_usage/(1024),2)."KB";
					}else{
						$live_usage_value = $live_usage_value."MB" ;
					}
				}else{
					$live_usage_value = $live_usage_value."GB";
				}
				
				$plandata_allocated_value = round(($plandata_allocated/(1024*1024*1024)),2)."GB";
				if($live_usage > $plandata_allocated){
					$ispostfup++;
					
					$gen .= "<tr><td>&nbsp;</td><td><div class='checkbox' style='margin-top:0px; margin-bottom:0px;'><label><input type='checkbox' name='morefilteruids' class='collapse_checkbox' value='".$sobj->id."'></label></div></td><td><a href='".base_url()."user/edit/".$sobj->id."' ".$onoff_style." target='_blank'>".$sobj->uid."</a></td><td>".$user_plan_type."</td><td>".$sobj->firstname." ".$sobj->lastname."</td><td>".$city."</td><td>".$activeplan."</td><td>".$plandata_allocated_value."</td><td>".$live_usage_value."</td><td>".$active_from."</td><td>".$expiration."</td><td ".$onoff_style.">".ucfirst($status)."</td><td>".$logout_router."</td><td>".$framedip."</td><td>".$wstyle."</td><td class='mui--text-left'>".$tstyle."</td></tr>";
				}
			}
		}
		
		if($ispostfup == 0){
			$gen .= "<tr><td style='text-align:center' colspan='15'> No Result Found !!</td></tr>";
		}
		
		$data['online_users'] = $uonline;
		$data['offline_users'] = $uoffline;
		$data['total_results'] = $result_count;
		$data['search_results'] = $gen;
		$data['qofset_total_csearch'] = $qofset.'@'.$total_csearch.'@'.$result_count;
		/*if(($qofset + $total_csearch) == $result_count){
			$data['loadmore'] = 0;
		}else{
			$data['loadmore'] = 1;
		}*/
		$data['loadmore'] = 0;
		$data['limit'] = $qlmt;
		$data['offset'] = $qlmt+$qofset;
		
		return $data;
	}
	
	
	public function getuser_plandata($useruid){
		$monthly_data = 0;
		$get_plan = $this->db->query("select su.uid, ss.plantype, ss.datalimit, ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$useruid' AND su.baseplanid != '0'");
		if($get_plan->num_rows() > 0){
			$get_plan_row = $get_plan->row_array();
			if($get_plan_row['plantype'] == '1'){
                                $monthly_data = 'UL';
			}else{				
				$monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";	
			}
		}
		
                return $monthly_data;
	}
	
	public function radacct_dataused($useruid){
		$data = array();
		$live_usage = 0;
		$get_plan = $this->db->query("select su.uid, ss.plantype, ss.datalimit, ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$useruid' AND su.baseplanid != '0'");
		$get_billing_date = $this->db->query("select plan_activated_date, next_bill_date from sht_users where uid = '$useruid'");

		if($get_billing_date->num_rows() > 0){
			$row_billing_date = $get_billing_date->row_array();
			$start_date = date('Y-m-d', strtotime($row_billing_date['plan_activated_date']));
			$end = $row_billing_date['next_bill_date'];
			$end_date = date('Y-m-d', strtotime($row_billing_date['next_bill_date']));
		}
		$query = $this->db->query("select acctinputoctets, acctoutputoctets from radacct where username = '$useruid' AND DATE(acctstarttime) between '$start_date' and '$end_date'");
		foreach($query->result() as $row){
			$get_plan_row = $get_plan->row_array();
			$datacalc = $get_plan_row['datacalc'];
			if(($datacalc == 2) || ($datacalc == 0)){
				$live_usage = $live_usage + $row->acctinputoctets + $row->acctoutputoctets;
			}else{
				$live_usage = $live_usage + $row->acctoutputoctets;
			}
		}

		$live_usage_value = round($live_usage/(1024*1024*1024),2);
		if($live_usage_value < 1){
			//get in mb
			$live_usage_value = round($live_usage/(1024*1024),2);
			if($live_usage_value < 1){
				//get in kb
				$live_usage_value = round($live_usage/(1024),2)."KB";
			}else{
				$live_usage_value = $live_usage_value."MB" ;
			}
		}else{
			$live_usage_value = $live_usage_value."GB";
		}
		
		$frameipQuery = $this->db->query("select framedipaddress from radacct where username = '$useruid' ORDER BY radacctid DESC LIMIT 1");
                if($frameipQuery->num_rows() > 0){
                    $frowdata = $frameipQuery->row();
                    $data['framedip'] = $frowdata->framedipaddress;
                }else{
                    $data['framedip'] = '-';
                }
		
		$data['live_usage'] = $live_usage_value;
		
		return $data;
	}	

	public function active_inactive_userslist($searchtype){
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$regiontype = $this->dept_region_type();
		
		$data = array();
		$gen = '';
		$total_search = 0;
		$cwhere = '1';
		$search_user = $this->input->post('search_user');
		//$searchtype = $this->input->post('searchtype');
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$zone = $this->input->post('zone');
		$locality = $this->input->post('locality');
		$netstatus = $this->input->post('netstatus');
		
		if(isset($state) && $state != ''){
			$cwhere .= " AND state='".$state."'";
		}
		if(isset($city) && $city != ''){
			$cwhere .= " AND city='".$city."'";
		}
		if(isset($zone) && $zone != ''){
			$cwhere .= " AND zone='".$zone."'";
		}
		if(isset($locality) && $locality != ''){
			$cwhere .= " AND usuage_locality='".$locality."'";
		}
		
		if(isset($search_user)){
			$cwhere .= " AND ((username LIKE '%".$search_user."%') OR (uid LIKE '%".$search_user."%') OR (firstname LIKE '%".$search_user."%') OR (mobile LIKE '%".$search_user."%')) ";
		}
		if(isset($searchtype) && $searchtype == 'inact'){
			$cwhere .= " AND enableuser='0' AND inactivate_type = '' ";
		}
		if(isset($searchtype) && $searchtype == 'act'){
			if($netstatus != 'terminate'){
				$cwhere .= " AND (enableuser='1' OR (enableuser='0' AND inactivate_type != '' AND inactivate_type != 'terminate' ))";
			}else{
				$cwhere .= " AND enableuser='0' AND inactivate_type = 'terminate'";
			}
		}
		
		
		$orderby = '';
		$sortlistby = $this->input->post('sortby');
		if(isset($sortlistby) && $sortlistby != ''){
			$asc_dsc = $this->input->post('orderby');
			if($sortlistby == 'username'){
				$orderby = ' ORDER BY username '.$asc_dsc;
			}elseif($sortlistby == 'newusers'){
				$orderby = ' ORDER BY account_activated_on '.$asc_dsc;
			}elseif($sortlistby == 'fullname'){
				$orderby = ' ORDER BY firstname '.$asc_dsc;
			}
		}
		
		$permicond = '';
		if ($regiontype == "region") {
			$dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='".$dept_id."' AND status='1' AND is_deleted='0'");
			$total_deptregion = $dept_regionQ->num_rows();
			if($dept_regionQ->num_rows() > 0){
				$c = 1;
				foreach($dept_regionQ->result() as $deptobj){
					$stateid = $deptobj->state_id;
					$cityid = $deptobj->city_id;
					$zoneid = $deptobj->zone_id;
					if($cityid == 'all'){
						$permicond .= " (state='".$stateid."') ";
					}elseif($zoneid == 'all'){
						$permicond .= " (state='".$stateid."' AND city='".$cityid."') ";
					}else{
						$permicond .= " (state='".$stateid."' AND city='".$cityid."' AND zone='".$zoneid."') ";
					}
					if($c != $total_deptregion){
						$permicond .= ' OR ';
					}
					$c++;
				}
			}
			$cwhere .= ' AND ('.$permicond.')';
		}
		
		$uonline = 0; $uoffline = 0; $fsucc = 0;
		$qlmt = 50; $qofset = 0;
		$limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
		
		$todaydate = date('Y-m-d');
		$expireduids = array();
		$offlineuids = array(); $onlineuids = array(); $suspenduids = array(); $terminateuids = array();
		/************ FOR HEADER COUNT ************/
		$countQ = $this->db->query("SELECT uid,inactivate_type,expiration FROM sht_users WHERE $cwhere AND isp_uid='".$isp_uid."' $orderby");
		//echo $this->db->last_query(); die;
		$result_count = $countQ->num_rows();
		if($result_count > 0){
			foreach($countQ->result() as $cobj){
				$inactivate_type = $cobj->inactivate_type;
				if($inactivate_type == 'suspended'){
					$suspenduids[] = $cobj->uid;
				}elseif($inactivate_type == 'terminate'){
					$terminateuids[] = $cobj->uid;
				}
				$expiration = date('Y-m-d', strtotime($cobj->expiration));
				if(strtotime($todaydate) > strtotime($expiration)){
					$expireduids[] = $cobj->uid;
				}
				
				$user_radactstat = $this->check_user_onlineoffline($cobj->uid);
				if($user_radactstat == 'online'){
					$uonline++;
					$onlineuids[] = $cobj->uid;
				}elseif($user_radactstat == 'offline'){
					$uoffline++;
					$offlineuids[] = $cobj->uid;
				}
			}
		}
		/*****************************************/
		$filteruids = '';
		if($netstatus == 'online'){
			$filteruids = " AND uid IN ('". implode("','", $onlineuids) ."')" ;
		}elseif($netstatus == 'offline'){
			$filteruids = " AND uid IN ('". implode("','", $offlineuids) ."')" ;
		}elseif($netstatus == 'suspended'){
			$filteruids = " AND uid IN ('". implode("','", $suspenduids) ."')" ;
		}elseif($netstatus == 'terminate'){
			$filteruids = " AND uid IN ('". implode("','", $terminateuids) ."')" ;
		}elseif($netstatus == 'expired'){
			$filteruids = " AND uid IN ('". implode("','", $expireduids) ."')" ;
		}
		$ispcodet = $this->countrydetails();
		
		$cquery = $this->db->query("SELECT id, uid, enableuser, firstname, lastname, state, city, zone, account_activated_on, paidtill_date, expiration, usuage_locality, inactivate_type,mobile,user_plan_type FROM sht_users WHERE $cwhere AND isp_uid='".$isp_uid."' $filteruids $orderby LIMIT $qofset,$qlmt");
		//echo $this->db->last_query(); die;
		$total_csearch = $cquery->num_rows();
		if($total_csearch > 0){
			$i = 1;
			foreach($cquery->result() as $sobj){
				$keypwd = '';
				$cust_uuid = $sobj->uid;
				/*$radchkQ = $this->db->query("SELECT value FROM radcheck WHERE username='".$cust_uuid."' AND attribute = 'Cleartext-Password'");
				if($radchkQ->num_rows() > 0){
					$keypwd = "<br/><i class='fa fa-key' aria-hidden='true'></i>&nbsp;".$radchkQ->row()->value;
				}*/
				
				//$user_plan_dataArr = $this->live_usage($cust_uuid);
                                //$monthly_datalimit = $user_plan_dataArr['monthly_datalimit'];
                                //$data_used = $user_plan_dataArr['live_usage'];
                                //$framedip = $user_plan_dataArr['framedip'];
				
                                $monthly_datalimit = $this->getuser_plandata($cust_uuid);
				//$user_plan_dataArr = $this->radacct_dataused($cust_uuid);
                                $data_used = '';
                                $framedip = '';
                                $logout_router = '';
                                
				$state = $this->getstatename($sobj->state);
				$city = $this->getcityname($sobj->state, $sobj->city);
				$zone = $this->getzonename($sobj->zone);
				//$status = ($sobj->enableuser == 1)?'active':'inactive';
				$activeplan = $this->userassoc_planname($sobj->id);
				$user_plan_type = $sobj->user_plan_type;
				
				$expiration = $sobj->expiration;
				if($expiration == '0000-00-00 00:00:00'){ $expiration = '-'; }
				else{ $expiration = date('d-m-Y', strtotime($sobj->expiration)); }
				$wallet = $this->userbalance_amount($sobj->uid);
				
				$wstyle= ''; $onoff_style = ''; $tstyle = ''; $profstyle = '';
				if($wallet < 0){
					$wstyle = "<a href='javascript:void()' style='color:#f00; text-decoration: underline;' onclick='migrate_tobilling(".$sobj->id.")' target='_blank'>".$ispcodet['currency']." ".$wallet."</a>";
				}elseif(($wallet > 0) && ($wallet != 0)){
					$wstyle = "<a href='javascript:void()' style='color:#439B46; text-decoration: underline;' onclick='migrate_tobilling(".$sobj->id.")' target='_blank'>".$ispcodet['currency']." ".$wallet."</a>";
				}else{
					$wstyle = $ispcodet['currency']." ".$wallet;
				}
				
				//$profilestat = $this->check_user_profilestatus($sobj->uid);
				$status = $this->check_user_onlineoffline($sobj->uid);
				if($status == 'online'){
					//$uonline++;
                                        $logout_router = "<a href='javascript:void(0)' onclick='routerlogout(&apos;".$cust_uuid."&apos;)' style='color:#000'><i class='fa fa-sign-out' aria-hidden='true'></i></a>";
					$onoff_style = "style='color:#439B46'";
				}elseif($status == 'offline'){
					//$uoffline++;
					$onoff_style = "style='color:#f00'";
				}
				
				if($sobj->inactivate_type == 'suspended'){
					$status = 'suspended';
					$onoff_style = "style='color:#2D9EE0'";
				}elseif($sobj->inactivate_type == 'terminate'){
					$status = 'terminate';
					$onoff_style = "style='color:#D28C32; text-decoration:underline;'";
				}

				$expiration = date('Y-m-d', strtotime($sobj->expiration));
				if(strtotime($todaydate) > strtotime($expiration)){
					$status = 'expired';
				}
				
				$activatedon = date('d-m-Y', strtotime($sobj->account_activated_on));
				$ticket_count = $this->customer_tickets_count($sobj->id);
				if($ticket_count > 0){
					$tstyle = "<a href='javascript:void()' style='color:#f00; text-decoration: underline;' onclick='migrate_totickets(".$sobj->id.")'>".$ticket_count."</a>";
				}else{
					$tstyle = $ticket_count;
				}
				
				if(isset($netstatus) && ($netstatus != '')){
					if($netstatus == $status){
						if($status == 'suspended'){
							$status = 'suspended';
						}
						if($status == 'terminate'){
                                                        $gen .= "<tr><td>&nbsp;</td><td>&nbsp;</td><td><a href='".base_url()."user/edit/".$sobj->id."' ".$onoff_style." target='_blank'>".$sobj->uid."</a>".$keypwd."</td><td>".$user_plan_type."</td><td>".$sobj->firstname." ".$sobj->lastname."</td><td>".$city."</td><td>".$activeplan."</td><td>".$monthly_datalimit."</td><td>".$data_used."</td><td>".$activatedon."</td><td>".$expiration."</td><td style='color:#D28C32;'>".ucfirst($status)."</td><td>".$logout_router."</td><td>".$framedip."</td><td>".$wstyle."</td><td class='mui--text-left'>".$tstyle."</td></tr>";
						}else{
							$gen .= "<tr><td>&nbsp;</td><td><div class='checkbox' style='margin-top:0px; margin-bottom:0px;'><label><input type='checkbox' name='morefilteruids' class='collapse_checkbox' value='".$sobj->id."'></label></div></td><td><a href='".base_url()."user/edit/".$sobj->id."' ".$onoff_style." target='_blank'>".$sobj->uid."</a>".$keypwd."</td><td>".$user_plan_type."</td><td>".$sobj->firstname." ".$sobj->lastname."</td><td>".$city."</td><td>".$activeplan."</td><td>".$monthly_datalimit."</td><td>".$data_used."</td><td>".$activatedon."</td><td>".$expiration."</td><td ".$onoff_style.">".ucfirst($status)."</td><td>".$logout_router."</td><td>".$framedip."</td><td>".$wstyle."</td><td class='mui--text-left'>".$tstyle."</td></tr>";
						}
						$fsucc++;
						$i++;
					}else{
						continue;
					}
				}else{
					
					if(isset($searchtype) && $searchtype == 'inact'){
						$trash_inactiveUser = '';
						if($superadmin == 1){
							$trash_inactiveUser = "<a href='javascript:void(0)' style='padding-right:20px;float:right;' onclick='delete_inactiveUser(".$sobj->id.")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
						}
						$pendingdoc = '';
						$activQ = $this->db->order_by('id', 'DESC')->limit(1)->get_where('sht_subscriber_activation_panel', array('subscriber_id' => $sobj->id));
						if($activQ->num_rows() > 0){
							$activeQrow = $activQ->row();
							$personal_details = $activeQrow->personal_details;
							$kyc_details = $activeQrow->kyc_details;
							$plan_details = $activeQrow->plan_details;
							$installation_details = $activeQrow->installation_details;
							$security_details = $activeQrow->security_details;
							
							if(($personal_details == 1) && ($kyc_details == 1) && ($plan_details == 1) && ($installation_details == 1) && ($security_details == 1)){
								$pendingdoc = "<td style='text-align:center;'>-</td>";
							}else{
								$pendingdoc = "<td style='text-align:center;cursor:pointer' onclick='inactiveuser_pendingdocs($sobj->id)'><i class='fa fa-book' aria-hidden='true'></i></td>";
							}
						}

						$gen .= "<tr><td>&nbsp;</td><td><div class='checkbox' style='margin-top:0px; margin-bottom:0px;'><label><input type='checkbox' class='collapse_checkbox' value='".$sobj->id."'></label></div></td><td><a href='".base_url()."user/edit/".$sobj->id."' ".$onoff_style." target='_blank'>".$sobj->uid."</a>".$keypwd."</td><td>".$sobj->firstname." ".$sobj->lastname."</td><td>".$sobj->mobile."</td><td>".$state."</td><td>".$city."</td><td>".$zone."</td><td>".$activeplan."</td>".$pendingdoc."<td>".$wstyle."</td><td class='mui--text-left'>".$tstyle." ".$trash_inactiveUser."</td></tr>";

					}else{
						if($status == 'terminate'){
							$gen .= "<tr><td>&nbsp;</td><td>&nbsp;</td><td ".$onoff_style.">".$sobj->uid."".$keypwd."</td><td>".$user_plan_type."</td><td>".$sobj->firstname." ".$sobj->lastname."</td><td>".$city."</td><td>".$activeplan."</td><td>".$monthly_datalimit."</td><td>".$data_used."</td><td>".$activatedon."</td><td>".$expiration."</td><td ".$onoff_style.">".ucfirst($status)."</td><td>".$logout_router."</td><td>".$framedip."</td><td>".$wstyle."</td><td class='mui--text-left'>".$tstyle."</td></tr>";
						}else{
						    $gen .= "<tr><td>&nbsp;</td><td><div class='checkbox' style='margin-top:0px; margin-bottom:0px;'><label><input type='checkbox' name='morefilteruids' class='collapse_checkbox' value='".$sobj->id."'></label></div></td><td><a href='".base_url()."user/edit/".$sobj->id."' ".$onoff_style." target='_blank'>".$sobj->uid."</a>".$keypwd."</td><td>".$user_plan_type."</td><td>".$sobj->firstname." ".$sobj->lastname."</td><td>".$city."</td><td>".$activeplan."</td><td>".$monthly_datalimit."</td><td>".$data_used."</td><td>".$activatedon."</td><td>".$expiration."</td><td ".$onoff_style.">".ucfirst($status)."</td><td>".$logout_router."</td><td>".$framedip."</td><td>".$wstyle."</td><td class='mui--text-left'>".$tstyle."</td></tr>";
                                                
						}
					}
					$i++;
				}
			}
		}
		
		if($total_csearch == 0){
			$gen .= "<tr><td style='text-align:center' colspan='14'> No Result Found !!</td></tr>";
		}
		if(isset($netstatus) && ($netstatus != '') && ($fsucc == 0)){
			$gen .= "<tr><td style='text-align:center' colspan='14'> No Result Found !!</td></tr>";
		}
		
		$data['online_users'] = $uonline;
		$data['offline_users'] = $uoffline;
		$data['total_results'] = $result_count;
		$data['search_results'] = $gen;
		$data['qofset_total_csearch'] = $qofset.'@'.$total_csearch.'@'.$result_count;
		if($total_csearch == '0'){
			$data['loadmore'] = 0;
		}else{
			$data['loadmore'] = 1;
		}
		$data['limit'] = $qlmt;
		$data['offset'] = $qlmt+$qofset;
		
		return $data;
	}
	
	public function advanced_search(){
		$state = $this->input->post('advsearch_state');
		$city = $this->input->post('advsearch_city');
		$zone = $this->input->post('advsearch_zone');
		$locality = $this->input->post('advsearch_locality');
		$category = $this->input->post('advsearch_category');
		
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		
		$ispcodet = $this->countrydetails();
		$cocurrency = $ispcodet['currency'];
		
		$regiontype = $this->dept_region_type();
		$permicond = ''; $sczcond = '';
		if ($regiontype == "region") {
			$dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='".$dept_id."' AND status='1' AND is_deleted='0'");
			$total_deptregion = $dept_regionQ->num_rows();
			if($dept_regionQ->num_rows() > 0){
				$c = 1;
				foreach($dept_regionQ->result() as $deptobj){
					$stateid = $deptobj->state_id;
					$cityid = $deptobj->city_id;
					$zoneid = $deptobj->zone_id;
					if($cityid == 'all'){
						$permicond .= " (state='".$stateid."') ";
					}elseif($zoneid == 'all'){
						$permicond .= " (state='".$stateid."' AND city='".$cityid."') ";
					}else{
						$permicond .= " (state='".$stateid."' AND city='".$cityid."' AND zone='".$zoneid."') ";
					}
					if($c != $total_deptregion){
						$permicond .= ' OR ';
					}
					$c++;
				}
			}
			$sczcond .= ' AND ('.$permicond.')';
		}
		
		$where = ''; $gen = '';
		$total_search = 0;
		$search_user = $this->input->post('search_user');
		if(isset($state) && $state != ''){
			$where .= " AND state='".$state."'";
		}
		if(isset($city) && $city != ''){
			$where .= " AND city='".$city."'";
		}
		if(isset($zone) && $zone != ''){
			$where .= " AND zone='".$zone."'";
		}
		if(isset($locality) && $locality != ''){
			$where .= " AND usuage_locality='".$locality."'";
		}
		if(isset($category) && $category != '' && $category != '3'){
			$where .= " AND usertype='".$category."'";
		}
		
		$data['state'] = $state;
		$data['city'] = $city;
		$data['zone'] = $zone;
		$data['locality'] = $locality;
		
		$query = $this->db->query("SELECT * FROM sht_subscriber WHERE status='1' AND is_deleted='0' AND ((username LIKE '%".$search_user."%') OR (first_name LIKE '%".$search_user."%') OR (phone LIKE '%".$search_user."%')) $sczcond $where AND isp_uid='".$isp_uid."' ");
		$total_search = $query->num_rows();
		if($total_search > 0){
			$i = 1;
			foreach($query->result() as $sobj){
				$state = $this->getstatename($sobj->state);
				$city = $this->getcityname($sobj->state, $sobj->city);
				$zone = $this->getzonename($sobj->zone);
				$status = ($sobj->status == 1)?'active':'inactive';
				
				$gen .= '
				<tr>
					<td>&nbsp;</td>
					<td>
					   <div class="checkbox" style="margin-top:0px; margin-bottom:0px;">
					      <label>
					      <input type="checkbox" class="collapse_checkbox" value="'.$sobj->id.'"> 
					      </label>
					   </div>
					</td>
					<td><a href="'.base_url().'user/edit/'.$sobj->id.'" target="_blank">'.$sobj->first_name.'</a></td>
					<td>'.$sobj->first_name.' '.$sobj->last_name.'</td>
					<td>'.$state.'</td>
					<td>'.$city.'</td>
					<td>'.$zone.'</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>'.$status.'</td>
					<td><span>'.$cocurrency.'</span> 0</td>
					<td class="mui--text-right">0</td>
				</tr>
				';
				$i++;
			}
		}
		
		$total_csearch = 0;
		if($category == '3'){
			$cquery = $this->db->query("SELECT id, uid, enableuser, firstname, lastname, state, city, zone, account_activated_on, paidtill_date, expiration FROM sht_users WHERE enableuser='1' AND expired='0' AND ((username LIKE '%".$search_user."%') OR (firstname LIKE '%".$search_user."%') OR (mobile LIKE '%".$search_user."%')) $sczcond $where  AND isp_uid='".$isp_uid."'");
			$total_csearch = $cquery->num_rows();
			if($total_csearch > 0){
				$i = 1;
				foreach($cquery->result() as $sobj){
					$ticket_count = $this->customer_tickets_count($sobj->id);
					$state = $this->getstatename($sobj->state);
					$city = $this->getcityname($sobj->state, $sobj->city);
					$zone = $this->getzonename($sobj->zone);
					$status = ($sobj->enableuser == 1)?'active':'inactive';
					$activeplan = $this->userassoc_planname($sobj->id);
					$active_from = date('d-m-Y', strtotime($sobj->account_activated_on));
					$paidtill_date = ($sobj->paidtill_date != '0000-00-00') ? date('d-m-Y', strtotime($sobj->paidtill_date)) : '0000-00-00';
					$expiration = $sobj->expiration;
					if($expiration == '0000-00-00 00:00:00'){ $expiration = '-'; }
					else{ $expiration = date('d-m-Y', strtotime($sobj->expiration)); }
					$wallet = $this->userbalance_amount($sobj->uid);
					
					$gen .= '
					<tr>
						<td>&nbsp;</td>
						<td>
						   <div class="checkbox" style="margin-top:0px; margin-bottom:0px;">
						      <label>
						      <input type="checkbox" class="collapse_checkbox" value="'.$sobj->id.'"> 
						      </label>
						   </div>
						</td>
						<td><a href="'.base_url().'user/edit/'.$sobj->id.'" target="_blank">'.$sobj->uid.'</a></td>
						<td>'.$sobj->firstname.' '.$sobj->lastname.'</td>
						<td>'.$state.'</td>
						<td>'.$city.'</td>
						<td>'.$zone.'</td>
						<td>'.$activeplan.'</td>
						<td>'.$active_from.'</td>
						<td>'.$paidtill_date.'</td>
						<td>'.$expiration.'</td>
						<td>'.$status.'</td>
						<td><span>'.$cocurrency.'</span> '.$wallet.'</td>
						<td class="mui--text-right">'.$ticket_count.'</td>
					</tr>
					';
					$i++;
				}
			}		
		}
		
		$total_results = $total_search + $total_csearch;
		if($total_results == 0){
			$gen .= "<tr><td style='text-align:center' colspan='10'> No Result Found !!</td></tr>";
		}
		
		$data['total_results'] = $total_results;
		$data['search_results'] = $gen;
		
		echo json_encode($data);
	}


	public function lead_enquiry_userslist(){
		$data = array();
		$gen = '';
		$total_search = 0;
		$where = '';
		
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$regiontype = $this->dept_region_type();
		$permicond = ''; $sczcond = '';
		if ($regiontype == "region") {
			$dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='".$dept_id."' AND status='1' AND is_deleted='0'");
			$total_deptregion = $dept_regionQ->num_rows();
			if($dept_regionQ->num_rows() > 0){
				$c = 1;
				foreach($dept_regionQ->result() as $deptobj){
					$stateid = $deptobj->state_id;
					$cityid = $deptobj->city_id;
					$zoneid = $deptobj->zone_id;
					if($cityid == 'all'){
						$permicond .= " (state='".$stateid."') ";
					}elseif($zoneid == 'all'){
						$permicond .= " (state='".$stateid."' AND city='".$cityid."') ";
					}else{
						$permicond .= " (state='".$stateid."' AND city='".$cityid."' AND zone='".$zoneid."') ";
					}
					if($c != $total_deptregion){
						$permicond .= ' OR ';
					}
					$c++;
				}
			}
			$sczcond .= ' AND ('.$permicond.')';
		}
		
		$search_user = $this->input->post('search_user');
		$where .= " AND ((username LIKE '%".$search_user."%') OR (first_name LIKE '%".$search_user."%') OR (last_name LIKE '%".$search_user."%') OR (middle_name LIKE '%".$search_user."%') OR (email LIKE '%".$search_user."%') OR (phone LIKE '%".$search_user."%'))  ";
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$zone = $this->input->post('zone');
		$locality = $this->input->post('locality');
		if(isset($state) && $state != ''){
			$where .= " AND state='".$state."'";
		}
		if(isset($city) && $city != ''){
			$where .= " AND city='".$city."'";
		}
		if(isset($zone) && $zone != ''){
			$where .= " AND zone='".$zone."'";
		}
		if(isset($locality) && $locality != ''){
			$where .= " AND usuage_locality='".$locality."'";
		}
		$query = $this->db->query("SELECT id,usertype,first_name,last_name,state,city,zone,status,usuage_locality,phone,email FROM sht_subscriber WHERE status='1' AND is_deleted='0' AND (usertype='1' OR usertype='2') $where $sczcond AND isp_uid='".$isp_uid."' ORDER BY id DESC");
		$total_search = $query->num_rows();
		if($query->num_rows() > 0){
			$i = 1;
			foreach($query->result() as $leobj){
				$state = $this->getstatename($leobj->state);
				$city = $this->getcityname($leobj->state, $leobj->city);
				$zone = $this->getzonename($leobj->zone);
				$status = ($leobj->status == 1)?'active':'inactive';
				$usertype = ($leobj->usertype == 1) ? 'Lead' : 'Enquiry';
				
				$gen .= "<tr><td>".$i."</td><td><a href='".base_url()."user/add/".$leobj->id."' target='_blank'>".$leobj->first_name." ".$leobj->last_name."</a></td><td>".$usertype."</td><td>".$this->locality_name($leobj->usuage_locality)."</td><td>".$leobj->email."</td><td>".$leobj->phone."</td><td>".$state."</td><td>".$city."</td><td>".$zone."</td><td><a href='".base_url()."user/add/".$leobj->id."'><i class='fa fa-edit' aria-hidden='true'></i></a> &nbsp;<a href='javascript:void(0)' onclick='delete_lead_enquiry(".$leobj->id.")'><i class='fa fa-trash-o' aria-hidden='true'></i></span></td></tr>";
				$i++;
			}
		}else{
			$gen .= "<tr><td style='text-align:center' colspan='10'> No Result Found !!</td></tr>";
		}
		
		$data['total_results'] = $total_search;
		$data['search_results'] = $gen;
		
		return $data;
	}
	
	public function complaints_list(){
		$data = array();
		$gen = ''; $where = '';
		
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$regiontype = $this->dept_region_type();
		$permicond = ''; $sczcond = '';
		if ($regiontype == "region") {
			$dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='".$dept_id."' AND status='1' AND is_deleted='0'");
			$total_deptregion = $dept_regionQ->num_rows();
			if($dept_regionQ->num_rows() > 0){
				$c = 1;
				foreach($dept_regionQ->result() as $deptobj){
					$stateid = $deptobj->state_id;
					$cityid = $deptobj->city_id;
					$zoneid = $deptobj->zone_id;
					if($cityid == 'all'){
						$permicond .= " (state='".$stateid."') ";
					}elseif($zoneid == 'all'){
						$permicond .= " (state='".$stateid."' AND city='".$cityid."') ";
					}else{
						$permicond .= " (state='".$stateid."' AND city='".$cityid."' AND zone='".$zoneid."') ";
					}
					if($c != $total_deptregion){
						$permicond .= ' OR ';
					}
					$c++;
				}
			}
			$sczcond .= ' AND ('.$permicond.')';
		}
		
		$search_user = $this->input->post('search_user');
		if(isset($search_user) && $search_user != ''){
			$where .= " AND ((tb2.firstname LIKE '%".$search_user."%') OR (tb2.lastname LIKE '%".$search_user."%') OR (tb2.middlename LIKE '%".$search_user."%') OR (tb2.mobile LIKE '%".$search_user."%'))  ";
		}
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$zone = $this->input->post('zone');
		if(isset($state) && $state != ''){
			$where .= " AND tb2.state='".$state."'";
		}
		if(isset($city) && $city != ''){
			$where .= " AND tb2.city='".$city."'";
		}
		if(isset($zone) && $zone != ''){
			$where .= " AND tb2.zone='".$zone."'";
		}
		
		
		$tkttype = $this->input->post('tkttype');
		$tkttimezone = $this->input->post('tkttimezone');
		$tktpriority = $this->input->post('tktpriority');
		$tktassignto = $this->input->post('tktassignto');
		if(isset($tkttype) && $tkttype != ''){
			if($tkttype == 'opentkt'){
				$where .= " AND tb1.status='0'";
			}elseif($tkttype == 'closedtkt'){
				$where .= " AND tb1.status='1'";
			}
		}else{
			$where .= " AND tb1.status='0'";
		}
		if(isset($tkttimezone) && $tkttimezone != ''){
			$tkttimezone = date('Y-m-d', strtotime($tkttimezone));
			$where .= " AND DATE(added_on) = '".$tkttimezone."'";
		}
		if(isset($tktpriority) && $tktpriority != ''){
			$where .= " AND ticket_priority = '".$tktpriority."'";	
		}
		if(isset($tktassignto) && $tktassignto != ''){
			$where .= " AND ticket_assign_to = '".$tktassignto."'";
		}
		
		$query = $this->db->query("SELECT tb1.* FROM sht_subscriber_tickets as tb1 INNER JOIN sht_users as tb2 ON(tb1.subscriber_id = tb2.id) WHERE 1 $where $sczcond AND tb1.isp_uid='".$isp_uid."' ORDER BY tb1.id DESC");
		//echo $this->db->last_query(); die;
		$num_rows = $query->num_rows();
		$total_search = $query->num_rows();
		if($num_rows > 0){
			$i = 1;
			foreach($query->result() as $tktobj){
				$ticket_raise_by = $this->isp_username($tktobj->ticket_raise_by);
				$ticket_assign_to = $this->tkt_sorter_username($tktobj->ticket_assign_to);
				$tktdate = date('d-m-Y', strtotime($tktobj->added_on));
				$tktclosedate = date('d-m-Y', strtotime($tktobj->closed_on));
				$customer_data = $this->getcustomer_data($tktobj->subscriber_id);
				$ticket_addedon = strtotime($tktobj->added_on);
				$action = '';
				if($tktobj->status == '0'){
					$status = 'Open';
					//$action = "<td><a href='javascript:void(0)' onclick='close_ticket_confirmation_alert(".$tktobj->id.")' style='text-decoration: none; color:#29abe2'>Close</a></td><td><a href='javascript:void(0)' style='text-decoration: none; color:#29abe2' onclick='show_edit_ticket_request(".$tktobj->id.")'>Edit</a></td><td>".$ticket_assign_to."</td>";
				}else{
					$status = 'Close';
					//$action = "<td colspan='2'>".$tktclosedate."</td><td>".$ticket_assign_to."</td>";
				}
				$tat_days = $this->tat_days($tktobj->added_on);
				$gen .= "<tr><td>".$i.".</td><td><a href='".base_url()."user/edit/".$tktobj->subscriber_id."/t' id='custname_".$tktobj->subscriber_id."' target='_blank'>".$customer_data->firstname." ".$customer_data->lastname."</a><input type='hidden' id='phone_".$tktobj->subscriber_id."' value='".$customer_data->mobile."'/></td><td><a href='".base_url()."user/edit/".$tktobj->subscriber_id."/t'>".$customer_data->uid."</a></td><td>".$tktobj->ticket_id."</td><td>".$tktdate."</td><td>".$tktobj->ticket_type."</td><td><span class='".$status."'>".$status."</span></td><td class='mui--text-center'>".$tat_days."</td><td class='mui--text-center'>".$tktobj->ticket_priority."</td>".$action."</tr>";
				$i++;
			}
		}else{
			$gen .= "<tr><td style='text-align:center' colspan='12'> No Complaints Issued Yet !!</td></tr>";
		}
		
		$data['total_results'] = $total_search;
		$data['search_results'] = $gen;
		
		return $data;
	}
	
	public function tat_days($issue_date){ //turnaround time
		$date=strtotime($issue_date);//Converted to a PHP date (a second count)
		
		//Calculate difference
		$diff=time() - $date;//time returns current time in seconds
		$days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
		$hours=round(($diff-$days*60*60*24)/(60*60));
		return "$days day $hours hours";
	}

	

	public function user_account_status($uuid){
		$query = $this->db->query("SELECT enableuser FROM `sht_users` WHERE uid='".$uuid."' ");
		$numrows = $query->num_rows();
		if($numrows > 0){
			return $query->row()->enableuser;
		}
	}
	public function paydue_userids($where=''){
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		
		$data = array();
		$pwhere = '';
		if($where != ''){
			$uidArr = array();
			$customerlistQ = $this->db->query("SELECT uid FROM sht_users WHERE 1 $where AND isp_uid='".$isp_uid."'");
			if($customerlistQ->num_rows() > 0){
				foreach($customerlistQ->result() as $uidobj){
					$uidArr[] = $uidobj->uid;
				}
			}
			$uuids = '"' . implode('", "', $uidArr) . '"';
			$pwhere .= " subscriber_uuid IN({$uuids}) ";
		}else{
			$pwhere .= " 1 ";
		}
		$passbookQ = $this->db->query("SELECT subscriber_uuid, SUM(plan_cost) AS expense_cost FROM sht_subscriber_passbook WHERE $pwhere AND isp_uid='".$isp_uid."' GROUP BY `subscriber_uuid` ");
		if($passbookQ->num_rows() > 0){
			foreach($passbookQ->result() as $pbobj){
				$wallet_amt = 0;
				$passbook_amt = $pbobj->expense_cost;
				$uuid = $pbobj->subscriber_uuid;
				$walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."' AND isp_uid='".$isp_uid."'");
				if($walletQ->num_rows() > 0){
					$wallet_amt = $walletQ->row()->wallet_amt;
				}
				$balanceamt = $wallet_amt - $passbook_amt;
				if($balanceamt < 0){
					$data['userids'][] = $uuid;
					$data['balanceamt'][] = $balanceamt;
				}
			}
		}
		
		return $data;
	}
	public function payoverdue_list(){
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$regiontype = $this->dept_region_type();
		
		$data = array();
		$gen = '';
		$total_search = 0;
		$cwhere = '';
		$search_user = $this->input->post('search_user');
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$zone = $this->input->post('zone');
		$locality = $this->input->post('locality');
		$netstatus = $this->input->post('netstatus');
		if(isset($state) && $state != ''){
			$cwhere .= " AND state='".$state."'";
		}
		if(isset($city) && $city != ''){
			$cwhere .= " AND city='".$city."'";
		}
		if(isset($zone) && $zone != ''){
			$cwhere .= " AND zone='".$zone."'";
		}
		if(isset($locality) && $locality != ''){
			$cwhere .= " AND usuage_locality='".$locality."'";
		}
		
		if(isset($search_user)){
			$cwhere .= " AND ((username LIKE '%".$search_user."%') OR (firstname LIKE '%".$search_user."%') OR (mobile LIKE '%".$search_user."%')) ";
		}
		
		$permicond = '';
		if ($regiontype == "region") {
			$dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='".$dept_id."' AND status='1' AND is_deleted='0'");
			$total_deptregion = $dept_regionQ->num_rows();
			if($dept_regionQ->num_rows() > 0){
				$c = 1;
				foreach($dept_regionQ->result() as $deptobj){
					$stateid = $deptobj->state_id;
					$cityid = $deptobj->city_id;
					$zoneid = $deptobj->zone_id;
					if($cityid == 'all'){
						$permicond .= " (state='".$stateid."') ";
					}elseif($zoneid == 'all'){
						$permicond .= " (state='".$stateid."' AND city='".$cityid."') ";
					}else{
						$permicond .= " (state='".$stateid."' AND city='".$cityid."' AND zone='".$zoneid."') ";
					}
					if($c != $total_deptregion){
						$permicond .= ' OR ';
					}
					$c++;
				}
			}
			$cwhere .= ' AND ('.$permicond.')';
		}
		
		$ispcodet = $this->countrydetails();
		$qlmt = 50; $qofset = 0;
		$limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
		
		$uonline = 0; $uoffline = 0; $total_csearch = 0; $statuidsArr = array();
		$paydueArr = $this->paydue_userids($cwhere);
		if(!empty($paydueArr)){
			$paydueidsArr = $paydueArr['userids'];
			if($qofset == 0){
				foreach($paydueidsArr as $puid){
					$chkstatus = $this->check_user_onlineoffline($puid);
					$statuidsArr["$puid"] = $chkstatus;
				}
			}
			$paydueidsArr = array_slice($paydueidsArr, $qofset, $qlmt, true);
			$uuids = '"' . implode('", "', $paydueidsArr) . '"';
			$cquery = $this->db->query("SELECT id, uid, enableuser, firstname, lastname, state, city, zone, account_activated_on, paidtill_date, expiration, usuage_locality, inactivate_type,mobile FROM sht_users WHERE 1 $cwhere AND uid IN ({$uuids}) AND isp_uid='".$isp_uid."'");
			//echo $this->db->last_query(); die;
			$total_csearch = $cquery->num_rows();
			if($total_csearch > 0){
				$i = 1;
				foreach($cquery->result() as $sobj){
					$state = $this->getstatename($sobj->state);
					$city = $this->getcityname($sobj->state, $sobj->city);
					$zone = $this->getzonename($sobj->zone);
					//$status = ($sobj->enableuser == 1)?'active':'inactive';
					$activeplan = $this->userassoc_planname($sobj->id);
					$active_from = ($sobj->account_activated_on != '0000-00-00 00:00:00') ? date('d-m-Y', strtotime($sobj->account_activated_on)) : '0000-00-00';
					$paidtill_date = ($sobj->paidtill_date != '0000-00-00') ? date('d-m-Y', strtotime($sobj->paidtill_date)) : '0000-00-00';
					$expiration = $sobj->expiration;
					if($expiration == '0000-00-00 00:00:00'){ $expiration = '-'; }
					else{ $expiration = date('d-m-Y', strtotime($sobj->expiration)); }
					$wallet = $this->userbalance_amount($sobj->uid);
					
					$wstyle= ''; $onoff_style = ''; $tstyle = '';
					if($wallet < 0){
						$wstyle = "<a href='javascript:void()' style='color:#f00; text-decoration: underline;' onclick='migrate_tobilling(".$sobj->id.")' target='_blank'>".$ispcodet['currency']." ".$wallet."</a>";
					}elseif(($wallet > 0) && ($wallet != 0)){
						$wstyle = "<a href='javascript:void()' style='color:#439B46; text-decoration: underline;' onclick='migrate_tobilling(".$sobj->id.")' target='_blank'>".$ispcodet['currency']." ".$wallet."</a>";
					}else{
						$wstyle = $ispcodet['currency']." ".$wallet;
					}
					
					$status = $statuidsArr["$sobj->uid"];
					if($status == 'online'){
						$onoff_style = "style='color:#439B46'";
					}else{
						$onoff_style = "style='color:#f00'";
					}
					
					$ticket_count = $this->customer_tickets_count($sobj->id);
					if($ticket_count > 0){
						$tstyle = "<a href='javascript:void()' style='color:#f00; text-decoration: underline;' onclick='migrate_totickets(".$sobj->id.")'>".$ticket_count."</a>";
					}else{
						$tstyle = $ticket_count;
					}
					
					if(isset($netstatus) && ($netstatus != '')){
						if($netstatus == $status){
							$gen .= "<tr><td>&nbsp;</td><td><div class='checkbox' style='margin-top:0px; margin-bottom:0px;'><label><input type='checkbox' class='collapse_checkbox' name='paydueuids' value='".$sobj->id."'></label></div></td><td><a href='".base_url()."user/edit/".$sobj->id."' ".$onoff_style." target='_blank'>".$sobj->uid."</a>&nbsp;</td><td>".$sobj->firstname." ".$sobj->lastname."</td><td>".$state."</td><td>".$city."</td><td>".$zone."</td><td>".$activeplan."</td><td>".$active_from."</td><td>".$paidtill_date."</td><td>".$expiration."</td><td ".$onoff_style.">".ucfirst($status)."</td><td>".$wstyle."</td><td class='mui--text-right'>".$tstyle."</td></tr>";
							$i++;
						}else{
							continue;
						}
					}else{
						$gen .= "<tr><td>&nbsp;</td><td><div class='checkbox' style='margin-top:0px; margin-bottom:0px;'><label><input type='checkbox' class='collapse_checkbox' name='paydueuids' value='".$sobj->id."'></label></div></td><td><a href='".base_url()."user/edit/".$sobj->id."' ".$onoff_style." target='_blank'>".$sobj->uid."</a>&nbsp;</td><td>".$sobj->firstname." ".$sobj->lastname."</td><td>".$state."</td><td>".$city."</td><td>".$zone."</td><td>".$activeplan."</td><td>".$active_from."</td><td>".$paidtill_date."</td><td>".$expiration."</td><td ".$onoff_style.">".ucfirst($status)."</td><td>".$wstyle."</td><td class='mui--text-right'>".$tstyle."</td></tr>";
						$i++;
					}
				}
			}
			
			if($total_csearch == 0){
				$gen .= "<tr><td style='text-align:center' colspan='14'> No Result Found !!</td></tr>";
			}
		}else{
			$gen .= "<tr><td style='text-align:center' colspan='14'> No Result Found !!</td></tr>";
		}
		
		$statuids = array_count_values($statuidsArr);
		$data['online_users'] = $statuids['online'];
		$data['offline_users'] = $statuids['offline'];
		$data['total_results'] = count($paydueArr['userids']);
		$data['search_results'] = $gen;
		if($total_csearch == '0'){
			$data['loadmore'] = 0;
		}else{
			$data['loadmore'] = 1;
		}
		$data['limit'] = $qlmt;
		$data['offset'] = $qlmt+$qofset;
		
		return $data;
	}
	public function delete_inactiveUser(){
		$userid = $this->input->post('userid');
		$this->db->delete("sht_users", array('id' => $userid));
		$this->db->delete("sht_subscriber_billing", array('subscriber_id' => $userid));
		$this->db->delete("sht_subscriber_documents", array('subscriber_id' => $userid));
		echo json_encode(true);
	}

	public function otcdue_list(){
		$data = array();
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$gen = ''; $totalsum = 0;
		$otcQ = $this->db->query("SELECT tb1.bill_number, tb1.receipt_number, tb1.receipt_received, DATE(tb1.bill_added_on) as bill_added_on, tb2.uid, tb2.id, GROUP_CONCAT(tb1.bill_type SEPARATOR ', ')  as pendingbills, GROUP_CONCAT(tb1.total_amount SEPARATOR ', ')  as amtbills FROM sht_subscriber_billing as tb1 LEFT JOIN sht_users as tb2 ON(tb1.subscriber_uuid=tb2.uid) WHERE tb1.isp_uid='".$isp_uid."' AND tb1.receipt_received='0' AND (bill_type='installation' OR bill_type='security') GROUP BY uid ORDER BY bill_added_on DESC");
		if($otcQ->num_rows() > 0){
			foreach($otcQ->result() as $otcobj){
				$amtbills = $otcobj->amtbills;
				$billsamtArr = explode(',',$amtbills);
				$total_pendingamt = array_sum($billsamtArr);
				$totalsum += $total_pendingamt;
				
				$gen .= "<tr>
						<td>&nbsp;</td>
						<td><a href='".base_url()."user/edit/".$otcobj->id."/b' target='_blank'>".$otcobj->uid."</a></td>
						<td>".$otcobj->pendingbills."</td>
						<td>".$otcobj->amtbills."</td>
						<td>".$total_pendingamt."</td>
						<td>".$otcobj->bill_added_on."</td>
					</tr>";	
			}
		}else{
			$gen .= '<tr><td colspan="6">No Records Found.</td></tr>';
		}
		
		$data['otcdue_list'] = $gen;
		$data['total_pendingAmt'] = number_format($totalsum,2);
		return $data;
	}
	
	public function walletdue_list(){
		$data = array();
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$gen = ''; $totalsum = 0;
		
		$walletQ = $this->db->query("SELECT SUM(tb1.total_amount) as total_amount, tb2.uid, tb2.id FROM sht_subscriber_billing as tb1 LEFT JOIN sht_users as tb2 ON(tb1.subscriber_uuid=tb2.uid) WHERE tb1.isp_uid='".$isp_uid."' AND tb1.wallet_amount_received = '0' AND tb1.bill_type='addtowallet' GROUP BY uid");
		if($walletQ->num_rows() > 0){
			foreach($walletQ->result() as $wobj){
				$totalsum += $wobj->total_amount;
				$gen .= "<tr>
						<td>&nbsp;</td>
						<td><a href='".base_url()."user/edit/".$wobj->id."/b' target='_blank'>".$wobj->uid."</a></td>
						<td>".$wobj->total_amount."</td>
					</tr>";	
			}
		}else{
			$gen .= '<tr><td colspan="3">No Records Found.</td></tr>';
		}
		
		$data['walletdue_list'] = $gen;
		$data['total_pendingAmt'] = number_format($totalsum,2);
		return $data;
	}

	public function custombilldue_list(){
		$data = array();
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$gen = ''; $totalsum = 0;
		
		$custbillQ = $this->db->query("SELECT tb1.total_amount,tb1.bill_details, tb2.uid, tb2.id FROM sht_subscriber_custom_billing as tb1 LEFT JOIN sht_users as tb2 ON(tb1.uid=tb2.uid) WHERE tb1.isp_uid='".$isp_uid."' AND tb1.receipt_received='0' AND tb1.is_deleted='0'");
		if($custbillQ->num_rows() > 0){
			foreach($custbillQ->result() as $wobj){
				$totalsum += $wobj->total_amount;
				$gen .= "<tr>
						<td>&nbsp;</td>
						<td><a href='".base_url()."user/edit/".$wobj->id."/b' target='_blank'>".$wobj->uid."</a></td>
						<td>".$wobj->bill_details."</td>
						<td>".$wobj->total_amount."</td>
					</tr>";	
			}
		}else{
			$gen .= '<tr><td colspan="4">No Records Found.</td></tr>';
		}
		
		$data['custbilldue_list'] = $gen;
		$data['total_pendingAmt'] = number_format($totalsum,2);
		return $data;
	}


	public function totalotherdues_count(){
		$data = array();
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		
		$otc=0; $wamt=0; $camt=0;
		$otcQ = $this->db->query("SELECT COALESCE(SUM(tb1.total_amount),0)  as otcamt FROM sht_subscriber_billing as tb1  WHERE tb1.isp_uid='".$isp_uid."' AND tb1.receipt_received='0' AND (tb1.bill_type='installation' OR tb1.bill_type='security')");
		if($otcQ->num_rows() > 0){
			$otc = $otcQ->row()->otcamt;
		}
		
		$walletQ = $this->db->query("SELECT COALESCE(SUM(tb1.total_amount),0) as walletamt FROM sht_subscriber_billing as tb1 WHERE tb1.isp_uid='".$isp_uid."' AND tb1.wallet_amount_received = '0' AND tb1.bill_type='addtowallet'");
		if($walletQ->num_rows() > 0){
			$wamt = $walletQ->row()->walletamt;
		}
		
		$custbillQ = $this->db->query("SELECT COALESCE(SUM(tb1.total_amount),0) as custamt FROM sht_subscriber_custom_billing as tb1 WHERE tb1.isp_uid='".$isp_uid."' AND tb1.receipt_received='0' AND tb1.is_deleted='0'");
		if($custbillQ->num_rows() > 0){
			$camt = $custbillQ->row()->custamt;
		}

		$totaldue = ( $otc + $wamt + $camt);
		return number_format($totaldue);
	}

	
/*------------------- PERSONAL DETAILS STARTS ----------------------------------------------------------------*/

	public function add_subscriber(){
		$data = array();
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$login_isp = $sessiondata['userid'];	
		
		$userid = $this->input->post('subscriber_userid');
		$uuid = $this->input->post('subscriber_uuid');
		$username = $this->input->post('subscriber_username');
		$priority_account = $this->input->post('priority_account');
		$usertype = $this->input->post('subscriber_usertype');
		$letoc_dataid = $this->input->post('letoc_dataid');
		
		if(($usertype == 1) || ($usertype == 2)){
			$userdatarr = array(
			    'isp_uid'      => isset($isp_uid) ? $isp_uid : '',
			    'username'    => isset($username) ? trim($username) : '',
			    'usertype' 	  => $this->input->post('subscriber_usertype'),
			    'first_name'  => trim($this->input->post('subscriber_fname')),
			    'middle_name' => trim($this->input->post('subscriber_mname')), 
			    'last_name'   => trim($this->input->post('subscriber_lname')),
			    'email'	  => trim($this->input->post('subscriber_email')),
			    'phone'	  => $this->input->post('subscriber_phone'),
			    'dob'	  => $this->input->post('subscriber_dob'),
			    'flat_number' => trim($this->input->post('subscriber_flatno')),
			    'address1'	  => trim($this->input->post('subscriber_address1')),
			    'address2'	  => trim($this->input->post('subscriber_address2')),
			    'state'	  => $this->input->post('state'),
			    'city'	  => $this->input->post('city'),
			    'zone'	  => $this->input->post('zone'),
			    'usuage_locality' => $this->input->post('usuage_locality'),
			    'priority_account' => isset($priority_account) ? $priority_account : '0',
			    'createdby' => $login_isp,
			    'added_on'    => date('Y-m-d H:i:s')
			);
		}elseif($usertype == '3'){
			if(isset($letoc_dataid) && $letoc_dataid != 0){
				$this->db->delete('sht_subscriber', array('id' => $letoc_dataid));
			}
			$geoaddress_searchtype = $this->input->post('geoaddress_searchtype');
			if($geoaddress_searchtype == 'bylatlng'){
				$geoaddress = trim($this->input->post('geoaddress'));
				$place_id = trim($this->input->post('geoplaceid'));
				$latitude = trim($this->input->post('geolat'));
				$longitude = trim($this->input->post('geolong'));
			}else{
				$geoaddress = trim($this->input->post('geoaddr'));
				$place_id = trim($this->input->post('placeid'));
				$latitude = trim($this->input->post('lat'));
				$longitude = trim($this->input->post('long'));
			}
			
			$radiusUserArr = array(
				'username' => isset($username) ? trim($username) : '',
				'isp_uid' => isset($isp_uid) ? $isp_uid : '',
				'uid' => isset($uuid) ? $uuid : '',
				'password' => '',
				'enableuser' => '0',
				'firstname' => trim($this->input->post('subscriber_fname')),
				'middlename' => trim($this->input->post('subscriber_mname')),
				'lastname' => trim($this->input->post('subscriber_lname')),
				'mobile' => trim($this->input->post('subscriber_phone')),
				'alt_mobile' => trim($this->input->post('subscriber_altphone')),
				'dob'	  => $this->input->post('subscriber_dob'),
				'flat_number' => trim($this->input->post('subscriber_flatno')),
				'address' => trim($this->input->post('subscriber_address1')),
				'address2' => trim($this->input->post('subscriber_address2')),
				'state'	  => $this->input->post('state'),
				'city'	  => $this->input->post('city'),
				'zone'	  => $this->input->post('zone'),
				'usuage_locality' => $this->input->post('usuage_locality'),
				'priority_account' => isset($priority_account) ? $priority_account : '0',
				'baseplanid' => '0',
				'createdon' => date('Y-m-d'),
				'createdby' => $login_isp,
				'owner' => 'admin',
				'email' => trim($this->input->post('subscriber_email')),
				'expired' => '0',
				'geoaddress'=> $geoaddress,
				'place_id'=> $place_id,
				'lat'=> $latitude,
				'longitude'=> $longitude,
				'gstin_number' => trim($this->input->post('gstin_number')),
                                'taxtype' => $this->input->post('taxtype'),
				'billaddr' => '1',
				'geoaddress_searchtype' => $geoaddress_searchtype
			);
			
			$radiusUserArr['billing_username'] = trim($this->input->post('subscriber_fname')) .' '. trim($this->input->post('subscriber_lname'));
			$radiusUserArr['billing_mobileno'] = $this->input->post('subscriber_phone');
			$radiusUserArr['billing_email'] = $this->input->post('subscriber_email');
			$radiusUserArr['billing_flat_number'] = $this->input->post('subscriber_flatno');
			$radiusUserArr['billing_address'] = $this->input->post('subscriber_address1');
			$radiusUserArr['billing_address2'] = $this->input->post('subscriber_address2');
			$radiusUserArr['billing_state'] = $this->input->post('state');
			$radiusUserArr['billing_city'] = $this->input->post('city');
			$radiusUserArr['billing_zone'] = $this->input->post('zone');

		}
		//echo '<pre>'; print_r($userdatarr); die;
		
		if(isset($userid) && $userid != ''){
			$user_rowdata = $this->getcustomer_data($userid);
			$data['customer_id'] = $userid;
			$data['customer_name'] = $user_rowdata->firstname.' '.$user_rowdata->lastname.' ('.$user_rowdata->uid.')';
			$data['customer_uuid'] = $user_rowdata->uid;
			$data['customer_email'] = $user_rowdata->email;
			$data['customer_mobile'] = $user_rowdata->mobile;
			$this->db->update('sht_users', $radiusUserArr, array('id' => $userid));
		}else{
			if(($usertype == 1) || ($usertype == 2)){
				$this->db->insert('sht_subscriber', $userdatarr);
				$userid = $this->db->insert_id();
				$this->db->delete('sht_temp_subscriber', array('subscriber_id' => $uuid));
				//$this->db->insert('sht_subscriber_activation_panel', array('subscriber_id' => $userid, 'personal_details' => '1'));
				
				$data['customer_id'] = $userid;
				$data['customer_name'] = $userdatarr['first_name'].' '.$userdatarr['last_name'].' ('.$userdatarr['useruid'].')';
				$data['customer_uuid'] = $userdatarr['useruid'];
				$data['customer_email'] = $userdatarr['email'];
				$data['customer_mobile'] = $userdatarr['phone'];
			
			}elseif($usertype == 3){
				$this->db->insert('sht_users', $radiusUserArr);
				$userid = $this->db->insert_id();
				$this->db->delete('sht_temp_subscriber', array('subscriber_id' => $uuid));
				$this->db->insert('sht_subscriber_activation_panel', array('subscriber_id' => $userid, 'personal_details' => '1'));
				$user_rowdata = $this->getcustomer_data($userid);
				$data['customer_id'] = $userid;
				$data['customer_name'] = $user_rowdata->firstname.' '.$user_rowdata->lastname.' ('.$user_rowdata->uid.')';
				$data['customer_uuid'] = $user_rowdata->uid;
				$data['customer_email'] = $user_rowdata->email;
				$data['customer_mobile'] = $user_rowdata->mobile;
				
				$this->notify_model->add_user_activitylogs($uuid, 'Customer Registration');
			}
		}
		if(isset($uuid) && $uuid != ''){
			echo json_encode($data);
		}else{
			redirect(base_url().'user');
		}
	}
	public function getcustomer_data($userid){
		$this->db->where('id', $userid);
		$this->db->or_where('uid', $userid);
		$userQuery = $this->db->get('sht_users');
		$num_rows = $userQuery->num_rows();
		return $userQuery->row();
	}
	public function customer_uuid($userid){
		$uid = '';
		$userQuery = $this->db->query("SELECT uid FROM sht_users WHERE id='".$userid."'");
		$num_rows = $userQuery->num_rows();
		if($num_rows > 0){
			$uid = $userQuery->row()->uid;
		}
		
		return $uid;
	}
	public function personal_details($sid){
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$query = $this->db->query("SELECT * FROM sht_users WHERE id='".$sid."' AND isp_uid='".$isp_uid."'");
		$num_rows = $query->num_rows();
		if($num_rows > 0){
			return $query->result();
		}else{
			return $num_rows;
		}
	}
	public function lepersonal_details($leid){
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$query = $this->db->query("SELECT * FROM sht_subscriber WHERE id='".$leid."' AND status='1' AND is_deleted='0'  AND (usertype='1' OR usertype='2') AND isp_uid='".$isp_uid."'" );
		$num_rows = $query->num_rows();
		if($num_rows > 0){
			return $query->result();
		}else{
			return $num_rows;
		}
	}
	
	public function edit_personaldetails(){
		$data = array(); $currentArr = array();
		$userid = $this->input->post('subscriber_userid');
		$uuid = $this->input->post('subscriber_uuid');
		$username = $this->input->post('subscriber_username');
		$priority_account = $this->input->post('priority_account');
		$billaddr = $this->input->post('billaddr');
		$geoaddress_searchtype = $this->input->post('geoaddress_searchtype');
		if($geoaddress_searchtype == 'bylatlng'){
			$geoaddress = trim($this->input->post('geoaddress'));
			$place_id = trim($this->input->post('geoplaceid'));
			$latitude = trim($this->input->post('geolat'));
			$longitude = trim($this->input->post('geolong'));
		}else{
			$geoaddress = trim($this->input->post('geoaddr'));
			$place_id = trim($this->input->post('placeid'));
			$latitude = trim($this->input->post('lat'));
			$longitude = trim($this->input->post('long'));
		}
		
		$currentdataQ = $this->db->query("SELECT username, uid, firstname, middlename, lastname, mobile, dob, flat_number, address, address2, state, city, zone, usuage_locality, priority_account, createdon, createdby, email, geoaddress, place_id, lat, longitude FROM sht_users WHERE id='".$userid."'");
		//echo $this->db->last_query();
		if($currentdataQ->num_rows() > 0){
			$currentArr = $currentdataQ->row_array();
		}
		$userdatarr = array(
			'username' => isset($username) ? trim($username) : '',
			'uid' => isset($uuid) ? $uuid : '',
			'firstname' => trim($this->input->post('subscriber_fname')),
			'middlename' => trim($this->input->post('subscriber_mname')),
			'lastname' => trim($this->input->post('subscriber_lname')),
			'mobile' => trim($this->input->post('subscriber_phone')),
			'alt_mobile' => trim($this->input->post('subscriber_altphone')),
			'dob'	  => $this->input->post('subscriber_dob'),
			'flat_number' => trim($this->input->post('subscriber_flatno')),
			'address' => trim($this->input->post('subscriber_address1')),
			'address2' => trim($this->input->post('subscriber_address2')),
			'state'	  => $this->input->post('state'),
			'city'	  => $this->input->post('city'),
			'zone'	  => $this->input->post('zone'),
			'usuage_locality' => $this->input->post('usuage_locality'),
			'priority_account' => isset($priority_account) ? $priority_account : '0',
			'createdon' => date('Y-m-d'),
			'createdby' => 'admin',
			'email' => trim($this->input->post('subscriber_email')),
			'geoaddress'=> $geoaddress,
			'place_id'=> $place_id,
			'lat'=> $latitude,
			'longitude'=> $longitude,
			'gstin_number' => trim($this->input->post('gstin_number')),
                        'taxtype' => $this->input->post('taxtype'),
			'billaddr' => $billaddr,
			'geoaddress_searchtype' => $geoaddress_searchtype
		);
		if($billaddr == '1'){
			$userdatarr['billing_username'] = trim($this->input->post('subscriber_fname')) .' '. trim($this->input->post('subscriber_lname'));
			$userdatarr['billing_mobileno'] = $this->input->post('subscriber_phone');
			$userdatarr['billing_email'] = $this->input->post('subscriber_email');
			$userdatarr['billing_flat_number'] = $this->input->post('subscriber_flatno');
			$userdatarr['billing_address'] = $this->input->post('subscriber_address1');
			$userdatarr['billing_address2'] = $this->input->post('subscriber_address2');
			$userdatarr['billing_state'] = $this->input->post('state');
			$userdatarr['billing_city'] = $this->input->post('city');
			$userdatarr['billing_zone'] = $this->input->post('zone');
		}else{
			$userdatarr['billing_username'] = trim($this->input->post('billing_subscriber_name'));
			$userdatarr['billing_mobileno'] = $this->input->post('billing_subscriber_phone');
			$userdatarr['billing_email'] = $this->input->post('billing_subscriber_email');
			$userdatarr['billing_flat_number'] = $this->input->post('billing_subscriber_flatno');
			$userdatarr['billing_address'] = $this->input->post('billing_subscriber_address1');
			$userdatarr['billing_address2'] = $this->input->post('billing_subscriber_address2');
			$userdatarr['billing_state'] = $this->input->post('billing_state');
			$userdatarr['billing_city'] = $this->input->post('billing_city');
			$userdatarr['billing_zone'] = $this->input->post('billing_zone');
		}
		
		$diffupdateArr = array_merge(array_diff($currentArr, $userdatarr), array_diff($userdatarr,$currentArr));
		if(count($diffupdateArr) > 0){
			$this->notify_model->add_user_activitylogs($uuid, $diffupdateArr);
		}
		
		$this->db->update('sht_users', $userdatarr, array('id' => $userid));
		$data['customer_id'] = $userid;
		$data['customer_name'] = $userdatarr['firstname'].' '.$userdatarr['lastname'].' ('.$userdatarr['uid'].')';
		$data['customer_uuid'] = $userdatarr['uid'];
		$data['customer_email'] = $userdatarr['email'];
		$data['customer_mobile'] = $userdatarr['mobile'];
		$data['billaddr'] = $billaddr;
		
		echo json_encode($data);
		
	}

	public function radcheck_password($uuid){
		$radchkQ = $this->db->query("SELECT value FROM radcheck WHERE username='".$uuid."' AND attribute = 'Cleartext-Password'");
		if($radchkQ->num_rows() > 0){
			return $radchkQ->row()->value;
		}else{
			return '';
		}
	}

	public function editrouterpassword(){
		$uuid = $this->input->post('uuid');
		$radchkpwd = $this->input->post('radiuspassword');
		$prevradchkQ = $this->db->query("SELECT id FROM radcheck WHERE value != '".$radchkpwd."' AND username='".$uuid."' AND attribute = 'Cleartext-Password'");
		if($prevradchkQ->num_rows() > 0){
			$this->notify_model->add_user_activitylogs($uuid, 'Update Router Password');
		}
		
		$this->db->query("UPDATE radcheck SET value='".$radchkpwd."' WHERE username='".$uuid."' AND attribute = 'Cleartext-Password'");
		echo json_encode(true);
	}

	public function enquiryfeedback($enq_userid){
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$enq_username = '';
		$subsdataQ = $this->db->query("SELECT first_name,middle_name,last_name FROM sht_subscriber WHERE id='".$enq_userid."' AND status='1' AND is_deleted='0'  AND (usertype='1' OR usertype='2') AND isp_uid='".$isp_uid."'" );
		$num_rows = $subsdataQ->num_rows();
		if($num_rows > 0){
			$subs_rowdata = $subsdataQ->row();
			$enq_username = $subs_rowdata->first_name.' '.$subs_rowdata->last_name;
		}
		$gen = '';
		$enquiryQ = $this->db->query("SELECT * FROM sht_enquiry_feedback WHERE isp_uid='".$isp_uid."' AND subsc_id='".$enq_userid."' ORDER BY id DESC");
		if($enquiryQ->num_rows() > 0){
			foreach($enquiryQ->result() as $enqobj){
				$added_on = explode(' ', $enqobj->added_on);
				$date = $added_on[0];
				$time = $added_on[1];
				$feedback_message = $enqobj->message;
				$gen .= "<tr><td>".$date."</td><td>".$time."</td><td>".$enq_username."</td><td>".$feedback_message."</td></tr>";	
			}
		}
		
		echo json_encode($gen);
	}
	public function addenquiryfeedback($enq_userid){
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$feedback_message = $this->input->post('feedback_message');
		$enq_username = '';
		$subsdataQ = $this->db->query("SELECT first_name,middle_name,last_name FROM sht_subscriber WHERE id='".$enq_userid."' AND status='1' AND is_deleted='0'  AND (usertype='1' OR usertype='2') AND isp_uid='".$isp_uid."'" );
		$num_rows = $subsdataQ->num_rows();
		if($num_rows > 0){
			$subs_rowdata = $subsdataQ->row();
			$enq_username = $subs_rowdata->first_name.' '.$subs_rowdata->last_name;
		}
		
		$enquiry_data = array(
			'isp_uid' => $isp_uid,
			'subsc_id' => $enq_userid,
			'subsc_name' => $enq_username,
			'message' => $feedback_message,
			'added_on' => date('Y-m-d H:i:s')
		);
		$this->db->insert('sht_enquiry_feedback', $enquiry_data);
		
		$gen = '';
		$enquiryQ = $this->db->query("SELECT * FROM sht_enquiry_feedback WHERE isp_uid='".$isp_uid."' AND subsc_id='".$enq_userid."' ORDER BY id DESC");
		if($enquiryQ->num_rows() > 0){
			foreach($enquiryQ->result() as $enqobj){
				$added_on = explode(' ', $enqobj->added_on);
				$date = $added_on[0];
				$time = $added_on[1];
				$feedback_message = $enqobj->message;
				$gen .= "<tr><td>".$date."</td><td>".$time."</td><td>".$enq_username."</td><td>".$feedback_message."</td></tr>";	
			}
		}
		
		echo json_encode($gen);
		
	}
/*-------------------- KYC METHODS START -------------------------------------------------------------------*/
	
	public function add_subscriber_documents($filename_array,$kyc_docarr,$kyc_docnum, $idp_docarr,$idp_docnum, $corp_docarr,$corp_docnum){
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$login_isp = $sessiondata['userid'];
		
		$subscriber_userid = $this->input->post('subscriber_userid');
		$subscriber_uuid = $this->input->post('subscriber_uuid');
		$global_counter = 0;
		$total_idpdocs = count($idp_docarr);
		if(!empty($idp_docarr)){
			foreach($idp_docarr as $idpkey => $idpval){
				if($idpval != ''){
					$doctype = $idpval;
					$docnumber = $idp_docnum[$idpkey];
					$filename = $filename_array[$idpkey];
					$doc_prooftype = 'PhotoID';
					$this->db->insert('sht_subscriber_documents', array('isp_uid' => $isp_uid, 'subscriber_id' => $subscriber_userid, 'subscriber_uuid' => $subscriber_uuid, 'doc_prooftype' =>$doc_prooftype ,'doctype' => $doctype, 'document_number' => $docnumber, 'filename' => $filename, 'added_on' => date('Y-m-d H:i:s')));
					$global_counter++;
				}
			}
		}

		if(!empty($kyc_docarr)){
			foreach($kyc_docarr as $kyckey => $kycval){
				if($kycval != ''){
					$doctype = $kycval;
					$docnumber = $kyc_docnum[$kyckey];
					$filename = $filename_array[$global_counter];
					$doc_prooftype = 'KYC';
					$this->db->insert('sht_subscriber_documents', array('isp_uid' => $isp_uid, 'subscriber_id' => $subscriber_userid, 'subscriber_uuid' => $subscriber_uuid, 'doc_prooftype' =>$doc_prooftype ,'doctype' => $doctype, 'document_number' => $docnumber, 'filename' => $filename, 'added_on' => date('Y-m-d H:i:s')));
					$global_counter++;
				}
			}
		}
		
		if(!empty($corp_docarr)){
			foreach($corp_docarr as $corpkey => $corpval){
				if($corpval != ''){
					$doctype = $corpval;
					$docnumber = $corp_docnum[$corpkey];
					$filename = $filename_array[$global_counter];
					$doc_prooftype = 'Corporate';
					$this->db->insert('sht_subscriber_documents', array('isp_uid' => $isp_uid, 'subscriber_id' => $subscriber_userid, 'subscriber_uuid' => $subscriber_uuid, 'doc_prooftype' =>$doc_prooftype ,'doctype' => $doctype, 'document_number' => $docnumber, 'filename' => $filename, 'added_on' => date('Y-m-d H:i:s')));
					$global_counter++;
				}
			}
		}
		
		$user_signature = '';
		if(isset($filename_array['profile_image'])){
			$user_image = $filename_array['profile_image'];
			$this->db->update('sht_users', array('user_profile_image' => $user_image), array('id' => $subscriber_userid));
		}
		$global_counter += 1;

		if(isset($filename_array['signature'])){
			$user_signature = $filename_array['signature'];
			$this->db->update('sht_users', array('user_signature_image' => $user_signature), array('id' => $subscriber_userid));
		}
		
		$this->db->update('sht_subscriber_activation_panel', array('kyc_details' => '1'), array('subscriber_id' => $subscriber_userid));
		
		$this->notify_model->add_user_activitylogs($subscriber_uuid, 'Add KYC Details');
		
		$target_path = "assets/media/documents/".$subscriber_uuid;
		$files = glob($target_path."/*"); //get all file names
		foreach($files as $file){
		    if(is_file($file)){
			$filename = basename($file);
			$checkoldpics = $this->db->get_where('sht_subscriber_documents',  array('subscriber_uuid' => $subscriber_uuid, 'filename' => $filename));
			if($checkoldpics->num_rows() == 0){
				$checkforotherpics = $this->db->query("SELECT id FROM sht_users WHERE (user_profile_image='".$filename."' OR user_signature_image='".$filename."')");
				if($checkforotherpics->num_rows() == 0){
					unlink($file); //delete file
				}
			}
		    }
		}
		
		/*echo '<pre>'; print_r($filename_array);
		echo '<pre>'; print_r($kyc_docarr);
		echo '<pre>'; print_r($kyc_docnum);
		echo '<pre>'; print_r($idp_docarr);
		echo '<pre>'; print_r($idp_docnum);*/
		echo $subscriber_userid;
	}

	public function edit_allkyc_details(){
		$data = array();		
		$userid = $this->input->post('cust_uuid');
	
		$permstyle = '';
		$isperm = $this->is_permission(KYC,'EDIT');
		if($isperm == false){
		  if($this->is_permission(KYC,'RO')){
			$data["isreadonly_permission"] = '1';
		  }else{
			$data["isreadonly_permission"] = '0';
		  }
		}
		
		$query = $this->db->query("SELECT * FROM sht_subscriber_documents WHERE subscriber_id='".$userid."' AND is_deleted='0'");
		$num_rows = $query->num_rows();
		if($num_rows > 0){
			$i = 0; $user_profile_image = ''; $user_signature_image = '';
			foreach($query->result() as $docobj){
				$uuid = $docobj->subscriber_uuid;
				$imageQ = $this->db->query("SELECT user_profile_image, user_signature_image FROM sht_users WHERE uid='".$uuid."'");
				if($imageQ->num_rows() > 0){
					$image_rowdata = $imageQ->row();
					$user_profile_image = $image_rowdata->user_profile_image;
					$user_signature_image = $image_rowdata->user_signature_image;
				}
				$data['kycdocs'][$i]['docid'] = $docobj->id;
				$data['kycdocs'][$i]['subscriber_uuid'] = $docobj->subscriber_uuid;
				$data['kycdocs'][$i]['doctype'] = $docobj->doctype;
				$data['kycdocs'][$i]['filename'] = $docobj->filename;
				$data['kycdocs'][$i]['document_number'] = $docobj->document_number;
				$data['kycdocs'][$i]['doc_prooftype'] = $docobj->doc_prooftype;
				$i++;
			}
			$data['kycdocs'][$i]['subscriber_uuid'] = $docobj->subscriber_uuid;
			$data['kycdocs'][$i]['user_profile_image'] = $user_profile_image;
			$data['kycdocs'][$i]['user_signature_image'] = $user_signature_image;
		}else{
			$imageQ = $this->db->query("SELECT uid, user_profile_image, user_signature_image FROM sht_users WHERE id='".$userid."'");
			if($imageQ->num_rows() > 0){
				$image_rowdata = $imageQ->row();
				$user_profile_image = $image_rowdata->user_profile_image;
				$user_signature_image = $image_rowdata->user_signature_image;
				$data['kycdocs'][0]['subscriber_uuid'] = $image_rowdata->uid;
				$data['kycdocs'][0]['user_profile_image'] = $user_profile_image;
				$data['kycdocs'][0]['user_signature_image'] = $user_signature_image;
			}
		}
		
		echo json_encode($data);
	      
	}
	public function edit_docdelete(){
		$userid = $this->input->post('cust_uuid');
		$docid = $this->input->post('docid');
		if(($docid != 'userprofile') && ($docid != 'usersign')){
			$query = $this->db->get_where('sht_subscriber_documents',  array('id' => $docid));
			$filename = $query->row()->filename;
			$this->db->update('sht_subscriber_documents', array('is_deleted' => '1'), array('id' => $docid));
			$target_path = "assets/media/documents/";
			unlink($target_path.$userid.'/'.$filename);
			$this->s3->deleteObject(bucket, AMAZON_HOMEUSERS_PATH.'kyc_docs/'.$userid.'/'.$filename);
		}else{
			if($docid == 'userprofile'){
				$pquery = $this->db->query('SELECT user_profile_image FROM sht_users WHERE uid="'.$userid.'"');
				$filename = $pquery->row()->user_profile_image;
				$this->db->update('sht_users', array('user_profile_image' => ''), array('uid' => $userid));
				$target_path = "assets/media/documents/";
				unlink($target_path.$userid.'/'.$filename);
				$this->s3->deleteObject(bucket, AMAZON_HOMEUSERS_PATH.'kyc_docs/'.$userid.'/'.$filename);
			}else if($docid == 'usersign'){
				$psquery = $this->db->query('SELECT user_signature_image FROM sht_users WHERE uid="'.$userid.'"');
				$filename = $psquery->row()->user_signature_image;
				$this->db->update('sht_users', array('user_signature_image' => ''), array('uid' => $userid));
				$target_path = "assets/media/documents/";
				unlink($target_path.$userid.'/'.$filename);
				$this->s3->deleteObject(bucket, AMAZON_HOMEUSERS_PATH.'kyc_docs/'.$userid.'/'.$filename);
			}
		}
		
		$this->notify_model->add_user_activitylogs($userid, 'Delete KYC Details');
		echo json_encode(1);
	}



	public function downloadkyc($subid){
		$kycarr = array();
		$query = $this->db->query("SELECT doctype, filename FROM sht_subscriber_documents WHERE subscriber_id='".$subid."' AND is_deleted='0'");
		$num_rows = $query->num_rows();
		$i = 0;
		if($num_rows > 0){
			foreach($query->result() as $docobj){
				$kycarr[$i] = $docobj->doctype.'__'.$docobj->filename;
				$i++;
			}
		}
		$uuid = '';
		$imageQ = $this->db->query("SELECT uid, user_profile_image, user_signature_image FROM sht_users WHERE id='".$subid."'");
		$imgnum_rows = $imageQ->num_rows();
		if($imgnum_rows > 0){
			$rowdata = $imageQ->row();
			$uuid = $rowdata->uid;
			$kycarr[$i] =  'profile__'.$rowdata->user_profile_image;
			$kycarr[$i+1] =  'signature__'.$rowdata->user_signature_image;
		}

		//https://www.brightcherry.co.uk/scribbles/php-check-if-file-exists-on-different-domain/
		if(count($kycarr) > 0){
			$zip = new ZipArchive();
			$zipname = $uuid."_kycdocuments.zip"; // Zip name
			$zip->open($zipname,  ZipArchive::CREATE);
			foreach($kycarr as $kycdocarr){
				$explodekyc = explode('__', $kycdocarr);
				$doctype = $explodekyc['0'];
				$kycdoc = $explodekyc['1'];
				$kycext = substr($kycdoc, strrpos($kycdoc, '.')+1);
				$path = AMAZON_HOMEUSERS_VIEWPATH.'kyc_docs/'.$uuid.'/'.$kycdoc;
				$header_response = get_headers($path, 1);
				if ( strpos( $header_response[0], "404" ) !== false ){
					// FILE DOES NOT EXIST
					echo "FILE DOES NOT EXIST";
				} else {
					//echo "FILE EXISTS!!";
					$content = file_get_contents($path);
					$zip->addFromString(pathinfo ( $kycdoc, PATHINFO_BASENAME), $content);
					$zip->renameName($kycdoc, $doctype.'.'.$kycext);
				}
			}
			$zip->close();
			$zipname = $uuid."_kycdocuments.zip"; // Zip name
			header("Pragma: no-cache");
			header("Expires: 0");
			header ("Content-Type: application/octet-stream");
			header("Content-Transfer-Encoding: Binary");
			header('Content-disposition: attachment; filename="'.$zipname.'"');
			header('Content-Length: ' . filesize($zipname));
			ob_clean();
			flush();
			readfile($ziploc);
			@unlink($ziploc);
		}
	}
	
	public function upload_allimages_s3server(){
		ini_set('max_execution_time', '0');
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$target_path = "assets/media/documents/";
		$checkkycpicsQ = $this->db->query("SELECT subscriber_uuid, filename FROM sht_subscriber_documents WHERE isp_uid='".$isp_uid."' AND is_deleted='0'");
		if($checkkycpicsQ->num_rows() > 0){
			foreach($checkkycpicsQ->result() as $kycpicobj){
				$uuid = $kycpicobj->subscriber_uuid;
				$kycfile = $kycpicobj->filename;
				//Create Folder If not exists
				$s3folder_exists = $this->s3->getObjectInfo(bucket, AMAZON_HOMEUSERS_PATH.'kyc_docs/'.$uuid);
				if (!$s3folder_exists){
					$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
				}
				$checkforotherpics = $this->db->query("SELECT user_profile_image,user_signature_image FROM sht_users WHERE uid='".$uuid."'");
				if($checkforotherpics->num_rows() > 0){
					$rowdata = $checkforotherpics->row();
					$profile_image = $rowdata->user_profile_image;
					$signature_image = $rowdata->user_signature_image;
					
					$pfname = $target_path.$uuid.'/'.$profile_image;
					$pamazonname = AMAZON_HOMEUSERS_PATH.'kyc_docs/'.$uuid.'/'.$profile_image;
					$this->s3->putObjectFile($pfname, bucket , $pamazonname, S3::ACL_PUBLIC_READ) ;
					//unlink($pfname);
					
					$sfname = $target_path.$uuid.'/'.$signature_image;
					$samazonname = AMAZON_HOMEUSERS_PATH.'kyc_docs/'.$uuid.'/'.$signature_image;
					$this->s3->putObjectFile($sfname, bucket , $samazonname, S3::ACL_PUBLIC_READ) ;
					//unlink($sfname);
				}
				
				
				$fname = $target_path.$uuid.'/'.$kycfile;
				$famazonname = AMAZON_HOMEUSERS_PATH.'kyc_docs/'.$uuid.'/'.$kycfile;
				$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
				//unlink($fname);
				
			}
		}
		
		sleep(10);
		if($checkkycpicsQ->num_rows() > 0){
			foreach($checkkycpicsQ->result() as $kycpicobj){
				$uuid = $kycpicobj->subscriber_uuid;
				
				$target_path = "assets/media/documents/".$uuid;
				$files = glob($target_path."/*"); //get all file names
				foreach($files as $file){
				    if(is_file($file)){
					unlink($file); //delete file
				    }
				}
			}
		}
		
	}

	
/*------------------- TICKETS METHODS START ------------------------------------------------------------------*/
	
	public function ticket_sorters_list($userid=''){
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$login_userid = $sessiondata['userid'];
		$gen = ''; $permicond = ''; $sczcond = ''; $deptarr=array();
		
		$deptquery = $this->db->get_where('sht_department', array('assign_ticket' => '1', 'status' => '1', 'is_deleted' => '0', 'id' => $dept_id));
		if ($deptquery->num_rows() > 0) {
			foreach($deptquery->result() as $deptobj){
				$regiontype = $deptobj->region_type;
				if($regiontype == 'region'){
					$dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='" . $dept_id . "' AND status='1' AND is_deleted='0'");
					$total_deptregion = $dept_regionQ->num_rows();
					if ($dept_regionQ->num_rows() > 0) {
					    $c = 1;
					    foreach ($dept_regionQ->result() as $deptobj) {
						$stateid = $deptobj->state_id;
						$cityid = $deptobj->city_id;
						$zoneid = $deptobj->zone_id;
						if ($cityid == 'all') {
						    $permicond .= " (state_id='" . $stateid . "') ";
						} elseif ($zoneid == 'all') {
						    $permicond .= " (state_id='" . $stateid . "' AND city_id='" . $cityid . "') ";
						} else {
						    $permicond .= " (state_id='" . $stateid . "' AND city_id='" . $cityid . "' AND zone_id='" . $zoneid . "') ";
						}
						if ($c != $total_deptregion) {
						    $permicond .= ' OR ';
						}
						$c++;
					    }
					}
					$sczcond .= ' AND (' . $permicond . ')';
					$query=$this->db->query("select dept_id from sht_dept_region where status='1' {$sczcond}");
				      //  echo $this->db->last_query();
					$deptarr=array();
					foreach($query->result() as $val){
					    $deptarr[]=$val->dept_id;
					}
				}
			}
		}
			//print_r($deptarr);
		if(!empty($deptarr)){
			$paramid = '"' . implode('", "', $deptarr) . '"';
			$tktsortQ = $this->db->query("SELECT id, name FROM sht_isp_users WHERE dept_id IN ({$paramid}) AND status='1' AND is_deleted='0'");
		}else{
			$tktsortQ = $this->db->query("SELECT tb1.id, tb1.name FROM sht_isp_users as tb1 INNER JOIN sht_department as tb2 ON(tb1.dept_id=tb2.id) WHERE tb2.assign_ticket='1' AND tb1.status='1' AND tb1.is_deleted='0' AND tb2.status='1' AND tb2.is_deleted='0' AND tb1.isp_uid='".$isp_uid."'");
		}
		
		if($tktsortQ->num_rows() > 0){
			foreach($tktsortQ->result() as $listobj){
				$sel = '';
				if($userid == $listobj->id){
					$sel = 'selected="selected"';
				}
				$gen .= '<option value="'.$listobj->id.'" '.$sel.'>'.$listobj->name.'</option>';
			}
		}

		echo $gen;
	}
	
	
	public function add_ticket_request(){
		
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$super_admin = $session_data['super_admin'];
		$isp_userid = $session_data['userid'];
		if($super_admin == 1){
		    $added_by = 'SuperAdmin';
		}else{
		    /*$teamQ = $this->db->query("SELECT name, lastname FROM sht_isp_users WHERE id='".$isp_userid."'");
		    if($teamQ->num_rows() > 0){
			$team_rowdata = $teamQ->row();
			$added_by = $team_rowdata->name. " " .$team_rowdata->lastname; 
		    }else{
			$added_by = 'TeamMember';
		    }*/
		    $added_by = $isp_userid;
		}
		
		$post = $this->input->post();
		$ticketRequestData = array(
			'ticket_id' => $post['ticketid'],
			'isp_uid' => $isp_uid,
			'subscriber_id' => $post['subscriber_userid'],
			'subscriber_uuid' => $post['customer_uuid'],
			'added_on' => $post['ticket_datetime'],
			'ticket_type' => $post['ticket_type'],
			'ticket_priority' => $post['ticket_priority'],
			'ticket_assign_to' => $post['ticket_assignto'],
			'ticket_raise_by' => $added_by,
			'ticket_description' => $post['ticket_description'],
			'ticket_comment' => $post['ticket_comment'],
			'added_on' => date('Y-m-d H:i:s')
		);
		$this->db->insert('sht_subscriber_tickets', $ticketRequestData);
		$lastid = $this->db->insert_id();
		
		$tktcomment = $post['ticket_comment'];
		if(isset($tkt_comment) && $tkt_comment != ''){
			$this->db->insert('sht_subscriber_tickets_comments', array('ticket_id' => $lastid, 'ticket_edit_by' => $added_by, 'comment' => $tktcomment, 'added_on' => date('Y-m-d H:i:s') ));
		}
		
		$uuid = $post['customer_uuid'];
		$tickettype = $post['ticket_type'];
		$tickettype = str_replace("_", " ", $tickettype);
                $tkt_number = $post['ticketid'];
		$this->notify_model->add_user_activitylogs($uuid, "$tickettype Ticket Request");
		
		$useralerts_status = $this->useralerts_status($uuid);
		$sms_alert = $useralerts_status['sms_alert'];
		$email_alert = $useralerts_status['email_alert'];
		if($sms_alert == 1){
			$this->notify_model->ticket_activation_alert($uuid, $tickettype, $tkt_number);
		}
		if($email_alert == 1){
			$this->emailer_model->ticket_activation_emailalert($uuid, $tickettype, $tkt_number);
		}
		echo json_encode($lastid);
	}
	
	public function fetch_allopenticket(){
		$subscriber_uuid = $this->input->post('cust_uuid');
		$query = $this->db->query("SELECT * FROM sht_subscriber_tickets WHERE subscriber_uuid='".$subscriber_uuid."' AND status != '1' ORDER BY id DESC");
		$num_rows = $query->num_rows();
		if($num_rows > 0){
			$i = 1;
			foreach($query->result() as $tktobj){
				$tktid = $tktobj->id;
				$ticket_raise_by = $tktobj->ticket_raise_by;
				$tktdate = date('d-m-Y', strtotime($tktobj->added_on));
				$tktclosedate = date('d-m-Y', strtotime($tktobj->closed_on));
				$tat_days = $this->tat_days($tktobj->added_on);
				
				if($ticket_raise_by != 'SuperAdmin'){
					if($tktobj->ticket_raise_by == '0'){
						$user_rowdata = $this->getcustomer_data($subscriber_uuid);
						$ticket_raise_by = $user_rowdata->firstname.' '.$user_rowdata->lastname;
					}else{
						$ticket_raise_by = $this->isp_username($tktobj->ticket_raise_by);
					}
				}
				
				$ticket_assign_to = '-';
				if($tktobj->ticket_assign_to != 0){
					$ticket_assign_to = $this->tkt_sorter_username($tktobj->ticket_assign_to) .' <a href="javascript:void(0)" onclick="assign_ticket_member('.$tktid.')">(Re-Assign Ticket)</a>';
				}else{
					if($tktobj->status == '0'){
						$ticket_assign_to = '<a href="javascript:void(0)" onclick="assign_ticket_member('.$tktid.')">Assign Ticket</a>';
					}
				}
				echo '
				      <tr>
					<td>'.$i.'.</td>
					<td>'.$tktobj->ticket_id.'</td>
					<td>'.$tktdate.'</td>
					<td>'.ucwords(str_replace('_', ' ', $tktobj->ticket_type)).'</td>';
					//<td colspan="2">'.$tktobj->ticket_comment.'</td>';
					$sel1 = ''; $sel2=''; $sel3 = '';
					if($tktobj->status != '1'){
						if($tktobj->status == '0'){
							$sel1 = 'selected="selected"';
						}elseif($tktobj->status == '2'){
							$sel2 = 'selected="selected"';
						}
						echo '<td colspan="2">
							<select id="changetktstatus" style="width:100%;box-shadow:none;margin:0px">
								<option value="0" '.$sel1.' data-tktid="'.$tktobj->id.'">Open</option>
								<option value="2" '.$sel2.' data-tktid="'.$tktobj->id.'">Pending</option>
								<option value="1" data-tktid="'.$tktobj->id.'">Closed</option>
							</select>
						      </td>';	
					}
					
				echo'	<td style="text-align:center">'.$tat_days.'</td>
					<td>'.$tktobj->ticket_priority.'</td>';
					if($tktobj->status != '1'){
				echo	'
					<td><a href="javascript:void(0)" style="text-decoration: none; color:#29abe2" onclick="show_edit_ticket_request('.$tktobj->id.')" title="Edit Ticket">Edit</a> | <a href="javascript:void(0)" style="text-decoration: none; color:#29abe2" onclick="show_ticket_summary('.$tktobj->id.')" title="View Summary">View</a></td>
					<td>'.$ticket_raise_by.'</td>
					<td style="text-align:center;">'.$ticket_assign_to.'</td>';
					}else{
				echo	'<td colspan="2">'.$tktclosedate.'</td>
					<td>&nbsp;</td>
					 <td>'.$ticket_assign_to.'</td>';		
					}
				echo '</tr>';
				$i++;
			}
		}else{
			echo "<tr><td style='text-align:center' colspan='12'> No Complaints Issued Yet !!</td></tr>";
		}
	}
	public function fetch_allclosedticket(){
		$subscriber_uuid = $this->input->post('cust_uuid');
		$query = $this->db->query("SELECT * FROM sht_subscriber_tickets WHERE subscriber_uuid='".$subscriber_uuid."' AND status = '1' ORDER BY id DESC");
		$num_rows = $query->num_rows();
		if($num_rows > 0){
			$i = 1;
			foreach($query->result() as $tktobj){
				$tktid = $tktobj->id;
				$ticket_raise_by = $tktobj->ticket_raise_by;
				$tktdate = date('d-m-Y', strtotime($tktobj->added_on));
				$tktclosedate = date('d-m-Y', strtotime($tktobj->closed_on));
				$tat_days = $this->tat_days($tktobj->added_on);
				
				if($ticket_raise_by != 'SuperAdmin'){
					if($tktobj->ticket_raise_by == '0'){
						$user_rowdata = $this->getcustomer_data($subscriber_uuid);
						$ticket_raise_by = $user_rowdata->firstname.' '.$user_rowdata->lastname;
					}else{
						$ticket_raise_by = $this->isp_username($tktobj->ticket_raise_by);
					}
				}
				
				$ticket_assign_to = '-';
				if($tktobj->ticket_assign_to != 0){
					$ticket_assign_to = $this->tkt_sorter_username($tktobj->ticket_assign_to);
				}else{
					if($tktobj->status == '0'){
						$ticket_assign_to = '<a href="javascript:void(0)" onclick="assign_ticket_member('.$tktid.')">Assign Ticket</a>';
					}
				}
				echo '
				      <tr>
					<td>'.$i.'.</td>
					<td>'.$tktobj->ticket_id.'</td>
					<td>'.$tktdate.'</td>
					<td>'.ucwords(str_replace('_', ' ', $tktobj->ticket_type)).'</td>';
					//<td colspan="2">'.$tktobj->ticket_comment.'</td>';
					$sel1 = ''; $sel2=''; $sel3 = '';
					if($tktobj->status == '1'){
						echo '<td colspan="2">
							<select id="changetktstatus" style="width:100%;box-shadow:none;margin:0px" disabled>
								<option value="0" data-tktid="'.$tktobj->id.'">Open</option>
								<option value="2" data-tktid="'.$tktobj->id.'">Pending</option>
								<option value="1" data-tktid="'.$tktobj->id.'" selected>Closed</option>
							</select>
						      </td>';	
					}
					
				echo'	<td style="text-align:center">'.$tat_days.'</td>
					<td>'.$tktobj->ticket_priority.'</td>
					<td>'.$ticket_raise_by.'</td>
					<td>'.$ticket_assign_to.'</td>
					<td>'.$tktclosedate.'</td>
					<td><a href="javascript:void(0)" style="text-decoration: none; color:#29abe2" onclick="assign_ticket_member('.$tktid.')" title="Re-Open Ticket">Re-Open Ticket</a> | <a href="javascript:void(0)" style="text-decoration: none; color:#29abe2" onclick="show_ticket_summary('.$tktobj->id.')" title="View Summary">View</a></td>';
				echo '</tr>';
				$i++;
			}
		}else{
			echo "<tr><td style='text-align:center' colspan='12'> No Complaints Closed Yet !!</td></tr>";
		}
	}
	public function getticket_sortername($userid){
		$data = array();
		$sorternameQ = $this->db->query("SELECT CONCAT_WS(' ',name, lastname) as member, phone FROM sht_isp_users WHERE id='".$userid."'");
		if($sorternameQ->num_rows() > 0){
			$rowdata = $sorternameQ->row();
			$data['tkt_issuesto'] = $rowdata->member;
			$data['tkt_contact'] = $rowdata->phone;
		}
		return $data;
	}
	public function edit_ticket_request(){
		$data = array();
		$isp_userid = $this->session->userdata['isp_session']['userid'];
		$tktid = $this->input->post('tktid');
		$edticketid = $this->input->post('edticketid');
		$post = $this->input->post();
		$tkt_comment = $post['edticket_comment'];
		if(isset($tkt_comment) && $tkt_comment != ''){
			$ticketRequestData = array(
				'ticket_id' => $tktid,
				'ticket_edit_by' => $isp_userid,
				'comment' => $post['edticket_comment'],
				'added_on' => date('Y-m-d H:i:s')
			);
			$data['comment'] = 1;
			$this->db->insert('sht_subscriber_tickets_comments', $ticketRequestData);
			//$this->notify_model->add_user_activitylogs($uuid, "Comment added for Ticket: $tktid");
		}
		
		$ticket_assignto =  $this->input->post('edticket_assignto');
		if(isset($ticket_assignto) && ($ticket_assignto != '')){
			$chkteamuser = $this->db->query("SELECT ticket_assign_to FROM sht_subscriber_tickets_assignment WHERE ticket_id='".$tktid."' AND ticket_assign_to='".$ticket_assignto."'");
			if($chkteamuser->num_rows() == 0){
				$this->db->update('sht_subscriber_tickets_assignment', array('status' => '0'), array('ticket_id' => $tktid));
				$this->db->insert('sht_subscriber_tickets_assignment', array('ticket_id' => $tktid, 'ticket_assign_to' => $ticket_assignto, 'comments' => '', 'status' => '1', 'added_on' => date('Y-m-d H:i:s')));
			}
			
			$this->db->update('sht_subscriber_tickets', array('ticket_assign_to' => $ticket_assignto), array('id' => $tktid));
			$issuedUserArr = $this->getticket_sortername($ticket_assignto);
			$issuedto = $issuedUserArr['tkt_issuesto'];
			$this->notify_model->add_user_activitylogs($uuid, "Ticket assigned to $issuedto");
		}
		
		$tktQuery = $this->db->get_where('sht_subscriber_tickets', array('id' => $tktid));
		$num_rows = $tktQuery->num_rows();
		if($num_rows > 0){
			foreach($tktQuery->result() as $tktobj){
				$data['tid'] = $tktobj->id;
				$data['ticket_id'] = $tktobj->ticket_id;
				$data['subscriber_id'] = $tktobj->subscriber_id;
				$data['subscriber_uuid'] = $tktobj->subscriber_uuid;
				$data['ticket_type'] = $tktobj->ticket_type;
				$data['ticket_priority'] = $tktobj->ticket_priority;
				$data['ticket_assign_to'] = $tktobj->ticket_assign_to;
				$data['ticket_description'] = $tktobj->ticket_description;
				//$data['ticket_comment'] = $tktobj->ticket_comment;
				$data['ticket_added_on'] = $tktobj->added_on;
			}
		}
		
		$tcarr = array();
		$tktcommentQuery = $this->db->order_by('id', 'DESC')->get_where('sht_subscriber_tickets_comments', array('ticket_id' => $tktid));
		$tcnum_rows = $tktcommentQuery->num_rows();
		if($tcnum_rows > 0){
			$i = 0;
			foreach($tktcommentQuery->result() as $tcobj){
				$tcarr[$i] = $tcobj->comment." (".$tcobj->added_on.")";
				$i++;
			}
		}
		$data['ticket_comment'] = $tcarr;
		
		echo json_encode($data);
	}

	public function getticketnumber($tktid){
		$data = array();
		$tktQuery = $this->db->query("SELECT ticket_type, ticket_id, subscriber_uuid FROM sht_subscriber_tickets WHERE id = '".$tktid."'");
		$num_rows = $tktQuery->num_rows();
		if($num_rows > 0){
			$rowdata = $tktQuery->row();
			$data['ticket_number'] = $rowdata->ticket_id;
			$data['uuid'] = $rowdata->subscriber_uuid;
                        $data['ticket_type'] = $rowdata->ticket_type;
		}
		return $data;
	}
	public function changeticket_statusconfirmation(){
		$isp_userid = $this->session->userdata['isp_session']['userid'];
		$chngtktstatus = $this->input->post('chngtktstatus');
		$chngtktid = $this->input->post('chngtktstatusid');
		$chngtktstatus_desc = $this->input->post('chngtktstatus_description');
		if($chngtktstatus != '1'){
			$this->db->update('sht_subscriber_tickets', array('status' => $chngtktstatus), array('id' => $chngtktid));
		}else{
			$this->db->update('sht_subscriber_tickets', array('closed_on' => date('Y-m-d H:i:s'), 'status' => '1'), array('id' => $chngtktid));
		}
		if(isset($chngtktstatus_desc) && $chngtktstatus_desc != ''){
			$ticketRequestData = array(
				'ticket_id' => $chngtktid,
				'ticket_edit_by' => $isp_userid,
				'comment' => $chngtktstatus_desc,
				'added_on' => date('Y-m-d H:i:s')
			);
			$this->db->insert('sht_subscriber_tickets_comments', $ticketRequestData);
		}
		
		if($chngtktstatus == '0'){ $status = 'Open'; }
		elseif($chngtktstatus == '1'){ $status = 'Closed'; }
		elseif($chngtktstatus == '2'){ $status = 'Pending'; }
		
		$ticketArr = $this->getticketnumber($chngtktid);
		$ticketnumber = $ticketArr['ticket_number'];
		$uuid = $ticketArr['uuid'];
		$tickettype = $ticketArr['ticket_type'];
		$this->notify_model->add_user_activitylogs($uuid, "Ticket $ticketnumber $status");
		
		$useralerts_status = $this->useralerts_status($uuid);
		$sms_alert = $useralerts_status['sms_alert'];
		$email_alert = $useralerts_status['email_alert'];
		if(($sms_alert == 1) && ($chngtktstatus == '1')){
			$this->notify_model->ticket_status_alert($uuid, $tickettype, $ticketnumber, $tktstatus);
		}

		echo json_encode($chngtktid);
	}
	public function show_ticket_summary(){
		$gen = '';
		$tktid = $this->input->post('tktid');
		$tktcommentQuery = $this->db->order_by('id', 'DESC')->get_where('sht_subscriber_tickets_comments', array('ticket_id' => $tktid));
		$tcnum_rows = $tktcommentQuery->num_rows();
		if($tcnum_rows > 0){
			foreach($tktcommentQuery->result() as $tktobj){
				$gen .= '<li style="list-style-type:none;clear:both;"><span style="float:left;color:#a1a1a1">'.$tktobj->comment.'</span> <span style="float:right;color:#a1a1a1">DateAdded: '.$tktobj->added_on.'</span></li>';
			}
		}else{
			$gen .= '<li style="list-style-type:none">No Comments Added !!</li>';
		}
		
		echo json_encode($gen);
	}
	public function updateTicketMember(){
		$uuid = $this->input->post('subscriber_uuid');
		$tktid = $this->input->post('tktid_toupdate');
		$ticket_assign_to = $this->input->post('team_member');
		$this->db->update('sht_subscriber_tickets', array('ticket_assign_to' => $ticket_assign_to), array('id' => $tktid));
		
		$tkt_number = '';
		$chktktstatus = $this->db->query("SELECT status,ticket_id FROM sht_subscriber_tickets WHERE id='".$tktid."'");
		if($chktktstatus->num_rows() > 0){
			$tktrowdata = $chktktstatus->row();
			$tkt_number = $tktrowdata->ticket_id;
			if($tktrowdata->status == '1'){
				$this->db->update('sht_subscriber_tickets', array('status' => '2'), array('id' => $tktid));
			}
		}
		
		$isp_userid = $this->session->userdata['isp_session']['userid'];
		$chngtktstatus_desc = $this->input->post('chngtktstatus_description');
		if(isset($chngtktstatus_desc) && $chngtktstatus_desc != ''){
			$ticketRequestData = array(
				'ticket_id' => $tktid,
				'ticket_edit_by' => $isp_userid,
				'comment' => $chngtktstatus_desc,
				'added_on' => date('Y-m-d H:i:s')
			);
			$this->db->insert('sht_subscriber_tickets_comments', $ticketRequestData);
		}
		
		$this->db->update('sht_subscriber_tickets_assignment', array('status' => '0'), array('ticket_id' => $tktid));
		$this->db->insert('sht_subscriber_tickets_assignment', array('ticket_id' => $tktid, 'ticket_assign_to' => $ticket_assign_to, 'comments' => '', 'status' => '1', 'added_on' => date('Y-m-d H:i:s')));
		
		$issuedUserArr = $this->getticket_sortername($ticket_assign_to);
		$issuedto = $issuedUserArr['tkt_issuesto'];
		$this->notify_model->add_user_activitylogs($uuid, "Update Ticket assigned to $issuedto");

		$this->notify_model->ticket_assignment_alert($uuid, $ticket_assign_to, $tkt_number, 'team_member');
		$useralerts_status = $this->useralerts_status($uuid);
		$sms_alert = $useralerts_status['sms_alert'];
		$email_alert = $useralerts_status['email_alert'];
		if($sms_alert == 1){
			$this->notify_model->ticket_assignment_alert($uuid, $ticket_assign_to, $tkt_number, 'customer');
		}
		
		echo json_encode(true);
	}
	public function update_ticketassigner(){
		$query = $this->db->query("SELECT id, ticket_assign_to,added_on FROM sht_subscriber_tickets WHERE ticket_assign_to != '0'");
		if($query->num_rows() > 0){
			foreach($query->result() as $qobj){
				$tktid = $qobj->id;
				$tkt_assignto = $qobj->ticket_assign_to;
				$added_on = $qobj->added_on;
				
				$this->db->insert('sht_subscriber_tickets_assignment', array('ticket_id' => $tktid, 'ticket_assign_to' => $tkt_assignto, 'comments' => '', 'status' => '1', 'added_on' => $added_on ));
			}
		}
	}
	
	public function getticketstype(){
		$gen = '';
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$tkttypeQ = $this->db->query("SELECT * FROM sht_tickets_type WHERE isp_uid='".$isp_uid."' ORDER BY id DESC");
		if($tkttypeQ->num_rows() > 0){
			$i = 1;
			foreach($tkttypeQ->result() as $tktobj){
				/*$checktktassign = $this->db->query("SELECT id FROM sht_subscriber_tickets WHERE ticket_type='".$tktobj->ticket_type_value."' AND isp_uid='".$isp_uid."'");
				if($checktktassign->num_rows() > 0){
					$gen .= "<tr><td>".$i."</td><td>".$tktobj->ticket_type."</td><td style='text-align:right;'>&nbsp;</td><td>&nbsp;</td></tr>";
				}else{
					$gen .= "<tr><td>".$i."</td><td>".$tktobj->ticket_type."</td><td style='text-align:right;'><a href='javascript:void(0)' onclick='formedit_ticket_types(".$tktobj->id.")'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></td><td><a href='javascript:void(0)' onclick='delete_ticket_types(".$tktobj->id.")'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td></tr>";
				}*/
				$gen .= "<tr><td>".$i."</td><td>".$tktobj->ticket_type."</td><td style='text-align:right;'>&nbsp;</td><td>&nbsp;</td></tr>";
				$i++;
			}
		}else{
			$gen = "<tr><td style='text-align:center' colspan='3'> No Records Found </td></tr>";
		}
		echo json_encode($gen);
	}
	public function ticketstype_optionslist(){
		$gen = "<option value=''>Select Ticket Type</option>";
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$tkttypeQ = $this->db->query("SELECT * FROM sht_tickets_type WHERE isp_uid='".$isp_uid."' ORDER BY id DESC");
		if($tkttypeQ->num_rows() > 0){
			$i = 1;
			foreach($tkttypeQ->result() as $tktobj){
				$gen .= "<option value='".$tktobj->ticket_type_value."'>".$tktobj->ticket_type."</option>";
			}
		}
		return $gen;
	}
	public function tickettype_iddetails(){
		$data = array();
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$tickettypeid = $this->input->post('tktypeid');
		$tkttypeQ = $this->db->query("SELECT * FROM sht_tickets_type WHERE id='".$tickettypeid."'");
		if($tkttypeQ->num_rows() > 0){
			$data = $tkttypeQ->row_array();
		}
		echo json_encode($data);
	}
	public function add_tickettypes(){
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$tickettypeid = $this->input->post('tickettypeid');
		$tickettype_name = ucwords($this->input->post('tickettype_name'));
		$tickettype_value = str_replace(' ', '_', $tickettype_name);
		
		$tkttypearr = array('isp_uid' => $isp_uid, 'ticket_type' => $tickettype_name, 'ticket_type_value' => strtolower($tickettype_value), 'added_on' => date('Y-m-d H:i:s'));
		if($tickettypeid != ''){
			$this->db->update('sht_tickets_type', $tkttypearr, array('id' => $tickettypeid));
		}else{
			$this->db->insert('sht_tickets_type', $tkttypearr);
		}
		echo json_encode(true);
	}
	public function delete_tickettypes(){
		$data = array();
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$tickettypeid = $this->input->post('tktypeid');
		$tkttypeQ = $this->db->query("SELECT * FROM sht_tickets_type WHERE id='".$tickettypeid."'");
		if($tkttypeQ->num_rows() > 0){
			$this->db->delete('sht_tickets_type', array('id' => $tickettypeid));
		}
		echo json_encode(true);
	}

/*------------------- SUBSCRIBER PLAN METHODS START ----------------------------------------------------------*/	

	public function affected_date_on_wallet($uid){
		$action_date = '';
		$wallet_date = ''; $passbook_date = '';
		$lastwallet_addedQ = $this->db->query("SELECT DATE(added_on) as wallet_date FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uid."' ORDER BY id DESC LIMIT 1");
		$lnumrows = $lastwallet_addedQ->num_rows();
		if($lnumrows > 0){
			$wallet_date = $lastwallet_addedQ->row()->wallet_date;
		}
		
		$lastpassbook_addedQ = $this->db->query("SELECT DATE(added_on) as passbook_date FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uid."' ORDER BY id DESC LIMIT 1");
		$pnumrows = $lastpassbook_addedQ->num_rows();
		if($pnumrows > 0){
			$passbook_date = $lastpassbook_addedQ->row()->passbook_date;
		}
		
		if(($wallet_date != '') && ($passbook_date != '')){
			if(strtotime($wallet_date) >= strtotime($passbook_date)){
				$action_date = $wallet_date;
			}else{
				$action_date = $passbook_date;
			}
		}elseif(($wallet_date == '') && ($passbook_date != '')){
			$action_date = $passbook_date;
		}elseif(($wallet_date != '') && ($passbook_date == '')){
			$action_date = $wallet_date;
		}elseif(($wallet_date == '') && ($passbook_date == '')){
			/*$advpayQ = $this->db->query("SELECT SUM(advancepay_for_months) as advancepay_for_months, SUM(number_of_free_months) as number_of_free_months FROM sht_advance_payments WHERE uid='".$uid."' AND balance_left != '0.00'");
			if($advpayQ->num_rows() > 0){
				$advrowdata = $advpayQ->row();
				$advancepay_for_months = $advrowdata->advancepay_for_months;
				$number_of_free_months = $advrowdata->number_of_free_months;
				$payformonths = $advancepay_for_months + $number_of_free_months;
				$action_date = date("Y-m-d", strtotime(" +$payformonths months"));
			}else{
				$subsAcctQ = $this->db->query("SELECT plan_activated_date FROM sht_users WHERE uid='".$uid."'");
				if($subsAcctQ->num_rows() > 0){
					$rowdata = $subsAcctQ->row();
					$action_date = $rowdata->plan_activated_date;
				}
			}*/
			$subsAcctQ = $this->db->query("SELECT plan_activated_date FROM sht_users WHERE uid='".$uid."'");
			if($subsAcctQ->num_rows() > 0){
				$rowdata = $subsAcctQ->row();
				$action_date = $rowdata->plan_activated_date;
			}
			
		}
		
		return $action_date;
	}

	public function userassoc_planvalidity($uuid){
		$data = array();
		$subsplanQ = $this->db->query("SELECT plan_activated_date,paidtill_date,expiration,next_bill_date FROM sht_users WHERE uid='".$uuid."'");
		$numrows = $subsplanQ->num_rows();
		if($numrows > 0){
			$planrow = $subsplanQ->row();
			$data['plan_activatedon'] = ($planrow->plan_activated_date == '0000-00-00') ? '-' : date('d-m-Y', strtotime($planrow->plan_activated_date));
			//$data['paidtill_date'] = ($planrow->paidtill_date == '0000-00-00') ? '-' : date('d-m-Y', strtotime($planrow->paidtill_date));
			$data['expiration_date'] = ($planrow->expiration == '0000-00-00 00:00:00') ? '-' : date('d-m-Y', strtotime($planrow->expiration));
			$data['next_bill_date'] = ($planrow->next_bill_date == '0000-00-00') ? '-' : date('d-m-Y', strtotime($planrow->next_bill_date));
		}
		//print_r($data); die;
		return $data;
	}
	public function user_state_city_zone($uid){
		$data = array();
		$query = $this->db->query("SELECT state,city,zone FROM sht_users WHERE uid='".$uid."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			$data['state'] = $rowdata->state;
			$data['city'] = $rowdata->city;
			$data['zone'] = $rowdata->zone;
		}
		return $data;
	}
	public function sht_plan_regionwise($subscriber_uuid){
		$data = array();
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		
		$user_regionArr = $this->user_state_city_zone($subscriber_uuid);
		$user_state = $user_regionArr['state'];
		$user_city = $user_regionArr['city'];
		$user_zone = $user_regionArr['zone'];
		
		$cond1 = "( state_id='".$user_state."' AND city_id='".$user_city."' AND zone_id='".$user_zone."' )";
		$cond2 = " (state_id='all') ";
		$cond3 = " (state_id='".$user_state."' AND city_id='".$user_city."' AND zone_id='all') ";
		$cond4 = " (state_id='".$user_state."' AND city_id='all') ";
		
		$query = $this->db->query("SELECT plan_id FROM sht_plan_region WHERE ($cond1 OR $cond2 OR $cond3 OR $cond4) AND status='1' AND is_deleted='0' AND isp_uid='".$isp_uid."'");
		//echo $this->db->last_query();
		if($query->num_rows() > 0){
			$i = 0;
			foreach($query->result() as $pobj){
				$data[$i] = $pobj->plan_id;
				$i++;
			}
		}
		return $data;
	}
	public function active_planlist($subscriber_uuid=''){
		$plans_inregion = $this->sht_plan_regionwise($subscriber_uuid);
		if(count($plans_inregion) > 0){
			$plans_inregion = implode(',' , $plans_inregion);
		}else{
			$plans_inregion = 0;
		}
		
		$planQ = $this->db->query("SELECT tb1.srvid, tb1.srvname, tb2.net_total, tb2.region_type FROM sht_services as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb1.enableplan='1' AND is_deleted='0' AND tb1.plantype != '0' AND (tb2.srvid IN($plans_inregion) OR tb2.region_type != 'region') ");
		$num_rows = $planQ->num_rows();
		$gen = "<option value=''>Select Plan<sup>*</sup></option>";
		if($num_rows > 0){
			foreach($planQ->result() as $pobj){
				$gen .= '<option value="'.$pobj->srvid.'">'.$pobj->srvname.'</option>';
			}
		}
		return $gen;
	}
	public function nextcycle_planlist($subscriber_uuid=''){
		$data = array();
		$nextplanid = '0'; $nextuser_plan_type = ''; $prebill_oncycle = '0';
		$nextplanQ = $this->db->query("SELECT baseplanid,user_plan_type,prebill_oncycle FROM sht_nextcycle_userplanassoc WHERE uid='".$subscriber_uuid."' AND status='0' ORDER BY id DESC LIMIT 1");
		if($nextplanQ->num_rows() > 0){
			$nextplanrow = $nextplanQ->row();
			$nextplanid = $nextplanrow->baseplanid;
			$nextuser_plan_type = $nextplanrow->user_plan_type;
			$prebill_oncycle = $nextplanrow->prebill_oncycle;
		}
		$active_planid = ''; $nwhere = '';
		$curr_planQ = $this->db->query("SELECT baseplanid,user_plan_type,planautorenewal FROM sht_users WHERE uid='".$subscriber_uuid."' AND enableuser='1' AND expired='0'");
		$curr_planrow = $curr_planQ->row();
		$curr_activeplan = $curr_planrow->baseplanid;
		$curr_user_plan_type = $curr_planrow->user_plan_type;
		
		$data['planautorenewal'] = $curr_planrow->planautorenewal;
		$data['curr_user_plan_type'] = $curr_user_plan_type;
		if($nextuser_plan_type != ''){
			$data['nxtuser_plan_type'] = $nextuser_plan_type;
		}else{
			$data['nxtuser_plan_type'] = $curr_user_plan_type;
		}
		
		$advpayQ = $this->db->query("SELECT id FROM sht_advance_payments WHERE uid='".$subscriber_uuid."' AND (balance_left != '0.00' OR bonus_amount > 0) ORDER BY id ASC");
		if($advpayQ->num_rows() > 0){
			$data['advance_prepay'] = '1';
		}else{
			$data['advance_prepay'] = '0';
		}
		
		$plans_inregion = $this->sht_plan_regionwise($subscriber_uuid);
		if(count($plans_inregion) > 0){
			$active_planid = array($curr_activeplan);
			if(in_array($curr_activeplan, $plans_inregion)){
				//$plans_inregion = array_diff($plans_inregion, $active_planid);
				if(count($plans_inregion) > 0){
					$plans_inregion = implode(',' , $plans_inregion);
					$nwhere .= " AND (tb2.srvid IN($plans_inregion) OR tb2.region_type != 'region')";
				}else{
					$nwhere .= " AND tb2.region_type != 'region'";
				}
			}else{
				$plans_inregion = implode(',' , $plans_inregion);
				$nwhere .= " AND (tb2.srvid IN($plans_inregion) OR (tb2.srvid != '".$curr_activeplan."' AND tb2.region_type != 'region'))";
			}
		}else{
			$nwhere .= " AND tb2.srvid != '".$curr_activeplan."' AND tb2.region_type != 'region'";
		}
		
		$planQ = $this->db->query("SELECT tb1.srvid, tb1.srvname, tb2.net_total, tb2.region_type FROM sht_services as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb1.enableplan='1' AND tb1.is_deleted='0' AND tb1.plantype != '0' $nwhere ");
		//echo $this->db->last_query(); die;
		$num_rows = $planQ->num_rows();
		$gen = "<option value=''>Select Plan<sup>*</sup></option>";
		if($num_rows > 0){
			foreach($planQ->result() as $pobj){
				$sel = '';
				if($nextplanid == $pobj->srvid){
					$sel = 'selected="selected"';
				}
				$gen .= '<option value="'.$pobj->srvid.'" '.$sel.'>'.$pobj->srvname.'</option>';
			}
		}

		$data['active_planlist'] = $gen;
		$data['nextplanid'] = $nextplanid;
		$data['nxtprebill_oncycle'] = $prebill_oncycle;
		echo json_encode($data);
	}

	public function getplan_pricing($srvid, $uuid=''){
		$data = array();
		$query = $this->db->query("SELECT tb2.net_total, tb2.gross_amt, tb1.plan_duration FROM sht_services as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb2.srvid='".$srvid."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			$tax = $this->current_taxapplicable();
			$plan_duration = $rowdata->plan_duration;
			$gross_amt_wt = $rowdata->gross_amt;
			
			$custom_planpriceArr = $this->getcustom_planprice($uuid, $srvid);
			$custom_planprice = $custom_planpriceArr['gross_amt'];
			if($custom_planprice != ''){
				$planprice = round($custom_planprice);
			}else{
				$gross_amt = ($gross_amt_wt * $plan_duration);
				$planprice = round($gross_amt + (($gross_amt * $tax)/100));	
			}
			
			$plan_tilldays = ($plan_duration * 30);
			$data['plan_price'] = $planprice;
			$data['plan_tilldays'] = $plan_tilldays;
		}
		$ispcodet = $this->countrydetails();
		$data['currency'] = $ispcodet['currency'];
		return $data;
		
	}
	public function current_plan_pricing($srvid, $uuid=''){
		$data = array();
		$query = $this->db->query("SELECT tb2.net_total, tb2.gross_amt, tb1.plan_duration FROM sht_services as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb2.srvid='".$srvid."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			$tax = $this->current_taxapplicable();
			$plan_duration = $rowdata->plan_duration;
			$gross_amt_wt = $rowdata->gross_amt;
			
			$custom_planpriceArr = $this->getcustom_planprice($uuid, $srvid);
			$custom_planprice = $custom_planpriceArr['gross_amt'];
			if($custom_planprice != ''){
				$planprice = round($custom_planprice);
			}else{
				$planprice = round($gross_amt_wt + (($gross_amt_wt * $tax)/100));	
			}
			
			$data['plan_price'] = $planprice;
			$data['plan_duration'] = $plan_duration;
		}
		$ispcodet = $this->countrydetails();
		$data['currency'] = $ispcodet['currency'];
		return $data;
	}
	public function getuser_creditlimit($srvid, $uuid=''){
		$user_creditlimit = 0;
		$query = $this->db->query("SELECT tb2.net_total, tb2.gross_amt, tb1.plan_duration FROM sht_services as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb2.srvid='".$srvid."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			$tax = $this->current_taxapplicable();
			$plan_duration = $rowdata->plan_duration;
			$gross_amt_wt = $rowdata->gross_amt;
			$gross_amt = round($gross_amt_wt + (($gross_amt_wt * $tax)/100));
				
			$custom_planpriceArr = $this->getcustom_planprice($uuid, $srvid);
			$custom_planprice = $custom_planpriceArr['gross_amt'];
			if($custom_planprice != ''){
				$gross_amt = round($custom_planprice / $plan_duration);
				$user_creditlimit = $custom_planprice + round($gross_amt / 2);
			}else{
				$planprice = ($gross_amt * $plan_duration);
				$user_creditlimit = $planprice + round($gross_amt / 2);
			}
		}
		$user_creditlimit = round($user_creditlimit);
		return $user_creditlimit;
	}
	public function get_miscellaneous_detail($id, $rowdata){
		$query = $this->db->query("SELECT $rowdata FROM  sht_services  WHERE srvid='".$id."'");
		$row = $query->row();
		return $row->$rowdata;
	}
	public function getplan_details(){
		$data = array();
		$planid = $this->input->post('planid');
		$subscriber_uuid = $this->input->post('cust_uuid');
		$nextplancycle = $this->input->post('nextplancycle');
		$user_account_type = '';
		$pquery = $this->db->query('SELECT baseplanid, enableuser, account_activated_on, user_plan_type, prebill_oncycle, planautorenewal FROM sht_users WHERE uid = "'.$subscriber_uuid.'"');
		if(isset($nextplancycle) && $nextplancycle == '1'){
			$npquery = $this->db->get_where('sht_nextcycle_userplanassoc', array('uid' => $subscriber_uuid, 'status' => '0'));
			if($npquery->num_rows() > 0){
				$planid = $npquery->row()->baseplanid;
			}
		}else{
			if(isset($subscriber_uuid) && $subscriber_uuid != ''){
				$data['active_planlist'] = $this->active_planlist($subscriber_uuid);			
				if($pquery->num_rows() > 0){
					$planrow = $pquery->row();
					$planid = $planrow->baseplanid;
					$activated_on = $planrow->account_activated_on;
				}
			}
		}
		if($pquery->num_rows() > 0){
			$planrow1 = $pquery->row();
			$user_plan_type = $planrow1->user_plan_type;
			$data['user_plan_type'] = $user_plan_type;
			$data['enableuser'] = $planrow1->enableuser;
			$data['prebill_oncycle'] = $planrow1->prebill_oncycle;
			$data['planautorenewal'] = $planrow1->planautorenewal;
		}
		
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		
		$custupload_speed = 0; $custdownload_speed = 0;
		$customize_fupspeedQ = $this->db->query("SELECT * FROM sht_customize_plan_speed WHERE uid='".$subscriber_uuid."' AND isp_uid='".$isp_uid."' AND status='1'");
		if($customize_fupspeedQ->num_rows() > 0){
			$custfup_rowdata = $customize_fupspeedQ->row();
			$custupload_speed = $custfup_rowdata->upload_speed;
			$custdownload_speed = $custfup_rowdata->download_speed;
		}
		
		$ispcodet = $this->countrydetails();
		$data['currency'] = $ispcodet['currency'];
		$query = $this->db->query("SELECT tb1.*, tb2.net_total, tb2.gross_amt, tb2.region_type FROM sht_services as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb1.enableplan='1' AND tb1.srvid='".$planid."'");
		$num_rows = $query->num_rows();
		if($num_rows > 0){
			foreach($query->result() as $pobj){
				$data['plan_id'] = $planid;
				$data['plan_type_id'] = '1';
				$data['plan_type'] = 'Unlimited Plan';
				$data['plan_name'] = $pobj->srvname;
				$data['plan_desc'] = $pobj->descr;
				$data['plan_validity'] = $pobj->validity;
				
				if($pobj->plantype != '3'){
					if($custupload_speed == '0'){
						$custupload_speed = $pobj->uprate;
					}
					if($custdownload_speed == '0'){
						$custdownload_speed = $pobj->downrate;
					}
					$data['plan_dwnld_rate'] = $this->convertTodata($custdownload_speed.'KB');
					$data['plan_upld_rate'] = $this->convertTodata($custupload_speed.'KB');
				}else{
					$data['plan_dwnld_rate'] = $this->convertTodata($pobj->downrate.'KB');
					$data['plan_upld_rate'] = $this->convertTodata($pobj->uprate.'KB');
				}
				$data['plan_duration'] = $pobj->plan_duration;
				
				
				$tax = $this->current_taxapplicable();
				$gross_amt = $pobj->gross_amt;
				$plan_duration = $pobj->plan_duration;
				$gross_amt = ($pobj->gross_amt * $plan_duration);
				$planprice = round($gross_amt + (($gross_amt * $tax)/100));
				$plan_tilldays = ($plan_duration * 30);
				
				$custom_planprice = $this->getcustom_planprice($subscriber_uuid, $planid);
				$data['custom_planprice'] = $custom_planprice['gross_amt'];
				
				
				$data['plan_net_amount'] = $planprice;
				$data['plandatalimit'] = 0;
				$data['plandata_calculatedon'] = 0;
				if($pobj->plantype == '2'){
					$data['plan_type'] = 'Time Plan';
					$data['plan_type_id'] = '2';
					$time_calculated_on = $this->get_miscellaneous_detail($planid, 'timecalc');
					if($time_calculated_on == '1'){
						$time_calculated_on = 'Login Time';
					}else{
						$time_calculated_on = 'Online Time';
					}
					$data['plan_time_calculated_on'] = $time_calculated_on;
					$data['plan_timelimit'] = $pobj->timelimit;
				
				}else if($pobj->plantype == '3'){
					$data['plan_type_id'] = '3';
					$data['plan_type'] = 'FUP Plan';
					$data['plan_data_limit'] = $this->convertTodata($pobj->datalimit.'GB');
					$data_calculated_on = $this->get_miscellaneous_detail($planid, 'datacalc');
					if($data_calculated_on == '1'){
						$data_calculated_on = 'Download Only';
					}else{
						$data_calculated_on = 'Download Upload Combined';
					}
					
					if($custupload_speed == '0'){
						$custupload_speed = $pobj->fupuprate;
					}
					if($custdownload_speed == '0'){
						$custdownload_speed = $pobj->fupdownrate;
					}
					
					$data['plan_data_calculated_on'] = $data_calculated_on;
					$data['plan_postfup_dwnld_rate'] = $this->convertTodata($custdownload_speed.'KB');
					$data['plan_postfup_upld_rate'] = $this->convertTodata($custupload_speed.'KB');
					$data['plandatalimit'] = $pobj->datalimit;
					$data['plandata_calculatedon'] = $this->get_miscellaneous_detail($planid, 'datacalc');
					
				}else if($pobj->plantype == '4'){
					$data['plan_type_id'] = '4';
					$data['plan_type'] = 'Data Plan';
					$data['plan_data_limit'] = $this->convertTodata($pobj->datalimit.'GB');
					$data_calculated_on = $this->get_miscellaneous_detail($planid, 'datacalc');
					if($data_calculated_on == '1'){
						$data_calculated_on = 'Download Only';
					}else{
						$data_calculated_on = 'Download Upload Combined';
					}
					$data['plan_data_calculated_on'] = $data_calculated_on;
					$data['plandatalimit'] = $pobj->datalimit;
					$data['plandata_calculatedon'] = $this->get_miscellaneous_detail($planid, 'datacalc');
				}
			}
		}
		
		$next_subsplanQ = $this->db->get_where('sht_nextcycle_userplanassoc', array('uid' => $subscriber_uuid, 'status' => '0'));
		$nextplan_numrows = $next_subsplanQ->num_rows();
		if($nextplan_numrows > 0){
			$nextplanrow = $next_subsplanQ->row();
			$data['next_cycle_plan'] = $nextplanrow->baseplanid;
		}
		
		$userassoc_planvalidity = $this->userassoc_planvalidity($subscriber_uuid);
		
		$data['plan_activatedon'] = (isset($userassoc_planvalidity['plan_activatedon'])) ? $userassoc_planvalidity['plan_activatedon'] : '-';
		//$data['paidtill_date'] = (isset($userassoc_planvalidity['paidtill_date'])) ? $userassoc_planvalidity['paidtill_date'] : '-';
		$data['expiration_date'] = $this->update_expirationdate($subscriber_uuid);
		$data['next_bill_date'] = (isset($userassoc_planvalidity['next_bill_date'])) ? $userassoc_planvalidity['next_bill_date'] : '-';
		
		//$data['live_usage_data'] = $this->live_usage($subscriber_uuid);
		
		echo json_encode($data);
	}
	
	public function update_expirationdate($uuid){
		$userassoc_plandata = $this->userassoc_plandata($uuid);
		$user_credit_limit = $userassoc_plandata['user_credit_limit'];
		$user_wallet_balance = $this->userbalance_amount($uuid);
		$plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
		$total_advpay = $userassoc_plandata['total_advpay'];
		$actiondate = $this->affected_date_on_wallet($uuid);
		$expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
		$expiration_date = date('d-m-Y', strtotime($actiondate . " +".$expiration_acctdays." days"));
		$expiration_ddate = date('Y-m-d', strtotime($expiration_date)).' 00:00:00';
		$this->db->update('sht_users', array('expiration' => $expiration_ddate), array('uid' => $uuid));
		
		return $expiration_date;
	}
	
	public function planassoc_withuser(){
		$subscid = $this->input->post('subsid');
		$subsc_uuid = $this->input->post('subs_uuid');
		$subsplanQ = $this->db->query("SELECT baseplanid FROM sht_users WHERE uid = '".$subsc_uuid."'");
		$numrows = $subsplanQ->num_rows();
		if($numrows > 0){
			$planid = $subsplanQ->row()->baseplanid;
			echo json_encode($planid);
		}else{
			echo json_encode($numrows);
		}
	}
	public function add_subscriber_plan(){
		$data = array();
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		
		$subscriber_id = $this->input->post('subscriber_userid');
		$subscriber_uuid = $this->input->post('subscriber_uuid');
		$plan_id = $this->input->post('assign_plan');
		$plan_typeid = $this->input->post('plan_type_id');
		$user_account_status = $this->user_account_status($subscriber_uuid);
		$plandatalimit = $this->input->post('plandatalimit');
		$plandata_calculatedon = $this->input->post('plandata_calculatedon');
		$user_plan_type = $this->input->post('user_plan_type');
		$prebill_oncycle = $this->input->post('prebill_oncycle');
		if((int) $prebill_oncycle != 1){
			$prebill_oncycle = '0';
		}
		
		$plandatarr = array(
			'baseplanid' => $this->input->post('assign_plan')
		);
		$plandatarr['user_plan_type'] = $user_plan_type;
		$plandatarr['prebill_oncycle'] = $prebill_oncycle;
		
		$custom_planprice = $this->input->post('custom_planprice');
		if($custom_planprice != ''){
			$this->addcustom_planprice($subscriber_uuid, $plan_id, $custom_planprice, 'Now');
		}else{
			$this->db->delete("sht_custom_plan_pricing", array('uid' => $subscriber_uuid, 'isp_uid' => $isp_uid));
		}
		
		$userassoc_planarr = array(
			'isp_uid' => $isp_uid,
			'uid' => $subscriber_uuid,
			'plantype' => $plan_typeid,
			'plan_id' => $plan_id,
			'added_on' => date('Y-m-d H:i:s')
		);
		$subsplanQ = $this->db->get_where('sht_users', array('uid' => $subscriber_uuid, 'baseplanid' => $plan_id));
		$plan_numrows = $subsplanQ->num_rows();
		if(isset($user_account_status) && $user_account_status == '1'){
		}else{
			$checkPlanQ = $this->db->get_where('sht_userplanassoc', array('uid' => $subscriber_uuid, 'status' => '1'));
			if($checkPlanQ->num_rows() > 0){
				$this->db->update('sht_userplanassoc', array('status' => '0'), array('uid' => $subscriber_uuid));
				$this->db->insert('sht_userplanassoc', $userassoc_planarr);
			}else{
				$this->db->insert('sht_userplanassoc', $userassoc_planarr);
			}
			
			if($plandata_calculatedon == '1'){
				$plandatarr['downlimit'] = $plandatalimit;
				$plandatarr['comblimit'] = '0';
			}elseif($plandata_calculatedon == '2'){
				$plandatarr['downlimit'] = '0';
				$plandatarr['comblimit'] = $plandatalimit;
			}else{
				$plandatarr['downlimit'] = '0';
				$plandatarr['comblimit'] = '0';
			}
			
			$this->db->update('sht_users', $plandatarr, array('uid' => $subscriber_uuid));
		}
		
		$this->db->update('sht_subscriber_activation_panel', array('plan_details' => '1'), array('subscriber_id' => $subscriber_id));
		
		$user_creditlimit = $this->getuser_creditlimit($plan_id, $subscriber_uuid);
		$this->db->update('sht_users', array('user_credit_limit' => $user_creditlimit), array('uid' => $subscriber_uuid));
		
		$ispcodet = $this->countrydetails();
		$data['user_creditlimit'] = $user_creditlimit;
		$data['currency'] = $ispcodet['currency'];
		
		$planname = $this->planname($plan_id);
		$this->notify_model->add_user_activitylogs($subscriber_uuid, "Plan: $planname Added");
		
		$useralerts_status = $this->useralerts_status($subscriber_uuid);
		$sms_alert = $useralerts_status['sms_alert'];
		$email_alert = $useralerts_status['email_alert'];
		if($sms_alert == 1){
			$this->notify_model->newplanchange_alert($subscriber_uuid, $plan_id, 'applyplan');
		}
		if($email_alert == 1){
			$this->emailer_model->newplanchangemail_alert($subscriber_uuid, $plan_id, 'applyplan');
		}
		echo json_encode($data);
	}
	public function add_nextcycleplan(){
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$nextuser_plan_type = $this->input->post('nextuser_plan_type');
		$prebill_oncycle = $this->input->post('nextprebill_oncycle');
		if((int) $prebill_oncycle != 1){
			$prebill_oncycle = '0';
		}
		
		$uuid = $this->input->post('subscriber_uuid');
		$currActivePlan = $this->db->query("SELECT uid,plan_activated_date,plan_changed_datetime, baseplanid, user_plan_type FROM sht_users WHERE uid='".$uuid."' AND DATE(expiration) >= CURRENT_DATE()");
		$numrows = $currActivePlan->num_rows();
		if($numrows > 0){
			$data['is_expired'] = 0;
			$plandatarr['isp_uid'] = $isp_uid;
			$plandatarr['baseplanid'] = $this->input->post('nextassign_plan');
			$plandatarr['uid'] = $this->input->post('subscriber_uuid');
			$plandatarr['created_on'] = date('Y-m-d H:i:s');
			$plandatarr['status'] = '0'; //Plan NOT USED YET
			$plandatarr['user_plan_type'] = $nextuser_plan_type;
			$plandatarr['prebill_oncycle'] = $prebill_oncycle;
			$next_subsplanQ = $this->db->get_where('sht_nextcycle_userplanassoc', array('uid' => $uuid, 'status' => '0'));
			$nextplan_numrows = $next_subsplanQ->num_rows();
			if($nextplan_numrows > 0){
				$this->db->update('sht_nextcycle_userplanassoc', array('baseplanid' => $plandatarr['baseplanid'], 'user_plan_type' => "$nextuser_plan_type", 'prebill_oncycle' => $prebill_oncycle, 'updated_on' => date('Y-m-d H:i:s')), array('uid' => $uuid));
			}else{
				$this->db->insert('sht_nextcycle_userplanassoc', $plandatarr);
			}
			
			$custom_planprice = $this->input->post('custom_planprice');
			$nextplanid = $this->input->post('nextassign_plan');
			if($custom_planprice != ''){
				$this->addcustom_planprice($uuid, $nextplanid, $custom_planprice, 'NextCycle');
			}else{
				$this->db->delete("sht_custom_plan_pricing", array('uid' => $uuid, 'isp_uid' => $isp_uid, 'action' => 'NextCycle'));
			}
			
			$data['nextplanid'] = $plandatarr['baseplanid'];
			
			$plan_id = $this->input->post('nextassign_plan');
			$planname = $this->planname($plan_id);
			$this->notify_model->add_user_activitylogs($uuid, "NextCycle Plan: $planname Added");
			
			$useralerts_status = $this->useralerts_status($uuid);
			$sms_alert = $useralerts_status['sms_alert'];
			$email_alert = $useralerts_status['email_alert'];
			if($sms_alert == 1){
				$this->notify_model->newplanchange_alert($uuid, $plan_id, 'nextcycle');
			}
			if($email_alert == 1){
				$this->emailer_model->newplanchangemail_alert($uuid, $plan_id, 'nextcycle');
			}
		}else{
			$data['is_expired'] = 1;
		}
		echo json_encode($data);
	}
	public function userassoc_plan_pricing(){
		$data = array();
		$price = 0; $planid = '';
		$subs_uuid = $this->input->post('subs_uuid');
		$userplanQ = $this->db->query("SELECT baseplanid FROM sht_users WHERE uid = '".$subs_uuid."'");
		if($userplanQ->num_rows() > 0){
			$planid = $userplanQ->row()->baseplanid;
			$priceArr = $this->getplan_pricing($planid, $subs_uuid);
			$price = $priceArr['plan_price'];
			$plan_tilldays = $priceArr['plan_tilldays'];
		}
		$data['planid'] = $planid;
		$data['plan_tilldays'] = $plan_tilldays;
		$data['price'] = $price;
		echo json_encode($data);
	}
	public function userassoc_planname($userid){
		$planname = '-';
		$query = $this->db->query("SELECT tb2.srvname FROM sht_users as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid=tb2.srvid) WHERE tb1.id='".$userid."'");
		if($query->num_rows() > 0){
			$planname = $query->row()->srvname;
		}
		return $planname;
	}
	public function planname($srvid){
		$planname = '-';
		$query = $this->db->query("SELECT tb2.srvname FROM sht_services as tb2 WHERE tb2.srvid='".$srvid."'");
		if($query->num_rows() > 0){
			$planname = $query->row()->srvname;
		}
		return $planname;
	}


	public function userassoc_plantype($subscriber_uuid){
		$user_currplanQ = $this->db->query("SELECT tb2.plantype, tb1.baseplanid FROM sht_users as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid=tb2.srvid) WHERE tb1.uid='".$subscriber_uuid."'");
		$current_plantype = $user_currplanQ->row()->plantype;
		return $current_plantype;
	}
	
	public function current_taxapplicable(){
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$query = $this->db->query("SELECT tax FROM sht_tax WHERE isp_uid='".$isp_uid."'");
		if($query->num_rows() > 0){
		    $rowdata = $query->row();
		    return $rowdata->tax;
		}
	}
	
	public function userassoc_plandata($subscriber_uuid){
		$data = array();
		$user_currplanQ = $this->db->query("SELECT tb2.datacalc, tb2.datalimit, tb2.plantype, tb2.plan_duration, tb1.baseplanid, tb1.user_plan_type, tb1.user_credit_limit, DATE(tb1.account_activated_on) as account_activated_on, tb1.paidtill_date, DATE(tb1.expiration) as expiration_date, tb3.net_total, tb3.gross_amt FROM sht_users as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid=tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb1.uid='".$subscriber_uuid."'");
		$current_planrow = $user_currplanQ->row();
		$tax = $this->current_taxapplicable();
		$plan_duration = $current_planrow->plan_duration;
		$srvid = $current_planrow->baseplanid;
		
		$custom_planpriceArr = $this->getcustom_planprice($subscriber_uuid, $srvid);
		$custom_planprice = $custom_planpriceArr['gross_amt'];
		if($custom_planprice != ''){
			$gross_amt = round($custom_planprice);
			$plan_price = round($custom_planprice);
		}else{
			$gross_amt = ($current_planrow->gross_amt * $plan_duration);
			$plan_price = round($gross_amt + (($gross_amt * $tax)/100));
		}
		
		$data['plan_duration'] = $plan_duration;
		$data['datacalc'] = $current_planrow->datacalc;
		$data['datalimit'] = $current_planrow->datalimit;
		$data['planid'] = $current_planrow->baseplanid;
		$data['plan_price'] = $plan_price;
		$data['plantype'] = $current_planrow->plantype;
		$data['user_credit_limit'] = $current_planrow->user_credit_limit;
		$data['account_activated_on'] = $current_planrow->account_activated_on;
		//$data['paidtill_date'] = $current_planrow->paidtill_date;
		
		$plan_tilldays = ($plan_duration * 30);
		$data['plan_tilldays'] = $plan_tilldays;
		$data['plan_cost_perday'] = round($plan_price/$plan_tilldays);
		$data['expiration_date'] = $current_planrow->expiration_date;
		$data['user_plan_type'] = $current_planrow->user_plan_type;
		$data['total_advpay'] = $this->user_advpayments($subscriber_uuid);
		return $data;
	}
	

	public function calcPrevPlanCost(){
		$data = array();
		$live_usage =0;
		$percent = 0;
		$uuid = $this->input->post('uuid');
		$today_datetime = date('Y-m-d H:i:s');
		
		$currActivePlan = $this->db->query("SELECT uid,plan_activated_date,plan_changed_datetime, baseplanid, user_plan_type FROM sht_users WHERE uid='".$uuid."' AND DATE(expiration) >= CURRENT_DATE()");
		$numrows = $currActivePlan->num_rows();
		if($numrows > 0){
			$data['is_expired'] = 0;
			$rowdata = $currActivePlan->row();
			$baseplanid = $rowdata->baseplanid;
			$planPriceArr = $this->getplan_pricing($baseplanid, $uuid);
			$plan_price = $planPriceArr['plan_price'];
			$plan_tilldays = $planPriceArr['plan_tilldays'];
			
			$user_plan_type = $rowdata->user_plan_type;
			
			$plan_changed_datetime = $rowdata->plan_changed_datetime;
			if($plan_changed_datetime == '0000-00-00 00:00:00'){
				$start_datetime = $rowdata->plan_activated_date.' 00:00:00';	
			}else{
				$start_datetime = $rowdata->plan_changed_datetime;
			}
			$planinfoQ = $this->db->query("SELECT plantype, datalimit, datacalc, plan_duration FROM sht_services WHERE srvid='".$baseplanid."'");
			if($planinfoQ->num_rows() > 0){
				$prowdata = $planinfoQ->row();
				$plantype = $prowdata->plantype;
				$datalimit = $prowdata->datalimit;
				$datacalc = $prowdata->datacalc;
				$plan_duration = $prowdata->plan_duration;
				
				$pacttimestamp = strtotime($start_datetime);
				$todtimestamp = strtotime($today_datetime);
				$used_netdays = (abs($todtimestamp - $pacttimestamp) / 86400) + 1;
				$cost_per_day = ($plan_price/$plan_tilldays);
				$cost_tillnow = round($cost_per_day * $used_netdays);
				
				$query = $this->db->query("select acctinputoctets, acctoutputoctets from radacct where username = '$uuid' AND acctstarttime between '$start_datetime' and '$today_datetime'");
				foreach($query->result() as $row){
					if($datacalc == 2){
						$live_usage = $live_usage + $row->acctinputoctets + $row->acctoutputoctets;
					}else{
						$live_usage = $live_usage + $row->acctoutputoctets;
					}
				}
				$live_usage_value = round($live_usage/(1024*1024*1024),2);
				if($live_usage_value < 1){
					//get in mb
					$live_usage_value = round($live_usage/(1024*1024),2);
				if($live_usage_value < 1){
					//get in kb
					$live_usage_value = round($live_usage/(1024),2)."KB";
				}else{
					$live_usage_value = $live_usage_value."MB" ;
				}
				}else{
					$live_usage_value = $live_usage_value."GB";
				}
				
				if($datalimit == 0){
					$percent = 101;	
				}else{
					$percent = ($live_usage*100)/$datalimit;
				}
				if($percent > 100){
					$data['total_data'] = $live_usage_value;
					$data['total_data_cost'] = $cost_tillnow;
				}else{
					$data['total_data'] = $live_usage_value;
					$data['total_data_cost'] = round(($plan_price * $live_usage) / $datalimit);
				}
				
				if($user_plan_type == 'prepaid'){
					$prev_billdata = $this->getbillid_forchangeplan($uuid, $baseplanid);
					$data['prev_billid'] = $prev_billdata['billid'];
					$data['prev_billpaid'] = $prev_billdata['billpaid'];
				}else{
					$data['prev_billid'] = '0';
					$data['prev_billpaid'] = '0';
				}
				
				$data['total_days_cost'] = $cost_tillnow;
				$data['total_days'] = $used_netdays;
				$data['srvid'] = $baseplanid;
				$data['user_plan_type'] = $user_plan_type;
			}
		}else{
			$data['is_expired'] = 1;
		}
		$ispcodet = $this->countrydetails();
		$data['currency'] = $ispcodet['currency'];
		echo json_encode($data);
	}
	
	public function current_user_plan_type(){
		$data = array();
		$uuid = $this->input->post('subs_uuid');
		$query = $this->db->query("SELECT user_plan_type FROM sht_users WHERE uid='".$uuid."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			$data['user_plan_type'] = $rowdata->user_plan_type;
		}
		$nextPlanQ = $this->db->query("SELECT user_plan_type FROM sht_nextcycle_userplanassoc WHERE uid='".$uuid."' AND status='0' ORDER by id desc");
		if($nextPlanQ->num_rows() > 0){
			$nprowdata = $nextPlanQ->row();
			$data['nextuser_plan_type'] = $nprowdata->user_plan_type;
		}else{
			$data['nextuser_plan_type'] = '';
		}
		echo json_encode($data);
	}

	public function paid_prevbill_bywallet($uuid,$wallet_amount){
		$lastPendingQ = $this->db->query("SELECT id, total_amount, bill_type, adjusted_amount, DATE(bill_added_on) as lastbilldate FROM sht_subscriber_billing WHERE subscriber_uuid='".$uuid."' AND receipt_received='0' AND ((bill_type != 'installation') AND (bill_type != 'security') AND (bill_type != 'advprepay') AND (bill_type != 'addtowallet')) ORDER BY id ASC");
		$lastPending = $lastPendingQ->num_rows();
		if($lastPending > 0){
			$balanceAmt = 0;
			foreach($lastPendingQ->result() as $lpobj){
				$billid = $lpobj->id;
				$pendingAmt = $lpobj->total_amount;
				$adjusted_amount = $lpobj->adjusted_amount;
				$billtype = $lpobj->bill_type;
				if($adjusted_amount != '0.00'){
					$wallet_amount = ($wallet_amount + $adjusted_amount);
				}
				if($wallet_amount >= $pendingAmt){
					$balanceAmt = $wallet_amount - $pendingAmt;
					$this->db->update('sht_subscriber_billing', array('payment_mode' => 'wallet', 'adjusted_amount' => '0.00', 'receipt_received' => '1'), array('id' => $billid));
				}elseif($wallet_amount <= $pendingAmt){
					$balanceAmt = 0;
					$this->db->update('sht_subscriber_billing', array('adjusted_amount' => $wallet_amount), array('id' => $billid));
				}
				
				
				if($balanceAmt == 0){
					break;
				}else{
					$wallet_amount = $balanceAmt;
				}
			}
		}
		return 1;
	}
	public function apply_planfornow(){
		$plandatarr = array(); $billcost = 0;
		$uuid = $this->input->post('subscriber_uuid');
		$prev_srvid = $this->input->post('prev_srvid');
		$prev_user_plantype = $this->input->post('prev_user_plantype');
		$prev_billid = $this->input->post('prev_billid');
		$prev_billpaid = $this->input->post('prev_billpaid');
		$prebill_oncycle = $this->input->post('nextprebill_oncycle');
		
		$session_data = $this->session->userdata('isp_session');;
		$isp_uid = $session_data['isp_uid'];
		$super_admin = $session_data['super_admin'];
		$userid = $session_data['userid'];
		if($super_admin == 1){
		    $added_by = 'SuperAdmin';
		}else{
		    $teamQ = $this->db->query("SELECT name, lastname FROM sht_isp_users WHERE id='".$userid."'");
		    if($teamQ->num_rows() > 0){
			$team_rowdata = $teamQ->row();
			$added_by = $team_rowdata->name. " " .$team_rowdata->lastname; 
		    }else{
			$added_by = 'TeamMember';
		    }
		}
		$pbcycle_rowdata = $this->plan_billing_cycle($isp_uid);
		$billing_cycle_date = $pbcycle_rowdata->billing_cycle;
		
		$nextplanid = $this->input->post('nextplanid');
		$bill_calculatedon = $this->input->post('prevplan_calcon');
		$datacost = $this->input->post('datacost');
		$dayscost = $this->input->post('dayscost');
		$nextuser_plan_type = $this->input->post('nextuser_plan_type');
		$actiondate = date('Y-m-d');
		if($bill_calculatedon == 'data'){
			$billcost = $datacost;
		}else{
			$billcost = $dayscost;
		}
		
		$this->disconnect_userineternet($uuid); // Disconnect User Internet
		
		$planPriceArr = $this->getplan_pricing($nextplanid, $uuid);
		$plan_price = $planPriceArr['plan_price'];
		$plan_tilldays = $planPriceArr['plan_tilldays'];
		
		$lastplan_activatedate = '0000-00-00';
		$current_plandateQ = $this->db->query("SELECT plan_activated_date FROM sht_users WHERE uid='".$uuid."'");
		if($current_plandateQ->num_rows() > 0){
			$lastplan_activatedate = $current_plandateQ->row()->plan_activated_date;
			$this->db->insert('sht_userplan_datechange_records', array('uid' => $uuid, 'isp_uid' => $isp_uid, 'bill_id' => $prev_billid, 'srvid' => $prev_srvid, 'lastplan_activatedate' => $lastplan_activatedate, 'added_on' => date('Y-m-d H:i:s')));
		}
		
		if($prev_user_plantype == 'postpaid'){
			$total_advpay = $this->user_advpayments($uuid);
			if($total_advpay > 0){
				$walletArr = array(
					'isp_uid' => $isp_uid,
					'subscriber_uuid' => $uuid,
					'wallet_amount' => $total_advpay,
					'added_on'	=> date('Y-m-d H:i:s')
				);
				$this->db->insert('sht_subscriber_wallet', $walletArr);
				$wlastid = $this->db->insert_id();
				$wallet_billarr = array(
					'isp_uid' => $isp_uid,
					'subscriber_id' => $uuid,
					'subscriber_uuid' => $uuid,
					'wallet_id' => $wlastid,
					'connection_type' => '',
					'bill_added_on' => date('Y-m-d H:i:s'),
					'receipt_number' => date('jnyHis'),
					'bill_type' => 'addtowallet',
					'payment_mode' => 'cash',
					'actual_amount' => $total_advpay,
					'total_amount' => $total_advpay,
					'receipt_received' => '1',
					'alert_user' => '1',
					'bill_generate_by' => $isp_uid,
					'bill_paid_on' => date('Y-m-d H:i:s'),
					'wallet_amount_received' => '1'
				);
				$this->db->insert('sht_subscriber_billing', $wallet_billarr);
				$this->db->update('sht_advance_payments', array('balance_left' => '0.00', 'bonus_amount' => '0.00'), array('uid' => $uuid));
				//$this->paid_prevbill_bywallet($uuid, $total_advpay);
			}
			sleep(1);
			$user_walletamt = $this->userbalance_amount($uuid);
			$payment_mode = ''; $receipt_received = '0'; $adjusted_amount = 0;
			/*if($user_walletamt >= $billcost){
				$payment_mode = 'wallet';
				$receipt_received = '1';
			}elseif(($billcost > $user_walletamt) && ($user_walletamt > 0)){
				$adjusted_amount = $user_walletamt;
			}*/
			
			$billnumber = $this->generate_billnumber();
			$billingarr = array(
				'subscriber_uuid' => $uuid,
				'plan_id' => $prev_srvid,
				'user_plan_type' => $prev_user_plantype,
				'bill_starts_from' => $lastplan_activatedate,
				'bill_added_on' => date('Y-m-d H:i:s'),
				'bill_number' => $billnumber,
				'bill_type' => 'midchange_plancost',
				'payment_mode' => $payment_mode,
				'receipt_received' => $receipt_received,
				'actual_amount' => round(( $billcost * 100 ) / 118),
				'adjusted_amount' => $adjusted_amount,
				'discount' => '0',
				'total_amount' => $billcost,
				'isp_uid' => $isp_uid
			);
			$this->db->insert('sht_subscriber_billing', $billingarr);
			$billid = $this->db->insert_id();
			$passbookArr = array(
				'isp_uid' => $isp_uid,
				'subscriber_uuid' => $uuid,
				'billing_id' => $billid,
				'srvid' => $prev_srvid,
				'plan_cost' => $billcost,
				'added_on'	=> date('Y-m-d H:i:s')
			);
			$this->db->insert('sht_subscriber_passbook', $passbookArr);
			$this->delete_temp_billnumber($isp_uid, $billnumber);
		}
		sleep(1);
		
		/******* PREPAID USER CONNECTION *********/
		if($prev_user_plantype == 'prepaid'){
			$this->db->update('sht_subscriber_passbook', array('plan_cost' => $billcost), array('billing_id' => $prev_billid, 'subscriber_uuid' => $uuid));
			$this->db->update('sht_subscriber_billing', array('total_amount' => $billcost,'bill_type' => 'midchange_plancost'), array('id' => $prev_billid, 'subscriber_uuid' => $uuid));
			/*$chkadjustamtQ = $this->db->query("SELECT adjusted_amount FROM sht_subscriber_billing WHERE id = '".$prev_billid."' AND subscriber_uuid = '".$uuid."' AND adjusted_amount != '0.00'" );
			if($chkadjustamtQ->num_rows() > 0){
				$adjst_rowdata = $chkadjustamtQ->row();
				$adjusted_amount = $adjst_rowdata->adjusted_amount;
				//$this->paid_prevbill_bywallet($uuid, $adjusted_amount);
			}*/
		}
		sleep(1);
		
		$custom_planprice = $this->input->post('custom_planprice');
		if($custom_planprice != ''){
			$plan_price = $custom_planprice;
			$this->addcustom_planprice($uuid, $nextplanid, $custom_planprice, 'Now');
			$this->db->delete("sht_custom_plan_pricing", array('uid' => $uuid, 'isp_uid' => $isp_uid, 'action' => 'NextCycle'));
		}else{
			$this->db->delete("sht_custom_plan_pricing", array('uid' => $uuid, 'isp_uid' => $isp_uid));
		}
		
		/******* PREPAID USER CONNECTION *********/
		if($nextuser_plan_type == 'prepaid'){
			$user_walletamt = $this->userbalance_amount($uuid); //check again wallet balance
			$payment_mode = ''; $receipt_received = '0'; $today_billdate = date('Y-m-d'); $adjusted_amount = 0;
			/*if($user_walletamt >= $plan_price){
				$payment_mode = 'wallet';
				$receipt_received = '1';
				$paidtill_date = date('Y-m-d', strtotime($today_billdate. " +30 day"));
				//$plandatarr['paidtill_date'] = $paidtill_date;
			
			}elseif(($plan_price > $user_walletamt) && ($user_walletamt > 0)){
				$adjusted_amount = $user_walletamt;
			}*/
			
			if($prebill_oncycle == '1'){
				$nextbdate = date('Y-m-d', strtotime('+1 month'));
				$nextbdate = new DateTime($nextbdate);
				$nextbdate->setDate($nextbdate->format('Y'), $nextbdate->format('m'), $billing_cycle_date);
				$next_month_billdate = $nextbdate->format('Y-m-d');
				
				$nxttimestamp = strtotime($next_month_billdate);
				$todtimestamp = strtotime(date('Y-m-d'));
				$netdays_alloted = round(abs($nxttimestamp - $todtimestamp) / 86400);
				
				$plancost_per_day = round(($plan_price/$plan_tilldays));
				$plancost_fornow = round($plancost_per_day * $netdays_alloted);
				
			}else{
				$plancost_fornow = $plan_price;
			}
			
			$billnumber = $this->generate_billnumber();
			$prebillingarr = array(
				'subscriber_uuid' => $uuid,
				'plan_id' => $nextplanid,
				'user_plan_type' => 'prepaid',
				'bill_starts_from' => date('Y-m-d'),
				'bill_added_on' => date('Y-m-d H:i:s'),
				'bill_number' => $billnumber,
				'bill_type' => 'montly_pay',
				'payment_mode' => $payment_mode,
				'receipt_received' => $receipt_received,
				'actual_amount' => round(( $plancost_fornow * 100 ) / 118),
				'discount' => '0',
				'total_amount' => $plancost_fornow,
				'adjusted_amount' => $adjusted_amount,
				'isp_uid' => $isp_uid
			);
			$this->db->insert('sht_subscriber_billing', $prebillingarr);
			$prebillid = $this->db->insert_id();
			$prePassbookArr = array(
				'isp_uid' => $isp_uid,
				'subscriber_uuid' => $uuid,
				'billing_id' => $prebillid,
				'srvid' => $nextplanid,
				'plan_cost' => $plancost_fornow,
				'added_on'	=> date('Y-m-d H:i:s')
			);
			$this->db->insert('sht_subscriber_passbook', $prePassbookArr);
			$this->delete_temp_billnumber($isp_uid, $billnumber);
			
		}
		
		$custfupQ = $this->db->query("SELECT id FROM sht_customize_plan_speed WHERE uid='".$uuid."' AND isp_uid='".$isp_uid."' AND status='1'");
		if($custfupQ->num_rows() > 0){
			$this->db->update('sht_customize_plan_speed', array('status' => '0', 'modified_on' => date('Y-m-d H:i:s'), 'modified_by' => $added_by), array('uid' => $uuid));
		}
		
		$user_wallet_balance = $this->userbalance_amount($uuid);
		$plan_cost_perday = round($plan_price/$plan_tilldays);
		$user_creditlimit = $this->getuser_creditlimit($nextplanid, $uuid);
		$total_advpay = $this->user_advpayments($uuid);
		$expiration_acctdays = round(($user_creditlimit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
		$expiration_date = date('Y-m-d', strtotime($actiondate . " +".$expiration_acctdays." days"));
		$expiration_date = $expiration_date.' 00:00:00';
		
		$plandatarr['user_credit_limit'] = $user_creditlimit;
		$plandatarr['expiration'] = $expiration_date;
		$plandatarr['user_plan_type'] = $nextuser_plan_type;
		$plandatarr['prebill_oncycle'] = $prebill_oncycle;
		$plandatarr['planautorenewal'] = '1';
		
		$today = date('d');
		if($today >= $billing_cycle_date){
			$nextdate = date('Y-m-d', strtotime('+1 month'));
			$nextdate = new DateTime($nextdate);
			$nextdate->setDate($nextdate->format('Y'), $nextdate->format('m'), $billing_cycle_date);
			$next_month = $nextdate->format('Y-m-d');
			$plandatarr['next_bill_date'] = $next_month;
		}else{
			$plandatarr['next_bill_date'] = date('Y-m').'-'.$billing_cycle_date;
		}
		
		if($nextuser_plan_type == 'prepaid'){
			if($prebill_oncycle == '1'){
				$nextbdate = date('Y-m-d', strtotime('+1 month'));
				$nextbdate = new DateTime($nextbdate);
				$nextbdate->setDate($nextbdate->format('Y'), $nextbdate->format('m'), $billing_cycle_date);
				$plandatarr['next_bill_date'] = $nextbdate->format('Y-m-d');
			}else{
				$pnext_bill_date = date('Y-m-d',strtotime("+$plan_tilldays day"));
				$plandatarr['next_bill_date'] = $pnext_bill_date;
			}
		}
		
		
		$next_subsplanQ = $this->db->get_where('sht_nextcycle_userplanassoc', array('uid' => $uuid, 'status' => '0'));
		$nextplan_numrows = $next_subsplanQ->num_rows();
		if($nextplan_numrows > 0){
			$this->db->delete('sht_nextcycle_userplanassoc', array('uid' => $uuid, 'status' => '0'));
		}
		$this->db->update('sht_users', array('downlimit' => '0', 'comblimit' => '0'), array('uid' => $uuid));
		$planinfoQ = $this->db->query("SELECT plantype, datalimit, datacalc FROM sht_services WHERE srvid='".$nextplanid."'");
		if($planinfoQ->num_rows() > 0){
			$rowdata = $planinfoQ->row();
			$plandata_calculatedon = $rowdata->datacalc;
			$plandatalimit = $rowdata->datalimit;
			
			$plandatarr['plan_activated_date'] = date('Y-m-d');
			$plandatarr['plan_changed_datetime'] = date('Y-m-d H:i:s');
			$plandatarr['baseplanid'] = $nextplanid;
			if($plandata_calculatedon == '1'){
				$plandatarr['downlimit'] = $plandatalimit;
				$plandatarr['comblimit'] = '0';
			}elseif($plandata_calculatedon == '2'){
				$plandatarr['downlimit'] = '0';
				$plandatarr['comblimit'] = $plandatalimit;
			}else{
				$plandatarr['downlimit'] = '0';
				$plandatarr['comblimit'] = '0';
			}
			
			$this->db->update('sht_users', $plandatarr, array('uid' => $uuid));
		}
		
		$planname = $this->planname($nextplanid);
		$this->notify_model->add_user_activitylogs($uuid, "Changed Plan: $planname Added");
		
		$useralerts_status = $this->useralerts_status($uuid);
		$sms_alert = $useralerts_status['sms_alert'];
		$email_alert = $useralerts_status['email_alert'];
		if($sms_alert == 1){
			$this->notify_model->newplanchange_alert($uuid, $nextplanid, 'changenow');
		}
		if($email_alert == 1){
			$this->emailer_model->newplanchangemail_alert($uuid, $nextplanid, 'changenow');
		}
		
		echo json_encode(true);
	}
	public function cancel_nextcycleplan(){
		$uuid = $this->input->post('uuid');
		$srvid = $this->input->post('srvid');
		
		$next_subsplanQ = $this->db->get_where('sht_nextcycle_userplanassoc', array('uid' => $uuid, 'status' => '0'));
		$nextplan_numrows = $next_subsplanQ->num_rows();
		if($nextplan_numrows > 0){
			$this->db->delete('sht_nextcycle_userplanassoc', array('uid' => $uuid, 'status' => '0'));
		}
		
		$planname = $this->planname($srvid);
		$this->notify_model->add_user_activitylogs($uuid, "NextCycle Plan: $planname Cancelled");
		
		$useralerts_status = $this->useralerts_status($uuid);
		$sms_alert = $useralerts_status['sms_alert'];
		$email_alert = $useralerts_status['email_alert'];
		if($sms_alert == 1){
			$this->notify_model->newplanchange_alert($uuid, $srvid, 'cancelplan');
		}
		if($email_alert == 1){
			$this->emailer_model->newplanchangemail_alert($uuid, $srvid, 'cancelplan');
		}
		echo json_encode(true);
	}
	public function addcustom_planprice($uuid, $nextplanid, $custom_planprice, $action){
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		
		$customplanQ = $this->db->query("SELECT baseplanid,gross_amt FROM sht_custom_plan_pricing WHERE isp_uid='".$isp_uid."' AND uid='".$uuid."' AND action='".$action."' ORDER BY id DESC LIMIT 1");
		$num_rows = $customplanQ->num_rows();
		if($num_rows > 0){
			$this->db->update("sht_custom_plan_pricing", array('baseplanid' => $nextplanid, 'gross_amt' => $custom_planprice, 'action' => $action, 'added_on' => date('Y-m-d H:i:s')), array('uid' => $uuid, 'isp_uid' => $isp_uid, 'action' => $action));
		}else{
			$this->db->insert("sht_custom_plan_pricing", array('uid' => $uuid, 'isp_uid' => $isp_uid, 'baseplanid' => $nextplanid, 'gross_amt' => $custom_planprice, 'action' => $action, 'added_on' => date('Y-m-d H:i:s')));
		}
		return 1;
	}
	public function getcustom_planprice($uuid, $srvid){
		$data = array();
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$customplanQ = $this->db->query("SELECT baseplanid,gross_amt FROM sht_custom_plan_pricing WHERE isp_uid='".$isp_uid."' AND uid='".$uuid."' AND baseplanid='".$srvid."' ORDER BY id DESC LIMIT 1");
		$num_rows = $customplanQ->num_rows();
		if($num_rows > 0){
			$rowdata = $customplanQ->row();
			$data['baseplanid'] = $rowdata->baseplanid;
			$data['gross_amt'] = $rowdata->gross_amt;
		}else{
			$data['baseplanid'] = '';
			$data['gross_amt'] = '';
		}
		return $data;
	}
	
	

	public function update_planspeed(){
		$uuid = $this->input->post('subscriber_uuid');
		$upload = $this->input->post('editplan_uploadspeed');
		$download = $this->input->post('editplan_downloadspeed');
		$baseplanid = $this->input->post('assigned_activeplan');
		$plan_speedalert = $this->input->post('editplan_speedalert');
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$super_admin = $session_data['super_admin'];
		$userid = $session_data['userid'];
		if($super_admin == 1){
		    $added_by = 'SuperAdmin';
		}else{
		    $teamQ = $this->db->query("SELECT name, lastname FROM sht_isp_users WHERE id='".$userid."'");
		    if($teamQ->num_rows() > 0){
			$team_rowdata = $teamQ->row();
			$added_by = $team_rowdata->name. " " .$team_rowdata->lastname; 
		    }else{
			$added_by = 'TeamMember';
		    }
		}
		
		$custfupQ = $this->db->query("SELECT id FROM sht_customize_plan_speed WHERE uid='".$uuid."' AND isp_uid='".$isp_uid."' AND status='1'");
		if($custfupQ->num_rows() > 0){
			$this->db->update('sht_customize_plan_speed', array('status' => '0', 'modified_on' => date('Y-m-d H:i:s'), 'modified_by' => $added_by), array('uid' => $uuid));
		}
		
		$this->db->insert('sht_customize_plan_speed', array('upload_speed' => ($upload * 1024), 'download_speed' => ($download * 1024), 'baseplanid' => $baseplanid, 'uid' => $uuid, 'isp_uid' => $isp_uid, 'status' => '1', 'added_on' => date('Y-m-d H:i:s'), 'added_by' => $added_by));
		
		$planname = $this->planname($baseplanid);
		$this->notify_model->add_user_activitylogs($uuid, "$planname : Speed Changed to $upload / $download");
		if($plan_speedalert == 1){
			$this->notify_model->newplanchange_alert($uuid, $baseplanid, 'planspeedchange');
			$this->emailer_model->newplanchangemail_alert($uuid, $baseplanid, 'planspeedchange');
		}
		
	}
	public function change_planautorenewal(){
		$data = array();
		$uuid = $this->input->post('uuid');
		
		$advpayQ = $this->db->query("SELECT id FROM sht_advance_payments WHERE uid='".$uuid."' AND (balance_left != '0.00' OR bonus_amount > 0) ORDER BY id ASC");
		if($advpayQ->num_rows() > 0){
			$data['advance_prepay'] = '1';
			$querry = $this->db->query("SELECT planautorenewal, baseplanid FROM sht_users WHERE uid='".$uuid."'");
			if($querry->num_rows() > 0){
				$rowdata = $querry->row();
				$planautorenewal = $rowdata->planautorenewal;
				$baseplanid = $rowdata->baseplanid;
				$data['renewaltype'] = $planautorenewal;
			}
		}else{
			$data['advance_prepay'] = '0';
			$querry = $this->db->query("SELECT planautorenewal, baseplanid FROM sht_users WHERE uid='".$uuid."'");
			if($querry->num_rows() > 0){
				$rowdata = $querry->row();
				$planautorenewal = $rowdata->planautorenewal;
				$baseplanid = $rowdata->baseplanid;
				$planautorenewal = ($planautorenewal == '0') ? '1' : '0';
				
				$user_creditlimit = $this->getuser_creditlimit($baseplanid, $uuid);
				$this->db->update('sht_users', array('planautorenewal' => $planautorenewal, 'user_credit_limit' => $user_creditlimit), array('uid' => $uuid));
				
				$data['renewaltype'] = $planautorenewal;
				$renewaltype = ($planautorenewal == '0') ? 'Disable' : 'Enable';
				$planname = $this->planname($baseplanid);
				$this->notify_model->add_user_activitylogs($uuid, "AutoRenewal Plan is $renewaltype");
				if($planautorenewal == 1){
					//$this->notify_model->planautorenewal_alert($uuid, $planname);
					//$this->emailer_model->planautorenewalemail_alert($uuid, $planname);
				}
			}
		}
		
		
		echo json_encode($data);
	}
	public function user_planautorenewal_status(){
		$data = array();
		$uuid = $this->input->post('uuid');
		
		$querry = $this->db->query("SELECT planautorenewal, baseplanid FROM sht_users WHERE uid='".$uuid."'");
		if($querry->num_rows() > 0){
			$rowdata = $querry->row();
			$planautorenewal = $rowdata->planautorenewal;
			$baseplanid = $rowdata->baseplanid;
			$data['renewaltype'] = $planautorenewal;
		}
		echo json_encode($data);
	}
/*------------------- TOPUP DETAILS STARTS -----------------------------------------------------------------*/

	public function disconnect_userineternet($uuid){
		$nasipQ = $this->db->query("SELECT tb1.radacctid, tb1.username, tb1.nasipaddress, tb1.framedipaddress, tb2.secret FROM radacct as tb1 INNER JOIN nas as tb2 ON(tb1.nasipaddress=tb2.nasname) WHERE tb1.username='".$uuid."' ORDER BY radacctid DESC LIMIT 1");
		if($nasipQ->num_rows() > 0){
			$nasrowdata = $nasipQ->row();
                        $radacctid = $nasrowdata->radacctid;
			$username = $nasrowdata->username;
			$userframedipaddress = $nasrowdata->framedipaddress;
			$usernasipaddress = $nasrowdata->nasipaddress;
			$secret = $nasrowdata->secret;
			//@exec("echo User-Name:=$username,Framed-Ip-Address=$userframedipaddress | radclient -x $usernasipaddress:3799 disconnect $secret");
			$this->disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret);
			$this->db->query("UPDATE radacct SET acctstoptime='".date('Y-m-d H:i:s')."' WHERE username='".$uuid."' AND acctstoptime IS NULL");
		}
		return 1;
	}
	public function check_topup_authenticy(){
		$userid = $this->input->post('userid');
		$uuid = $this->input->post('uid');
		$query = $this->db->query("SELECT id FROM sht_users WHERE id='".$userid."' AND uid='".$uuid."' AND baseplanid != '0' AND (enableuser ='1' OR account_activated_on != '0000-00-00 00:00:00')");
		$numrows = $query->num_rows();
		echo json_encode($numrows);
	}
	public function sht_topup_regionwise($subscriber_uuid){
		$data = array();
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		
		$user_regionArr = $this->user_state_city_zone($subscriber_uuid);
		$user_state = $user_regionArr['state'];
		$user_city = $user_regionArr['city'];
		$user_zone = $user_regionArr['zone'];
		
		$cond1 = "( state_id='".$user_state."' AND city_id='".$user_city."' AND zone_id='".$user_zone."' )";
		$cond2 = " (state_id='all') ";
		$cond3 = " (state_id='".$user_state."' AND city_id='".$user_city."' AND zone_id='all') ";
		$cond4 = " (state_id='".$user_state."' AND city_id='all') ";
		
		$query = $this->db->query("SELECT topup_id FROM sht_topup_region WHERE ($cond1 OR $cond2 OR $cond3 OR $cond4) AND status='1' AND is_deleted='0' AND isp_uid='".$isp_uid."'");
		//echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
			$i = 0;
			foreach($query->result() as $pobj){
				$data[$i] = $pobj->topup_id;
				$i++;
			}
		}
		return $data;
	}
	public function active_topuplist(){
		$data = array();
		$subscriber_uuid = $this->input->post('uuid');
		$plantype = $this->userassoc_plantype($subscriber_uuid);
		$topup_inregion = $this->sht_topup_regionwise($subscriber_uuid);
		if(count($topup_inregion) > 0){
			$topup_inregion = implode(',' , $topup_inregion);
		}else{
			$topup_inregion = 0;
		}
			
		$topcond = '';
		if($plantype == '1'){
			$topcond = " AND (topuptype='3') ";
		}else{
			$topup_filter = $this->input->post('topup_filter');
			$topcond = " AND (topuptype='".$topup_filter."') ";
		}
		
		$planQ = $this->db->query("SELECT tb1.srvid, tb1.srvname, tb2.net_total, tb2.region_type, tb2.payment_type FROM sht_services as tb1 INNER JOIN sht_topup_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb1.enableplan='1' AND tb1.topuptype != '0' AND (tb2.srvid IN($topup_inregion) OR tb2.region_type != 'region') $topcond ");
		$num_rows = $planQ->num_rows();
		$gen = "<option value=''>Select TopUp<sup>*</sup></option>";
		if($num_rows > 0){
			foreach($planQ->result() as $pobj){
				$gen .= '<option value="'.$pobj->srvid.'">'.$pobj->srvname.'</option>';
			}
		}
		
		$data['topup_inregion'] = count($topup_inregion);
		$data['active_topuplist'] = $gen;
		$data['plantype'] = $plantype;
		
		echo json_encode($data);
	}

	public function assingtopup_touser(){
		//print_r($this->input->post()); die;
		$uuid = $this->input->post('uuid');
		$topuptype = $this->input->post('topuptype');
		$active_topups = $this->input->post('active_topups');
		$topup_startdate = $this->input->post('topup_startdate');
		$topup_enddate = $this->input->post('topup_enddate');
		$topup_totalcost = $this->input->post('topup_totalcost');
		$topup_dayscount = $this->input->post('topup_dayscount');
		
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		if($super_admin == 1){
		    $added_by = 'SuperAdmin';
		}else{
		    $teamQ = $this->db->query("SELECT name, lastname FROM sht_isp_users WHERE id='".$userid."'");
		    if($teamQ->num_rows() > 0){
			$team_rowdata = $teamQ->row();
			$added_by = $team_rowdata->name. " " .$team_rowdata->lastname; 
		    }else{
			$added_by = 'TeamMember';
		    }
		}
		
		if(isset($topup_startdate) && $topup_startdate != ''){
			$topup_startdate = date("Y-m-d", strtotime($topup_startdate));
		}else{
			$topup_startdate = '0000-00-00';
		}
		
		if(isset($topup_enddate) && $topup_enddate != ''){
			$topup_enddate = date("Y-m-d", strtotime($topup_enddate));
		}else{
			$topup_enddate = '0000-00-00';
		}
		$topupArr = array(
			'isp_uid' => $isp_uid,
			'uid' => $uuid,
			'topuptype' => $topuptype,
			'topup_id' => $active_topups,
			'topup_startdate' => $topup_startdate,
			'topup_enddate' => $topup_enddate,
			'applicable_days' => $topup_dayscount,
			'added_on' => date('Y-m-d H:i:s')
		);
		//echo '<pre>'; print_r($topupArr); die;
		$userassoc_plandata = $this->userassoc_plandata($uuid);
		$datacalc = $userassoc_plandata['datacalc'];
		$plantype = $userassoc_plandata['plantype'];

		$spdunactt = 0;
		if($topuptype == '2' || $topuptype == '3'){
			$checkActiveQ = $this->db->query("SELECT id FROM sht_usertopupassoc WHERE uid = '".$uuid."' AND topuptype = '".$topuptype."' AND status='1' AND terminate='0'");
			if($checkActiveQ->num_rows() > 0){
				$spdunactt = 1;
				echo "This TopUp type is already in use. Please try later.";
			}else{
				$spdunactt = 0;
			}
		}

		$topuptype_desc = '';
		if($topuptype == '1'){
			$topuptype_desc = 'Data';
		}elseif($topuptype == '2'){
			$topuptype_desc = 'DAU';
		}elseif($topuptype == '3'){
			$topuptype_desc = 'Speed';
		}
		
		if($topuptype == '1' || $spdunactt == '0'){
			$paytypeArr = $this->userassoc_topupdata($active_topups);
			if($paytypeArr['paytype'] == 'Free'){ //Generate 0 Billing
				$this->db->insert('sht_usertopupassoc', $topupArr);
				
				$topupname = $this->planname($active_topups);
				$this->notify_model->add_user_activitylogs($uuid, $topuptype_desc." Topup: $topupname Added");
				
				$useralerts_status = $this->useralerts_status($uuid);
				$sms_alert = $useralerts_status['sms_alert'];
				$email_alert = $useralerts_status['email_alert'];
				if($sms_alert == 1){
                                $this->notify_model->topup_activation_alert($uuid, $topuptype_desc, $topupname, $active_topups);
				}
				if($email_alert == 1){
				$this->emailer_model->topup_activation_emailalert($uuid, $topuptype_desc, $topupname, $active_topups);
				}
				
				
				$updateTopupArr = array();
				if($topuptype == '2'){
					$updateTopupArr = array( 'dataunacttopupid' => $active_topups, 'dataunacnttopup_startdate' => $topup_startdate, 'dataunacnttopup_enddate' => $topup_enddate );
					$this->db->update('sht_users', $updateTopupArr, array('uid' => $uuid));
				}elseif($topuptype == '3'){
					$updateTopupArr = array( 'speedtopupid' => $active_topups, 'speedtopup_enddate' => $topup_enddate, 'speedtopup_startdate' => $topup_startdate );
					$this->db->update('sht_users', $updateTopupArr, array('uid' => $uuid));
				}elseif($topuptype == '1'){
					$this->disconnect_userineternet($uuid);
					$user_databalance = $this->user_databalance($uuid);
					if($datacalc == '1'){
						$downlimit = $user_databalance['downlimit'];
						if($downlimit < 0){
							$downlimit = $paytypeArr['datalimit'];
						}else{
							$downlimit = $user_databalance['downlimit'] + $paytypeArr['datalimit'];
						}
						$this->db->update('sht_users', array('downlimit' => $downlimit), array('uid' => $uuid));
					}elseif($datacalc == '2'){
						$comblimit = $user_databalance['comblimit'];
						if($comblimit < 0){
							$comblimit = $paytypeArr['datalimit'];
						}else{
							$comblimit = $user_databalance['comblimit'] + $paytypeArr['datalimit'];	
						}
						$this->db->update('sht_users', array('comblimit' => $comblimit), array('uid' => $uuid));
					}
					$custfupQ = $this->db->query("SELECT id FROM sht_customize_plan_speed WHERE uid='".$uuid."' AND isp_uid='".$isp_uid."' AND status='1'");
					if($custfupQ->num_rows() > 0){
						$this->db->update('sht_customize_plan_speed', array('status' => '0', 'modified_on' => date('Y-m-d H:i:s'), 'modified_by' => $added_by), array('uid' => $uuid));
					}
					$this->db->query("UPDATE sht_users SET enableuser='1', inactivate_type = '', suspended_days = '0', suspendedon = '0000-00-00' WHERE uid = '".$uuid."' AND account_activated_on != '0000-00-00 00:00:00'");
				}
				
				$this->addbilling($uuid,'topup',$active_topups,'Free',$paytypeArr['topup_price'],0,$topup_totalcost,$topup_dayscount);
				//$this->wallet_entry($uuid, $paytypeArr['topup_price']);
			}else{
				$isfeasible = $this->check_useracct($uuid, $topup_totalcost);
				if($isfeasible == '1'){
					$this->db->insert('sht_usertopupassoc', $topupArr);
					
					$topupname = $this->planname($active_topups);
					$this->notify_model->add_user_activitylogs($uuid, $topuptype_desc." Topup: $topupname Added");
					
					$useralerts_status = $this->useralerts_status($uuid);
					$sms_alert = $useralerts_status['sms_alert'];
					$email_alert = $useralerts_status['email_alert'];
					if($sms_alert == 1){
						$this->notify_model->topup_activation_alert($uuid, $topuptype_desc, $topupname, $active_topups);
					}
					if($email_alert == 1){
						$this->emailer_model->topup_activation_emailalert($uuid, $topuptype_desc, $topupname, $active_topups);
					}
					$updateTopupArr = array();
					if($topuptype == '2'){
						$updateTopupArr = array( 'dataunacttopupid' => $active_topups, 'dataunacnttopup_startdate' => $topup_startdate, 'dataunacnttopup_enddate' => $topup_enddate );
						$this->db->update('sht_users', $updateTopupArr, array('uid' => $uuid));
					}elseif($topuptype == '3'){
						$updateTopupArr = array( 'speedtopupid' => $active_topups, 'speedtopup_enddate' => $topup_enddate, 'speedtopup_startdate' => $topup_startdate );
						$this->db->update('sht_users', $updateTopupArr, array('uid' => $uuid));	
					}elseif($topuptype == '1'){
						$this->disconnect_userineternet($uuid);
						$user_databalance = $this->user_databalance($uuid);
						if($datacalc == '1'){
							$downlimit = $user_databalance['downlimit'];
							if($downlimit < 0){
								$downlimit = $paytypeArr['datalimit'];
							}else{
								$downlimit = $user_databalance['downlimit'] + $paytypeArr['datalimit'];
							}
							$this->db->update('sht_users', array('downlimit' => $downlimit), array('uid' => $uuid));
						}elseif($datacalc == '2'){
							$comblimit = $user_databalance['comblimit'];
							if($comblimit < 0){
								$comblimit = $paytypeArr['datalimit'];
							}else{
								$comblimit = $user_databalance['comblimit'] + $paytypeArr['datalimit'];	
							}
							$this->db->update('sht_users', array('comblimit' => $comblimit), array('uid' => $uuid));
						}
						$custfupQ = $this->db->query("SELECT id FROM sht_customize_plan_speed WHERE uid='".$uuid."' AND isp_uid='".$isp_uid."' AND status='1'");
						if($custfupQ->num_rows() > 0){
							$this->db->update('sht_customize_plan_speed', array('status' => '0', 'modified_on' => date('Y-m-d H:i:s'), 'modified_by' => $added_by), array('uid' => $uuid));
						}
						$this->db->query("UPDATE sht_users SET enableuser='1', inactivate_type = '', suspended_days = '0', suspendedon = '0000-00-00' WHERE uid = '".$uuid."' AND account_activated_on != '0000-00-00 00:00:00'");
					}
					$this->addbilling($uuid,'topup',$active_topups,'',$paytypeArr['topup_price'],0,$topup_totalcost,$topup_dayscount);
					//$this->wallet_entry($uuid, $paytypeArr['topup_price']);
				}else{
					echo "You have reached your credit limit. <br/> Please recharge or pay to continue.";
				}
			}
		}
		
		//return 1;
	}
	public function userassoc_topupdata($topupid){
		$data = array();
		$user_currtopupQ = $this->db->query("SELECT tb2.topuptype, tb2.srvid, tb2.datalimit, tb3.net_total,tb3.gross_amt, tb3.payment_type FROM  sht_services as tb2 INNER JOIN sht_topup_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb2.srvid='".$topupid."' AND enableplan='1'");
		$current_topuprow = $user_currtopupQ->row();
		$tax = $this->current_taxapplicable();
		$gross_amt = $current_topuprow->gross_amt;
		$topup_price = round($gross_amt + (($gross_amt * $tax)/100));
		
		$data['datalimit'] = $current_topuprow->datalimit;
		$data['topup_id'] = $current_topuprow->srvid;
		$data['topup_price'] = $topup_price;
		$data['paytype'] = $current_topuprow->payment_type;
		$data['topuptype'] = $current_topuprow->topuptype;
		
		return $data;
	}
	public function userassoc_topuplist(){
		$gen = '';
		$uuid = $this->input->post('uuid');
		$ispcodet = $this->countrydetails();
		$cocurrency = $ispcodet['currency'];
		$query = $this->db->query("SELECT tb1.topuptype, tb1.status, tb1.topup_startdate,tb1.topup_enddate, DATE(tb1.added_on) as added_on, tb1.applicable_days, tb2.srvname, tb3.net_total, tb3.gross_amt, tb3.payment_type FROM sht_usertopupassoc as tb1 INNER JOIN sht_services as tb2 ON(tb1.topup_id=tb2.srvid) INNER JOIN sht_topup_pricing as tb3 ON(tb3.srvid=tb2.srvid) WHERE tb1.uid='".$uuid."' ORDER BY tb1.added_on DESC");
		if($query->num_rows() > 0){
			$i = 1;
			foreach($query->result() as $topobj){
				$topup_addedon = date('d-m-Y', strtotime($topobj->added_on));
				$tax = $this->current_taxapplicable();
				$gross_amt = $topobj->gross_amt;
				$net_total = round($gross_amt + (($gross_amt * $tax)/100));
				//$net_total = $topobj->net_total;
				
				
				if(($topobj->topup_startdate != '0000-00-00') && ($topobj->topup_enddate != '0000-00-00')){
					$topup_startdate = date('d-m-Y', strtotime($topobj->topup_startdate));
					$topup_enddate = date('d-m-Y', strtotime($topobj->topup_enddate));
					
					//$startdate = strtotime($topup_startdate);
					//$enddate = strtotime($topup_enddate);
					//$dayscount = ceil(abs($enddate - $startdate) / 86400);
					$dayscount = $topobj->applicable_days;
					$net_total = $net_total * $dayscount;
				}else{
					$topup_startdate = $topobj->topup_startdate;
					$topup_enddate = $topobj->topup_enddate;
				}
				$activestyle = '';
				$status = ($topobj->status == '1') ? 'Active' : 'Inactive';
				if(($topobj->status == '0') && ($topobj->topuptype == '1')){
					$status = 'Applied';	
				}elseif($status == 'Active'){
					$activestyle = "style='color:#46A149'";
				}elseif($status == 'Inactive'){
					$activestyle = "style='color:#f00'";
				}
				if($topobj->topuptype == '1'){
					$topuptype = 'Data Topup';
				}elseif($topobj->topuptype == '2'){
					$topuptype = 'Data Unactt Topup';
				}elseif($topobj->topuptype == '3'){
					$topuptype = 'Speed Topup';
				}

				$gen .= "<tr><td>".$i.".</td><td>".$topup_addedon."</td><td>".$topuptype."</td><td ".$activestyle.">".$topobj->srvname."</td><td>".$cocurrency." ".number_format($net_total,2)."</td><td>".$topup_startdate."</td><td>".$topup_enddate."</td><td>".$status."</td><td>NA</td></tr>";
				$i++;
			}
			
			$userassoc_plandata = $this->userassoc_plandata($uuid);
			$user_credit_limit = $userassoc_plandata['user_credit_limit'];
			$user_wallet_balance = $this->userbalance_amount($uuid);
			$plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
			$total_advpay = $userassoc_plandata['total_advpay'];
			$actiondate = $this->affected_date_on_wallet($uuid);
			$expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
			$expiration_date = date('Y-m-d', strtotime($actiondate . " +".$expiration_acctdays." days"));
			$expiration_date = $expiration_date.' 00:00:00';
			$this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $uuid));
			
		}else{
			$gen .= "<tr><td colspan='9'><center> No TopUp Activated Yet !! </center></td></tr>";
		}
		
		echo json_encode($gen);
	}
	public function check_useracct($uuid, $topup_totalcost){
		$userplandata = $this->userassoc_plandata($uuid);
		$user_credit_limit = $userplandata['user_credit_limit'];
		$wallet_amt = $this->userbalance_amount($uuid);
		$balance_update = $user_credit_limit + $wallet_amt;
		
		if($topup_totalcost > $balance_update){
			return 0;
		}else{
			return 1;
		}
		
	}
	public function topup_pricing(){
		$data = array();
		$topupid = $this->input->post('topupid');
		$topuptype = $this->input->post('topuptype');
		if(isset($topupid) && $topupid != '' ){
			$user_currtopupQ = $this->db->query("SELECT net_total, gross_amt FROM sht_topup_pricing WHERE srvid='".$topupid."'");
			$current_topuprow = $user_currtopupQ->row();
			$tax = $this->current_taxapplicable();
			$gross_amt = $current_topuprow->gross_amt;
			$topup_price = round($gross_amt + (($gross_amt * $tax)/100));

			$data['topup_price'] =  $topup_price;
			if($topuptype == '2'){ //DATA U/C TOPUP
				$ducQ = $this->db->query("SELECT days FROM sht_dataunaccountancy WHERE srvid='".$topupid."'");
				if($ducQ->num_rows() > 0){
					$daysArr = array(); $i=0;
					foreach($ducQ->result() as $ducobj){
						$daysArr[$i] = $ducobj->days;
						$i++;
					}
					$data['days'] = implode(',' ,$daysArr);
				}
			}elseif($topuptype == '3'){ //SPEED TOPUP
				$ducQ = $this->db->query("SELECT days FROM sht_speedtopup WHERE srvid='".$topupid."'");
				if($ducQ->num_rows() > 0){
					$daysArr = array(); $i=0;
					foreach($ducQ->result() as $ducobj){
						$daysArr[$i] = $ducobj->days;
						$i++;
					}
					$data['days'] = implode(',' ,$daysArr);
				}
			}
		}
		return $data;
	}
/*------------------- SUBSCRIBER BILLING METHODS STARTS ------------------------------------------------------*/	

	public function user_advpayments($uid){
		$query = $this->db->query("SELECT COALESCE(SUM(balance_left),0) as balance_left FROM sht_advance_payments WHERE uid='".$uid."'");
		$advobj = $query->row();
		$total_advpay = $advobj->balance_left;
		return $total_advpay;
	}
	public function plan_billing_cycle($isp_uid){
		$planbillingQ = $this->db->get_where('sht_billing_cycle', array('isp_uid' => $isp_uid));
		$numrows = $planbillingQ->num_rows();
		return $planbillingQ->row();
	}
	public function add_installation_charges(){
		//echo '<pre>'; print_r($_POST); die;
		$userid = $this->session->userdata['isp_session']['userid'];
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		
		$subscid = $this->input->post('subscriber_userid');
		$uuid = $this->input->post('subscriber_uuid');
		$receipt_number = $this->input->post('install_chqddrecpt');
		$payment_confirm = $this->input->post('install_bill_payment_received');
		$billing_with_tax = $this->input->post('install_bill_withtax');
		
		if(($receipt_number != '') && ($payment_confirm == '1')){
			$receipt_received = '1';
			$bill_paid_on = date('Y-m-d H:i:s');
		}else{
			$receipt_received = '0';
			$bill_paid_on = '0000-00-00 00:00:00';
		}
		$installcharges_applicable = $this->input->post('installcharges_applicable');
		$installation_billid = $this->input->post('installation_billid');
		$install_bill_comments = $this->input->post('install_bill_comments');
		$install_bill_remarks = $this->input->post('install_bill_remarks');
		
		if($installcharges_applicable == 'applycharges'){
			$billdiscounttype = $this->input->post('install_discounttype');
			$total_discount = 0;
			if($billdiscounttype == 'percent'){
				$total_discount = $this->input->post('install_bill_percentdiscount');
			}elseif($billdiscounttype == 'flat'){
				$total_discount = $this->input->post('install_bill_flatdiscount');
			}
                        if($receipt_received == '1'){
                            	$amount_received = $this->input->post('install_bill_total_amount');
				$useralerts_status = $this->useralerts_status($uuid);
				$sms_alert = $useralerts_status['sms_alert'];
				$email_alert = $useralerts_status['email_alert'];
				if($sms_alert == 1){
					$this->notify_model->payment_received_alert($uuid, $amount_received);
				}
				if($email_alert == 1){
					$this->emailer_model->payment_received_emailalert($uuid, $amount_received);
				}
                        }
			
			$installarr = array(
				'isp_uid' => $isp_uid,
				'subscriber_id' => $this->input->post('subscriber_userid'),
				'subscriber_uuid' => $this->input->post('subscriber_uuid'),
				'connection_type' => $this->input->post('connection_type'),
				'bill_added_on' => $this->input->post('install_bill_datetime'),
				'receipt_number' => $this->input->post('install_chqddrecpt'),
				'bill_type' => $this->input->post('install_bill_transaction_type'),
				'payment_mode' => $this->input->post('install_bill_payment_mode'),
				'actual_amount' => $this->input->post('install_bill_amount'),
				'discounttype' => $billdiscounttype,
				'discount' => $total_discount,
				'total_amount' => $this->input->post('install_bill_total_amount'),
				'receipt_received' => $receipt_received,
				'alert_user' => $this->input->post('install_send_copy_to_customer'),
				'bill_generate_by' => $userid,
				'bill_paid_on' => $bill_paid_on,
				'bill_comments' => $install_bill_comments,
				'bill_remarks' => $install_bill_remarks,
				'installsecurity_amount_received' => $this->input->post('install_bill_payment_received'),
				'billing_with_tax' => $billing_with_tax
			);
		}else{
			$installarr = array(
				'isp_uid' => $isp_uid,
				'subscriber_id' => $this->input->post('subscriber_userid'),
				'subscriber_uuid' => $this->input->post('subscriber_uuid'),
				'connection_type' => $this->input->post('connection_type'),
				'bill_added_on' => date('Y-m-d H:i:s'),
				'receipt_number' => '-',
				'bill_type' => $this->input->post('install_bill_transaction_type'),
				'payment_mode' => 'NA',
				'actual_amount' => '0.00',
				'discounttype' => 'percent',
				'discount' => '0',
				'total_amount' => '0.00',
				'receipt_received' => '1',
				'alert_user' => '0',
				'bill_generate_by' => $userid,
				'bill_paid_on' => $bill_paid_on,
				'bill_comments' => $install_bill_comments,
				'bill_remarks' => $install_bill_remarks,
				'installsecurity_amount_received' => '0',
				'billing_with_tax' => '1'
			);
		}
		
		if($installation_billid != ''){
			$this->db->update('sht_subscriber_billing', $installarr, array('id' => $installation_billid));
			$this->notify_model->add_user_activitylogs($uuid, "Installation Bill Updated");
		}else{
			if($installcharges_applicable == 'applycharges'){
				$billnumber = $this->generate_billnumber();
				$installarr['bill_number'] = $billnumber;
				$this->delete_temp_billnumber($isp_uid, $billnumber);
			}else{
				$installarr['bill_number'] = '-';
			}
			
			$this->db->insert('sht_subscriber_billing', $installarr);
			$this->db->update('sht_subscriber_activation_panel', array('installation_details' => '1'), array('subscriber_id' => $subscid));
			$this->notify_model->add_user_activitylogs($uuid, "Installation Bill Added");
			//$this->emailer_model->installation_billing($subscid);
		}
		
		echo json_encode($uuid);
	}
	public function add_security_charges(){
		//echo '<pre>'; print_r($_POST); die;
		$userid = $this->session->userdata['isp_session']['userid'];
		$subscid = $this->input->post('subscriber_userid');
		$uuid = $this->input->post('subscriber_uuid');
		$receipt_number = $this->input->post('security_receipt_number');
		$payment_confirm = $this->input->post('security_bill_payment_received');
		
		if(($receipt_number != '') && ($payment_confirm == '1')){
			$receipt_received = '1';
			$bill_paid_on = date('Y-m-d H:i:s');
		}else{
			$receipt_received = '0';
			$bill_paid_on = '0000-00-00 00:00:00';
		}
		
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$securitycharges_applicable = $this->input->post('securitycharges_applicable');
		$security_billid = $this->input->post('security_billid');
		$security_bill_comments = $this->input->post('security_bill_comments');
		$security_bill_remarks = $this->input->post('security_bill_remarks');
		$billing_with_tax = $this->input->post('security_bill_withtax');
		
		if($securitycharges_applicable == 'applycharges'){
			$billdiscounttype = $this->input->post('security_discounttype');
			$total_discount = 0;
			if($billdiscounttype == 'percent'){
				$total_discount = $this->input->post('security_bill_percentdiscount');
			}elseif($billdiscounttype == 'flat'){
				$total_discount = $this->input->post('security_bill_flatdiscount');
			}
                        if($receipt_received == '1'){
				$amount_received = $this->input->post('security_bill_total_amount');
				$useralerts_status = $this->useralerts_status($uuid);
				$sms_alert = $useralerts_status['sms_alert'];
				$email_alert = $useralerts_status['email_alert'];
				if($sms_alert == 1){
					$this->notify_model->payment_received_alert($uuid, $amount_received);
				}
				if($email_alert == 1){
					$this->emailer_model->payment_received_emailalert($uuid, $amount_received);
				}
                        }
			$securityarr = array(
				'isp_uid' => $isp_uid,
				'subscriber_id' => $this->input->post('subscriber_userid'),
				'subscriber_uuid' => $this->input->post('subscriber_uuid'),
				'connection_type' => $this->input->post('connection_type'),
				'bill_added_on' => $this->input->post('security_bill_datetime'),
				'receipt_number' => $this->input->post('security_receipt_number'),
				'bill_type' => $this->input->post('security_bill_transaction_type'),
				'payment_mode' => $this->input->post('security_bill_payment_mode'),
				'actual_amount' => $this->input->post('security_bill_amount'),
				'discounttype' => $billdiscounttype,
				'discount' => $total_discount,
				'total_amount' => $this->input->post('security_bill_total_amount'),
				'receipt_received' => $receipt_received,
				'alert_user' => $this->input->post('security_send_copy_to_customer'),
				'bill_generate_by' => $userid,
				'bill_paid_on' => $bill_paid_on,
				'bill_comments' => $security_bill_comments,
				'bill_remarks' => $security_bill_remarks,
				'installsecurity_amount_received' => $this->input->post('security_bill_payment_received'),
				'billing_with_tax' => $billing_with_tax
			);
		}else{
			$securityarr = array(
				'isp_uid' => $isp_uid,
				'subscriber_id' => $this->input->post('subscriber_userid'),
				'subscriber_uuid' => $this->input->post('subscriber_uuid'),
				'connection_type' => $this->input->post('connection_type'),
				'bill_added_on' => date('Y-m-d H:i:s'),
				'receipt_number' => '-',
				'bill_type' => $this->input->post('security_bill_transaction_type'),
				'payment_mode' => 'NA',
				'actual_amount' => '0.00',
				'discounttype' => 'percent',
				'discount' => '0',
				'total_amount' => '0.00',
				'receipt_received' => '1',
				'alert_user' => '0',
				'bill_generate_by' => $userid,
				'bill_paid_on' => $bill_paid_on,
				'bill_comments' => $security_bill_comments,
				'bill_remarks' => $security_bill_remarks,
				'installsecurity_amount_received' => '0',
				'billing_with_tax' => '1'
			);			
		}
		
		if($security_billid != ''){
			$this->db->update('sht_subscriber_billing', $securityarr, array('id' => $security_billid));
			$this->notify_model->add_user_activitylogs($uuid, "Security Bill Updated");
		}else{
			$this->db->insert('sht_subscriber_billing', $securityarr);
			$this->db->update('sht_subscriber_activation_panel', array('security_details' => '1'), array('subscriber_id' => $subscid));
			$this->notify_model->add_user_activitylogs($uuid, "Security Bill Added");
			//$this->emailer_model->security_billing($subscid);
		}
		echo json_encode($uuid);
	}
	public function advance_prepaid_charges(){
		//echo '<pre>'; print_r($_POST); die;
		$data = array();
		$userid = $this->session->userdata['isp_session']['userid'];
		$subscid = $this->input->post('subscriber_userid');
		$uuid = $this->input->post('subscriber_uuid');
		$srvid = $this->input->post('userassoc_planid');
		$receipt_number = $this->input->post('advprepay_chqddrecpt');
		$plan_monthly_cost = $this->input->post('plan_monthly_cost');
		$receipt_billid = $this->input->post('receipt_billid');
		$planname = $this->planname($srvid);
		$advbillid = $this->input->post('prevadvpayid');
		
		if($receipt_number != ''){
			$receipt_received = '1';
			$this->notify_model->add_user_activitylogs($uuid, "AdvPrepay Payment for $planname Added");
                        $amount_received = $this->input->post('advprepay_bill_total_amount');
			
			$useralerts_status = $this->useralerts_status($uuid);
			$sms_alert = $useralerts_status['sms_alert'];
			$email_alert = $useralerts_status['email_alert'];
			if($sms_alert == 1){
				$this->notify_model->payment_received_alert($uuid, $amount_received);
			}
			if($email_alert == 1){
				$this->emailer_model->payment_received_emailalert($uuid, $amount_received);
			}
			$bill_paid_on = date('Y-m-d H:i:s');
		}else{
			$receipt_received = '0';
			$this->notify_model->add_user_activitylogs($uuid, "AdvPrepay Request for $planname Added");
			$bill_paid_on = '0000-00-00 00:00:00';
		}
		
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
                $billingid = 0;
		
		if(isset($receipt_billid) && $receipt_billid != ''){
		    $this->db->update('sht_subscriber_billing', array('payment_mode' => $this->input->post('advprepay_bill_payment_mode'), 'receipt_received' => $receipt_received, 'receipt_number' => $this->input->post('advprepay_chqddrecpt')), array('id' => $receipt_billid));
                    $billingid = $receipt_billid;
		}else{
			$billdiscounttype = $this->input->post('advprepay_discounttype');
			$total_discount = 0;
			if($billdiscounttype == 'percent'){
				$total_discount = $this->input->post('advprepay_bill_percentdiscount');
			}elseif($billdiscounttype == 'flat'){
				$total_discount = $this->input->post('advprepay_bill_flatdiscount');
			}
			
			$advbillarr = array(
				'isp_uid' => $isp_uid,
				'subscriber_id' => $this->input->post('subscriber_userid'),
				'subscriber_uuid' => $this->input->post('subscriber_uuid'),
				'plan_id' => $srvid,
				'plan_cost' => $plan_monthly_cost,
				'connection_type' => $this->input->post('connection_type'),
				'bill_added_on' => $this->input->post('advprepay_bill_datetime'),
				'bill_type' => $this->input->post('advprepay_bill_transaction_type'),
				'payment_mode' => $this->input->post('advprepay_bill_payment_mode'),
				'actual_amount' => $this->input->post('advprepay_bill_amount'),
				'discounttype' => $billdiscounttype,
				'discount' => $total_discount,
				'total_amount' => $this->input->post('advprepay_bill_total_amount'),
				'receipt_received' => $receipt_received,
				'receipt_number' => $this->input->post('advprepay_chqddrecpt'),
				'alert_user' => '1',
				'bill_generate_by' => $userid,
				'advancepay_for_months' => $this->input->post('advprepay_month_count'),
				'number_of_free_months' => $this->input->post('advprepay_freemonth_count'),
				'bill_paid_on' => $bill_paid_on
			);
			
			if($advbillid != ''){
				$this->db->update('sht_subscriber_billing', $advbillarr, array('id' => $advbillid));
				$billingid = $advbillid;
			}else{
				$billnumber = $this->generate_billnumber();
				$advbillarr['bill_number'] = $billnumber;
				$this->db->insert('sht_subscriber_billing', $advbillarr);
				$billingid = $this->db->insert_id();
				$this->delete_temp_billnumber($isp_uid, $billnumber);
			}
		}
		
		if($receipt_received == '1'){
			$actual_amount = $this->input->post('advprepay_bill_amount');
			$total_PaidAmount = $this->input->post('advprepay_bill_total_amount');
			
			$discounttype = $this->input->post('advprepay_discounttype');
			$discount = 0;
			if($discounttype == 'percent'){
				$discount = $this->input->post('advprepay_bill_percentdiscount');
			}elseif($discounttype == 'flat'){
				$discount = $this->input->post('advprepay_bill_flatdiscount');
			}
			if(!isset($discount)){ $discount = 0;  }
			$number_of_free_months = $this->input->post('advprepay_freemonth_count');
			if(!isset($number_of_free_months)){ $number_of_free_months = 0;  }
			$bonus_amount = '0.00';
			
			if(($discount != '') && ($discount != '0')){
			    $bonus_amount = $actual_amount - $total_PaidAmount;
			}else if(($number_of_free_months != '') && ($number_of_free_months != '0')){
			    $bonus_amount = $number_of_free_months * $plan_monthly_cost;
			}
                        
			$advpayarr = array(
                                'bill_id' => $billingid,
				'uid' => $this->input->post('subscriber_uuid'),
				'srvid' => $srvid,
				'payment_mode' => $this->input->post('advprepay_bill_payment_mode'),
				'plan_cost' => $plan_monthly_cost,
				'actual_amount' => $this->input->post('advprepay_bill_amount'),
				'discounttype' => $discounttype,
				'discount' => $discount,
				'total_amount' => $this->input->post('advprepay_bill_total_amount'),
				'bonus_amount' => $bonus_amount,
				'balance_left' => $total_PaidAmount,
				'advancepay_for_months' => $this->input->post('advprepay_month_count'),
				'number_of_free_months' => $number_of_free_months,
				'added_on' => date('Y-m-d H:i:s')
			);
			
			if($advbillid != ''){
				$this->db->update('sht_advance_payments', $advpayarr, array('bill_id' => $billingid));
			}else{
				$this->db->insert('sht_advance_payments', $advpayarr);
			}
                        
			
			$next_subsplanQ = $this->db->get_where('sht_nextcycle_userplanassoc', array('uid' => $uuid, 'status' => '0'));
			$nextplan_numrows = $next_subsplanQ->num_rows();
			if($nextplan_numrows > 0){
				$this->db->delete('sht_nextcycle_userplanassoc', array('uid' => $uuid, 'status' => '0'));
			}
			
			$advpayQ = $this->db->query("SELECT SUM(advancepay_for_months) as advancepay_for_months, SUM(number_of_free_months) as number_of_free_months FROM sht_advance_payments WHERE uid='".$uuid."' AND balance_left != '0.00'");
			if($advpayQ->num_rows() > 0){
				$advrowdata = $advpayQ->row();
				$advancepay_for_months = $advrowdata->advancepay_for_months;
				$number_of_free_months = $advrowdata->number_of_free_months;
				$payformonths = $advancepay_for_months + $number_of_free_months;
				//$paidtill_date = date("Y-m-d", strtotime(" +$payformonths months"));
				
				$userassoc_plandata = $this->userassoc_plandata($uuid);
				$user_credit_limit = $userassoc_plandata['user_credit_limit'];
				$user_wallet_balance = $this->userbalance_amount($uuid);
				$plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
				$total_advpay = $userassoc_plandata['total_advpay'];
				
				//$actiondate = $this->affected_date_on_wallet($uuid);
				$actiondate = date('Y-m-d');
				$expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
				$expiration_date = date('Y-m-d', strtotime($actiondate . " +".$expiration_acctdays." days"));
				$expiration_date = $expiration_date.' 00:00:00';		
				$this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $uuid));
			}
		}
		//$this->passbook_entry($uuid, $billingid, $srvid, $wallet_amount);
		$data['subscid'] = $uuid;
		echo json_encode($data);
	}
	public function user_amtcredit_listing(){
		$data = array();
		$permstyle = '';
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$data['superadmin_perm'] = $superadmin;
		if($superadmin != '1'){
			if($this->is_permission('DELETEWALLETRECEIPTS','RO') == true){
				$data['delrcpt_readperm'] = '1';
			}elseif($this->is_permission('DELETEWALLETRECEIPTS','HIDE') == false){
				$data['delrcpt_hideperm'] = '1';
			}
		}
		
		$subscid = $this->input->post('subscriber_uuid');
		$qlmt = 12; $qofset = 0;
		$limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
		
		/****************************************/
		$totalbillQ = $this->db->query('SELECT id FROM sht_subscriber_billing WHERE subscriber_uuid="'.$subscid.'" AND status="1" AND is_deleted="0" AND bill_type="addtowallet" ORDER BY id');
		$totalbillcount = $totalbillQ->num_rows();
		/***************************************/
		$ispcodet = $this->countrydetails();
		$cocurrency = $ispcodet['currency'];
		
		$query = $this->db->query('SELECT * FROM sht_subscriber_billing WHERE subscriber_uuid="'.$subscid.'" AND status="1" AND is_deleted="0" AND bill_type="addtowallet" ORDER BY id DESC LIMIT '.$qofset.','.$qlmt);
		$billcount = $query->num_rows();
		
		$creditgen = ''; $totalwallet_amt = '0';
		if($billcount > 0){			
			$i = $qofset + 1; //for S.NO. Counter
			foreach($query->result() as $billobj){
				$billid = $billobj->id;
				$billtype = $billobj->bill_type;
				$uuid = $billobj->subscriber_uuid;
				$bno = ($billobj->bill_number == '') ? '<center>-</center>' : $billobj->bill_number;

				$recno = $billobj->receipt_number;
				$receipt_received = $billobj->receipt_received;
				
				$trash = '';
				if($billtype != 'advprepay'){
					$totalwallet_amt += $billobj->total_amount;
					$trash = '| <a href="javascript:void(0)" onclick="delete_walletentry('.$billid.')" style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="Delete Receipt" class="delete_mbillperm"><i class="fa fa-trash-o" aria-hidden="true"></i></a> ';
				}
				
				$billcopy = '<a href="javascript:void(0)" onclick="billcopy_actions('.$billid.',\''.$billtype.'\', \''.$uuid.'\', \'viewbill\')" style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="View Receipt"><i class="fa fa-eye" aria-hidden="true"></i></a> | <a href="javascript:void(0)" onclick="edit_walletbillentry('.$billid.',\''.$billtype.'\')" class="edit_mbillperm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> '.$trash.'| <a href="javascript:void(0)" onclick="billcopy_actions('.$billid.',\''.$billtype.'\', \''.$uuid.'\', \'sendbill\')" style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="Send Receipt on Mail" class="send_mbillperm"><i class="fa fa-envelope-o" aria-hidden="true"></i></a> | <a href="'.base_url().'user/download_billpdf/'.$billid.'" target="_blank" style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="Download Receipt" class="dwlnd_mbillperm"><i class="fa fa-download" aria-hidden="true"></i></a> | <a href="'.base_url().'user/print_billpdf/'.$billid.'" target="_blank"  style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="Print Receipt" class="print_mbillperm"><i class="fa fa-print" aria-hidden="true"></i></a>';
                                
				
				$paymode = $billobj->payment_mode;
				$wallet_amount_received = $billobj->wallet_amount_received;
				
				$rowstyle = '';
				if($wallet_amount_received == '0'){ $rowstyle="style='background-color:#F4C205;text-align:center'"; }
				
				$total_amount = $billobj->total_amount;
				$bill_addedon = date('d-m-Y', strtotime($billobj->bill_added_on));
				
				//<div class='checkbox' style='margin-top:0px; margin-bottom:0px;'><label><input type='checkbox' name='export_billid' class='collapse_rcptcheckbox' value='".$billobj->id."'></label></div>
				$creditgen .= "<tr><td>&nbsp;</td><td>&nbsp;</td><td>".$recno."</td><td $rowstyle>".$cocurrency." ".number_format($total_amount,2)."</td><td>".$paymode."</td><td>".$bill_addedon."</td><td>".$billcopy."</td>";
				
				$i++;
			}
		}
		$total_advpay = $this->user_advpayments($subscid);
		$data['adv_totalpay'] = $total_advpay;
		$data['totalwallet_amt'] = $totalwallet_amt;
		
		if(($qofset + $billcount) == $totalbillcount){
			$data['loadmore'] = 0;
		}else{
			$data['loadmore'] = 1;
		}
		$data['limit'] = $qlmt;
		$data['offset'] = $qlmt+$qofset;
		$data['creditbill_listing'] = $creditgen;
		
		echo json_encode($data);
	}
	public function getwallet_billdetails(){
		$data = array();
		$billarr = array(); $rcptarr = array();
		$billid = $this->input->post('billid');
		$wbQ = $this->db->query("SELECT wallet_id, receipt_number, bill_added_on, payment_mode, total_amount, wallet_amount_received FROM sht_subscriber_billing WHERE id='".$billid."'");
		if($wbQ->num_rows() > 0){
			$billarr = $wbQ->row_array();
			$billarr['bill_datetimeformat'] = date('d-m-Y H:i:s', strtotime($billarr['bill_added_on']));
			$paymode = $data['payment_mode'];
			if($paymode != 'cash'){
				$receiptQ = $this->db->query("SELECT tb2.cheque_dd_paytm_number, tb2.account_number, tb2.branch_address, tb2.bankslip_issued_date, tb2.branch_name FROM sht_subscriber_receipt_history as tb2 WHERE tb2.bill_id='".$billid."'");
				if($receiptQ->num_rows() > 0){
					$rcptarr = $receiptQ->row_array();
				}
			}
			
			$data = ($billarr + $rcptarr);
		}
		
		echo json_encode($data);
	}
	public function user_billing_listing(){
		$data = array();
		$permstyle = '';
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$data['superadmin_perm'] = $superadmin;
		if($superadmin != '1'){
			$billreadperm = $this->is_permission('BILLING','RO');
			if($billreadperm == false){
				$data['billtab_readperm'] = '0';
				
				if($this->is_permission('CREDITLIMIT','RO') == true){
					$data['creditlimit_readperm'] = '1';
				}elseif($this->is_permission('CREDITLIMIT','HIDE') == false){
					$data['creditlimit_hideperm'] = '1';
				}
				if($this->is_permission('ADDTOWALLET','RO') == true){
					$data['wallet_readperm'] = '1';
				}elseif($this->is_permission('ADDTOWALLET','HIDE') == false){
					$data['wallet_hideperm'] = '1';
				}
				if($this->is_permission('ADVANCEPREPAY','RO') == true){
					$data['advprepay_readperm'] = '1';
				}elseif($this->is_permission('ADVANCEPREPAY','HIDE') == false){
					$data['advprepay_hideperm'] = '1';
				}
				if($this->is_permission('EDITMONTHLYBILLS','RO') == true){
					$data['editbill_readperm'] = '1';
				}elseif($this->is_permission('EDITMONTHLYBILLS','HIDE') == false){
					$data['editbill_hideperm'] = '1';
				}
				if($this->is_permission('SENDMONTHLYBILLS','RO') == true){
					$data['sendbill_readperm'] = '1';
				}elseif($this->is_permission('SENDMONTHLYBILLS','HIDE') == false){
					$data['sendbill_hideperm'] = '1';
				}
				if($this->is_permission('DOWNLOADMONTHLYBILLS','RO') == true){
					$data['dwnldbill_readperm'] = '1';
				}elseif($this->is_permission('DOWNLOADMONTHLYBILLS','HIDE') == false){
					$data['dwnldbill_hideperm'] = '1';
				}
				if($this->is_permission('PRINTMONTHLYBILLS','RO') == true){
					$data['printbill_readperm'] = '1';
				}elseif($this->is_permission('PRINTMONTHLYBILLS','HIDE') == false){
					$data['printbill_hideperm'] = '1';
				}
				
				
				if($this->is_permission('SETUPBILLS','RO') == true){
					$data['setupbill_readperm'] = '1';
				}else{
					if($this->is_permission('SETUPBILLS','ADD') == true){
						$data['setupbill_addperm'] = '1';
					}
					if($this->is_permission('SETUPBILLS','EDIT') == true){
						$data['setupbill_editperm'] = '1';
					}
				}
			}else{
				$data['billtab_readperm'] = '1';
			}
		}
		
		$subscid = $this->input->post('subscriber_uuid');
		$qlmt = 12; $qofset = 0;
		$limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
		
		/****************************************/
		$totalbillQ = $this->db->query('SELECT id FROM sht_subscriber_billing WHERE subscriber_uuid="'.$subscid.'" AND status="1" AND is_deleted="0" AND (bill_type !="addtowallet" AND bill_type !="advprepay") ORDER BY id');
		$totalbillcount = $totalbillQ->num_rows();
		/***************************************/
		$ispcodet = $this->countrydetails();
		$cocurrency = $ispcodet['currency'];
		
		$query = $this->db->query('SELECT * FROM sht_subscriber_billing WHERE subscriber_uuid="'.$subscid.'" AND status="1" AND is_deleted="0" AND (bill_type !="addtowallet" AND bill_type !="advprepay") ORDER BY id DESC LIMIT '.$qofset.','.$qlmt);
		//echo $this->db->last_query(); die;
		$billcount = $query->num_rows();
		$debitgen = ''; $creditgen = ''; $wallet_amt = '0'; $instsecugen = ''; $totalbill_amt = '0';
		if($billcount > 0){
			$wallet_amt = $this->userbalance_amount($subscid);
			
			$i = $qofset + 1; //for S.NO. Counter
			foreach($query->result() as $billobj){
				$billid = $billobj->id;
				$billtype = $billobj->bill_type;
				$uuid = $billobj->subscriber_uuid;
				$bno = ($billobj->bill_number == '') ? '<center>-</center>' : $billobj->bill_number;

				$recno = $billobj->receipt_number;
				$receipt_received = $billobj->receipt_received;
				$installsecurity_billpaid = $billobj->installsecurity_amount_received;
				
				$enter_recno = '';
				if($receipt_received != '1'){
					$enter_recno = '<a href="javascript:void(0)" onclick="show_updateReceiptModal('.$billid.',\''.$billtype.'\')" class="isbillperm" style="'.$permstyle.'" title="Enter Receipt"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>';
				}
				
				$billcopy = '<a href="javascript:void(0)" onclick="billcopy_actions('.$billid.',\''.$billtype.'\', \''.$uuid.'\', \'sendbill\')" style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="Send Bill on Mail" class="send_mbillperm"><i class="fa fa-envelope-o" aria-hidden="true"></i></a> | <a href="javascript:void(0)" onclick="billcopy_actions('.$billid.',\''.$billtype.'\', \''.$uuid.'\', \'viewbill\')" style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="View Bill"><i class="fa fa-eye" aria-hidden="true"></i></a> | <a href="'.base_url().'user/download_billpdf/'.$billid.'" target="_blank" style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="Download Bill" class="dwlnd_mbillperm"><i class="fa fa-download" aria-hidden="true"></i></a> | <a href="'.base_url().'user/print_billpdf/'.$billid.'" target="_blank"  style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="Print Bill" class="print_mbillperm"><i class="fa fa-print" aria-hidden="true"></i></a>';
				
				//$rcptcopy = '<a href="javascript:void(0)" onclick="billcopy_actions('.$billid.',\''.$billtype.'\', \''.$uuid.'\', \'viewbill\')" style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="View Bill"><i class="fa fa-eye" aria-hidden="true"></i></a> | <a href="javascript:void(0)" style="padding-right:20px;float:right;" onclick=""><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
                                
				
				$paymode = $billobj->payment_mode;
				if($paymode == 'NA'){
					$billstatus = 'No Charges';
					$billcopy = "";
				}else{
					$billstatus = ($billobj->receipt_received == '0') ? 'Pending' : 'Paid';
				}
				
				$rowstyle = '';
				
				if($receipt_received == '0'){ $rowstyle="style='background-color:#F4C205;text-align:center'"; }
				else{ $rowstyle="style='background-color:#4DDC54;text-align:center'"; }
				
				$trash_bill = ''; $edit_bill = '';
				if(($billtype != 'topup') && ($billtype != 'installation') && ($billtype != 'security') && ($billstatus != 'Paid') && ($billtype != 'montly_pay') && ($billtype != 'midchange_plancost')){
					$trash_bill = '<a href="javascript:void(0)" style="padding-right:20px;float:right;" onclick="delete_billentry('.$billid.')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';	
				}
				$edit_bill = '<a href="javascript:void(0)" onclick="edit_billentry('.$billid.',\''.$billtype.'\')" class="edit_mbillperm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> | ';	
				$paymode = $billobj->payment_mode;
				if($paymode == 'wallet'){
					$recno = '-';
				}elseif($paymode == 'Net Banking'){
					$recno = $billobj->transection_id;
				}
				$total_amount = $billobj->total_amount;
				/*$adjusted_amount = $billobj->adjusted_amount;
				if($adjusted_amount != '0.00'){
					$total_amount = $total_amount - $adjusted_amount;
					$adjusted_amount = ' <br/>('.$cocurrency.' '.number_format($billobj->adjusted_amount,2).')' ;
				}else{
					$adjusted_amount = '';
				}*/
				
				$bill_addedon = date('d-m-Y', strtotime($billobj->bill_added_on));
				$plan_id = $billobj->plan_id;
				$planname = $this->planname($plan_id);
				if($planname == '-'){
					$planname = $billobj->bill_type;
				}else{
					$planname = $this->planname($plan_id). '<br/>('.$billobj->bill_type.')';
				}
				
				if(($billobj->bill_type == 'montly_pay') || ($billobj->bill_type == 'topup') || ($billtype == 'midchange_plancost')){
					$totalbill_amt += $total_amount;
					$debitgen .= "<tr><td>&nbsp;</td><td><div class='checkbox' style='margin-top:0px; margin-bottom:0px;'><label><input type='checkbox' name='export_billid' class='collapse_billcheckbox' value='".$billobj->id."'></label></div></td><td>".$bno."</td><td>".$planname."</td><td>".$cocurrency." ".number_format($total_amount,2)."</td><td>".$bill_addedon."</td><td>".$trash_bill.$edit_bill.$billcopy."</td>";
				}
				elseif(($billobj->bill_type == 'installation') || ($billobj->bill_type == 'security')){
					$instsecugen .= "<tr><td>".$bno."</td><td>".$recno."</td><td>".$planname."</td><td ".$rowstyle.">".$cocurrency." ".number_format($total_amount,2)."</td><td style='text-align:center;'>".$bill_addedon."</td><td>".$trash_bill.'<a href="javascript:void(0)" onclick="show_instsecuReceiptModal('.$billid.',\''.$billtype.'\')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> |' .$billcopy."</td><td>&nbsp;</td>";
					
				}
				
				$i++;
			}
		}
		
		
		$creditQ = $this->db->query("SELECT tb1.user_credit_limit, DATE(tb1.account_activated_on) as account_activated_on, tb1.baseplanid, tb2.net_total, tb2.gross_amt, tb3.plan_duration FROM sht_users as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.baseplanid=tb2.srvid) INNER JOIN sht_services as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb1.uid='".$subscid."'");
		$user_creditrow = $creditQ->row();
		$user_creditlimit = $user_creditrow->user_credit_limit;
		$user_account_activated_on = $user_creditrow->account_activated_on;
		$srvid = $user_creditrow->baseplanid;

		$tax = $this->current_taxapplicable();
		$plan_duration = $user_creditrow->plan_duration;
		$gross_amt = ($user_creditrow->gross_amt * $plan_duration);
		$planprice = round($gross_amt + (($gross_amt * $tax)/100));
		
		$custom_planpriceArr = $this->getcustom_planprice($subscid, $srvid);
		$custom_planprice = $custom_planpriceArr['gross_amt'];
		if($custom_planprice != ''){
			//$gross_amt = round($custom_planprice / $plan_duration);
			$planprice = $custom_planprice;
		}
		
		$plan_tilldays = ($plan_duration * 30);
		$plan_cost_perday = round($planprice/$plan_tilldays);
		
		if($user_account_activated_on != '0000-00-00'){
			$actiondate = $this->affected_date_on_wallet($subscid);
			$total_advpay = $this->user_advpayments($subscid);
			$data['actiondate'] = $actiondate;
			$expiration_acctdays = round(($user_creditlimit + $wallet_amt + $total_advpay) / $plan_cost_perday);
			$data['expiration_acctdays'] = $expiration_acctdays;
			$expiration_date = date('Y-m-d', strtotime($actiondate . " +".$expiration_acctdays." days"));
			$expiration_date = $expiration_date.' 00:00:00';
			$this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $subscid));
		}

		$data['user_creditlimit'] = $user_creditlimit;
		$data['wallet_amt'] = number_format($wallet_amt,2);
		$total_advpay = $this->user_advpayments($subscid);
		$data['adv_totalpay'] = $total_advpay;
		
		$halfplan_price = round($planprice/2);
		if($halfplan_price > abs($wallet_amt)){
			$data['change_limit_upto'] = $halfplan_price;	
		}else{
			$data['change_limit_upto'] = abs($wallet_amt);	
		}
		
		if(($qofset + $billcount) == $totalbillcount){
			$data['loadmore'] = 0;
		}else{
			$data['loadmore'] = 1;
		}
		$data['limit'] = $qlmt;
		$data['offset'] = $qlmt+$qofset;
		$data['instsecugen'] = $instsecugen;
		$data['debitbill_listing'] = $debitgen;
		$data['totalbill_amt'] = $totalbill_amt;
		
		echo json_encode($data);
	}
	public function user_advanced_payment_listing(){
		$data = array();
		$permstyle = '';
		$isperm = $this->is_permission(CREATEUSER,'EDIT');
		if($isperm == false){
		  if($this->is_permission(CREATEUSER,'RO')){
			$permstyle= "pointer-events: none; cursor: not-allowed;";
		  }
		}
		
		$subscid = $this->input->post('uuid');
		$qlmt = 100; $qofset = 0;
		$limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
		
		$ispcodet = $this->countrydetails();
		$cocurrency = $ispcodet['currency'];
		
		$query = $this->db->query('SELECT tb1.*, tb2.bill_number, tb2.receipt_number FROM sht_advance_payments as tb1 INNER JOIN sht_subscriber_billing as tb2 ON(tb1.bill_id=tb2.id) WHERE tb1.uid="'.$subscid.'" ORDER BY tb2.id DESC');
		$billcount = $query->num_rows();
		
		$creditgen = ''; $totalwallet_amt = '0';
		if($billcount > 0){			
			$i = $qofset + 1; //for S.NO. Counter
			foreach($query->result() as $billobj){
				$advpayid = $billobj->id;
				$billid = $billobj->bill_id;
				$uuid = $billobj->uid;
				$bno = ($billobj->bill_number == '') ? '<center>-</center>' : $billobj->bill_number;

				$recno = $billobj->receipt_number;
				$srvid = $billobj->srvid;
				$planname = $this->planname($srvid);
				$paymonths = $billobj->advancepay_for_months;
				$freemonths = $billobj->number_of_free_months;
				$billtype = 'advprepay'; $trash = '';
				
				$billcopy = '<a href="javascript:void(0)" onclick="billcopy_actions('.$billid.',\''.$billtype.'\', \''.$uuid.'\', \'viewbill\')" style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="View Receipt"><i class="fa fa-eye" aria-hidden="true"></i></a> | <a href="javascript:void(0)" onclick="edit_billentry('.$billid.',\''.$billtype.'\')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> '.$trash.'| <a href="javascript:void(0)" onclick="billcopy_actions('.$billid.',\''.$billtype.'\', \''.$uuid.'\', \'sendbill\')" style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="Send Receipt on Mail"><i class="fa fa-envelope-o" aria-hidden="true"></i></a> | <a href="'.base_url().'user/download_billpdf/'.$billid.'" target="_blank" style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="Download Receipt"><i class="fa fa-download" aria-hidden="true"></i></a> | <a href="'.base_url().'user/print_billpdf/'.$billid.'" target="_blank"  style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="Print Receipt"><i class="fa fa-print" aria-hidden="true"></i></a>| <a href="javascript:void(0)" onclick="delete_advpaymentrcpt('.$billid.')" style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="Delete Receipt"><i class="fa fa-trash" aria-hidden="true"></i></a>';
				 //| <a href="javascript:void(0)" onclick="delete_advpaymentrcpt('.$billid.')" style="text-decoration: none; color:#29abe2; '.$permstyle.'" title="Delete Receipt"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                
				
				$paymode = $billobj->payment_mode;
				$total_amount = $billobj->total_amount;
				$bill_addedon = date('d-m-Y', strtotime($billobj->added_on));

				$creditgen .= "<tr><td>&nbsp;</td><td>".$bno."</td><td>".$recno."</td><td>".$planname."</td><td>". number_format($total_amount,2)."</td><td>".$paymonths."</td><td>".$freemonths."</td><td>".$bill_addedon."</td><td>".$billcopy."</td>";
				
				$i++;
			}
		}

		$data['advprepay_listing'] = $creditgen;
		echo json_encode($data);
	}
	public function delete_advprepaid_rcpt(){
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		
		$uuid = $this->input->post('subscriber_uuid');
		$advprepay_addtowallet = $this->input->post('advprepay_addtowallet');
		$advprepayid = $this->input->post('advprepayid');
		
		$balance_left = 0;
		$advbalanceamtQ = $this->db->query("SELECT COALESCE(SUM(balance_left),0) as balance_left FROM sht_advance_payments WHERE bill_id='".$advprepayid."'");
		if($advbalanceamtQ->num_rows() > 0){
			$balance_left = $advbalanceamtQ->row()->balance_left;
		}
		
		if($advprepay_addtowallet == 0){
			$this->db->delete('sht_advance_payments', array('bill_id' => $advprepayid));
			$this->db->delete('sht_subscriber_billing', array('id' => $advprepayid));
		}else{
			$walletArr = array(
				'isp_uid' => $isp_uid,
				'subscriber_uuid' => $uuid,
				'wallet_amount' => $balance_left,
				'added_on'	=> date('Y-m-d H:i:s')
			);
			$this->db->insert('sht_subscriber_wallet', $walletArr);
			$wlastid = $this->db->insert_id();
			$wallet_billarr = array(
				'isp_uid' => $isp_uid,
				'subscriber_id' => $uuid,
				'subscriber_uuid' => $uuid,
				'wallet_id' => $wlastid,
				'connection_type' => '',
				'bill_added_on' => date('Y-m-d H:i:s'),
				'receipt_number' => date('jnyHis'),
				'bill_type' => 'addtowallet',
				'payment_mode' => 'cash',
				'actual_amount' => $balance_left,
				'total_amount' => $balance_left,
				'receipt_received' => '1',
				'alert_user' => '1',
				'bill_generate_by' => $isp_uid,
				'bill_paid_on' => date('Y-m-d H:i:s'),
				'wallet_amount_received' => '1'
			);
			$this->db->insert('sht_subscriber_billing', $wallet_billarr);
			$this->db->delete('sht_advance_payments', array('bill_id' => $advprepayid));
			$this->db->delete('sht_subscriber_billing', array('id' => $advprepayid));
		}
		
		$this->notify_model->add_user_activitylogs($uuid, "Delete AdvPrepay Receipt");
		echo json_encode(true);
	}
	public function getbillnumber($billid){
		$bill_number = '';
		$custombillQ = $this->db->query("SELECT bill_number FROM sht_subscriber_custom_billing WHERE id='".$billid."'");
		if($custombillQ->num_rows() > 0){
			$bill_number = $custombillQ->row()->bill_number;
		}else{
			$subsbillQ = $this->db->query("SELECT bill_number FROM sht_subscriber_billing WHERE id='".$billid."'");
			if($subsbillQ->num_rows() > 0){
				$bill_number = $subsbillQ->row()->bill_number;
			}
		}
		return $bill_number;
	}
	public function pastreceiptbill_listing(){
		$billid = $this->input->post('billid_toupdate');
		$receiptQ = $this->db->query("SELECT tb1.* FROM sht_subscriber_receipt_history as tb1 WHERE tb1.bill_id='".$billid."' ORDER BY id DESC");
		$gen = '
		<div class="table-responsive">
			<table class="table table-striped">
			   <thead>
			      <tr class="active">
				 <th>S.no</th>
				 <th>Receipt Number</th>
				 <th>Receipt Amount</th>
				 <th>Payment Via</th>
				 <th>Added On</th>
				 <th>Action</th>
			      </tr>
			   </thead>
			   <tbody>
                  
		';
		if($receiptQ->num_rows() > 0){
			$i = 1;
			foreach($receiptQ->result() as $recobj){
				$recptid = $recobj->id;
				$recptamt = $recobj->receipt_amount;
				$gen .= '<tr><td>'.$i.'</td><td>'.$recobj->receipt_number.'</td><td>'.$recobj->receipt_amount.'</td><td>'.$recobj->payment_mode.'</td><td>'.$recobj->added_on.'</td><td><a href="javascript:void(0)" style="padding-right:20px;float:right;" onclick="deletebill_receiptentry('.$billid.', '.$recptid.', '.$recptamt.')"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td></tr>';
			}
		}else{
			$gen .= '<tr><td colspan="5">No Records Found.</td></tr>';
		}
			$gen .= '
			   </tbody>
			</table>
		</div>';
		
		echo json_encode($gen);
		
	}
	public function update_receipt_number(){
		$billid = $this->input->post('billid_toupdate');
		$billtype = $this->input->post('billtype_toupdate');
                $receipt_amount = $this->input->post('update_receipt_amount');
		$receipt_number = $this->input->post('update_receipt_number');
		$update_payment_mode = $this->input->post('update_payment_mode');
		$uuid = $this->input->post('subscriber_uuid');
		$bill_paid_on = date('Y-m-d H:i:s');
		
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		
		if($billtype == 'custom_invoice'){
			$this->db->update('sht_subscriber_custom_billing', array('receipt_number' => $receipt_number, 'receipt_received' => '1', 'payment_mode' => $update_payment_mode, 'bill_paid_on' => $bill_paid_on), array('id' => $billid));
			
			$custombillno = $this->getbillnumber($billid);
			$this->notify_model->add_user_activitylogs($uuid, "Custom Bill: $custombillno, Receipt Updated");
		}else{
			
			$slipimage = '';
			if(isset($_FILES) && (count($_FILES) > 0)){
				$slipimage = $_FILES['bill_slipimage']['name'];
			}
			
			$billingArr = array();
			$receiptArr = array(
				'bill_id' => $billid,
				'receipt_number' => $receipt_number,
				'receipt_amount' => $receipt_amount,
				'added_on' => date('Y-m-d H:i:s')
			);
			if(($update_payment_mode == 'cheque') || ($update_payment_mode == 'dd')){
				$receiptArr['account_number'] = $this->input->post('bill_bankaccount_number');
				$receiptArr['branch_name'] = $this->input->post('bill_bankname');
				$receiptArr['branch_address'] = $this->input->post('bill_bankaddress');
				$receiptArr['bankslip_issued_date'] = $this->input->post('bill_bankissued_date');
				$receiptArr['bankslip_image'] = $slipimage;
				$receiptArr['cheque_dd_paytm_number'] = $this->input->post('update_cheque_dd_paytm');
			}
			if($update_payment_mode == 'viapaytm'){
				$receiptArr['cheque_dd_paytm_number'] = $this->input->post('update_cheque_dd_paytm');
				
			}
			$receiptArr['isp_uid'] = $isp_uid;
			$receiptArr['uid'] = $uuid;
			$receiptArr['payment_mode'] = $update_payment_mode;
			$receiptArr['bill_type'] = $billtype;
			$this->db->insert('sht_subscriber_receipt_history', $receiptArr);
			
			$totalreceipt_amount = 0;
			$receipthistoryQ = $this->db->query("SELECT COALESCE(SUM(receipt_amount),0) as receipt_amt FROM sht_subscriber_receipt_history WHERE bill_id = '".$billid."'");
			if($receipthistoryQ->num_rows() > 0){
				$totalreceipt_amount = $receipthistoryQ->row()->receipt_amt;
			}
			
			$checkamtQ = $this->db->query("SELECT bill_number, bill_type, total_amount, adjusted_amount, receipt_received FROM sht_subscriber_billing WHERE id='".$billid."'");
			if($checkamtQ->num_rows() > 0){
				$rowdata = $checkamtQ->row();
				$bill_amount = $rowdata->total_amount;
				$adjusted_amount = $rowdata->adjusted_amount;
				$billtype = $rowdata->bill_type;
				$bill_number = $rowdata->bill_number;
				$receipt_received = $rowdata->receipt_received;
				
				$pendingAmt = ($bill_amount - $totalreceipt_amount);

				if($update_payment_mode == 'viawallet'){
					$walletQ = $this->db->query("SELECT id, left_amount FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."' AND left_amount != '0.00' ORDER BY id ASC");
					if($walletQ->num_rows() > 0){
						$rcptbalamt = $receipt_amount;
						foreach($walletQ->result() as $wltobj){
							$left_amount = $wltobj->left_amount;
							$wid = $wltobj->id;
							if($left_amount > $rcptbalamt){
								$this->db->query("UPDATE sht_subscriber_wallet SET left_amount = (left_amount - $rcptbalamt) WHERE id='".$wid."' ");
								break;
							}else{
								$rcptbalamt = ($rcptbalamt - $left_amount);
								$this->db->update('sht_subscriber_wallet', array('left_amount' => '0.00'), array('id' => $wid));
							}
						}
					}
				}
				
				if($pendingAmt > 0){
					$billingArr['adjusted_amount'] = $totalreceipt_amount;
					$billingArr['receipt_number'] = $receipt_number;
					$billingArr['payment_mode'] = $update_payment_mode;
					
					$this->db->update('sht_subscriber_billing', $billingArr, array('id' => $billid));
					
				}
				elseif($pendingAmt == 0){
					$billingArr['adjusted_amount'] = '0.00';
					$billingArr['receipt_number'] = $receipt_number;
					$billingArr['payment_mode'] = $update_payment_mode;
					$billingArr['receipt_received'] = '1';
					$billingArr['bill_paid_on'] = $bill_paid_on;

					
					$this->db->update('sht_subscriber_billing', $billingArr, array('id' => $billid));
				}
				
				$userassoc_plandata = $this->userassoc_plandata($uuid);
				$user_credit_limit = $userassoc_plandata['user_credit_limit'];
				$user_wallet_balance = $this->userbalance_amount($uuid);
				$plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
				$account_activated_on = $userassoc_plandata['account_activated_on'];
				$total_advpay = $userassoc_plandata['total_advpay'];
				
				if($account_activated_on != '0000-00-00'){
					$actiondate = $this->affected_date_on_wallet($uuid);
					$expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
					$expiration_date = date('Y-m-d', strtotime($actiondate . " +".$expiration_acctdays." days"));
					$expiration_date = $expiration_date.' 00:00:00';
					$this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $uuid));
				}
				
				$this->notify_model->add_user_activitylogs($uuid, "$billtype bill: $bill_number, Receipt Updated");
			}
		}
		echo json_encode($billid);
	}
	public function userbalance_amount($uuid){	
		$wallet_amt = 0;
		$walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
		if($walletQ->num_rows() > 0){
			$wallet_amt = $walletQ->row()->wallet_amt;
		}
		
		$passbook_amt = 0;
		$passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
		if($passbookQ->num_rows() > 0){
			$passbook_amt = $passbookQ->row()->plan_cost;
		}
		$balanceamt = ($wallet_amt - $passbook_amt);
		return $balanceamt;
	}
	public function addbilling($uuid,$billtype,$srvid,$paymode,$actual_amt,$disc,$totalamt,$topup_dayscount){	
		$login_isp = $this->session->userdata['isp_session']['userid'];
		$user_wallet_balance = $this->userbalance_amount($uuid);
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		
		$billnumber = $this->generate_billnumber();
		$billingarr = array(
			'subscriber_uuid' => $uuid,
			'isp_uid' => $isp_uid,
			'plan_id' => $srvid,
			'bill_added_on' => date('Y-m-d H:i:s'),
			'bill_number' => $billnumber,
			'bill_type' => $billtype,
			'payment_mode' => $paymode,
			'actual_amount' => $actual_amt,
			'discount' => $disc,
			'total_amount' => $totalamt,
			'alert_user' => '1',
			'number_of_days' => $topup_dayscount,
			'bill_generate_by' => $login_isp
		);
		
		if($paymode != 'Free'){
			if($totalamt <= $user_wallet_balance){
				$billingarr['receipt_received'] = '1';
				$billingarr['receipt_number'] = '';
				$billingarr['payment_mode'] = 'wallet';
				
			}elseif(($totalamt >= $user_wallet_balance) && ($user_wallet_balance > 0)){
				$billingarr['adjusted_amount'] = $user_wallet_balance;
			}
		}
		
		$this->db->insert('sht_subscriber_billing', $billingarr);
		$billid = $this->db->insert_id();
		$this->delete_temp_billnumber($isp_uid, $billnumber);
		
		$this->emailer_model->topups_billing($uuid, $billid);
		
		$this->passbook_entry($uuid, $billid, $srvid, $totalamt);
		return 1;
	}
	public function userwallet_amount($uuid){
		$data = array();
		$walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amount, COALESCE(SUM(left_amount),0) as left_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
		if($walletQ->num_rows() > 0){
			$wrowdata = $walletQ->row();
			$data['wallet_leftamt'] = $wrowdata->left_amt;
			$data['wallet_amt'] = $wrowdata->wallet_amount;
		}
		return $data;
	}
	public function addtowallet(){
		$login_isp = $this->session->userdata['isp_session']['userid'];
		$uuid = $this->input->post('subscriber_uuid');
		$wallet_amount = $this->input->post('addtowallet_amount');
		$walletbillid_foredit = $this->input->post('walletbillid_foredit');
		$walletid_foredit = $this->input->post('walletid_foredit');
		
		$receipt_number = $this->input->post('addtowallet_receipt_number');
		if($receipt_number != ''){
			$receipt_received = '1';
			$this->notify_model->add_user_activitylogs($uuid, "Amount added to Wallet");
			$bill_paid_on = date('Y-m-d H:i:s');
		}else{
			$receipt_received = '0';
			$this->notify_model->add_user_activitylogs($uuid, "Wallet Amount Request Added");
			$bill_paid_on = '0000-00-00 00:00:00';
		}
		
		$receiptArr = array();
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		$uuid = $this->input->post('subscriber_uuid');
		
		$slipimage = '';
		if(isset($_FILES) && (count($_FILES) > 0)){
			$slipimage = $_FILES['bill_slipimage']['name'];
		}
		$update_payment_mode = $this->input->post('addtowallet_payment_mode');
		if(($update_payment_mode == 'cheque') || ($update_payment_mode == 'dd')){
			$receiptArr['account_number'] = $this->input->post('bill_bankaccount_number');
			$receiptArr['branch_name'] = $this->input->post('bill_bankname');
			$receiptArr['branch_address'] = $this->input->post('bill_bankaddress');
			$receiptArr['bankslip_issued_date'] = $this->input->post('bill_bankissued_date');
			$receiptArr['bankslip_image'] = $slipimage;
			$receiptArr['cheque_dd_paytm_number'] = $this->input->post('update_cheque_dd_paytm');
		}
		if(($update_payment_mode == 'viapaytm') || ($update_payment_mode == 'netbanking')){
			$receiptArr['cheque_dd_paytm_number'] = $this->input->post('update_cheque_dd_paytm');
			
		}
		$wallet_amount_received = $this->input->post('wallet_amount_received');
		
		$walletArr = array(
			'isp_uid' => $isp_uid,
			'subscriber_uuid' => $uuid,
			'wallet_amount' => $wallet_amount,
			'added_on'	=> date('Y-m-d H:i:s')
		);
		
		if($walletid_foredit != ''){
			$this->db->update('sht_subscriber_wallet', $walletArr, array('id' => $walletid_foredit));
			$wlastid = $walletid_foredit;
		}else{
			$this->db->insert('sht_subscriber_wallet', $walletArr);
			$wlastid = $this->db->insert_id();
		}
		
		$wallet_billarr = array(
			'isp_uid' => $isp_uid,
			'subscriber_id' => $this->input->post('subscriber_userid'),
			'subscriber_uuid' => $this->input->post('subscriber_uuid'),
			'wallet_id' => $wlastid,
			'bill_added_on' => $this->input->post('addtowallet_bill_datetime'),
			'receipt_number' => $this->input->post('addtowallet_receipt_number'),
			'bill_type' => $this->input->post('addtowallet_bill_transaction_type'),
			'payment_mode' => $this->input->post('addtowallet_payment_mode'),
			'actual_amount' => $this->input->post('addtowallet_amount'),
			'total_amount' => $this->input->post('addtowallet_amount'),
			'receipt_received' => $receipt_received,
			'alert_user' => $this->input->post('addtowallet_send_copy_to_customer'),
			'bill_generate_by' => $login_isp,
			'bill_paid_on' => $bill_paid_on,
			'wallet_amount_received' => $wallet_amount_received
		);
		
		if($walletbillid_foredit != ''){
			$this->db->update('sht_subscriber_billing', $wallet_billarr, array('id' => $walletbillid_foredit));
			$wlast_billid = $walletbillid_foredit;
		}else{
			$this->db->insert('sht_subscriber_billing', $wallet_billarr);
			$wlast_billid = $this->db->insert_id();
		}
		if(count($receiptArr) > 0){
			$receiptArr['isp_uid'] = $isp_uid;
			$receiptArr['uid'] = $uuid;
			$receiptArr['bill_id'] = $wlast_billid;
			$receiptArr['receipt_number'] = $this->input->post('addtowallet_receipt_number');
			$receiptArr['receipt_amount'] = $this->input->post('addtowallet_amount');
			$receiptArr['added_on'] = date('Y-m-d H:i:s');
			
			$checkrcpthistoryQ = $this->db->query("SELECT id FROM sht_subscriber_receipt_history WHERE bill_id='".$wlast_billid."'");
			if($checkrcpthistoryQ->num_rows() > 0){
				$this->db->update('sht_subscriber_receipt_history', $receiptArr, array('bill_id' => $wlast_billid));
			}else{
				$this->db->insert('sht_subscriber_receipt_history', $receiptArr);
			}
		}else{
			$this->db->delete('sht_subscriber_receipt_history', array('bill_id' => $wlast_billid));
		}
		
		$userassoc_plandata = $this->userassoc_plandata($uuid);
		$plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
		$account_activated_on = $userassoc_plandata['account_activated_on'];
		
		$user_credit_limit = $userassoc_plandata['user_credit_limit'];
		$total_advpay = $userassoc_plandata['total_advpay'];
		$user_wallet_balance = $this->userbalance_amount($uuid);
		if($account_activated_on != '0000-00-00'){
			$actiondate = $this->affected_date_on_wallet($uuid);
			$expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
			$expiration_date = date('Y-m-d', strtotime($actiondate . " +".$expiration_acctdays." days"));
			$expiration_date = $expiration_date.' 00:00:00';
			$this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $uuid));
		}

		if($wallet_amount_received == 1){
			$useralerts_status = $this->useralerts_status($uuid);
			$sms_alert = $useralerts_status['sms_alert'];
			$email_alert = $useralerts_status['email_alert'];
			if($sms_alert == 1){
				$this->notify_model->payment_received_alert($uuid, $wallet_amount);
			}
		}
		echo json_encode($uuid);
	}
	public function passbook_entry($uuid, $billid, $srvid, $wallet_amount){
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$dept_id = $sessiondata['dept_id'];
		$isp_uid = $sessiondata['isp_uid'];
		
		$walletArr = array(
		'isp_uid' => $isp_uid,
		'subscriber_uuid' => $uuid,
		'billing_id' => $billid,
		'srvid' => $srvid,
		'plan_cost' => $wallet_amount,
		'added_on'	=> date('Y-m-d H:i:s')
		);
		$this->db->insert('sht_subscriber_passbook', $walletArr);
		return 1;
	}
	public function get_actual_plancost($srvid, $used_netdays, $uuid){
		$userplantypeQ = $this->db->query("SELECT user_plan_type FROM `sht_users` WHERE `uid` = '".$uuid."'");
		$usertype = $userplantypeQ->row()->user_plan_type;
		
		$gross_amt = 0;
		$get_grossamtQ = $this->db->query("SELECT gross_amt FROM `sht_plan_pricing` WHERE `srvid` = '".$srvid."'");
		if($get_grossamtQ->num_rows() > 0){
			$gross_amt = $get_grossamtQ->row()->gross_amt;
			$plan_cost_perday = ($gross_amt / 30);
		}
		
		$custom_planpriceArr = $this->getcustom_planprice($uuid, $srvid);
		$custom_planprice = $custom_planpriceArr['gross_amt'];
		if($custom_planprice != ''){
		    $gross_amt = (($custom_planprice * 100) / 118);
		    $plan_cost_perday = ($custom_planprice / 30);
		}
		
		if($usertype == 'prepaid'){
			return round($gross_amt);    
		}else{
			if($used_netdays > 27){
				return round($gross_amt);    
			}else{
				return round($plan_cost_perday * $used_netdays);
			}
		}
	}
	public function getbilldetail(){
		$data = array();
		$billid = $this->input->post('billid');
		$billtype = $this->input->post('billtype');
		if($billtype == 'custom_invoice'){
			$custbillQ = $this->db->query("SELECT * FROM sht_subscriber_custom_billing WHERE id='".$billid."' AND is_deleted='0' ORDER BY id DESC");
			if($custbillQ->num_rows() > 0){
				$rowdata = $custbillQ->row();
				$data['billid'] = $billid;
				$data['bill_number'] = $rowdata->bill_number;
				$data['receipt_number'] = $rowdata->receipt_number;
				$data['uuid'] = $rowdata->uid;
				$data['paymode'] = $rowdata->payment_mode;
				$data['discount'] = $rowdata->discount;
				$data['total_amount'] = $rowdata->total_amount;
			}
		}else{
			$receipt_amount = 0;
			$query = $this->db->query("SELECT bill_type,wallet_id,bill_number,receipt_number,subscriber_uuid,plan_id,plan_cost ,payment_mode,actual_amount,discounttype,discount,total_amount,advancepay_for_months,number_of_free_months,bill_added_on,adjusted_amount,receipt_received,bill_comments,bill_remarks,is_advanced_paid,bill_starts_from FROM sht_subscriber_billing WHERE id = '".$billid."'");
			$numrows = $query->num_rows();
			if($numrows > 0){
				$rowdata = $query->row();
				$data['billid'] = $billid;
				$data['walletid'] = $rowdata->wallet_id;
				$data['bill_number'] = $rowdata->bill_number;
				$data['receipt_number'] = $rowdata->receipt_number;
				$data['uuid'] = $rowdata->subscriber_uuid;
				$data['srvid'] = $rowdata->plan_id;
				
				$pacttimestamp = strtotime($rowdata->bill_starts_from);
				$todtimestamp = strtotime($rowdata->bill_added_on);
				$used_netdays = round(abs($todtimestamp - $pacttimestamp) / 86400);
				
				if($rowdata->bill_type == 'midchange_plancost'){
					$total_bill_cost = $rowdata->total_amount;
					$data['plan_cost'] = round(($total_bill_cost * 100) / 118);
				}else{
					$data['plan_cost'] = $this->get_actual_plancost($rowdata->plan_id, $used_netdays, $rowdata->subscriber_uuid);
				}
				$data['paymode'] = $rowdata->payment_mode;
				$data['actual_amount'] = $rowdata->actual_amount;
				$data['discounttype'] = $rowdata->discounttype;
				$data['discount'] = $rowdata->discount;
				$data['total_amount'] = $rowdata->total_amount;
				$data['advancepay_for_months'] = $rowdata->advancepay_for_months;
				$data['number_of_free_months'] = $rowdata->number_of_free_months;
				$data['bill_added_on'] = date('d-m-Y H:i:s', strtotime($rowdata->bill_added_on));
				$data['adjusted_amount'] = (int)$rowdata->adjusted_amount;
				$data['pending_amount'] = ($data['total_amount'] - $receipt_amount);
				$data['receipt_amount'] = (int)$receipt_amount;
				$data['bill_comments'] = $rowdata->bill_comments;
				$data['bill_remarks'] = $rowdata->bill_remarks;
				$data['is_advanced_paid'] = $rowdata->is_advanced_paid;
				
				$receipt_received = $rowdata->receipt_received;
				$payment_mode = $rowdata->payment_mode;
				if($receipt_received == '1'){
					$data['billpaid'] = '1';
				}else{
					$data['billpaid'] = '0';
				}
				
				$additional_charges = ''; $addiamount = 0;
				$addichrgQ = $this->db->query("SELECT * FROM sht_billing_additional_charges WHERE bill_id='".$billid."'");
				if($addichrgQ->num_rows() > 0){
					foreach($addichrgQ->result() as $addchrgobj){
						$addid = $addchrgobj->id;
						$charges_details = $addchrgobj->charges_details;
						$amount = $addchrgobj->amount;
						
						$addiamount += $amount;
						$additional_charges .= '<div class="addichrgrow row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><input type="text" name="additional_chrgtxt['.$addid.']" class="form-control" value="'.$charges_details.'" placeholder="Enter Charges Details" style="font-size:12px" /></div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><input type="number" name="additional_chrgval['.$addid.']" class="form-control" value="'.$amount.'" style="font-size:12px" /></div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><a href="javascript:void(0)" onclick="removemorecharges(this)"><i class="fa fa-minus" aria-hidden="true"></i> &nbsp;Remove</a></div></div>';
					}
				}
				
				$data['additional_chargesdiv'] = $additional_charges;
				$data['addiamount'] = $addiamount;
			}
		}
		
		echo json_encode($data);
	}
	public function delete_billentry(){
		$billid = $this->input->post('billid');
		$uuid = $this->input->post('uid');
		$this->db->update('sht_subscriber_billing', array('is_deleted' => '1'), array('id' => $billid));
		
		$bill_number = $this->getbillnumber($billid);
		$this->notify_model->add_user_activitylogs($uuid, "Bill: $bill_number, Deleted");
		echo json_encode(true);
	}
	public function update_walletLimit(){
		$data = array();
		$user_creditlimit = $this->input->post('walletLimit');
		$uuid = $this->input->post('uuid');
		$optionsel = $this->input->post('optionsel');
		
		$curr_creditlimit = 0;
		$wallet_amt = $this->userbalance_amount($uuid);
		$curr_creditlimitQ = $this->db->query("SELECT user_credit_limit FROM sht_users WHERE uid='".$uuid."'");
		if($curr_creditlimitQ->num_rows() > 0){
			$curr_creditlimit = $curr_creditlimitQ->row()->user_credit_limit;
		}
		
		$userPlanQ = $this->db->query("SELECT tb2.net_total, tb2.gross_amt, tb1.paidtill_date, tb3.plan_duration FROM sht_users as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.baseplanid = tb2.srvid) INNER JOIN sht_services as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb1.uid='".$uuid."' AND tb1.enableuser='1'");
		if($userPlanQ->num_rows() > 0){
			$uprow = $userPlanQ->row();			
			$tax = $this->current_taxapplicable();
			$plan_duration = $uprow->plan_duration;
			$gross_amt = ($uprow->gross_amt * $plan_duration);
			$planprice = round($gross_amt + (($gross_amt * $tax)/100));
			$plan_tilldays = ($plan_duration * 30);
			$plan_cost_perday = round($planprice/$plan_tilldays);
			$actiondate = $this->affected_date_on_wallet($uuid);
			$total_advpay = $this->user_advpayments($uuid);
			
			if($optionsel == 'bydays'){
				$user_creditlimit = round($curr_creditlimit + ($plan_cost_perday * $user_creditlimit));
			}
			
			$expiration_acctdays = round(($user_creditlimit + $wallet_amt + $total_advpay) / $plan_cost_perday);
			$expiration_date = date('Y-m-d', strtotime($actiondate . " +".$expiration_acctdays." days"));
			$expiration_date = $expiration_date.' 00:00:00';
			$todaydate = date('Y-m-d').' 00:00:00';
			if(strtotime($todaydate) == strtotime($expiration_date)){
				$this->db->update('sht_users', array('user_credit_limit' => $user_creditlimit, 'expiration' => $expiration_date, 'enableuser' => '0', 'inactivate_type' => 'suspended', 'suspended_days' => '0', 'suspendedon' => date('Y-m-d')), array('uid' => $uuid));
				
				$login_userid = $this->session->userdata['isp_session']['userid'];
				$this->db->insert('sht_users_status_report', array('uid' => $uuid, 'status_changeby' => $login_userid, 'inactivate_type' => 'suspended', 'suspended_days' => '0', 'added_on' => date('Y-m-d H:i:s')));
				$nasipQ = $this->db->query("SELECT tb1.username, tb1.nasipaddress, tb1.framedipaddress, tb2.secret FROM radacct as tb1 INNER JOIN nas as tb2 ON(tb1.nasipaddress=tb2.nasname) WHERE tb1.username='".$uuid."' ORDER BY radacctid DESC LIMIT 1");
				if($nasipQ->num_rows() > 0){
					$nasrowdata = $nasipQ->row();
					$username = $nasrowdata->username;
					$userframedipaddress = $nasrowdata->framedipaddress;
					$usernasipaddress = $nasrowdata->nasipaddress;
					$secret = $nasrowdata->secret;
					//@exec("echo User-Name:=$username,Framed-Ip-Address=$userframedipaddress | radclient -x $usernasipaddress:3799 disconnect $secret");
					$this->disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret);
                                        $this->db->query("UPDATE radacct SET acctstoptime='".date('Y-m-d H:i:s')."' WHERE username='".$uuid."' AND acctstoptime IS NULL");
				}
				
				$data['user_suspended'] = '1';
			}else{
				$this->db->update('sht_users', array('user_credit_limit' => $user_creditlimit, 'expiration' => $expiration_date), array('uid' => $uuid));
				$data['user_suspended'] = '0';
			}
			
			$data['user_creditlimit'] = $user_creditlimit;
			$this->notify_model->add_user_activitylogs($uuid, "Updated Wallet Credit Limit From $curr_creditlimit to $user_creditlimit");
		}
		
		echo json_encode($data);
	}
	public function updatebilling_charges(){
		//print_r($_POST); die;
		$billingArr = array(); $slipimage = '';
		$billtype = $this->input->post('editbilltype');
		$billid = $this->input->post('editbillid');
		$billpaid = $this->input->post('editbillpaid');
		$billnumber = $this->input->post('editdbbill_number');
		$billdiscounttype = $this->input->post('discounttype');
		$total_discount = 0;
		if($billdiscounttype == 'percent'){
			$total_discount = $this->input->post('editdbbill_percentdiscount');
		}elseif($billdiscounttype == 'flat'){
			$total_discount = $this->input->post('editdbbill_flatdiscount');
		}
		
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$uuid = $this->input->post('subscriber_uuid');

		$prevbillamount = $this->input->post('editbill_prevbillamount');
		$netamount = $this->input->post('editdbbill_netamount');
		
		$is_advanced_paid = $this->input->post('is_advanced_paid');
		if($is_advanced_paid == 1){
			$billingArr = array(
				'hsn_number' => $this->input->post('editdbbill_hsncode'),
				'bill_comments' => $this->input->post('editdbbill_comments'),
				'bill_remarks' => $this->input->post('editdbbill_remarks'),
				'is_advanced_paid' => $this->input->post('is_advanced_paid')
			);	
		}else{
			$billingArr = array(
				'bill_type' => $this->input->post('editbilltype'),
				'actual_amount' => $this->input->post('editdbbill_billamount'),
				'discounttype' => $billdiscounttype,
				'discount' => $total_discount,
				'total_amount' => $this->input->post('editdbbill_netamount'),
				'hsn_number' => $this->input->post('editdbbill_hsncode'),
				'bill_comments' => $this->input->post('editdbbill_comments'),
				'bill_remarks' => $this->input->post('editdbbill_remarks'),
				'is_advanced_paid' => $this->input->post('is_advanced_paid')
			);
		}
		$billingArr['service_tax'] = '0.00';
		$billingArr['swachh_bharat_tax'] = '0.00';
		$billingArr['krishi_kalyan_tax'] = '0.00';
		$billingArr['cgst_tax'] = '0.00';
		$billingArr['sgst_tax'] = '0.00';
		$billingArr['igst_tax'] = '0.00';
		
		if(($billtype != 'installation') && ($billtype != 'security') && ($billtype != ' advprepay') && ($billtype != ' addtowallet')){
			//$this->db->query("UPDATE sht_subscriber_passbook SET plan_cost = $netamount, added_on='".date('Y-m-d H:i:s')."' WHERE billing_id='".$billid."'");
		}
		
		$this->db->update('sht_subscriber_billing', $billingArr, array('id' => $billid));
		
		if($is_advanced_paid == 0){
			$additional_chrgtxtArr = $this->input->post('additional_chrgtxt');
			$additional_chrgvalArr = $this->input->post('additional_chrgval');
			
			$addikeyarr = array(); $addipricearr = array();
			foreach($additional_chrgtxtArr as $key => $chrgtxt){
				$chrgval = $additional_chrgvalArr[$key];
				if(($chrgtxt != '') && ($chrgval != '')){
					$addipricearr[] = $chrgval;
					$addichrgQ = $this->db->query("SELECT * FROM sht_billing_additional_charges WHERE id='".$key."' AND bill_id='".$billid."'");
					if($addichrgQ->num_rows() > 0){
						$this->db->update("sht_billing_additional_charges", array('charges_details' => $chrgtxt, 'amount' => $chrgval, 'added_on' => date('Y-m-d H:i:s')), array('bill_id' => $billid, 'id' => $key));
						$addikeyarr[] = $key;
					}else{
						$this->db->insert("sht_billing_additional_charges", array('bill_id' => $billid, 'charges_details' => $chrgtxt, 'amount' => $chrgval, 'added_on' => date('Y-m-d H:i:s')));
						$addikeyarr[] = $this->db->insert_id();
					}
				}
			}
			
			$actual_amount = $this->input->post('editdbbill_billamount');
			if($billdiscounttype == 'percent'){
				$netamt = $actual_amount - round(($actual_amount * $total_discount)/100);
			}else{
				$netamt = ($actual_amount - $total_discount);
			}
			$tax = $this->current_taxapplicable();
			$netamt = round($netamt + ($netamt * $tax)/100);
			if(count($addipricearr) > 0){
				$addiprice = array_sum($addipricearr);
				$netamt = $netamt + $addiprice;
				$this->db->query("UPDATE sht_subscriber_billing SET total_amount = $netamt WHERE id=$billid");
				$this->db->query("UPDATE sht_subscriber_passbook SET plan_cost = $netamt, added_on='".date('Y-m-d H:i:s')."' WHERE billing_id='".$billid."'");
			}else{
				$this->db->query("UPDATE sht_subscriber_billing SET total_amount = $netamt WHERE id=$billid");
				$this->db->query("UPDATE sht_subscriber_passbook SET plan_cost = $netamt, added_on='".date('Y-m-d H:i:s')."' WHERE billing_id='".$billid."'");
			}
			
			$addikeyarr = array_filter($addikeyarr, function($a) { return ($a !== 0); });
			if(count($addikeyarr) > 0){
				$chkallkeys = implode(',',$addikeyarr);
				$this->db->query("DELETE FROM sht_billing_additional_charges WHERE id NOT IN ($chkallkeys) AND bill_id='".$billid."'");
			}else{
				$this->db->query("DELETE FROM sht_billing_additional_charges WHERE bill_id='".$billid."'");
			}
		}
		$userassoc_plandata = $this->userassoc_plandata($uuid);
		$user_credit_limit = $userassoc_plandata['user_credit_limit'];
		$user_wallet_balance = $this->userbalance_amount($uuid);
		$plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
		$account_activated_on = $userassoc_plandata['account_activated_on'];
		$total_advpay = $userassoc_plandata['total_advpay'];
		
		if($account_activated_on != '0000-00-00'){
			$actiondate = $this->affected_date_on_wallet($uuid);
			$expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
			$expiration_date = date('Y-m-d', strtotime($actiondate . " +".$expiration_acctdays." days"));
			$expiration_date = $expiration_date.' 00:00:00';
			$this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $uuid));
		}
		
		$this->notify_model->add_user_activitylogs($uuid, "Bill: ".$billnumber." Amount changed from ".$prevbillamount." to ".$netamount);
		
		echo json_encode(true);
	}
	public function billcopy_actions(){
		$billid = $this->input->post('billid');
		$billtype = $this->input->post('billtype');
		$uid = $this->input->post('uid');
		$action = $this->input->post('action');
		
		if($billtype == 'installation'){
			$this->emailer_model->installation_billing($uid, $billid, $action);
		}elseif($billtype == 'security'){
			$this->emailer_model->security_billing($uid, $billid, $action);
		}elseif($billtype == 'montly_pay'){
			$this->emailer_model->monthly_billing($uid, $billid, $action);
		}elseif($billtype == 'midchange_plancost'){
			$this->emailer_model->monthly_billing($uid, $billid, $action);
		}elseif($billtype == 'topup'){
			$this->emailer_model->topups_billing($uid, $billid, $action);
		}elseif($billtype == 'addtowallet'){
			$this->emailer_model->addtowallet_billing($uid, $billid, $action);
		}elseif($billtype == 'custom_invoice'){
			$this->emailer_model->custominvoice_billing($uid, $billid, $action);
		}elseif($billtype == 'advprepay'){
			$this->emailer_model->advmonthly_billing($uid, $billid, $action);
		}else{
			echo 'No Bill Available.';
		}
		
	}
	public function check_billpaid($billid){
		$billpaid = 0;
		$query = $this->db->query("SELECT id FROM sht_subscriber_billing WHERE id='".$billid."' AND status='1' AND is_deleted='0' AND payment_mode != '' AND receipt_received = '1'");
		if($query->num_rows() > 0){
			$billpaid = 1;
		}
		return $billpaid;
	}
	public function getbillid_forchangeplan($uuid, $srvid){
		$data = array();
		$billQ = $this->db->query("SELECT id FROM sht_subscriber_billing WHERE subscriber_uuid='".$uuid."' AND plan_id='".$srvid."' AND (bill_type='montly_pay' OR bill_type='midchange_plancost') AND status='1' AND is_deleted='0' ORDER BY id DESC LIMIT 1");
		if($billQ->num_rows() > 0){
			$rowdata = $billQ->row();
			$billid = $rowdata->id;
			$billpaid = $this->check_billpaid($billid);
			
			$data['billid'] = $billid;
			$data['billpaid'] = $billpaid;
		}
		return $data;
	}
	public function getdetails_custombilling(){
		$data = array(); $cgen = ''; $tgen = '';
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$companyQ = $this->db->query("SELECT id,company_name FROM sht_isp_company_details WHERE isp_uid='".$isp_uid."'");
		$comp_numrows = $companyQ->num_rows();
		if($comp_numrows > 0){
			foreach($companyQ->result() as $compobj){
				$company_id = $compobj->id;
				$company_name = $compobj->company_name;
				$cgen .= '<option value="'.$company_id.'">'.$company_name.'</option>';
			}
		}
		
		$isptopicsQ = $this->db->query("SELECT id,topic_name FROM sht_isp_bill_topics WHERE isp_uid='".$isp_uid."'");
		$topic_numrows = $isptopicsQ->num_rows();
		if($topic_numrows > 0){
			foreach($isptopicsQ->result() as $topicobj){
				$topic_id = $topicobj->id;
				$topic_name = $topicobj->topic_name;
				$tgen .= '<option value="'.$topic_id.'">'.$topic_name.'</option>';
			}
		}
		
		$data['company_list'] = $cgen;
		$data['topic_list'] = $tgen;
		
		echo json_encode($data);
	}
	public function addisp_company(){
		$data = array(); $cgen = ''; 
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$company_name = $this->input->post('bill_company_name');
		$company_tax = $this->input->post('bill_company_tax');
		
		if(($company_tax != '') && ($company_name != '')){
			$this->db->insert('sht_isp_company_details', array('isp_uid' => $isp_uid, 'company_name' => $company_name, 'tax' => $company_tax, 'added_on' => date('Y-m-d H:i:s')));
			$companyQ = $this->db->query("SELECT id,company_name FROM sht_isp_company_details WHERE isp_uid='".$isp_uid."'");
			$comp_numrows = $companyQ->num_rows();
			if($comp_numrows > 0){
				foreach($companyQ->result() as $compobj){
					$company_id = $compobj->id;
					$company_name = $compobj->company_name;
					$cgen .= '<option value="'.$company_id.'">'.$company_name.'</option>';
				}
			}
			
			$data['company_list'] = $cgen;
		}
		
		echo json_encode($data);
	}

	public function addisp_topic(){
		$data = array(); $tgen = ''; 
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$topic_name = $this->input->post('topic_name');
		$bill_topictype = $this->input->post('bill_topictype');
		$topic_rate = $this->input->post('topic_rate');
		$taxtypes = $this->input->post('taxtypes');
		
		$this->db->insert('sht_isp_bill_topics', array('isp_uid' => $isp_uid, 'topic_name' => $topic_name, 'rate_type' => $bill_topictype, 'price' => $topic_rate, 'tax_applicable' => $taxtypes, 'added_on' => date('Y-m-d H:i:s')));
		$isptopicsQ = $this->db->query("SELECT id,topic_name FROM sht_isp_bill_topics WHERE isp_uid='".$isp_uid."'");
		$topic_numrows = $isptopicsQ->num_rows();
		if($topic_numrows > 0){
			foreach($isptopicsQ->result() as $topicobj){
				$topic_id = $topicobj->id;
				$topic_name = $topicobj->topic_name;
				$tgen .= '<option value="'.$topic_id.'">'.$topic_name.'</option>';
			}
			$data['topic_list'] = $tgen;
		}
		
		echo json_encode($data);
	}
	
	public function gettopicdetails(){
		$data = array();
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$topicid = $this->input->post('topicid');
		$isptopicsQ = $this->db->query("SELECT topic_name, rate_type,price,tax_applicable FROM sht_isp_bill_topics WHERE isp_uid='".$isp_uid."' AND id='".$topicid."'");
		$topic_numrows = $isptopicsQ->num_rows();
		if($topic_numrows > 0){
			$data = $isptopicsQ->row_array();
		}
		echo json_encode($data);
	}
	public function addcustombill_invoice(){
		$data = array();
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$topicid = $this->input->post('topicid');
		$cust_uuid = $this->input->post('cust_uuid');
		$billid = $this->input->post('billid');
		$topic_price = $this->input->post('itmprice');
		$topic_qty = $this->input->post('itmqty');
		$total_price = ($topic_price * $topic_qty);
		$companyid = $this->input->post('companyid');
		
		$companyQ = $this->db->query("SELECT tax FROM sht_isp_company_details WHERE isp_uid='".$isp_uid."' AND id='".$companyid."'");
		$comp_numrows = $companyQ->num_rows();
		if($comp_numrows > 0){
			$billtax = $companyQ->row()->tax;
		}else{
			$billtax = '18'; //18%
		}
		
		$topicQ = $this->db->query("SELECT topic_name, rate_type,price,tax_applicable FROM sht_isp_bill_topics WHERE id='".$topicid."' AND isp_uid='".$isp_uid."'");
		$topicrow = $topicQ->row();
		$topic_name = $topicrow->topic_name;
		$billinvoice = array(
			'isp_uid' => $isp_uid,
			'uid' => $cust_uuid,
			'bill_companyid' => '',
			'topic_id' => $topicid,
			'topic_name' => $topic_name,
			'price' => $topic_price,
			'quantity' => $topic_qty,
			'total_price' => ($topic_price * $topic_qty),
			'added_on' => date('Y-m-d H:i:s')
		);
		if($billid != ''){
			$billinvoice['bill_id'] = $billid;
		}
		$this->db->insert('sht_custom_invoice_list', $billinvoice);
		$invoiceid = $this->db->insert_id();
		
		$custominvoiceQ = $this->db->query("SELECT SUM(total_price) as subtotal FROM sht_custom_invoice_list WHERE bill_id='".$billid."' AND isp_uid='".$isp_uid."' AND uid='".$cust_uuid."'");
		if($custominvoiceQ->num_rows() > 0){
			$data['subtotal'] = $custominvoiceQ->row()->subtotal;	
		}
		$data['custinvoicelist'] = '<tr bgcolor="#f9f9f9" class="custinvoicelist" id="invid_'.$invoiceid.'"><td colspan="5" width="550px">'.$topic_name.'</td><td>'.$topic_price.'</td><td>'.$topic_qty.'</td><td>&nbsp;</td><td class="text-right">'.$total_price.'</td><td class="text-right"><a href="javascript:void(0)" onclick="delete_custbillinvoice(&apos;'.$billid.'&apos;,'.$invoiceid.')"><i class="fa fa-times close-fa" aria-hidden="true"></i></a></td></tr>';
		
		$data['billtax_amt'] = round(($data['subtotal'] * $billtax) / 100 );
		$data['billtax'] = $billtax;
		echo json_encode($data);
	}
	
	public function generate_custombilling(){
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$billcompanyid = $this->input->post('billcompany_name');
		$cust_uuid = $this->input->post('cust_uuid');
		$custom_billno = $this->input->post('custom_billnumber');
		$billdate = $this->input->post('billdate');
		$remarks = $this->input->post('remarks');
		$comments = $this->input->post('comments');
		$billid = $this->input->post('billid');
		$discount = $this->input->post('discount');
		$billgenerated_aftermonth = $this->input->post('custombill_monthwise');
		$billgenerated_afterdays = $this->input->post('custombill_daywise');
		
		$companyQ = $this->db->query("SELECT tax FROM sht_isp_company_details WHERE isp_uid='".$isp_uid."' AND id='".$billcompanyid."'");
		$comp_numrows = $companyQ->num_rows();
		if($comp_numrows > 0){
			$billtax = $companyQ->row()->tax;
		}else{
			$billtax = '18'; //18%
		}
		
		$data = array();
		$custominvoiceQ = $this->db->query("SELECT COALESCE( SUM(total_price), 0 ) as total_bill, GROUP_CONCAT(topic_name)  as bill_details FROM sht_custom_invoice_list WHERE bill_id='$billid' AND isp_uid='".$isp_uid."' AND uid='".$cust_uuid."'");
		if($custominvoiceQ->num_rows() > 0){
			$rowdata = $custominvoiceQ->row();
			$total_bill = $rowdata->total_bill;
			if($total_bill != '0.00'){
				$total_billpay = round( $total_bill + (($total_bill * $billtax) / 100) );
				$total_billpay = ($total_billpay - $discount);
				
				if($billid != ''){
					$todaydate = date('Y-m-d');
					$custombillArr = array(
						'bill_details' => $rowdata->bill_details,
						'total_amount' => $total_billpay,
						'discount' => $discount,
						'bill_companyid' => $billcompanyid,
						//'bill_number' => $custom_billno,
						'bill_comments' => $comments,
						'bill_remarks' => $remarks,
						'added_on' => $billdate,
						'billgenerated_aftermonth' => $billgenerated_aftermonth,
						'billgenerated_afterdays' => $billgenerated_afterdays
					);
					
					$this->db->update('sht_subscriber_custom_billing', $custombillArr, array('id' => $billid));
					$this->checkfornext_billgenerated($billid);
				}else{
					//$this->db->update('sht_subscriber_custom_billing', array('isnext_billgenerated' => '1'), array('uid' => $cust_uuid, 'isp_uid' => $isp_uid));
					$billnumber = $this->generate_billnumber();
					$custombillArr = array(
						'isp_uid' => $isp_uid,
						'uid' => $cust_uuid,
						'receipt_received' => '0',
						'bill_companyid' => $billcompanyid,
						'bill_number' => $billnumber,
						'bill_type' => 'custom_bill',
						'bill_details' => $rowdata->bill_details,
						'payment_mode' => '',
						'discount' => $discount,
						'total_amount' => $total_billpay,
						'receipt_received' => '',
						'added_on' => $billdate,
						'bill_comments' => $comments,
						'bill_remarks' => $remarks,
						'receipt_number' => '',
						'is_deleted' => '0',
						'billgenerated_aftermonth' => $billgenerated_aftermonth,
						'billgenerated_afterdays' => $billgenerated_afterdays,
						'isnext_billgenerated' => '0'
					);
					
					$this->db->insert('sht_subscriber_custom_billing', $custombillArr);
					$billid = $this->db->insert_id();
					$this->delete_temp_billnumber($isp_uid, $billnumber);
				}
				
				$this->db->update("sht_subscriber_custom_billing", array('bill_uniqid' => 'cb'.$billid), array('id' => $billid));
				$this->db->update("sht_custom_invoice_list", array('bill_id' => $billid, 'bill_companyid' => $billcompanyid), array('isp_uid' => $isp_uid, 'uid' => $cust_uuid, 'bill_id' => ''));
				$this->notify_model->add_user_activitylogs($cust_uuid, "Custom Bill: $custom_billno Added");
				
				$data['message'] = 'ok';
			}else{
				$data['message'] = 'Please add invoices to generate billing.';
			}
		}else{
			$data['message'] = 'Please add invoices to generate billing.';
		}
		
		echo json_encode($data);
	}
	public function checkfornext_billgenerated($billid){
		$today_date = date('Y-m-d');
		$custombills = $this->db->query("SELECT id, uid, isp_uid, discount, DATE(added_on) as billdate, billgenerated_afterdays, billgenerated_aftermonth FROM sht_subscriber_custom_billing WHERE id='".$billid."' AND (billgenerated_afterdays != '0' OR billgenerated_aftermonth != '0') AND isnext_billgenerated='0'");
		if($custombills->num_rows() > 0){
		    foreach($custombills->result() as $custobj){
			$billdate = $custobj->billdate;
			$billid = $custobj->id;
			$uuid = $custobj->uuid;
			$isp_uid = $custobj->isp_uid;
			$billgenerated_afterdays = $custobj->billgenerated_afterdays;
			$billgenerated_aftermonth = $custobj->billgenerated_aftermonth;
			if($billgenerated_afterdays != '0'){
			    $billdate = date('Y-m-d', strtotime($billdate ."+$billgenerated_afterdays days"));
			}elseif($billgenerated_aftermonth != '0'){
			    $calculateddays = ($billgenerated_aftermonth * 30);
			    $billdate = date('Y-m-d', strtotime($billdate ."+$calculateddays days"));
			}
			$discount = $custobj->discount;
			
			//CHECK FOR BILL DATE
			if(strtotime($today_date) > strtotime($billdate)){
			    $this->db->update('sht_subscriber_custom_billing', array('isnext_billgenerated' => '1'), array('id' => $billid));
			    $billtax = '18'; //18%
			    $bill_companyid = 0;
			    $custominvoiceQ = $this->db->query("SELECT * FROM sht_custom_invoice_list WHERE bill_id='".$billid."'");
			    if($custominvoiceQ->num_rows() > 0){
				foreach($custominvoiceQ->result() as $custinvobj){
				    $uuid = $custinvobj->uid;
				    $topic_id = $custinvobj->topic_id;
				    $topic_name = $custinvobj->topic_name;
				    $bill_companyid = $custinvobj->bill_companyid;
				    $price = $custinvobj->price;
				    $quantity = $custinvobj->quantity;
				    $total_price = $custinvobj->total_price;
				    
				    $billinvoice = array(
					'isp_uid' => $isp_uid,
					'uid' => $uuid,
					'bill_companyid' => $bill_companyid,
					'topic_id' => $topic_id,
					'topic_name' => $topic_name,
					'price' => $price,
					'quantity' => $quantity,
					'total_price' => $total_price,
					'added_on' => $billdate.' '.date('H:i:s')
				    );
				    $this->db->insert('sht_custom_invoice_list', $billinvoice);
				    
				}
			    }
			    
			    $companyQ = $this->db->query("SELECT tax FROM sht_isp_company_details WHERE isp_uid='".$isp_uid."' AND id='".$bill_companyid."'");
			    $comp_numrows = $companyQ->num_rows();
			    if($comp_numrows > 0){
				$billtax = $companyQ->row()->tax;
			    }
			    $custominvoiceQ = $this->db->query("SELECT COALESCE( SUM(total_price), 0 ) as total_bill, GROUP_CONCAT(topic_name)  as bill_details FROM sht_custom_invoice_list WHERE bill_id='' AND isp_uid='".$isp_uid."' AND uid='".$uuid."'");
			    if($custominvoiceQ->num_rows() > 0){
				$rowdata = $custominvoiceQ->row();
				$total_bill = $rowdata->total_bill;
				if($total_bill != '0.00'){
				    $total_billpay = round( $total_bill + (($total_bill * $billtax) / 100) );
				    $total_billpay = ($total_billpay - $discount);
				
					$billnumber = $this->generate_billnumber();
				    $custombillArr = array(
					'isp_uid' => $isp_uid,
					'uid' => $uuid,
					'receipt_received' => '0',
					'bill_companyid' => $bill_companyid,
					'bill_number' => $billnumber,
					'bill_type' => 'custom_bill',
					'bill_details' => $rowdata->bill_details,
					'payment_mode' => '',
					'discount' => $discount,
					'total_amount' => $total_billpay,
					'receipt_received' => '',
					'added_on' => $billdate.' '.date('H:i:s'),
					'bill_comments' => '',
					'bill_remarks' => '',
					'receipt_number' => '',
					'is_deleted' => '0',
					'billgenerated_aftermonth' => $billgenerated_aftermonth,
					'billgenerated_afterdays' => $billgenerated_afterdays,
					'isnext_billgenerated' => '0'
				    );	
				    $this->db->insert('sht_subscriber_custom_billing', $custombillArr);
				    $nxtbillid = $this->db->insert_id();
				    $this->delete_temp_billnumber($isp_uid, $billnumber);
				}
				
				$this->db->update("sht_subscriber_custom_billing", array('bill_uniqid' => 'cb'.$nxtbillid), array('id' => $nxtbillid));
				$this->db->update("sht_custom_invoice_list", array('bill_id' => $nxtbillid), array('isp_uid' => $isp_uid, 'uid' => $uuid, 'bill_id' => ''));
			    }
			    
			    sleep(1);
			    $this->checkfornext_billgenerated($nxtbillid);
			}
			
		    }
		}
		return 1;
	}
	public function update_custombilling(){
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$billid = $this->input->post('billid_toedit');
		$billcompanyid = $this->input->post('billcompany_name');
		$cust_uuid = $this->input->post('cust_uuid');
		$custom_billno = $this->input->post('custom_billnumber');
		
		$companyQ = $this->db->query("SELECT tax FROM sht_isp_company_details WHERE isp_uid='".$isp_uid."' AND id='".$billcompanyid."'");
		$comp_numrows = $companyQ->num_rows();
		if($comp_numrows > 0){
			$billtax = $companyQ->row()->tax;
		}else{
			$billtax = '18'; //18%
		}
		
		$data = array();
		$custominvoiceQ = $this->db->query("SELECT COALESCE( SUM(total_price), 0 ) as total_bill, GROUP_CONCAT(topic_name)  as bill_details FROM sht_custom_invoice_list WHERE bill_id='".$billid."'");
		if($custominvoiceQ->num_rows() > 0){
			$rowdata = $custominvoiceQ->row();
			$total_bill = $rowdata->total_bill;
			if($total_bill != '0.00'){
				$total_billpay = round( $total_bill + (($total_bill * $billtax) / 100) );
				
				$custombillArr = array(
					'bill_details' => $rowdata->bill_details,
					'total_amount' => $total_billpay
				);
				
				$this->db->update('sht_subscriber_custom_billing', $custombillArr, array('id' => $billid));
				$billid = $this->db->insert_id();

				$this->notify_model->add_user_activitylogs($cust_uuid, "Custom Bill: $custom_billno Added");
				
				$data['message'] = 'ok';
			}else{
				$data['message'] = 'Please add invoices to generate billing.';
			}
		}else{
			$data['message'] = 'Please add invoices to generate billing.';
		}
		
		echo json_encode($data);
	}
	
	public function customBillHandler(){
		$data = array();
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$cust_uuid = $this->input->post('cust_uuid');
		$deleteconfirm = $this->input->post('deleteconfirm');
		$custominvoiceQ = $this->db->query("SELECT * FROM sht_custom_invoice_list WHERE bill_id='' AND isp_uid='".$isp_uid."' AND uid='".$cust_uuid."'");
		if($custominvoiceQ->num_rows() > 0){
			$data['isexists'] = 1;
			if($deleteconfirm == 1){
				$this->db->delete('sht_custom_invoice_list', array('bill_id' => '', 'isp_uid' => $isp_uid, 'uid' =>$cust_uuid));
			}
		}else{
			$data['isexists'] = 0;
		}
		
		echo json_encode($data);
	}
	
	public function custombilling_listing(){
		$data = array(); $gen = '';
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$data['superadmin_perm'] = $superadmin;
		if($superadmin != '1'){
			$billreadperm = $this->is_permission('CUSTOMBILLS','RO');
			if($billreadperm == false){
				$data['custbilltab_readperm'] = '0';
				
				if($this->is_permission('CUSTOMBILLS','ADD') == true){
					$data['custbill_addperm'] = '1';
				}
				if($this->is_permission('CUSTOMBILLS','EDIT') == true){
					$data['custbill_editperm'] = '1';
				}
				if($this->is_permission('CUSTOMBILLS','DELETE') == true){
					$data['custbill_deleteperm'] = '1';
				}
				if($this->is_permission('CUSTOMBILLS','HIDE') == false){
					$data['custbill_hideperm'] = '1';
				}
			}else{
				$data['custbilltab_readperm'] = '1';
			}
		}
		
		$cust_uuid = $this->input->post('subs_uuid');
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$ispcodet = $this->countrydetails();
		$cocurrency = $ispcodet['currency'];
		
		$totalbillamt = 0; $totalrcptamt = 0; $totalbalamt = 0;
		$custbillQ = $this->db->query("SELECT * FROM sht_subscriber_custom_billing WHERE isp_uid='".$isp_uid."' AND uid='".$cust_uuid."' AND is_deleted='0' ORDER BY id DESC");
		if($custbillQ->num_rows() > 0){
			$i = 1;
			foreach($custbillQ->result() as $billobj){
				$billid = $billobj->id;
				$billtype = 'custom_invoice';
				$bill_no = $billobj->bill_number;
				$total_amount = $billobj->total_amount;
				$receipt_received = $billobj->receipt_received;
				$isnext_billgenerated = $billobj->isnext_billgenerated;
				
				$added_rcptamt = 0;
				$recptQ = $this->db->query("SELECT COALESCE(SUM(receipt_amount),0) as receipt_amount FROM sht_custom_receipt_list WHERE bill_id='".$billid."'");
				if($recptQ->num_rows() > 0){
					$added_rcptamt = $recptQ->row()->receipt_amount;
				}
				
				$pending_amount = ($total_amount - $added_rcptamt);
				$recno = ($receipt_received == '0') ? '<a href="javascript:void(0)" onclick="show_customReceiptModal('.$billid.',\''.$bill_no.'\',\''.$pending_amount.'\')" class="isbillperm"> Enter Receipt</a>' : "Paid";
				
				if(($pending_amount != 0) && ($added_rcptamt != 0)){
					$total_amount = number_format($pending_amount,2).' ('.$added_rcptamt.')';
				}
				
				$billstatus = ($receipt_received == '0') ? 'Pending' : 'Paid';
				if($receipt_received == '0'){ $rowstyle="style='background-color:#F4C205;text-align:center'"; }
				else{ $rowstyle="style='background-color:#4DDC54;text-align:center'"; }
				
				$billcopy = '<td><a href="javascript:void(0)" onclick="edit_custbillentry('.$billid.',\''.$billtype.'\')" class="edit_cbillperm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> | <a href="javascript:void(0)" onclick="billcopy_actions('.$billid.',\''.$billtype.'\', \''.$cust_uuid.'\', \'sendbill\')" style="text-decoration: none; color:#29abe2; title="Send Bill on Mail" class="edit_cbillperm"><i class="fa fa-envelope-o" aria-hidden="true"></i></a> | <a href="javascript:void(0)" onclick="billcopy_actions('.$billid.',\''.$billtype.'\', \''.$cust_uuid.'\', \'viewbill\')" style="text-decoration: none; color:#29abe2;" title="View Bill" class="edit_cbillperm"><i class="fa fa-eye" aria-hidden="true"></i></a> | <a href="'.base_url().'user/download_custombillpdf/'.$billid.'" target="_blank" style="text-decoration: none; color:#29abe2;" title="Download Bill" class="edit_cbillperm"><i class="fa fa-download" aria-hidden="true"></i></a> | <a href="'.base_url().'user/print_custbillpdf/'.$billid.'" target="_blank"  style="text-decoration: none; color:#29abe2;" title="Print Bill" class="edit_cbillperm"><i class="fa fa-print" aria-hidden="true"></i></a> | <a href="javascript:void(0)" onclick="delete_custbillentry('.$billid.','.$isnext_billgenerated.')" class="delete_cbillperm"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>';
				
				$paymode = $billobj->payment_mode;
				$bill_addedon = date('d-m-Y', strtotime($billobj->added_on));
				
				$gen .= "<tr><td>".$i."</td><td>".$bill_no."</td><td>".$recno."</td><td>".$billobj->bill_details."</td><td ".$rowstyle.">$cocurrency ".$total_amount."</td>".$billcopy."<td>".$bill_addedon."</td>";
				
				$totalbillamt += $total_amount;
				$i++;
			}
		}else{
			$gen .= '<tr><td colspan="10" style="text-align:center">No Records Found.</td></td>';
		}
		
		$rcptgen = '';
		$recptQ = $this->db->query("SELECT id, bill_id, bill_number, receipt_number, receipt_amount, added_on,payment_mode  FROM sht_custom_receipt_list WHERE uid='".$cust_uuid."' ORDER BY id DESC");
		if($recptQ->num_rows() > 0){
			foreach($recptQ->result() as $rcptobj){
				$rcptid = $rcptobj->id;
				$billid = $rcptobj->bill_id;
				$bill_no = $rcptobj->bill_number;
				$recno = $rcptobj->receipt_number;
				$rcptamt = $rcptobj->receipt_amount;
				$rcptpaymode = $rcptobj->payment_mode;
				$rcptadded = date('d-m-Y', strtotime($rcptobj->added_on));
				$rcptdel = '<td><a href="javascript:void(0)" onclick="delete_custbillrcptentry('.$rcptid.','.$billid.')"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>';
				
				$rcptgen .= "<tr><td>&nbsp;</td><td>".$bill_no."</td><td>".$recno."</td><td>$cocurrency ".number_format($rcptamt,2)."</td><td>".$rcptadded."</td><td>".$rcptpaymode."</td>".$rcptdel."</tr>";
				$totalrcptamt += $rcptamt;
			}
		}else{
			$rcptgen .= '<tr><td colspan="7" style="text-align:center">No Records Found.</td></td>';
		}
		
		$totalbalamt = ($totalbillamt - $totalrcptamt);
		$data['custombill_list'] = $gen;
		$data['customrcpt_list'] = $rcptgen;
		$data['totalbillamt'] = $cocurrency.' '.number_format($totalbillamt,2);
		$data['totalrcptamt'] = $cocurrency.' '.number_format($totalrcptamt,2);
		$data['totalbalamt'] = $cocurrency.' '.number_format($totalbalamt,2);
		
		echo json_encode($data);
	}
	public function getcustombill_details(){
		$data = array();
		$billid = $this->input->post('billid');
		$custbillQ = $this->db->query("SELECT * FROM sht_subscriber_custom_billing WHERE id='".$billid."'");
		if($custbillQ->num_rows() > 0){
			$data = $custbillQ->row_array();
		}
		
		$companyid = $data['bill_companyid'];
		$companyQ = $this->db->query("SELECT tax FROM sht_isp_company_details WHERE isp_uid='".$isp_uid."' AND id='".$companyid."'");
		$comp_numrows = $companyQ->num_rows();
		if($comp_numrows > 0){
			$billtax = $companyQ->row()->tax;
		}else{
			$billtax = '18'; //18%
		}
		
		$gen = ''; $subtotal = 0; $invoice_exists = 0;
		$custominvoiceQ = $this->db->query("SELECT * FROM sht_custom_invoice_list WHERE bill_id='".$billid."'");
		if($custominvoiceQ->num_rows() > 0){
			$invoice_exists = 1;
			foreach($custominvoiceQ->result() as $custinvoiceobj){
				$invoiceid = $custinvoiceobj->id;
				$topic_name = $custinvoiceobj->topic_name;
				$topic_price = $custinvoiceobj->price;
				$topic_qty = $custinvoiceobj->quantity;
				$total_price = $custinvoiceobj->total_price;
				$subtotal += $total_price;
				$topicid = $custinvoiceobj->topic_id;
				
				$gen .= '<tr bgcolor="#f9f9f9" class="custinvoicelist" id="invid_'.$invoiceid.'"><td colspan="5" width="550px">'.$topic_name.'</td><td>'.$topic_price.'</td><td>'.$topic_qty.'</td><td>&nbsp;</td><td class="text-right">'.$total_price.'</td><td class="text-right"><a href="javascript:void(0)" onclick="delete_custbillinvoice(&apos;'.$billid.'&apos;,'.$invoiceid.')"><i class="fa fa-times close-fa" aria-hidden="true"></i></a></td></tr>';
			}
		}
		$data['datepicker'] = date('d-m-Y', strtotime($data['added_on']));
		$data['subtotal'] = $subtotal;
		$data['custinvoicelist'] = $gen;
		$data['invoice_exists'] = $invoice_exists;
		$data['billtax_amt'] =   round(($data['subtotal'] * $billtax) / 100 );
		$data['billtax'] = $billtax;
		
		echo json_encode($data);
	}
	public function delete_custombillentry(){
		$billid = $this->input->post('billid');
		$uuid = $this->input->post('uid');
		$bill_number = $this->getbillnumber($billid);
		$this->db->update('sht_subscriber_custom_billing', array('is_deleted' => '1'), array('id' => $billid));
		$this->db->delete('sht_custom_receipt_list', array('bill_id' => $billid));
		
		$this->notify_model->add_user_activitylogs($uuid, "CustomBill: $bill_number, Deleted");
		echo json_encode(true);
	}
	public function delete_custbillinvoice(){
		$data = array();
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$billid = $this->input->post('billid');
		$invoiceid = $this->input->post('invoiceid');
		$uuid = $this->input->post('uid');
		$this->db->delete('sht_custom_invoice_list', array('id' => $invoiceid));
		$companyid = $this->input->post('companyid');
		
		$companyQ = $this->db->query("SELECT tax FROM sht_isp_company_details WHERE isp_uid='".$isp_uid."' AND id='".$companyid."'");
		$comp_numrows = $companyQ->num_rows();
		if($comp_numrows > 0){
			$billtax = $companyQ->row()->tax;
		}else{
			$billtax = '18'; //18%
		}

		if($billid != ''){
			$bill_number = $this->getbillnumber($billid);
			$custominvoiceQ = $this->db->query("SELECT COALESCE( SUM(total_price), 0 ) as total_bill, GROUP_CONCAT(topic_name)  as bill_details FROM sht_custom_invoice_list WHERE bill_id='".$billid."'");
			if($custominvoiceQ->num_rows() > 0){
				$rowdata = $custominvoiceQ->row();
				$total_bill = $rowdata->total_bill;
				if($total_bill != '0.00'){
					$total_billpay = round( $total_bill + (($total_bill * $billtax) / 100) );
					
					$custombillArr = array(
						'bill_details' => $rowdata->bill_details,
						'total_amount' => $total_billpay
					);
					
					$this->db->update('sht_subscriber_custom_billing', $custombillArr, array('id' => $billid));	
				}else{
					$custombillArr = array(
						'bill_details' => '',
						'total_amount' => $total_bill
					);
					
					$this->db->update('sht_subscriber_custom_billing', $custombillArr, array('id' => $billid));	
				}
			}
			$this->notify_model->add_user_activitylogs($uuid, "Custom Bill Invoice: $bill_number, Deleted");
		}
		

		$custominvoiceQ = $this->db->query("SELECT SUM(total_price) as subtotal FROM sht_custom_invoice_list WHERE bill_id='".$billid."' AND isp_uid='".$isp_uid."' AND uid='".$uuid."' GROUP BY uid HAVING COUNT(total_price) = COUNT(*)");
		//$data['query'] = $this->db->last_query();
		if($custominvoiceQ->num_rows() > 0){
			$data['invoices_exists'] = 1;
			$data['subtotal'] = $custominvoiceQ->row()->subtotal;	
		}else{
			$data['invoices_exists'] = 0;
			$data['subtotal'] = 0;	
		}
		$data['billtax_amt'] =   round(($data['subtotal'] * $billtax) / 100 );
		$data['billtax'] = $billtax;
		echo json_encode($data);
	}
	public function deletebill_receiptentry(){
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		
		$billid = $this->input->post('billid');
		$receiptid = $this->input->post('receiptid');
		//$receiptamt = $this->input->post('receiptamt');
		
		$rcpt_billtype = '';
		$receiptQ = $this->db->query("SELECT tb1.wallet_id, tb1.bill_type, tb1.payment_mode, tb1.receipt_amount, tb2.wallet_id, tb2.subscriber_uuid, tb2.receipt_received, tb2.adjusted_amount, tb2.total_amount, tb2.subscriber_uuid FROM sht_subscriber_receipt_history as tb1 LEFT JOIN sht_subscriber_billing as tb2 ON(tb1.bill_id=tb2.id) WHERE tb1.id = $receiptid AND tb1.bill_id = $billid");
		if($receiptQ->num_rows() > 0){
			$rcptbilldata = $receiptQ->row();
			$rcpt_billtype = $rcptbilldata->bill_type;
			$receiptamt = $rcptbilldata->receipt_amount;
			$rcpt_walletid = $rcptbilldata->wallet_id;
			$rcpt_uuid = $rcptbilldata->subscriber_uuid;
			$rcpt_payment_mode = $rcptbilldata->payment_mode;
			$rcpt_received = $rcptbilldata->receipt_received;
			$bill_adjustedamt = $rcptbilldata->adjusted_amount;
			$bill_totalamt = $rcptbilldata->total_amount;
			$uuid = $rcptbilldata->subscriber_uuid;
			
			if(($rcpt_payment_mode != 'viawallet') && ($rcpt_received != '1')){
				$bill_adjustedamt = ($bill_adjustedamt - $receiptamt);
				$this->db->update("sht_subscriber_billing", array('adjusted_amount' => $bill_adjustedamt, 'receipt_received' => '0'), array('id' => $billid));
			}
			elseif(($rcpt_payment_mode != 'viawallet') && ($rcpt_received == '1')){
				$bill_adjustedamt = ($bill_totalamt - $receiptamt);
				$this->db->update("sht_subscriber_billing", array('adjusted_amount' => $bill_adjustedamt, 'receipt_received' => '0'), array('id' => $billid));
			}
			elseif(($rcpt_payment_mode == 'viawallet') && ($rcpt_received != '1')){
				$bill_adjustedamt = ($bill_adjustedamt - $receiptamt);
				$this->db->update("sht_subscriber_billing", array('adjusted_amount' => $bill_adjustedamt, 'receipt_received' => '0'), array('id' => $billid));
				$this->db->query("UPDATE sht_subscriber_wallet SET left_amount = (left_amount + $receiptamt) WHERE subscriber_uuid = '".$uuid."'");
			}
			elseif(($rcpt_payment_mode == 'viawallet') && ($rcpt_received == '1')){
				$bill_adjustedamt = ($bill_totalamt - $receiptamt);
				$this->db->update("sht_subscriber_billing", array('adjusted_amount' => $bill_adjustedamt, 'receipt_received' => '0'), array('id' => $billid));
				$this->db->query("UPDATE sht_subscriber_wallet SET left_amount = (left_amount + $receiptamt) WHERE subscriber_uuid = '".$uuid."'");
			}
			
		}
		
		
		$this->db->delete('sht_subscriber_receipt_history', array('id' => $receiptid, 'bill_id' => $billid));
		
		$userassoc_plandata = $this->userassoc_plandata($uuid);
		$user_credit_limit = $userassoc_plandata['user_credit_limit'];
		$user_wallet_balance = $this->userbalance_amount($uuid);
		$plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
		$account_activated_on = $userassoc_plandata['account_activated_on'];
		$total_advpay = $userassoc_plandata['total_advpay'];
		
		if($account_activated_on != '0000-00-00'){
			$actiondate = $this->affected_date_on_wallet($uuid);
			$expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
			$expiration_date = date('Y-m-d', strtotime($actiondate . " +".$expiration_acctdays." days"));
			$expiration_date = $expiration_date.' 00:00:00';
			$this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $uuid));
		}
		
		echo json_encode(true);
	}
	public function payinstall_securitybill(){
		$billid = $this->input->post('billid');
		$receipt_number = $this->input->post('update_receipt_number');
		$this->db->update('sht_subscriber_billing', array('receipt_received' => '1', 'receipt_number' => $receipt_number, 'adjusted_amount' => '0'), array('id' => $billid));
		echo json_encode(true);
	}
	public function show_wallethistory(){
		$gen = '<div class="table-responsive">
				<table class="table table-striped">';
		$uuid = $this->input->post('uuid');
		$walletQ = $this->db->query("SELECT total_amount, bill_added_on FROM sht_subscriber_billing WHERE subscriber_uuid='".$uuid."' AND status='1' AND is_deleted='0' AND bill_type='addtowallet' ORDER BY id DESC");
		if($walletQ->num_rows() > 0){
			$i = 1;
			foreach($walletQ->result() as $walletobj){
				$bill_added_on = date('d-m-Y H:i:s', strtotime($walletobj->bill_added_on));
				$gen .= '<tr><td>'.$i.'</td><td>'.$walletobj->total_amount.'</td><td>'.$bill_added_on.'</td></tr>';
				$i++;
			}
		}
		$gen .= '</table></div>';
		echo $gen;
	}
	public function delete_walletentry(){
		$billid = $this->input->post('billid');
		$uuid = $this->input->post('uid');
		
		$billwalletQ = $this->db->query("SELECT wallet_id, subscriber_uuid, receipt_number FROM sht_subscriber_billing WHERE id='".$billid."'");
		if($billwalletQ->num_rows() > 0){
			$rowdata = $billwalletQ->row();
			$wallet_id = $rowdata->wallet_id;
			$uuid = $rowdata->subscriber_uuid;
			$bill_number = $rowdata->receipt_number;
			
			$this->db->delete('sht_subscriber_billing', array('id' => $billid));
			$this->db->delete('sht_subscriber_wallet', array('id' => $wallet_id));
			
			$bill_number = $this->getbillnumber($billid);
			$this->notify_model->add_user_activitylogs($uuid, "Wallet Receipt: $bill_number, Deleted");
		}
		echo json_encode(true);
	}
	public function calculate_custbilltax(){
		$data = array();
		$session_data = $this->session->userdata('isp_session');
		$isp_uid = $session_data['isp_uid'];
		$billcompanyid = $this->input->post('companyid');
		$billid = $this->input->post('billid');
		$uuid = $this->input->post('cust_uuid');
		$companyQ = $this->db->query("SELECT tax FROM sht_isp_company_details WHERE isp_uid='".$isp_uid."' AND id='".$billcompanyid."'");
		$comp_numrows = $companyQ->num_rows();
		if($comp_numrows > 0){
			$billtax = $companyQ->row()->tax;
		}else{
			$billtax = '18'; //18%
		}
		
		$custominvoiceQ = $this->db->query("SELECT SUM(total_price) as subtotal FROM sht_custom_invoice_list WHERE bill_id='".$billid."' AND isp_uid='".$isp_uid."' AND uid='".$uuid."' GROUP BY uid HAVING COUNT(total_price) = COUNT(*)");
		if($custominvoiceQ->num_rows() > 0){
			$data['subtotal'] = $custominvoiceQ->row()->subtotal;	
		}else{
			$data['subtotal'] = 0;	
		}
		
		$data['billtax_amt'] =  round(($data['subtotal'] * $billtax) / 100);
		$data['billtax'] = $billtax;
		
		echo json_encode($data);
	}
	public function addcustombillrcpt(){
		$billid = $this->input->post('rcptcustom_billid');
		$billno = $this->input->post('rcptcustom_billno');
		$uuid = $this->input->post('subscriber_uuid');
		$rcpt_addedon = $this->input->post('rcptcustom_billdatetime');
		$rcpt_number = $this->input->post('rcptcustom_receipt_number');
		
		$receiptArr = array();
		$update_payment_mode = $this->input->post('rcptcustom_payment_mode');
		if(($update_payment_mode == 'cheque') || ($update_payment_mode == 'dd')){
			$receiptArr['account_number'] = $this->input->post('bill_bankaccount_number');
			$receiptArr['branch_name'] = $this->input->post('bill_bankname');
			$receiptArr['branch_address'] = $this->input->post('bill_bankaddress');
			$receiptArr['bankslip_issued_date'] = $this->input->post('bill_bankissued_date');
			$receiptArr['cheque_dd_paytm_number'] = $this->input->post('rcptcustom_cheque_dd_paytm');
		}
		if(($update_payment_mode == 'viapaytm') || ($update_payment_mode == 'netbanking')){
			$receiptArr['cheque_dd_paytm_number'] = $this->input->post('rcptcustom_cheque_dd_paytm');
			
		}
		$receiptArr['payment_mode'] = $update_payment_mode;
		$receiptArr['bill_id'] = $billid;
		$receiptArr['bill_number'] = $billno;
		$receiptArr['receipt_number'] = $rcpt_number;
		$receiptArr['added_on'] = $rcpt_addedon;
		$receiptArr['receipt_amount'] = $this->input->post('rcptcustom_amount');
		$receiptArr['uid'] = $uuid;
		
		$this->db->insert('sht_custom_receipt_list', $receiptArr);
		
		$totalbillamt = 0; $added_rcptamt = 0;
		$custbillQ = $this->db->query("SELECT total_amount FROM sht_subscriber_custom_billing WHERE id='".$billid."'");
		if($custbillQ->num_rows() > 0){
			$totalbillamt = $custbillQ->row()->total_amount;
		}
		
		$recptQ = $this->db->query("SELECT COALESCE(SUM(receipt_amount),0) as receipt_amount FROM sht_custom_receipt_list WHERE bill_id='".$billid."'");
		if($recptQ->num_rows() > 0){
			$added_rcptamt = $recptQ->row()->receipt_amount;
		}
		
		if($totalbillamt == $added_rcptamt){
			$this->db->update('sht_subscriber_custom_billing', array('receipt_received' => '1', 'bill_paid_on' => date('Y-m-d H:i:s')), array('id' => $billid));
		}
		
		echo json_encode(true);
	}
	public function delete_custbillrcptentry(){
		$rcptid = $this->input->post('rcptid');
		$billid = $this->input->post('billid');
		$this->db->delete('sht_custom_receipt_list', array('id' => $rcptid));
		
		$totalbillamt = 0; $added_rcptamt = 0;
		$custbillQ = $this->db->query("SELECT total_amount FROM sht_subscriber_custom_billing WHERE id='".$billid."'");
		if($custbillQ->num_rows() > 0){
			$totalbillamt = $custbillQ->row()->total_amount;
		}
		$recptQ = $this->db->query("SELECT COALESCE(SUM(receipt_amount),0) as receipt_amount FROM sht_custom_receipt_list WHERE bill_id='".$billid."'");
		if($recptQ->num_rows() > 0){
			$added_rcptamt = $recptQ->row()->receipt_amount;
		}
		
		if($totalbillamt != $added_rcptamt){
			$this->db->update('sht_subscriber_custom_billing', array('receipt_received' => '0'), array('id' => $billid));
		}
		
		echo json_encode(true);
	}

	public function manage_instsecuritybills(){
		$data = array();
		$billid = $this->input->post('billid');
		$billtype = $this->input->post('billtype');
		
		$query = $this->db->query("SELECT bill_number, receipt_number, subscriber_uuid, payment_mode, actual_amount, discounttype, discount, total_amount, bill_added_on, receipt_received, bill_comments, bill_remarks,installsecurity_amount_received , billing_with_tax FROM sht_subscriber_billing WHERE id = '".$billid."'");
		$numrows = $query->num_rows();
		if($numrows > 0){
			$rowdata = $query->row();
			$data['billid'] = $billid;
			$data['bill_number'] = $rowdata->bill_number;
			$data['receipt_number'] = $rowdata->receipt_number;
			$data['uuid'] = $rowdata->subscriber_uuid;
			$data['payment_mode'] = $rowdata->payment_mode;
			$data['actual_amount'] = $rowdata->actual_amount;
			$data['discounttype'] = $rowdata->discounttype;
			$data['discount'] = $rowdata->discount;
			$data['total_amount'] = $rowdata->total_amount;
			$data['datetimeformat'] = date('d-m-Y H:i:s', strtotime($rowdata->bill_added_on));
			$data['bill_added_on'] = $rowdata->bill_added_on;
			$data['bill_comments'] = $rowdata->bill_comments;
			$data['bill_remarks'] = $rowdata->bill_remarks;
			$data['receipt_received'] = $rowdata->receipt_received;
			$data['billpaid'] = $rowdata->installsecurity_amount_received;
			$data['billing_with_tax'] = $rowdata->billing_with_tax;
			
		}
		echo json_encode($data);
	}
	public function viewadded_topiclist(){
		$data = array(); $tgen = ''; 
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$isptopicsQ = $this->db->query("SELECT id,topic_name FROM sht_isp_bill_topics WHERE isp_uid='".$isp_uid."'");
		$topic_numrows = $isptopicsQ->num_rows();
		if($topic_numrows > 0){
			foreach($isptopicsQ->result() as $topicobj){
				$topic_id = $topicobj->id;
				$topic_name = $topicobj->topic_name;
				$tgen .= '<li style="list-style:none"><label>'.$topic_name.'</label>&nbsp;<span style="float:right"  onclick="delete_isptopics(&apos;'.$topic_id.'&apos;)"><i class="fa fa-times close-fa" aria-hidden="true"></i></span></li>';
			}
			$data['topic_list'] = $tgen;
		}
		
		echo json_encode($data);
	}
	public function delete_isptopics(){
		$data = array(); $tgen = ''; 
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$topicid = $this->input->post('topic_id');
		$isptopicsQ = $this->db->query("SELECT id,topic_name FROM sht_isp_bill_topics WHERE isp_uid='".$isp_uid."' AND id='".$topicid."'");
		$topic_numrows = $isptopicsQ->num_rows();
		if($topic_numrows > 0){
			$this->db->delete("sht_isp_bill_topics", array('isp_uid' => $isp_uid, 'id' => $topicid));
		}
		
		$isptopicsQ = $this->db->query("SELECT id,topic_name FROM sht_isp_bill_topics WHERE isp_uid='".$isp_uid."'");
		$topic_numrows = $isptopicsQ->num_rows();
		if($topic_numrows > 0){
			foreach($isptopicsQ->result() as $topicobj){
				$topic_id = $topicobj->id;
				$topic_name = $topicobj->topic_name;
				$tgen .= '<option value="'.$topic_id.'">'.$topic_name.'</option>';
			}
			$data['topic_list'] = $tgen;
		}
		
		echo json_encode($data);
	}
	
/*-------------------------------------------------------------------------------------------------------*/
	
	public function usage_locality($locality=''){
		$gen = '';
		$query = $this->db->get_where('sht_usage_locality',array('status' => '1', 'is_deleted' => '0'));
		if($query->num_rows() > 0){
			foreach($query->result() as $qobj){
				$sel = '';
				if($locality == $qobj->id){
					$sel = 'selected="selected"';
				}
				$gen .= '<option value="'.$qobj->id.'" '.$sel.'>'.$qobj->locality_name.'</option>';
			}
		}
		echo $gen;
	}
	
	public function locality_name($id){
		$locality = '';
		$query = $this->db->get_where('sht_usage_locality',array('id' => $id, 'status' => '1', 'is_deleted' => '0'));
		if($query->num_rows() > 0){
			$locality = $query->row()->locality_name;
		}
		return $locality;
	}
	
	public function delete_subscriber(){
		$userid = $this->input->post('userid');
		$this->db->update('sht_subscriber', array('is_deleted' => '1'), array('id' => $userid));
		echo json_encode(1);
	}
	

/*----------------- SETTING DETAILS HERE -----------------------------------------------------------------*/
	
	public function getuser_plan_speed($uuid){
		$data = array();
		$baseplanQ = $this->db->query("SELECT baseplanid FROM sht_users WHERE uid='".$uuid."'");
		if($baseplanQ->num_rows() > 0){
			$baseplanid = $baseplanQ->row()->baseplanid;
			$plandetQ = $this->db->query("SELECT uprate, downrate FROM sht_services WHERE srvid='".$baseplanid."'");
			$rawdata = $plandetQ->row();
			$data['uprate'] = $rawdata->uprate;
			$data['downrate'] = $rawdata->downrate;
		}
		return $data;
	}
	public function getcustomer_password($uuid){
		$orig_pwd = '';
		$custpwdQ = $this->db->query("SELECT orig_pwd FROM sht_users WHERE uid='".$uuid."'");
		if($custpwdQ->num_rows() > 0){
			$rawdata = $custpwdQ->row();
			$orig_pwd = $rawdata->orig_pwd;
		}
		return $orig_pwd;
	}

	public function update_syssetting(){
		//print_r($_POST);
		$this->load->library('routerlib');
		$session_data = $this->session->userdata('isp_session');;
		$isp_uid = $session_data['isp_uid'];
		$ipaddrtype = $this->input->post('ipaddrtype');
		$ipaddress = $this->input->post('ipaddress');
		if($ipaddress == ''){
			$ipaddress = $this->input->post('ipaddress_pool');
		}
		$maclockedstatus = $this->input->post('bindmaclock');
		$uuid = $this->input->post('subscriber_uuid');
		$logintype = $this->input->post('logintype');
		if($logintype == 'hotspot'){
			$hotspotMacID = $this->input->post('hotspotMacID');
		}else{
			$hotspotMacID = '';
		}
		
		$prevlogtype = '';
		$previouslogintype = $this->db->query("SELECT logintype FROM sht_users WHERE uid='".$uuid."'");
		if($previouslogintype->num_rows() > 0){
			$prevlogtype = $previouslogintype->row()->logintype;
		}
		
		if($logintype == 'ill_ipuser'){
			$planspeed = $this->getuser_plan_speed($uuid);
			$uprate = $planspeed['uprate'];
			$downrate = $planspeed['downrate'];
			
			$nasid = $this->input->post('nas_illoptions');
			$ill_iptype = $this->input->post('ill_iptype');
			$ill_iprange = $this->input->post('nas_iprange[]');
			if(count($ill_iprange) > 0){
				$db_iprange = "'" . implode("','" , $ill_iprange). "'";
				$router_iprange = implode(',' , $ill_iprange);
			}else{
				$db_iprange = "'". $ill_iprange. "'";
				$router_iprange = $ill_iprange;
			}
			
			$router_id = '';
			$router_user = '';
			$router_password = '';
			$nasdetailsQ = $this->db->query("SELECT tb1.* FROM nas_ill as tb1 WHERE tb1.nasid='".$nasid."' AND tb1.ip_type='".$ill_iptype."' LIMIT 1");
			if($nasdetailsQ->num_rows() > 0){
				$nasobj = $nasdetailsQ->row();
				$gateway_ip = $nasobj->gateway_ip_address;
				$netmaskip = $nasobj->netmask_ip;
				$netmask = $nasobj->netmask;
				$tableid = $nasobj->id;
				$router_port = $nasobj->port;
				$interface = $nasobj->interface_name;
				$router_port = (isset($router_port) && ($router_port != '')) ? $router_port : '8728';
				
				$router_id = $nasobj->nasname;
				$router_user = $nasobj->username;
				$router_password = $nasobj->password;
				
				if($router_port != ''){
				    $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
				}else{
				    $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password");	
				}
				
				if($router_conn !== null){
					$chkexists = 0;
					$already_ILLUser = $this->db->query("SELECT tb1.* FROM nas_ill_iprange as tb1 WHERE tb1.ip_address IN($db_iprange) AND uid='".$uuid."' AND is_assigned='1'");
					$chkexists = $already_ILLUser->num_rows();
					
					foreach($ill_iprange as $ipassigned){
						$router_conn->add("/ip/firewall/address-list",array("comment"=>$uuid."_ILL", "list" => "ALLOW" ,"address" => $ipassigned));
						$router_conn->add("/ip/firewall/address-list",array("comment"=>$uuid."_ILL", "list" => $uuid."_ILL" ,"address" => $ipassigned));
					}
					
					if($chkexists == 0){
						$router_conn->add("/ip/firewall/filter",array("comment"=>$uuid."_ILL", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>$uuid."_ILL", "action" => "accept"));
						$check_queue = $router_conn->getall("/queue/simple");
						if(count($check_queue) > 0){
							$router_conn->add("/queue/simple",array("name"=>$uuid."_ILL", "target"=>"$router_iprange", "disabled"=>"no", "max-limit" => $uprate."/".$downrate, "place-before" => 0));
						}else{
							$router_conn->add("/queue/simple",array("name"=>$uuid."_ILL", "target"=>"$router_iprange", "disabled"=>"no", "max-limit" => $uprate."/".$downrate));
						}
					}else{
						$router_conn->set("/queue/simple",array(".id"=>$uuid."_ILL", "target"=>"$router_iprange"));
					}
				}
			}
			
			$this->db->query("UPDATE nas_ill_iprange SET is_assigned='1', uid='".$uuid."' WHERE ip_address IN(".$db_iprange.") AND isp_uid='".$isp_uid."'");
			$this->db->query("UPDATE nas_ill_iprange SET is_assigned='0', uid='' WHERE ip_address NOT IN (".$db_iprange.") AND isp_uid='".$isp_uid."' AND uid='".$uuid."'");
			
			$getilldetails = $this->db->query("SELECT GROUP_CONCAT(ip_assigned) as ip_assigned FROM sht_users_ill_ips_assigned WHERE uid='".$uuid."' AND isp_uid='".$isp_uid."' AND status='1'");
			if($getilldetails->num_rows() > 0){
				$previp_assigned = $getilldetails->row()->ip_assigned;
				$previp_assigned = explode(',' , $previp_assigned);
			}
			
			$deactip_array = array_diff($previp_assigned , $ill_iprange);
			$insertip_array = array_diff($ill_iprange, $previp_assigned);
			
			foreach($deactip_array as $dip){
				$this->db->update('sht_users_ill_ips_assigned', array('status' => '0'), array('isp_uid' => $isp_uid, 'uid' => $uuid, 'ip_assigned' => $dip));
				$addrlist = $router_conn->getall("/ip/firewall/address-list");
				foreach($addrlist as $addrlistobj){
					if($addrlistobj['address'] == $dip){
						$router_conn->remove("/ip/firewall/address-list",array(".id" => $addrlistobj['.id']));
					}
				}
			}
			
			foreach($insertip_array as $ipassigned){
				$this->db->insert('sht_users_ill_ips_assigned', array('isp_uid' => $isp_uid, 'uid' => $uuid, 'nasname' => $router_id, 'username' => $router_user, 'password' => $router_password, 'iprange_type' => $ill_iptype, 'ip_assigned' => $ipassigned, 'status' => '1', 'ip_assigned_date' => date('Y-m-d H:i:s')));
			}
			$data['ipexists'] = '0';
			$data['ipaddrtype'] = $ipaddrtype;
			$data['ipaddress'] = $ipaddress;
			$data['maclockedstatus'] = $maclockedstatus;
			$data['logintype'] = $logintype;
			$data['hotspotMac'] = $hotspotMacID;
			
			$this->db->update('sht_users', array('ipaddr_type' => '1', 'staticipcpe' => '', 'usermacauth' => $maclockedstatus, 'maclockedstatus' => $maclockedstatus, 'logintype' => $logintype, 'hotspotMac' => $hotspotMacID ), array('uid' => $uuid));
			
			$this->db->delete('radcheck', array('username' => $uuid));
		}
		else{
			$radchk_pwd = $this->getcustomer_password($uuid);
			$radchkQ = $this->db->get_where('radcheck', array('username' => $uuid));
			if($radchkQ->num_rows() == 0){
				$this->db->query("INSERT INTO radcheck SET username='".$uuid."', attribute = 'Cleartext-Password', op = ':=', value = '".$radchk_pwd."'");
				$this->db->query("INSERT INTO radcheck SET username='".$uuid."', attribute = 'Simultaneous-Use', op = ':=', value = '1'");
				
			}
			
			$previouslogintype = $this->db->query("SELECT logintype FROM sht_users WHERE uid='".$uuid."'");
			if($previouslogintype->num_rows() > 0){
				$prevlogintype = $previouslogintype->row()->logintype;
				if($prevlogintype == 'ill_ipuser'){
					$chk_illassigned = $this->db->query("SELECT * FROM sht_users_ill_ips_assigned WHERE  isp_uid = '".$isp_uid."' AND uid='".$uuid."' AND status='1' LIMIT 1");
					if($chk_illassigned->num_rows() > 0){
						$nasobj = $chk_illassigned->row();
						$router_port = '8728';
						$router_id = $nasobj->nasname;
						$router_user = $nasobj->username;
						$router_password = $nasobj->password;
						
						if($router_port != ''){
						    $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
						}else{
						    $router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password");	
						}
						
						if($router_conn !== null){
							$addrlist = $router_conn->getall("/ip/firewall/address-list");
							foreach($addrlist as $addrlistobj){
							    if(isset($addrlistobj['comment']) && ($addrlistobj['comment'] == $uuid."_ILL")){
								$router_conn->remove("/ip/firewall/address-list", $uuid."_ILL");
							    }
							}
							//$router_conn->remove("/ip/firewall/address-list",array("comment"=>$uuid."_ILL"));
							$router_conn->remove("/ip/firewall/filter",array("comment"=>$uuid."_ILL"));
						}
					}
					$this->db->query("UPDATE nas_ill_iprange SET is_assigned='0', uid='' WHERE isp_uid='".$isp_uid."' AND uid='".$uuid."'");
					$this->db->update('sht_users_ill_ips_assigned', array('status' => '0', 'ip_assigned_date' => date('Y-m-d H:i:s')), array('isp_uid' => $isp_uid, 'uid' => $uuid));
				}
				
			}
			
			/****************************************************************************************************/
			
			$radchk_clearid = $this->input->post('radchk_clearid');
			$radchk_simuseid = $this->input->post('radchk_simuseid');
			
			if($ipaddrtype == '1'){
				$ipaddress = '';
			}
			if($ipaddress != ''){
				$ipexistsQ = $this->db->query("SELECT staticipcpe FROM sht_users WHERE staticipcpe='".$ipaddress."' AND uid != '".$uuid."'");
				if($ipexistsQ->num_rows() > 0){
					$data['ipexists'] = '1';
				}else{
					if($logintype == 'hotspot'){
						$loginQ = $this->db->query("SELECT username FROM radcheck WHERE username='".$hotspotMacID."' AND ((id != '".$radchk_clearid."') OR (id != '".$radchk_simuseid."')");
						if($loginQ->num_rows() > 0){
							$data['loginMacId_exists'] = '1';
						}else{
							$this->db->insert('sht_user_hotspot_assoc', array('uid' => $uuid, 'hotspotMac' => $hotspotMacID, 'added_on' => date('Y-m-d H:i:s')));
							$this->db->query("UPDATE radcheck SET username='".$hotspotMacID."', value='' WHERE id='".$radchk_clearid."' AND attribute='Cleartext-Password'");
							$this->db->query("UPDATE radcheck SET username='".$hotspotMacID."', value='1' WHERE id='".$radchk_simuseid."' AND attribute='Simultaneous-Use'");
						}
					}elseif($logintype == 'pppoe'){
						$pppoeQ = $this->db->query("SELECT id FROM radcheck WHERE username='".$uuid."' AND attribute='Cleartext-Password' AND value=''");
						if($pppoeQ->num_rows() > 0){
							$radchk_pwd = random_string('alnum', 7);
							$this->db->query("UPDATE radcheck SET username='".$uuid."', value='".$radchk_pwd."' WHERE id='".$radchk_clearid."' AND attribute='Cleartext-Password'");
							$this->db->query("UPDATE radcheck SET username='".$uuid."', value='1' WHERE id='".$radchk_simuseid."' AND attribute='Simultaneous-Use'");
						}
					}
					
					if($ipaddrtype == '2'){
						$this->db->update('sht_ippool', array('uid' => $uuid, 'is_assigned' => '1'), array('isp_uid' => $isp_uid, 'ip' => "$ipaddress"));
					}
					$this->db->update('sht_users', array('ipaddr_type' => $ipaddrtype, 'staticipcpe' => $ipaddress, 'usermacauth' => $maclockedstatus, 'maclockedstatus' => $maclockedstatus, 'logintype' => $logintype, 'hotspotMac' => $hotspotMacID ), array('uid' => $uuid));
					$data['ipexists'] = '0';
					$data['ipaddrtype'] = $ipaddrtype;
					$data['ipaddress'] = $ipaddress;
					$data['maclockedstatus'] = $maclockedstatus;
					$data['logintype'] = $logintype;
					$data['hotspotMac'] = $hotspotMacID;
				}
			}else{
				if($logintype == 'hotspot'){
					$loginQ = $this->db->query("SELECT username FROM radcheck WHERE username='".$hotspotMacID."'");
					if($loginQ->num_rows() > 0){
						$data['loginMacId_exists'] = '1';
					}else{
						$this->db->insert('sht_user_hotspot_assoc', array('uid' => $uuid, 'hotspotMac' => $hotspotMacID, 'added_on' => date('Y-m-d H:i:s')));
						$this->db->query("UPDATE radcheck SET username='".$hotspotMacID."', value='' WHERE id='".$radchk_clearid."' AND attribute='Cleartext-Password'");
						$this->db->query("UPDATE radcheck SET username='".$hotspotMacID."', value='1' WHERE id='".$radchk_simuseid."' AND attribute='Simultaneous-Use'");
					}
				}elseif($logintype == 'pppoe'){
					$pppoeQ = $this->db->query("SELECT id FROM radcheck WHERE username='".$uuid."' AND attribute='Cleartext-Password' AND value=''");
					if($pppoeQ->num_rows() > 0){
						$radchk_pwd = random_string('alnum', 7);
						$this->db->query("UPDATE radcheck SET username='".$uuid."', value='".$radchk_pwd."' WHERE id='".$radchk_clearid."' AND attribute='Cleartext-Password'");
						$this->db->query("UPDATE radcheck SET username='".$uuid."', value='1' WHERE id='".$radchk_simuseid."' AND attribute='Simultaneous-Use'");
					}
				}
				$this->db->update('sht_users', array('ipaddr_type' => $ipaddrtype, 'staticipcpe' => $ipaddress, 'usermacauth' => $maclockedstatus, 'maclockedstatus' => $maclockedstatus, 'logintype' => $logintype, 'hotspotMac' => $hotspotMacID ), array('uid' => $uuid));
				$data['ipexists'] = '0';
				$data['ipaddrtype'] = $ipaddrtype;
				$data['ipaddress'] = $ipaddress;
				$data['maclockedstatus'] = $maclockedstatus;
				$data['logintype'] = $logintype;
				$data['hotspotMac'] = $hotspotMacID;
	
			}
		}
		
		$changes = '';
		if($prevlogtype != $logintype){
			$changes = ' Change '.$prevlogtype.' TO '.$logintype;
		}
		
		if($logintype == 'ill_ipuser'){
			$changes .= ' , Assigned: '.$router_iprange;
		}
		
		$this->notify_model->add_user_activitylogs($uuid, 'Update Settings :'.$changes);

		echo json_encode($data);
	}
	public function getuser_currentmac($uuid){
		$usermac = '';
		$radactQ = $this->db->query("SELECT callingstationid FROM radacct WHERE username='".$uuid."' ORDER BY radacctid DESC LIMIT 1");
		if($radactQ->num_rows() > 0){
			$rowdata = $radactQ->row();
			$usermac = $rowdata->callingstationid;
		}
		return $usermac;
	}
	public function view_syssetting(){
		$data = array();
		$uuid = $this->input->post('uuid');
		$session_data = $this->session->userdata('isp_session');;
		$isp_uid = $session_data['isp_uid'];
		$sysQ = $this->db->query("SELECT mac, ipaddr_type, staticipcpe, maclockedstatus,logintype,hotspotMac FROM sht_users WHERE uid='".$uuid."'");
		if($sysQ->num_rows() > 0){
			$rowdata = $sysQ->row();
			$data['ipaddrtype'] = $rowdata->ipaddr_type;
			$data['ipaddress'] = $rowdata->staticipcpe;
			$data['maclockedstatus'] = $rowdata->maclockedstatus;
			$data['mac'] = $this->getuser_currentmac($uuid);
			
			$logintype = $rowdata->logintype;
			$hotspotMacID = $rowdata->hotspotMac;
			if($logintype == 'hotspot'){
				$radclearQ = $this->db->query("SELECT id FROM radcheck WHERE username='".$hotspotMacID."' AND attribute='Cleartext-Password'");
				$radsimuseQ = $this->db->query("SELECT id FROM radcheck WHERE username='".$hotspotMacID."' AND attribute='Simultaneous-Use'");
				if($radclearQ->num_rows() > 0){
					$data['radchk_clearid'] = $radclearQ->row()->id;
					$data['radchk_simuseid'] = $radsimuseQ->row()->id;
				}
			}elseif($logintype == 'pppoe'){
				$radclearQ = $this->db->query("SELECT id FROM radcheck WHERE username='".$uuid."' AND attribute='Cleartext-Password'");
				$radsimuseQ = $this->db->query("SELECT id FROM radcheck WHERE username='".$uuid."' AND attribute='Simultaneous-Use'");
				if($radclearQ->num_rows() > 0){
					$data['radchk_clearid'] = $radclearQ->row()->id;
					$data['radchk_simuseid'] = $radsimuseQ->row()->id;
				}
			}elseif($logintype == 'ill_ipuser'){
				$getilldetails = $this->db->query("SELECT nasname, iprange_type, GROUP_CONCAT(ip_assigned) as ip_assigned FROM sht_users_ill_ips_assigned WHERE uid='".$uuid."' AND isp_uid='".$isp_uid."' AND status='1'");
				if($getilldetails->num_rows() > 0){
					$rawdata = $getilldetails->row();
					$data['nasname'] = $rawdata->nasname;
					$data['iprange_type'] = $rawdata->iprange_type;
					$data['ip_assigned'] = $rawdata->ip_assigned;
				}
			}
			
			$data['logintype'] = $logintype;
			$data['hotspotMac'] = $hotspotMacID;
		}
		$ippoolQ = $this->db->query("SELECT ip FROM sht_ippool WHERE isp_uid='".$isp_uid."' AND is_assigned='0' AND status='1'");
		$data['total_poolips'] = $ippoolQ->num_rows();
		
		echo json_encode($data);
	}
	
	public function ippooladdr_list(){
		$session_data = $this->session->userdata('isp_session');;
		$isp_uid = $session_data['isp_uid'];
		$gen = '<option value="">Select IP Pool</option>';
		$uuid = $this->input->post('uuid');
		$ippoolQ = $this->db->query("SELECT ip FROM sht_ippool WHERE isp_uid='".$isp_uid."' AND is_assigned='0' AND status='1'");
		if($ippoolQ->num_rows() > 0){
			foreach($ippoolQ->result() as $ipobj){
				$gen .= '<option value="'.$ipobj->ip.'">'.$ipobj->ip.'</option>';
			}
		}
		return $gen;
	}
	
	public function resetUserMac(){
		$uuid = $this->input->post('uuid');
		$this->db->update('sht_users', array('mac' => '0'), array('uid' => $uuid));
		
		$this->notify_model->add_user_activitylogs($uuid, 'Reset UserMacID');
		echo json_encode(true);
	}
	
	
	public function datausuage_logs(){
		$uuid = $this->input->post('uuid');
		//$uuid = '10000016';
		$date_range = $this->input->post('date_range');
		//$date_range = '26.08.2017 - 26.08.2017';
		$date_filter_from  = '';
		$date_filter_to = '';
		$date_range_explode = explode('-',$date_range);
		$date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
		$date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
		$total_upload = 0;
		$total_download = 0;
		$data = array();
		$query = $this->db->query("SELECT *, DAY( start_time ) as date, MONTH( start_time ) as month,  TIME( start_time ) as starttime , TIME( end_time ) as endtime  FROM sht_user_daily_data_usage WHERE uid='".$uuid."' AND DATE(start_time) BETWEEN '$date_filter_from' AND '$date_filter_to'");
		//echo $this->db->last_query();die;
		if($query->num_rows() > 0){
		$i = 0;
		foreach($query->result() as $uobj){
			$total_upload = $total_upload + $uobj->upload;
			$total_download = $total_download + $uobj->download;
			$starttime = $uobj->starttime;
			$endtime = $uobj->endtime;
			$date = $uobj->date;
			$dataconsumed = round( ((($uobj->upload) + ($uobj->download)) / (1024*1024)), 2 );
			if(($starttime == '00:00:00') && ($endtime == '11:59:59')){
				$data['firsthalfdata'][] = array('y' => $dataconsumed, 'label' => $date, 'toolTipContent' => 'Data Used: '.$dataconsumed. "MB");
			}elseif(($starttime == '12:00:00') && ($endtime == '23:59:59')){
				$data['secondhalfdata'][] = array('y' => $dataconsumed, 'label' => $date, 'toolTipContent' => 'Data Used: '.$dataconsumed. "MB");
			}
			$i++;
		}
		//$data = array_values($data);
		}
		if($total_upload > 0){
			$total_upload = round($total_upload/(1024*1024*1024),2);
		}else{
			$total_upload = "0";
		}
		$data['total_upload'] = $total_upload. " GB";
		if($total_download > 0){
			$total_download = round($total_download/(1024*1024*1024),2);
		}else{
			$total_download = "0";
		}
		$data['total_download'] = $total_download. " GB";
		$data['total_used'] = ($total_upload+$total_download). " GB";
		
		$sessiondata = $this->session->userdata('isp_session');
		$superadmin = $sessiondata['super_admin'];
		$data['superadmin_perm'] = $superadmin;
		if($superadmin != '1'){
			$isperm = $this->is_permission('LOGS','EDIT');
			if($isperm == false){
			  if($this->is_permission('LOGS','RO')){
				$data["isreadonly_permission"] = '1';
			  }else{
				$data["isreadonly_permission"] = '0';
			  }
			}
		}
		echo json_encode($data);
	}

	public function data_logs(){
		$data = array();
		$uuid = $this->input->post('uuid');
		//$uuid = '10000148';
		$date_filter  = $this->input->post('date_range');
		$date_filter_from = '';
		$date_filter_to = '';
		if($date_filter == 'today'){
		    $date_filter_from = date("Y-m-d");
		    $date_filter_to = date("Y-m-d");
		    $query = $this->dbgraph->query("select DATE_FORMAT(time, '%H:%i') as time,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY UNIX_TIMESTAMP(time) DIV 300");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->time;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		elseif($date_filter == 'yesterday'){
		    $date_filter_from = date('Y-m-d', strtotime('-1 days'));
		    $date_filter_to = date('Y-m-d', strtotime('-1 days'));
		    $query = $this->dbgraph->query("select DATE_FORMAT(time, '%H:%i') as time,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY UNIX_TIMESTAMP(time) DIV 300");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->time;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		elseif($date_filter == 'last_week'){
		    $date_filter_from = date('Y-m-d', strtotime('-6 days'));
		    $date_filter_to = date('Y-m-d');
		    $query = $this->dbgraph->query("select DATE_FORMAT(date, '%a') as dates,DATE_FORMAT(time, '%H:%i') as time,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY UNIX_TIMESTAMP(time) DIV 1800");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->dates." ".$row->time;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		elseif($date_filter == 'last_month'){
		    $date_filter_from = date('Y-m-d', strtotime('-30 days'));
		    $date_filter_to = date('Y-m-d');
		    $query = $this->dbgraph->query("select DATE_FORMAT(date, '%a %d %b') as dates,DATE_FORMAT(time, '%H:%i') as time,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY UNIX_TIMESTAMP(time) DIV 7200");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->dates." ".$row->time;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		elseif($date_filter == 'last_year'){
		    $date_filter_from = date('Y-m-d', strtotime('-365 days'));
		    $date_filter_to = date('Y-m-d');
		    $query = $this->dbgraph->query("select DATE_FORMAT(date, '%a %d %b') as dates,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY DATE(date)");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->dates;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		
		//echo "<pre>";print_r($data)
		echo json_encode($data);
		
	}
	
	public function check_sysdatabase(){
		$data = array();
		$isperm = $this->is_permission('SYSLOG','EDIT');
		if($isperm == false){
		  if($this->is_permission('SYSLOG','RO')){
			$data["isreadonly_permission"] = '1';
		  }else{
			$data["isreadonly_permission"] = '0';
		  }
		}
		
		ini_set('display_errors', 'Off');    
		//  Load the database config file.
		if(file_exists($file_path = APPPATH.'config/database.php')){
			include($file_path);
		}
		$config = $db['db2_syslog'];
		if( $config['dbdriver'] === 'mysqli' ){
			$mysqli = new mysqli( $config['hostname'] , $config['username'] , $config['password'] , $config['database'] );
			if( !$mysqli->connect_error ){
				$data['connected'] = 1;
			}else{
				$data['connected'] =  0;
			}
		}else{
			$data['connected'] =  0;
		}
		
		echo json_encode($data);
	}
	
	public function today_uniquesession(){
		$costamt = 0; $c = 0;
		//$radacttQ = $this->db->query("SELECT distinct username FROM `radacct` WHERE YEAR(`acctstarttime`) = YEAR(CURRENT_DATE()) AND MONTH(`acctstarttime`) = MONTH(CURRENT_DATE()) AND DATE(`acctstarttime`) = DATE(CURRENT_DATE()) AND username REGEXP '^[0-9]+$'");
		$radacttQ = $this->db->query("SELECT DISTINCT username FROM radacct WHERE date(`acctstarttime`) = date( NOW( ) - INTERVAL 1 DAY ) AND username REGEXP '^[0-9]+$'");
		if($radacttQ->num_rows() > 0){
			foreach($radacttQ->result() as $radobj){
				$uid = $radobj->username;
				$isp_uid = $this->getisp_uid($uid);
				if($isp_uid != 0){
					$checkUserQ = $this->db->query("SELECT uid FROM sht_isp_users_track WHERE uid='".$uid."' AND  YEAR(`added_on`) = YEAR(CURRENT_DATE()) AND MONTH(`added_on`) = MONTH(CURRENT_DATE())");
					if($checkUserQ->num_rows() == 0){
						$this->db->insert('sht_isp_users_track', array('isp_uid' => $isp_uid, 'uid' => $uid, 'added_on' => date('Y-m-d H:i:s')));
						$costamt += 10;
						$c++;
						
					}
				}
			}
			if(($c != 0) && ($costamt != 0)){
				$this->db->insert('sht_isp_passbook', array('isp_uid' => $isp_uid, 'total_active_users' => $c, 'cost' => $costamt, 'added_on' => date('Y-m-d H:i:s')));
			}
		}
	}
	
	public function getisp_uid($uid){
		$userQ = $this->db->query("SELECT isp_uid FROM sht_users WHERE uid='".$uid."' AND account_activated_on != '0000-00-00 00:00:00'");
		if($userQ->num_rows() > 0){
			$rowdata = $userQ->row();
			return $rowdata->isp_uid;
		}else{
			return 0;
		}
	}
	
	public function ispbilling_details(){
		$data = array();
		$ispadmin_uid = $this->input->post('ispadmin_uid');
		$ispcodet = $this->countrydetails();
		$data['currency'] = $ispcodet['currency'];
		
		$wallet_amt = 0; $decibel_account = ''; $is_paid = 0; $passbook_amt = 0; $home_license_used = 0; $public_license_used = 0;
		$homewifi_budget = $ispcodet['cost_per_user']; $publicwifi_budget = $ispcodet['cost_per_location'];
		
		$walletinfoQ = $this->db->query("SELECT decibel_account,is_paid,homewifi_budget,publicwifi_budget FROM sht_isp_wallet WHERE isp_uid='".$ispadmin_uid."' AND is_paid='1' ORDER BY id DESC LIMIT 1");
		if($walletinfoQ->num_rows() > 0){
		    $rowdata = $walletinfoQ->row();
		    $decibel_account = $rowdata->decibel_account;
		    $is_paid = $rowdata->is_paid;
		    $homewifi_budget = $rowdata->homewifi_budget;
		    $publicwifi_budget = $rowdata->publicwifi_budget;
		}
		
		$walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='".$ispadmin_uid."'");
		if($walletQ->num_rows() > 0){
		    $rowdata = $walletQ->row();
		    $wallet_amt = $rowdata->wallet_amt;
		}
		$data['wallet_amt'] = $ispcodet['currency']." ".$wallet_amt;
		
		
		$passbookQ = $this->db->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost, COALESCE(SUM(total_active_users),0) as home_license_used, COALESCE(SUM(total_active_ap),0) as public_license_used FROM sht_isp_passbook WHERE isp_uid='".$ispadmin_uid."'");
		if($passbookQ->num_rows() > 0){
		    $passrowdata = $passbookQ->row();
		    $passbook_amt = $passrowdata->activeusers_cost;
		    $home_license_used = $passrowdata->home_license_used;
		    $public_license_used = $passrowdata->public_license_used;
		}
		$balanceamt = $wallet_amt - $passbook_amt;
		$data['balance_amt'] = $ispcodet['currency']." ".number_format($balanceamt, 2);
		$data['total_license_used'] = $passbook_amt;
		$data['home_license_used'] = $home_license_used;
		$data['public_license_used'] = $public_license_used;
		$data['decibel_account'] = $decibel_account;
		
		//Usuage Reports
		$gen = '';
		$usuageQ = $this->db->query("SELECT YEAR(added_on) AS y, MONTH(added_on) AS m, SUM(`total_active_users`) as homelicense_used, SUM(`total_active_ap`) as publiclicense_used FROM sht_isp_passbook WHERE `isp_uid`='".$ispadmin_uid."'  GROUP BY y, m Order by y DESC,m DESC");
		if($usuageQ->num_rows() > 0){
		    $i = 1;
		    foreach($usuageQ->result() as $uobj){
			$monthNum = $uobj->m;
			$year = $uobj->y;
			$total_homelic = $uobj->homelicense_used;
			$total_publiclic = $uobj->publiclicense_used;
			$total_amount = ($total_homelic * $homewifi_budget) + ($total_publiclic * $publicwifi_budget);
			$monthName = date("M", mktime(0, 0, 0, $monthNum, 10));
			$gen .= '<tr><td>'.$i.'.</td><td>'.$monthName.', '.$year.'</td><td>'.$total_homelic.'</td><td>'.$total_publiclic.'</td><td>'.$ispcodet['currency']." ".number_format($total_amount, 2).'</td></tr>';
			$i++;
		    }
		}
		$data['usuage_logs'] = $gen;
		
		echo json_encode($data);	
	}
	
	public function isp_transaction_update($success){
		$data = array();
		$postdata = $this->input->post();
		
		$ispcodet = $this->countrydetails();
		if($ispcodet['countryid'] != '101'){
			$isp_uid = $postdata['firstName'];
			$txnid = $postdata['TxId'];
			$amount = $postdata['amount'];
			if($success){
			    $this->db->insert('sht_isp_wallet', array('transaction_response' => json_encode($postdata), 'transaction_payment' => '1', 'is_paid' => '1', 'isp_uid' => $isp_uid, 'decibel_account' => 'paid', 'added_on' => date('Y-m-d H:i:s'), 'wallet_amount' => $amount));
			    $this->db->update('sht_isp_admin', array('decibel_account' => 'paid'), array('isp_uid' => $isp_uid));
			    $data['transaction'] = 1;
			    $data['txnid'] = $txnid;
			    $data['amount'] = $amount;
			}else{
			    $this->db->insert('sht_isp_wallet', array('transaction_response' => json_encode($postdata), 'transaction_payment' => '0', 'is_paid' => '0', 'isp_uid' => $isp_uid, 'decibel_account' => 'paid', 'added_on' => date('Y-m-d H:i:s')));
			    $data['transaction'] = 0;
			    $data['txnid'] = $txnid;
			    $data['amount'] = $amount;
			}
		}else{
			$isp_uid = $postdata['userid'];
			$txnid = $postdata['stripeToken'];
			$amount = ($postdata['amt']/100);
			if($success){
			    $this->db->insert('sht_isp_wallet', array('transaction_response' => json_encode($postdata), 'transaction_payment' => '1', 'is_paid' => '1', 'isp_uid' => $isp_uid, 'decibel_account' => 'paid', 'added_on' => date('Y-m-d H:i:s'), 'wallet_amount' => $amount));
			    $this->db->update('sht_isp_admin', array('decibel_account' => 'paid'), array('isp_uid' => $isp_uid));
			    $data['transaction'] = 1;
			    $data['txnid'] = $txnid;
			    $data['amount'] = $amount;
			}
		}
		return (object) $data;
	}
	
	public function citrus_hashmac(){
		$data = array();
		$formPostUrl = CITRUSPOSTURL;
		$secret_key = CITRUSSECRETKEY;
		$vanityUrl = CITRUSVANITYURL;
		$merchantTxnId = uniqid();
		$orderAmount = $this->input->post('orderAmount');
		$currency = CITRUSCURRENCY;
		$dataid = $vanityUrl . $orderAmount . $merchantTxnId . $currency;
		$securitySignature = hash_hmac('sha1', $dataid, $secret_key);
		
		$data['merchantTxnId'] = $merchantTxnId;
		$data['ssign'] = $securitySignature;
		echo json_encode($data);
	}
	
	public function isp_detail_info(){
		$session_data = $this->session->userdata('isp_session');;
		$isp_uid = $session_data['isp_uid'];
		$data = array();
		$query = $this->db->query("SELECT email, phone, isp_uid, country_id FROM sht_isp_admin WHERE isp_uid='".$isp_uid."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			$data['email'] = $rowdata->email;
			$data['phone'] = $rowdata->phone;
			$data['isp_uid'] = $rowdata->isp_uid;
			$data['country_id'] = $rowdata->country_id;
		}
		
		return $data;
	}
	
	public function isp_license_data(){
		$wallet_amt = 0; $passbook_amt = 0;
		$walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='".ISPID."' AND is_paid='1'");
		if($walletQ->num_rows() > 0){
		    $wallet_amt = $walletQ->row()->wallet_amt;
		}
		
		$passbookQ = $this->db->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost FROM sht_isp_passbook WHERE isp_uid='".ISPID."'");
		if($passbookQ->num_rows() > 0){
		    $passrowdata = $passbookQ->row();
		    $passbook_amt = $passrowdata->activeusers_cost;
		}
		$balanceamt = $wallet_amt - $passbook_amt;
		$ispcodet = $this->countrydetails();
		$cost_per_user = $ispcodet['cost_per_user'];
		if($balanceamt < $cost_per_user){
			return 0;
		}else{
			return 1;
		}
	}
	
	public function isplicense_budgetmanage(){
		$ispadmin_uid = ''; $curr_month = '11'; $curr_year = '2017';
		$xarr=array(); $mArr = array();
		$radacttQ = $this->db->query("SELECT YEAR(acctstarttime) AS y, MONTH(acctstarttime) AS m,  username, acctstarttime FROM radacct WHERE MONTH(acctstarttime) = '".$curr_month."' AND YEAR(acctstarttime) = '".$curr_year."' GROUP BY username,YEAR(acctstarttime), MONTH(acctstarttime) ORDER BY m");
		//echo $this->db->last_query(); die;
		if($radacttQ->num_rows() > 0){
			foreach($radacttQ->result() as $radobj){
				$uid = $radobj->username;
				$isp_uid = $this->getisp_uid($uid);
				$month = $radobj->m;
				$year = $radobj->y;
				$added_on = $radobj->acctstarttime;
				$xarr[$isp_uid][]=$radobj;
			}
			//echo "<pre>"; print_R($xarr); die;
			
			foreach($xarr as $mkey => $marr){
				$costamt = 0; $c = 0;
				foreach($marr as $licobj){
					$uid = $licobj->username;
					$isp_uid = $this->getisp_uid($uid);
					$month = $licobj->m;
					$year = $licobj->y;
					$added_on = $licobj->acctstarttime;
					
					$checkUserQ = $this->db->query("SELECT uid FROM sht_isp_users_track WHERE uid='".$uid."' AND  YEAR(`added_on`) = '".$year."' AND MONTH(`added_on`) = '".$month."'");
					if($checkUserQ->num_rows() == 0){
						//$this->db->insert('sht_isp_users_track', array('isp_uid' => $isp_uid, 'uid' => $uid, 'added_on' => "$added_on"));
						$costamt += 10;
						$c++;
						
					}
				}
				if(($c != 0) && ($costamt != 0)){
					$mArr[] = "isp_uid =$isp_uid, total_active_users =$c, cost =$costamt, added_o =$added_on" ;
					//$this->db->insert('sht_isp_passbook', array('isp_uid' => $isp_uid, 'total_active_users' => $c, 'total_active_ap' => '0', 'cost' => $costamt, 'added_on' => "$added_on"));
				}

			}
			echo '<pre>'; print_r($mArr);
		}
		die;
		
	}

/*------------------- NOTIFICATIONS ALERTS ---------------------------------------------------------------*/
	public function savenotification_alerts(){
		$uuid = $this->input->post('uuid');
		$sms_notify = $this->input->post('sms_notify');
		$email_notify = $this->input->post('email_notify');
		
		$this->db->update('sht_users', array('sms_alert' => $sms_notify, 'email_alert' => $email_notify), array('uid' => $uuid));
		$this->notify_model->add_user_activitylogs($uuid, 'Update Notifications Alert');
		echo json_encode(true);
	}	

	public function notification_listing(){
		$data = array();
		$uuid = $this->input->post('uuid');
		$notifyQ = $this->db->query("SELECT sms_alert, email_alert FROM sht_users WHERE uid='".$uuid."'");
		if($notifyQ->num_rows() > 0){
			$notifydata = $notifyQ->row();
			$data['sms_notify'] = $notifydata->sms_alert;
			$data['email_notify'] = $notifydata->email_alert;
		}
		$gen = '';
		$notifylist = $this->db->query("SELECT * FROM sht_users_notifications WHERE uid='".$uuid."' ORDER BY id DESC");
		if($notifylist->num_rows() > 0){
			foreach($notifylist->result() as $notifyobj){
				$uuid = $notifyobj->uid;
				$addedon = date('d-m-Y H:i:s', strtotime($notifyobj->added_on));
				$type = $notifyobj->notify_type;
				
				$sendto = '';
				$useridQ = $this->db->query("SELECT mobile, email FROM sht_users WHERE uid='".$uuid."'");
				if($useridQ->num_rows() > 0){
					$rowdata = $useridQ->row();
					if($type == 'SMS'){
						$sendto = $rowdata->mobile;
					}
				}
				
				$gen .= '<tr><td class="col-sm-2 col-xs-2">'.$addedon.'</td><td class="col-sm-2 col-xs-2">'.$notifyobj->notify_type.'</td><td class="col-sm-2 col-xs-2">'.$sendto.'</td><td class="col-sm-6 col-xs-6">'.$notifyobj->message.'</td></tr>';
			}
		}else{
			$gen .= '<tr><td colspan="3">No Records Found.</td></tr>';
		}
		$data['notifylist'] = $gen;
		echo json_encode($data);
	}
/*------------------- DIAGNOTICS ------------------------------------------------------------------------*/
        public function convertMBinGB($data) {
		$live_usage_value = 0;
		$live_usage_value = round($data/(1024*1024*1024),2);
		if($live_usage_value < 1){
			//get in mb
			$live_usage_value = round($data/(1024*1024),2);
			if($live_usage_value < 1){
				//get in kb
				$live_usage_value = round($data/(1024),2)."KB";
			}else{
				$live_usage_value = $live_usage_value."MB" ;
			}
		}else{
			$live_usage_value = $live_usage_value."GB";
		}
		return $live_usage_value;
	}
	
	public function convertsectotime($time){
		$livetime = '';
		if($time <= 86400){
			$livetime = gmdate("H:i:s", $time);
		}else{
			$extratime = ($time - 86400);
			if($extratime < 86400){
				$livetime = '1 day & '.gmdate("H:i:s", $extratime);
			}
		}
	}
	
	public function radius_diagnostic($user_uid,$date_range){
            $data = array();
	    $content = '
            <div class="table-responsive">
            <table class="table table-striped">
               <thead>
                  <tr class="active">
                     <th>S.NO</th>
		     <th>Username</th>
		     <th>Ip Address</th>
		     <th>Mac Address</th>
                     <th>Login Time</th>
                     <th>Logout Time</th>
		     <th>Download</th>
		     <th>Upload</th>
		     <th>Session Time</th>
		     <th>Terminate Cause</th>
                  </tr>
               </thead>
               <tbody>
                  ';
                $total_download = 0; $total_upload = 0;
		$date_range_explode = explode('-',$date_range);
		$date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
		$date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
                $query = $this->db->query("select username,framedipaddress,callingstationid,acctstarttime,acctstoptime,acctoutputoctets,acctinputoctets,acctsessiontime,acctterminatecause from radacct where username = '$user_uid' AND DATE(acctstarttime) between '$date_filter_from' and '$date_filter_to' order by radacctid desc");
		$records_num = $query->num_rows();
                if($query->num_rows() > 0){
                  $i = 1;
                  foreach($query->result() as $row){
			$seetime = $row->acctsessiontime;
			$total_download += $row->acctoutputoctets;
			$total_upload += $row->acctinputoctets;
			$content .= '
			<tr>
			   <td >'.$i.'</td>
			   <td >'.$row->username.'</td>
			   <td >'.$row->framedipaddress.'</td>
			   <td >'.$row->callingstationid.'</td>
			   <td >'.$row->acctstarttime.'</td>
			   <td >'.$row->acctstoptime.'</td>
			   <td >'.$this->convertMBinGB($row->acctoutputoctets).'</td>
			   <td >'.$this->convertMBinGB($row->acctinputoctets).'</td>
			   <td >'.gmdate("H:i:s", $seetime).'</td>
			   <td >'.$row->acctterminatecause.'</td>
			</tr>
			';
			$i++;
                  }
                }else{
                  $content .= '
                  <tr>
                     <td colspan="4">No Records Found.</td>
                  </tr>
                  ';
                }
                  $content .= '
               </tbody>
            </table>
            </div>
            ';
	    $data['radius_logs'] = $content;
	    $data['total_download'] = $this->convertMBinGB($total_download);
	    $data['total_upload'] = $this->convertMBinGB($total_upload);
            
	    echo json_encode($data);
        }
	public function radiuslogs_rptexport(){
		$user_uid = $this->input->post('cust_uuid');
		$date_range = $this->input->post('date_range');
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(0);
		//Set sheet style
		$styleArray = array(
		    'font'  => array(
			'bold'  => false,
			'color' => array('rgb' => '000000'),
			'name'  => 'Tahoma',
			'size' => 11
		    )
		);
		//Set sheet style
		$styleArray1 = array(
		    'font'  => array(
			'bold'  => false,
			'color' => array('rgb' => '000000'),
			'name'  => 'Tahoma',
			'size' => 9
		    )
		);
		
		
		$titlestyleArray = array(
		    'font'  => array(
			'bold'  => false,
			'color' => array('rgb' => '000000'),
			'name'  => 'Tahoma',
			'size' => 14
		    ),
		    'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
		    )
		);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		
		$objPHPExcel->getActiveSheet()->setTitle("Data Usuage Report");
		//Set sheet columns Heading
		$objPHPExcel->getActiveSheet()->setCellValue('A1','Data Usuage: '.$date_range);
		$objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
		$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($titlestyleArray);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A2','UID');
		$objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('B2','IP Address');
		$objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('C2','Mac Address');
		$objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('D2','Login Time');
		$objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('E2','Logout Time');
		$objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('F2','Download');
		$objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('G2','Upload');
		$objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('H2','Session Time');
		$objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('I2','Terminate Cause');
		$objPHPExcel->getActiveSheet()->getStyle('I2')->applyFromArray($styleArray);
		
		$total_download = 0; $total_upload = 0;
		$date_range_explode = explode('-',$date_range);
		$date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
		$date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
                $query = $this->db->query("select username,framedipaddress,callingstationid,acctstarttime,acctstoptime,acctoutputoctets,acctinputoctets,acctsessiontime,acctterminatecause from radacct where username = '$user_uid' AND DATE(acctstarttime) between '$date_filter_from' and '$date_filter_to' order by radacctid desc");
		$records_num = $query->num_rows();
                if($query->num_rows() > 0){
			$j = 3;
			foreach($query->result() as $row){
			      $seetime = $row->acctsessiontime;
			      $total_download += $row->acctoutputoctets;
			      $total_upload += $row->acctinputoctets;
      
			      $objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $row->username);
			      $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
			      $objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $row->framedipaddress);
			      $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
			      $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $row->callingstationid);
			      $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
			      $objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row->acctstarttime);
			      $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
			      $objPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->acctstoptime);
			      $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
			      $objPHPExcel->getActiveSheet()->setCellValue('F'.$j, $this->convertMBinGB($row->acctoutputoctets));
			      $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
			      $objPHPExcel->getActiveSheet()->setCellValue('G'.$j, $this->convertMBinGB($row->acctinputoctets));
			      $objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($styleArray1);
			      $objPHPExcel->getActiveSheet()->setCellValue('H'.$j, gmdate("H:i:s", $seetime));
			      $objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($styleArray1);
			      $objPHPExcel->getActiveSheet()->setCellValue('I'.$j, $row->acctterminatecause);
			      $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($styleArray1);
			      
			      $j++;
			}
			
			$total_download = $this->convertMBinGB($total_download);
			$total_upload = $this->convertMBinGB($total_upload);
			
			$j++;
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$j, 'Total Download');
			$objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$j, $total_download);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray);
			$j++;
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$j, 'Total Upload');
			$objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$j, $total_upload);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray);
                }
		
		// Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		// Write the Excel file to filename some_excel_file.xlsx in the current directory
		$objWriter->save('reports/radiuslogs_report.xlsx');
		
		echo json_encode(true);
	}
        public function mikrotik_log_detail($user_uid,$router_ip,$router_username,$router_password){
            $content = '
            <table class="table table-striped">
               <thead>
                  <tr class="active">
                     <th class="col-sm-1 col-xs-1">S.NO</th>
                     <th class="col-sm-2 col-xs-2">TIME</th>
                     <th class="col-sm-2 col-xs-2">TOPICS</th>
                     <th class="col-sm-2 col-xs-2">MESSAGE</th>
                  </tr>
               </thead>
               <tbody>
                  ';
                  $this->load->library('routerlib');
                  $conn = RouterOS::connect("$router_ip", "$router_username", "$router_password")or die("couldn't connectto router");
                  $logs = $conn->getall("/log");
                  $logs = array_reverse($logs);
                  $i = 1;
                  foreach($logs as $logs_data){
                  if($i > 50){
                  break;
                  }
                  $content .= '
                  <tr>
                     <td >'.$i.'</td>
                     <td >'.$logs_data['time'].'</td>
                     <td >'.$logs_data['topics'].'</td>
                     <td >'.$logs_data['message'].'</td>
                  </tr>
                  ';
                  $i++;
                  }
                  $content .= '
               </tbody>
            </table>
            ';
            return $content;
        }



        public function router_authlogs(){
            $gen = '
                <div class="table-responsive" >
                <table class="table table-striped">
                    <thead>
                       <tr class="active">
                          <th>&nbsp;</th>
                          <th>USERNAME</th>
                          <th>NASIP</th>
                          <th>ERROR</th>
                          <th>FAILED CONDITION</th>
                          <th>DATE</th>
                          <th>TIME</th>
                       </tr>
                    </thead>
                    <tbody>
            ';
            $uuid = $this->input->post('uid');
            $authlogsQ = $this->db->query("SELECT * FROM authentication_log WHERE userid='".$uuid."' ORDER BY id DESC LIMIT 0,50");
            if($authlogsQ->num_rows() > 0){
                foreach($authlogsQ->result() as $authobj){
                   $gen .= '<tr><td>&nbsp;</td><td>'.$authobj->userid.'</td><td>'.$authobj->nasip.'</td><td>'.$authobj->error.'</td><td>'.$authobj->condition_failed.'</td><td>'.$authobj->date.'</td><td>'.$authobj->time.'</td></tr>';
                }
            }else{
                    $gen .= '<tr><td colspan="7">No Logs Found.</td></tr>';
            }
            
            $gen .= '
                   </tbody>
                </table>
                </div>';
                
            echo $gen;
        }
	public function nasips_listing(){
		$gen = '';
		$session_data = $this->session->userdata('isp_session');;
		$isp_uid = $session_data['isp_uid'];
		$nasQuery = $this->db->query("SELECT nasname FROM nas WHERE isp_uid='".$isp_uid."' AND nastype='1'");
		if($nasQuery->num_rows() > 0){
			foreach($nasQuery->result() as $nasobj){
				$routerip = $nasobj->nasname;
				$gen .= '<option value="'.$routerip.'">'.$routerip.'</option>';
			}
		}
		
		echo $gen;
	}
	public function update_expirationdate_forall(){
		$query = $this->db->query("SELECT uid FROM sht_users WHERE `expiration` LIKE '%0000-00-00%'");
		if($query->num_rows() > 0){
			foreach($query->result() as $qobj){
				$uuid = $qobj->uid;
				$userassoc_plandata = $this->userassoc_plandata($uuid);
				$user_credit_limit = $userassoc_plandata['user_credit_limit'];
				$user_wallet_balance = $this->userbalance_amount($uuid);
				$plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
				$total_advpay = $userassoc_plandata['total_advpay'];
				$actiondate = $this->affected_date_on_wallet($uuid);
				$expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
				$expiration_date = date('d-m-Y', strtotime($actiondate . " +".$expiration_acctdays." days"));
				$expiration_ddate = date('Y-m-d', strtotime($expiration_date)).' 00:00:00';
				//$this->db->update('sht_users', array('expiration' => $expiration_ddate), array('uid' => $uuid));
			}
		}
	}
	public function update_comblimit_forall(){
		$session_data = $this->session->userdata('isp_session');
		$superadmin = $session_data['super_admin'];
		$isp_uid = $session_data['isp_uid'];
		//$isp_uid = '105';
		$query = $this->db->query("SELECT uid,downlimit,comblimit FROM sht_users WHERE isp_uid='".$isp_uid."' AND comblimit < 0 AND inactivate_type != 'terminate'");
		if($query->num_rows() > 0){
			foreach($query->result() as $qobj){
				$uuid = $qobj->uid;
				$comblimit = $qobj->comblimit;
				$downlimit = $qobj->downlimit;
				$live_usage = $this->live_usage($uuid);
				
				$newdatalimit = 0;
				$plan_type = $live_usage['plan_type'];
				$datacalc = $live_usage['datacalcon'];
				$plandatalimit = $live_usage['plandatalimit'];
				$radactt_usage = $live_usage['radactt_usage'];
				if($plan_type == '3'){
					echo $uuid. '=>comb: '. $comblimit . '=>radact: '.$radactt_usage .'=>data: '. $plandatalimit ; echo '<br/>';
					if($datacalc == 2){
						if(($comblimit < $radactt_usage) && ($radactt_usage < $plandatalimit)){
							$newdatalimit = ($plandatalimit - $radactt_usage);
							$this->db->update('sht_users', array('comblimit' => $newdatalimit), array('uid' => $uuid));
						}
					}elseif($datacalc == 1){
						if(($downlimit < $radactt_usage) && ($radactt_usage < $plandatalimit)){
							$newdatalimit = ($plandatalimit - $radactt_usage);
							$this->db->update('sht_users', array('downlimit' => $newdatalimit), array('uid' => $uuid));
						}
					}
				}
				
			}
		}
	}
	public function usage_graph_billing_date($subscriber_uid){
		$query = $this->db->query("select plan_activated_date from sht_users where uid = '$subscriber_uid'");
		if($query->num_rows() > 0){
			$row = $query->row_array();
			$from = date('d-m-Y', strtotime($row['plan_activated_date']));
			$to = date('d-m-Y');
		}else{
			$from = date('d-m-Y');
			$to = date('d-m-Y');
		}
		return $from.' - '.$to;
	}
/*------------------- EXTRA FUNCTIONS ---------------------------------------------------*/
	public function assign_new_permission() {
		$slugarr = array('ACTIVEUSERS', 'INACTIVEUSERS', 'LEADENQUIRYUSERS', 'COMPLAINTSUSERS', 'PAYMENTDUEUSERS', 'OTHERDUESUSERS', 'USERSTATUS', 'SYSLOG', 'INVENTORY', 'NOTIFICATIONS', 'DIAGNOSTICS');
		foreach($slugarr as $slug){
			$query = $this->db->query("select id,super_admin,dept_id from sht_isp_users");
			foreach($query->result() as $rowarr){
				$deptid = $rowarr->dept_id;
				$query1 = $this->db->query("select id from sht_tab_menu where slug='" . $slug . "' ");
				$tabarr = $query1->row_array();
				$tabid = $tabarr['id'];

				$query2 = $this->db->query("select id from sht_isp_permission where isp_deptid='" . $deptid . "' and tabid='" . $tabid . "'");
				$tabledata1=array("tabid"=>$tabid, "isp_deptid"=>$deptid, "is_edit"=>'1', "is_add"=>'0', "is_delete"=>'0', "is_readonly"=>'0', "is_hide"=>'0', "added_on"=>date("Y-m-d H:i:s"));
				if($query2->num_rows()>0){
				      $this->db->update(SHTISPPERM, $tabledata1, array('tabid' => $tabid, "isp_deptid"=>$deptid));
				}
				else {
				    $this->db->insert(SHTISPPERM, $tabledata1);
				}

			}
		}
		
		sleep(5);
		
		$billarr = array('MONTHLYTAB', 'SETUPBILLS', 'CUSTOMBILLS', 'DELETEWALLETRECEIPTS', 'PRINTMONTHLYBILLS', 'DOWNLOADMONTHLYBILLS', 'SENDMONTHLYBILLS', 'EDITMONTHLYBILLS', 'ADVANCEPREPAY', 'ADDTOWALLET', 'CREDITLIMIT');
		
		foreach($billarr as $slug){
			$query = $this->db->query("select id,super_admin,dept_id from sht_isp_users");
			foreach($query->result() as $rowarr){
				$deptid = $rowarr->dept_id;
	
				$query2 = $this->db->query("select is_edit,is_readonly,is_hide from sht_isp_permission where isp_deptid='" . $deptid . "' and tabid='3'");
				if($query2->num_rows()>0){
					$rowarr = $query2->row();
					$billedit = $rowarr->is_edit;
					$billreadonly = $rowarr->is_readonly;
					$billhide = $rowarr->is_hide;
					
					$query1 = $this->db->query("select id from sht_tab_menu where slug='" . $slug . "' ");
					$tabarr = $query1->row_array();
					$tabid = $tabarr['id'];
					
					$tabledata2=array("tabid"=>$tabid, "isp_deptid"=>$deptid, "is_edit"=>$billedit, "is_add"=>'0', "is_delete"=>'0', "is_readonly"=>$billreadonly, "is_hide"=>$billhide, "added_on"=>date("Y-m-d H:i:s"));
					$query3 = $this->db->query("select id from sht_isp_permission where isp_deptid='" . $deptid . "' and tabid='" . $tabid . "'");
					if($query3->num_rows()>0){
						$this->db->update(SHTISPPERM, $tabledata2, array('tabid' => $tabid, "isp_deptid"=>$deptid));
					}else {
						$this->db->insert(SHTISPPERM, $tabledata2);
					}
				      
				}
				
			}
		}
		
	}
	
	public function get_isp_name(){
		$isp_uid = 0;
		if(defined('ISPID')){
			$isp_uid = ISPID;
		}
		$data = array();
		$isp_name = '';
		$fevicon_icon = '';
		$query = $this->db->query("select isp_name,fevicon_icon from sht_isp_admin where isp_uid = '$isp_uid'");
		if($query->num_rows() > 0){
			$row = $query->row_array();
			$isp_name = $row['isp_name'];
			$fevicon_icon = $row['fevicon_icon'];
		}
		$data['isp_name'] = $isp_name;
		$data['fevicon_icon'] = $fevicon_icon;
		return $data;
	}
	
	public function illnaslisting(){
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$nasname = $this->input->post('nasname');
		
		$content = '<option value="">Select NAS</option>';
		$nas_illQuery = $this->db->query("SELECT nasid, nasname FROM nas_ill WHERE isp_uid='".$isp_uid."' GROUP BY nasname");
		if($nas_illQuery->num_rows() > 0){
			foreach($nas_illQuery->result() as $nasobj){
				$sel = '';
				if($nasname == $nasobj->nasname){
					$sel = 'selected="selected"';
				}
				$content .= '<option value="'.$nasobj->nasid.'" '.$sel.'>'.$nasobj->nasname.'</option>';
			}
		}
		
		echo $content;
	}
	public function get_ipranges_Ill(){
		$data = array();
		$content = '<option value="">Select Available IP</option>';
		$sessiondata = $this->session->userdata('isp_session');
		$isp_uid = $sessiondata['isp_uid'];
		$nasid = $this->input->post('nasid');
		$iptype = $this->input->post('iptype');
		
		$selips = $this->input->post('ip_assigned');
		if(strpos($selips, ',') > 0){
			$ip_assigned = explode(',' , $this->input->post('ip_assigned'));
		}else{
			$ip_assigned[0] = $this->input->post('ip_assigned');
		}
		
		$uuid = $this->input->post('uuid');
		
		$fetchnasQ = $this->db->query("SELECT id FROM nas_ill WHERE isp_uid='".$isp_uid."' AND nasid='".$nasid."' AND ip_type='".$iptype."'");
		if($fetchnasQ->num_rows() > 0){
			foreach($fetchnasQ->result() as $nasobj){
				$nas_ill_id = $nasobj->id;
				$nas_iprangeQ = $this->db->query("SELECT ip_address FROM nas_ill_iprange WHERE nas_ill_id='".$nas_ill_id."' AND (is_assigned='0' OR (is_assigned='1' AND uid='".$uuid."'))");
				if($nas_iprangeQ->num_rows() > 0){
					foreach($nas_iprangeQ->result() as $nasipobj){
						$ip_address = $nasipobj->ip_address;
						$sel = '';
						if(in_array($ip_address, $ip_assigned)){
							$sel = 'selected="selected"';
						}
						$content .= '<option value="'.$ip_address.'" '.$sel.'>'.$ip_address.'</option>';
					}
				}
			}
		}
		$data['content'] = $content;
		echo json_encode($data);
	}
	
	public function refresh_data(){
		$useruid=$this->input->post('uid');
		$live_usage =0;
		$percent = 0;
		$topupdata = 0; $topup_allocated = 0; $topup_added_on = ''; $topup_usuage = 0;
		$data=array();
		// get topup limit
		$get_topup = $this->db->query("select ss.plantype, ss.datalimit, su.added_on from sht_usertopupassoc as su inner join sht_services as ss on(su.topup_id = ss.srvid) where su.uid = '$useruid' AND su.status='1' AND su.terminate='0' AND su.topuptype='1'");
		if($get_topup->num_rows() > 0){
			foreach($get_topup->result() as $tobj){
				$topupdata += $tobj->datalimit;
				$topup_added_on = date('Y-m-d H:i:s', strtotime($tobj->added_on." -2 minutes"));
			}
		}
		$plandata_allocated = 0; $plan_type = '';
		$get_plan = $this->db->query("select su.comblimit, su.plan_changed_datetime, su.downlimit, ss.plantype, ss.datalimit, ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$useruid' AND su.baseplanid != '0'");
		if($get_plan->num_rows() > 0){
			$get_plan_row = $get_plan->row_array();
			$plandata_allocated = $get_plan_row['datalimit'];
			$plan_type = $get_plan_row['plantype'];
		}
		
		
		// get billing date
		$get_billing_date = $this->db->query("select plan_activated_date, next_bill_date from sht_users where uid = '$useruid'");

		if($get_billing_date->num_rows() > 0){
			$row_billing_date = $get_billing_date->row_array();
			$start_date = date('Y-m-d', strtotime($row_billing_date['plan_activated_date']));
			$end = $row_billing_date['next_bill_date'];
			$end_date = date('Y-m-d', strtotime($row_billing_date['next_bill_date']));
		}
		$query = $this->db->query("select acctinputoctets, acctoutputoctets from radacct where username = '$useruid' AND DATE(acctstarttime) between '$start_date' and '$end_date'");
		foreach($query->result() as $row){
			$get_plan_row = $get_plan->row_array();
			$datacalc = $get_plan_row['datacalc'];
			if($datacalc == 2){
				$live_usage = $live_usage + $row->acctinputoctets + $row->acctoutputoctets;
			}else{
				$live_usage = $live_usage + $row->acctoutputoctets;
			}
		}
		
		if($plan_type == '3'){
			//echo 'live_usuage: '.$this->convertbytestodata($live_usage);
			if($live_usage <= $plandata_allocated){
				$finaldata = ($plandata_allocated + $topupdata) - $live_usage;
				$tabledata = array("comblimit" => $finaldata);
				$this->db->update("sht_users",$tabledata,array("uid"=>$useruid));
			}
			// fetch query after topup apply if limit goes in negative
			elseif(($topupdata != 0) && ($live_usage > $plandata_allocated)){
				$end_date = date('Y-m-d H:i:s');
				$query = $this->db->query("select acctinputoctets, acctoutputoctets from radacct where username = '$useruid' AND acctstarttime between '$topup_added_on' and '$end_date'");
				//echo $this->db->last_query();
				foreach($query->result() as $row){
					$get_plan_row = $get_plan->row_array();
					$datacalc = $get_plan_row['datacalc'];
					if(($datacalc == 2) || ($datacalc == 0)){
						$topup_usuage = $topup_usuage + $row->acctinputoctets + $row->acctoutputoctets;
					}else{
						$topup_usuage = $topup_usuage + $row->acctoutputoctets;
					}
				}
				
				
				$data['live_usage'] = $this->convertbytestodata($live_usage);
				
				if($topup_usuage > $topupdata){
					$postfupdata = (($plandata_allocated + $topupdata) - $live_usage);
					$data['topup_data_done'] = $this->convertbytestodata($topupdata);
					
					$tabledata = array("comblimit" => $postfupdata);
					$this->db->update("sht_users",$tabledata,array("uid"=>$useruid));
				}else{
					$postfupdata = ($topupdata - $topup_usuage);
					$data['topup_data_still'] = $this->convertbytestodata($topupdata);
					
					$tabledata = array("comblimit" => $postfupdata);
					$this->db->update("sht_users",$tabledata,array("uid"=>$useruid));
				}
				$data['postfup_data'] = $this->convertbytestodata($postfupdata);
				
			}
			elseif($live_usage > $plandata_allocated){
				$postfupdata = ($plandata_allocated - $live_usage);
				$tabledata = array("comblimit" => $postfupdata);
				$this->db->update("sht_users",$tabledata,array("uid"=>$useruid));
			}
		}else{
			$finaldata = ($plandata_allocated+$topupdata) - $live_usage;
			$tabledata=array("comblimit"=>$finaldata);
			$this->db->update("sht_users",$tabledata,array("uid"=>$useruid));
		}
		
		
		
		$data['resultcode']=1;
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		return $data;
		
		
	}

}


?>