<?php

class Mgmt_model extends CI_Model{
    
    public function __construct() {
        parent::__construct();
    }
    
    
    public function list_department(){
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        $ispcond='';
        $ispcond=" and isp_uid='".$isp_uid."'";
        
        $deptarr=$this->dept_user_cond();
        $cond='';
       
        if(!empty($deptarr))
        {
            $paramid = '"' . implode('", "', $deptarr) . '"';
            $cond.="AND sd.id IN ({$paramid})";
        }
    
        
        $query=$this->db->query("SELECT sd.id,sd.`dept_name`,(SELECT COUNT(id) FROM `sht_isp_users` siu WHERE siu.dept_id=sd.id and status='1' and is_deleted='0') AS team_count FROM `sht_department` sd WHERE sd.status='1' AND sd.is_deleted=0 {$ispcond} {$cond} ");
    
    
        return $query->result();
    }
    
    public function list_franchise(){
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        $ispcond='';
        $ispcond=" and parent_isp='".$isp_uid."'";
        $cond='';
        $query=$this->db->query("SELECT isp_name,email,phone,id FROM sht_isp_admin where is_activated='1' and is_deleted='0' {$ispcond}");
        return $query->result();
    }
    
    public function get_tab_data($parent){
        
        $query=$this->db->query("select id,`tab_name`,`is_addable`,`is_editable`,`is_deletable`,`is_readable`,`is_hidable` from sht_tab_menu where parent='".$parent."' and status='1' order by sequence asc");
       //  echo $this->db->last_query();
        return $query->result();
    }
    
    public function add_permission(){
        $postdata=$this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
      //  echo "<pre>"; print_R($postdata); die;
        $assign_ticket=(isset($postdata['assigntkt']) && $postdata['assigntkt']!='')?$postdata['assigntkt']:0;
        if(isset($postdata['dept_id']) && $postdata['dept_id']!='')
        {
             $tabledata1=array("dept_name"=>$postdata['dept_name'],"isp_uid"=>$isp_uid,"region_type"=>$postdata['region'],"created_on"=>date("Y-m-d H:i:s"),"assign_ticket"=>$assign_ticket);
            $this->db->update('sht_department', $tabledata1, array('id' => $postdata['dept_id']));
             
              if ($postdata['region'] == "region") {

                $listarr = array_filter(explode(",", $postdata['regiondat']));

                foreach ($listarr as $vald) {
                    $datarr = explode("::", $vald);
                    //  `plan_id`,`state_id`,`city_id`,`zone_id`,
                    $state=($datarr[0]!='')?$datarr[0]:'all';
                    $city=($datarr[1]!='')?$datarr[1]:'all';
                    $zone=($datarr[2]!='')?$datarr[2]:'all';
                    $tabledata = array("dept_id" => $postdata['dept_id'], "state_id" => $state,
                        "city_id" => $city, "zone_id" => $zone,
                        "created_on" => date("Y-m-d H:i:s"),"isp_uid"=>$isp_uid);

                    if (isset($datarr[3]) && $datarr[3] != '') {
                        $this->db->update(SHTDEPTREGION, $tabledata, array('id' => $datarr[3]));
                    } else {
                        $this->db->insert(SHTDEPTREGION, $tabledata);
                    }
                }
            } else {
                $this->db->delete(SHTDEPTREGION, array("dept_id" => $postdata['dept_id']));
            }
            foreach($postdata['tabmenu'] as $vald)
            {
                $isedit=0;$isadd=0;$isdel=0;$isro=0;$ishide=0;
                $key="perm_".$vald;
                if(isset($postdata[$key]))
                {
                    if(in_array("add",$postdata[$key]))
                    {
                       $isadd =1;
                    }
                  if(in_array("edit",$postdata[$key]))
                    {
                        $isedit =1;
                    }
                   if(in_array("delete",$postdata[$key]))
                    {
                        $isdel=1;
                    }
                   if(in_array("ro",$postdata[$key]))
                    {
                        $isro=1;
                    }
                    if(in_array("hide",$postdata[$key]))
                    {
                        $ishide=1;
                    }
                        
                }
                else
                {
                    $isedit=0;$isadd=0;$isdel=0;$isro=1;$ishide=0;
                }
                 $tabledata1=array("tabid"=>$vald,"isp_deptid"=>$postdata['dept_id'],"is_edit"=>$isedit,"is_add"=>$isadd,"is_delete"=>$isdel,
                    "is_readonly"=>$isro,"is_hide"=>$ishide,"added_on"=>date("Y-m-d H:i:s"));
                $query=$this->db->query("select id from sht_isp_permission where isp_deptid='".$postdata['dept_id']."' and tabid='".$vald."'");
                if($query->num_rows()>0){
                      $this->db->update(SHTISPPERM, $tabledata1, array('tabid' => $vald,"isp_deptid"=>$postdata['dept_id']));
                }
                else {
                    $this->db->insert(SHTISPPERM, $tabledata1);
                }
               
            }
            return $postdata['dept_id'];
        }
        else
        {
            $tabledata=array("dept_name"=>$postdata['dept_name'],"isp_uid"=>$isp_uid,"region_type"=>$postdata['region'],"created_on"=>date("Y-m-d H:i:s"),"assign_ticket"=>$assign_ticket);
            $this->db->insert(SHTDEPT,$tabledata);
            $deptid=$this->db->insert_id();
            
            
             if ($postdata['region'] == "region") {

                $listarr = array_filter(explode(",", $postdata['regiondat']));

                foreach ($listarr as $vald) {
                    $datarr = explode("::", $vald);
                    //  `plan_id`,`state_id`,`city_id`,`zone_id`,
                    $tabledata = array("dept_id" => $deptid, "state_id" => $datarr[0],
                        "city_id" => $datarr['1'], "zone_id" => $datarr[2],
                        "created_on" => date("Y-m-d H:i:s"),"isp_uid"=>$isp_uid);

                    if (isset($datarr[3]) && $datarr[3] != '') {
                        $this->db->update(SHTDEPTREGION, $tabledata, array('id' => $datarr[3]));
                    } else {
                        $this->db->insert(SHTDEPTREGION, $tabledata);
                    }
                }
            } else {
                $this->db->delete(SHTDEPTREGION, array("dept_id" => $deptid));
            }

            
            foreach($postdata['tabmenu'] as $vald)
            {
                $isedit=0;$isadd=0;$isdel=0;$isro=0;$ishide=0;
                $key="perm_".$vald;
                if(isset($postdata[$key]))
                {
                    if(in_array("add",$postdata[$key]))
                    {
                       $isadd =1;
                    }
                  if(in_array("edit",$postdata[$key]))
                    {
                        $isedit =1;
                    }
                   if(in_array("delete",$postdata[$key]))
                    {
                        $isdel=1;
                    }
                   if(in_array("ro",$postdata[$key]))
                    {
                        $isro=1;
                    }
                    if(in_array("hide",$postdata[$key]))
                    {
                        $ishide=1;
                    }
                        
                }
                else
                {
                    $isedit=0;$isadd=0;$isdel=0;$isro=1;$ishide=0;
                }
                $tabledata1=array("tabid"=>$vald,"is_edit"=>$isedit,"is_add"=>$isadd,"is_delete"=>$isdel,
                    "is_readonly"=>$isro,"is_hide"=>$ishide,"added_on"=>date("Y-m-d H:i:s"),"isp_deptid"=>$deptid);
                $this->db->insert(SHTISPPERM,$tabledata1);
            }
            return $deptid;
        }
    }
    
    
    public function add_franchise_data(){
        $postdata=$this->input->post();
       
        if(isset($postdata['franchise_id']) && $postdata['franchise_id']!='')
        {
            $query=$this->db->query("select isp_uid from sht_isp_admin where id='".$postdata['franchise_id']."'");
            $row=$query->row_array();
            $isp_uid=$row['isp_uid'];
             $tabledata=array("isp_name"=>$postdata['franchise_name'],"legal_name"=>$postdata['legal_name'],"phone"=>$postdata['franchise_mobile'],"super_admin"=>0,
                         "is_activated"=>'1',"is_deleted"=>'0',"franchise_margin"=>$postdata['margin'],"activated_on"=>date("Y-m-d H:i:s"),
                         "added_on"=>date("Y-m-d H:i:s"));
             $this->db->update('sht_isp_admin',$tabledata,array("id"=>$postdata['franchise_id']));
              $listarr = array_filter(explode(",", $postdata['regiondat']));

                foreach ($listarr as $vald) {
                    $datarr = explode("::", $vald);
                   
                    $tabledata = array("isp_uid" => $isp_uid, "state" => $datarr[0],
                        "city" => $datarr['1'], "zone" => $datarr[2],
                        "added_on" => date("Y-m-d H:i:s"),"status"=>1);

                     if (isset($datarr[3]) && $datarr[3] != '') {
                        $this->db->update('sht_isp_admin_region', $tabledata, array('id' => $datarr[3]));
                    } else {
                        $this->db->insert('sht_isp_admin_region', $tabledata);
                    }
                }
                 return $postdata['franchise_id'];
        }
        else{
             $query=$this->db->query("select isp_uid from sht_isp_admin order by id desc limit 1");
        $row=$query->row_array();
        $isp_uid=$row['isp_uid']+1;
       // $pwd=random_string('alnum', 7);
        $pwd=$postdata['franchise_mobile'];
        $encpwd=md5($pwd);
        $sessiondata = $this->session->userdata('isp_session');
        $parentisp_uid = $sessiondata['isp_uid'];
        $query1=$this->db->query("select country_id from sht_isp_admin where isp_uid='".$parentisp_uid."'");
        $rowdata=$query1->row_array();
        $country_id=$rowdata['country_id'];
        
         
        $tabledata=array("isp_uid"=>$isp_uid,"isp_name"=>$postdata['franchise_name'],"legal_name"=>$postdata['legal_name'],"orig_pwd"=>$pwd,
                         "password"=>$encpwd,"email"=>$postdata['franchise_email'],"phone"=>$postdata['franchise_mobile'],"super_admin"=>1,
                         "is_activated"=>'1',"is_deleted"=>'0',"franchise_margin"=>$postdata['margin'],"activated_on"=>date("Y-m-d H:i:s"),
                         "added_on"=>date("Y-m-d H:i:s"),"parent_isp"=>$parentisp_uid,"is_franchise"=>1,"country_id"=>$country_id);
         $this->db->insert('sht_isp_admin',$tabledata);
          $franchiseid=$this->db->insert_id();
          $listarr = array_filter(explode(",", $postdata['regiondat']));

                foreach ($listarr as $vald) {
                    $datarr = explode("::", $vald);
                    //  `plan_id`,`state_id`,`city_id`,`zone_id`,
                    $tabledata = array("isp_uid" => $isp_uid, "state" => $datarr[0],
                        "city" => $datarr['1'], "zone" => $datarr[2],
                        "added_on" => date("Y-m-d H:i:s"),"status"=>1);

                    if (isset($datarr[3]) && $datarr[3] != '') {
                        $this->db->update('sht_isp_admin_region', $tabledata, array('id' => $datarr[3]));
                    } else {
                        $this->db->insert('sht_isp_admin_region', $tabledata);
                    }
                }
                return $franchiseid;
        }
       
    }
     
    
    public function dept_data($id){
        $condarr=array("id"=>$id);
        $query=$this->db->get_where(SHTDEPT,$condarr);
        return $query->row_array();
    }
    
     public function franchise_data($id){
        $condarr=array("id"=>$id);
        $query=$this->db->get_where('sht_isp_admin',$condarr);
        return $query->row_array();
    }
    
     public function dept_region_data($id) {
        $condarr = array('dept_id' => $id, 'is_deleted' => 0);
        $query = $this->db->get_where(SHTDEPTREGION, $condarr);
        return $query->result();
    }
    
    public function franchise_region_data($ispid){
         $condarr = array('isp_uid' => $ispid, 'status' => 1);
        $query = $this->db->get_where('sht_isp_admin_region', $condarr);
        return $query->result();
    }
    
    public function dept_permission_data($id){
        $condarr=array("isp_deptid"=>$id);
        $query=$this->db->get_where(SHTISPPERM,$condarr);
        $data=array();
        foreach($query->result() as $val)
        {
            $data[$val->tabid]=$val;
        }
        return $data;
        
    }
    
       public function delete_region_dept() {
        $postdata = $this->input->post();
        $tabledata = array("is_deleted" => 1);
        $this->db->update(SHTDEPTREGION, $tabledata, array('id' => $postdata['id']));
        return $postdata['id'];
    }
    
      public function delete_region_franchise() {
        $postdata = $this->input->post();
        $tabledata = array("status" => 0);
        $this->db->update('sht_isp_admin_region', $tabledata, array('id' => $postdata['id']));
        return $postdata['id'];
    }
    
      public function delete_dept()
        {
            $postdata=$this->input->post();
            $tabledata=array("is_deleted"=>1);
           $this->db->update(SHTDEPT, $tabledata, array('id' => $postdata['deptid']));
                        return  $postdata['deptid'];
        }
        
         public function delete_franchise()
        {
            $postdata=$this->input->post();
            $tabledata=array("is_deleted"=>"1");
           $this->db->update('sht_isp_admin', $tabledata, array('id' => $postdata['franchid']));
                        return  $postdata['franchid'];
        }
        
        public function department_region_access()
        {
             $session_data = $this->session->userdata['isp_session'];
             if($session_data['super_admin']==1)
             {
                 return true;
             }
             else
             {
                 $userdeptid=$session_data['dept_id'];
                 $query=$this->db->query("select region_type from sht_department where id='".$userdeptid."'");
                 $rowarr=$query->row_array();
                 $region_type=$rowarr['region_type'];
                 if($region_type=="allindia")
                 {
                     return true;
                 }
                 else
                 {
                     return false;
                 }
             }
        }
        
        public function dept_user_cond()
        {
            $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
        $regiontype = $this->plan_model->dept_region_type();
       
        $permicond = '';
        $sczcond = '';
        $deptarr=array();
        if ($regiontype == "region") {
            $dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='" . $dept_id . "'  AND is_deleted='0' AND status='1'");
            $total_deptregion = $dept_regionQ->num_rows();
            if ($dept_regionQ->num_rows() > 0) {
                $c = 1;
                foreach ($dept_regionQ->result() as $deptobj) {
                    $stateid = $deptobj->state_id;
                    $cityid = $deptobj->city_id;
                    $zoneid = $deptobj->zone_id;
                    if ($cityid == 'all') {
                        $permicond .= " (state_id='" . $stateid . "') ";
                    } elseif ($zoneid == 'all') {
                        $permicond .= " (state_id='" . $stateid . "' AND city_id='" . $cityid . "') ";
                    } else {
                        $permicond .= " (state_id='" . $stateid . "' AND city_id='" . $cityid . "' AND zone_id='" . $zoneid . "') ";
                    }
                    if ($c != $total_deptregion) {
                        $permicond .= ' OR ';
                    }
                    $c++;
                }
            }
            if($permicond!='')
            {
            $sczcond .= ' AND (' . $permicond . ')';
            }
            $query=$this->db->query("select dept_id from sht_dept_region where status='1' {$sczcond}");
          //  echo $this->db->last_query();
            $deptarr=array();
            foreach($query->result() as $val)
            {
                $deptarr[]=$val->dept_id;
            }
           // echo "<pre>"; print_R($deptarr); die;
             
        }
        return $deptarr;
        }
        
          public function check_dept_deletable() {
        $deptid = $this->input->post('deldeptid');
        $query = $this->db->query("select id from sht_isp_users where dept_id='" . $deptid . "' ");
        if ($query->num_rows() > 0) {
            return 0;
        } else {
            return 1;
        }
    }
    
    public function isp_license_data(){
		$wallet_amt = 0; $passbook_amt = 0;
		$walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='".ISPID."' AND is_paid='1'");
		if($walletQ->num_rows() > 0){
		    $wallet_amt = $walletQ->row()->wallet_amt;
		}
		
		$passbookQ = $this->db->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost FROM sht_isp_passbook WHERE isp_uid='".ISPID."'");
		if($passbookQ->num_rows() > 0){
		    $passrowdata = $passbookQ->row();
		    $passbook_amt = $passrowdata->activeusers_cost;
		}
		$balanceamt = $wallet_amt - $passbook_amt;
		if($balanceamt < 0){
			return 0;
		}else{
			return 1;
		}
	}

    
}

?>
