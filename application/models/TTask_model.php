<?php
class TTask_model extends CI_Model{
    
    public function check_ispmodules(){
        $moduleArr = array();
        $query = $this->db->query("SELECT module FROM sht_isp_admin_modules WHERE isp_uid='".ISPID."' AND status='1' AND (module='1' OR module='7')");
        if($query->num_rows() > 0){
            foreach($query->result() as $mobj){
                $moduleArr[] = $mobj->module;
            }
        }
        if(count($moduleArr) > 0){
            if((count($moduleArr) == 1) && (in_array('7' , $moduleArr))){
                redirect(base_url().'publicwifi');
            }
        }
    }
    
    public function isp_license_data(){
        $wallet_amt = 0; $passbook_amt = 0;
        $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='".ISPID."' AND is_paid='1'");
        if($walletQ->num_rows() > 0){
            $wallet_amt = $walletQ->row()->wallet_amt;
        }
        
        $passbookQ = $this->db->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost FROM sht_isp_passbook WHERE isp_uid='".ISPID."'");
        if($passbookQ->num_rows() > 0){
            $passrowdata = $passbookQ->row();
            $passbook_amt = $passrowdata->activeusers_cost;
        }
        $balanceamt = $wallet_amt - $passbook_amt;
        $ispcodet = $this->countrydetails();
        $cost_per_user = $ispcodet['cost_per_user'];
        if($balanceamt < $cost_per_user){
            return 0;
        }else{
            return 1;
        }
    }
    
    public function countrydetails(){
        $data = array();
        $ispcountryQ = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".ISPID."'");
        if($ispcountryQ->num_rows() > 0){
            $crowdata = $ispcountryQ->row();
            $countryid = $crowdata->country_id;
            
            $data['countryid'] = $countryid;
            $countryQ = $this->db->query("SELECT * FROM sht_countries WHERE id='".$countryid."'");
            if($countryQ->num_rows() > 0){
                $rowdata = $countryQ->row();
                $currid = $rowdata->currency_id;
                
                $currencyQ = $this->db->get_where('sht_currency', array('currency_id' => $currid));
                if($currencyQ->num_rows() > 0){
                    $currobj = $currencyQ->row();
                    $currsymbol = $currobj->currency_symbol;
                    $data['currency'] = $currsymbol;
                }
                $data['demo_cost'] = $rowdata->demo_cost;
                $data['cost_per_user'] = $rowdata->cost_per_user;
                $data['cost_per_location'] = $rowdata->cost_per_location;
            }
        }
        return $data;
    }
    
    public function get_ispdetail_info(){
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        $query = $this->db->query("select logo_image from sht_isp_detail where status='1' AND isp_uid='".$isp_uid."'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return $rowdata->logo_image;
        }else{
            return  0;
        }
    }
    
    public function leftnav_permission() {
        $userid = $this->session->userdata['isp_session']['userid'];
        $query = $this->db->query("select id,super_admin,dept_id from sht_isp_users where id='" . $userid . "'");
        $tabarr = array();
        $rowarr = $query->row_array();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        $deptid = $rowarr['dept_id'];
        if ($sessiondata['super_admin'] == 1) {
            $query = $this->db->query("select slug from sht_tab_menu where status='1'");
            foreach ($query->result() as $val) {
                $tabarr[$val->slug] = 0;
            }
        } else {
            $query1 = $this->db->query("SELECT sip.is_hide,stm.slug FROM sht_isp_permission sip INNER JOIN sht_tab_menu stm ON (stm.id=sip.tabid) WHERE isp_deptid='" . $deptid . "' ");
            foreach ($query1->result() as $val) {
                $tabarr[$val->slug] = ($sessiondata['super_admin'] == 1) ? 0 : $val->is_hide;
            }
        }
        //  echo $deptid."::".$tabid;
      
        $tabarr['PUBLICWIFI']=$this->isp_publicwifi_perm();
        return $tabarr;
    }
    
    public function isp_publicwifi_perm(){
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        $query=$this->db->query("select id from sht_isp_admin_modules where isp_uid='".$isp_uid."' and module='7' and status='1'");
        //echo $this->db->last_query();die;
        if($query->num_rows()>0){
            return 1;
        }else{
            return 0;
        }
    }
    public function ticket_sorters_list($userid=''){
        $gen = '';
        $sessiondata = $this->session->userdata('isp_session');
	$isp_uid = $sessiondata['isp_uid'];
        $tktsortQ = $this->db->query("SELECT tb1.id, tb1.name, tb2.dept_name FROM sht_isp_users as tb1 INNER JOIN sht_department as tb2 ON(tb1.dept_id=tb2.id) WHERE tb1.status='1' AND tb1.is_deleted='0' AND tb2.status='1' AND tb2.is_deleted='0' AND tb1.isp_uid='".$isp_uid."'");		
	if($tktsortQ->num_rows() > 0){
	    foreach($tktsortQ->result() as $listobj){
		$sel = '';
		if($userid == $listobj->id){
		    $sel = 'selected="selected"';
		}
		$gen .= '<option value="'.$listobj->id.'" '.$sel.'>'.$listobj->name.' ('.$listobj->dept_name.')</option>';
	    }
	}
        return $gen;
    }
    public function add_task(){
        $sessiondata = $this->session->userdata('isp_session');
	$isp_uid = $sessiondata['isp_uid'];
        $super_admin = $sessiondata['super_admin'];
        $added_by = $sessiondata['userid'];
        if($super_admin == 1){
            $added_by = 'SuperAdmin';
        }
        
        $postdata = $this->input->post();
        $geoaddress_searchtype = $postdata['geoaddress_searchtype'];
        if($geoaddress_searchtype == 'bylatlng'){
            $geoaddress = trim($this->input->post('geoaddress'));
            $place_id = trim($this->input->post('geoplaceid'));
            $latitude = trim($this->input->post('geolat'));
            $longitude = trim($this->input->post('geolong'));
        }else{
            $geoaddress = trim($this->input->post('geoaddr'));
            $place_id = trim($this->input->post('placeid'));
            $latitude = trim($this->input->post('lat'));
            $longitude = trim($this->input->post('long'));
        }
        $task_array = array(
            'isp_uid' => $isp_uid,
            'task_name' => $postdata['task_name'],
            'task_description' => $postdata['task_description'],
            'task_assignby' => $added_by,
            'task_assignto' => $postdata['task_assignto'],
            'task_priority' => $postdata['task_priority'],
            'task_assign_datetime' => $postdata['task_assign_datetime'],
            'geoaddress_searchtype' => $postdata['geoaddress_searchtype'],
            'geoaddr' => $geoaddress,
            'placeid' => $place_id,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'task_status' => '0',
            'added_on' => date('Y-m-d H:i:s')
        );
        
        $taskdetails = array('task_name' => $postdata['task_name'], 'geoaddr' => $geoaddress, 'placeid' => $place_id, 'latitude' => $latitude, 'longitude' => $longitude);
        $this->notify_model->teamtaskassign_alert($postdata['task_assignto'], $taskdetails);
        
        $this->db->insert('sht_team_task', $task_array);
        echo json_encode(true);
    }
    
    public function tat_days($issue_date){ //turnaround time
        $date=strtotime($issue_date);//Converted to a PHP date (a second count)
        
        //Calculate difference
        $diff=time() - $date;//time returns current time in seconds
        $days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
        $hours=round(($diff-$days*60*60*24)/(60*60));
        return "$days day $hours hours";
    }

    public function tkt_sorter_username($ispid){
        $name = '';
        $query = $this->db->query("SELECT name FROM sht_isp_users WHERE id='".$ispid."' AND status='1' AND is_deleted='0'");
        $exists = $query->num_rows();
        if($exists > 0){
            $name = $query->row()->name;
        }
        return $name;
    }
    public function open_tasklist(){
        $sessiondata = $this->session->userdata('isp_session');
	$isp_uid = $sessiondata['isp_uid'];
        $data = array(); $gen = '';
        $tasklistQ = $this->db->query("SELECT * FROM sht_team_task WHERE isp_uid='".$isp_uid."' AND task_status != '2' ORDER BY id DESC");
        if($tasklistQ->num_rows() > 0){
            $i = 1;
            foreach($tasklistQ->result() as $taskobj){
                $task_assign_to = $this->tkt_sorter_username($taskobj->task_assignto);
                $task_assigndt = date('d-m-Y H:i:s', strtotime($taskobj->task_assign_datetime));
                $tat_days = $this->tat_days($taskobj->added_on);
                $sel1 = ''; $sel2=''; $sel3 = '';
                if($taskobj->task_status == '0'){
		    $sel1 = 'selected="selected"';
		}elseif($taskobj->task_status == '2'){
		    $sel2 = 'selected="selected"';
		}
                /*$taskstatus = '
                <select id="changetktstatus" style="width:50%;box-shadow:none;margin:0px">
		    <option value="0" '.$sel1.' data-tktid="'.$taskobj->id.'">Open</option>
		    <option value="1" '.$sel2.' data-tktid="'.$taskobj->id.'">Pending</option>
		    <option value="2" data-tktid="'.$taskobj->id.'">Closed</option>
		</select>
                ';*/
                if($taskobj->task_status == '0'){
		    $taskstatus = 'Open';
		}elseif($taskobj->task_status == '1'){
		    $taskstatus = 'Pending';
		}elseif($taskobj->task_status == '2'){
		    $taskstatus = 'Closed';
		}
                
                $added_by = $taskobj->task_assignby;
                if($added_by != 'SuperAdmin'){
                    $teamQ = $this->db->query("SELECT name, lastname FROM sht_isp_users WHERE id='".$taskobj->task_assignby."'");
                    if($teamQ->num_rows() > 0){
                        $team_rowdata = $teamQ->row();
                        $added_by = $team_rowdata->name. " " .$team_rowdata->lastname; 
                    }else{
                        $added_by = 'TeamMember';
                    }
                }
                
                $gen .= '<tr><td>'.$i.'</td><td>'.$taskobj->task_name.'</td><td>'.$added_by.'</td><td>'.$task_assign_to.'</td><td>'.ucwords($taskobj->task_priority).'</td><td>'.$task_assigndt.'</td><td>'.$tat_days.'</td><td>'.$taskstatus.'</td><td colspan="2"><a href="javascript:void(0)" onclick="showedittaskModal('.$taskobj->id.')" title="Manage Task"><i class="fa fa-odnoklassniki fa-lg" aria-hidden="true"></i></a></td></tr>';
                $i++;
            }
        }else{
            $gen .= '<tr><td colspan="9" style="text-align:center">No Pending Task !!</td></tr>';
        }
        
        $data['task_list'] = $gen;
        echo json_encode($data);
    }
    
    public function taskdetails(){
        $sessiondata = $this->session->userdata('isp_session');
	$isp_uid = $sessiondata['isp_uid'];
        $taskid = $this->input->post('taskid');
        $data = array(); $gen = '';
        $tasklistQ = $this->db->query("SELECT * FROM sht_team_task WHERE isp_uid='".$isp_uid."' AND id= '".$taskid."'");
        if($tasklistQ->num_rows() > 0){
            $data = $tasklistQ->row_array();
            $data['task_assign_datetimeformat'] = date('d-m-Y H:i:s', strtotime($data['task_assign_datetime']));
            $data['task_assignto'] = $this->ticket_sorters_list($data['task_assignto']);
        }
        
        echo json_encode($data);
    }
    
    public function edit_task(){
        $sessiondata = $this->session->userdata('isp_session');
	$isp_uid = $sessiondata['isp_uid'];
        $postdata = $this->input->post();
        $geoaddress_searchtype = $postdata['geoaddress_searchtype'];
        if($geoaddress_searchtype == 'bylatlng'){
            $geoaddress = trim($this->input->post('geoaddress'));
            $place_id = trim($this->input->post('geoplaceid'));
            $latitude = trim($this->input->post('geolat'));
            $longitude = trim($this->input->post('geolong'));
        }else{
            $geoaddress = trim($this->input->post('geoaddr'));
            $place_id = trim($this->input->post('placeid'));
            $latitude = trim($this->input->post('lat'));
            $longitude = trim($this->input->post('long'));
        }
        
        $data = array();
        $taskid = $postdata['taskid_foredit'];
        $tasklistQ = $this->db->query("SELECT * FROM sht_team_task WHERE isp_uid='".$isp_uid."' AND id= '".$taskid."'");
        if($tasklistQ->num_rows() > 0){
            $data = $tasklistQ->row_array();
        }
        
        $task_array = array(
            'isp_uid' => $isp_uid,
            'task_name' => $postdata['task_name'],
            'task_description' => $postdata['task_description'],
            'task_assignto' => $postdata['task_assignto'],
            'task_priority' => $postdata['task_priority'],
            'task_assign_datetime' => $postdata['task_assign_datetime'],
            'geoaddress_searchtype' => $postdata['geoaddress_searchtype'],
            'geoaddr' => $geoaddress,
            'placeid' => $place_id,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'task_status' => $postdata['edittask_status'],
            'added_on' => date('Y-m-d H:i:s')
        );
        
        if($data['task_assignto'] != $postdata['task_assignto']){
            $taskdetails = array('task_name' => $postdata['task_name'], 'geoaddr' => $geoaddress, 'placeid' => $place_id, 'latitude' => $latitude, 'longitude' => $longitude);
            $this->notify_model->teamtaskassign_alert($postdata['task_assignto'], $taskdetails);
            
            $detaskdetails = array('task_name' => $postdata['task_name']);
            $this->notify_model->teamtask_deassign_alert($data['task_assignto'], $detaskdetails);
        }
        elseif($data['geoaddr'] != $postdata['geoaddr']){
            
        }
        
        
        if($taskid != ''){
            $this->db->update('sht_team_task', $task_array, array('id' => $taskid));
        }
        echo json_encode(true);
    }
    
    public function addtask_comment(){
        $data = array();
        $sessiondata = $this->session->userdata('isp_session');
	$isp_uid = $sessiondata['isp_uid'];
        $super_admin = $sessiondata['super_admin'];
        $userid = $sessiondata['userid'];
        if($super_admin == 1){
            $added_by = 'SuperAdmin';
        }else{
            $teamQ = $this->db->query("SELECT name, lastname FROM sht_isp_users WHERE id='".$userid."'");
            if($teamQ->num_rows() > 0){
                $team_rowdata = $teamQ->row();
                $added_by = $team_rowdata->name. " " .$team_rowdata->lastname; 
            }else{
                $added_by = 'TeamMember';
            }
        }
        $taskid = $this->input->post('taskid_foredit');
        $task_comment = $this->input->post('task_comment');
        $this->db->insert('sht_team_task_summary', array('task_id' => $taskid, 'comment' => $task_comment, 'comment_addedby' => $added_by, 'added_on' => date('Y-m-d H:i:s')));
        
        $gen = '';
        $query = $this->db->query("SELECT * FROM sht_team_task_summary WHERE task_id='".$taskid."' ORDER BY added_on DESC");
        if($query->num_rows() > 0){
            foreach($query->result() as $taskobj){
                $gen .= '<li>'.$taskobj->comment.' :by '.$added_by.' :on '.$taskobj->added_on.'</li>';
            }
        }
        
        $data['task_summary'] = $gen;
        echo json_encode($data);
    }
    
    public function showtask_comment(){
        $gen = ''; $data = array();
        $taskid = $this->input->post('taskid_foredit');
        $query = $this->db->query("SELECT * FROM sht_team_task_summary WHERE task_id='".$taskid."' ORDER BY added_on DESC");
        if($query->num_rows() > 0){
            foreach($query->result() as $taskobj){
                $gen .= '<li>'.$taskobj->comment.' :by '.$taskobj->comment_addedby.' :on '.$taskobj->added_on.'</li>';
            }
        }else{
            $gen .= '<li style="color: #c1c1c1">No comments added yet!!</li>';
        }
        
        $data['task_summary'] = $gen;
        echo json_encode($data);
    }
    
    public function filtertask_listing(){
        $sessiondata = $this->session->userdata('isp_session');
	$isp_uid = $sessiondata['isp_uid'];
        
        $team_membr = $this->input->post('team_membr');
	$taskstatus = $this->input->post('taskstatus');
	$priority = $this->input->post('priority');
        
        $data = array(); $gen = '';
        
        $where = '1';
        if($team_membr != ''){
            $where .= " AND task_assignto = '".$team_membr."' ";
        }
        if($taskstatus != ''){
            $where .= " AND task_status = '".$taskstatus."' ";
        }
        if($priority != ''){
            $where .= " AND task_priority = '".$priority."' ";
        }
        $tasklistQ = $this->db->query("SELECT * FROM sht_team_task WHERE $where AND isp_uid='".$isp_uid."' ORDER BY id DESC");
        if($tasklistQ->num_rows() > 0){
            $i = 1;
            foreach($tasklistQ->result() as $taskobj){
                $task_assign_to = $this->tkt_sorter_username($taskobj->task_assignto);
                $task_assigndt = date('d-m-Y H:i:s', strtotime($taskobj->task_assign_datetime));
                $tat_days = $this->tat_days($taskobj->added_on);
                $sel1 = ''; $sel2=''; $sel3 = '';
                if($taskobj->task_status == '0'){
		    $sel1 = 'selected="selected"';
		}elseif($taskobj->task_status == '2'){
		    $sel2 = 'selected="selected"';
		}
                /*$taskstatus = '
                <select id="changetktstatus" style="width:50%;box-shadow:none;margin:0px">
		    <option value="0" '.$sel1.' data-tktid="'.$taskobj->id.'">Open</option>
		    <option value="1" '.$sel2.' data-tktid="'.$taskobj->id.'">Pending</option>
		    <option value="2" data-tktid="'.$taskobj->id.'">Closed</option>
		</select>
                ';*/
                if($taskobj->task_status == '0'){
		    $taskstatus = 'Open';
		}elseif($taskobj->task_status == '1'){
		    $taskstatus = 'Pending';
		}elseif($taskobj->task_status == '2'){
		    $taskstatus = 'Closed';
		}
                
                $added_by = $taskobj->task_assignby;
                if($added_by != 'SuperAdmin'){
                    $teamQ = $this->db->query("SELECT name, lastname FROM sht_isp_users WHERE id='".$taskobj->task_assignby."'");
                    if($teamQ->num_rows() > 0){
                        $team_rowdata = $teamQ->row();
                        $added_by = $team_rowdata->name. " " .$team_rowdata->lastname; 
                    }else{
                        $added_by = 'TeamMember';
                    }
                }
                
                $gen .= '<tr><td>'.$i.'</td><td>'.$taskobj->task_name.'</td><td>'.$added_by.'</td><td>'.$task_assign_to.'</td><td>'.ucwords($taskobj->task_priority).'</td><td>'.$task_assigndt.'</td><td>'.$tat_days.'</td><td>'.$taskstatus.'</td><td colspan="2"><a href="javascript:void(0)" onclick="showedittaskModal('.$taskobj->id.')" title="Manage Task"><i class="fa fa-odnoklassniki fa-lg" aria-hidden="true"></i></a></td></tr>';
                $i++;
            }
        }else{
            $gen .= '<tr><td colspan="9" style="text-align:center">No Task Found !!</td></tr>';
        }
        
        $data['task_list'] = $gen;
        echo json_encode($data);
    }


}

?>