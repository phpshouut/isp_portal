<?php
class Login_model extends CI_Model{
    
	/*
	public function check_credentials($username, $password){
	    $query = $this->db->query("SELECT salt FROM sht_isp_users WHERE (username= '".$username."' OR email= '".$username." ') AND status='1' AND is_deleted='0'");
	    $exists = $query->num_rows();
	    if($exists > 0){
		$salt = $query->row()->salt;
		$entered_password = md5($password.$salt);
		
		$authenQuery = $this->db->query("SELECT * FROM sht_isp_users WHERE (username= '".$username."' OR email= '".$username."') AND password='".$entered_password."' AND status='1' AND is_deleted='0'");
		//echo $this->db->last_query();die;
                
                $validate = $authenQuery->num_rows();
		if($validate == 1){
			return $authenQuery->result();
		}
		
	    }else{
		return false;
	    }
	}
	*/
	public function check_terms_condition_read(){
		$isp_uid = 0;
		if(defined('ISPID')){
			$isp_uid = ISPID;
		}
		$is_check = 1;
		$query = $this->db->query("select is_terms_check from sht_isp_admin where isp_uid = '$isp_uid'");
		if($query->num_rows() > 0){
			$row = $query->row_array();
			$is_check = $row['is_terms_check'];
		}
		return $is_check;
	}
	
	public function check_terms_condition_update(){
		$isp_uid = 0;
		if(defined('ISPID')){
			$isp_uid = ISPID;
		}
		$query = $this->db->query("update sht_isp_admin set is_terms_check = '1' where isp_uid = '$isp_uid'");
	}
	public function get_isplogo(){
		$isp_uid = 0;
		if(defined('ISPID')){
			$isp_uid = ISPID;
		}
		$query = $this->db->query("select small_image from sht_isp_detail where status='1' AND isp_uid='".$isp_uid."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			return $rowdata->small_image;
		}else{
			return  0;
		}
		
        }
	
	public function isp_license_data(){
		$wallet_amt = 0; $passbook_amt = 0;
		$walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='".ISPID."' AND is_paid='1'");
		if($walletQ->num_rows() > 0){
		    $wallet_amt = $walletQ->row()->wallet_amt;
		}
		
		$passbookQ = $this->db->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost FROM sht_isp_passbook WHERE isp_uid='".ISPID."'");
		if($passbookQ->num_rows() > 0){
		    $passrowdata = $passbookQ->row();
		    $passbook_amt = $passrowdata->activeusers_cost;
		}
		$balanceamt = $wallet_amt - $passbook_amt;
		if($balanceamt < 0){
			return 0;
		}else{
			return 1;
		}
	}
	
	public function check_credentials($username, $password){
		$entered_password = md5($password);
		$authenQuery = $this->db->query("SELECT * FROM sht_isp_admin WHERE email= '".$username."' AND password='".$entered_password."' AND is_activated='1' AND is_deleted='0' AND (isp_uid='".ISPID."' or parent_isp='".ISPID."')");
		//echo $this->db->last_query();die;
	        $validate = $authenQuery->num_rows();
		if($validate == 1){
			return $authenQuery->result();
		}else{
			$query = $this->db->query("SELECT salt FROM sht_isp_users WHERE (username= '".$username."' OR email= '".$username." ') AND status='1' AND is_deleted='0' AND isp_uid='".ISPID."'");
			$exists = $query->num_rows();
			if($exists > 0){
			    $salt = $query->row()->salt;
			    $entered_password = md5($password.$salt);
			    
			    $authenQuery = $this->db->query("SELECT * FROM sht_isp_users WHERE (username= '".$username."' OR email= '".$username."') AND password='".$entered_password."' AND status='1' AND is_deleted='0' AND isp_uid='".ISPID."'");
			    //echo $this->db->last_query();die;
			    
			    $validate = $authenQuery->num_rows();
			    if($validate == 1){
				return $authenQuery->result();
			    }
			    
			}else{
			    $supquery = $this->db->query("SELECT salt FROM sht_isp_public_users WHERE (username= '".$username."' OR email= '".$username." ') AND status='1' AND is_deleted='0' and isp_uid = '".ISPID."'");
			    $exists = $supquery->num_rows();
			    if($exists > 0){
				$salt = $supquery->row()->salt;
				$entered_password = md5($password.$salt);
				
				$authenQuery = $this->db->query("SELECT * FROM sht_isp_public_users WHERE (username= '".$username."' OR email= '".$username."') AND password='".$entered_password."' AND status='1' AND is_deleted='0' and isp_uid = '".ISPID."'");
				//echo $this->db->last_query();die;
				
				$validate = $authenQuery->num_rows();
				if($validate == 1){
				    return $authenQuery->result();
				}
				
			    }else{
				return false;
			    }
			}
		}
	}
	
	public function check_ispmodules(){
		$query = $this->db->query("SELECT module FROM sht_isp_admin_modules WHERE isp_uid='".ISPID."' AND status='1' AND (module='1' OR module='7')");
		if($query->num_rows() == 2){
			return 1;
		}else{
			$moduleid = $query->row()->module;
			if($moduleid == '1'){
				return 1;
			}else{
				return 2;
			}
		}
		
		
	}
	
	 public function get_isp_name(){
	  $isp_uid = 0;
		if(defined('ISPID')){
			$isp_uid = ISPID;
		}
		  $data = array();
		  $isp_name = '';
		  $fevicon_icon = '';
		  $query = $this->db->query("select isp_name,fevicon_icon from sht_isp_admin where isp_uid = '$isp_uid'");
		  if($query->num_rows() > 0){
			   $row = $query->row_array();
			   $isp_name = $row['isp_name'];
			   $fevicon_icon = $row['fevicon_icon'];
		  }
		  $data['isp_name'] = $isp_name;
		  $data['fevicon_icon'] = $fevicon_icon;
		  return $data;
     }
    
}


?>