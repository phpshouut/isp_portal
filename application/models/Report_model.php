<?php

class Report_model extends CI_Model
{
    
    public function __construct(){
        parent::__construct();
    }
    
    public function persp_user(){
        //to check if file is getting uploaded
        $data     = array();
        $cond     = "";
        $postdata = $this->input->post();
        $persparr = array();
        $con      = $this->user_department_cond();
        $usercond = "";
        if ($con != '') {
            $usercond .= $con;
        }
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        $ispcond     = '';
        $ispcond     = " and su.isp_uid='" . $isp_uid . "'";
        
        
        $countq   = $this->db->query("SELECT COUNT(su.id) AS inactivecount,(SELECT COUNT(su.id) FROM `sht_subscriber` su WHERE su.usertype='1' and su.status='1' and su.is_deleted='0' {$usercond} {$ispcond}) AS leadcount,
(SELECT COUNT(su.id) FROM `sht_subscriber` su WHERE su.usertype='2' and su.status='1' and su.is_deleted='0' {$usercond} {$ispcond}) AS enquirycount FROM `sht_users` su WHERE su.enableuser='0' and su.inactivate_type= '' {$usercond} {$ispcond}");
        $countarr = $countq->row_array();
        
        //  echo $this->db->last_query()."<br>";
        
        
        $query    = $this->db->query("SELECT su.id,su.`first_name` as firstname,su.`last_name` as lastname,su.`email`,su.`phone` as mobile,su.usertype FROM `sht_subscriber` su where su.status='1' and su.is_deleted='0' {$usercond} {$ispcond}");
        //  echo $this->db->last_query()."<br>";
        $persparr = array();
        $leadarr  = array();
        foreach ($query->result() as $val) {
            $persparr[] = $val;
        }
        
        $queryinact = $this->db->query("SELECT su.id,su.`firstname`,su.`lastname`,su.`email`,su.`mobile`,sap.kyc_details FROM `sht_users` su INNER JOIN `sht_subscriber_activation_panel` sap ON (su.id=sap.subscriber_id) WHERE su.enableuser='0' and su.inactivate_type= '' $usercond {$ispcond}");
        //echo $this->db->last_query(); //die;
        //echo $this->db->last_query()."<br>";
        foreach ($queryinact->result() as $valinactuser) {
            $persparr[] = $valinactuser;
        }
        $data['count'] = $countarr;
        $data['list']  = $persparr;
        
        
        //echo "<pre>"; print_R($data); die;
        
        return $data;
    }
    
    public function get_franchise($isp_uid){
        $data                = array();
        $ispquery            = $this->db->query("select isp_uid,isp_name from `sht_isp_admin` where isp_uid='" . $isp_uid . "'  ");
        $rowarr              = $ispquery->row_array();
        $data[0]['isp_uid']  = $rowarr['isp_uid'];
        $data[0]['isp_name'] = 'My User';
        
        $query = $this->db->query("select isp_uid,isp_name from `sht_isp_admin` where is_activated='1' and is_deleted='0' and
                                parent_isp='" . $isp_uid . "'");
        $i     = 1;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $valf) {
                $data[$i]['isp_uid']  = $valf->isp_uid;
                $data[$i]['isp_name'] = $valf->isp_name;
                $i++;
            }
        }
        return $data;
    }
    
    public function persp_user_filter(){
        $postdata    = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        $ispcond     = '';
        $ispcond     = " and su.isp_uid='" . $isp_uid . "'";
        $persparr    = array();
        $data        = array();
        $con         = $this->user_department_cond();
        $usercond    = "";
        if ($con != '') {
            $usercond .= $con;
        }
        if ($postdata['filter'] == "all" || $postdata['filter'] == "lead" || $postdata['filter'] == "enquiry") {
            if ($postdata['filter'] == "all") {
                $cond = "where su.status='1' and su.is_deleted='0'";
            } else {
                $usertype = ($postdata['filter'] == "lead") ? 1 : 2;
                $cond     = "where su.status='1' and su.is_deleted='0' and usertype={$usertype}";
            }
            
            
            
            $query   = $this->db->query("SELECT su.id,`first_name` as firstname,su.`last_name` as lastname,su.`email`,su.`phone` as mobile,su.usertype FROM `sht_subscriber` su {$cond} $usercond $ispcond");
            //   echo $this->db->last_query()."<br>";
            $leadarr = array();
            foreach ($query->result() as $val) {
                $persparr[] = $val;
            }
            
        }
        if ($postdata['filter'] == "all" || $postdata['filter'] == "inactive") {
            $queryinact = $this->db->query("SELECT su.id,su.`firstname`,su.`lastname`,su.`email`,su.`mobile`,sap.kyc_details FROM `sht_users` su INNER JOIN `sht_subscriber_activation_panel` sap ON (su.id=sap.subscriber_id) WHERE enableuser='0' and su.inactivate_type= '' $usercond $ispcond");
            
            //   echo $this->db->last_query()."<br>";
            foreach ($queryinact->result() as $valinactuser) {
                $persparr[] = $valinactuser;
            }
        }
        $countq   = $this->db->query("SELECT COUNT(su.id) AS inactivecount,(SELECT COUNT(su.id) FROM `sht_subscriber` su WHERE su.usertype='1' and su.status='1' and su.is_deleted='0' {$usercond} {$ispcond}) AS leadcount,
(SELECT COUNT(su.id) FROM `sht_subscriber` su WHERE su.usertype='2' and su.status='1' and su.is_deleted='0' {$usercond} {$ispcond}) AS enquirycount FROM `sht_users` su WHERE su.enableuser='0' and su.inactivate_type= '' {$usercond} {$ispcond}");
        // echo $this->db->last_query()."<br>";
        $countarr = $countq->row_array();
        $i        = 1;
        $gen      = '';
        $gen .= '<table id="persp_table" class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>NAME</th>
                                                   <th>EMAIL</th>
                                                   <th>MOBILE</th>
                                                   <th>STAGE / STATUS</th>
                                                   <th>KYC STATUS</th>
                                                   <th class="mui--text-right">ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody >';
        if (count($persparr) > 0) {
            foreach ($persparr as $vallist) {
                
                $usertype = (isset($vallist->usertype)) ? ($vallist->usertype == 1) ? "Lead" : "Enquiry" : "Inactive";
                $kyc      = (isset($vallist->kyc_details)) ? ($vallist->kyc_details == 1) ? "Done" : "Not Done" : "-";
                $url      = (isset($vallist->usertype)) ? base_url() . "user/add/" . $vallist->id : base_url() . "user/edit/" . $vallist->id;
                
                
                $gen .= '
                <tr>
                    <td>' . $i . '.</td>
                    
                    <td><a href="' . $url . '">' . $vallist->firstname . ' ' . $vallist->lastname . '</a></td>
                    <td>' . $vallist->email . '</td>
                    <td> ' . $vallist->mobile . '</td>
                    <td>' . $usertype . '</td>
                                        <td>' . $kyc . '</td>';
                
                $gen .= ' <td><a href="' . $url . '">view</a></td>';
                
                
                
                $gen .= '</tr>
                ';
                $i++;
            }
            $gen .= ' </tbody>
                                          </table>';
        } else {
            $gen .= '<tr><td style="text-align:center" colspan="7"> No Result Found !!</td></tr> </tbody>
                                          </table>';
        }
        $data['result'] = 0;
        $data['count']  = $countarr;
        $data['html']   = $gen;
        
        echo json_encode($data);
    }
    
    public function active_user(){
        $this->benchmark->mark('code_start');
        $data           = array();
        $postdata       = $this->input->post();
        $strtlast30     = date('Y-m-d', strtotime('today - 30 days'));
        $enddate        = date('Y-m-d');
        $start180       = date('Y-m-d', strtotime('today - 210 days'));
        $enddate180     = date('Y-m-d', strtotime('today - 30 days'));
        $enddate180more = date('Y-m-d', strtotime('today - 180 days'));
        
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        $ispcond     = '';
        $ispcond     = " and su.isp_uid='" . $isp_uid . "'";
        
        $sczcond  = $this->user_department_cond();
        $query1   = $this->db->query("SELECT COUNT(su.id) AS totalcount,
        (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon between '" . $strtlast30 . "' and '" . $enddate . "' {$sczcond} {$ispcond})as last30count,
            (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon between '" . $start180 . "' and '" . $enddate180 . "' {$sczcond} {$ispcond})as last180count, 
            (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon < '" . $enddate180more . "' {$sczcond} {$ispcond})as more180count FROM `sht_users` su WHERE su.enableuser=1 AND expired=0 {$sczcond} {$ispcond}");
        //echo $this->db->last_query()."<br/>";//die;
        $countarr = $query1->row_array();
        
        $cond = "";
        if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "last30") {
            $cond .= "and su.createdon between '" . $strtlast30 . "' and '" . $enddate . "'";
        } else if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "last180") {
            $cond .= "and su.createdon between '" . $start180 . "' and '" . $enddate180 . "'";
        } else if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "more180") {
            $cond .= "and  su.createdon < '" . $enddate180more . "'";
        }
        //query to find acctive user 
        
        $Uquery   = $this->db->query("SELECT su.id,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname FROM sht_users su
        INNER JOIN sht_services ss ON (su.baseplanid=ss.srvid) 
        INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
        INNER JOIN `sht_zones` sz ON (sz.id=su.zone) WHERE su.enableuser=1 AND expired=0 {$sczcond} {$cond} {$ispcond}");
        //echo $this->db->last_query()."<br/>";
        $userarr  = array();
        $userdarr = array();
        foreach ($Uquery->result() as $val) {
            $userdarr[$val->uid] = $val;
            $userarr[]           = $val->uid;
        }
        //query to get online uers
        $activeuserid = "";
        if (!empty($userarr)) {
            
            
            $userids     = '"' . implode('", "', $userarr) . '"';
            $onlinearr   = array();
            $onlinequery = $this->db->query("SELECT `username` FROM `radacct` WHERE `acctstoptime` IS NULL AND `username` IN ($userids) group by username");
            // echo "===>>".$this->db->last_query();die;
            // echo $this->db->last_query()."<br/>";
            foreach ($onlinequery->result() as $valon) {
                $onlinearr[] = $valon->username;
            }
            
            //query to get user top associated
            $topupq   = $this->db->query("SELECT `uid`,COUNT(DISTINCT(topup_id)) AS topupcount FROM `sht_usertopupassoc` sut
                INNER JOIN sht_services ss ON (ss.srvid=sut.topup_id)   WHERE sut.terminate=0 AND `uid` IN ($userids) GROUP BY sut.uid");
            // echo $this->db->last_query()."<br/>";
            $topupass = array();
            foreach ($topupq->result() as $valtop) {
                $topupass[$valtop->uid] = $valtop->topupcount;
            }
            
            //function to get dues user list
            $duearr = $this->dues_list($userids);
            
            //  echo "<pre>online arr";print_R($onlinearr);
            //  echo "<pre>topuparr";print_R($topupass);
            //  echo "<pre>duearr";print_R($duearr);
            //  echo "<pre>userarr";print_R($userdarr);
            
            $fnlarr['online']    = array();
            $fnlarr['offline']   = array();
            $fnlarr['suspended'] = array();
            $i                   = 0;
            foreach ($userdarr as $key => $valu) {
                if (in_array($key, $onlinearr)) {
                    $fnlarr['online'][$i]             = $valu;
                    $fnlarr['online'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                    $fnlarr['online'][$i]->dues       = (array_key_exists($valu->uid, $duearr)) ? $duearr[$valu->uid] : "-";
                } else {
                    
                    if ($valu->inactivate_type == "suspended") {
                        $key = "suspended";
                    } else {
                        $key = "offline";
                    }
                    $fnlarr[$key][$i]             = $valu;
                    $fnlarr[$key][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                    $fnlarr[$key][$i]->dues       = (array_key_exists($valu->uid, $duearr)) ? $duearr[$valu->uid] : "-";
                }
                
                
                $i++;
            }
            
            
            if (isset($postdata['filter']) && $postdata['filter'] == "online") {
                $datarray = array();
                $datarray = $fnlarr['online'];
            } else if (isset($postdata['filter']) && $postdata['filter'] == "offline") {
                $datarray = array();
                $datarray = $fnlarr['offline'];
            } else if (isset($postdata['filter']) && $postdata['filter'] == "suspended") {
                $datarray = array();
                
                $datarray = $fnlarr['suspended'];
            } else {
                $datarray = array();
                $datarray = array_merge($fnlarr['online'], $fnlarr['offline']);
                $datarray = array_merge($datarray, $fnlarr['suspended']);
            }
            
            
            
            
            
            
            
            
            $i   = 1;
            $gen = '';
            $gen .= '<table id="activeuser_table" class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>NAME</th>
                                                   <th>UID</th>
                                                   <th>CITY</th>
                                                   <th>ZONE</th>
                                                   <th>PLAN</th>
                                                   <th>TOP UP PLAN</th>
                                                   <th class="mui--text-right">DUES</th>
                                                </tr>
                                             </thead>
                                             <tbody id="search_activegrid">';
            if (count($datarray) > 0) {
                
                foreach ($datarray as $vallist) {
                    
                    
                    $url = base_url() . "user/edit/" . $vallist->id;
                    
                    
                    $gen .= ' 
                <tr>
                    <td>' . $i . '.</td>
                    
                    <td><a href="' . $url . '">' . $vallist->firstname . ' ' . $vallist->lastname . '</a></td>
                    <td>' . $vallist->uid . '</td>
                    <td> ' . $vallist->city_name . '</td>
                    <td>' . $vallist->zone_name . '</td>
                                        <td>' . $vallist->srvname . '</td>' . '<td>' . $vallist->topupcount . '</td>' . ' <td>' . $vallist->dues . '</td>';
                    
                    
                    
                    
                    
                    $gen .= '</tr>
                ';
                    $i++;
                }
                
            } else {
                $gen .= '<tr><td style="text-align:center" colspan="8"> No Result Found !!</td></tr>';
            }
            $gen .= '</tbody>
                                          </table>';
            
            $data['html']           = $gen;
            $data['total_count']    = $countarr['totalcount'];
            $data['last30count']    = $countarr['last30count'];
            $data['last180count']   = $countarr['last180count'];
            $data['more180count']   = $countarr['more180count'];
            $data['online']         = count($fnlarr['online']);
            $data['offline']        = count($fnlarr['offline']);
            $data['suspended']      = count($fnlarr['suspended']);
            $data['totaloffsuspon'] = count($fnlarr['online']) + count($fnlarr['offline']) + count($fnlarr['suspended']);
        } else {
            $gen = '';
            $gen .= '<table id="activeuser_table" class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>NAME</th>
                                                   <th>UID</th>
                                                   <th>CITY</th>
                                                   <th>ZONE</th>
                                                   <th>PLAN</th>
                                                   <th>TOP UP PLAN</th>
                                                   <th class="mui--text-right">DUES</th>
                                                </tr>
                                             </thead>
                                             <tbody id="search_activegrid"><tr><td style="text-align:center" colspan="8"> No Result Found !!</td></tr></tbody></table>';
            $data['html']           = $gen;
            $data['total_count']    = $countarr['totalcount'];
            $data['last30count']    = $countarr['last30count'];
            $data['last180count']   = $countarr['last180count'];
            $data['more180count']   = $countarr['more180count'];
            $data['online']         = 0;
            $data['offline']        = 0;
            $data['suspended']      = 0;
            $data['totaloffsuspon'] = 0;
            
        }
        echo json_encode($data);
        //echo $this->db->last_query();
        die;
        
    }
    
    
    public function dues_list($uidcond){
        $passbookQ = $this->db->query("SELECT subscriber_uuid, SUM(plan_cost) AS expense_cost FROM sht_subscriber_passbook WHERE subscriber_uuid in($uidcond) GROUP BY `subscriber_uuid` ");
        $data      = array();
        if ($passbookQ->num_rows() > 0) {
            
            foreach ($passbookQ->result() as $pbobj) {
                
                $wallet_amt = 0;
                
                $passbook_amt = $pbobj->expense_cost;
                
                $uuid = $pbobj->subscriber_uuid;
                
                $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='" . $uuid . "'");
                
                if ($walletQ->num_rows() > 0) {
                    
                    $wallet_amt = $walletQ->row()->wallet_amt;
                    
                }
                
                $balanceamt = $wallet_amt - $passbook_amt;
                
                if ($balanceamt < 0) {
                    
                    $data[$pbobj->subscriber_uuid] = str_replace("-", "", $balanceamt);
                    
                }
                
            }
            
        }
        return $data;
    }
    
    
    
    public function dues_list_billing($uidcond){
        $passbookQ = $this->db->query("SELECT subscriber_uuid, SUM(plan_cost) AS expense_cost FROM sht_subscriber_passbook WHERE subscriber_uuid in($uidcond) GROUP BY `subscriber_uuid` ");
        $data      = array();
        // echo "<pre>";print_R($passbookQ->result() ); die;
        if ($passbookQ->num_rows() > 0) {
            
            foreach ($passbookQ->result() as $pbobj) {
                
                $wallet_amt = 0;
                
                $passbook_amt = $pbobj->expense_cost;
                
                $uuid = $pbobj->subscriber_uuid;
                
                $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='" . $uuid . "'");
                
                if ($walletQ->num_rows() > 0) {
                    
                    $wallet_amt = $walletQ->row()->wallet_amt;
                    
                }
                
                $balanceamt = $wallet_amt - $passbook_amt;
                
                
                
                $data[$pbobj->subscriber_uuid] = $balanceamt;
                
                
                
            }
            
        }
        return $data;
    }
    
    
    public function user_department_cond(){
        $sessiondata = $this->session->userdata('isp_session');
        $superadmin  = $sessiondata['super_admin'];
        $dept_id     = $sessiondata['dept_id'];
        $regiontype  = $this->plan_model->dept_region_type();
        $permicond   = '';
        $sczcond     = '';
        if ($regiontype == "region") {
            $dept_regionQ     = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='" . $dept_id . "' AND status='1' AND is_deleted='0'");
            $total_deptregion = $dept_regionQ->num_rows();
            if ($dept_regionQ->num_rows() > 0) {
                $c = 1;
                foreach ($dept_regionQ->result() as $deptobj) {
                    $stateid = $deptobj->state_id;
                    $cityid  = $deptobj->city_id;
                    $zoneid  = $deptobj->zone_id;
                    if ($cityid == 'all') {
                        $permicond .= " (state='" . $stateid . "') ";
                    } elseif ($zoneid == 'all') {
                        $permicond .= " (state='" . $stateid . "' AND city='" . $cityid . "') ";
                    } else {
                        $permicond .= " (state='" . $stateid . "' AND city='" . $cityid . "' AND zone='" . $zoneid . "') ";
                    }
                    if ($c != $total_deptregion) {
                        $permicond .= ' OR ';
                    }
                    $c++;
                }
            }
            if ($permicond != '') {
                $sczcond .= ' AND (' . $permicond . ')';
            }
        }
        return $sczcond;
    }
    public function gettkttype(){
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = $sessiondata['isp_uid'];
        $complaintarr = array();
        $tkttypeQ = $this->db->query("SELECT * FROM sht_tickets_type WHERE isp_uid='".$isp_uid."' ORDER BY id DESC");
	if($tkttypeQ->num_rows() > 0){
            foreach($tkttypeQ->result() as $tktobj){
                $complaintarr["$tktobj->ticket_type_value"] = $tktobj->ticket_type;
	    }
        }
        return $complaintarr;
    }
    public function compaint_request(){
        $postdata    = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $ispcond = '';
        $ispcond = " and su.isp_uid='" . $isp_uid . "'";
        
        $cond = '';
        if (isset($postdata['complaint_type']) && $postdata['complaint_type'] != 'all') {
            //  echo "sssss";
            $cond .= " and sst.ticket_type='" . $postdata['complaint_type'] . "'";
        }
        $sczcond    = $this->user_department_cond();
        $montharr   = $this->get_last_sixmonthwithyear();
        $monthlyarr = $this->get_last_sixmonth();
        //echo "<pre>"; print_R($monthlyarr); //die;
        $opentquery = $this->db->query("
SELECT YEAR(sst.`added_on`) AS Y, MONTH(sst.`added_on`) AS m,MONTHNAME(sst.`added_on`) AS mo, COUNT(sst.id) AS totalcount
FROM `sht_subscriber_tickets` sst
INNER JOIN `sht_users` su ON (su.uid=sst.`subscriber_uuid`) where 1 {$cond} {$sczcond} {$ispcond}
 and status='0' and ticket_assign_to='0' GROUP BY Y, m");
        //echo $this->db->last_query()."<br/>";
        $monthdata  = array();
        foreach ($monthlyarr as $val) {
            $monthdata[$val] = 0;
        }
        //foreach()
        foreach ($opentquery->result() as $openval) {
            //$data
            $monthval = $openval->m . " " . $openval->Y;
            if (in_array($monthval, $montharr)) {
                $monthdata[substr($openval->mo, 0, 3)] = (int) $openval->totalcount;
                //    $monthdata[substr($openval->mo,0,3)]=1;
            } else {
                $monthdata[substr($openval->mo, 0, 3)] = 0;
            }
            
            
        }
        $data['graph']            = $monthdata;
        /*$complaintarr             = array(
            "change_address" => "Address Change",
            "change_plan" => "Plan Change",
            "terminate_service" => "Terminate Service",
            "suspend_service" => "Suspend Service",
            "service_down" => "Service Down",
            "router_not_working" => "Router Issues",
            "other_request" => "Other Request"
        );*/
        $complaintarr = array();
        $tkttypeQ = $this->db->query("SELECT * FROM sht_tickets_type WHERE isp_uid='".$isp_uid."' ORDER BY id DESC");
	if($tkttypeQ->num_rows() > 0){
            foreach($tkttypeQ->result() as $tktobj){
                $complaintarr["$tktobj->ticket_type_value"] = $tktobj->ticket_type;
	    }
        }
        $ticketarr['open']        = array();
        $ticketarr['close']       = array();
        $ticketarr['in_progress'] = array();
        
        $queryticket = $this->db->query("
  SELECT su.id,su.uid,sst.`subscriber_uuid`,sst.status,sst.ticket_assign_to,sst.`ticket_type`,su.`firstname`,su.`lastname`,ss.srvname,sc.city_name,sz.zone_name FROM  sht_subscriber_tickets sst
  INNER JOIN sht_users su ON (su.uid=sst.subscriber_uuid)
  INNER JOIN sht_services ss ON (su.baseplanid=ss.srvid) 
        INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
        INNER JOIN `sht_zones` sz ON (sz.id=su.zone) where 1 {$cond} {$sczcond} {$ispcond} order by sst.added_on desc
              ");
        
        // echo $this->db->last_query()."<br/>";
        
        $i = 0;
        foreach ($queryticket->result() as $val) {
            if ($val->status == 0) {
                if ($val->ticket_assign_to == 0) {
                    $ticketarr['open'][$i] = $val;
                } else {
                    $ticketarr['in_progress'][$i] = $val;
                }
                
            } else {
                $ticketarr['close'][$i] = $val;
            }
            $i++;
        }
        //       echo "<pre>"; print_R($ticketarr);die;
        
        if (isset($postdata['filter']) && $postdata['filter'] == "open") {
            $datarray = array();
            $datarray = $ticketarr['open'];
        } else if (isset($postdata['filter']) && $postdata['filter'] == "close") {
            $datarray = array();
            $datarray = $ticketarr['close'];
        } else if (isset($postdata['filter']) && $postdata['filter'] == "in_progress") {
            $datarray = array();
            
            $datarray = $ticketarr['in_progress'];
        } else {
            $datarray = array();
            $datarray = array_merge($ticketarr['open'], $ticketarr['close']);
            $datarray = array_merge($datarray, $ticketarr['in_progress']);
        }
        $i   = 1;
        $gen = '';
        $gen .= '<table id="compuser_table" class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>UID</th>
                                                    <th>NAME</th>
                                                    <th>CITY</th>
                                                   <th>ZONE</th>
                                                   <th>PLAN</th>
                                                   <th>Complaint Type</th>
                                                  
                                                </tr>
                                             </thead>
                                             <tbody id="search_activegrid">';
        if (count($datarray) > 0) {
            
            foreach ($datarray as $vallist) {
                $url = base_url() . "user/edit/" . $vallist->id;
                $gen .= ' 
                <tr>
                    <td>' . $i . '.</td>
                    
                    <td><a href="' . $url . '">' . $vallist->firstname . ' ' . $vallist->lastname . '</a></td>
                    <td>' . $vallist->uid . '</td>
                    <td> ' . $vallist->city_name . '</td>
                    <td>' . $vallist->zone_name . '</td>
                                        <td>' . $vallist->srvname . '</td>' . '<td>' . $complaintarr[$vallist->ticket_type] . '</td>';
                $gen .= '</tr>
                ';
                $i++;
            }
            
        } else {
            $gen .= '<tr><td style="text-align:center" colspan="8"> No Result Found !!</td></tr>';
        }
        $gen .= '</tbody>
                                          </table>';
        
        $data['html']       = $gen;
        $data['openticket'] = count($ticketarr['open']);
        echo json_encode($data);
        
    }
    
    
    
    
    public function get_last_sixmonthwithyear(){
        $m = date('n');
        for ($i = 0; $i < 6; $i++) {
            $m_array[] = date('n Y', mktime(0, 0, 0, $m - $i, 15, date('Y')));
        }
        return $m_array;
    }
    
    
    
    
    
    
    public function get_last_sixmonth(){
        $m = date('n');
        for ($i = 0; $i < 6; $i++) {
            $m_array[] = date('M', mktime(0, 0, 0, $m - $i, 15, date('Y')));
        }
        return $m_array;
    }
    
    
    
    public function plan_topup_report(){
        $data           = array();
        $postdata       = $this->input->post();
        $strtlast30     = date('Y-m-d', strtotime('today - 30 days'));
        $enddate        = date('Y-m-d');
        $start180       = date('Y-m-d', strtotime('today - 210 days'));
        $enddate180     = date('Y-m-d', strtotime('today - 30 days'));
        $enddate180more = date('Y-m-d', strtotime('today - 180 days'));
        
        
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        $ispcond     = '';
        $ispcond     = " and su.isp_uid='" . $isp_uid . "'";
        
        $sczcond  = $this->user_department_cond();
        $query1   = $this->db->query("SELECT COUNT(su.id) AS totalcount,
        (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon between '" . $strtlast30 . "' and '" . $enddate . "' {$sczcond} {$ispcond})as last30count,
            (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon between '" . $start180 . "' and '" . $enddate180 . "' {$sczcond} {$ispcond})as last180count, 
            (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon < '" . $enddate180more . "' {$sczcond} {$ispcond})as more180count FROM `sht_users` su WHERE su.enableuser=1 AND expired=0 {$sczcond} {$ispcond}");
        //echo $this->db->last_query()."<br/>";//die;
        $countarr = $query1->row_array();
        
        $cond = "";
        if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "last30") {
            $cond .= "and su.createdon between '" . $strtlast30 . "' and '" . $enddate . "'";
        } else if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "last180") {
            $cond .= "and su.createdon between '" . $start180 . "' and '" . $enddate180 . "'";
        } else if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "more180") {
            $cond .= "and  su.createdon < '" . $enddate180more . "'";
        }
        //query to find acctive user 
        
        $Uquery   = $this->db->query("SELECT su.id,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname FROM sht_users su
        INNER JOIN sht_services ss ON (su.baseplanid=ss.srvid) 
        INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
        INNER JOIN `sht_zones` sz ON (sz.id=su.zone) WHERE su.enableuser=1 AND expired=0 {$sczcond} {$cond} {$ispcond}");
        //echo $this->db->last_query()."<br/>";
        $userarr  = array();
        $userdarr = array();
        foreach ($Uquery->result() as $val) {
            $userdarr[$val->uid] = $val;
            $userarr[]           = $val->uid;
        }
        //query to get online uers
        $activeuserid = "";
        if (!empty($userarr)) {
            
            
            $userids = '"' . implode('", "', $userarr) . '"';
            
            
            //query to get user top associated
            $topupq        = $this->db->query("SELECT count(sut.`uid`) as usercount,ss.srvname,ss.srvid,sut.topup_id FROM `sht_usertopupassoc` sut
                INNER JOIN sht_services ss ON (ss.srvid=sut.topup_id)   WHERE sut.terminate=0 AND `uid` IN ($userids) GROUP BY sut.topup_id order by usercount desc");
            //   echo $this->db->last_query()."<br/>";
            $topupass      = array();
            $planfilterarr = array();
            $i             = 0;
            foreach ($topupq->result() as $valtop) {
                
                $topupass[$valtop->srvname]                = (int) $valtop->usercount;
                $planfilterarr[$valtop->srvid . "::topup"] = $valtop->srvname . " (" . (int) $valtop->usercount . " Users)";
            }
            
            
            
            
            //query to get user top associated
            $planq   = $this->db->query("SELECT COUNT(su.`uid`) AS usercount,ss.srvname,ss.srvid FROM `sht_users` su
                INNER JOIN sht_services ss ON (ss.srvid=su.baseplanid)   WHERE  `uid` IN ($userids) GROUP BY su.`baseplanid` order by usercount desc");
            // echo $this->db->last_query()."<br/>";
            $planass = array();
            $i       = 0;
            foreach ($planq->result() as $valp) {
                
                $planass[$valp->srvname]                = (int) $valp->usercount;
                $planfilterarr[$valp->srvid . "::plan"] = $valp->srvname . " (" . (int) $valp->usercount . " Users) ";
                $i++;
            }
            
            $data['plangraph']    = $planass;
            $data['topupgraph']   = $topupass;
            $data['plandropdown'] = $planfilterarr;
            $datarray             = array();
            $datarray1            = array();
            $datarray2            = array();
            
            if (isset($postdata['planfilter']) && $postdata['planfilter'] == "plan") {
                $id = $postdata['id'];
                
                
                $dataquery = $this->db->query("SELECT su.id,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname FROM sht_users su
        INNER JOIN sht_services ss ON (su.baseplanid=ss.srvid) 
        INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
        INNER JOIN `sht_zones` sz ON (sz.id=su.zone) WHERE su.enableuser=1 AND expired=0 {$sczcond} {$cond} and su.baseplanid='" . $id . "'");
                //   echo $this->db->last_query()."<br/>";  
                $i         = 0;
                foreach ($dataquery->result() as $valp) {
                    $datarray[$i] = $valp;
                    $i++;
                }
                
                
            } else if (isset($postdata['planfilter']) && $postdata['planfilter'] == "topup") {
                $id        = $postdata['id'];
                $datarray  = array();
                $dataquery = $this->db->query(" SELECT su.id,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,sut.topup_id,
              su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname   FROM sht_usertopupassoc sut
               INNER JOIN  `sht_users` su ON (sut.uid=su.uid)
              INNER JOIN sht_services ss ON (sut.topup_id=ss.srvid) 
             INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
             INNER JOIN `sht_zones` sz ON (sz.id=su.zone)
             WHERE su.enableuser=1  AND expired=0 {$sczcond} {$cond} {$ispcond} and sut.topup_id='" . $id . "' GROUP BY sut.topup_id");
                $i         = 0;
                //  echo $this->db->last_query()."<br/>";
                foreach ($dataquery->result() as $valp) {
                    $datarray[$i] = $valp;
                    $i++;
                }
            } else {
                $dataquery = $this->db->query("SELECT su.id,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname FROM sht_users su
                INNER JOIN sht_services ss ON (su.baseplanid=ss.srvid) 
                INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
                INNER JOIN `sht_zones` sz ON (sz.id=su.zone) WHERE su.enableuser=1 AND expired=0 {$sczcond} {$cond} {$ispcond}");
                $i         = 0;
                //  echo $this->db->last_query()."<br/>";
                foreach ($dataquery->result() as $valp) {
                    $datarray1[$i] = $valp;
                    $i++;
                }
                
                $dataquery1 = $this->db->query(" SELECT su.id,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,sut.topup_id,
              su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname   FROM sht_usertopupassoc sut
               INNER JOIN  `sht_users` su ON (sut.uid=su.uid)
              INNER JOIN sht_services ss ON (sut.topup_id=ss.srvid) 
             INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
             INNER JOIN `sht_zones` sz ON (sz.id=su.zone)
             WHERE su.enableuser=1  AND expired=0 {$sczcond} {$cond} {$ispcond} GROUP BY sut.topup_id");
                //   echo $this->db->last_query()."<br/>";
                foreach ($dataquery1->result() as $valp2) {
                    $datarray2[$i] = $valp2;
                    $i++;
                }
                
                $datarray = array_merge($datarray1, $datarray2);
                
            }
            
            
            
            
            
            
            
            
            $i   = 1;
            $gen = '';
            $gen .= '<table id="planuser_table" class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>UID</th>
                                                   <th>NAME</th>
                                                  
                                                   <th>CITY</th>
                                                   <th>ZONE</th>
                                                   <th>PLAN</th>
                                                  
                                                </tr>
                                             </thead>
                                             <tbody id="search_activegrid">';
            if (count($datarray) > 0) {
                
                foreach ($datarray as $vallist) {
                    
                    
                    $url = base_url() . "user/edit/" . $vallist->id;
                    
                    
                    $gen .= ' 
                <tr>
                    <td>' . $i . '.</td>
                    <td>' . $vallist->uid . '</td>
                    <td><a href="' . $url . '">' . $vallist->firstname . ' ' . $vallist->lastname . '</a></td>
                    <td> ' . $vallist->city_name . '</td>
                    <td>' . $vallist->zone_name . '</td>
                                        <td>' . $vallist->srvname . '</td>';
                    
                    
                    
                    
                    
                    
                    $gen .= '</tr>
                ';
                    $i++;
                }
                
            } else {
                $gen .= '<tr><td style="text-align:center" colspan="8"> No Result Found !!</td></tr>';
            }
            $gen .= '</tbody>
                                          </table>';
            
            $data['html']         = $gen;
            $data['total_count']  = $countarr['totalcount'];
            $data['last30count']  = $countarr['last30count'];
            $data['last180count'] = $countarr['last180count'];
            $data['more180count'] = $countarr['more180count'];
            
            
            //  echo json_encode($data);
        } else {
            $gen = '';
            $gen .= '<table id="planuser_table" class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>UID</th>
                                                   <th>NAME</th>
                                                   <th>CITY</th>
                                                   <th>ZONE</th>
                                                   <th>PLAN</th>
                                                  
                                                </tr>
                                             </thead>
                                             <tbody id="search_activegrid"><tr><td style="text-align:center" colspan="8"> No Result Found !!</td></tr></tbody></table>';
            $data['html']         = $gen;
            $data['total_count']  = $countarr['totalcount'];
            $data['last30count']  = $countarr['last30count'];
            $data['last180count'] = $countarr['last180count'];
            $data['more180count'] = $countarr['more180count'];
            
            
        }
        
        echo json_encode($data);
        
    }
    
    
    public function billing_report(){
        $this->benchmark->mark('code_start');
        $data           = array();
        $postdata       = $this->input->post();
        $strtlast30     = date('Y-m-d', strtotime('today - 30 days'));
        $enddate        = date('Y-m-d');
        $start180       = date('Y-m-d', strtotime('today - 210 days'));
        $enddate180     = date('Y-m-d', strtotime('today - 30 days'));
        $enddate180more = date('Y-m-d', strtotime('today - 180 days'));
        
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        $ispcond     = '';
        $ispcond     = " and su.isp_uid='" . $isp_uid . "'";
        
        $sczcond  = $this->user_department_cond();
        $query1   = $this->db->query("SELECT COUNT(su.id) AS totalcount,
        (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon between '" . $strtlast30 . "' and '" . $enddate . "' {$sczcond} {$ispcond})as last30count,
            (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon between '" . $start180 . "' and '" . $enddate180 . "' {$sczcond} {$ispcond})as last180count, 
            (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon < '" . $enddate180more . "' {$sczcond} {$ispcond})as more180count FROM `sht_users` su WHERE su.enableuser=1 AND expired=0 {$sczcond} {$ispcond}");
        // echo $this->db->last_query()."<br/>";//die;
        $countarr = $query1->row_array();
        
        $cond = "";
        if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "last30") {
            $cond .= "and su.createdon between '" . $strtlast30 . "' and '" . $enddate . "'";
        } else if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "last180") {
            $cond .= "and su.createdon between '" . $start180 . "' and '" . $enddate180 . "'";
        } else if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "more180") {
            $cond .= "and  su.createdon < '" . $enddate180more . "'";
        }
        //query to find acctive user 
        
        $Uquery   = $this->db->query("SELECT su.id,su.expiration,spp.net_total,su.user_credit_limit,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname FROM sht_users su
        INNER JOIN sht_services ss ON (su.baseplanid=ss.srvid) 
         INNER JOIN sht_plan_pricing spp ON (spp.srvid=ss.srvid) 
        INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
        INNER JOIN `sht_zones` sz ON (sz.id=su.zone) WHERE su.enableuser=1 AND expired=0 {$sczcond} {$cond} {$ispcond}");
        // echo $this->db->last_query()."<br/>";
        $userarr  = array();
        $userdarr = array();
        foreach ($Uquery->result() as $val) {
            $userdarr[$val->uid] = $val;
            $usernet[]           = $val->net_total;
            $userarr[]           = $val->uid;
            $userarr[]           = $val->expiration;
        }
        $avgrevenue          = 0;
        //query to get online uers
        $activeuserid        = "";
        $fnlarr['100perc']   = array();
        $fnlarr['25perc']    = array();
        $fnlarr['50perc']    = array();
        $fnlarr['75perc']    = array();
        $fnlarr['90perc']    = array();
        $fnlarr['p500']      = array();
        $fnlarr['500p1000']  = array();
        $fnlarr['1000p2000'] = array();
        $fnlarr['p2000']     = array();
        $fnlarr['1day']      = array();
        $fnlarr['2day']      = array();
        $fnlarr['3day']      = array();
        if (!empty($userarr)) {
            
            $avgrevenue = array_sum($usernet) / count($userarr);
            $userids    = '"' . implode('", "', $userarr) . '"';
            
            
            //query to get user top associated
            $topupq   = $this->db->query("SELECT `uid`,COUNT(DISTINCT(topup_id)) AS topupcount FROM `sht_usertopupassoc` sut
                INNER JOIN sht_services ss ON (ss.srvid=sut.topup_id)   WHERE sut.terminate=0 AND `uid` IN ($userids) GROUP BY sut.uid");
            //  echo $this->db->last_query()."<br/>";
            $topupass = array();
            foreach ($topupq->result() as $valtop) {
                $topupass[$valtop->uid] = $valtop->topupcount;
            }
            
            //function to get dues user list
            $duearr     = $this->dues_list_billing($userids);
            $dueuserarr = $this->dues_list($userids);
            
            // echo "<pre>"; print_R($userdarr); die;
            
            
            $i      = 0;
            $dayarr = array();
            foreach ($userdarr as $key => $valu) {
                $days        = 0;
                $currentdate = date_create(date("Y-m-d"));
                $expirydate  = date_create($valu->expiration);
                if (strtotime(date("Y-m-d")) < strtotime($valu->expiration)) {
                    $difference = date_diff($currentdate, $expirydate);
                    // echo "<pre>"; print_R($difference);
                    
                    $days = $difference->format("%a");
                }
                
                //echo "====>>".date("Y-m-d")."::".$valu->expiration."::".$days."<br/>";
                $valu->expirydays = $days;
                
                if (!array_key_exists($key, $duearr)) {
                    // $fnlarr['100perc'][$i]=$valu;
                    //$fnlarr['100perc'][$i]->topupcount= (array_key_exists($valu->uid,$topupass))?$topupass[$valu->uid]:"-";
                    //        $fnlarr['100perc'][$i]->dues= (array_key_exists($valu->uid,$dueuserarr))?$dueuserarr[$valu->uid]:"-";
                } else {
                    if ($duearr[$key] >= 0) {
                        // $fnlarr['100perc'][$i]=$valu;
                        //$fnlarr['100perc'][$i]->topupcount= (array_key_exists($valu->uid,$topupass))?$topupass[$valu->uid]:"-";
                        //    $fnlarr['100perc'][$i]->dues= (array_key_exists($valu->uid,$dueuserarr))?$dueuserarr[$valu->uid]:"-";
                    } else {
                        
                        $percentage = (str_replace("-", "", $duearr[$key]) / $valu->user_credit_limit) * 100;
                        $percentage = 100 - $percentage;
                        // echo "====>>>".$percentage;
                        if ($percentage > 0 && $percentage <= 25) {
                            $fnlarr['25perc'][$i]             = $valu;
                            $fnlarr['25perc'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                            $fnlarr['25perc'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                        } else if ($percentage > 25 && $percentage <= 50) {
                            $fnlarr['50perc'][$i]             = $valu;
                            $fnlarr['50perc'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                            $fnlarr['50perc'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                        } else if ($percentage > 50 && $percentage <= 75) {
                            $fnlarr['75perc'][$i]             = $valu;
                            $fnlarr['75perc'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                            $fnlarr['75perc'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                        } else if ($percentage > 75 && $percentage <= 90) {
                            $fnlarr['90perc'][$i]             = $valu;
                            $fnlarr['90perc'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                            $fnlarr['90perc'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                        } else if ($percentage > 90) {
                            $fnlarr['100perc'][$i]             = $valu;
                            $fnlarr['100perc'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                            $fnlarr['100perc'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                        }
                        
                        
                        if ($duearr[$key] < 0) {
                            $amt = str_replace("-", "", $duearr[$key]);
                            if ($amt > 0 && $amt <= 500) {
                                $fnlarr['p500'][$i]             = $valu;
                                $fnlarr['p500'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                                $fnlarr['p500'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                            } else if ($amt > 500 && $amt <= 1000) {
                                $fnlarr['500p1000'][$i]             = $valu;
                                $fnlarr['500p1000'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                                $fnlarr['500p1000'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                            } else if ($amt > 1000 && $amt <= 2000) {
                                $fnlarr['1000p2000'][$i]             = $valu;
                                $fnlarr['1000p2000'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                                $fnlarr['1000p2000'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                            } else if ($amt > 2000) {
                                $fnlarr['p2000'][$i]             = $valu;
                                $fnlarr['p2000'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                                $fnlarr['p2000'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                            }
                        }
                        
                        if ($days == 1) {
                            $fnlarr['1day'][$i] = $valu;
                        } else if ($days == 2) {
                            $fnlarr['2day'][$i] = $valu;
                        } else if ($days == 3) {
                            $fnlarr['3day'][$i] = $valu;
                        }
                        
                    }
                    
                }
                
                $i++;
            }
            //  echo "<pre>"; print_R($fnlarr); die;
            
            if (isset($postdata['filter']) && $postdata['filter'] == "perc") {
                $datarray = array();
                if ($postdata['filtertype'] == "25perc") {
                    $datarray = $fnlarr['25perc'];
                } else if ($postdata['filtertype'] == "100perc") {
                    $datarray = $fnlarr['100perc'];
                } else if ($postdata['filtertype'] == "90perc") {
                    $datarray = $fnlarr['90perc'];
                } else if ($postdata['filtertype'] == "50perc") {
                    $datarray = $fnlarr['50perc'];
                } else if ($postdata['filtertype'] == "75perc") {
                    $datarray = $fnlarr['75perc'];
                } else {
                    $datarray = array_merge($fnlarr['25perc'], $fnlarr['100perc']);
                    $datarray = array_merge($datarray, $fnlarr['50perc']);
                    $datarray = array_merge($datarray, $fnlarr['75perc']);
                    $datarray = array_merge($datarray, $fnlarr['90perc']);
                }
                
                
                
            } else if (isset($postdata['filter']) && $postdata['filter'] == "number") {
                $datarray = array();
                if ($postdata['filtertype'] == "p500") {
                    $datarray = $fnlarr['p500'];
                } else if ($postdata['filtertype'] == "500p1000") {
                    $datarray = $fnlarr['500p1000'];
                } else if ($postdata['filtertype'] == "1000p2000") {
                    $datarray = $fnlarr['1000p2000'];
                } else if ($postdata['filtertype'] == "p2000") {
                    $datarray = $fnlarr['p2000'];
                } else {
                    $datarray = array_merge($fnlarr['p500'], $fnlarr['500p1000']);
                    $datarray = array_merge($datarray, $fnlarr['1000p2000']);
                    $datarray = array_merge($datarray, $fnlarr['p2000']);
                    
                }
                
                
                
            } else if (isset($postdata['filter']) && $postdata['filter'] == "days") {
                $datarray = array();
                if ($postdata['filtertype'] == "1day") {
                    $datarray = $fnlarr['1day'];
                } else if ($postdata['filtertype'] == "2day") {
                    $datarray = $fnlarr['2day'];
                } else if ($postdata['filtertype'] == "3day") {
                    $datarray = $fnlarr['3day'];
                } else {
                    $datarray = array_merge($fnlarr['1day'], $fnlarr['2day']);
                    $datarray = array_merge($datarray, $fnlarr['3day']);
                    
                    
                }
            } else {
                $datarray = array();
                $datarray = array_merge($fnlarr['100perc'], $fnlarr['25perc']);
                $datarray = array_merge($datarray, $fnlarr['90perc']);
                $datarray = array_merge($datarray, $fnlarr['50perc']);
                $datarray = array_merge($datarray, $fnlarr['75perc']);
            }
            
            
            //  echo "<pre>"; print_R($datarray); die;
            
            
            
            
            
            $i   = 1;
            $gen = '';
            $gen .= '<table id="billuser_table" class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>UID</th>
                                                   <th>NAME</th>
                                                   <th>Expire In</th>
                                                  
                                                   <th>CITY</th>
                                                   <th>ZONE</th>
                                                   <th>PLAN</th>
                                                   <th>TOP UP PLAN</th>
                                                   <th class="mui--text-right">DUES</th>
                                                </tr>
                                             </thead>
                                             <tbody id="search_activegrid">';
            if (count($datarray) > 0) {
                
                foreach ($datarray as $vallist) {
                    
                    
                    $url = base_url() . "user/edit/" . $vallist->id;
                    
                    
                    $gen .= ' 
                <tr>
                    <td>' . $i . '.</td>
                    <td>' . $vallist->uid . '</td>
                    <td><a href="' . $url . '">' . $vallist->firstname . ' ' . $vallist->lastname . '</a></td>
                    <td> ' . $vallist->expirydays . " days" . '</td>
                    <td> ' . $vallist->city_name . '</td>
                    <td>' . $vallist->zone_name . '</td>
                                        <td>' . $vallist->srvname . '</td>' . '<td>' . $vallist->topupcount . '</td>' . ' <td>' . $vallist->dues . '</td>';
                    
                    
                    
                    
                    
                    $gen .= '</tr>
                ';
                    $i++;
                }
                
            } else {
                $gen .= '<tr><td style="text-align:center" colspan="8"> No Result Found !!</td></tr>';
            }
            $gen .= '</tbody>
                                          </table>';
            
            $data['html']         = $gen;
            $datarray             = array_merge($fnlarr['p500'], $fnlarr['500p1000']);
            $datarray             = array_merge($datarray, $fnlarr['1000p2000']);
            $datarray             = array_merge($datarray, $fnlarr['p2000']);
            $data['total_count']  = $countarr['totalcount'];
            $data['last30count']  = $countarr['last30count'];
            $data['last180count'] = $countarr['last180count'];
            $data['more180count'] = $countarr['more180count'];
            $data['tfperc']       = count($fnlarr['25perc']);
            $data['ozzperc']      = count($fnlarr['100perc']);
            $data['fzperc']       = count($fnlarr['50perc']);
            $data['nzperc']       = count($fnlarr['90perc']);
            $data['sfperc']       = count($fnlarr['75perc']);
            $data['tfperc']       = count($fnlarr['25perc']);
            $data['avgrevenue']   = round($avgrevenue);
            $data['pf']           = count($fnlarr['p500']);
            $data['fpo']          = count($fnlarr['500p1000']);
            $data['opt']          = count($fnlarr['1000p2000']);
            $data['pt']           = count($fnlarr['p2000']);
            
        } else {
            $gen = '';
            $gen .= '<table id="billuser_table" class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>NAME</th>
                                                   <th>UID</th>
                                                   <th>CITY</th>
                                                   <th>ZONE</th>
                                                   <th>PLAN</th>
                                                   <th>TOP UP PLAN</th>
                                                   <th class="mui--text-right">DUES</th>
                                                </tr>
                                             </thead>
                                             <tbody id="search_activegrid"><tr><td style="text-align:center" colspan="8"> No Result Found !!</td></tr></tbody></table>';
            $data['html']         = $gen;
            $data['total_count']  = $countarr['totalcount'];
            $data['last30count']  = $countarr['last30count'];
            $data['last180count'] = $countarr['last180count'];
            $data['more180count'] = $countarr['more180count'];
            $data['tfperc']       = count($fnlarr['25perc']);
            $data['ozzperc']      = count($fnlarr['100perc']);
            $data['fzperc']       = count($fnlarr['50perc']);
            $data['nzperc']       = count($fnlarr['90perc']);
            $data['sfperc']       = count($fnlarr['75perc']);
            $data['tfperc']       = count($fnlarr['25perc']);
            $data['avgrevenue']   = round($avgrevenue);
            $data['pf']           = count($fnlarr['p500']);
            $data['fpo']          = count($fnlarr['500p1000']);
            $data['opt']          = count($fnlarr['1000p2000']);
            $data['pt']           = count($fnlarr['p2000']);
            
        }
        echo json_encode($data);
        //echo $this->db->last_query();
        die;
        
    }
    
    public function isp_license_data(){
        $wallet_amt   = 0;
        $passbook_amt = 0;
        $walletQ      = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='" . ISPID . "' AND is_paid='1'");
        if ($walletQ->num_rows() > 0) {
            $wallet_amt = $walletQ->row()->wallet_amt;
        }
        
        $passbookQ = $this->db->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost FROM sht_isp_passbook WHERE isp_uid='" . ISPID . "'");
        if ($passbookQ->num_rows() > 0) {
            $passrowdata  = $passbookQ->row();
            $passbook_amt = $passrowdata->activeusers_cost;
        }
        $balanceamt = $wallet_amt - $passbook_amt;
        if ($balanceamt < 0) {
            return 0;
        } else {
            return 1;
        }
    }
    
    
    //Excel function php
    
    public function billing_report_excel(){
        $this->benchmark->mark('code_start');
        $data           = array();
        $postdata       = $this->input->post();
        $strtlast30     = date('Y-m-d', strtotime('today - 30 days'));
        $enddate        = date('Y-m-d');
        $start180       = date('Y-m-d', strtotime('today - 210 days'));
        $enddate180     = date('Y-m-d', strtotime('today - 30 days'));
        $enddate180more = date('Y-m-d', strtotime('today - 180 days'));
        
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        $ispcond     = '';
        $ispcond     = " and su.isp_uid='" . $isp_uid . "'";
        
        $sczcond  = $this->user_department_cond();
        $query1   = $this->db->query("SELECT COUNT(su.id) AS totalcount,
        (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon between '" . $strtlast30 . "' and '" . $enddate . "' {$sczcond} {$ispcond})as last30count,
            (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon between '" . $start180 . "' and '" . $enddate180 . "' {$sczcond} {$ispcond})as last180count, 
            (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon < '" . $enddate180more . "' {$sczcond} {$ispcond})as more180count FROM `sht_users` su WHERE su.enableuser=1 AND expired=0 {$sczcond} {$ispcond}");
        // echo $this->db->last_query()."<br/>";//die;
        $countarr = $query1->row_array();
        
        $cond = "";
        if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "last30") {
            $cond .= "and su.createdon between '" . $strtlast30 . "' and '" . $enddate . "'";
        } else if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "last180") {
            $cond .= "and su.createdon between '" . $start180 . "' and '" . $enddate180 . "'";
        } else if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "more180") {
            $cond .= "and  su.createdon < '" . $enddate180more . "'";
        }
        //query to find acctive user 
        
        $Uquery   = $this->db->query("SELECT su.id,su.expiration,spp.net_total,su.user_credit_limit,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname FROM sht_users su
        INNER JOIN sht_services ss ON (su.baseplanid=ss.srvid) 
         INNER JOIN sht_plan_pricing spp ON (spp.srvid=ss.srvid) 
        INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
        INNER JOIN `sht_zones` sz ON (sz.id=su.zone) WHERE su.enableuser=1 AND expired=0 {$sczcond} {$cond} {$ispcond}");
        // echo $this->db->last_query()."<br/>";
        $userarr  = array();
        $userdarr = array();
        foreach ($Uquery->result() as $val) {
            $userdarr[$val->uid] = $val;
            $usernet[]           = $val->net_total;
            $userarr[]           = $val->uid;
            $userarr[]           = $val->expiration;
        }
        $avgrevenue          = 0;
        //query to get online uers
        $activeuserid        = "";
        $fnlarr['100perc']   = array();
        $fnlarr['25perc']    = array();
        $fnlarr['50perc']    = array();
        $fnlarr['75perc']    = array();
        $fnlarr['90perc']    = array();
        $fnlarr['p500']      = array();
        $fnlarr['500p1000']  = array();
        $fnlarr['1000p2000'] = array();
        $fnlarr['p2000']     = array();
        $fnlarr['1day']      = array();
        $fnlarr['2day']      = array();
        $fnlarr['3day']      = array();
        if (!empty($userarr)) {
            
            $avgrevenue = array_sum($usernet) / count($userarr);
            $userids    = '"' . implode('", "', $userarr) . '"';
            
            
            //query to get user top associated
            $topupq   = $this->db->query("SELECT `uid`,COUNT(DISTINCT(topup_id)) AS topupcount FROM `sht_usertopupassoc` sut
                INNER JOIN sht_services ss ON (ss.srvid=sut.topup_id)   WHERE sut.terminate=0 AND `uid` IN ($userids) GROUP BY sut.uid");
            //  echo $this->db->last_query()."<br/>";
            $topupass = array();
            foreach ($topupq->result() as $valtop) {
                $topupass[$valtop->uid] = $valtop->topupcount;
            }
            
            //function to get dues user list
            $duearr     = $this->dues_list_billing($userids);
            $dueuserarr = $this->dues_list($userids);
            
            // echo "<pre>"; print_R($userdarr); die;
            
            
            $i      = 0;
            $dayarr = array();
            foreach ($userdarr as $key => $valu) {
                $days        = 0;
                $currentdate = date_create(date("Y-m-d"));
                $expirydate  = date_create($valu->expiration);
                if (strtotime(date("Y-m-d")) < strtotime($valu->expiration)) {
                    $difference = date_diff($currentdate, $expirydate);
                    // echo "<pre>"; print_R($difference);
                    
                    $days = $difference->format("%a");
                }
                
                //echo "====>>".date("Y-m-d")."::".$valu->expiration."::".$days."<br/>";
                $valu->expirydays = $days;
                
                if (!array_key_exists($key, $duearr)) {
                    // $fnlarr['100perc'][$i]=$valu;
                    //$fnlarr['100perc'][$i]->topupcount= (array_key_exists($valu->uid,$topupass))?$topupass[$valu->uid]:"-";
                    //        $fnlarr['100perc'][$i]->dues= (array_key_exists($valu->uid,$dueuserarr))?$dueuserarr[$valu->uid]:"-";
                } else {
                    if ($duearr[$key] >= 0) {
                        // $fnlarr['100perc'][$i]=$valu;
                        //$fnlarr['100perc'][$i]->topupcount= (array_key_exists($valu->uid,$topupass))?$topupass[$valu->uid]:"-";
                        //    $fnlarr['100perc'][$i]->dues= (array_key_exists($valu->uid,$dueuserarr))?$dueuserarr[$valu->uid]:"-";
                    } else {
                        
                        $percentage = (str_replace("-", "", $duearr[$key]) / $valu->user_credit_limit) * 100;
                        $percentage = 100 - $percentage;
                        // echo "====>>>".$percentage;
                        if ($percentage > 0 && $percentage <= 25) {
                            $fnlarr['25perc'][$i]             = $valu;
                            $fnlarr['25perc'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                            $fnlarr['25perc'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                        } else if ($percentage > 25 && $percentage <= 50) {
                            $fnlarr['50perc'][$i]             = $valu;
                            $fnlarr['50perc'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                            $fnlarr['50perc'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                        } else if ($percentage > 50 && $percentage <= 75) {
                            $fnlarr['75perc'][$i]             = $valu;
                            $fnlarr['75perc'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                            $fnlarr['75perc'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                        } else if ($percentage > 75 && $percentage <= 90) {
                            $fnlarr['90perc'][$i]             = $valu;
                            $fnlarr['90perc'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                            $fnlarr['90perc'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                        } else if ($percentage > 90) {
                            $fnlarr['100perc'][$i]             = $valu;
                            $fnlarr['100perc'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                            $fnlarr['100perc'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                        }
                        
                        
                        if ($duearr[$key] < 0) {
                            $amt = str_replace("-", "", $duearr[$key]);
                            if ($amt > 0 && $amt <= 500) {
                                $fnlarr['p500'][$i]             = $valu;
                                $fnlarr['p500'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                                $fnlarr['p500'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                            } else if ($amt > 500 && $amt <= 1000) {
                                $fnlarr['500p1000'][$i]             = $valu;
                                $fnlarr['500p1000'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                                $fnlarr['500p1000'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                            } else if ($amt > 1000 && $amt <= 2000) {
                                $fnlarr['1000p2000'][$i]             = $valu;
                                $fnlarr['1000p2000'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                                $fnlarr['1000p2000'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                            } else if ($amt > 2000) {
                                $fnlarr['p2000'][$i]             = $valu;
                                $fnlarr['p2000'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                                $fnlarr['p2000'][$i]->dues       = (array_key_exists($valu->uid, $dueuserarr)) ? $dueuserarr[$valu->uid] : "-";
                            }
                        }
                        
                        if ($days == 1) {
                            $fnlarr['1day'][$i] = $valu;
                        } else if ($days == 2) {
                            $fnlarr['2day'][$i] = $valu;
                        } else if ($days == 3) {
                            $fnlarr['3day'][$i] = $valu;
                        }
                        
                    }
                    
                }
                
                $i++;
            }
            //  echo "<pre>"; print_R($fnlarr); die;
            
            if (isset($postdata['filter']) && $postdata['filter'] == "perc") {
                $datarray = array();
                if ($postdata['filtertype'] == "25perc") {
                    $datarray = $fnlarr['25perc'];
                } else if ($postdata['filtertype'] == "100perc") {
                    $datarray = $fnlarr['100perc'];
                } else if ($postdata['filtertype'] == "90perc") {
                    $datarray = $fnlarr['90perc'];
                } else if ($postdata['filtertype'] == "50perc") {
                    $datarray = $fnlarr['50perc'];
                } else if ($postdata['filtertype'] == "75perc") {
                    $datarray = $fnlarr['75perc'];
                } else {
                    $datarray = array_merge($fnlarr['25perc'], $fnlarr['100perc']);
                    $datarray = array_merge($datarray, $fnlarr['50perc']);
                    $datarray = array_merge($datarray, $fnlarr['75perc']);
                    $datarray = array_merge($datarray, $fnlarr['90perc']);
                }
                
                
                
            } else if (isset($postdata['filter']) && $postdata['filter'] == "number") {
                $datarray = array();
                if ($postdata['filtertype'] == "p500") {
                    $datarray = $fnlarr['p500'];
                } else if ($postdata['filtertype'] == "500p1000") {
                    $datarray = $fnlarr['500p1000'];
                } else if ($postdata['filtertype'] == "1000p2000") {
                    $datarray = $fnlarr['1000p2000'];
                } else if ($postdata['filtertype'] == "p2000") {
                    $datarray = $fnlarr['p2000'];
                } else {
                    $datarray = array_merge($fnlarr['p500'], $fnlarr['500p1000']);
                    $datarray = array_merge($datarray, $fnlarr['1000p2000']);
                    $datarray = array_merge($datarray, $fnlarr['p2000']);
                    
                }
                
                
                
            } else if (isset($postdata['filter']) && $postdata['filter'] == "days") {
                $datarray = array();
                if ($postdata['filtertype'] == "1day") {
                    $datarray = $fnlarr['1day'];
                } else if ($postdata['filtertype'] == "2day") {
                    $datarray = $fnlarr['2day'];
                } else if ($postdata['filtertype'] == "3day") {
                    $datarray = $fnlarr['3day'];
                } else {
                    $datarray = array_merge($fnlarr['1day'], $fnlarr['2day']);
                    $datarray = array_merge($datarray, $fnlarr['3day']);
                    
                    
                }
            } else {
                $datarray = array();
                $datarray = array_merge($fnlarr['100perc'], $fnlarr['25perc']);
                $datarray = array_merge($datarray, $fnlarr['90perc']);
                $datarray = array_merge($datarray, $fnlarr['50perc']);
                $datarray = array_merge($datarray, $fnlarr['75perc']);
            }
            
            if (count($datarray) > 0) {
                $this->billing_excel_create($datarray);
                $data['resultcode'] = 1;
                $data['msg']        = "Success";
            } else {
                $data['resultcode'] = 0;
                $data['msg']        = "No Data found";
            }
            //echo "<pre>"; print_R($datarray); die;
            
            
            //echo $this->db->last_query();
            // die;
            
        } else {
            $data['resultcode'] = 0;
            $data['msg']        = "No Data found";
        }
        echo json_encode($data);
    }
    
    
    public function billing_excel_create($datarray){
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray  = array(
            'font' => array(
                'bold' => false,
                'color' => array(
                    'rgb' => '000000'
                ),
                'name' => 'Tahoma',
                'size' => 14
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font' => array(
                'bold' => false,
                'color' => array(
                    'rgb' => '000000'
                ),
                'name' => 'Tahoma',
                'size' => 12
            )
        );
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        
        
        $objPHPExcel->getActiveSheet()->setTitle("Billing Report");
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'UID');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2', 'NAME');
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C2', 'CITY');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D2', 'ZONE');
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E2', 'PLAN');
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('F2', 'TOP UP PLAN');
        $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('G2', 'DUES');
        $objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('H2', 'Expires in Days');
        $objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Billing Report');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $j = 3;
        foreach ($datarray as $valdata) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $valdata->uid);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $valdata->firstname . " " . $valdata->lastname);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $valdata->city_name);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $j, $valdata->zone_name);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $j, $valdata->srvname);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $j, $valdata->topupcount);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('G' . $j, $valdata->dues);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('H' . $j, $valdata->expirydays);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $j)->applyFromArray($styleArray1);
            $j++;
        }
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('reports/billing_reports.xlsx');
        
        
        
    }
    
    
    
    public function persp_user_filter_excel(){
        $postdata    = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        $ispcond     = '';
        $ispcond     = " and su.isp_uid='" . $isp_uid . "'";
        $persparr    = array();
        $data        = array();
        $con         = $this->user_department_cond();
        $usercond    = "";
        if ($con != '') {
            $usercond .= $con;
        }
        if ($postdata['filter'] == "all" || $postdata['filter'] == "lead" || $postdata['filter'] == "enquiry") {
            if ($postdata['filter'] == "all") {
                $cond = "where su.status='1' and su.is_deleted='0'";
            } else {
                $usertype = ($postdata['filter'] == "lead") ? 1 : 2;
                $cond     = "where su.status='1' and su.is_deleted='0' and usertype={$usertype}";
            }
            
            
            
            $query   = $this->db->query("SELECT su.id,`first_name` as firstname,su.`last_name` as lastname,su.`email`,su.`phone` as mobile,su.usertype FROM `sht_subscriber` su {$cond} $usercond $ispcond");
            //   echo $this->db->last_query()."<br>";
            $leadarr = array();
            foreach ($query->result() as $val) {
                $persparr[] = $val;
            }
            
        }
        if ($postdata['filter'] == "all" || $postdata['filter'] == "inactive") {
            $queryinact = $this->db->query("SELECT su.id,su.`firstname`,su.`lastname`,su.`email`,su.`mobile`,sap.kyc_details FROM `sht_users` su INNER JOIN `sht_subscriber_activation_panel` sap ON (su.id=sap.subscriber_id) WHERE enableuser='0' and su.inactivate_type= '' $usercond $ispcond");
            
            //   echo $this->db->last_query()."<br>";
            foreach ($queryinact->result() as $valinactuser) {
                $persparr[] = $valinactuser;
            }
        }
        // echo "<pre>"; print_R($persparr);die;
        
        if (count($persparr) > 0) {
            $data['resultcode'] = 1;
            $this->perspective_excel_create($persparr);
            
        } else {
            $data['msg'] = "No Data found";
        }
        echo json_encode($data);
    }
    
    public function perspective_excel_create($datarray){
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray  = array(
            'font' => array(
                'bold' => false,
                'color' => array(
                    'rgb' => '000000'
                ),
                'name' => 'Tahoma',
                'size' => 14
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font' => array(
                'bold' => false,
                'color' => array(
                    'rgb' => '000000'
                ),
                'name' => 'Tahoma',
                'size' => 12
            )
        );
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        
        
        $objPHPExcel->getActiveSheet()->setTitle("Perspective User Report");
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Name');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2', 'EMAIL');
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C2', 'MOBILE');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D2', 'STAGE/STATUS');
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E2', 'KYC STATUS');
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Perspective User Report');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $j = 3;
        foreach ($datarray as $valdata) {
            $usertype = (isset($valdata->usertype)) ? ($valdata->usertype == 1) ? "Lead" : "Enquiry" : "Inactive";
            $kyc      = (isset($valdata->kyc_details)) ? ($valdata->kyc_details == 1) ? "Done" : "Not Done" : "-";
            
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $valdata->firstname . " " . $valdata->lastname);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $valdata->email);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $valdata->mobile);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $j, $usertype);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $j, $kyc);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $j)->applyFromArray($styleArray1);
            
            $j++;
        }
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('reports/perspectiveuser_reports.xlsx');
        
        
        
    }
    
    
    public function active_user_excel(){
        $this->benchmark->mark('code_start');
        $data           = array();
        $postdata       = $this->input->post();
        $strtlast30     = date('Y-m-d', strtotime('today - 30 days'));
        $enddate        = date('Y-m-d');
        $start180       = date('Y-m-d', strtotime('today - 210 days'));
        $enddate180     = date('Y-m-d', strtotime('today - 30 days'));
        $enddate180more = date('Y-m-d', strtotime('today - 180 days'));
        
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        $ispcond     = '';
        $ispcond     = " and su.isp_uid='" . $isp_uid . "'";
        
        $sczcond  = $this->user_department_cond();
        $query1   = $this->db->query("SELECT COUNT(su.id) AS totalcount,
        (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon between '" . $strtlast30 . "' and '" . $enddate . "' {$sczcond} {$ispcond})as last30count,
            (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon between '" . $start180 . "' and '" . $enddate180 . "' {$sczcond} {$ispcond})as last180count, 
            (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon < '" . $enddate180more . "' {$sczcond} {$ispcond})as more180count FROM `sht_users` su WHERE su.enableuser=1 AND expired=0 {$sczcond} {$ispcond}");
        //echo $this->db->last_query()."<br/>";//die;
        $countarr = $query1->row_array();
        
        $cond = "";
        if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "last30") {
            $cond .= "and su.createdon between '" . $strtlast30 . "' and '" . $enddate . "'";
        } else if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "last180") {
            $cond .= "and su.createdon between '" . $start180 . "' and '" . $enddate180 . "'";
        } else if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "more180") {
            $cond .= "and  su.createdon < '" . $enddate180more . "'";
        }
        //query to find acctive user 
        
        $Uquery   = $this->db->query("SELECT su.id,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname FROM sht_users su
        INNER JOIN sht_services ss ON (su.baseplanid=ss.srvid) 
        INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
        INNER JOIN `sht_zones` sz ON (sz.id=su.zone) WHERE su.enableuser=1 AND expired=0 {$sczcond} {$cond} {$ispcond}");
        //echo $this->db->last_query()."<br/>";
        $userarr  = array();
        $userdarr = array();
        foreach ($Uquery->result() as $val) {
            $userdarr[$val->uid] = $val;
            $userarr[]           = $val->uid;
        }
        //query to get online uers
        $activeuserid = "";
        if (!empty($userarr)) {
            
            
            $userids     = '"' . implode('", "', $userarr) . '"';
            $onlinearr   = array();
            $onlinequery = $this->db->query("SELECT `username` FROM `radacct` WHERE `acctstoptime` IS NULL AND `username` IN ($userids) group by username");
            // echo "===>>".$this->db->last_query();die;
            // echo $this->db->last_query()."<br/>";
            foreach ($onlinequery->result() as $valon) {
                $onlinearr[] = $valon->username;
            }
            
            //query to get user top associated
            $topupq   = $this->db->query("SELECT `uid`,COUNT(DISTINCT(topup_id)) AS topupcount FROM `sht_usertopupassoc` sut
                INNER JOIN sht_services ss ON (ss.srvid=sut.topup_id)   WHERE sut.terminate=0 AND `uid` IN ($userids) GROUP BY sut.uid");
            // echo $this->db->last_query()."<br/>";
            $topupass = array();
            foreach ($topupq->result() as $valtop) {
                $topupass[$valtop->uid] = $valtop->topupcount;
            }
            
            //function to get dues user list
            $duearr = $this->dues_list($userids);
            
            //  echo "<pre>online arr";print_R($onlinearr);
            //  echo "<pre>topuparr";print_R($topupass);
            //  echo "<pre>duearr";print_R($duearr);
            //  echo "<pre>userarr";print_R($userdarr);
            
            $fnlarr['online']    = array();
            $fnlarr['offline']   = array();
            $fnlarr['suspended'] = array();
            $i                   = 0;
            foreach ($userdarr as $key => $valu) {
                if (in_array($key, $onlinearr)) {
                    $fnlarr['online'][$i]             = $valu;
                    $fnlarr['online'][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                    $fnlarr['online'][$i]->dues       = (array_key_exists($valu->uid, $duearr)) ? $duearr[$valu->uid] : "-";
                } else {
                    
                    if ($valu->inactivate_type == "suspended") {
                        $key = "suspended";
                    } else {
                        $key = "offline";
                    }
                    $fnlarr[$key][$i]             = $valu;
                    $fnlarr[$key][$i]->topupcount = (array_key_exists($valu->uid, $topupass)) ? $topupass[$valu->uid] : "-";
                    $fnlarr[$key][$i]->dues       = (array_key_exists($valu->uid, $duearr)) ? $duearr[$valu->uid] : "-";
                }
                
                
                $i++;
            }
            
            
            if (isset($postdata['filter']) && $postdata['filter'] == "online") {
                $datarray = array();
                $datarray = $fnlarr['online'];
            } else if (isset($postdata['filter']) && $postdata['filter'] == "offline") {
                $datarray = array();
                $datarray = $fnlarr['offline'];
            } else if (isset($postdata['filter']) && $postdata['filter'] == "suspended") {
                $datarray = array();
                
                $datarray = $fnlarr['suspended'];
            } else {
                $datarray = array();
                $datarray = array_merge($fnlarr['online'], $fnlarr['offline']);
                $datarray = array_merge($datarray, $fnlarr['suspended']);
            }
            
            
            
            if (count($datarray) > 0) {
                $this->activeuser_excel_create($datarray);
                $data['resultcode'] = 1;
                $data['msg']        = "Success";
            } else {
                $data['resultcode'] = 0;
                $data['msg']        = "No Data found";
            }
            
            
            
            
            
        } else {
            $data['resultcode'] = 0;
            $data['msg']        = "No Data found";
        }
        echo json_encode($data);
        
        die;
    }
    
    public function activeuser_excel_create($datarray){
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray  = array(
            'font' => array(
                'bold' => false,
                'color' => array(
                    'rgb' => '000000'
                ),
                'name' => 'Tahoma',
                'size' => 14
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font' => array(
                'bold' => false,
                'color' => array(
                    'rgb' => '000000'
                ),
                'name' => 'Tahoma',
                'size' => 12
            )
        );
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        
        
        $objPHPExcel->getActiveSheet()->setTitle("Billing Report");
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'NAME');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2', 'UID');
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C2', 'CITY');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D2', 'ZONE');
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E2', 'PLAN');
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('F2', 'TOP UP PLAN');
        $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('G2', 'DUES');
        $objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Active User Report');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $j = 3;
        foreach ($datarray as $valdata) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $valdata->uid);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $valdata->firstname . " " . $valdata->lastname);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $valdata->city_name);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $j, $valdata->zone_name);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $j, $valdata->srvname);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $j, $valdata->topupcount);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('G' . $j, $valdata->dues);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $j)->applyFromArray($styleArray1);
            $j++;
        }
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('reports/activeuser_reports.xlsx');
        
        
        
    }
    
    
    public function compaint_request_excel(){
        $postdata    = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $ispcond = '';
        $ispcond = " and su.isp_uid='" . $isp_uid . "'";
        
        $cond = '';
        if (isset($postdata['complaint_type']) && $postdata['complaint_type'] != 'all') {
            //  echo "sssss";
            $cond .= " and sst.ticket_type='" . $postdata['complaint_type'] . "'";
        }
        $sczcond    = $this->user_department_cond();
        $montharr   = $this->get_last_sixmonthwithyear();
        $monthlyarr = $this->get_last_sixmonth();
        //echo "<pre>"; print_R($monthlyarr); //die;
        $opentquery = $this->db->query("
SELECT YEAR(sst.`added_on`) AS Y, MONTH(sst.`added_on`) AS m,MONTHNAME(sst.`added_on`) AS mo, COUNT(sst.id) AS totalcount
FROM `sht_subscriber_tickets` sst
INNER JOIN `sht_users` su ON (su.uid=sst.`subscriber_uuid`) where 1 {$cond} {$sczcond} {$ispcond}
 and status='0' and ticket_assign_to='0' GROUP BY Y, m");
        //echo $this->db->last_query()."<br/>";
        $monthdata  = array();
        foreach ($monthlyarr as $val) {
            $monthdata[$val] = 0;
        }
        //foreach()
        foreach ($opentquery->result() as $openval) {
            //$data
            $monthval = $openval->m . " " . $openval->Y;
            if (in_array($monthval, $montharr)) {
                $monthdata[substr($openval->mo, 0, 3)] = (int) $openval->totalcount;
                //    $monthdata[substr($openval->mo,0,3)]=1;
            } else {
                $monthdata[substr($openval->mo, 0, 3)] = 0;
            }
            
            
        }
        $data['graph']            = $monthdata;
        /*$complaintarr             = array(
            "change_address" => "Address Change",
            "change_plan" => "Plan Change",
            "terminate_service" => "Terminate Service",
            "suspend_service" => "Suspend Service",
            "service_down" => "Service Down",
            "router_not_working" => "Router Issues",
            "other_request" => "Other Request"
        );*/
        $complaintarr = array();
        $tkttypeQ = $this->db->query("SELECT * FROM sht_tickets_type WHERE isp_uid='".$isp_uid."' ORDER BY id DESC");
	if($tkttypeQ->num_rows() > 0){
            foreach($tkttypeQ->result() as $tktobj){
                $complaintarr["$tktobj->ticket_type_value"] = $tktobj->ticket_type;
	    }
        }
        $ticketarr['open']        = array();
        $ticketarr['close']       = array();
        $ticketarr['in_progress'] = array();
        
        $queryticket = $this->db->query("
  SELECT su.id,su.uid,sst.`subscriber_uuid`,sst.status,sst.ticket_assign_to,sst.`ticket_type`,su.`firstname`,su.`lastname`,ss.srvname,sc.city_name,sz.zone_name FROM  sht_subscriber_tickets sst
  INNER JOIN sht_users su ON (su.uid=sst.subscriber_uuid)
  INNER JOIN sht_services ss ON (su.baseplanid=ss.srvid) 
        INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
        INNER JOIN `sht_zones` sz ON (sz.id=su.zone) where 1 {$cond} {$sczcond} {$ispcond} order by sst.added_on desc
              ");
        
        // echo $this->db->last_query()."<br/>";
        
        $i = 0;
        foreach ($queryticket->result() as $val) {
            if ($val->status == 0) {
                if ($val->ticket_assign_to == 0) {
                    $ticketarr['open'][$i] = $val;
                } else {
                    $ticketarr['in_progress'][$i] = $val;
                }
                
            } else {
                $ticketarr['close'][$i] = $val;
            }
            $i++;
        }
        //       echo "<pre>"; print_R($ticketarr);die;
        
        if (isset($postdata['filter']) && $postdata['filter'] == "open") {
            $datarray = array();
            $datarray = $ticketarr['open'];
        } else if (isset($postdata['filter']) && $postdata['filter'] == "close") {
            $datarray = array();
            $datarray = $ticketarr['close'];
        } else if (isset($postdata['filter']) && $postdata['filter'] == "in_progress") {
            $datarray = array();
            
            $datarray = $ticketarr['in_progress'];
        } else {
            $datarray = array();
            $datarray = array_merge($ticketarr['open'], $ticketarr['close']);
            $datarray = array_merge($datarray, $ticketarr['in_progress']);
        }
        
        if (count($datarray) > 0) {
            $this->complaint_excel_create($datarray);
            $data['resultcode'] = 1;
            $data['msg']        = "Success";
        } else {
            $data['resultcode'] = 0;
            $data['msg']        = "No Data found";
        }
        
        echo json_encode($data);
        
    }
    
    
    public function complaint_excel_create($datarray){
        /*$complaintarr = array(
            "change_address" => "Address Change",
            "change_plan" => "Plan Change",
            "terminate_service" => "Terminate Service",
            "suspend_service" => "Suspend Service",
            "service_down" => "Service Down",
            "router_not_working" => "Router Issues",
            "other_request" => "Other Request"
        );*/
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = $sessiondata['isp_uid'];
        $complaintarr = array();
        $tkttypeQ = $this->db->query("SELECT * FROM sht_tickets_type WHERE isp_uid='".$isp_uid."' ORDER BY id DESC");
	if($tkttypeQ->num_rows() > 0){
            foreach($tkttypeQ->result() as $tktobj){
                $complaintarr["$tktobj->ticket_type_value"] = $tktobj->ticket_type;
	    }
        }
        $objPHPExcel  = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray  = array(
            'font' => array(
                'bold' => false,
                'color' => array(
                    'rgb' => '000000'
                ),
                'name' => 'Tahoma',
                'size' => 14
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font' => array(
                'bold' => false,
                'color' => array(
                    'rgb' => '000000'
                ),
                'name' => 'Tahoma',
                'size' => 12
            )
        );
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        
        
        $objPHPExcel->getActiveSheet()->setTitle("Complaint Report");
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'NAME');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2', 'UID');
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C2', 'CITY');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D2', 'ZONE');
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E2', 'PLAN');
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('F2', 'Complaint Type');
        $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Complaint User Report');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $j = 3;
        foreach ($datarray as $valdata) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $valdata->uid);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $valdata->firstname . " " . $valdata->lastname);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $valdata->city_name);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $j, $valdata->zone_name);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $j, $valdata->srvname);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $j, $complaintarr[$valdata->ticket_type]);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $j)->applyFromArray($styleArray1);
            
            $j++;
        }
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('reports/complaint_report.xlsx');
        
        
        
    }
    
    
    public function plan_topup_report_excel(){
        $data           = array();
        $postdata       = $this->input->post();
        $strtlast30     = date('Y-m-d', strtotime('today - 30 days'));
        $enddate        = date('Y-m-d');
        $start180       = date('Y-m-d', strtotime('today - 210 days'));
        $enddate180     = date('Y-m-d', strtotime('today - 30 days'));
        $enddate180more = date('Y-m-d', strtotime('today - 180 days'));
        
        
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        $ispcond     = '';
        $ispcond     = " and su.isp_uid='" . $isp_uid . "'";
        
        $sczcond  = $this->user_department_cond();
        $query1   = $this->db->query("SELECT COUNT(su.id) AS totalcount,
        (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon between '" . $strtlast30 . "' and '" . $enddate . "' {$sczcond} {$ispcond})as last30count,
            (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon between '" . $start180 . "' and '" . $enddate180 . "' {$sczcond} {$ispcond})as last180count, 
            (SELECT COUNT(su.id)  FROM `sht_users` su WHERE su.enableuser=1 AND su.expired=0 and 
        su.createdon < '" . $enddate180more . "' {$sczcond} {$ispcond})as more180count FROM `sht_users` su WHERE su.enableuser=1 AND expired=0 {$sczcond} {$ispcond}");
        //echo $this->db->last_query()."<br/>";//die;
        $countarr = $query1->row_array();
        
        $cond = "";
        if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "last30") {
            $cond .= "and su.createdon between '" . $strtlast30 . "' and '" . $enddate . "'";
        } else if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "last180") {
            $cond .= "and su.createdon between '" . $start180 . "' and '" . $enddate180 . "'";
        } else if (isset($postdata['daysfilter']) && $postdata['daysfilter'] == "more180") {
            $cond .= "and  su.createdon < '" . $enddate180more . "'";
        }
        //query to find acctive user 
        
        $Uquery   = $this->db->query("SELECT su.id,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname FROM sht_users su
        INNER JOIN sht_services ss ON (su.baseplanid=ss.srvid) 
        INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
        INNER JOIN `sht_zones` sz ON (sz.id=su.zone) WHERE su.enableuser=1 AND expired=0 {$sczcond} {$cond} {$ispcond}");
        //echo $this->db->last_query()."<br/>";
        $userarr  = array();
        $userdarr = array();
        foreach ($Uquery->result() as $val) {
            $userdarr[$val->uid] = $val;
            $userarr[]           = $val->uid;
        }
        //query to get online uers
        $activeuserid = "";
        if (!empty($userarr)) {
            
            
            $userids = '"' . implode('", "', $userarr) . '"';
            
            
            //query to get user top associated
            $topupq        = $this->db->query("SELECT count(sut.`uid`) as usercount,ss.srvname,ss.srvid,sut.topup_id FROM `sht_usertopupassoc` sut
                INNER JOIN sht_services ss ON (ss.srvid=sut.topup_id)   WHERE sut.terminate=0 AND `uid` IN ($userids) GROUP BY sut.topup_id order by usercount desc");
            //   echo $this->db->last_query()."<br/>";
            $topupass      = array();
            $planfilterarr = array();
            $i             = 0;
            foreach ($topupq->result() as $valtop) {
                
                $topupass[$valtop->srvname]                = (int) $valtop->usercount;
                $planfilterarr[$valtop->srvid . "::topup"] = $valtop->srvname . " (" . (int) $valtop->usercount . " Users)";
            }
            
            
            
            
            //query to get user top associated
            $planq   = $this->db->query("SELECT COUNT(su.`uid`) AS usercount,ss.srvname,ss.srvid FROM `sht_users` su
                INNER JOIN sht_services ss ON (ss.srvid=su.baseplanid)   WHERE  `uid` IN ($userids) GROUP BY su.`baseplanid` order by usercount desc");
            // echo $this->db->last_query()."<br/>";
            $planass = array();
            $i       = 0;
            foreach ($planq->result() as $valp) {
                
                $planass[$valp->srvname]                = (int) $valp->usercount;
                $planfilterarr[$valp->srvid . "::plan"] = $valp->srvname . " (" . (int) $valp->usercount . " Users) ";
                $i++;
            }
            
            $data['plangraph']    = $planass;
            $data['topupgraph']   = $topupass;
            $data['plandropdown'] = $planfilterarr;
            $datarray             = array();
            $datarray1            = array();
            $datarray2            = array();
            
            if (isset($postdata['planfilter']) && $postdata['planfilter'] == "plan") {
                $id = $postdata['id'];
                
                
                $dataquery = $this->db->query("SELECT su.id,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname FROM sht_users su
        INNER JOIN sht_services ss ON (su.baseplanid=ss.srvid) 
        INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
        INNER JOIN `sht_zones` sz ON (sz.id=su.zone) WHERE su.enableuser=1 AND expired=0 {$sczcond} {$cond} and su.baseplanid='" . $id . "'");
                //   echo $this->db->last_query()."<br/>";  
                $i         = 0;
                foreach ($dataquery->result() as $valp) {
                    $datarray[$i] = $valp;
                    $i++;
                }
                
                
            } else if (isset($postdata['planfilter']) && $postdata['planfilter'] == "topup") {
                $id        = $postdata['id'];
                $datarray  = array();
                $dataquery = $this->db->query(" SELECT su.id,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,sut.topup_id,
              su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname   FROM sht_usertopupassoc sut
               INNER JOIN  `sht_users` su ON (sut.uid=su.uid)
              INNER JOIN sht_services ss ON (sut.topup_id=ss.srvid) 
             INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
             INNER JOIN `sht_zones` sz ON (sz.id=su.zone)
             WHERE su.enableuser=1  AND expired=0 {$sczcond} {$cond} {$ispcond} and sut.topup_id='" . $id . "' GROUP BY sut.topup_id");
                $i         = 0;
                echo $this->db->last_query() . "<br/>";
                foreach ($dataquery->result() as $valp) {
                    $datarray[$i] = $valp;
                    $i++;
                }
            } else {
                $dataquery = $this->db->query("SELECT su.id,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname FROM sht_users su
                INNER JOIN sht_services ss ON (su.baseplanid=ss.srvid) 
                INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
                INNER JOIN `sht_zones` sz ON (sz.id=su.zone) WHERE su.enableuser=1 AND expired=0 {$sczcond} {$cond} {$ispcond}");
                $i         = 0;
                //  echo $this->db->last_query()."<br/>";
                foreach ($dataquery->result() as $valp) {
                    $datarray1[$i] = $valp;
                    $i++;
                }
                
                $dataquery1 = $this->db->query(" SELECT su.id,su.`uid`,sz.zone_name,sc.city_name,su.inactivate_type,su.`firstname`,sut.topup_id,
              su.`lastname`,su.`email`,su.`createdon`,su.`baseplanid`,ss.srvname   FROM sht_usertopupassoc sut
               INNER JOIN  `sht_users` su ON (sut.uid=su.uid)
              INNER JOIN sht_services ss ON (sut.topup_id=ss.srvid) 
             INNER JOIN `sht_cities` sc ON (sc.city_id=su.city)
             INNER JOIN `sht_zones` sz ON (sz.id=su.zone)
             WHERE su.enableuser=1  AND expired=0 {$sczcond} {$cond} {$ispcond} GROUP BY sut.topup_id");
                //   echo $this->db->last_query()."<br/>";
                foreach ($dataquery1->result() as $valp2) {
                    $datarray2[$i] = $valp2;
                    $i++;
                }
                
                $datarray = array_merge($datarray1, $datarray2);
                
            }
            
            if (count($datarray) > 0) {
                $this->plantopup_excel_create($datarray);
                $data['resultcode'] = 1;
                $data['msg']        = "Success";
            } else {
                $data['resultcode'] = 0;
                $data['msg']        = "No Data found";
            }
            
            
            
        } else {
            $data['resultcode'] = 0;
            $data['msg']        = "No Data found";
        }
        echo json_encode($data);
    }
    
    public function plantopup_excel_create($datarray){
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray  = array(
            'font' => array(
                'bold' => false,
                'color' => array(
                    'rgb' => '000000'
                ),
                'name' => 'Tahoma',
                'size' => 14
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font' => array(
                'bold' => false,
                'color' => array(
                    'rgb' => '000000'
                ),
                'name' => 'Tahoma',
                'size' => 12
            )
        );
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        
        
        $objPHPExcel->getActiveSheet()->setTitle("Plan Topup Report");
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'NAME');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2', 'UID');
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C2', 'CITY');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D2', 'ZONE');
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E2', 'PLAN');
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Plan Topup Report');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $j = 3;
        foreach ($datarray as $valdata) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $valdata->uid);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $valdata->firstname . " " . $valdata->lastname);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $valdata->city_name);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $j, $valdata->zone_name);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $j)->applyFromArray($styleArray1);
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $j, $valdata->srvname);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $j)->applyFromArray($styleArray1);
            
            
            $j++;
        }
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('reports/plantopup_report.xlsx');
        
        
        
    }
    

/**************** CUSTOM REPORT SECTION ******************************/
    
    /*
     * Payment users report
     */
    
    public function getcustomer_data($uuid){
        $userQuery = $this->db->get_where('sht_users', array('uid' => $uuid));
        $num_rows = $userQuery->num_rows();
        return $userQuery->row();
    }

    public function getfullname($uuid){
        $userQuery = $this->db->query("SELECT firstname,lastname FROM sht_users WHERE uid='".$uuid."'");
        $num_rows = $userQuery->num_rows();
        $rowdata = $userQuery->row();
        return $rowdata->firstname.' '.$rowdata->lastname;
    }
    
    public function getcustid($uuid){
        $userQuery = $this->db->query("SELECT id FROM sht_users WHERE uid='".$uuid."'");
        $num_rows = $userQuery->num_rows();
        $rowdata = $userQuery->row();
        return $rowdata->id;
    }
    
    public function addtowallet_sheet(){
        $data = array();
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        //$date_range = '26.08.2017 - 26.08.2017';
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $limit = $postdata['limit'];
        $offset = $postdata['offset'];
        $search_user = $postdata['search_user'];
        
        $cwhere = '1';
        if($search_user != ''){
            $cwhere .= ' AND subscriber_uuid LIKE \'%'.$search_user.'%\' ';
        }
        
        $gen = '';
        if($offset == 0){
            $gen = '<thead><tr class="active"><th>&nbsp;</th><th>UserID</th><th>Name</th><th>Amount Added</th><th>Payment Via</th><th>Added On</th></tr></thead>';
        }
        
        //$sczcond = $this->user_department_cond();
        $sczcond = '';
        $totalwalletlist = $this->db->query("SELECT subscriber_uuid,total_amount,payment_mode,bill_added_on,wallet_amount_received FROM sht_subscriber_billing WHERE $cwhere AND isp_uid='".$isp_uid."' AND bill_type='addtowallet' AND DATE(bill_added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' $sczcond ORDER BY bill_added_on DESC");
        $total_records = $totalwalletlist->num_rows();
        
        $walletQ = $this->db->query("SELECT subscriber_uuid,total_amount,payment_mode,bill_added_on,wallet_amount_received FROM sht_subscriber_billing WHERE $cwhere AND isp_uid='".$isp_uid."' AND bill_type='addtowallet' AND DATE(bill_added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' $sczcond ORDER BY bill_added_on DESC LIMIT $offset,$limit");
        if($walletQ->num_rows() > 0){
            $gen .= '<tbody>';
            $i = ($offset + 1);
            foreach($walletQ->result() as $wobj){
                $uuid = $wobj->subscriber_uuid;
                $fullname = $this->getfullname($uuid);
                $custid = $this->getcustid($uuid);
                $total_amount = number_format($wobj->total_amount, 2);
                $gen .= '<tr><td>'.$i.'</td><td><a href="'.base_url().'user/edit/'.$custid.'" target="_blank">'.$uuid.'</a></td><td>'.$fullname.'</td><td>'.$total_amount.'</td><td>'.$wobj->payment_mode.'</td><td>'.$wobj->bill_added_on.'</td></tr>';
                $i++;
            }
            $gen .= '</tbody>';
        }else{
            $gen .= '<tbody><tr><td colspan="6" style="text-align:center">No Records Found</td></tr></tbody>';
        }
        
        $nxtoffset = ($limit + $offset);
        $data['reportlist_total'] = $total_records;
        $data['reportlist'] = $gen;
        $data['offset'] = $nxtoffset;
        if($total_records > $nxtoffset){
            $data['loadmore'] = 1;
        }else{
            $data['loadmore'] = 0;
        }
        echo json_encode($data);
    }
    
    public function exportaddtowallet_sheet(){
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 11
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            )
        );
        
        $styleArray2 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F28A8C')
            )
        );
        
        $titlestyleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 14
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->setTitle("Wallet Payment Report");
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A1','Payment Report: '.$date_range);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($titlestyleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A2','UID');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2','Fullname');
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C2','Amount Added');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D2','Payment Via');
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E2','Receipt Number');
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('F2','Cheque_DD_Paytm_Number');
        $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('G2','Account Number');
        $objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('H2','branch_name');
        $objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('I2','Branch Address');
        $objPHPExcel->getActiveSheet()->getStyle('I2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('J2','Bankslip Issuedate');
        $objPHPExcel->getActiveSheet()->getStyle('J2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('K2','Payment AddedOn');
        $objPHPExcel->getActiveSheet()->getStyle('K2')->applyFromArray($styleArray);
        
        $j = 3; $total_payment = 0;
        //$sczcond = $this->user_department_cond();
        $sczcond = '';
        $totalwalletlist = $this->db->query("SELECT tb1.id, tb1.subscriber_uuid, tb1.total_amount, tb1.payment_mode, tb1.bill_added_on, tb1.wallet_amount_received, tb1.receipt_number FROM sht_subscriber_billing as tb1 WHERE tb1.isp_uid='".$isp_uid."' AND tb1.bill_type='addtowallet' AND DATE(tb1.bill_added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' $sczcond ORDER BY tb1.bill_added_on DESC");
        $total_records = $totalwalletlist->num_rows();
        if($total_records > 0){
            foreach($totalwalletlist->result() as $wobj){
                $uuid = $wobj->subscriber_uuid;
                $fullname = $this->getfullname($uuid);
                $custid = $this->getcustid($uuid);
                $total_amount = number_format($wobj->total_amount, 2, '.', '');
                $paymode = $wobj->payment_mode;
                $receipt_number = (int)$wobj->receipt_number;
                $billid = $wobj->id;
                $bill_added_on = $wobj->bill_added_on;
                $wallet_amount_received = $wobj->wallet_amount_received;
                
                $total_payment += $total_amount;
                $cheque_dd_paytm_number = '';
                $account_number = '';
                $branch_address = '';
                $slip_issuedate = '';
                $branch_name = '';
                if($paymode != 'cash'){
                    $receiptQ = $this->db->query("SELECT tb2.cheque_dd_paytm_number, tb2.account_number, tb2.branch_address, tb2.bankslip_issued_date, tb2.branch_name FROM sht_subscriber_receipt_history as tb2 WHERE tb2.bill_id='".$billid."'");
                    if($receiptQ->num_rows() > 0){
                        $recptrowdata = $receiptQ->row();
                        $cheque_dd_paytm_number = $recptrowdata->cheque_dd_paytm_number;
                        $account_number = ($recptrowdata->account_number !== NULL) ? $recptrowdata->account_number : '';
                        $branch_address = ($recptrowdata->branch_address !== NULL) ? $recptrowdata->branch_address : '';
                        $slip_issuedate = ($recptrowdata->bankslip_issued_date !== NULL) ? $recptrowdata->bankslip_issued_date : '';
                        $branch_name = ($recptrowdata->branch_name !== NULL) ? $recptrowdata->branch_name : '';
                    }
                }
                
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $uuid);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $fullname);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
                if($wallet_amount_received != 1){
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $total_amount);
                    $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray2);
                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $total_amount);
                    $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $paymode);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$j, $receipt_number);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
                
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$j, (int)$cheque_dd_paytm_number);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$j, (int)$account_number);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$j, $branch_name);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($styleArray1);           
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$j, $branch_address);
                $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$j, $slip_issuedate);
                $objPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('K'.$j, $bill_added_on);
                $objPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray($styleArray1);
                
                $j++;
            }
            
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$j, 'Total Amount');
            $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $total_payment);
            $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray);
        }
        
        
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('reports/'.$isp_uid.'_customreport.xlsx');
        
        echo json_encode(true);
    }

    
    
    /*
     * New Users, Suspended, terminated users report
     */
    
    public function getstatename($stateid){
        $name = '';
        $stateQ = $this->db->get_where('sht_states', array('id' => $stateid));
        $num_rows = $stateQ->num_rows();
        if($num_rows > 0){
                $data = $stateQ->row();
                $name = ucwords($data->state);
        }
        return $name;
    }
    public function getcityname($stateid, $cityid){
        $name = '';
        $cityQ = $this->db->get_where('sht_cities', array('city_state_id' => $stateid, 'city_id' => $cityid));
        $num_rows = $cityQ->num_rows();
        if($num_rows > 0){
                $data = $cityQ->row();
                $name = ucwords($data->city_name);
        }
        return $name;
    }
    public function getzonename($zoneid){
        $name = '';
        $zoneQ = $this->db->get_where('sht_zones', array('id' => $zoneid));
        $num_rows = $zoneQ->num_rows();
        if($num_rows > 0){
                $data = $zoneQ->row();
                $name = ucwords($data->zone_name);
        }
        return $name;
    }


    public function getplanname($srvid){
	$planname = '-';
	$query = $this->db->query("SELECT tb2.srvname FROM sht_services as tb2 WHERE tb2.srvid='".$srvid."'");
	if($query->num_rows() > 0){
	    $planname = $query->row()->srvname;
	}
	return $planname;
    }
    public function userbalance_amount($uuid){
        $wallet_amt = 0;
        $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
        if($walletQ->num_rows() > 0){
            $wallet_amt = $walletQ->row()->wallet_amt;
        }
        
        $passbook_amt = 0;
        $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
        if($passbookQ->num_rows() > 0){
            $passbook_amt = $passbookQ->row()->plan_cost;
        }
        $balanceamt = $wallet_amt - $passbook_amt;
        return $balanceamt;
    }



    public function userslist_sheet($report_type){
        $data = array();
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        //$date_range = '26.08.2017 - 26.08.2017';
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $limit = $postdata['limit'];
        $offset = $postdata['offset'];

        $gen = ''; $cwhere = '1';
        $search_user = $postdata['search_user'];
        if($search_user != ''){
            $cwhere .= ' AND (uid LIKE \'%'.$search_user.'%\' OR firstname LIKE \'%'.$search_user.'%\') ';
        }
        
        if($offset == 0){
            if($report_type == 'newusers_sheet'){
                $gen = '<thead><tr class="active"><th>&nbsp;</th><th>UserID</th><th>Name</th><th>Mobile</th><th>Account Type</th><th>Plan</th><th>City</th><th>Zone</th><th>Added On</th></tr></thead>';
            }elseif($report_type == 'suspendedusers_sheet'){
                $gen = '<thead><tr class="active"><th>&nbsp;</th><th>UserID</th><th>Name</th><th>Mobile</th><th>Account Type</th><th>Plan</th><th>City</th><th>Zone</th><th>Suspended On</th></tr></thead>';
            }elseif($report_type == 'terminatedusers_sheet'){
                $gen = '<thead><tr class="active"><th>&nbsp;</th><th>UserID</th><th>Name</th><th>Mobile</th><th>Account Type</th><th>Plan</th><th>City</th><th>Zone</th><th>Terminated On</th></tr></thead>';
            }
        }
        
        if($report_type == 'newusers_sheet'){
            $cwhere .= " AND DATE(account_activated_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' ";
        }elseif($report_type == 'suspendedusers_sheet'){
            $cwhere .= " AND inactivate_type = 'suspended' AND suspendedon BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' ";
        }elseif($report_type == 'terminatedusers_sheet'){
            $cwhere .= " AND inactivate_type = 'terminate' AND suspendedon BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' ";
        }
        
        $sczcond = $this->user_department_cond();
        $totaluserlist = $this->db->query("SELECT id, uid, enableuser, firstname, lastname, state, city, zone, account_activated_on, paidtill_date, expiration, usuage_locality, inactivate_type,mobile,user_plan_type FROM sht_users WHERE $cwhere $sczcond AND isp_uid='".$isp_uid."' ORDER by id DESC");
        $total_records = $totaluserlist->num_rows();
        
        $userlistQ = $this->db->query("SELECT id, isp_uid, uid, username, enableuser, firstname, lastname, email, flat_number, address, address2, state, city, zone, account_activated_on, gstin_number, expiration, usuage_locality, inactivate_type, mobile, alt_mobile, user_plan_type, baseplanid, suspendedon FROM sht_users WHERE $cwhere $sczcond AND isp_uid='".$isp_uid."' ORDER by id DESC LIMIT $offset, $limit");
        if($userlistQ->num_rows() > 0){
            $gen .= '<tbody>';
            $i = ($offset + 1);
            foreach($userlistQ->result() as $urowdata){
                $custid = $urowdata->id;
                $isp_uid = $urowdata->isp_uid;
                $uuid = $urowdata->uid;
                $username = $urowdata->username;
                $firstname = $urowdata->firstname;
                $lastname = $urowdata->lastname;
                
                $fullname = $firstname .' '. $lastname;
                $mobile = $urowdata->mobile;
                $alt_mobile = $urowdata->alt_mobile;
                $flat_number = $urowdata->flat_number;
                $address = $urowdata->address;
                $address2 = $urowdata->address2;
                $state = $this->getstatename($urowdata->state);
                $city = $this->getcityname($urowdata->state,$urowdata->city);
                $zone = $this->getzonename($urowdata->zone);
                $expiration = date('d-m-Y', strtotime($urowdata->expiration));
                $gstin_number = $urowdata->gstin_number;
                $baseplanid = $urowdata->baseplanid;
                $email = $urowdata->email;
                $user_plan_type = $urowdata->user_plan_type;
                $planname = $this->getplanname($baseplanid);
                
                if($report_type == 'newusers_sheet'){
                    $addedon = date('d-m-Y', strtotime($urowdata->account_activated_on));
                }elseif($report_type == 'suspendedusers_sheet'){
                    $addedon = date('d-m-Y', strtotime($urowdata->suspendedon));
                }elseif($report_type == 'terminatedusers_sheet'){
                    $addedon = date('d-m-Y', strtotime($urowdata->suspendedon));
                }
                
                $gen .= '<tr><td>'.$i.'</td><td><a href="'.base_url().'user/edit/'.$custid.'" target="_blank">'.$uuid.'</a></td><td>'.$fullname.'</td><td>'.$mobile.'</td><td>'.$user_plan_type.'</td><td>'.$planname.'</td><td>'.$city.'</td><td>'.$zone.'</td><td>'.$addedon.'</td></tr>';
                $i++;
            }
        }else{
            $gen .= '<tbody><tr><td colspan="6" style="text-align:center">No Records Found</td></tr></tbody>';
        }
        
        $nxtoffset = ($limit + $offset);
        $data['reportlist_total'] = $total_records;
        $data['reportlist'] = $gen;
        $data['offset'] = $nxtoffset;
        if($total_records > $nxtoffset){
            $data['loadmore'] = 1;
        }else{
            $data['loadmore'] = 0;
        }
        echo json_encode($data);
        
    }
    
    public function exportuserslist_sheet($report_type){
        $data = array();
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        //$date_range = '26.08.2017 - 26.08.2017';
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 11
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            )
        );
        $styleArray2 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F28A8C')
            )
        );
        
        $titlestyleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 14
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
       
        $objPHPExcel->getActiveSheet()->setTitle("Users Listing");
        if($report_type == 'newusers_sheet'){
            $objPHPExcel->getActiveSheet()->setCellValue('A1','New Users Report: '.$date_range);    
        }elseif($report_type == 'suspendedusers_sheet'){
            $objPHPExcel->getActiveSheet()->setCellValue('A1','Suspended Users Report: '.$date_range);    
        }elseif($report_type == 'terminatedusers_sheet'){
            $objPHPExcel->getActiveSheet()->setCellValue('A1','Terminated Users Report: '.$date_range);    
        }
        
        $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($titlestyleArray);
        
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A2','UID');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2','Username');
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C2','Name');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D2','Account Type');
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E2','Email');
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('F2','Mobile');
        $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('G2','Alternate Mobile');
        $objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('H2','GSTIN Number');
        $objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('I2','Expired On');
        $objPHPExcel->getActiveSheet()->getStyle('I2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('J2','Wallet Amount');
        $objPHPExcel->getActiveSheet()->getStyle('J2')->applyFromArray($styleArray);        
        $objPHPExcel->getActiveSheet()->setCellValue('K2','Active Plan');
        $objPHPExcel->getActiveSheet()->getStyle('K2')->applyFromArray($styleArray);

        if($report_type == 'newusers_sheet'){
            $objPHPExcel->getActiveSheet()->setCellValue('L2','Added On');
            $objPHPExcel->getActiveSheet()->getStyle('L2')->applyFromArray($styleArray);
        }elseif($report_type == 'suspendedusers_sheet'){
            $objPHPExcel->getActiveSheet()->setCellValue('L2','Suspended On');
            $objPHPExcel->getActiveSheet()->getStyle('L2')->applyFromArray($styleArray);
        }elseif($report_type == 'terminatedusers_sheet'){
            $objPHPExcel->getActiveSheet()->setCellValue('L2','Terminated On');
            $objPHPExcel->getActiveSheet()->getStyle('L2')->applyFromArray($styleArray);
        }
        
        $objPHPExcel->getActiveSheet()->setCellValue('M2','Address');
        $objPHPExcel->getActiveSheet()->getStyle('M2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('N2','State');
        $objPHPExcel->getActiveSheet()->getStyle('N2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('O2','City');
        $objPHPExcel->getActiveSheet()->getStyle('O2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('P2','Zone');
        $objPHPExcel->getActiveSheet()->getStyle('P2')->applyFromArray($styleArray);
        
        $cwhere = '1';
        if($report_type == 'newusers_sheet'){
            $cwhere .= " AND DATE(account_activated_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' ";
        }elseif($report_type == 'suspendedusers_sheet'){
            $cwhere .= " AND inactivate_type = 'suspended' AND suspendedon BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' ";
        }elseif($report_type == 'terminatedusers_sheet'){
            $cwhere .= " AND inactivate_type = 'terminate' AND suspendedon BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' ";
        }
        
        $sczcond = $this->user_department_cond();        
        $userlistQ = $this->db->query("SELECT id, isp_uid, uid, username, enableuser, firstname, lastname, email, flat_number, address, address2, state, city, zone, account_activated_on, gstin_number, expiration, usuage_locality, inactivate_type, mobile, alt_mobile, user_plan_type, baseplanid,suspendedon FROM sht_users WHERE $cwhere $sczcond AND isp_uid='".$isp_uid."' ORDER by id DESC");
        if($userlistQ->num_rows() > 0){
            $today_date = date('d-m-Y');
            $j = 3;
            foreach($userlistQ->result() as $urowdata){
                $custid = $urowdata->id;
                $isp_uid = $urowdata->isp_uid;
                $uuid = $urowdata->uid;
                $username = $urowdata->username;
                $firstname = $urowdata->firstname;
                $lastname = $urowdata->lastname;
                
                $fullname = $firstname .' '. $lastname;
                $mobile = $urowdata->mobile;
                $alt_mobile = $urowdata->alt_mobile;
                $flat_number = $urowdata->flat_number;
                $address = $urowdata->address;
                $address2 = $urowdata->address2;
                $state = $this->getstatename($urowdata->state);
                $city = $this->getcityname($urowdata->state,$urowdata->city);
                $zone = $this->getzonename($urowdata->zone);
                $expiration = date('d-m-Y', strtotime($urowdata->expiration));
                $gstin_number = $urowdata->gstin_number;
                $baseplanid = $urowdata->baseplanid;
                $email = $urowdata->email;
                $user_plan_type = $urowdata->user_plan_type;
                $wallet_amount = $this->userbalance_amount($uuid);
                
                $planname = $this->getplanname($baseplanid);
                if($report_type == 'newusers_sheet'){
                    $addedon = date('d-m-Y', strtotime($urowdata->account_activated_on));
                }elseif($report_type == 'suspendedusers_sheet'){
                    $addedon = date('d-m-Y', strtotime($urowdata->suspendedon));
                }elseif($report_type == 'terminatedusers_sheet'){
                    $addedon = date('d-m-Y', strtotime($urowdata->suspendedon));
                }
                
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $uuid);
                if(strtotime($expiration) > strtotime($today_date) ){
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
                }else{
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray2);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$j,$username);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$j,$fullname);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$j,$user_plan_type);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$j,$email);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$j,$mobile);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$j,$alt_mobile);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$j,$gstin_number);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$j,$expiration);
                
                if(strtotime($expiration) > strtotime($today_date) ){
                    $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($styleArray1);
                }else{
                    $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($styleArray2);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$j,$wallet_amount);
                $objPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('K'.$j,$planname);
                $objPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray($styleArray1);
                
                if($report_type == 'newusers_sheet'){
                    $objPHPExcel->getActiveSheet()->setCellValue('L'.$j,$addedon);
                    $objPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray($styleArray1);
                }elseif($report_type == 'suspendedusers_sheet'){
                    $objPHPExcel->getActiveSheet()->setCellValue('L'.$j,$addedon);
                    $objPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray($styleArray1);
                }elseif($report_type == 'terminatedusers_sheet'){
                    $objPHPExcel->getActiveSheet()->setCellValue('L'.$j,$addedon);
                    $objPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray($styleArray1);
                }
                
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$j, $flat_number.', '. $address.', '.$address2);
                $objPHPExcel->getActiveSheet()->getStyle('M'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('N'.$j,$state);
                $objPHPExcel->getActiveSheet()->getStyle('N'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('O'.$j,$city);
                $objPHPExcel->getActiveSheet()->getStyle('O'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('P'.$j,$zone);
                $objPHPExcel->getActiveSheet()->getStyle('P'.$j)->applyFromArray($styleArray1);
                
                $j++;
            }
        }
        
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('reports/'.$isp_uid.'_customreport.xlsx');
        
        echo json_encode(true);
        
    }

    /*
     * Online, Offline users report
     */
    
    public function userstatus_sheet(){
        $data = array();
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        //$date_range = '26.08.2017 - 26.08.2017';
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $limit = $postdata['limit'];
        $offset = $postdata['offset'];
        
        $gen = ''; $cwhere = '1';
        if($offset == 0){
            $gen = '<thead><tr class="active"><th>&nbsp;</th><th>Total Online Users</th><th>Total Offline Users</th></tr></thead>';
        }
        
        $uonline = 0; $uoffline = 0;
        $offlineuids = array(); $onlineuids = array();
        $sczcond = $this->user_department_cond();
        $countQ = $this->db->query("SELECT uid FROM sht_users WHERE $cwhere $sczcond AND isp_uid='".$isp_uid."' AND (enableuser='1' OR (enableuser='0' AND inactivate_type != '' AND inactivate_type != 'terminate' ))");
        $result_count = $countQ->num_rows();
        if($result_count > 0){
            foreach($countQ->result() as $cobj){
                $uuid = $cobj->uid;
                $onoffQ = $this->db->query("SELECT radacctid FROM radacct WHERE username='".$uuid."' AND acctstoptime BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' GROUP BY username");
                if($onoffQ->num_rows() > 0){
                    $uonline++;
                    $onlineuids[] = $cobj->uid;
                }else{
                    $uoffline++;
                    $offlineuids[] = $cobj->uid;
                }
            }
            
            $gen .= '<tbody><tr><td>&nbsp;</td><td>'.$uonline.'</td><td>'.$uoffline.'</td></tr><tr><td colspan="3" style="text-align:center">Export Excel for Username List</td></tr></tbody>';
        }else{
            $gen .= '<tbody><tr><td colspan="3" style="text-align:center">No Records Found</td></tr></tbody>';
        }
        
        $total_records = 2;
        $nxtoffset = ($limit + $offset);
        $data['reportlist_total'] = $total_records;
        $data['reportlist'] = $gen;
        $data['offset'] = $nxtoffset;
        if($total_records > $nxtoffset){
            $data['loadmore'] = 1;
        }else{
            $data['loadmore'] = 0;
        }
        echo json_encode($data);
        
    }
    
    public function exportuserstatus_sheet(){
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        //$date_range = '26.08.2017 - 26.08.2017';
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 11
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            )
        );
        $styleArray2 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F28A8C')
            )
        );
        
        $titlestyleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 14
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
       
        $objPHPExcel->getActiveSheet()->setTitle("Users Listing");
        $objPHPExcel->getActiveSheet()->setCellValue('A1','Online/Offline Users Report: '.$date_range);    
        $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($titlestyleArray);
        
        $uonline = 0; $uoffline = 0;
        $offlineuids = array(); $onlineuids = array();
        $cwhere = '1';
        $sczcond = $this->user_department_cond();
        $countQ = $this->db->query("SELECT uid FROM sht_users WHERE $cwhere $sczcond AND isp_uid='".$isp_uid."' AND (enableuser='1' OR (enableuser='0' AND inactivate_type != '' AND inactivate_type != 'terminate' ))");
        $result_count = $countQ->num_rows();
        if($result_count > 0){
            foreach($countQ->result() as $cobj){
                $uuid = $cobj->uid;
                $onoffQ = $this->db->query("SELECT radacctid FROM radacct WHERE username='".$uuid."' AND acctstoptime BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' GROUP BY username");
                if($onoffQ->num_rows() > 0){
                    $uonline++;
                    $onlineuids[] = $cobj->uid;
                }else{
                    $uoffline++;
                    $offlineuids[] = $cobj->uid;
                }
            }
            
        }
        
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "ONLINE USERS ($uonline)");
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2', "OFFLINE USERS ($uoffline)");
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        
        if((count($onlineuids) > 0) || (count($offlineuids) > 0)){
            $on = 3; $off = 3;
            foreach($onlineuids as $onuid){
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$on, $onuid);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$on)->applyFromArray($styleArray1);
                $on++;
            }
            
            foreach($offlineuids as $offuid){
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$off, $offuid);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$off)->applyFromArray($styleArray1);
                $off++;
            }
        }
        
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('reports/'.$isp_uid.'_customreport.xlsx');
        
        echo json_encode(true);
        
    }



    /*
     * Invoices users report
     */
    public function getuser_gstnumber($uuid){
        $userQuery = $this->db->query("SELECT gstin_number FROM sht_users WHERE uid='".$uuid."'");
        $num_rows = $userQuery->num_rows();
        $rowdata = $userQuery->row();
        return $rowdata->gstin_number;
    }
    public function usersinvoice_sheet(){
        $data = array();
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        //$date_range = '26.08.2017 - 26.08.2017';
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $limit = $postdata['limit'];
        $offset = $postdata['offset'];
        
        $cwhere = '1';
        $search_user = $postdata['search_user'];
        if($search_user != ''){
            $cwhere .= ' AND subscriber_uuid LIKE \'%'.$search_user.'%\' ';
        }
        
        $gen = '';
        if($offset == 0){
            $gen = '<thead><tr class="active"><th>&nbsp;</th><th>UserID</th><th>Name</th><th>Bill Type</th><th>PayFor</th><th>Bill Number</th><th>Amount Added</th><th>Added On</th></tr></thead>';
        }
        
        $geninvoicearr = array();
        $custinvoicearr = array();
        
        //$sczcond = $this->user_department_cond();
        $sczcond = '';
        $totalinvoicelist = $this->db->query("SELECT subscriber_uuid as uid, bill_type,bill_number, total_amount, plan_id, payment_mode, bill_added_on as added_on FROM sht_subscriber_billing WHERE $cwhere AND bill_type != 'addtowallet' AND isp_uid='".$isp_uid."' AND DATE(bill_added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' $sczcond ORDER BY subscriber_uuid ASC");
        $invoice_records = $totalinvoicelist->num_rows();
        if($invoice_records > 0){
            $geninvoicearr = $totalinvoicelist->result_array();
        }
        
        $cwhere = '1';
        if($search_user != ''){
            $cwhere .= ' AND uid LIKE \'%'.$search_user.'%\' ';
        }
        
        $custinv_start = count($geninvoicearr);
        $custominvoicelist = $this->db->query("SELECT uid,bill_type, bill_number,total_amount,bill_details,payment_mode,added_on FROM sht_subscriber_custom_billing WHERE $cwhere AND bill_type = 'custom_bill' AND isp_uid='".$isp_uid."' AND DATE(added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' AND is_deleted='0' ORDER BY uid ASC");
        $custom_records = $custominvoicelist->num_rows();
        if($custom_records > 0){
            foreach($custominvoicelist->result() as $custinvobj){
                $custinvoicearr[$custinv_start] = $custominvoicelist->row_array();
                $custinv_start++;    
            }
        }
        
        $total_records = ($invoice_records + $custom_records);
        
        //Total Invoices Array
        $finalinvoicesarr = ($geninvoicearr + $custinvoicearr);
        //$gen .=  '<pre>'; print_r($geninvoicearr); print_r($custinvoicearr); print_r($finalinvoicesarr); '</pre>';
        
        if(count($finalinvoicesarr) > 0){
            $finalinvoicesarr = array_slice($finalinvoicesarr, $offset, $limit);
            $i = ($offset + 1);
            foreach($finalinvoicesarr as $invobj){
                $uuid = $invobj['uid'];
                $fullname = $this->getfullname($uuid);
                $custid = $this->getcustid($uuid);
                $total_amount = number_format($invobj['total_amount'], 2);
                $billtype = $invobj['bill_type'];
                $paymode = $invobj['payment_mode'];
                $bill_added_on = $invobj['added_on'];
                $billno = $invobj['bill_number'];
                $bill_details = '';
                if(isset($invobj['plan_id']) && $invobj['plan_id'] !== NULL){
                    $bill_details = $this->getplanname($invobj['plan_id']);
                }
                if(isset($invobj['bill_details']) && $invobj['bill_details'] !== NULL){
                    $bill_details = $invobj['bill_details'];
                }
                
                $gen .= '<tr><td>'.$i.'</td><td><a href="'.base_url().'user/edit/'.$custid.'" target="_blank">'.$uuid.'</a></td><td>'.$fullname.'</td><td>'.$billtype.'</td><td>'.$bill_details.'</td><td>'.$billno.'</td><td>'.$total_amount.'</td><td>'.$bill_added_on.'</td></tr>';
                $i++;
            }
        }else{
            $gen .= '<tbody><tr><td colspan="8" style="text-align:center">No Records Found</td></tr></tbody>';
        }
        
        $nxtoffset = ($limit + $offset);
        $data['reportlist_total'] = $total_records;
        $data['reportlist'] = $gen;
        $data['offset'] = $nxtoffset;
        if($total_records > $nxtoffset){
            $data['loadmore'] = 1;
        }else{
            $data['loadmore'] = 0;
        }
        echo json_encode($data);
    }
    
    public function exportusersinvoice_sheet(){
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 11
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            )
        );
        
        $styleArray2 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F28A8C')
            )
        );
        
        $titlestyleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 14
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->setTitle("Invoices Report");
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A1','Users Invoice Report: '.$date_range);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($titlestyleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A2','UID');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2','Fullname');
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C2','Bill Number');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D2','Bill Type');
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E2','PayFor');
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('F2','Bill Amount');
        $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('G2','Bill AddedOn');
        $objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('H2','GSTIN Number');
        $objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($styleArray);
        
        $geninvoicearr = array();
        $custinvoicearr = array();        
        //$sczcond = $this->user_department_cond();
        $sczcond = '';
        $totalinvoicelist = $this->db->query("SELECT subscriber_uuid as uid, bill_type,bill_number, total_amount, plan_id, payment_mode, bill_added_on as added_on FROM sht_subscriber_billing WHERE bill_type != 'addtowallet' AND isp_uid='".$isp_uid."' AND DATE(bill_added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' $sczcond ORDER BY subscriber_uuid ASC");
        $invoice_records = $totalinvoicelist->num_rows();
        if($invoice_records > 0){
            $geninvoicearr = $totalinvoicelist->result_array();
        }
        
        $custinv_start = count($geninvoicearr);
        $custominvoicelist = $this->db->query("SELECT uid,bill_type, bill_number,total_amount,bill_details,payment_mode,added_on FROM sht_subscriber_custom_billing WHERE bill_type = 'custom_bill' AND isp_uid='".$isp_uid."' AND DATE(added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' AND is_deleted='0' ORDER BY uid ASC");
        $custom_records = $custominvoicelist->num_rows();
        if($custom_records > 0){
            foreach($custominvoicelist->result() as $custinvobj){
                $custinvoicearr[$custinv_start] = $custominvoicelist->row_array();
                $custinv_start++;    
            }
        }
        
        $total_records = ($invoice_records + $custom_records);
        
        //Total Invoices Array
        $finalinvoicesarr = ($geninvoicearr + $custinvoicearr);
        //$gen .=  '<pre>'; print_r($geninvoicearr); print_r($custinvoicearr); print_r($finalinvoicesarr); '</pre>';
        
        if(count($finalinvoicesarr) > 0){
            $j = 3; $total_payment = 0;
            foreach($finalinvoicesarr as $invobj){
                $uuid = $invobj['uid'];
                $fullname = $this->getfullname($uuid);
                $custid = $this->getcustid($uuid);
                $total_amount = number_format($invobj['total_amount'], 2, '.', '');
                $gstnumber = $this->getuser_gstnumber($uuid);
                
                $total_payment += $total_amount;
                $billtype = $invobj['bill_type'];
                $paymode = $invobj['payment_mode'];
                $bill_added_on = $invobj['added_on'];
                $billno = $invobj['bill_number'];
                $bill_details = '';
                if(isset($invobj['plan_id']) && $invobj['plan_id'] !== NULL){
                    $bill_details = $this->getplanname($invobj['plan_id']);
                }
                if(isset($invobj['bill_details']) && $invobj['bill_details'] !== NULL){
                    $bill_details = $invobj['bill_details'];
                }
                
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $uuid);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $fullname);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $billno);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $billtype);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$j, $bill_details);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$j, $total_amount);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$j, $bill_added_on);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$j, $gstnumber);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($styleArray1);
                
                $j++;
            }
            $j++;
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$j, 'Total Amount');
            $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$j, $total_payment);
            $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray);
        }
        
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('reports/'.$isp_uid.'_customreport.xlsx');
        
        echo json_encode(true);
    }
    
    
    /*
     *  Online Payments Report
     */
    
    public function onlinepayments_sheet(){
        $data = array();
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        //$date_range = '26.08.2017 - 26.08.2017';
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $limit = $postdata['limit'];
        $offset = $postdata['offset'];
        $search_user = $postdata['search_user'];
        
        $cwhere = '1';
        if($search_user != ''){
            $cwhere .= ' AND subscriber_uuid LIKE \'%'.$search_user.'%\' ';
        }
        
        $gen = '';
        if($offset == 0){
            $gen = '<thead><tr class="active"><th>&nbsp;</th><th>UserID</th><th>Name</th><th>Amount Added</th><th>Payment Via</th><th>Platform</th><th>Added On</th></tr></thead>';
        }
        
        //$sczcond = $this->user_department_cond();
        $sczcond = '';
        $totalwalletlist = $this->db->query("SELECT subscriber_uuid, total_amount, payment_mode, bill_added_on, wallet_amount_received, payment_platform FROM sht_subscriber_billing WHERE $cwhere AND isp_uid='".$isp_uid."' AND bill_type='addtowallet' AND (payment_mode='netbanking' OR payment_mode='Net Banking') AND DATE(bill_added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' $sczcond ORDER BY bill_added_on DESC");
        $total_records = $totalwalletlist->num_rows();
        
        $walletQ = $this->db->query("SELECT subscriber_uuid, total_amount, payment_mode, bill_added_on, wallet_amount_received, payment_platform FROM sht_subscriber_billing WHERE $cwhere AND isp_uid='".$isp_uid."' AND bill_type='addtowallet' AND (payment_mode='netbanking' OR payment_mode='Net Banking') AND DATE(bill_added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' $sczcond ORDER BY bill_added_on DESC LIMIT $offset,$limit");
        if($walletQ->num_rows() > 0){
            $gen .= '<tbody>';
            $i = ($offset + 1);
            foreach($walletQ->result() as $wobj){
                $uuid = $wobj->subscriber_uuid;
                $fullname = $this->getfullname($uuid);
                $custid = $this->getcustid($uuid);
                $total_amount = number_format($wobj->total_amount, 2);
                $platform = '';
                if($wobj->payment_platform == '0'){
                    $platform = 'Admin Portal';
                }elseif($wobj->payment_platform == '1'){
                    $platform = 'APK';
                }elseif($wobj->payment_platform == '2'){
                    $platform = 'Website';
                }
                $gen .= '<tr><td>'.$i.'</td><td><a href="'.base_url().'user/edit/'.$custid.'" target="_blank">'.$uuid.'</a></td><td>'.$fullname.'</td><td>'.$total_amount.'</td><td>'.$wobj->payment_mode.'</td><td>'.$platform.'</td><td>'.$wobj->bill_added_on.'</td></tr>';
                $i++;
            }
            $gen .= '</tbody>';
        }else{
            $gen .= '<tbody><tr><td colspan="6" style="text-align:center">No Records Found</td></tr></tbody>';
        }
        
        $nxtoffset = ($limit + $offset);
        $data['reportlist_total'] = $total_records;
        $data['reportlist'] = $gen;
        $data['offset'] = $nxtoffset;
        if($total_records > $nxtoffset){
            $data['loadmore'] = 1;
        }else{
            $data['loadmore'] = 0;
        }
        echo json_encode($data);
    }
    
    public function exportonlinepayments_sheet(){
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 11
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            )
        );
        
        $styleArray2 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F28A8C')
            )
        );
        
        $titlestyleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 14
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->setTitle("Wallet Payment Report");
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A1','Payment Report: '.$date_range);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($titlestyleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A2','UID');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2','Fullname');
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C2','Amount Added');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D2','Payment Via');
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E2','Receipt Number');
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('F2','Cheque_DD_Paytm_OnlineTxnID');
        $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('G2','Account Number');
        $objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('H2','branch_name');
        $objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('I2','Branch Address');
        $objPHPExcel->getActiveSheet()->getStyle('I2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('J2','Bankslip Issuedate');
        $objPHPExcel->getActiveSheet()->getStyle('J2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('K2','Payment Platform');
        $objPHPExcel->getActiveSheet()->getStyle('K2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('L2','Payment AddedOn');
        $objPHPExcel->getActiveSheet()->getStyle('L2')->applyFromArray($styleArray);
        
        $j = 3; $total_payment = 0;
        //$sczcond = $this->user_department_cond();
        $sczcond = '';
        $totalwalletlist = $this->db->query("SELECT tb1.id, tb1.subscriber_uuid, tb1.total_amount, tb1.payment_mode, tb1.bill_added_on, tb1.wallet_amount_received, tb1.receipt_number, tb1.payment_platform FROM sht_subscriber_billing as tb1 WHERE tb1.isp_uid='".$isp_uid."' AND tb1.bill_type='addtowallet' AND (payment_mode='netbanking' OR payment_mode='Net Banking') AND DATE(tb1.bill_added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' $sczcond ORDER BY tb1.bill_added_on DESC");
        $total_records = $totalwalletlist->num_rows();
        if($total_records > 0){
            foreach($totalwalletlist->result() as $wobj){
                $uuid = $wobj->subscriber_uuid;
                $fullname = $this->getfullname($uuid);
                $custid = $this->getcustid($uuid);
                $total_amount = number_format($wobj->total_amount, 2, '.', '');
                $paymode = $wobj->payment_mode;
                $receipt_number = (int)$wobj->receipt_number;
                $billid = $wobj->id;
                $bill_added_on = $wobj->bill_added_on;
                $wallet_amount_received = $wobj->wallet_amount_received;
                $payment_platform = $wobj->payment_platform;
                
                $total_payment += $total_amount;
                $cheque_dd_paytm_number = '';
                $account_number = '';
                $branch_address = '';
                $slip_issuedate = '';
                $branch_name = '';
                if($paymode != 'cash'){
                    $receiptQ = $this->db->query("SELECT tb2.cheque_dd_paytm_number, tb2.account_number, tb2.branch_address, tb2.bankslip_issued_date, tb2.branch_name FROM sht_subscriber_receipt_history as tb2 WHERE tb2.bill_id='".$billid."'");
                    if($receiptQ->num_rows() > 0){
                        $recptrowdata = $receiptQ->row();
                        $cheque_dd_paytm_number = $recptrowdata->cheque_dd_paytm_number;
                        $account_number = ($recptrowdata->account_number !== NULL) ? $recptrowdata->account_number : '';
                        $branch_address = ($recptrowdata->branch_address !== NULL) ? $recptrowdata->branch_address : '';
                        $slip_issuedate = ($recptrowdata->bankslip_issued_date !== NULL) ? $recptrowdata->bankslip_issued_date : '';
                        $branch_name = ($recptrowdata->branch_name !== NULL) ? $recptrowdata->branch_name : '';
                    }
                }
                
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $uuid);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $fullname);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
                if($wallet_amount_received != 1){
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $total_amount);
                    $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray2);
                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $total_amount);
                    $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $paymode);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$j, $receipt_number);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
                
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$j, (int)$cheque_dd_paytm_number);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$j, (int)$account_number);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$j, $branch_name);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($styleArray1);           
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$j, $branch_address);
                $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$j, $slip_issuedate);
                $objPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('K'.$j, $payment_platform);
                $objPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('L'.$j, $bill_added_on);
                $objPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray($styleArray1);
                
                $j++;
            }
            
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$j, 'Total Amount');
            $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $total_payment);
            $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray);
        }
        
        
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('reports/'.$isp_uid.'_customreport.xlsx');
        
        echo json_encode(true);
    }

    
    

    /*
     *  Tickets Report
     */
    public function isp_username($ispid){
        $name = '';
        $query = $this->db->query("SELECT name FROM sht_isp_users WHERE id='".$ispid."' AND status='1' AND is_deleted='0'");
        $exists = $query->num_rows();
        if($exists > 0){
            $name = $query->row()->name;
        }
        return $name;
    }

    public function tkt_sorter_username($ispid){
        $name = '';
        $query = $this->db->query("SELECT name FROM sht_isp_users WHERE id='".$ispid."' AND status='1' AND is_deleted='0'");
        $exists = $query->num_rows();
        if($exists > 0){
            $name = $query->row()->name;
        }
        return $name;
    }

    public function tat_days($issue_date){ //turnaround time
        $date=strtotime($issue_date);//Converted to a PHP date (a second count)
        
        //Calculate difference
        $diff=time() - $date;//time returns current time in seconds
        $days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
        $hours=round(($diff-$days*60*60*24)/(60*60));
        return "$days day $hours hours";
    }
    
    public function userstickets_sheet(){
        $data = array();
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        //$date_range = '26.08.2017 - 26.08.2017';
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $limit = $postdata['limit'];
        $offset = $postdata['offset'];
        $search_user = $postdata['search_user'];
        
        $cwhere = '1';
        if($search_user != ''){
            $cwhere .= ' AND tb1.subscriber_uuid LIKE \'%'.$search_user.'%\' ';
        }
        
        $gen = '';
        if($offset == 0){
            $gen = '<thead><tr class="active"><th>&nbsp;</th><th>UserID</th><th>Name</th><th>Request ID</th><th>Ticket Type</th><th>Status</th><th>TAT Days</th><th>Priority</th><th>Added On</th></tr></thead>';
        }
        
        //$sczcond = $this->user_department_cond();
        $sczcond = '';
        $totaltktlist = $this->db->query("SELECT tb1.* FROM sht_subscriber_tickets as tb1 INNER JOIN sht_users as tb2 ON(tb1.subscriber_id = tb2.id) WHERE $cwhere AND DATE(added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' AND isp_uid='".$isp_uid."' $sczcond ORDER BY tb1.id DESC");
        $total_records = $totaltktlist->num_rows();
        
        $ticketQ = $this->db->query("SELECT tb1.* FROM sht_subscriber_tickets as tb1 INNER JOIN sht_users as tb2 ON(tb1.subscriber_id = tb2.id) WHERE $cwhere AND DATE(added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' AND isp_uid='".$isp_uid."' $sczcond ORDER BY tb1.id DESC LIMIT $offset,$limit");
        if($ticketQ->num_rows() > 0){
            $gen .= '<tbody>';
            $i = ($offset + 1);
            foreach($ticketQ->result() as $tktobj){
                $ticket_raise_by = $this->isp_username($tktobj->ticket_raise_by);
                $ticket_assign_to = $this->tkt_sorter_username($tktobj->ticket_assign_to);
                $tktdate = date('d-m-Y', strtotime($tktobj->added_on));
                $tktclosedate = date('d-m-Y', strtotime($tktobj->closed_on));
                $customer_data = $this->getcustomer_data($tktobj->subscriber_uuid);
                $ticket_addedon = strtotime($tktobj->added_on);
                
                $tktstyle= '';
                if($tktobj->status == '0'){
                    $status = 'Open';
                    $tktstyle = "style='color:#ff0000'";
                }else{
                    $status = 'Close';
                }
                $tat_days = $this->tat_days($tktobj->added_on);
                $gen .= "<tr><td>".$i.".</td><td><a href='".base_url()."user/edit/".$tktobj->subscriber_id."'>".$customer_data->uid."</a></td><td>".$customer_data->firstname." ".$customer_data->lastname."</td><td>".$tktobj->ticket_id."</td><td>".$tktobj->ticket_type."</td><td><span class='".$status."' $tktstyle>".$status."</span></td><td>".$tat_days."</td><td>".$tktobj->ticket_priority."</td><td>".$tktdate."</td></tr>";
                
                $i++;
            }
            $gen .= '</tbody>';
        }else{
            $gen .= '<tbody><tr><td colspan="9" style="text-align:center">No Records Found</td></tr></tbody>';
        }
        
        $nxtoffset = ($limit + $offset);
        $data['reportlist_total'] = $total_records;
        $data['reportlist'] = $gen;
        $data['offset'] = $nxtoffset;
        if($total_records > $nxtoffset){
            $data['loadmore'] = 1;
        }else{
            $data['loadmore'] = 0;
        }
        echo json_encode($data);
    }
    
    public function exportuserstickets_sheet(){
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 11
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            )
        );
        
        $styleArray2 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F28A8C')
            )
        );
        
        $titlestyleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 14
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->setTitle("Tickets & Complaints Report");
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A1','Tickets Report: '.$date_range);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($titlestyleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A2','UID');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2','Fullname');
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C2','Request ID');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D2','Ticket Type');
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E2','Status');
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('F2','TAT Days');
        $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('G2','Priority');
        $objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);
        //$objPHPExcel->getActiveSheet()->setCellValue('H2','Generated BY');
        //$objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('I2','Assign To');
        $objPHPExcel->getActiveSheet()->getStyle('I2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('J2','Added On');
        $objPHPExcel->getActiveSheet()->getStyle('J2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('K2','Closed On');
        $objPHPExcel->getActiveSheet()->getStyle('K2')->applyFromArray($styleArray);
        
        $j = 3; $total_payment = 0;
        //$sczcond = $this->user_department_cond();
        $sczcond = '';
        $totaltktlist = $this->db->query("SELECT tb1.* FROM sht_subscriber_tickets as tb1 INNER JOIN sht_users as tb2 ON(tb1.subscriber_id = tb2.id) WHERE DATE(added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' AND isp_uid='".$isp_uid."' $sczcond ORDER BY tb1.id DESC");
        $total_records = $totaltktlist->num_rows();
        if($total_records > 0){
            foreach($totaltktlist->result() as $tktobj){
                $ticket_raise_by = $this->isp_username($tktobj->ticket_raise_by);
                $ticket_assign_to = $this->tkt_sorter_username($tktobj->ticket_assign_to);
                $tktdate = date('d-m-Y', strtotime($tktobj->added_on));
                $tktclosedate = date('d-m-Y', strtotime($tktobj->closed_on));
                $customer_data = $this->getcustomer_data($tktobj->subscriber_uuid);
                $ticket_addedon = $tktobj->added_on;
                $ticket_closedon = $tktobj->closed_on;
                $tat_days = $this->tat_days($tktobj->added_on);
                $uuid = $customer_data->uid;
                $fullname = $customer_data->firstname. " " .$customer_data->lastname;
                $requestid = $tktobj->ticket_id;
                $tkttype = $tktobj->ticket_type;
                $ticket_priority = $tktobj->ticket_priority;
                
                if($tktobj->status == '0'){
                    $status = 'Open';
                }else{
                    $status = 'Close';
                }
                
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $uuid);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $fullname);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $requestid);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $tkttype);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
                if($status == 'Open'){
                    $objPHPExcel->getActiveSheet()->setCellValue('E'.$j, $status);
                    $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray2);
                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue('E'.$j, $status);
                    $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
                }
                
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$j, $tat_days);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$j, $ticket_priority);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($styleArray1);
                //$objPHPExcel->getActiveSheet()->setCellValue('H'.$j, $ticket_raise_by);
                //$objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($styleArray1);           
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$j, $ticket_assign_to);
                $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$j, $ticket_addedon);
                $objPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('K'.$j, $ticket_closedon);
                $objPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray($styleArray1);
                
                $j++;
            }
        }
        
        
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('reports/'.$isp_uid.'_customreport.xlsx');
        
        echo json_encode(true);
    }

    
    /*
     *  Pending Payments Report
     */
    public function getcontactnumb($uuid){
        $userQuery = $this->db->query("SELECT mobile FROM sht_users WHERE uid='".$uuid."'");
        $num_rows = $userQuery->num_rows();
        $rowdata = $userQuery->row();
        return $rowdata->mobile;
    }
    public function pendingpayment_sheet(){
        $data = array();
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $date_range = $this->input->post('customreport_daterange');
        //$date_range = '26.08.2017 - 26.08.2017';
        $date_filter_from  = '';
        $date_filter_to = '';
        /*
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        */
        $limit = $postdata['limit'];
        $offset = $postdata['offset'];
        $search_user = $postdata['search_user'];
        
        $cwhere = '1';
        if($search_user != ''){
            $cwhere .= ' AND (uid LIKE \'%'.$search_user.'%\' OR firstname LIKE \'%'.$search_user.'%\' OR lastname LIKE \'%'.$search_user.'%\') ';
        }
        
        $gen = '';
        if($offset == 0){
            $gen = '<thead><tr class="active"><th>&nbsp;</th><th>UserID</th><th>Name</th><th>Pending Amount</th><th>&nbsp;</th></thead>';
        }
        
        //$sczcond = $this->user_department_cond();
        $sczcond = '';
        
        $pendingbalance = array();
        $useridQ = $this->db->query("SELECT uid FROM sht_users WHERE $cwhere AND isp_uid='".$isp_uid."' AND (account_activated_on NOT LIKE '%0000-00-00%' AND account_activated_on IS NOT NULL)");
        if($useridQ->num_rows() > 0){
            foreach($useridQ->result() as $userobj){
                $uuid = $userobj->uid;
                
                $wallet_amt = 0;
                $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
                if($walletQ->num_rows() > 0){
                    $wallet_amt = $walletQ->row()->wallet_amt;
                }
                
                $passbook_amt = 0;
                $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
                if($passbookQ->num_rows() > 0){
                    $passbook_amt = $passbookQ->row()->plan_cost;
                }
                $balanceamt = ($wallet_amt - $passbook_amt);
                if($balanceamt > 0){
                    $balanceamt = 0;
                }else{
                    $balanceamt = abs($balanceamt);
                }
                
                //CHECK CUSTOM PENDINGS
                $totalbillamt = 0; $totalrcptamt = 0; $totalbalamt = 0;
                $custbillQ = $this->db->query("SELECT COALESCE(SUM(total_amount),0) as total_amount FROM sht_subscriber_custom_billing WHERE uid='".$uuid."' AND is_deleted='0'");
                if($custbillQ->num_rows() > 0){
                    $totalbillamt = $custbillQ->row()->total_amount;
                }

                $recptQ = $this->db->query("SELECT COALESCE(SUM(receipt_amount),0) as receipt_amount FROM sht_custom_receipt_list WHERE uid='".$uuid."' ORDER BY id DESC");
                if($recptQ->num_rows() > 0){
                    $totalrcptamt = $recptQ->row()->receipt_amount;
                }  
                $totalbalamt = abs($totalrcptamt - $totalbillamt);
                
                $totalbalamt = ($totalbalamt + $balanceamt);
                if($totalbalamt > 0){
                    $pendingbalance["$uuid"] = $totalbalamt;
                }
            }
        }
        
        $total_records = count($pendingbalance);
        if($total_records > 0){
            $pendingbalance = array_slice($pendingbalance, $offset, $limit, true);
            $gen .= '<tbody>';
            $i = ($offset + 1);
            foreach($pendingbalance as $uuid => $paybal){
                $fullname = $this->getfullname($uuid);
                $custid = $this->getcustid($uuid);
                $total_amount = number_format($paybal, 2);
                $gen .= '<tr><td>'.$i.'</td><td><a href="'.base_url().'user/edit/'.$custid.'/b" target="_blank">'.$uuid.'</a></td><td>'.$fullname.'</td><td>'.$total_amount.'</td><td><a href="javascript:void(0)" data-uuid="'.$uuid.'" class="send_pendingamt_alert">Send Notification</a><span class="smstxt hide">Dear '.$uuid.', <br/> Amount of Rs. '.$paybal.' is due on your account. Please make payment ASAP for uninterruption services. <br/> For any queries please call us @ '.$help_number.' <br/> Team '.$isp_name.'</span></td></tr>';
                $i++;
            }
            $gen .= '</tbody>';
        }else{
            $gen .= '<tbody><tr><td colspan="6" style="text-align:center">No Records Found</td></tr></tbody>';
        }
        
        $nxtoffset = ($limit + $offset);
        $data['reportlist_total'] = $total_records;
        $data['reportlist'] = $gen;
        $data['offset'] = $nxtoffset;
        if($total_records > $nxtoffset){
            $data['loadmore'] = 1;
        }else{
            $data['loadmore'] = 0;
        }
        echo json_encode($data);
    }
    
    public function getispdetails($isp_uid){
        $data = array();
        $ispdetQ = $this->db->query("SELECT tb1.company_name, tb1.isp_name, tb1.help_number1, tb2.isp_domain_name FROM sht_isp_detail as tb1 LEFT JOIN sht_isp_admin as tb2 ON(tb1.isp_uid=tb2.isp_uid) WHERE tb1.isp_uid='".$isp_uid."'");
        $rdata = $ispdetQ->row();
        $data['company_name'] = $rdata->company_name;
        $data['isp_name'] = $rdata->isp_name;
        $data['help_number'] = $rdata->help_number1;
        $data['isp_domain_name'] = $rdata->isp_domain_name;
        
        return $data;
    }
    
    public function exportpendingpayment_sheet(){
        $data = array();
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        //$date_range = '26.08.2017 - 26.08.2017';
        $date_filter_from  = '';
        $date_filter_to = '';
        /*
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        */
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 11
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            )
        );
        
        $styleArray2 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F28A8C')
            )
        );
        
        $titlestyleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 14
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->setTitle("Pending Payment Report");
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A1','Pending Payment Report: '.$date_range);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($titlestyleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A2','UID');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2','Fullname');
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C2','Mobile No.');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D2','Pending Amount');
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
        
        //$sczcond = $this->user_department_cond();
        $sczcond = '';
        
        $pendingbalance = array();
        $useridQ = $this->db->query("SELECT uid FROM sht_users WHERE isp_uid='".$isp_uid."' AND (account_activated_on NOT LIKE '%0000-00-00%' AND account_activated_on IS NOT NULL)");
        if($useridQ->num_rows() > 0){
            foreach($useridQ->result() as $userobj){
                $uuid = $userobj->uid;
                
                $wallet_amt = 0;
                $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
                if($walletQ->num_rows() > 0){
                    $wallet_amt = $walletQ->row()->wallet_amt;
                }
                
                $passbook_amt = 0;
                $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
                if($passbookQ->num_rows() > 0){
                    $passbook_amt = $passbookQ->row()->plan_cost;
                }
                $balanceamt = ($wallet_amt - $passbook_amt);
                if($balanceamt > 0){
                    $balanceamt = 0;
                }else{
                    $balanceamt = abs($balanceamt);
                }
                
                //CHECK CUSTOM PENDINGS
                $totalbillamt = 0; $totalrcptamt = 0; $totalbalamt = 0;
                $custbillQ = $this->db->query("SELECT COALESCE(SUM(total_amount),0) as total_amount FROM sht_subscriber_custom_billing WHERE uid='".$uuid."' AND is_deleted='0'");
                if($custbillQ->num_rows() > 0){
                    $totalbillamt = $custbillQ->row()->total_amount;
                }

                $recptQ = $this->db->query("SELECT COALESCE(SUM(receipt_amount),0) as receipt_amount FROM sht_custom_receipt_list WHERE uid='".$uuid."' ORDER BY id DESC");
                if($recptQ->num_rows() > 0){
                    $totalrcptamt = $recptQ->row()->receipt_amount;
                }  
                $totalbalamt = abs($totalrcptamt - $totalbillamt);
                
                $totalbalamt = ($totalbalamt + $balanceamt);
                if($totalbalamt > 0){
                    $pendingbalance["$uuid"] = $totalbalamt;
                }
            }
        }
        
        $total_records = count($pendingbalance);
        if($total_records > 0){
            $j = 3;
            foreach($pendingbalance as $uuid => $paybal){
                $fullname = $this->getfullname($uuid);
                $custid = $this->getcustid($uuid);
                $total_amount = number_format($paybal, 2, '.', '');
                $contactnumb = $this->getcontactnumb($uuid);
                
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $uuid);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $fullname);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $contactnumb);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $total_amount);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
                $j++;
            }
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, 'Total Amount');
            $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$j, array_sum($pendingbalance));
            $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray);
        }
        
        
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('reports/'.$isp_uid.'_customreport.xlsx');
        
        echo json_encode(true);
    }

    /*
     * SMS Reports Sheet
     */
    public function smsreports_sheet(){
        $data = array();
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        //$date_range = '26.08.2017 - 26.08.2017';
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $limit = $postdata['limit'];
        $offset = $postdata['offset'];
        $search_user = $postdata['search_user'];
        
        $cwhere = '1';
        if($search_user != ''){
            $cwhere .= ' AND tb1.uid LIKE \'%'.$search_user.'%\' ';
        }
        
        $gen = '';
        if($offset == 0){
            $gen = '<thead><tr class="active"><th>&nbsp;</th><th>UserID</th><th>Name</th><th>Mobile Number</th><th>Message Content</th><th>Send On</th></tr></thead>';
        }
        
        $sczcond = '';
        $totalsmslist = $this->db->query("SELECT tb1.* FROM sht_users_notifications as tb1 INNER JOIN sht_users as tb2 ON(tb1.uid = tb2.uid) WHERE $cwhere AND DATE(tb1.added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' AND tb1.isp_uid='".$isp_uid."' $sczcond ORDER BY tb1.id DESC");
        $total_records = $totalsmslist->num_rows();
        
        $smsQuery = $this->db->query("SELECT tb1.*, tb2.mobile, tb2.id FROM sht_users_notifications as tb1 LEFT JOIN sht_users as tb2 ON(tb1.uid = tb2.uid) WHERE $cwhere AND DATE(tb1.added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' AND tb1.isp_uid='".$isp_uid."' $sczcond ORDER BY tb1.id DESC LIMIT $offset,$limit");
        if($smsQuery->num_rows() > 0){
            $gen .= '<tbody>';
            $i = ($offset + 1);
            foreach($smsQuery->result() as $smsobj){
                $sms_sendtxt = $smsobj->message;
                $mobile = $smsobj->mobile;
                $customer_data = $this->getcustomer_data($smsobj->uid);
                $sms_sendon = date('d-m-Y H:i:s', strtotime($smsobj->added_on));
                
                $gen .= "<tr><td>".$i.".</td><td><a href='".base_url()."user/edit/".$smsobj->id."'>".$customer_data->uid."</a></td><td>".$customer_data->firstname." ".$customer_data->lastname."</td><td>".$smsobj->mobile."</td><td>".$sms_sendtxt."</td><td>".$sms_sendon."</td></tr>";
                
                $i++;
            }
            $gen .= '</tbody>';
        }else{
            $gen .= '<tbody><tr><td colspan="9" style="text-align:center">No Records Found</td></tr></tbody>';
        }
        
        $nxtoffset = ($limit + $offset);
        $data['reportlist_total'] = $total_records;
        $data['reportlist'] = $gen;
        $data['offset'] = $nxtoffset;
        if($total_records > $nxtoffset){
            $data['loadmore'] = 1;
        }else{
            $data['loadmore'] = 0;
        }
        echo json_encode($data);
    }
    public function exportsmsreports_sheet(){
        $data = array();
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid     = (isset($postdata['isp_uid']) && $postdata['isp_uid'] != '') ? $postdata['isp_uid'] : $sessiondata['isp_uid'];
        
        $date_range = $this->input->post('customreport_daterange');
        //$date_range = '26.08.2017 - 26.08.2017';
        $date_filter_from  = '';
        $date_filter_to = '';
        
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(0);
        //Set sheet style
        $styleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 11
            )
        );
        //Set sheet style
        $styleArray1 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            )
        );
        
        $styleArray2 = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 9
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'wrap' => true
            )
        );
        
        $titlestyleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'name'  => 'Tahoma',
                'size' => 14
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->setTitle("SMS Report");
        //Set sheet columns Heading
        $objPHPExcel->getActiveSheet()->setCellValue('A1','SMS Send Report: '.$date_range);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($titlestyleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A2','UID');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2','Fullname');
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('C2','Mobile No.');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D2','Send SMS');
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E2','Send On');
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
        
        $sczcond = ''; $j = 3;
        $totalsmslist = $this->db->query("SELECT tb1.*, tb2.mobile, tb2.id, tb2.state, tb2.city, tb2.zone FROM sht_users_notifications as tb1 INNER JOIN sht_users as tb2 ON(tb1.uid = tb2.uid) WHERE DATE(tb1.added_on) BETWEEN '".$date_filter_from."' AND '".$date_filter_to."' AND tb1.isp_uid='".$isp_uid."' $sczcond ORDER BY tb1.id DESC");
        $total_records = $totalsmslist->num_rows();
        if($total_records > 0){
            foreach($totalsmslist->result() as $smsobj){
                $sms_sendtxt = $smsobj->message;
                $mobile = $smsobj->mobile;
                $fullname = $this->getfullname($smsobj->uid);
                $sms_sendon = date('d-m-Y H:i:s', strtotime($smsobj->added_on));
                $state = $this->getstatename($smsobj->state);
                $city = $this->getcityname($smsobj->state,$smsobj->city);
                $zone = $this->getzonename($smsobj->zone);
                
                $lfcr = chr(10) . chr(13);
                $sms_sendtxt = str_replace('<br/>', $lfcr, $sms_sendtxt);
                
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $smsobj->uid);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $fullname);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $mobile);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $city);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$j, $zone);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$j, $sms_sendtxt);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray2);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$j, $sms_sendon);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($styleArray1);
                
                $j++;
            }
        }
        
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('reports/'.$isp_uid.'_customreport.xlsx');
        
        echo json_encode(true);
    }
}


?>

