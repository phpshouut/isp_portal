<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications_model extends CI_Model{
    
    public function __construct(){
        
    }
    
    public function getispdetails($isp_uid){
        $data = array();
        $ispdetQ = $this->db->query("SELECT tb1.company_name, tb1.isp_name, tb1.help_number1, tb2.isp_domain_name FROM sht_isp_detail as tb1 LEFT JOIN sht_isp_admin as tb2 ON(tb1.isp_uid=tb2.isp_uid) WHERE tb1.isp_uid='".$isp_uid."'");
        $rdata = $ispdetQ->row();
        $data['company_name'] = $rdata->company_name;
        $data['isp_name'] = $rdata->isp_name;
        $data['help_number'] = $rdata->help_number1;
        $data['isp_domain_name'] = $rdata->isp_domain_name;
        
        return $data;
    }
    
    public function getcustomer_fullname($uuid){
        $mobileQ = $this->db->query("SELECT firstname,lastname FROM sht_users WHERE uid='".$uuid."'");
        $rdata = $mobileQ->row();
        return $rdata->firstname .' '. $rdata->lastname;
    }
    
    public function getcustomer_mobileno($uuid){
        $mobileQ = $this->db->query("SELECT mobile FROM sht_users WHERE uid='".$uuid."'");
        return $mobileQ->row()->mobile;
    }
    
    public function isp_txtsmsgateway(){
        $data = array();
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
        $uuid = $this->input->post('uuid');
        $user_message = urlencode($this->input->post('sms_tosend'));
        $user_mobileno = $this->getcustomer_mobileno($uuid);
        $sms_tosend = $this->input->post('sms_tosend');
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                
                $sender = $txtsmsobj->scope;
                
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    $data['gateway_message'] = 'SMS send successfully to the user.';
                    $this->add_user_activitylogs($uuid, 'SMS ('.$sms_tosend.') send successfully to the user');
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $user_mobileno, $user_message, $sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    $data['gateway_message'] = 'SMS send successfully to the user.';
                    $this->add_user_activitylogs($uuid, 'SMS ('.$sms_tosend.') send successfully to the user');
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $user_mobileno, $user_message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    $data['gateway_message'] = 'SMS send successfully to the user.';
                    $this->add_user_activitylogs($uuid, 'SMS ('.$sms_tosend.') send successfully to the user');
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $user_mobileno, $user_message, $rightsms_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    $data['gateway_message'] = 'SMS send successfully to the user.';
                    $this->add_user_activitylogs($uuid, 'SMS ('.$sms_tosend.') send successfully to the user');
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    $data['gateway_message'] = 'SMS send successfully to the user.';
                    $this->add_user_activitylogs($uuid, 'SMS ('.$sms_tosend.') send successfully to the user');
                }
                
                else{
                    $data['gateway_message'] = 'Please setup TEXT SMS GATEWAY to send the messages to the user.';
                }
            }
        }else{
            $data['gateway_message'] = 'Please setup TEXT SMS GATEWAY to send the messages to the user.';
        }
        
        echo json_encode($data);
    }
    
    public function addmessage_todb($isp_uid, $uuid, $message){
        $this->db->insert("sht_users_notifications", array('isp_uid' => $isp_uid, 'uid' => $uuid, 'notify_type' => 'SMS', 'message' => $message, 'added_on' => date('Y-m-d H:i:s')));
        return 1;
    }
    
    public function gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message){
        $param = array();
        $request =""; //initialise the request variable
        $param['method']= "sendMessage";
        $param['send_to'] = "91"+$user_mobileno;
        $param['msg'] = $user_message;
        $param['userid'] = $gupshup_user;
        $param['password'] = $gupshup_pwd;
        $param['v'] = "1.1";
        $param['msg_type'] = "TEXT"; //Can be "FLASH�/"UNICODE_TEXT"/�BINARY�
        $param['auth_scheme'] = "PLAIN";
        
        //Have to URL encode the values
        foreach($param as $key=>$val) {
            $request.= $key."=".urlencode($val);
            $request.= "&";
        }
        
        $request = substr($request, 0, strlen($request)-1);
        //remove final (&) sign from the request
        $url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?".$request;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        //echo $curl_scraped_page;
        
        return 1;
    }
    
    public function msg91_txtsmsgateway($msg91authkey, $phone, $msg, $sender) {
        $postData = array(
           'authkey' => $msg91authkey, //'106103ADQeqKxOvbT856d19deb',
           'mobiles' => $phone,
           'message' => $msg,
           'sender' => $sender, //'SHOUUT',
           'route' => '4'
        );

        $url="https://control.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
           CURLOPT_URL => $url,
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_POST => true,
           CURLOPT_POSTFIELDS => $postData
           //,CURLOPT_FOLLOWLOCATION => true
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }

    public function bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $phone, $msg){
        $bulksmsurl = "http://bulksmsindia.mobi/sendurlcomma.aspx?";
        $postvalues = "user=".$bulksmsuser."&pwd=".$bulksms_pwd."&senderid=ABC&mobileno=".$phone."&msgtext=".$msg."&smstype=0/4/3";
        //print_r($bulksmsurl); die;
        
        $ch = curl_init($bulksmsurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }
    
    public function decibel_txtsmsgateway($mobile, $msg){
        $postData = array(
           'authkey' => '106103ADQeqKxOvbT856d19deb',
           'mobiles' => $mobile,
           'message' => $msg,
           'sender' => 'SHOUUT',
           'route' => '4'
        );
 
        $url="https://control.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));
 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        var_dump($output); die;
        
        curl_close($ch);
        return 1;
    }
    public function rightchoice_txtsmsgateway($rcuser, $rc_pwd, $phone, $msg, $sender){
        $rcsmsurl = "http://www.sms.rightchoicesms.com/sendsms/groupsms.php?";
        $postvalues = "username=".$rcuser."&password=".$rc_pwd."&type=TEXT&mobile=".$phone."&sender=".$sender."&message=".$msg;
        //print_r($bulksmsurl); die;
        
        $ch = curl_init($rcsmsurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }

    public function cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender){
        $bulksmsurl = "http://203.212.70.200/smpp/sendsms?";
        $postvalues = "username=".$cloudplace_user."&password=".$cloudplace_pwd."&to=".$user_mobileno."&from=".$cloudplace_sender."&udh=&text=".$user_message."&dlr-mask=19";
        //print_r($bulksmsurl); die;
        
        $ch = curl_init($bulksmsurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }
    public function infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message){
	$baseUrl = "https://alerts.solutionsinfini.com/api/v4/?api_key=".$infini_apikey;
	$user_message=urlencode($user_message); 

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $baseUrl."&method=sms&message=$user_message&to=$user_mobileno&sender=$infini_senderid");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);

	$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Returns 200 if everything went well
	if($statusCode==200){
	    return 1;
	}
	else{
	    return 0;
	}
	curl_close($ch);
    }
/***************** ACTIVITY LOGS ****************************************/
    
    //https://stackoverflow.com/questions/6296313/mysql-trigger-after-update-only-if-row-has-changed
    
/*------------------- TRIGGER AFTER UPDATE ---------------------------------------------------------------*/
    public function add_user_activitylogs($uuid, $action, $table=''){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        $super_admin = $session_data['super_admin'];
        $userid = $session_data['userid'];
        if($super_admin == 1){
            $added_by = 'SuperAdmin';
        }else{
            $teamQ = $this->db->query("SELECT name, lastname FROM sht_isp_users WHERE id='".$userid."'");
            if($teamQ->num_rows() > 0){
                $team_rowdata = $teamQ->row();
                $added_by = $team_rowdata->name. " " .$team_rowdata->lastname; 
            }else{
                $added_by = 'TeamMember';
            }
        }
        
        if(is_array($action)){
            $arraykeys = array_keys($action);
            $action = implode(',',$arraykeys);
        }
        
        $added_on = date('Y-m-d H:i:s');
        /*$this->db->query("
            CREATE TRIGGER after_data_update AFTER UPDATE ON $table
            FOR EACH ROW
            BEGIN
                IF NEW.updated_on <> OLD.updated_on THEN
                    INSERT INTO sht_activity_logs SET isp_uid='".$isp_uid."', uid='".$uuid."', action='".$action."', action_by='".$action_by."', added_on='".$added_on."';
                END IF;
            END;"
        );*/
        
        $logactivity = array(
            'isp_uid' => $isp_uid,
            'uid' => $uuid,
            'action' => "$action",
            'added_by' => $added_by,
            'added_on' => $added_on
        );
        
        $this->db->insert('sht_activity_logs', $logactivity);
        return 1;
    }
    
    public function total_activitylogs($uuid, $date_filter_from, $date_filter_to){
        $actdata = array();
        $activitylogsQ = $this->db->query("SELECT action, added_by, added_on FROM sht_activity_logs WHERE uid='".$uuid."' AND DATE(added_on) BETWEEN '$date_filter_from' AND '$date_filter_to'");
        $total_numlogs = $activitylogsQ->num_rows();
        $total_numpages = ceil($total_numlogs/10);
        
        $actdata['total_logs'] = $total_numlogs;
        $actdata['total_pages'] = $total_numpages;
        return $actdata;
    }
    
    public function list_user_activitylogs(){
        $data = array();
        $gen = '';
        $start = $this->input->post('start');
        $offset = $this->input->post('offset');
        $uuid = $this->input->post('uuid');
        $date_range = $this->input->post('date_range');
        //$date_range = '26.08.2017 - 26.08.2017';
        $date_filter_from  = '';
        $date_filter_to = '';
        $date_range_explode = explode('-',$date_range);
        $date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
        $date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
        
        $data = $this->total_activitylogs($uuid, $date_filter_from, $date_filter_to);
        
        $activitylogsQ = $this->db->query("SELECT action, added_by, added_on FROM sht_activity_logs WHERE uid='".$uuid."' AND DATE(added_on) BETWEEN '$date_filter_from' AND '$date_filter_to' ORDER BY id DESC LIMIT $start,$offset");
        $recnum = $activitylogsQ->num_rows();
        if($activitylogsQ->num_rows() > 0){
            $gen .= '<div class="uislider_container">';
            foreach($activitylogsQ->result() as $actlogobj){
                $action = ucwords($actlogobj->action);
                $added_by = ucwords($actlogobj->added_by);
                $added_on = $actlogobj->added_on;
                $gen .= '<h5>'.$action.' <small>'.$added_on.' by '.$added_by.'</small></h5>';
            }
            $gen .= '</div>';
        }else{
            $gen .= 'No Logs found in this period.';
        }
        
        $data['recnum'] = $recnum;
        $data['activitylogs'] = $gen;
        echo json_encode($data);
    }

/*------------------- SMS ALERTS ------------------------------------------------------------------------*/
    //vikas user Activation alert
    public function user_activation_alert($uuid,$upwd){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
        $username = $this->getcustomer_fullname($uuid);
        $user_mobileno = $this->getcustomer_mobileno($uuid);
        $ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $user_message = urlencode('Dear '.$username).'%0a'.urlencode('Thank you for choosing '.$company_name).'%0a'.urlencode('Your internet connection will be installed soon! Your account details are - ').'%0a'. urlencode('UserID: '.$uuid).'%0a'. urlencode('Password: '.$upwd).'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
        
        $sms_tosend = 'Dear '.$username.'<br/> Thank you for choosing '.$company_name.'<br/> Your internet connection will be installed soon! Your account details are - <br/> UserID: '.$uuid. '<br/>Password: '.$upwd. '<br/>For any queries please call us @ '.$help_number. '<br/> Team '.$isp_name;
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                
                $sender = $txtsmsobj->scope;
                
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $user_mobileno, $user_message, $sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $user_mobileno, $user_message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $user_mobileno, $user_message, $rightsms_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                }else{
                    return 1;
                }
            }
        }
    }
    
    
   

    public function getplanname($srvid){
        $planname = '-';
        $query = $this->db->query("SELECT tb2.srvname FROM sht_services as tb2 WHERE tb2.srvid='".$srvid."'");
        if($query->num_rows() > 0){
            $planname = $query->row()->srvname;
        }
        return $planname;
    }

    public function getnextplandate($uuid){
        $next_bill_date = '';
        $userdetQ = $this->db->query("SELECT next_bill_date FROM sht_users WHERE uid='".$uuid."'");
        if($userdetQ->num_rows() > 0){
            $rdata = $userdetQ->row();
            $next_bill_date = date('d-m-Y', strtotime($rdata->next_bill_date));
            return $next_bill_date;
        }
    }
    public function newplanchange_alert($uuid, $baseplanid, $type){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
        $username = $this->getcustomer_fullname($uuid);
        $mobileno = $this->getcustomer_mobileno($uuid);
        $ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        $planname = $this->getplanname($baseplanid);
        $plandate = $this->getnextplandate($uuid);

        if($type == 'nextcycle'){   
            $message = urlencode('Dear '.$username).'%0a'.urlencode('Please note that your plan has been changed to '.$planname.' as per your request.').'%0a'.urlencode('New plan will be effective from '.$plandate).'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
            $sms_tosend = 'Dear '.$username.'<br/> Please note that your plan has been changed to '.$planname.' as per your request'.'<br/> New plan will be effective from '.$plandate. '<br/> For any queries please call us @ '.$help_number. '<br/> Team '.$isp_name;
            
        }elseif($type == 'changenow'){
            $message = urlencode('Dear '.$username).'%0a'.urlencode('Please note that your plan has been changed to '.$planname.' as per your request.').'%0a'.urlencode('New plan will be effective from today.').'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
            $sms_tosend = 'Dear '.$username.'<br/> Please note that your plan has been changed to '.$planname.' as per your request'.'<br/> New plan will be effective from today <br/> For any queries please call us @ '.$help_number. '<br/> Team '.$isp_name;
        }elseif($type == 'applyplan'){
            $message = urlencode('Dear '.$username).'%0a'.urlencode('Please note that your plan has been activated to '.$planname.' as per your request.').'%0a'.urlencode('New plan will be effective from today.').'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
            $sms_tosend = 'Dear '.$username.'<br/> Please note that your plan has been activated to '.$planname.' as per your request'.'<br/> New plan will be effective from today <br/> For any queries please call us @ '.$help_number. '<br/> Team '.$isp_name;
        }elseif($type == 'cancelplan'){
            $message = urlencode('Dear '.$username).'%0a'.urlencode('Please note that your plan '.$planname.' for next cycle has been cancelled as per your request.').'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
            $sms_tosend = 'Dear '.$username.'<br/> Please note that your plan '.$planname.' for next cycle has been cancelled as per your request.'. '<br/> For any queries please call us @ '.$help_number. '<br/> Team '.$isp_name;
        }elseif($type == 'planspeedchange'){
            $message = urlencode('Dear '.$username).'%0a'.urlencode('Please note that your plan '.$planname.' Speed has been updated as per your request.').'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
            $sms_tosend = 'Dear '.$username.'<br/> Please note that your plan '.$planname.' Speed has been updated as per your request.'. '<br/> For any queries please call us @ '.$help_number. '<br/> Team '.$isp_name;
        }
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                
                $sender = $txtsmsobj->scope;
                
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $mobileno, $message, $sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $mobileno, $message, $rightsms_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $mobileno, $message, $cloudplace_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                }else{
                    return 1;
                }
            }
        }
    }


    public function gettopupdetails($uuid, $topupid){
        $data = array();
        $topupQ = $this->db->query("SELECT topup_startdate, topup_enddate FROM sht_usertopupassoc WHERE uid='".$uuid."' AND topup_id='".$topupid."'");
        if($topupQ->num_rows() > 0){
            $rdata = $topupQ->row();
            $topup_startdate = $rdata->topup_startdate;
            $topup_enddate = $rdata->topup_enddate;
            
            $data['topup_startdate'] = $topup_startdate;
            $data['topup_enddate'] = $topup_enddate;
        }
        return $data;
    }
    public function topup_activation_alert($uuid, $topuptype, $topupname, $topupid){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
        $username = $this->getcustomer_fullname($uuid);
        $mobileno = $this->getcustomer_mobileno($uuid);
        $ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $topuptxt = '';
        if($topuptype != 'Data'){
            $topupArr = $this->gettopupdetails($uuid, $topupid);
            if(count($topupArr) > 0){
                $topup_startdate = date('d-m-Y', strtotime($topupArr['topup_startdate']));
                $topup_enddate = date('d-m-Y', strtotime($topupArr['topup_enddate']));
                $topuptxt .= $topup_startdate.' to '.$topup_enddate;
            }
        }else{
            $topuptxt .= 'today';
        }
        
        $message = urlencode('Dear '.$username).'%0a'.urlencode($topuptype.' Top-up : '.$topupname.' has been applied to your account. Top-up will be effective from '.$topuptxt).'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
        $sms_tosend = 'Dear '.$username.'<br/>'.$topuptype.' Top-up : '.$topupname.' has been applied to your account. Top-up will be effective from '.$topuptxt.'<br/> For any queries please call us @ '.$help_number. '<br/>Team '.$isp_name;
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                
                $sender = $txtsmsobj->scope;
                
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $mobileno, $message, $sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $mobileno, $message, $rightsms_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $mobileno, $message, $cloudplace_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }else{
                    return 1;
                }
            }
        }
    }


    public function ticket_activation_alert($uuid, $tickettype, $tkt_number){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
        $username = $this->getcustomer_fullname($uuid);
        $mobileno = $this->getcustomer_mobileno($uuid);
        $ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $message = urlencode('Dear '.$username.', your '.$tickettype.' #'.$tkt_number.' has been registered.').'%0a'.urlencode('Please allow us sometime to address your request, our team will ensure fastest possible action.').'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
        
        $sms_tosend = 'Dear '.$username.', your '.$tickettype.' #'.$tkt_number.' has been registered. <br/> Please allow us sometime to address your request, our team will ensure fastest possible action. <br/> For any queries please call us @ '.$help_number.' <br/>Team '.$isp_name;
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                
                $sender = $txtsmsobj->scope;
                
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $mobileno, $message, $sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $mobileno, $message, $rightsms_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $mobileno, $message, $cloudplace_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }else{
                    return 1;
                }
            }
        }
    }
    
    public function getticket_sortername($userid){
        $data = array();
        $sorternameQ = $this->db->query("SELECT CONCAT_WS(' ',name, lastname) as member, phone FROM sht_isp_users WHERE id='".$userid."'");
        if($sorternameQ->num_rows() > 0){
            $rowdata = $sorternameQ->row();
            $data['tkt_issuesto'] = $rowdata->member;
            $data['tkt_contact'] = $rowdata->phone;
        }
        return $data;
    }

    
    public function ticket_assignment_alert($uuid, $ticket_assign_to, $tkt_number, $msgto){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
        $username = $this->getcustomer_fullname($uuid);
        $mobileno = $this->getcustomer_mobileno($uuid);
        $ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $tktsoterarr = $this->getticket_sortername($ticket_assign_to);
        $team_member = $tktsoterarr['tkt_issuesto'];
        
        
        if($msgto == 'team_member'){
            $message = urlencode('#'.$tkt_number.' of UserID: '.$uuid.' ('.$username.') has been assigned to you.').'%0a'.urlencode('Please contact @'.$mobileno.' and resolve it ASAP.').'%0a'. urlencode('Team '.$isp_name);

            $sms_tosend = '#'.$tkt_number.' of UserID: '.$uuid.' ('.$username.') has been assigned to '.$team_member.'. <br/> Please contact @'.$mobileno.' and resolve it ASAP. <br/> Team '.$isp_name;
        }
        elseif($msgto == 'customer'){
            $message = urlencode('Your ticket with #'.$tkt_number.' has been assigned to '.$team_member.'.').'%0a'.urlencode('Please allow us sometime to address your request, our team will ensure fastest possible action.').'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
            
            $sms_tosend = 'Your ticket with #'.$tkt_number.' has been assigned to '.$team_member.'. <br/> Please allow us sometime to address your request, our team will ensure fastest possible action. <br/> For any queries please call us @ '.$help_number.' <br/> Team '.$isp_name;
        }
        
        if($msgto == 'team_member'){
            $mobileno = $tktsoterarr['tkt_contact'];
        }
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                
                $sender = $txtsmsobj->scope;
                
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $mobileno, $message, $sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $mobileno, $message, $rightsms_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $mobileno, $message, $cloudplace_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }else{
                    return 1;
                }
            }
        }
    }
    public function ticket_status_alert($uuid, $tickettype, $tkt_number, $tktstatus){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
        $username = $this->getcustomer_fullname($uuid);
        $mobileno = $this->getcustomer_mobileno($uuid);
        $ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $message = urlencode('Dear '.$username.', your ticket with #'.$tkt_number.' has been successfully resolved.').'%0a'.urlencode('If your issue has not been resolved, call us at '.$help_number).'%0a'.urlencode('Team '.$isp_name);
        
        $sms_tosend = 'Dear '.$username.', your '.$tickettype.' #'.$tkt_number.' has been successfully resolved. <br/> If your issue has not been resolved, call us at '.$help_number.' <br/>Team '.$isp_name;
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                
                $sender = $txtsmsobj->scope;
                
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $mobileno, $message, $sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $mobileno, $message, $rightsms_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $mobileno, $message, $cloudplace_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }else{
                    return 1;
                }
            }
        }
    }

    public function payment_received_alert($uuid, $amount_received){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
        $username = $this->getcustomer_fullname($uuid);
        $mobileno = $this->getcustomer_mobileno($uuid);
        $ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $message = urlencode('Dear '.$username.', we have received a payment of Rs. '.$amount_received.' towards your account with User ID: '.$uuid).'%0a'. urlencode(' Thank you for your payment!').'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
        $sms_tosend = 'Dear '.$username.', we have received a payment of Rs. '.$amount_received.' towards your account with User ID: '.$uuid.' <br/> Thank you for your payment! <br/> For any queries please call us @ '.$help_number.' <br/>Team '.$isp_name;
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                
                $sender = $txtsmsobj->scope;
                
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $mobileno, $message, $sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $mobileno, $message, $rightsms_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $mobileno, $message, $cloudplace_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }else{
                    return 1;
                }
            }
        }
    }



    public function sendbulksms_tousers(){
        $data = array();
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            $filteruids = $this->input->post('filteruids');
            $sms_tosend = $this->input->post('textmsg_tosend');
            
            $uidsarr = explode(',' , $filteruids);
            foreach($uidsarr as $uid_idno){
                $userQ = $this->db->query("SELECT uid, mobile, isp_uid FROM sht_users WHERE id='".$uid_idno."'");
                if($userQ->num_rows() > 0){
                    $urowdata = $userQ->row();
                    $isp_uid = $urowdata->isp_uid;
                    $uuid = $urowdata->uid;
                    $user_message = urlencode($sms_tosend);
                    $user_mobileno = $urowdata->mobile;
                    
                    foreach($txtsmsQ->result() as $txtsmsobj){
                        $gupshup_user = $txtsmsobj->gupshup_user;
                        $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                        $msg91authkey = $txtsmsobj->msg91authkey;
                        $bulksmsuser = $txtsmsobj->bulksmsuser;
                        $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                        $rightsms_user = $txtsmsobj->rightsms_user;
                        $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                        $rightsms_sender = $txtsmsobj->rightsms_sender;
                        $cloudplace_user = $txtsmsobj->cloudplace_user;
                        $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                        $cloudplace_sender = $txtsmsobj->scope;
                        
                        $sender = $txtsmsobj->scope;
                        
                        if(($gupshup_user != '') && ($gupshup_pwd != '')){
                            $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message);
                            $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                            //$data['gateway_message'] = 'SMS send successfully to the user.';
                        }elseif($msg91authkey != ''){
                            $this->msg91_txtsmsgateway($msg91authkey, $user_mobileno, $user_message, $sender);
                            $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                            //$data['gateway_message'] = 'SMS send successfully to the user.';
                        }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                            $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $user_mobileno, $user_message);
                            $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                            //$data['gateway_message'] = 'SMS send successfully to the user.';
                        }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                            $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $user_mobileno, $user_message, $rightsms_sender);
                            $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                            //$data['gateway_message'] = 'SMS send successfully to the user.';
                        }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                            $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender);
                            $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                            //$data['gateway_message'] = 'SMS send successfully to the user.';
                        }
                        
                        else{
                            //$data['gateway_message'] = 'Please setup TEXT SMS GATEWAY to send the messages to the user.';
                        }
                    }
                    
                }
            }
            $data['gateway_message'] = 'SMS send successfully to the users.';
        }else{
            $data['gateway_message'] = 'Please setup TEXT SMS GATEWAY to send the messages to the user.';
        }
        
        echo json_encode($data);
    }
    
    
    public function creditlimit_alert($uuid,$isp_uid,$msg,$dbmsg){
       
        $username = $this->getcustomer_fullname($uuid);
        $user_mobileno = $this->getcustomer_mobileno($uuid);
        $ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $user_message = $msg;
        $sms_tosend = $dbmsg;
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
      
       
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                
                $sender = $txtsmsobj->scope;
                
               
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $user_mobileno, $user_message, $sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $user_mobileno, $user_message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $user_mobileno, $user_message, $rightsms_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                }else{
                    return 1;
                }
            }
        }
    }
    
    public function acctexpiration_alert($uuid,$isp_uid,$msg,$dbmsg){
       
        $username = $this->getcustomer_fullname($uuid);
        $user_mobileno = $this->getcustomer_mobileno($uuid);
        $ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $user_message = $msg;
        $sms_tosend = $dbmsg;
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
      
       
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                
                $sender = $txtsmsobj->scope;
                
               
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $user_mobileno, $user_message, $sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $user_mobileno, $user_message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $user_mobileno, $user_message, $rightsms_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                }else{
                    return 1;
                }
            }
        }
    }
    
    public function planautorenewal_alert($uuid, $planname){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
        $username = $this->getcustomer_fullname($uuid);
        $user_mobileno = $this->getcustomer_mobileno($uuid);
        $ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $user_message = urlencode('Dear '.$username).'%0a'.urlencode('Your account with UserID: '.$uuid.' & Plan: '.$planname.' is set for AutoRenewal.').'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
        
        $sms_tosend = 'Dear '.$username.'<br/> Your account with UserID: '.$uuid.' & Plan: '.$planname.' is set for AutoRenewal. <br/>For any queries please call us @ '.$help_number. '<br/> Team '.$isp_name;
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                
                $sender = $txtsmsobj->scope;
                
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $user_mobileno, $user_message, $sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $user_mobileno, $user_message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $user_mobileno, $user_message, $rightsms_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                }else{
                    return 1;
                }
            }
        }
    }
    
/*------------------------------ TASK ALERTS --------------------------------------------------------------*/
    public function get_bitly_short_url($url) {
        $login = 'shouut';
        $appkey = 'R_495a520806484396941bd1a37d70228b';
        $query = array(
          "version" => "2.0.1",
          "longUrl" => $url,
          "login" => $login, // replace with your login
          "apiKey" => $appkey // replace with your api key
        );
  
        $query = http_build_query($query);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.bit.ly/shorten?".$query);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
  
        curl_close($ch);
  
        $response = json_decode($response);
        if($response->errorCode == 0 && $response->statusCode == "OK") {
          return $response->results->{$url}->shortUrl;
        }else {
          return '';
        }
    }
    public function teamtaskassign_alert($team_uid, $details = array()){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
        $tktsoterarr = $this->getticket_sortername($team_uid);
        $team_member = $tktsoterarr['tkt_issuesto'];
        $user_mobileno = $tktsoterarr['tkt_contact'];
        
        $ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $task_name = ''; $geoaddr = ''; $bitly = '';
        if(count($details) > 0){
            $task_name = $details['task_name'];
            $geoaddr = $details['geoaddr'];
        }
        if($geoaddr != ''){
            $mapurl = "https://www.google.co.in/maps/place/".$geoaddr;
            $bitly = $this->get_bitly_short_url($mapurl);
            
        }
        
        $user_message = urlencode('Dear '.$team_member).'%0a'.urlencode($task_name.' has been assigned to your task list.').'%0a'. urlencode($bitly).'%0a'.urlencode('Team '.$isp_name);
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                
                $sender = $txtsmsobj->scope;
                
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message);
                    return 1;
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $user_mobileno, $user_message, $sender);
                    return 1;
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $user_mobileno, $user_message);
                    return 1;
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $user_mobileno, $user_message, $rightsms_sender);
                    return 1;
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender);
                }else{
                    return 1;
                }
            }
        }
    }
    
    public function teamtask_deassign_alert($team_uid, $details = array()){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
        $tktsoterarr = $this->getticket_sortername($team_uid);
        $team_member = $tktsoterarr['tkt_issuesto'];
        $user_mobileno = $tktsoterarr['tkt_contact'];
        
        $ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $task_name = ''; $geoaddr = ''; $bitly = '';
        if(count($details) > 0){
            $task_name = $details['task_name'];
        }

        
        $user_message = urlencode('Dear '.$team_member).'%0a'.urlencode('Task: '.$task_name.' has been removed from your task list.').'%0a'.urlencode('Team '.$isp_name);
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                
                $sender = $txtsmsobj->scope;
                
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message);
                    return 1;
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $user_mobileno, $user_message, $sender);
                    return 1;
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $user_mobileno, $user_message);
                    return 1;
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $user_mobileno, $user_message, $rightsms_sender);
                    return 1;
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender);
                }else{
                    return 1;
                }
            }
        }
    }
    
    public function send_logincredentials_tousers(){
        $data = array();
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        $ispdetArr = $this->getispdetails($isp_uid);
        $isp_consumer_domain = 'https://'.$ispdetArr['isp_domain_name'].'/consumer';
        $bitly = $this->get_bitly_short_url($isp_consumer_domain);
        
        $isp_name = $ispdetArr['isp_name'];
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            $filteruids = $this->input->post('seluids');
            
            $uidsarr = explode(',' , $filteruids);
            foreach($uidsarr as $uid_idno){
                $userQ = $this->db->query("SELECT uid, mobile, isp_uid, orig_pwd, firstname, lastname FROM sht_users WHERE id='".$uid_idno."'");
                if($userQ->num_rows() > 0){
                    $urowdata = $userQ->row();
                    $isp_uid = $urowdata->isp_uid;
                    $uuid = $urowdata->uid;
                    $firstname = $urowdata->firstname;
                    $lastname = $urowdata->lastname;
                    $upwd = $urowdata->orig_pwd;
                    $user_message = urlencode('Dear '.$firstname).'%0a'.urlencode('Now you can track your data/plan/topup from  here: '.$bitly).'%0a'.urlencode('Your account details are - ').'%0a'. urlencode('UserID: '.$uuid).'%0a'. urlencode('Password: '.$upwd).'%0a'.urlencode('Team '.$isp_name);

                    $sms_tosend = 'Dear '.$firstname.'<br/> Now you can track your data/plan/topup from  here: '.$bitly.'<br/>Your account details are - <br/>UserID: '.$uuid.'<br/>Password: '.$upwd.'<br/>Team '.$isp_name;
                    $user_mobileno = $urowdata->mobile;
                    
                    foreach($txtsmsQ->result() as $txtsmsobj){
                        $gupshup_user = $txtsmsobj->gupshup_user;
                        $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                        $msg91authkey = $txtsmsobj->msg91authkey;
                        $bulksmsuser = $txtsmsobj->bulksmsuser;
                        $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                        $rightsms_user = $txtsmsobj->rightsms_user;
                        $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                        $rightsms_sender = $txtsmsobj->rightsms_sender;
                        $cloudplace_user = $txtsmsobj->cloudplace_user;
                        $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                        $cloudplace_sender = $txtsmsobj->scope;
                        $infini_apikey = $txtsmsobj->infini_apikey;
			$infini_senderid = $txtsmsobj->infini_senderid;
                        
                        $sender = $txtsmsobj->scope;
                        
                        if(($gupshup_user != '') && ($gupshup_pwd != '')){
                            $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message);
                            $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                            //$data['gateway_message'] = 'SMS send successfully to the user.';
                        }elseif($msg91authkey != ''){
                            $this->msg91_txtsmsgateway($msg91authkey, $user_mobileno, $user_message, $sender);
                            $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                            //$data['gateway_message'] = 'SMS send successfully to the user.';
                        }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                            $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $user_mobileno, $user_message);
                            $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                            //$data['gateway_message'] = 'SMS send successfully to the user.';
                        }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                            $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $user_mobileno, $user_message, $rightsms_sender);
                            $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                            //$data['gateway_message'] = 'SMS send successfully to the user.';
                        }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                            $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender);
                            $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                            //$data['gateway_message'] = 'SMS send successfully to the user.';
                        }elseif(($infini_apikey != '') && ($infini_senderid != '')){
                            $this->infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message);
			    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                        }
                        
                        else{
                            //$data['gateway_message'] = 'Please setup TEXT SMS GATEWAY to send the messages to the user.';
                        }
                    }
                    
                }
            }
            $data['gateway_message'] = 'SMS send successfully to the users.';
        }else{
            $data['gateway_message'] = 'Please setup TEXT SMS GATEWAY to send the messages to the user.';
        }
        
        echo json_encode($data);
    }
    
    public function send_payment_duealert($uuid, $sms_tosend){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
        $username = $this->getcustomer_fullname($uuid);
        $mobileno = $this->getcustomer_mobileno($uuid);
        /*$ispdetArr = $this->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];*/
        
        $message = urlencode($sms_tosend);
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                $infini_apikey = $txtsmsobj->infini_apikey;
		$infini_senderid = $txtsmsobj->infini_senderid;
                
                $sender = $txtsmsobj->scope;
                
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif($msg91authkey != ''){
                    $this->msg91_txtsmsgateway($msg91authkey, $mobileno, $message, $sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $mobileno, $message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $mobileno, $message, $rightsms_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $mobileno, $message, $cloudplace_sender);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    return 1;
                }elseif(($infini_apikey != '') && ($infini_senderid != '')){
                    $this->infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message);
                    $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                }else{
                    return 1;
                }
            }
        }
    }
    
    public function send_lastbill_tousers(){
        $data = array();
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        $ispdetArr = $this->getispdetails($isp_uid);
        $isp_consumer_domain = 'https://'.$ispdetArr['isp_domain_name'].'/consumer';
        $bitly = $this->get_bitly_short_url($isp_consumer_domain);
        
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
        if($txtsmsQ->num_rows() > 0){
            $filteruids = $this->input->post('seluids');
            
            $uidsarr = explode(',' , $filteruids);
            foreach($uidsarr as $uid_idno){
                $getuuidQ = $this->db->query("SELECT uid FROM sht_users WHERE id='".$uid_idno."'");
                $uuid = $getuuidQ->row()->uid;
                $checklastbillQ = $this->db->query("SELECT id, total_amount FROM sht_subscriber_billing WHERE subscriber_uuid='".$uuid."' AND bill_type='montly_pay' ORDER BY id DESC LIMIT 1");
                if($checklastbillQ->num_rows() > 0){
                    $billdata = $checklastbillQ->row();
                    $billid = $billdata->id;
                    $this->emailer_model->monthly_billing($uuid, $billid, 'sendbill', 'groupmail');
                    
                    $sms_tosend= 'Dear '.$uuid.', <br/> Your monthly plan amount of Rs. '.$billdata->total_amount.' is due on your account. Please make payment ASAP for uninterruption services. <br/> For any queries please call us @ '.$help_number.' <br/> Team '.$isp_name;
                    $this->send_payment_duealert($uuid, $sms_tosend);
                }
                sleep(1);
            }
        }
        echo json_encode(true);
    }

    
}

?>