<?php

class Promotion_model extends CI_Model{
    
    public function service_list(){
	$data = array();
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
	$query = $this->db->query("select srvid, srvname, plantype, topuptype from sht_services  where  enableplan = '1' and is_deleted = '0' and active = '0' $ispcond");
	$i = 0;
	foreach($query->result() as $row){
	    $data[$i]['srvid'] = $row->srvid;
	    $data[$i]['srvname'] = $row->srvname;
	    $data[$i]['plantype'] = $row->plantype;
	    
	    $i++;
	}
	//echo "<pre>";print_r($data);die;
	return $data;
    }
    
    public function banner_list(){
	$data = array();
          $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" where schsp.isp_uid='".$isp_uid."'";
	$query = $this->db->query("select schsp.*, ss.srvname from sht_consumer_home_slider_promotion schsp inner join sht_services as ss on (schsp.plan_id = ss.srvid) $ispcond");
	$i = 0;
	foreach($query->result() as $row){
	    $data[$i]['plan_name'] = $row->srvname;
	    $data[$i]['image_path'] = "https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/promotion_image/slider_promotion/".$row->image_path;
	    $data[$i]['description'] = $row->description;
	    $data[$i]['banner_number'] = $row->banner_number;
	    $i++;
	}
	return $data;
    }
    
    public function add_home_banner(){
	 $banner_number = $this->input->post('banner_number');
	 $planid = '';
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
	if($this->input->post("topup") == ''){
	    $planid = $this->input->post("plan");   
	}else{
	    $planid = $this->input->post("topup");
	}
	$description = '';
	$description = $this->input->post("banner_description");
	
	$image_path = '';
	if(isset($_POST['thumb_values'])){
            $logo_image_path = json_decode($_POST['thumb_values']);
            $image_name =  $logo_image_path->name;
            $image_src = $logo_image_path->data;
	    $temp = explode(".", $image_name);
	    $extension = end($temp);
	    $path = 'assets/media/promotion_image/slider_promotion/';
	    $filename = basename($image_name);
	    $filename = strtotime(date('Y-m-d H:i:s')).'.'.$extension;
	    $image_path = $filename;
	    $explode = explode(',',$image_src);
	    $image_src = $explode['1'];
	    $image_src = str_replace(' ', '+', $image_src);
	    $data_img = base64_decode($image_src);
	    $file = $path . $filename;
	    $success = file_put_contents($file, $data_img);
	    $fname = $path.$filename;
	    $famazonname = AMAZONPATH.'promotion_image/slider_promotion/'.$filename;
	    $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
        }
	/*if (isset($_FILES) && !empty($_FILES['banner_image']['name'])) {
	    $temp = explode(".", $_FILES["banner_image"]["name"]);
	    $extension = end($temp);
	    $path = 'assets/media/promotion_image/slider_promotion/';
	    $filename = basename($_FILES["banner_image"]["name"]);
	    $filename = strtotime(date('Y-m-d H:i:s')).'.'.$extension;
	    $image_path = $filename;
	    move_uploaded_file($_FILES["banner_image"]["tmp_name"],$path . $filename);
	    // upload on s3
	    $fname = $path.$filename;
	    $famazonname = AMAZONPATH.'promotion_image/slider_promotion/'.$filename;
	    $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
        }*/
	//check already inserted banner number detail
	$query = $this->db->query("select * from sht_consumer_home_slider_promotion where banner_number = '$banner_number' and isp_uid = '$isp_uid'");
	if($query->num_rows() > 0){
	    //already inserted here update
	    if($planid != ''){
		$this->db->query("update sht_consumer_home_slider_promotion set plan_id = '$planid', updated_on = now() where banner_number = '$banner_number'");
	    }
	    if($description != ''){
		$this->db->query("update sht_consumer_home_slider_promotion set description = '$description', updated_on = now() where banner_number = '$banner_number'");
	    }
	    if($image_path != ''){
		$this->db->query("update sht_consumer_home_slider_promotion set image_path = '$image_path', updated_on = now() where banner_number = '$banner_number'");
	    }
	}else{
	   
	    $this->db->query("insert into sht_consumer_home_slider_promotion(plan_id, description, image_path, created_on, banner_number,isp_uid) values('$planid', '$description', '$image_path', now(), '$banner_number','$isp_uid')");
	}
    }
    
    public function marketing_promotion_list(){
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
	$data = array();
	$query = $this->db->query("select * from sht_consumer_marketing_promotion where is_deleted = '0' $ispcond order by id desc");
	$i = 0;
	foreach($query->result() as $row){
	    $data[$i]['id'] = $row->id;
	    $data[$i]['title'] = $row->title;
	    $data[$i]['description'] = $row->description;
	    $data[$i]['promotion_offer'] = $row->promotion_offer;
	    $data[$i]['image_path'] = "https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/promotion_image/marketing_promotion/".$row->image_path;
	    $data[$i]['start_date'] = date('d.m.Y', strtotime($row->start_date));
	    $data[$i]['end_date'] = date('d.m.Y', strtotime($row->end_date));
	    $is_expire = 1;
	    if(strtotime($row->end_date) >= strtotime(date("y-m-d H:m:s"))){
		$is_expire = 0;
	    }
	    $data[$i]['is_expired'] = $is_expire;
	    $i++;
	}
	
	return $data;
    }
    
    public function add_marketing_promotion_banner(){
	$promotion_offer = $this->input->post("promotion_offer");
	$title = $this->input->post("title");
	$description = $this->input->post("description");
	$redirect_link = $this->input->post("redirect_link");
	$start_date = date("Y-m-d H:m:s", strtotime($this->input->post("start_date")));
	$end_date = date("Y-m-d H:m:s", strtotime($this->input->post("end_date")));
	$image_path = '';
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
	
	if(isset($_POST['thumb_values'])){
            $logo_image_path = json_decode($_POST['thumb_values']);
            $image_name =  $logo_image_path->name;
            $image_src = $logo_image_path->data;
	    $temp = explode(".", $image_name);
	    $extension = end($temp);
	    $path = 'assets/media/promotion_image/marketing_promotion/';
	    $filename = basename($image_name);
	    $filename = strtotime(date('Y-m-d H:i:s')).'.'.$extension;
	    $image_path = $filename;
	    $explode = explode(',',$image_src);
	    $image_src = $explode['1'];
	    $image_src = str_replace(' ', '+', $image_src);
	    $data_img = base64_decode($image_src);
	    $file = $path . $filename;
	    $success = file_put_contents($file, $data_img);
	    $fname = $path.$filename;
	    $famazonname = AMAZONPATH.'promotion_image/marketing_promotion/'.$filename;
	    $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
        }
	/*if (isset($_FILES) && !empty($_FILES['banner_image']['name'])) {
	    $temp = explode(".", $_FILES["banner_image"]["name"]);
	    $extension = end($temp);
	    $path = 'assets/media/promotion_image/marketing_promotion/';
	    $filename = basename($_FILES["banner_image"]["name"]);
	    $filename = strtotime(date('Y-m-d H:i:s')).'.'.$extension;
	    $image_path = $filename;
	    move_uploaded_file($_FILES["banner_image"]["tmp_name"],$path . $filename);
        }*/
	$this->db->query("insert into sht_consumer_marketing_promotion (promotion_offer,title, description, image_path, redirect_link, start_date, end_date, status, is_deleted, created_on,isp_uid) values('$promotion_offer','$title', '$description', '$image_path', '$redirect_link', '$start_date', '$end_date', '1', '0', now(),'$isp_uid')");
	//die;
    }
    
    public function delete_marketing_promotion(){
	$id = $this->input->get('id');
	$this->db->query("update sht_consumer_marketing_promotion set is_deleted = '1', updated_on = now() where id = '$id'");
	
    }
    
    public function edit_marketing_promotion_view($id){
	$data = array();
	$query = $this->db->query("select * from sht_consumer_marketing_promotion where id = '$id'");
	foreach($query->result() as $row){
	    $data['title'] = $row->title;
	    $data['description'] = $row->description;
	    $data['promotion_offer'] = $row->promotion_offer;
	    $data['redirect_link'] = $row->redirect_link;
	    $data['start_date'] = date('d.m.Y H:m:s', strtotime($row->start_date));
	    $data['end_date'] = date('d.m.Y H:m:s', strtotime($row->end_date));
	    $data['image_path'] = "https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/promotion_image/marketing_promotion/".$row->image_path;
	}
	return $data;
    }
    public function update_marketing_promotion_banner(){
	$id = $this->input->post("promotion_id");
	$promotion_offer = $this->input->post("promotion_offer_edit");
	$title = $this->input->post("title");
	$description = $this->input->post("description");
	$redirect_link = $this->input->post("redirect_link");
	$start_date = date("Y-m-d H:m:s", strtotime($this->input->post("start_date")));
	$end_date = date("Y-m-d H:m:s", strtotime($this->input->post("end_date")));
	$image_path = '';
	if(isset($_POST['thumb_edit_values'])){
            $logo_image_path = json_decode($_POST['thumb_edit_values']);
            $image_name =  $logo_image_path->name;
            $image_src = $logo_image_path->data;
	    $temp = explode(".", $image_name);
	    $extension = end($temp);
	    $path = 'assets/media/promotion_image/marketing_promotion/';
	    $filename = basename($image_name);
	    $filename = strtotime(date('Y-m-d H:i:s')).'.'.$extension;
	    $image_path = $filename;
	    $explode = explode(',',$image_src);
	    $image_src = $explode['1'];
	    $image_src = str_replace(' ', '+', $image_src);
	    $data_img = base64_decode($image_src);
	    $file = $path . $filename;
	    $success = file_put_contents($file, $data_img);
	    $fname = $path.$filename;
	    $famazonname = AMAZONPATH.'promotion_image/marketing_promotion/'.$filename;
	    $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
        }
	/*if (isset($_FILES) && !empty($_FILES['banner_image']['name'])) {
	    $temp = explode(".", $_FILES["banner_image"]["name"]);
	    $extension = end($temp);
	    $path = 'assets/media/promotion_image/marketing_promotion/';
	    $filename = basename($_FILES["banner_image"]["name"]);
	    $filename = strtotime(date('Y-m-d H:i:s')).'.'.$extension;
	    $image_path = $filename;
	    move_uploaded_file($_FILES["banner_image"]["tmp_name"],$path . $filename);
        }*/
	$image_set_query = '';
	if($image_path != ''){
	    $image_set_query = ", image_path = '$image_path'";
	}
	$this->db->query("update sht_consumer_marketing_promotion set promotion_offer = '$promotion_offer', title = '$title', description = '$description', redirect_link = '$redirect_link',
			 start_date = '$start_date', end_date = '$end_date', updated_on = now() $image_set_query where id = '$id'");
	
    }
    
    public function check_ispmodules(){
	$moduleArr = array();
	$query = $this->db->query("SELECT module FROM sht_isp_admin_modules WHERE isp_uid='".ISPID."' AND status='1' AND (module='1' OR module='7')");
	if($query->num_rows() > 0){
	    foreach($query->result() as $mobj){
		$moduleArr[] = $mobj->module;
	    }
	}
	if(count($moduleArr) > 0){
	    if((count($moduleArr) == 1) && (in_array('7' , $moduleArr))){
		redirect(base_url().'publicwifi');
	    }
	}
    }
    
    public function isp_license_data(){
		$wallet_amt = 0; $passbook_amt = 0;
		$walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='".ISPID."' AND is_paid='1'");
		if($walletQ->num_rows() > 0){
		    $wallet_amt = $walletQ->row()->wallet_amt;
		}
		
		$passbookQ = $this->db->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost FROM sht_isp_passbook WHERE isp_uid='".ISPID."'");
		if($passbookQ->num_rows() > 0){
		    $passrowdata = $passbookQ->row();
		    $passbook_amt = $passrowdata->activeusers_cost;
		}
		$balanceamt = $wallet_amt - $passbook_amt;
		if($balanceamt < 0){
			return 0;
		}else{
			return 1;
		}
	}
    
}

?>
