<?php
class Emailer_model extends CI_Model{
    public function __construct(){
    }
    public function checkemailserver(){
	$isp_uid = ISPID;
	$email = 'vksgupta63@gmail.com';
	$from = ''; $fromname = '';
	$message = 'Hi, <br/></br/> We are checking your mail server is working properly or not. If you can see this emailer, kindly confirm us. <br/><br/> Team Decibel';
        $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
	
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	     $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetailArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
    
	if(count($config) > 0){
	    $this->email->initialize($config);
	    $this->email->from($from, $fromname);
	    $this->email->to($email);
	    $this->email->subject('Mailserver Installation');
	    $this->email->message($message);	
	    $this->email->send();
	    echo $this->email->print_debugger();
	    echo 'Email has been send successfully.';
	}else{
	    echo 'Oops! Please setup email gateway to send mails.';
	}
    }
    public function getcustomer_data($uuid){
        $userQuery = $this->db->get_where('sht_users', array('uid' => $uuid));
        $num_rows = $userQuery->num_rows();
        return $userQuery->row();
    }
    public function user_billing_listing($uuid, $billid){
        $query = $this->db->query('SELECT * FROM sht_subscriber_billing WHERE subscriber_uuid="'.$uuid.'" AND status="1" AND is_deleted="0" AND id="'.$billid.'" ORDER BY id DESC LIMIT 1');
        $num_rows = $query->num_rows();
        return $query->row();
    }
    public function custom_billing_listing($billid){
        $custbillQ = $this->db->query("SELECT * FROM sht_subscriber_custom_billing WHERE id='".$billid."' AND is_deleted='0' ORDER BY id DESC");
	return $custbillQ->row();
    }
    public function fetch_custombill_invoice($billid){
        $custbillQ = $this->db->query("SELECT * FROM sht_custom_invoice_list WHERE bill_id='".$billid."'");
	return $custbillQ->result();
    }
    
    public function getstatename($stateid){
        $name = '';
        $stateQ = $this->db->get_where('sht_states', array('id' => $stateid));
        $num_rows = $stateQ->num_rows();
        if($num_rows > 0){
                $data = $stateQ->row();
                $name = ucwords($data->state);
        }
        return $name;
    }
    public function getcityname($stateid, $cityid){
        $name = '';
        $cityQ = $this->db->get_where('sht_cities', array('city_state_id' => $stateid, 'city_id' => $cityid));
        $num_rows = $cityQ->num_rows();
        if($num_rows > 0){
                $data = $cityQ->row();
                $name = ucwords($data->city_name);
        }
        return $name;
    }
    public function getzonename($zoneid){
        $name = '';
        $zoneQ = $this->db->get_where('sht_zones', array('id' => $zoneid));
        $num_rows = $zoneQ->num_rows();
        if($num_rows > 0){
                $data = $zoneQ->row();
                $name = ucwords($data->zone_name);
        }
        return $name;
    }
    public function isp_details($isp_uid){
        $query = $this->db->query("SELECT * FROM sht_isp_detail WHERE isp_uid='".$isp_uid."' AND status='1'");
        if($query->num_rows() > 0){
            return  $query->row_array();
        }
    }
    
    public function getplan_details($srvid, $uuid=''){
        $data = array();
        $planQ = $this->db->query("SELECT tb1.srvname, tb1.descr, tb2.gross_amt, tb1.plan_duration, tb1.isp_uid, tb1.plantype FROM sht_services as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb1.srvid='".$srvid."' AND tb1.enableplan='1'");
        if($planQ->num_rows() > 0){
            $rowdata = $planQ->row();
            $isp_uid = $rowdata->isp_uid;
            $gross_amt = $rowdata->gross_amt;
	    $actual_amt = 0;
            
            $custom_planpriceArr = $this->getcustom_planprice($isp_uid, $uuid, $srvid);
            $custom_planprice = $custom_planpriceArr['gross_amt'];
            if($custom_planprice != ''){
		$actual_amt = $custom_planprice;
                $planprice = round($custom_planprice);
                $plan_duration = $rowdata->plan_duration;
                $plan_tilldays = ($plan_duration * 30);
            }else{
		$actual_amt = $gross_amt;
                $tax = $this->current_taxapplicable($isp_uid);
                $plan_duration = $rowdata->plan_duration;
                $gross_amt = ($gross_amt * $plan_duration);
                $planprice = round($gross_amt + (($gross_amt * $tax)/100));
                $plan_tilldays = ($plan_duration * 30);
            }
            
            $planname = $rowdata->srvname;
            $plandesc = $rowdata->descr;
            
            $data['planname'] = $planname;
            $data['plandesc'] = $plandesc;
            $data['price'] = $planprice;
	    $data['actual_price'] = $actual_amt;
            $data['plantype'] = $rowdata->plantype;
            $data['plan_duration'] = $plan_duration;
            $data['plan_tilldays'] = $plan_tilldays;
            
            return $data;
        }
    }
    public function gettopup_details($srvid){
        $data = array();
        $planQ = $this->db->query("SELECT tb1.srvname, tb1.descr, tb2.gross_amt, tb1.isp_uid, tb1.topuptype FROM sht_services as tb1 INNER JOIN sht_topup_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb1.srvid='".$srvid."' AND tb1.enableplan='1'");
        if($planQ->num_rows() > 0){
            $rowdata = $planQ->row();
            $isp_uid = $rowdata->isp_uid;
            $gross_amt = $rowdata->gross_amt;
            $planname = $rowdata->srvname;
            $plandesc = $rowdata->descr;
            
            $data['topupname'] = $planname;
            $data['topupdesc'] = $plandesc;
            $data['topupprice'] = $gross_amt;
            $data['topuptype'] = $rowdata->topuptype;
            
            return $data;
        }
    }

    public function appliedtopup_details($srvid){
	$data = array();
        $planQ = $this->db->query("SELECT topup_startdate, topup_enddate, applicable_days, topuptype FROM sht_usertopupassoc WHERE topup_id='".$srvid."' AND status='1' AND terminate='0' ORDER BY id DESC LIMIT 1");
        if($planQ->num_rows() > 0){
            $rowdata = $planQ->row();
            
            $data['topup_startdate'] = $rowdata->topup_startdate;
	    $data['topup_enddate'] = $rowdata->topup_enddate;
            $data['applicable_days'] = $rowdata->applicable_days;
            $data['topuptype'] = $rowdata->topuptype;
        }
	return $data;
    }
    public function get_isplogo($isp_uid){
        $query = $this->db->query("select tb1.portal_url, tb2.logo_image from sht_isp_admin as tb1 INNER JOIN sht_isp_detail as tb2 ON(tb1.isp_uid=tb2.isp_uid) where tb1.is_activated='1' AND tb1.isp_uid='".$isp_uid."'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return "https://www.shouut.com/ispadmins/".$rowdata->portal_url.".shouut.com/ispmedia/logo/".$rowdata->logo_image;
        }else{
            return  0;
        }
    }
    
    public function get_ispsignaturelogo($isp_uid){
        $query = $this->db->query("select tb1.portal_url, tb2.logo_signature from sht_isp_admin as tb1 INNER JOIN sht_isp_detail as tb2 ON(tb1.isp_uid=tb2.isp_uid) where tb1.is_activated='1' AND tb1.isp_uid='".$isp_uid."'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return "https://www.shouut.com/ispadmins/".$rowdata->portal_url.".shouut.com/ispmedia/logo/".$rowdata->logo_signature;
        }else{
            return  0;
        }
    }


    
    
    public function getIndianCurrency($number){
        $number = (float) $number;
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $hundred = null;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred','thousand','lakh', 'crore');
        while( $i < $digits_length ) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str[] = null;
        }
        $Rupees = implode('', array_reverse($str));
        $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
        return ($Rupees ? ucwords($Rupees) . 'Rupees ' : '') . $paise ;
    }
    public function current_taxapplicable($isp_uid){
        $query = $this->db->query("SELECT tax FROM sht_tax WHERE isp_uid='".$isp_uid."'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return $rowdata->tax;
        }
    }
    public function getisp_accounttype($isp_uid){
        $query = $this->db->query("SELECT decibel_account FROM sht_isp_admin WHERE isp_uid='".$isp_uid."' AND is_activated='1'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return $rowdata->decibel_account;
        }
    }
    public function userbalance_amount($uuid){
        $wallet_amt = 0;
        $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
        if($walletQ->num_rows() > 0){
                $wallet_amt = $walletQ->row()->wallet_amt;
        }
        
        $passbook_amt = 0;
        $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
        if($passbookQ->num_rows() > 0){
                $passbook_amt = $passbookQ->row()->plan_cost;
        }
        $balanceamt = $wallet_amt - $passbook_amt;
        return $balanceamt;
    }
    public function lastplan_activatedate($billid){
        $lastplan_activatedate = '';
        $lastplandateQ = $this->db->query("SELECT lastplan_activatedate FROM sht_userplan_datechange_records WHERE bill_id='".$billid."'");
        if($lastplandateQ->num_rows() > 0){
            $rowdata = $lastplandateQ->row();
            $lastplan_activatedate = $rowdata->lastplan_activatedate;
        }
        return $lastplan_activatedate;
    }
    public function country_currency($isp_uid){
	$currsymb = '';
	$ispcountryQ = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".$isp_uid."'");
	if($ispcountryQ->num_rows() > 0){
	    $crowdata = $ispcountryQ->row();
	    $countryid = $crowdata->country_id;
    
	    $countryQ = $this->db->query("SELECT currency_id FROM sht_countries WHERE id='".$countryid."'");
	    if($countryQ->num_rows() > 0){
		$rowdata = $countryQ->row();
		$currid = $rowdata->currency_id;
		
		$currencyQ = $this->db->get_where('sht_currency', array('currency_id' => $currid));
		if($currencyQ->num_rows() > 0){
		    $currobj = $currencyQ->row();
		    $currsymb = $currobj->currency_symbol;
		}
	    }
	}
	return $currsymb;
    }

    /*---- BILLING EMAILER STARTS ----*/
    public function planname($srvid){
	$planname = '-';
	$query = $this->db->query("SELECT tb2.srvname FROM sht_services as tb2 WHERE tb2.srvid='".$srvid."'");
	if($query->num_rows() > 0){
	    $planname = $query->row()->srvname;
	}
	return $planname;
    }



    public function getcustom_planprice($isp_uid, $uuid, $srvid){
        $data = array();
        $customplanQ = $this->db->query("SELECT baseplanid,gross_amt FROM sht_custom_plan_pricing WHERE isp_uid='".$isp_uid."' AND uid='".$uuid."' AND baseplanid='".$srvid."' ORDER BY id DESC LIMIT 1");
        $num_rows = $customplanQ->num_rows();
        if($num_rows > 0){
                $rowdata = $customplanQ->row();
                $data['baseplanid'] = $rowdata->baseplanid;
                $data['gross_amt'] = $rowdata->gross_amt;
        }else{
                $data['baseplanid'] = '';
                $data['gross_amt'] = '';
        }
        return $data;
    }


    public function installation_billing($uuid, $billid, $action=''){
	
	$apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
        $user_rowdata = $this->getcustomer_data($uuid);
        $state = $this->getstatename($user_rowdata->billing_state);
        $city = $this->getcityname($user_rowdata->billing_state,$user_rowdata->billing_city);
        $zone = $this->getzonename($user_rowdata->billing_zone);
        $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
	$gstin_number = $user_rowdata->gstin_number;
	$mobileno = $user_rowdata->billing_mobileno;
	$email = $user_rowdata->email;
        $isp_uid = $user_rowdata->isp_uid;
	$taxtype = $user_rowdata->taxtype;
	$tax = $this->current_taxapplicable($isp_uid);
	if($taxtype == '1'){
	    $apply_cgst_sgst_tax = 1;
	}else{
	    $apply_igst_tax = 1;
	}

        $next_bill_date = date('d-m-Y', strtotime($user_rowdata->next_bill_date));
        $billingdata = $this->user_billing_listing($uuid, $billid);
        $bill_number = $billingdata->bill_number;
        $payment_mode = $billingdata->payment_mode;
        $amount = $billingdata->actual_amount;
        $discount = $billingdata->discount;
        if($discount != 0){
            $discount_amt = ($amount * $billingdata->discount)/100;
        }else{
            $discount_amt = 0;
        }
        $total_amount = $billingdata->total_amount;
	$service_tax = $billingdata->service_tax;
	$swachh_bharat_tax = $billingdata->swachh_bharat_tax;
	$krishi_kalyan_tax = $billingdata->krishi_kalyan_tax;
	$cgst_tax = $billingdata->cgst_tax;
	$sgst_tax = $billingdata->sgst_tax;
	$igst_tax = $billingdata->igst_tax;
	$billing_with_tax = $billingdata->billing_with_tax;
        $amount_inwords = $this->getIndianCurrency($total_amount);
        
        $bill_date = date('d M Y', strtotime($billingdata->bill_added_on));
	$hsn_sac = (($billingdata->hsn_number) != '') ? $billingdata->hsn_number : '9987';
	$bill_comments = $billingdata->bill_comments;
	if($bill_comments != ''){
	    $bill_comments = ' '.$bill_comments;
	}
	$bill_remarks = $billingdata->bill_remarks;
	if($bill_remarks != ''){
	    $bill_remarks = 'Remarks: '.$bill_remarks;
	}
	$currsymbol = $this->country_currency($isp_uid);
	
	$billremark_commnt = '';
	if($action == 'sendbill'){
	    $billremark_commnt = '<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.'</td>';
	}else{
	    $billremark_commnt = '<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>';
	}
	
	if($billing_with_tax == '1'){
	    if($service_tax != '0.00'){
		$actual_billamt = $billingdata->actual_amount;
		$tax_msg .= '
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Service Tax</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($service_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Krishi Kalyan Tax</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($krishi_kalyan_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="4" style="text-align: right">
			</td>
		     </tr>
		     <tr>'.$billremark_commnt.'
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			      NET AMOUNT (Including Tax)
			      </span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			   </p>
			</td>
		     </tr>
		';
		
	       
	    }
	    else{
		if($apply_cgst_sgst_tax == 1){
		    $tax = (100 + $tax);
		    $actual_billamt1 = round(($total_amount * 100) /  $tax);
		    if(($discounttype == 'percent') && ($discount != 0)){
			$discount_amt = round(($actual_billamt1 * $billingdata->discount)/100);
		    }elseif(($discounttype == 'flat') && ($discount != 0)){
			$discount_amt = $billingdata->discount;
		    }else{
			$discount_amt = 0;
		    }
		    $actual_billamt = $actual_billamt1 + $discount_amt; 
		    $after_disc_billamt = ($actual_billamt - $discount_amt);
		    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
			$taxamt = ($total_amount - $actual_billamt1);
			$cgst_tax = round($taxamt/2);
			$sgst_tax = round($taxamt/2);
		    }
		    
		    $tax_msg .= '
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
			       </p>
			    </td>
			</tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
			       </p>
			    </td>
			 </tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">CGST @9%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($cgst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">SGST @9%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($sgst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="4" style="text-align: right">
			    </td>
			 </tr>
			 <tr>'.$billremark_commnt.'
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
				  NET AMOUNT (Including Tax)
				  </span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			       </p>
			    </td>
			 </tr>
		    ';
		}else{
		    $tax = (100 + $tax);
		    $actual_billamt1 = round(($total_amount * 100) /  $tax);
		    if(($discounttype == 'percent') && ($discount != 0)){
			$discount_amt = round(($actual_billamt1 * $billingdata->discount)/100);
		    }elseif(($discounttype == 'flat') && ($discount != 0)){
			$discount_amt = $billingdata->discount;
		    }else{
			$discount_amt = 0;
		    }
		    $actual_billamt = $actual_billamt1 + $discount_amt; 
		    $after_disc_billamt = ($actual_billamt - $discount_amt);
		    if($igst_tax == '0.00'){
			$taxamt = ($total_amount - $actual_billamt1);
			$igst_tax = round($taxamt);
		    }
		    
		    $tax_msg .= '
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
			       </p>
			    </td>
			</tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
			       </p>
			    </td>
			 </tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">IGST @18%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($igst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="4" style="text-align: right">
			    </td>
			 </tr>
			 <tr>'.$billremark_commnt.'
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
				  NET AMOUNT (Including Tax)
				  </span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			       </p>
			    </td>
			 </tr>
		    ';
		    
		}
    
	    }
	    
	}
	else{
	    $actual_billamt1 = $billingdata->total_amount;
	    if(($discounttype == 'percent') && ($discount != 0)){
		$discount_amt = round(($actual_billamt1 * $billingdata->discount)/100);
	    }elseif(($discounttype == 'flat') && ($discount != 0)){
		$discount_amt = $billingdata->discount;
	    }else{
		$discount_amt = 0;
	    }
	    $actual_billamt = $actual_billamt1 + $discount_amt; 
	    $after_disc_billamt = ($actual_billamt - $discount_amt);
	    

	    $tax_msg .= '
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
		       </p>
		    </td>
		</tr>
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    '.$billremark_commnt.'
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	    
	}
	$ispdetailArr = $this->isp_details($isp_uid);
        $company_name = $ispdetailArr['company_name'];
        $address = $ispdetailArr['address1'];
        $helpline = $ispdetailArr['help_number1'];
        $support_email = $ispdetailArr['support_email'];
        $isp_name = ucwords($ispdetailArr['isp_name']);
        $ispgst_number = $ispdetailArr['gst_no'];
	$isp_service_tax_no = $ispdetailArr['service_tax_no'];
	$ispcin_number = $ispdetailArr['cin_number'];
	$isppancard = $ispdetailArr['pancard'];
	$isp_website = $ispdetailArr['website'];
        $isp_state = $this->getstatename($ispdetailArr['state']);
        $isplogo = $this->get_isplogo($isp_uid);
	$isp_signature = $this->get_ispsignaturelogo($isp_uid);
	
	$invoice_color = $ispdetailArr['invoice_color'];
	$invoice_banner1 = $ispdetailArr['invoice_banner1'];
	$invoice_banner2 = $ispdetailArr['invoice_banner2'];
	$invoice_statecode = $ispdetailArr['invoice_statecode'];
	
	
	$isptaxdet = '';
	$billmonth = date('m', strtotime($bill_date));
	$billyear = date('Y', strtotime($bill_date));
	if(($billmonth < 7) && ($billyear == '2017')){
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">Service Tax : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isp_service_tax_no.'</span>';
	}else{
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">GSTIN : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispgst_number.'</span>';
	}
	
        $message = '';
        
	if($action == 'sendbill'){
	$message .= '
	<!doctype html>
	    <html>
	       <head>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <title></title>
		  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=greek" rel="stylesheet">
		  <link rel="stylesheet" href="./source/invoice_emailer/css/font-awesome.min.css">
		  <style type="text/css">
		     html,  body {
		     margin: 0 !important;
		     padding: 0 !important;
		     height: 100% !important;
		     width: 100% !important;
		     }
		     * {
		     -ms-text-size-adjust: 100%;
		     -webkit-text-size-adjust: 100%;
		     }
		     .ExternalClass {
		     width: 100%;
		     }

		     div[style*="margin: 10px 0"] {
		     margin: 0 !important;
		     }

		     table,  td {
		     mso-table-lspace: 0pt !important;
		     mso-table-rspace: 0pt !important;
		     }
		     
		     table {
		     border-spacing: 0 !important;
		     border-collapse: collapse !important;
		     table-layout: fixed !important;
		     margin: 0 auto !important;
		     }
		     table table table {
		     table-layout: auto;
		     }

		     img {
		     -ms-interpolation-mode: bicubic;
		     }

		     .yshortcuts a {
		     border-bottom: none !important;
		     }

		     a[x-apple-data-detectors] {
		     color: inherit !important;
		     }
		  </style>
		  <style type="text/css">

		     .button-td,
		     .button-a {
		     transition: all 100ms ease-in;
		     }
		     .button-td:hover,
		     .button-a:hover {
		     background: #2fbdf2 !important;
		     border-color: #2fbdf2 !important;
		     }

		     .button-get-td,
		     .button-get-a {
		     transition: all 100ms ease-in;
		     }
		     .button-get-td:hover,
		     .button-get-a:hover {
		     background: #545454 !important;
		     border-color: #545454 !important;
		     }
		     /* Media Queries */
		     @media screen and (max-width: 600px) {
		     .email-container {
		     width: 100% !important;
		     }

		     .fluid,
		     .fluid-centered {
		     max-width: 100% !important;
		     height: auto !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* And center justify these ones. */
		     .fluid-centered {
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* What it does: Forces table cells into full-width rows. */
		     .stack-column,
		     .stack-column-center {
		     display: block !important;
		     width: 100% !important;
		     max-width: 100% !important;
		     direction: ltr !important;
		     }
		     /* And center justify these ones. */
		     .stack-column-center {
		     text-align: center !important;
		     }
		     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
		     .center-on-narrow {
		     text-align: center !important;
		     display: block !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     float: none !important;
		     }
		     table.center-on-narrow {
		     display: inline-block !important;
		     }
		     }
		  </style>
	       </head>
	       <body bgcolor="#ffffff"  style="margin: 0;">';
	}
	$message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Installation Charges</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">1. Installation Charges</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"><img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
	       </body>
	    </html>
	
	';
	
        
        
	$from = ''; $fromname = '';
        $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
	
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	     $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetailArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
	
        if($action == 'viewbill'){
            echo $message;
        }elseif($action == 'sendbill'){      
            if(count($config) > 0){
                $this->email->initialize($config);
                $this->email->from($from, $fromname);
                $this->email->to($email);
                $this->email->subject('Installation Cost Bill');
                $this->email->message($message);	
                $this->email->send();
                echo 'Email has been send successfully.';
            }else{
                echo 'Oops! Please setup email gateway to send mails.';
            }
        }elseif($action == 'downloadbill'){
            
            echo 'Download Option will be provided soon.';
        }else{
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject('Installation Cost Bill');
            $this->email->message($message);	
            $this->email->send();
            return 1;
        }
    }
    
    public function security_billing($uuid, $billid, $action=''){
	
	$apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
        $user_rowdata = $this->getcustomer_data($uuid);
        $state = $this->getstatename($user_rowdata->billing_state);
        $city = $this->getcityname($user_rowdata->billing_state,$user_rowdata->billing_city);
        $zone = $this->getzonename($user_rowdata->billing_zone);
        $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
	$gstin_number = $user_rowdata->gstin_number;
	$mobileno = $user_rowdata->billing_mobileno;
	$email = $user_rowdata->email;
        $isp_uid = $user_rowdata->isp_uid;
	$taxtype = $user_rowdata->taxtype;
	$tax = $this->current_taxapplicable($isp_uid);
	if($taxtype == '1'){
	    $apply_cgst_sgst_tax = 1;
	}else{
	    $apply_igst_tax = 1;
	}

        $next_bill_date = date('d-m-Y', strtotime($user_rowdata->next_bill_date));
        $billingdata = $this->user_billing_listing($uuid, $billid);
        $bill_number = $billingdata->bill_number;
        $payment_mode = $billingdata->payment_mode;
        $amount = $billingdata->actual_amount;
        $discount = $billingdata->discount;
	$discounttype = $billingdata->discounttype;
	$billing_with_tax = $billingdata->billing_with_tax;
	
        if($discount != 0){
            $discount_amt = ($amount * $billingdata->discount)/100;
        }else{
            $discount_amt = 0;
        }
        $total_amount = $billingdata->total_amount;
	$service_tax = $billingdata->service_tax;
	$swachh_bharat_tax = $billingdata->swachh_bharat_tax;
	$krishi_kalyan_tax = $billingdata->krishi_kalyan_tax;
	$cgst_tax = $billingdata->cgst_tax;
	$sgst_tax = $billingdata->sgst_tax;
	$igst_tax = $billingdata->igst_tax;
        $amount_inwords = $this->getIndianCurrency($total_amount);
        
        $bill_date = date('d-m-Y', strtotime($billingdata->bill_added_on));
	$hsn_sac = (($billingdata->hsn_number) != '') ? $billingdata->hsn_number : '9987';
	$bill_comments = $billingdata->bill_comments;
	if($bill_comments != ''){
	    $bill_comments = ' '.$bill_comments;
	}
	$bill_remarks = $billingdata->bill_remarks;
	if($bill_remarks != ''){
	    $bill_remarks = 'Remarks: '.$bill_remarks;
	}
	
	$currsymbol = $this->country_currency($isp_uid);
	
	$billremark_commnt = '';
	if($action == 'sendbill'){
	    $billremark_commnt = '<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.'</td>';
	}else{
	    $billremark_commnt = '<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>';
	}
	
	if($billing_with_tax == '1'){
	    if($service_tax != '0.00'){
		$actual_billamt = $billingdata->actual_amount;
		$tax_msg .= '
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Service Tax</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($service_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Krishi Kalyan Tax</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($krishi_kalyan_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="4" style="text-align: right">
			</td>
		     </tr>
		     <tr>
			<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			      NET AMOUNT (Including Tax)
			      </span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			   </p>
			</td>
		     </tr>
		';
		
	       
	    }
	    else{
		if($apply_cgst_sgst_tax == 1){
		    /*$tax = (100 + $tax);
		    $actual_billamt = round(($total_amount * 100) /  $tax);
		    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
			$taxamt = ($total_amount - $actual_billamt);
			$cgst_tax = round($taxamt/2);
			$sgst_tax = round($taxamt/2);
		    }*/
		    
		    $tax = (100 + $tax);
		    $actual_billamt1 = round(($total_amount * 100) /  $tax);
		    if(($discounttype == 'percent') && ($discount != 0)){
			$discount_amt = round(($actual_billamt1 * $billingdata->discount)/100);
		    }elseif(($discounttype == 'flat') && ($discount != 0)){
			$discount_amt = $billingdata->discount;
		    }else{
			$discount_amt = 0;
		    }
		    $actual_billamt = $actual_billamt1 + $discount_amt; 
		    $after_disc_billamt = ($actual_billamt - $discount_amt);
		    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
			$taxamt = ($total_amount - $actual_billamt1);
			$cgst_tax = round($taxamt/2);
			$sgst_tax = round($taxamt/2);
		    }
		    
		    $tax_msg .= '
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
			       </p>
			    </td>
			</tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
			       </p>
			    </td>
			 </tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">CGST @9%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($cgst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">SGST @9%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($sgst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="4" style="text-align: right">
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
				  NET AMOUNT (Including Tax)
				  </span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			       </p>
			    </td>
			 </tr>
		    ';
		}
		else{
		    /*$tax = (100 + $tax);
		    $actual_billamt = round(($total_amount * 100) /  $tax);
		    if($igst_tax == '0.00'){
			$igst_tax = ($total_amount - $actual_billamt);
		    }*/
		    $tax = (100 + $tax);
		    $actual_billamt1 = round(($total_amount * 100) /  $tax);
		    if(($discounttype == 'percent') && ($discount != 0)){
			$discount_amt = round(($actual_billamt1 * $billingdata->discount)/100);
		    }elseif(($discounttype == 'flat') && ($discount != 0)){
			$discount_amt = $billingdata->discount;
		    }else{
			$discount_amt = 0;
		    }
		    $actual_billamt = $actual_billamt1 + $discount_amt; 
		    $after_disc_billamt = ($actual_billamt - $discount_amt);
		    if($igst_tax == '0.00'){
			$taxamt = ($total_amount - $actual_billamt1);
			$igst_tax = round($taxamt);
		    }
		    $tax_msg .= '
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">IGST @18%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($igst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="4" style="text-align: right">
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
				  NET AMOUNT (Including Tax)
				  </span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			       </p>
			    </td>
			 </tr>
		    ';
		    
		}
	    }
	}
	else{	    
	    $actual_billamt1 = $billingdata->total_amount;
	    if(($discounttype == 'percent') && ($discount != 0)){
		$discount_amt = round(($actual_billamt1 * $billingdata->discount)/100);
	    }elseif(($discounttype == 'flat') && ($discount != 0)){
		$discount_amt = $billingdata->discount;
	    }else{
		$discount_amt = 0;
	    }
	    $actual_billamt = $actual_billamt1 + $discount_amt; 
	    $after_disc_billamt = ($actual_billamt - $discount_amt);
	    

	    $tax_msg .= '
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
		       </p>
		    </td>
		</tr>
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    '.$billremark_commnt.'
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	}
        $ispdetailArr = $this->isp_details($isp_uid);
        $company_name = $ispdetailArr['company_name'];
        $address = $ispdetailArr['address1'];
        $helpline = $ispdetailArr['help_number1'];
        $support_email = $ispdetailArr['support_email'];
        $isp_name = ucwords($ispdetailArr['isp_name']);
        $ispgst_number = $ispdetailArr['gst_no'];
	$isp_service_tax_no = $ispdetailArr['service_tax_no'];
	$ispcin_number = $ispdetailArr['cin_number'];
	$isppancard = $ispdetailArr['pancard'];
	$isp_website = $ispdetailArr['website'];
        $isp_state = $this->getstatename($ispdetailArr['state']);
        $isplogo = $this->get_isplogo($isp_uid);
	$isp_signature = $this->get_ispsignaturelogo($isp_uid);
	
	$invoice_color = $ispdetailArr['invoice_color'];
	$invoice_banner1 = $ispdetailArr['invoice_banner1'];
	$invoice_banner2 = $ispdetailArr['invoice_banner2'];
	$invoice_statecode = $ispdetailArr['invoice_statecode'];
	
	
	$isptaxdet = '';
	$billmonth = date('m', strtotime($bill_date));
	$billyear = date('Y', strtotime($bill_date));
	if(($billmonth < 7) && ($billyear == '2017')){
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">Service Tax : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isp_service_tax_no.'</span>';
	}else{
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">GSTIN : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispgst_number.'</span>';
	}
	
        $message = '';
        
	if($action == 'sendbill'){
	$message .= '
	<!doctype html>
	    <html>
	       <head>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <title></title>
		  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=greek" rel="stylesheet">
		  <link rel="stylesheet" href="./source/invoice_emailer/css/font-awesome.min.css">
		  <style type="text/css">
		     html,  body {
		     margin: 0 !important;
		     padding: 0 !important;
		     height: 100% !important;
		     width: 100% !important;
		     }
		     * {
		     -ms-text-size-adjust: 100%;
		     -webkit-text-size-adjust: 100%;
		     }
		     .ExternalClass {
		     width: 100%;
		     }

		     div[style*="margin: 10px 0"] {
		     margin: 0 !important;
		     }

		     table,  td {
		     mso-table-lspace: 0pt !important;
		     mso-table-rspace: 0pt !important;
		     }
		     
		     table {
		     border-spacing: 0 !important;
		     border-collapse: collapse !important;
		     table-layout: fixed !important;
		     margin: 0 auto !important;
		     }
		     table table table {
		     table-layout: auto;
		     }

		     img {
		     -ms-interpolation-mode: bicubic;
		     }

		     .yshortcuts a {
		     border-bottom: none !important;
		     }

		     a[x-apple-data-detectors] {
		     color: inherit !important;
		     }
		  </style>
		  <style type="text/css">

		     .button-td,
		     .button-a {
		     transition: all 100ms ease-in;
		     }
		     .button-td:hover,
		     .button-a:hover {
		     background: #2fbdf2 !important;
		     border-color: #2fbdf2 !important;
		     }

		     .button-get-td,
		     .button-get-a {
		     transition: all 100ms ease-in;
		     }
		     .button-get-td:hover,
		     .button-get-a:hover {
		     background: #545454 !important;
		     border-color: #545454 !important;
		     }
		     /* Media Queries */
		     @media screen and (max-width: 600px) {
		     .email-container {
		     width: 100% !important;
		     }

		     .fluid,
		     .fluid-centered {
		     max-width: 100% !important;
		     height: auto !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* And center justify these ones. */
		     .fluid-centered {
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* What it does: Forces table cells into full-width rows. */
		     .stack-column,
		     .stack-column-center {
		     display: block !important;
		     width: 100% !important;
		     max-width: 100% !important;
		     direction: ltr !important;
		     }
		     /* And center justify these ones. */
		     .stack-column-center {
		     text-align: center !important;
		     }
		     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
		     .center-on-narrow {
		     text-align: center !important;
		     display: block !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     float: none !important;
		     }
		     table.center-on-narrow {
		     display: inline-block !important;
		     }
		     }
		  </style>
	       </head>
	       <body bgcolor="#ffffff"  style="margin: 0;">';
	}
	$message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">SECURITY INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Security Deposit</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">Security Deposit</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>';
	if($action == 'sendbill'){
	$message .= '
		</body>
	    </html>';
	}
        
	$from = ''; $fromname = '';
        $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
	
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	     $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetailArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
	
        if($action == 'viewbill'){
            echo $message;
        }elseif($action == 'sendbill'){      
            if(count($config) > 0){
                $this->email->initialize($config);
                $this->email->from($from, $fromname);
                $this->email->to($email);
                $this->email->subject('Installation Cost Bill');
                $this->email->message($message);	
                $this->email->send();
                echo 'Email has been send successfully.';
            }else{
                echo 'Oops! Please setup email gateway to send mails.';
            }
        }elseif($action == 'downloadbill'){
            
            echo 'Download Option will be provided soon.';
        }else{
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject('Installation Cost Bill');
            $this->email->message($message);	
            $this->email->send();
            return 1;
        }
    }
    
    public function getisp_portalurl($isp_uid){
        $query = $this->db->query("SELECT portal_url FROM sht_isp_admin WHERE isp_uid='".$isp_uid."'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return $rowdata->portal_url;
        }else{
            return '';
        }
    }
    public function activate_user_emailer($uuid, $radchk_pwd){
        $this->load->library('email');
        $data = array();
        $user_rowdata = $this->getcustomer_data($uuid);
        $state = $this->getstatename($user_rowdata->state);
        $city = $this->getcityname($user_rowdata->state,$user_rowdata->city);
        $zone = $this->getzonename($user_rowdata->zone);
        $baseplanid = $user_rowdata->baseplanid;
        
        $email = $user_rowdata->email;
        $isp_uid = $user_rowdata->isp_uid;
        $ispdetailArr = $this->isp_details($isp_uid);
        $company_name = $ispdetailArr['company_name'];
        $address = $ispdetailArr['address1']." , ".$ispdetailArr['address2'];
        $helpline = $ispdetailArr['help_number1'];
        $support_email = $ispdetailArr['support_email'];
        $isp_name = ucwords($ispdetailArr['isp_name']);
        $gst_number = $ispdetailArr['gst_no'];
	$ispcin_number = $ispdetailArr['cin_number'];
        $isp_state = $this->getstatename($ispdetailArr['state']);
        $isplogo = $this->get_isplogo($isp_uid);
        $plandetails = $this->getplan_details($baseplanid);
        
        $ispacctQ = $this->db->query("SELECT tb1.portal_url, tb1.decibel_account FROM sht_isp_admin as tb1 WHERE tb1.isp_uid='".$isp_uid."'");
        if($ispacctQ->num_rows() > 0){
            $isprowdata = $ispacctQ->row();
            $portal_url = $isprowdata->portal_url;
            $decibel_account = $isprowdata->decibel_account;
            if($decibel_account == 'paid'){
                $ispurl = 'https://'.$portal_url.'.shouut.com/consumer/';
            }else{
                $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/consumer/';
            }
        }
        
        $message = '<!doctype html>
        <html>
           <head>
              <meta charset="utf-8">
              <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" >
              <meta http-equiv="x-ua-compatible" content="ie=edge">
              <link rel="icon" type="image/png" href="'.$isplogo.'"/>
              <title>'.$company_name.'</title>
           </head>
           <body style="color:#36465F">
              <body>
                 <table width="900" align="center" style="border:1px solid #A3A3A3;" cellpadding="0px" cellspacing="0px">
                    <tr>
                       <td>
                          <table width="900" align="center" cellpadding="0px" cellspacing="0px" border="0px">
                             <tr>
                                <td width="420" style="background-color:#F4F2F3;" valign="top">
                                   <table width="100%" border="0px" style="border-bottom-right-radius:15px; background-color:#FFFFFF;">
                                      <tr>
                                         <td height="30" width="50px" />
                                      </tr>
                                      <tr>
                                         <td valign="top" align="left" style="padding-left:20px">
                                            <!-- LOGO start--><img src="'.$isplogo.'" alt="logo" width="160px" /><!-- LOGO ENDS-->
                                         </td>
                                      </tr>
                                      <tr>
                                         <td style="font-family: \'Open Sans\', sans-serif; font-size:12px; padding-left:20px;font-weight:bold;">
                                            <!-- Address starts--><span style="color:#F00F64;font-weight:bold;"><br /> '.$company_name.',</span><br />
                                            <table width="100%" border="0px">
                                               <tr>
                                                  <td style="font-family: \'Open Sans\', sans-serif; font-size:12px;font-weight:bold;">'.$address.',</td>
                                               </tr>
                                               <tr>
                                                  <td style="font-family: \'Open Sans\', sans-serif; font-size:12px;font-weight:bold;"> '.$ispdetailArr['city'].' - '.$ispdetailArr['pincode'].'</td>
                                               </tr>
                                               <tr>
                                                  <td style="font-family: \'Open Sans\', sans-serif; font-size:12px;font-weight:bold;">Ph.No : +91 '.$ispdetailArr['help_number1'].'</td>
                                               </tr>
                                               <tr>
                                                  <td style="font-family: \'Open Sans\', sans-serif; font-size:12px;font-weight:bold;">E-mail : '.$ispdetailArr['support_email'].' </td>
                                               </tr>
                                               <tr>
                                                  <td style="font-family: \'Open Sans\', sans-serif; font-size:11px;font-weight:bold;">GSTIN : '.$ispdetailArr['gst_no'].'</td>
                                               </tr>
					       <tr>
                                                  <td style="font-family: \'Open Sans\', sans-serif; font-size:11px;font-weight:bold;">CIN : '.$ispcin_number.'</td>
                                               </tr>
                                            </table>
                                            <!-- Address ENDS-->
                                         </td>
                                      </tr>
                                   </table>
                                </td>
                                <td width="480" style="background-color:#F4F2F3;" valign="top">
                                   <table width="100%" border="0px" cellpadding="0px" cellspacing="0px">
                                      <tr>
                                         <td width="100%" style="background-color:#FFFFFF;" align="right" valign="top">&nbsp;
                                         </td>
                                      </tr>
                                      <tr>
                                         <td bgcolor="#FFFFFF" style="height:auto;">
                                            <table width="100%" border="0px" style="border-top-left-radius:15px;background-color:#F4F2F3;">
                                               <tr>
                                                  <td width="90%" height="195px" style="padding-left:35px;font-family: \'Open Sans\', sans-serif; font-size:12px;font-weight:600;">
                                                     <table width="84%" border="0px">
                                                        <tr>
                                                           <td style="font-family: \'Open Sans\', sans-serif; font-size:12px;font-weight:600;"><span style="line-height: 25px">Name</span> : <span style="line-height: 25px">'.ucfirst($user_rowdata->firstname).' '.ucfirst($user_rowdata->lastname).'</span></td>
                                                        </tr>
                                                        <tr>
                                                           <td style="font-family: \'Open Sans\', sans-serif; font-size:12px;font-weight:600;">Address : '.$user_rowdata->address.'</td>
                                                        </tr>
                                                        <tr>
                                                           <td style="font-family: \'Open Sans\', sans-serif; font-size:12px;font-weight:600;">'.$state.'</td>
                                                        </tr>
                                                        <tr>
                                                           <td style="font-family: \'Open Sans\', sans-serif; font-size:12px;font-weight:600;">India</td>
                                                        </tr>
                                                        <tr>
                                                           <td style="font-family: \'Open Sans\', sans-serif; font-size:12px;font-weight:600;">Home : </td>
                                                        </tr>
                                                        <tr>
                                                           <td style="font-family: \'Open Sans\', sans-serif; font-size:12px;font-weight:600;">Mobile : '.$user_rowdata->mobile.'</td>
                                                        </tr>
                                                        <tr>
                                                           <td style="font-family: \'Open Sans\', sans-serif; font-size:11px;font-weight:600;">GSTIN : </td>
                                                        </tr>
                                                     </table>
                                                  </td>
                                                  <td width="10%">
                                                     &nbsp;
                                                  </td>
                                               </tr>
                                            </table>
                                         </td>
                                      </tr>
                                      <tr height="20px">
                                         <td>&nbsp;</td>
                                      </tr>
                                   </table>
                                </td>
                             </tr>
                             <tr>
                                <td colspan="2" style="background-color:#F4F2F3;"><br /></td>
                             </tr>
                             <tr>
                                <td colspan="2" style="background-color:#F4F2F3;"><br /></td>
                             </tr>
                             <tr bgcolor="#FFFFFF">
                                <td style="background-color:#FFFFFF;" width="50%">
                                    <table width="100%" style="border-top-left-radius:0px; background-color:#F4F2F3;" cellpadding="5px" cellspacing="0px">
                                            <tr>
                                                    <td style="font-family: \'Open Sans\', sans-serif; font-size:14px;font-weight:500; padding-left:20px">Hi, '.ucfirst($user_rowdata->firstname).' '.ucfirst($user_rowdata->lastname).'</td>
                                            </tr>
                                            <tr>
                                                    <td style="font-family: \'Open Sans\', sans-serif; font-size:14px;font-weight:500; padding-left:20px">Now, you can login to your customer portal to upgrade plans, add topups, payments and many more exciting stuff. You can visit the link: <a href="'.$ispurl.'" target="_blank">Click Here</a></td>
                                            </tr>
                                            <tr>
                                                    <td style="font-family: \'Open Sans\', sans-serif; font-size:14px;font-weight:600; padding-left:20px">YOUR ACCOUNT CREDENTIALS ARE AS BELOW:</td>
                                            </tr>
                                            <tr>
                                                    <td style="padding-left:20px">
                                                            <span style="font-family: \'Open Sans\', sans-serif; font-size:14px;font-weight:400;">Username:</span> <span style="font-family: \'Open Sans\', sans-serif; font-size:14px;font-weight:600;">'.$user_rowdata->uid.'</span>
                                                    </td>
                                            </tr>
                                            <tr>
                                                   <td style="padding-left:20px">
                                                            <span style="font-family: \'Open Sans\', sans-serif; font-size:14px;font-weight:400;">Password:</span> <span style="font-family: \'Open Sans\', sans-serif; font-size:14px;font-weight:600;">'.$user_rowdata->orig_pwd.'</span>
                                                    </td>
                                            </tr>
                                            
                                    </table> 
                                </td>
                                <td bgcolor="#F4F2F3" width="50%" valign="top" style="padding-right:20px">
                                   <table width="100%" border="0px" cellpadding="0px" cellspacing="0px">
                                      <tr>
                                            <td colspan="2" bgcolor="#F4F2F3">
                                               <table width="100%" border="0px" cellpadding="5px" cellspacing="0px" style=" border-top-left-radius:5px;border-top-right-radius:5px;border: 1px solid #a1a1a1;">
                                                  <tr style="background-color:#ccc; color:#333; font-weight:700;" align="center" height="25px">
                                                     <td style="font-family: Open Sans, sans-serif; font-size:12px;border-right:1px solid #a1a1a1;border-bottom:1px  solid white;">Plan Name</td>
                                                     <td style="font-family: Open Sans, sans-serif; font-size:12px;border-right:1px solid #a1a1a1;border-bottom:1px  solid white;">Plan Description</td>
                                                     <td style="font-family: Open Sans, sans-serif; font-size:12px;border-bottom:1px  solid white;">Plan Price</td>
                                                  </tr>
                                                  <tr align="center" bgcolor="#FFFFFF">
                                                     <td style="font-family: Open Sans, sans-serif; font-size:12px;border-right: 1px solid #a1a1a1;">'.$plandetails['planname'].'</td>
                                                     <td style="font-family: Open Sans, sans-serif; font-size:12px;border-right: 1px solid #a1a1a1;">'.$plandetails['plandesc'].'</td>
                                                     <td style="font-family: Open Sans, sans-serif; font-size:12px">&#8377; '.number_format($plandetails['price'], 2).' /pm</td>
                                                  </tr>
                                               </table>
                                            </td>
                                     </tr>
                                   </table>
                                </td>
                             </tr>
                            
                             <tr bgcolor="#F4F2F3">
                                <td colspan="2" style="color:#36465f; text-align: right; font-weight:600;font-family: \'Open Sans\', sans-serif; font-size:12px; padding:10px">
                                   <p style="margin:0px">For Legal Entity</p>
                                   <p style="margin:0px">&nbsp;</p>
                                   <p style="margin:0px">'.$isp_name.'</p>
                                </td>
                             </tr>
                             <tr>
                                <td colspan="2" style="background-color:#F4F2F3;"><br /></td>
                             </tr>
                             <tr>
                                <td colspan="2" style="background-color:#F4F2F3;"><br /></td>
                             </tr>
                             <tr bgcolor="#F4F2F3">
                                <td colspan="2">
                                   <table width="95%" border="0px" align="center" cellpadding="0px" cellspacing="0px" style="border-radius:15px;background-color:#CCCCCC;">
                                      <tr height="25px" style="font-weight:bold; font-size:12px">
                                         <td style="padding-left:20px; font-size:20px; color:#000" height="50px; 
                                            font-family: \'Open Sans\', sans-serif;">Terms and Conditions </td>
                                      </tr>
                                      <tr>
                                         <td style="font-family: \'Open Sans\', sans-serif; font-size:12px;color:#7A7A7A;font-weight:500; border-radius:15px">
                                            <table width="100%" border="0px" align="center" cellpadding="0px" cellspacing="0px" style="font-family: \'Open Sans\', sans-serif; font-size:12px;border-bottom-right-radius:15px;border-bottom-left-radius:15px;background-color:#FFF; padding:0px 5px">
                                               <tr>
                                                  <td>
                                                     <!-- start Terms and conditions!-->
                                                     <ol style="line-height:25px">
                                                        <br />
                                                        <li>1. Please make your payments before the due date to enjoy uninterrupted service of your internet connection.</li>
                                                        <li>2. If the service is discontinued for non-payment of dues, then a reconnection charge may be applicable.</li>
                                                        <li>3. Late payment charges may be applicable after the payment due date.</li>
                                                        <li>4. All invoice disputes should be highlighted within 7 days of invoice generation.</li>
                                                        <li>5. GST charges as applicable and notified by the Govt. of India would be applicable.</li>
                                                        <li>6. All disputes are subject to the courts of '.$state.'</li>
                                                        <li>7. For any queries on the invoice please call '.$helpline.' or write at '.$support_email.'.</li>
                                                     </ol>
                                                     <!-- End Terms and conditions!-->
                                                  </td>
                                               </tr>
                                            </table>
                                         </td>
                                      </tr>
                                   </table>
                                </td>
                             </tr>
                             <tr bgcolor="#F4F2F3">
                                <td colspan="2" height="20px" />
                             </tr>
                             <tr bgcolor="#F4F2F3">
                                <td colspan="2" height="20px" />
                             </tr>
                             <tr>
                                <td colspan="2" height="5px" />
                             </tr>
                          </table>
                       </td>
                    </tr>
                 </table>
           </body>
           </body>
        </html>
        ';
        //echo $message; die;
        
        $from = ''; $fromname = '';
        $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
        
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	    $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetailArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
        
        if(count($config) > 0){
            //Initialise CI mail
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject('Welcome to '.$company_name);
            $this->email->message($message);	
            $this->email->send();
        }
        
        return 1;
    }
    public function isp_billduedays($isp_uid){
	$data = array();
	$bill_cycleQ = $this->db->query("SELECT billing_due_date, late_payment_charges, charges_unit FROM sht_billing_cycle WHERE isp_uid='".$isp_uid."'");
	if($bill_cycleQ->num_rows() > 0){
	    $rowdata = $bill_cycleQ->row();
	    $data['duedays'] = $rowdata->billing_due_date;
	    $data['late_charges'] = $rowdata->late_payment_charges;
	    $data['charges_unit'] = $rowdata->charges_unit;
	}
	return $data;
    }
    public function monthly_billing($uuid, $billid, $action='', $groupmail=''){
        //2104
	$apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
        $user_rowdata = $this->getcustomer_data($uuid);
        $state = $this->getstatename($user_rowdata->billing_state);
        $city = $this->getcityname($user_rowdata->billing_state,$user_rowdata->billing_city);
        $zone = $this->getzonename($user_rowdata->billing_zone);
        $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
	$gstin_number = $user_rowdata->gstin_number;
	$hsn_sac = '9984';
	$email = $user_rowdata->email;
        $isp_uid = $user_rowdata->isp_uid;
	$taxtype = $user_rowdata->taxtype;
	$mobileno = $user_rowdata->billing_mobileno;
	$tax = $this->current_taxapplicable($isp_uid);
	if($taxtype == '1'){
	    $apply_cgst_sgst_tax = 1;
	}else{
	    $apply_igst_tax = 1;
	}

        $next_bill_date = date('d-m-Y', strtotime($user_rowdata->next_bill_date));
        $billingdata = $this->user_billing_listing($uuid, $billid);
        $bill_number = $billingdata->bill_number;
        $payment_mode = $billingdata->payment_mode;
        $actual_amount = $billingdata->actual_amount;
        $discount = $billingdata->discount;
	$discounttype = $billingdata->discounttype;
	$is_advanced_paid = $billingdata->is_advanced_paid;
        
        $total_amount = $billingdata->total_amount;
	$service_tax = $billingdata->service_tax;
	$swachh_bharat_tax = $billingdata->swachh_bharat_tax;
	$krishi_kalyan_tax = $billingdata->krishi_kalyan_tax;
	$cgst_tax = $billingdata->cgst_tax;
	$sgst_tax = $billingdata->sgst_tax;
	$igst_tax = $billingdata->igst_tax;
        $amount_inwords = $this->getIndianCurrency($total_amount);
	$bill_comments = $billingdata->bill_comments;
	if($bill_comments != ''){
	    $bill_comments = ' '.$bill_comments;
	}
	$bill_remarks = $billingdata->bill_remarks;
	if($bill_remarks != ''){
	    $bill_remarks = 'Remarks: '.$bill_remarks;
	}
	
	$currsymbol = $this->country_currency($isp_uid);
	
	$billremark_commnt = '';
	if($action == 'sendbill'){
	    $billremark_commnt = '<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.'</td>';
	}else{
	    $billremark_commnt = '<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>';
	}
	
	$user_plan_type = $billingdata->user_plan_type;
	$bill_date = date('d-m-Y', strtotime($billingdata->bill_added_on));
	$bill_starts_from = $billingdata->bill_starts_from;
	$billduedaysArr = $this->isp_billduedays($isp_uid);
	$duedays = $billduedaysArr['duedays'];
	$late_charges = $billduedaysArr['late_charges'];
	$charges_unit = $billduedaysArr['charges_unit'];
	$latebillamount = ''; $bill_duedate = '';
	if($duedays != '0'){
	    $bill_duedate = date('d-m-Y', strtotime($bill_date ." +". $duedays ." days"));
	}
        
	if($late_charges != '0'){
	    if($charges_unit == 'percent'){
		$latebillamount = ($total_amount + round(($total_amount * $late_charges)/100));
	    }elseif($charges_unit == 'net'){
		$latebillamount = ($total_amount + $late_charges);
	    }
	}
	
        /*$lastplan_activatedate = $this->lastplan_activatedate($billid);  
        if($bill_starts_from == '0000-00-00'){
            if($lastplan_activatedate == ''){
                $plan_activation_date = date('d-m-Y', strtotime($user_rowdata->plan_activated_date));
            }else{
                $plan_activation_date = date('d-m-Y', strtotime($lastplan_activatedate));
            }
        }else{
            $plan_activation_date = date('d-m-Y', strtotime($billingdata->bill_starts_from));
        }
        
        if($user_plan_type == 'prepaid'){
            $bill_tilldate = date('d-m-Y', strtotime($plan_activation_date." +$plan_tilldays day"));
        }else{
            $bill_tilldate = $bill_date;
        }*/
	
	$plandetails = $this->getplan_details($billingdata->plan_id, $uuid);
        $plan_duration = $plandetails['plan_duration'];
        $plan_tilldays = $plandetails['plan_tilldays'];
	$plan_price = $plandetails['price'];
	$plan_grossamt = $plandetails['actual_price'];
	
	if($is_advanced_paid == 0){
	    $addichrgesrow = ''; $add_addicharges = 0;
	    $addichrgQ = $this->db->query("SELECT * FROM sht_billing_additional_charges WHERE bill_id='".$billid."'");
	    if($addichrgQ->num_rows() > 0){
		foreach($addichrgQ->result() as $addchrgobj){
		    $charges_details = ucwords($addchrgobj->charges_details);
		    $addiamount = $addchrgobj->amount;
		    $add_addicharges += $addiamount;
		    $addichrgesrow .= '
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">'.$charges_details.'</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($addiamount,2).'</span>
			   </p>
			</td>
		    </tr>';
		}
	    }
	    
	    if($service_tax != '0.00'){
		$actual_billamt = $billingdata->actual_amount;
		$tax_msg .= '
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Service Tax</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($service_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Krishi Kalyan Tax</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($krishi_kalyan_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="4" style="text-align: right">
			</td>
		     </tr>
		     <tr>
			'.$billremark_commnt.'
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			      NET AMOUNT (Including Tax)
			      </span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			   </p>
			</td>
		     </tr>
		';
		
	       
	    }
	    else{
		if($apply_cgst_sgst_tax == 1){
		    /*Example:
		     *	
		     *	Total Bill Amount = 30
		     *	Plan Price = 37
		     *	Tax = 18%
		     *
		     *	30.34 - 6.66 = 37
		     *     - 7.00 (Discount)
		     *
		     *    	23.34 + 6.66 = 30
		     *	
		     */
		    
		    /*$tax = (100 + $tax);
		    $actual_billamt1 = round((($total_amount - $add_addicharges) * 100) /  $tax);
		    if(($discounttype == 'percent') && ($discount != 0)){
			$discount_amt = round(($actual_billamt1 * $billingdata->discount)/100);
		    }elseif(($discounttype == 'flat') && ($discount != 0)){
			$discount_amt = $billingdata->discount;
		    }else{
			$discount_amt = 0;
		    }
		    $actual_billamt = $actual_billamt1 + $discount_amt; 
		    $after_disc_billamt = ($actual_billamt - $discount_amt);
		    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
			$taxamt = ($total_amount - $actual_billamt1 - $add_addicharges);
			$cgst_tax = round($taxamt/2);
			$sgst_tax = round($taxamt/2);
		    }*/
		    
		    /*$tax = (100 + $tax);
		    $actual_billamt = round(($actual_amount * 100) /  $tax);
		    if(($discounttype == 'percent') && ($discount != 0)){
			$discount_amt = round(($actual_billamt * $billingdata->discount)/100);
		    }elseif(($discounttype == 'flat') && ($discount != 0)){
			$discount_amt = $billingdata->discount;
		    }else{
			$discount_amt = 0;
		    }
		    //$actual_billamt = $actual_billamt1 + $discount_amt; 
		    $after_disc_billamt = ($actual_billamt - $discount_amt);
		    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
			$taxamt = (($after_disc_billamt * ($tax - 100)) / 100);
			$cgst_tax = round($taxamt/2);
			$sgst_tax = round($taxamt/2);
		    }*/
		    
		    $tax = (100 + $tax);
		    $actual_billamt = $billingdata->actual_amount;
		    if(($discounttype == 'percent') && ($discount != 0)){
			$discount_amt = round(($actual_billamt * $billingdata->discount)/100);
		    }elseif(($discounttype == 'flat') && ($discount != 0)){
			$discount_amt = $billingdata->discount;
		    }else{
			$discount_amt = 0;
		    }
		    $after_disc_billamt = ($actual_billamt - $discount_amt);
		    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
			$taxamt = (($after_disc_billamt * ($tax - 100)) / 100);
			$cgst_tax = round($taxamt/2);
			$sgst_tax = round($taxamt/2);
		    }
		    $tax_msg .= '
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
			       </p>
			    </td>
			</tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
			       </p>
			    </td>
			 </tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">CGST @9%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($cgst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">SGST @9%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($sgst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>'.$addichrgesrow.'
			 <tr>
			    <td colspan="4" style="text-align: right">
			    </td>
			 </tr>
			 <tr>
			    '.$billremark_commnt.'
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
				  NET AMOUNT (Including Tax)
				  </span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			       </p>
			    </td>
			 </tr>
		    ';
		}
		else{
    
		    /*$tax = (100 + $tax);
		    $actual_billamt = round(($actual_amount * 100) /  $tax);
		    if(($discounttype == 'percent') && ($discount != 0)){
			$discount_amt = round(($actual_billamt * $billingdata->discount)/100);
		    }elseif(($discounttype == 'flat') && ($discount != 0)){
			$discount_amt = $billingdata->discount;
		    }else{
			$discount_amt = 0;
		    }
		    //$actual_billamt = $actual_billamt1 + $discount_amt; 
		    $after_disc_billamt = ($actual_billamt - $discount_amt);
		    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
			$taxamt = (($after_disc_billamt * ($tax - 100)) / 100);
			$igst_tax = round($taxamt);
		    }*/
		    $tax = (100 + $tax);
		    $actual_billamt = $billingdata->actual_amount;
		    if(($discounttype == 'percent') && ($discount != 0)){
			$discount_amt = round(($actual_billamt * $billingdata->discount)/100);
		    }elseif(($discounttype == 'flat') && ($discount != 0)){
			$discount_amt = $billingdata->discount;
		    }else{
			$discount_amt = 0;
		    }
		    $after_disc_billamt = ($actual_billamt - $discount_amt);
		    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
			$taxamt = (($after_disc_billamt * ($tax - 100)) / 100);
			$igst_tax = round($taxamt);
		    }
		    $tax_msg .= '
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
			       </p>
			    </td>
			 </tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
			       </p>
			    </td>
			 </tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">IGST @18%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($igst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>'.$addichrgesrow.'
			 <tr>
			    <td colspan="4" style="text-align: right">
			    </td>
			 </tr>
			 <tr>
			    '.$billremark_commnt.'
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
				  NET AMOUNT (Including Tax)
				  </span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			       </p>
			    </td>
			 </tr>
		    ';
		    
		}
	    }
	}	
	else{
	    $actual_billamt = $billingdata->total_amount;
	    if($bill_comments == ''){
		$billremark_commnt = '<td colspan="2" rowspan="3" valign="middle">Payment Received in Advanced <br/> '.$bill_remarks.'</td>';
	    }
	    $tax_msg .= '
		 <tr>
		    '.$billremark_commnt.'
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	}
	$ispdetailArr = $this->isp_details($isp_uid);
        $company_name = $ispdetailArr['company_name'];
        $address = $ispdetailArr['address1'];
        $helpline = $ispdetailArr['help_number1'];
        $support_email = $ispdetailArr['support_email'];
        $isp_name = ucwords($ispdetailArr['isp_name']);
        $ispgst_number = $ispdetailArr['gst_no'];
	$isp_service_tax_no = $ispdetailArr['service_tax_no'];
	$ispcin_number = $ispdetailArr['cin_number'];
	$isppancard = $ispdetailArr['pancard'];
	$isp_website = $ispdetailArr['website'];
        $isp_state = $this->getstatename($ispdetailArr['state']);
        $isplogo = $this->get_isplogo($isp_uid);
	$isp_signature = $this->get_ispsignaturelogo($isp_uid);
	$invoice_color = $ispdetailArr['invoice_color'];
	$invoice_banner1 = $ispdetailArr['invoice_banner1'];
	$invoice_banner2 = $ispdetailArr['invoice_banner2'];
	$invoice_statecode = $ispdetailArr['invoice_statecode'];
	
	
	$isptaxdet = '';
	$billmonth = date('m', strtotime($bill_date));
	$billyear = date('Y', strtotime($bill_date));
	if(($billmonth < 7) && ($billyear == '2017')){
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">Service Tax : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isp_service_tax_no.'</span>';
	}else{
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">GSTIN : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispgst_number.'</span>';
	}
	
        $message = '';
        
	if($action == 'sendbill'){
	$message .= '
	<!doctype html>
	    <html>
	       <head>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <title></title>
		  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=greek" rel="stylesheet">
		  <link rel="stylesheet" href="./source/invoice_emailer/css/font-awesome.min.css">
		  <style type="text/css">
		     html,  body {
		     margin: 0 !important;
		     padding: 0 !important;
		     height: 100% !important;
		     width: 100% !important;
		     }
		     * {
		     -ms-text-size-adjust: 100%;
		     -webkit-text-size-adjust: 100%;
		     }
		     .ExternalClass {
		     width: 100%;
		     }

		     div[style*="margin: 10px 0"] {
		     margin: 0 !important;
		     }

		     table,  td {
		     mso-table-lspace: 0pt !important;
		     mso-table-rspace: 0pt !important;
		     }
		     
		     table {
		     border-spacing: 0 !important;
		     border-collapse: collapse !important;
		     table-layout: fixed !important;
		     margin: 0 auto !important;
		     }
		     table table table {
		     table-layout: auto;
		     }

		     img {
		     -ms-interpolation-mode: bicubic;
		     }

		     .yshortcuts a {
		     border-bottom: none !important;
		     }

		     a[x-apple-data-detectors] {
		     color: inherit !important;
		     }
		  </style>
		  <style type="text/css">

		     .button-td,
		     .button-a {
		     transition: all 100ms ease-in;
		     }
		     .button-td:hover,
		     .button-a:hover {
		     background: #2fbdf2 !important;
		     border-color: #2fbdf2 !important;
		     }

		     .button-get-td,
		     .button-get-a {
		     transition: all 100ms ease-in;
		     }
		     .button-get-td:hover,
		     .button-get-a:hover {
		     background: #545454 !important;
		     border-color: #545454 !important;
		     }
		     /* Media Queries */
		     @media screen and (max-width: 600px) {
		     .email-container {
		     width: 100% !important;
		     }

		     .fluid,
		     .fluid-centered {
		     max-width: 100% !important;
		     height: auto !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* And center justify these ones. */
		     .fluid-centered {
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* What it does: Forces table cells into full-width rows. */
		     .stack-column,
		     .stack-column-center {
		     display: block !important;
		     width: 100% !important;
		     max-width: 100% !important;
		     direction: ltr !important;
		     }
		     /* And center justify these ones. */
		     .stack-column-center {
		     text-align: center !important;
		     }
		     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
		     .center-on-narrow {
		     text-align: center !important;
		     display: block !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     float: none !important;
		     }
		     table.center-on-narrow {
		     display: inline-block !important;
		     }
		     }
		  </style>
	       </head>
	       <body bgcolor="#ffffff"  style="margin: 0;">';
	}
	$message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 11px; padding: 0px;">Invoice Date</p>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>';
						if(($bill_duedate != '') && ($is_advanced_paid == 0)){
						$message .=    '
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 11px; padding: 0px;">Due Date</p>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_duedate.'</p>
						</td>';
						}
					     $message .=    '
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>';
					    if(($latebillamount != '')  && ($is_advanced_paid == 0)){
						$message .= '
						<td style="padding:0px" valign="top" rowspan="2">
						    <p style="margin:0px;">
						       <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">AMOUNT AFTER DUE DATE</span> 
						    </p>
						    <p style="margin:0px; padding-bottom: 5px;">
							<span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 13px; padding: 0px; margin: 0px;">'.$currsymbol.' '.number_format($latebillamount,2).'</span> 
						    </p>
						</td>
						';
					    }else{
						$message .= '
						<td style="padding:0px" valign="top" rowspan="2">
						    <p style="margin:0px;">
						       <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						    </p>
						    <p style="margin:0px; padding-bottom: 5px;">
							<span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 13px; padding: 0px; margin: 0px;">&nbsp;</span> 
						    </p>
						</td>
						';
					    }
					    $message .= '
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Monthly Plan Bill</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">Package: '.$plandetails['planname'].'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>';
	if($action == 'sendbill'){  
	    $message .='
		</body>
	    </html>';
	}
        //echo $message; die;
        
        $from = ''; $fromname = '';
        $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
        $emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	     $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetailArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
        
        if(($action == 'viewbill') || ($action == 'printbill')){
            echo $message;
        }elseif($action == 'sendbill'){      
            if(count($config) > 0){
		$pdfattach = ''; $attched_file = '';
		$message = preg_replace('/>\s+</', "><", $message);
		
		$this->dpdf = new Dpdf();
		$this->dpdf->load_html($message);
		$this->dpdf->render();
		$output = $this->dpdf->output();
		
		$pdfattach = $uuid.'_monthlybill_' . $bill_date .'.pdf';
		$pdffile = file_put_contents('assets/emailer_pdf_attach/'.$pdfattach, $output);
		$attched_file= 'assets/emailer_pdf_attach/'.$pdfattach;
		unset($pdffile);
		
                $this->email->initialize($config);
		$this->email->clear(TRUE);
                $this->email->from($from, $fromname);
                $this->email->to($email);
                $this->email->subject('Monthly Plan Cost Bill');
                $this->email->message($message);
		$this->email->attach($attched_file);
                $this->email->send();
                
		if($groupmail != ''){
		    return 1; die;
		}else{
		    echo 'Email has been send successfully.'; die;
		}
            }else{
                echo 'Oops! Please setup email gateway to send mails.';
            }
        }elseif($action == 'downloadbill'){      
            echo 'Download Option will be provided soon.';
        }else{
            if(count($config) > 0){
                $this->email->initialize($config);
                $this->email->from($from, $fromname);
                $this->email->to($email);
                $this->email->subject('Monthly Plan Cost Bill');
                $this->email->message($message);	
                $this->email->send();
            }
            return 1;
        }
        
    }
    
    

    public function topups_billing($uuid, $billid, $action=''){
	
	$apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
        $user_rowdata = $this->getcustomer_data($uuid);
        $state = $this->getstatename($user_rowdata->billing_state);
        $city = $this->getcityname($user_rowdata->billing_state,$user_rowdata->billing_city);
        $zone = $this->getzonename($user_rowdata->billing_zone);
        $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
	$gstin_number = $user_rowdata->gstin_number;
	$mobileno = $user_rowdata->billing_mobileno;
	$email = $user_rowdata->email;
        $isp_uid = $user_rowdata->isp_uid;
	$taxtype = $user_rowdata->taxtype;
	$tax = $this->current_taxapplicable($isp_uid);
	if($taxtype == '1'){
	    $apply_cgst_sgst_tax = 1;
	}else{
	    $apply_igst_tax = 1;
	}

        $next_bill_date = date('d-m-Y', strtotime($user_rowdata->next_bill_date));
        $billingdata = $this->user_billing_listing($uuid, $billid);
        $bill_number = $billingdata->bill_number;
        $payment_mode = $billingdata->payment_mode;
        $amount = $billingdata->actual_amount;
        $discount = $billingdata->discount;
	$discounttype = $billingdata->discounttype;
	
        $total_amount = $billingdata->total_amount;
	$service_tax = $billingdata->service_tax;
	$swachh_bharat_tax = $billingdata->swachh_bharat_tax;
	$krishi_kalyan_tax = $billingdata->krishi_kalyan_tax;
	$cgst_tax = $billingdata->cgst_tax;
	$sgst_tax = $billingdata->sgst_tax;
	$igst_tax = $billingdata->igst_tax;
        $amount_inwords = $this->getIndianCurrency($total_amount);
	
	$hsn_sac = (($billingdata->hsn_number) != '') ? $billingdata->hsn_number : '9987';
	$bill_comments = $billingdata->bill_comments;
	if($bill_comments != ''){
	    $bill_comments = ' '.$bill_comments;
	}
	$bill_remarks = $billingdata->bill_remarks;
	if($bill_remarks != ''){
	    $bill_remarks = 'Remarks: '.$bill_remarks;
	}
	$currsymbol = $this->country_currency($isp_uid);
	
	$billremark_commnt = '';
	if($action == 'sendbill'){
	    $billremark_commnt = '<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.'</td>';
	}else{
	    $billremark_commnt = '<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>';
	}
	
	$user_plan_type = $billingdata->user_plan_type;
	$bill_date = date('d-m-Y', strtotime($billingdata->bill_added_on));
	$bill_starts_from = $billingdata->bill_starts_from;
	$billduedaysArr = $this->isp_billduedays($isp_uid);
	$duedays = $billduedaysArr['duedays'];
	$late_charges = $billduedaysArr['late_charges'];
	$charges_unit = $billduedaysArr['charges_unit'];
	$latebillamount = ''; $bill_duedate = '';
	if($duedays != '0'){
	    $bill_duedate = date('d-m-Y', strtotime($bill_date ." +". $duedays ." days"));
	}
        
	if($late_charges != '0'){
	    if($charges_unit == 'percent'){
		$latebillamount = ($total_amount + round(($total_amount * $late_charges)/100));
	    }elseif($charges_unit == 'net'){
		$latebillamount = ($total_amount + $late_charges);
	    }
	}
        
	
        /*$lastplan_activatedate = $this->lastplan_activatedate($billid);  
        if($bill_starts_from == '0000-00-00'){
            if($lastplan_activatedate == ''){
                $plan_activation_date = date('d-m-Y', strtotime($user_rowdata->plan_activated_date));
            }else{
                $plan_activation_date = date('d-m-Y', strtotime($lastplan_activatedate));
            }
        }else{
            $plan_activation_date = date('d-m-Y', strtotime($billingdata->bill_starts_from));
        }
        
        if($user_plan_type == 'prepaid'){
            $bill_tilldate = date('d-m-Y', strtotime($plan_activation_date." +$plan_tilldays day"));
        }else{
            $bill_tilldate = $bill_date;
        }*/
	
	$plandetails = $this->gettopup_details($billingdata->plan_id);
        $topuptype = $plandetails['topuptype'];
        if($topuptype == '1'){
            $topuptype = 'Data Topup';
        }elseif($topuptype == '2'){
            $topuptype = 'Data Unaccountancy Topup';
        }elseif($topuptype == '3'){
            $topuptype = 'Speed Topup';
        }
        $topupname = $plandetails['topupname'];
	$topupprice = $plandetails['topupprice'];
        
	$topupdetails = '';
	if($topuptype != 'Data Topup'){
	    $appliedtopup_details = $this->appliedtopup_details($billingdata->plan_id);
	    $topup_startdate = date('d-m-Y', strtotime($appliedtopup_details['topup_startdate']));
	    $topup_enddate = date('d-m-Y', strtotime($appliedtopup_details['topup_enddate']));
	    $applicable_days = $appliedtopup_details['applicable_days'];
	    $total_topupamount = ($topupprice * $applicable_days);
	    $topupdetails = $topup_startdate.' to '.$topup_enddate.' ('.$applicable_days.' days)';
	}
	$plan_grossamt = $topupprice;
	
	$addichrgesrow = ''; $add_addicharges = 0;
	$addichrgQ = $this->db->query("SELECT * FROM sht_billing_additional_charges WHERE bill_id='".$billid."'");
	if($addichrgQ->num_rows() > 0){
	    foreach($addichrgQ->result() as $addchrgobj){
		$charges_details = ucwords($addchrgobj->charges_details);
		$addiamount = $addchrgobj->amount;
		$add_addicharges += $addiamount;
		$addichrgesrow .= '
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">'.$charges_details.'</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($addiamount,2).'</span>
		       </p>
		    </td>
		</tr>';
	    }
	}
	
	if($service_tax != '0.00'){
	    $actual_billamt = $billingdata->actual_amount;
	    $tax_msg .= '
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Service Tax</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($service_tax,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Krishi Kalyan Tax</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($krishi_kalyan_tax,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="4" style="text-align: right">
		    </td>
		 </tr>
		 <tr>
		    '.$billremark_commnt.'
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT (Including Tax)
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	    
	   
	}
	else{
	    if($apply_cgst_sgst_tax == 1){
		/*Example:
		 *	
		 *	Total Bill Amount = 30
		 *	Plan Price = 37
		 *	Tax = 18%
		 *
		 *	30.34 - 6.66 = 37
		 *     - 7.00 (Discount)
		 *
		 *    	23.34 + 6.66 = 30
		 *	
		 */
		$tax = (100 + $tax);
		$actual_billamt1 = round((($total_amount - $add_addicharges) * 100) /  $tax);
		if(($discounttype == 'percent') && ($discount != 0)){
		    $discount_amt = round(($actual_billamt1 * $billingdata->discount)/100);
		}elseif(($discounttype == 'flat') && ($discount != 0)){
		    $discount_amt = $billingdata->discount;
		}else{
		    $discount_amt = 0;
		}
		$actual_billamt = $actual_billamt1 + $discount_amt; 
		$after_disc_billamt = ($actual_billamt - $discount_amt);
		if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
		    $taxamt = ($total_amount - $actual_billamt1 - $add_addicharges);
		    $cgst_tax = round($taxamt/2);
		    $sgst_tax = round($taxamt/2);
		}
		$tax_msg .= '
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
			   </p>
			</td>
		    </tr>
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
			   </p>
			</td>
		     </tr>
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">CGST @9%</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($cgst_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">SGST @9%</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($sgst_tax,2).'</span>
			   </p>
			</td>
		     </tr>'.$addichrgesrow.'
		     <tr>
			<td colspan="4" style="text-align: right">
			</td>
		     </tr>
		     <tr>
			'.$billremark_commnt.'
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			      NET AMOUNT (Including Tax)
			      </span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			   </p>
			</td>
		     </tr>
		';
	    }
	    else{

		$tax = (100 + $tax);
		$actual_billamt1 = round((($total_amount - $add_addicharges) * 100) /  $tax);
		if(($discounttype == 'percent') && ($discount != 0)){
		    $discount_amt = round(($actual_billamt1 * $billingdata->discount)/100);
		}elseif(($discounttype == 'flat') && ($discount != 0)){
		    $discount_amt = $billingdata->discount;
		}else{
		    $discount_amt = 0;
		}
		$actual_billamt = $actual_billamt1 + $discount_amt; 
		$after_disc_billamt = ($actual_billamt - $discount_amt);
		if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
		    $taxamt = ($total_amount - $actual_billamt1 - $add_addicharges);
		    $igst_tax = round($taxamt);
		}
		$tax_msg .= '
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
			   </p>
			</td>
		     </tr>
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
			   </p>
			</td>
		     </tr>
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">IGST @18%</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($igst_tax,2).'</span>
			   </p>
			</td>
		     </tr>'.$addichrgesrow.'
		     <tr>
			<td colspan="4" style="text-align: right">
			</td>
		     </tr>
		     <tr>
			'.$billremark_commnt.'
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			      NET AMOUNT (Including Tax)
			      </span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			   </p>
			</td>
		     </tr>
		';
		
	    }

	}
        
        $ispdetailArr = $this->isp_details($isp_uid);
        $company_name = $ispdetailArr['company_name'];
        $address = $ispdetailArr['address1'];
        $helpline = $ispdetailArr['help_number1'];
        $support_email = $ispdetailArr['support_email'];
        $isp_name = ucwords($ispdetailArr['isp_name']);
        $ispgst_number = $ispdetailArr['gst_no'];
	$isp_service_tax_no = $ispdetailArr['service_tax_no'];
	$ispcin_number = $ispdetailArr['cin_number'];
	$isppancard = $ispdetailArr['pancard'];
	$isp_website = $ispdetailArr['website'];
        $isp_state = $this->getstatename($ispdetailArr['state']);
        $isplogo = $this->get_isplogo($isp_uid);
	$isp_signature = $this->get_ispsignaturelogo($isp_uid);
	
	$invoice_color = $ispdetailArr['invoice_color'];
	$invoice_banner1 = $ispdetailArr['invoice_banner1'];
	$invoice_banner2 = $ispdetailArr['invoice_banner2'];
	$invoice_statecode = $ispdetailArr['invoice_statecode'];
	
	
	$isptaxdet = '';
	$billmonth = date('m', strtotime($bill_date));
	$billyear = date('Y', strtotime($bill_date));
	if(($billmonth < 7) && ($billyear == '2017')){
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">Service Tax : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isp_service_tax_no.'</span>';
	}else{
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">GSTIN : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispgst_number.'</span>';
	}
	
        $message = '';
        
	if($action == 'sendbill'){
	$message .= '
	<!doctype html>
	    <html>
	       <head>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <title></title>
		  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=greek" rel="stylesheet">
		  <link rel="stylesheet" href="./source/invoice_emailer/css/font-awesome.min.css">
		  <style type="text/css">
		     html,  body {
		     margin: 0 !important;
		     padding: 0 !important;
		     height: 100% !important;
		     width: 100% !important;
		     }
		     * {
		     -ms-text-size-adjust: 100%;
		     -webkit-text-size-adjust: 100%;
		     }
		     .ExternalClass {
		     width: 100%;
		     }

		     div[style*="margin: 10px 0"] {
		     margin: 0 !important;
		     }

		     table,  td {
		     mso-table-lspace: 0pt !important;
		     mso-table-rspace: 0pt !important;
		     }
		     
		     table {
		     border-spacing: 0 !important;
		     border-collapse: collapse !important;
		     table-layout: fixed !important;
		     margin: 0 auto !important;
		     }
		     table table table {
		     table-layout: auto;
		     }

		     img {
		     -ms-interpolation-mode: bicubic;
		     }

		     .yshortcuts a {
		     border-bottom: none !important;
		     }

		     a[x-apple-data-detectors] {
		     color: inherit !important;
		     }
		  </style>
		  <style type="text/css">

		     .button-td,
		     .button-a {
		     transition: all 100ms ease-in;
		     }
		     .button-td:hover,
		     .button-a:hover {
		     background: #2fbdf2 !important;
		     border-color: #2fbdf2 !important;
		     }

		     .button-get-td,
		     .button-get-a {
		     transition: all 100ms ease-in;
		     }
		     .button-get-td:hover,
		     .button-get-a:hover {
		     background: #545454 !important;
		     border-color: #545454 !important;
		     }
		     /* Media Queries */
		     @media screen and (max-width: 600px) {
		     .email-container {
		     width: 100% !important;
		     }

		     .fluid,
		     .fluid-centered {
		     max-width: 100% !important;
		     height: auto !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* And center justify these ones. */
		     .fluid-centered {
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* What it does: Forces table cells into full-width rows. */
		     .stack-column,
		     .stack-column-center {
		     display: block !important;
		     width: 100% !important;
		     max-width: 100% !important;
		     direction: ltr !important;
		     }
		     /* And center justify these ones. */
		     .stack-column-center {
		     text-align: center !important;
		     }
		     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
		     .center-on-narrow {
		     text-align: center !important;
		     display: block !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     float: none !important;
		     }
		     table.center-on-narrow {
		     display: inline-block !important;
		     }
		     }
		  </style>
	       </head>
	       <body bgcolor="#ffffff"  style="margin: 0;">';
	}
	$message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					    <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 11px; padding: 0px;">Invoice Date</p>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>';
						if($bill_duedate != ''){
						$message .=    '
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 11px; padding: 0px;">Due Date</p>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_duedate.'</p>
						</td>';
						}
					     $message .=    '
					    </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>';
					    if($latebillamount != ''){
						$message .= '
						<td style="padding:0px" valign="top" rowspan="2">
						    <p style="margin:0px;">
						       <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">AMOUNT AFTER DUE DATE</span> 
						    </p>
						    <p style="margin:0px; padding-bottom: 5px;">
							<span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 13px; padding: 0px; margin: 0px;">'.$currsymbol.' '.number_format($latebillamount,2).'</span> 
						    </p>
						</td>
						';
					    }else{
						$message .= '
						<td style="padding:0px" valign="top" rowspan="2">
						    <p style="margin:0px;">
						       <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						    </p>
						    <p style="margin:0px; padding-bottom: 5px;">
							<span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 13px; padding: 0px; margin: 0px;">&nbsp;</span> 
						    </p>
						</td>
						';
					    }
					    $message .= '
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Topups Billing</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$topuptype.': '.$topupname.' <br/> '.$topupdetails.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
	       </body>
	    </html>
	
	';
	
        
        
	$from = ''; $fromname = '';
        $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
	
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	    $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetailArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
	
        if($action == 'viewbill'){
            echo $message;
        }elseif($action == 'sendbill'){      
            if(count($config) > 0){
		$this->load->library('Dpdf');		
		$this->dpdf->load_html($message);
		$this->dpdf->render();
		$output = $this->dpdf->output();
		
		$pdfattach = $uuid.'_topupinvoice_' . $bill_date .'.pdf';
		file_put_contents('assets/emailer_pdf_attach/'.$pdfattach, $output);
		$attched_file= 'assets/emailer_pdf_attach/'.$pdfattach;
		
                $this->email->initialize($config);
                $this->email->from($from, $fromname);
                $this->email->to($email);
                $this->email->subject('Topup Invoice');
                $this->email->message($message);
		$this->email->attach($attched_file);
                $this->email->send();
                echo 'Email has been send successfully.';
            }else{
                echo 'Oops! Please setup email gateway to send mails.';
            }
        }elseif($action == 'downloadbill'){
            
            echo 'Download Option will be provided soon.';
        }else{
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject('Installation Cost Bill');
            $this->email->message($message);	
            $this->email->send();
            return 1;
        }
    }

    public function addtowallet_billing($uuid, $billid, $action=''){
        $apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
        $user_rowdata = $this->getcustomer_data($uuid);
        $state = $this->getstatename($user_rowdata->billing_state);
        $city = $this->getcityname($user_rowdata->billing_state,$user_rowdata->billing_city);
        $zone = $this->getzonename($user_rowdata->billing_zone);
        $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
	$gstin_number = $user_rowdata->gstin_number;
	$mobileno = $user_rowdata->billing_mobileno;
	$email = $user_rowdata->email;
        $isp_uid = $user_rowdata->isp_uid;
	$taxtype = $user_rowdata->taxtype;
	$tax = $this->current_taxapplicable($isp_uid);
	if($taxtype == '1'){
	    $apply_cgst_sgst_tax = 1;
	}else{
	    $apply_igst_tax = 1;
	}

        $next_bill_date = date('d-m-Y', strtotime($user_rowdata->next_bill_date));
        $billingdata = $this->user_billing_listing($uuid, $billid);
        $bill_number = $billingdata->bill_number;
        $payment_mode = $billingdata->payment_mode;
        $amount = $billingdata->actual_amount;
        $discount = $billingdata->discount;
        if($discount != 0){
            $discount_amt = ($amount * $billingdata->discount)/100;
        }else{
            $discount_amt = 0;
        }
        $total_amount = $billingdata->total_amount;
        $amount_inwords = $this->getIndianCurrency($total_amount);
        
        $bill_date = date('d M Y', strtotime($billingdata->bill_added_on));
	$hsn_sac = (($billingdata->hsn_number) != '') ? $billingdata->hsn_number : '9987';
	$bill_comments = $billingdata->bill_comments;
	if($bill_comments != ''){
	    $bill_comments = ' '.$bill_comments;
	}
	$bill_remarks = $billingdata->bill_remarks;
	if($bill_remarks != ''){
	    $bill_remarks = 'Remarks: '.$bill_remarks;
	}
	$currsymbol = $this->country_currency($isp_uid);
	
        $ispdetailArr = $this->isp_details($isp_uid);
        $company_name = $ispdetailArr['company_name'];
        $address = $ispdetailArr['address1'];
        $helpline = $ispdetailArr['help_number1'];
        $support_email = $ispdetailArr['support_email'];
        $isp_name = ucwords($ispdetailArr['isp_name']);
        $ispgst_number = $ispdetailArr['gst_no'];
	$isp_service_tax_no = $ispdetailArr['service_tax_no'];
	$ispcin_number = $ispdetailArr['cin_number'];
	$isppancard = $ispdetailArr['pancard'];
	$isp_website = $ispdetailArr['website'];
        $isp_state = $this->getstatename($ispdetailArr['state']);
        $isplogo = $this->get_isplogo($isp_uid);
	$isp_signature = $this->get_ispsignaturelogo($isp_uid);
	
	$invoice_color = $ispdetailArr['invoice_color'];
	$invoice_banner1 = $ispdetailArr['invoice_banner1'];
	$invoice_banner2 = $ispdetailArr['invoice_banner2'];
	$invoice_statecode = $ispdetailArr['invoice_statecode'];
	
	
	$isptaxdet = '';
	$billmonth = date('m', strtotime($bill_date));
	$billyear = date('Y', strtotime($bill_date));
	if(($billmonth < 7) && ($billyear == '2017')){
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">Service Tax : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isp_service_tax_no.'</span>';
	}else{
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">GSTIN : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispgst_number.'</span>';
	}
	
        $message = '';
        
	if($action == 'sendbill'){
	$message .= '
	<!doctype html>
	    <html>
	       <head>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <title></title>
		  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=greek" rel="stylesheet">
		  <link rel="stylesheet" href="./source/invoice_emailer/css/font-awesome.min.css">
		  <style type="text/css">
		     html,  body {
		     margin: 0 !important;
		     padding: 0 !important;
		     height: 100% !important;
		     width: 100% !important;
		     }
		     * {
		     -ms-text-size-adjust: 100%;
		     -webkit-text-size-adjust: 100%;
		     }
		     .ExternalClass {
		     width: 100%;
		     }

		     div[style*="margin: 10px 0"] {
		     margin: 0 !important;
		     }

		     table,  td {
		     mso-table-lspace: 0pt !important;
		     mso-table-rspace: 0pt !important;
		     }
		     
		     table {
		     border-spacing: 0 !important;
		     border-collapse: collapse !important;
		     table-layout: fixed !important;
		     margin: 0 auto !important;
		     }
		     table table table {
		     table-layout: auto;
		     }

		     img {
		     -ms-interpolation-mode: bicubic;
		     }

		     .yshortcuts a {
		     border-bottom: none !important;
		     }

		     a[x-apple-data-detectors] {
		     color: inherit !important;
		     }
		  </style>
		  <style type="text/css">

		     .button-td,
		     .button-a {
		     transition: all 100ms ease-in;
		     }
		     .button-td:hover,
		     .button-a:hover {
		     background: #2fbdf2 !important;
		     border-color: #2fbdf2 !important;
		     }

		     .button-get-td,
		     .button-get-a {
		     transition: all 100ms ease-in;
		     }
		     .button-get-td:hover,
		     .button-get-a:hover {
		     background: #545454 !important;
		     border-color: #545454 !important;
		     }
		     /* Media Queries */
		     @media screen and (max-width: 600px) {
		     .email-container {
		     width: 100% !important;
		     }

		     .fluid,
		     .fluid-centered {
		     max-width: 100% !important;
		     height: auto !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* And center justify these ones. */
		     .fluid-centered {
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* What it does: Forces table cells into full-width rows. */
		     .stack-column,
		     .stack-column-center {
		     display: block !important;
		     width: 100% !important;
		     max-width: 100% !important;
		     direction: ltr !important;
		     }
		     /* And center justify these ones. */
		     .stack-column-center {
		     text-align: center !important;
		     }
		     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
		     .center-on-narrow {
		     text-align: center !important;
		     display: block !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     float: none !important;
		     }
		     table.center-on-narrow {
		     display: inline-block !important;
		     }
		     }
		  </style>
	       </head>
	       <body bgcolor="#ffffff"  style="margin: 0;">';
	}
	$message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">RECEIPT INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Wallet Receipt</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">Wallet Amount Added</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$total_amount.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($total_amount, 2).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
						<td colspan="2" style="text-align: right">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
						      NET AMOUNT
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2" style="text-align: right">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2" style="text-align: right">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"><img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
	       </body>
	    </html>
	
	';
	
        
        
	$from = ''; $fromname = '';
        $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
	
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	    $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetailArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
	
        if($action == 'viewbill'){
            echo $message;
        }elseif($action == 'sendbill'){      
            if(count($config) > 0){
                $this->email->initialize($config);
                $this->email->from($from, $fromname);
                $this->email->to($email);
                $this->email->subject('Installation Cost Bill');
                $this->email->message($message);	
                $this->email->send();
                echo 'Email has been send successfully.';
            }else{
                echo 'Oops! Please setup email gateway to send mails.';
            }
        }elseif($action == 'downloadbill'){
            
            echo 'Download Option will be provided soon.';
        }else{
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject('Installation Cost Bill');
            $this->email->message($message);	
            $this->email->send();
            return 1;
        }
    }
    
    public function custominvoice_billing($uuid, $billid, $action=''){
	$apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
        $user_rowdata = $this->getcustomer_data($uuid);
        $state = $this->getstatename($user_rowdata->billing_state);
        $city = $this->getcityname($user_rowdata->billing_state,$user_rowdata->billing_city);
        $zone = $this->getzonename($user_rowdata->billing_zone);
        $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
	$gstin_number = $user_rowdata->gstin_number;
	$mobileno = $user_rowdata->billing_mobileno;
	$email = $user_rowdata->email;
        $isp_uid = $user_rowdata->isp_uid;
	$taxtype = $user_rowdata->taxtype;
	$tax = $this->current_taxapplicable($isp_uid);
	if($taxtype == '1'){
	    $apply_cgst_sgst_tax = 1;
	}else{
	    $apply_igst_tax = 1;
	}
	
	$billingdata = $this->custom_billing_listing($billid);
	$bill_companyid = $billingdata->bill_companyid;
        $bill_number = $billingdata->bill_number;
        $payment_mode = $billingdata->payment_mode;
        $total_amount = $billingdata->total_amount;
        $discount = $billingdata->discount;
	$bill_date = date('d-m-Y', strtotime($billingdata->added_on));
        if($discount != 0){
            $discount_amt = ($total_amount * $billingdata->discount)/100;
        }else{
            $discount_amt = 0;
        }
        $amount_inwords = $this->getIndianCurrency($total_amount);
        
        $bill_date = date('d M Y', strtotime($billingdata->added_on));
	$hsn_sac =  '9984';
	
	/*$bill_comments = $billingdata->bill_comments;
	if($bill_comments != ''){
	    $bill_comments = ' '.$bill_comments;
	}*/
	$bill_remarks = $billingdata->bill_remarks;
	if($bill_remarks != ''){
	    $bill_remarks = 'NOTE: '.$bill_remarks;
	}
	$currsymbol = $this->country_currency($isp_uid);
	
	//<td colspan="2" rowspan="5" valign="top">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
	$companyQ = $this->db->query("SELECT id,company_name FROM sht_isp_company_details WHERE id='".$bill_companyid."'");
	$comp_numrows = $companyQ->num_rows();
	if($comp_numrows > 0){
	    $company_name = $companyQ->row()->company_name;
	}
	
	if($apply_cgst_sgst_tax == 1){
	    $tax = (100 + $tax);
	    $actual_billamt = round(($total_amount * 100) /  $tax);
	    $taxamt = ($total_amount - $actual_billamt);
	    $cgst_tax = round($taxamt/2);
	    $sgst_tax = round($taxamt/2);
	    
	    $tax_msg .= '
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">CGST @9%</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($cgst_tax,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">SGST @9%</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($sgst_tax,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="4" style="text-align: right">
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" rowspan="3" valign="middle">'.$bill_remarks.'</td>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT (Including Tax)
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	}else{
	    $tax = (100 + $tax);
	    $actual_billamt = round(($total_amount * 100) /  $tax);
	    $igst_tax = ($total_amount - $actual_billamt);
	    
	    $tax_msg .= '
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">IGST @18%</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($igst_tax,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="4" style="text-align: right">
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT (Including Tax)
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	    
	}
	
        $ispdetailArr = $this->isp_details($isp_uid);
        //$company_name = $ispdetailArr['company_name'];
        $address = $ispdetailArr['address1'];
        $helpline = $ispdetailArr['help_number1'];
        $support_email = $ispdetailArr['support_email'];
        $isp_name = ucwords($ispdetailArr['isp_name']);
        $ispgst_number = $ispdetailArr['gst_no'];
	$isp_service_tax_no = $ispdetailArr['service_tax_no'];
	$ispcin_number = $ispdetailArr['cin_number'];
	$isppancard = $ispdetailArr['pancard'];
	$isp_website = $ispdetailArr['website'];
        $isp_state = $this->getstatename($ispdetailArr['state']);
        $isplogo = $this->get_isplogo($isp_uid);
	$isp_signature = $this->get_ispsignaturelogo($isp_uid);
	
	$invoice_color = $ispdetailArr['invoice_color'];
	$invoice_banner1 = $ispdetailArr['invoice_banner1'];
	$invoice_banner2 = $ispdetailArr['invoice_banner2'];
	$invoice_statecode = $ispdetailArr['invoice_statecode'];
	
	
	$isptaxdet = '';
	$billmonth = date('m', strtotime($bill_date));
	$billyear = date('Y', strtotime($bill_date));
	if(($billmonth < 7) && ($billyear == '2017')){
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">Service Tax : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isp_service_tax_no.'</span>';
	}else{
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">GSTIN : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispgst_number.'</span>';
	}
	
	$fetch_custombill_invoice = $this->fetch_custombill_invoice($billid);
        $message = '';
        
	if($action == 'sendbill'){
	$message .= '
	<!doctype html>
	    <html>
	       <head>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <title></title>
		  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=greek" rel="stylesheet">
		  <link rel="stylesheet" href="./source/invoice_emailer/css/font-awesome.min.css">
		  <style type="text/css">
		     html,  body {
		     margin: 0 !important;
		     padding: 0 !important;
		     height: 100% !important;
		     width: 100% !important;
		     }
		     * {
		     -ms-text-size-adjust: 100%;
		     -webkit-text-size-adjust: 100%;
		     }
		     .ExternalClass {
		     width: 100%;
		     }

		     div[style*="margin: 10px 0"] {
		     margin: 0 !important;
		     }

		     table,  td {
		     mso-table-lspace: 0pt !important;
		     mso-table-rspace: 0pt !important;
		     }
		     
		     table {
		     border-spacing: 0 !important;
		     border-collapse: collapse !important;
		     table-layout: fixed !important;
		     margin: 0 auto !important;
		     }
		     table table table {
		     table-layout: auto;
		     }

		     img {
		     -ms-interpolation-mode: bicubic;
		     }

		     .yshortcuts a {
		     border-bottom: none !important;
		     }

		     a[x-apple-data-detectors] {
		     color: inherit !important;
		     }
		  </style>
		  <style type="text/css">

		     .button-td,
		     .button-a {
		     transition: all 100ms ease-in;
		     }
		     .button-td:hover,
		     .button-a:hover {
		     background: #2fbdf2 !important;
		     border-color: #2fbdf2 !important;
		     }

		     .button-get-td,
		     .button-get-a {
		     transition: all 100ms ease-in;
		     }
		     .button-get-td:hover,
		     .button-get-a:hover {
		     background: #545454 !important;
		     border-color: #545454 !important;
		     }
		     /* Media Queries */
		     @media screen and (max-width: 600px) {
		     .email-container {
		     width: 100% !important;
		     }

		     .fluid,
		     .fluid-centered {
		     max-width: 100% !important;
		     height: auto !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* And center justify these ones. */
		     .fluid-centered {
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* What it does: Forces table cells into full-width rows. */
		     .stack-column,
		     .stack-column-center {
		     display: block !important;
		     width: 100% !important;
		     max-width: 100% !important;
		     direction: ltr !important;
		     }
		     /* And center justify these ones. */
		     .stack-column-center {
		     text-align: center !important;
		     }
		     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
		     .center-on-narrow {
		     text-align: center !important;
		     display: block !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     float: none !important;
		     }
		     table.center-on-narrow {
		     display: inline-block !important;
		     }
		     }
		  </style>
	       </head>
	       <body bgcolor="#ffffff"  style="margin: 0;">';
	}
	$message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Custom Charges</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Qty</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>';
					     
					     foreach($fetch_custombill_invoice as $custbillobj){
						$message .=  '
						 <tr style="border-bottom: 1px solid #f1f2f2">
						    <td>
							<p style="margin:5px 0px">
							   <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$custbillobj->topic_name.'</span>
							</p>
						    </td>
						    <td>
							<p style="margin:5px 0px; text-align:center;">
							   <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$custbillobj->price.'</span>
							</p>
						    </td>
						    <td>
							<p style="margin:5px 0px; text-align:center;">
							   <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$custbillobj->quantity.'</span>
							</p>
						    </td>
						    <td>
							<p style="margin:5px 0px; text-align:right;">
							   <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol .' '.number_format($custbillobj->total_price,2).'</span>
							</p>
						    </td>
						 </tr>';
					     }
					     
					     $message .=  '
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"><img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
	       </body>
	    </html>
	
	';
        
	$from = ''; $fromname = '';
        $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	     $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetailArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
	
        if($action == 'viewbill'){
            echo $message;
        }elseif($action == 'sendbill'){      
            if(count($config) > 0){
                $this->email->initialize($config);
                $this->email->from($from, $fromname);
                $this->email->to($email);
                $this->email->subject('Custom Invoice');
                $this->email->message($message);	
                $this->email->send();
                echo 'Email has been send successfully.';
            }else{
                echo 'Oops! Please setup email gateway to send mails.';
            }
        }elseif($action == 'downloadbill'){
            
            echo 'Download Option will be provided soon.';
        }else{
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject('Custom Invoice');
            $this->email->message($message);	
            $this->email->send();
            return 1;
        }
    }
    
    public function advmonthly_billing($uuid, $billid, $action=''){
        
	$apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
        $user_rowdata = $this->getcustomer_data($uuid);
        $state = $this->getstatename($user_rowdata->billing_state);
        $city = $this->getcityname($user_rowdata->billing_state,$user_rowdata->billing_city);
        $zone = $this->getzonename($user_rowdata->billing_zone);
        $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
	$gstin_number = $user_rowdata->gstin_number;
	$mobileno = $user_rowdata->billing_mobileno;
	$email = $user_rowdata->email;
        $isp_uid = $user_rowdata->isp_uid;
	$taxtype = $user_rowdata->taxtype;
	$tax = $this->current_taxapplicable($isp_uid);
	if($taxtype == '1'){
	    $apply_cgst_sgst_tax = 1;
	}else{
	    $apply_igst_tax = 1;
	}

        $next_bill_date = date('d-m-Y', strtotime($user_rowdata->next_bill_date));
        $billingdata = $this->user_billing_listing($uuid, $billid);
        $bill_number = $billingdata->bill_number;
        $payment_mode = $billingdata->payment_mode;
        $amount = $billingdata->actual_amount;
        $discount = $billingdata->discount;
        if($discount != 0){
            $discount_amt = ($amount * $billingdata->discount)/100;
        }else{
            $discount_amt = 0;
        }
        $total_amount = $billingdata->total_amount;
	$service_tax = $billingdata->service_tax;
	$swachh_bharat_tax = $billingdata->swachh_bharat_tax;
	$krishi_kalyan_tax = $billingdata->krishi_kalyan_tax;
	$cgst_tax = $billingdata->cgst_tax;
	$sgst_tax = $billingdata->sgst_tax;
	$igst_tax = $billingdata->igst_tax;
        $amount_inwords = $this->getIndianCurrency($total_amount);
        
        $bill_date = date('d M Y', strtotime($billingdata->bill_added_on));
	$hsn_sac = (($billingdata->hsn_number) != '') ? $billingdata->hsn_number : '9984';
	$bill_comments = $billingdata->bill_comments;
	if($bill_comments != ''){
	    $bill_comments = ' '.$bill_comments;
	}
	$bill_remarks = $billingdata->bill_remarks;
	if($bill_remarks != ''){
	    $bill_remarks = 'Remarks: '.$bill_remarks;
	}
	$currsymbol = $this->country_currency($isp_uid);
	
	$advpayfor = 0; $freemonths = 0; $planname = '';
	$advmonthpayQ = $this->db->query("SELECT srvid, advancepay_for_months, number_of_free_months FROM sht_advance_payments WHERE bill_id='".$billid."'");
	if($advmonthpayQ->num_rows() > 0){
	    $advrowdata = $advmonthpayQ->row();
	    $advpayfor = $advrowdata->advancepay_for_months;
	    $freemonths = $advrowdata->number_of_free_months;
	    $srvid = $advrowdata->srvid;
	    $planname = $this->planname($srvid);
	}
	
	//<td colspan="2" rowspan="5" valign="top">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
	if($service_tax != '0.00'){
	    $actual_billamt = $billingdata->actual_amount;
	    $tax_msg .= '
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Service Tax</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($service_tax,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Krishi Kalyan Tax</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($krishi_kalyan_tax,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="4" style="text-align: right">
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT (Including Tax)
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	    
	   
	}
	else{
	    if($apply_cgst_sgst_tax == 1){
		$tax = (100 + $tax);
		$actual_billamt = round(($total_amount * 100) /  $tax);
		if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
		    $taxamt = ($total_amount - $actual_billamt);
		    $cgst_tax = round($taxamt/2);
		    $sgst_tax = round($taxamt/2);
		}
		
		$tax_msg .= '
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">CGST @9%</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($cgst_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">SGST @9%</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($sgst_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="4" style="text-align: right">
			</td>
		     </tr>
		     <tr>
			<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			      NET AMOUNT (Including Tax)
			      </span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			   </p>
			</td>
		     </tr>
		';
	    }else{
		$tax = (100 + $tax);
		$actual_billamt = round(($total_amount * 100) /  $tax);
		if($igst_tax == '0.00'){
		    $igst_tax = ($total_amount - $actual_billamt);
		}
		
		$tax_msg .= '
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">IGST @18%</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($igst_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="4" style="text-align: right">
			</td>
		     </tr>
		     <tr>
			<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			      NET AMOUNT (Including Tax)
			      </span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			   </p>
			</td>
		     </tr>
		';
		
	    }

	}
	
        $ispdetailArr = $this->isp_details($isp_uid);
        $company_name = $ispdetailArr['company_name'];
        $address = $ispdetailArr['address1'];
        $helpline = $ispdetailArr['help_number1'];
        $support_email = $ispdetailArr['support_email'];
        $isp_name = ucwords($ispdetailArr['isp_name']);
        $ispgst_number = $ispdetailArr['gst_no'];
	$isp_service_tax_no = $ispdetailArr['service_tax_no'];
	$ispcin_number = $ispdetailArr['cin_number'];
	$isppancard = $ispdetailArr['pancard'];
	$isp_website = $ispdetailArr['website'];
        $isp_state = $this->getstatename($ispdetailArr['state']);
        $isplogo = $this->get_isplogo($isp_uid);
	$isp_signature = $this->get_ispsignaturelogo($isp_uid);
	
	$invoice_color = $ispdetailArr['invoice_color'];
	$invoice_banner1 = $ispdetailArr['invoice_banner1'];
	$invoice_banner2 = $ispdetailArr['invoice_banner2'];
	$invoice_statecode = $ispdetailArr['invoice_statecode'];
	
	
	$isptaxdet = '';
	$billmonth = date('m', strtotime($bill_date));
	$billyear = date('Y', strtotime($bill_date));
	if(($billmonth < 7) && ($billyear == '2017')){
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">Service Tax : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isp_service_tax_no.'</span>';
	}else{
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">GSTIN : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispgst_number.'</span>';
	}
	
        $message = '';
        
	if($action == 'sendbill'){
	$message .= '
	<!doctype html>
	    <html>
	       <head>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <title></title>
		  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=greek" rel="stylesheet">
		  <link rel="stylesheet" href="./source/invoice_emailer/css/font-awesome.min.css">
		  <style type="text/css">
		     html,  body {
		     margin: 0 !important;
		     padding: 0 !important;
		     height: 100% !important;
		     width: 100% !important;
		     }
		     * {
		     -ms-text-size-adjust: 100%;
		     -webkit-text-size-adjust: 100%;
		     }
		     .ExternalClass {
		     width: 100%;
		     }

		     div[style*="margin: 10px 0"] {
		     margin: 0 !important;
		     }

		     table,  td {
		     mso-table-lspace: 0pt !important;
		     mso-table-rspace: 0pt !important;
		     }
		     
		     table {
		     border-spacing: 0 !important;
		     border-collapse: collapse !important;
		     table-layout: fixed !important;
		     margin: 0 auto !important;
		     }
		     table table table {
		     table-layout: auto;
		     }

		     img {
		     -ms-interpolation-mode: bicubic;
		     }

		     .yshortcuts a {
		     border-bottom: none !important;
		     }

		     a[x-apple-data-detectors] {
		     color: inherit !important;
		     }
		  </style>
		  <style type="text/css">

		     .button-td,
		     .button-a {
		     transition: all 100ms ease-in;
		     }
		     .button-td:hover,
		     .button-a:hover {
		     background: #2fbdf2 !important;
		     border-color: #2fbdf2 !important;
		     }

		     .button-get-td,
		     .button-get-a {
		     transition: all 100ms ease-in;
		     }
		     .button-get-td:hover,
		     .button-get-a:hover {
		     background: #545454 !important;
		     border-color: #545454 !important;
		     }
		     /* Media Queries */
		     @media screen and (max-width: 600px) {
		     .email-container {
		     width: 100% !important;
		     }

		     .fluid,
		     .fluid-centered {
		     max-width: 100% !important;
		     height: auto !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* And center justify these ones. */
		     .fluid-centered {
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* What it does: Forces table cells into full-width rows. */
		     .stack-column,
		     .stack-column-center {
		     display: block !important;
		     width: 100% !important;
		     max-width: 100% !important;
		     direction: ltr !important;
		     }
		     /* And center justify these ones. */
		     .stack-column-center {
		     text-align: center !important;
		     }
		     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
		     .center-on-narrow {
		     text-align: center !important;
		     display: block !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     float: none !important;
		     }
		     table.center-on-narrow {
		     display: inline-block !important;
		     }
		     }
		  </style>
	       </head>
	       <body bgcolor="#ffffff"  style="margin: 0;">';
	}
	$message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Advanced Monthly Plan Receipt</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>';
						    if($freemonths != '0'){
					$message .= '<p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$planname.' <br/>(Pay for: '.$advpayfor.' Months & Free: '.$freemonths.' Months)</span>
						    </p>';
						    }else{
							$message .= '<p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$planname.' <br/>(Pay for: '.$advpayfor.' Months)</span>
						    </p>';
						    }
				$message .=	'</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"><img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
	       </body>
	    </html>
	
	';
	
        
        
	$from = ''; $fromname = '';
        $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
	
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	     $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetailArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
	
        if($action == 'viewbill'){
            echo $message;
        }elseif($action == 'sendbill'){      
            if(count($config) > 0){
                $this->email->initialize($config);
                $this->email->from($from, $fromname);
                $this->email->to($email);
                $this->email->subject('Installation Cost Bill');
                $this->email->message($message);	
                $this->email->send();
                echo 'Email has been send successfully.';
            }else{
                echo 'Oops! Please setup email gateway to send mails.';
            }
        }elseif($action == 'downloadbill'){
            
            echo 'Download Option will be provided soon.';
        }else{
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject('Installation Cost Bill');
            $this->email->message($message);	
            $this->email->send();
            return 1;
	}
        
    }
    
    
    
    /**************** PDF *********************************/
    public function download_billpdf($billid){
        $billquery = $this->db->query('SELECT * FROM sht_subscriber_billing WHERE status="1" AND is_deleted="0" AND id="'.$billid.'" ORDER BY id DESC LIMIT 1');
        $billnum_rows = $billquery->num_rows();
        $billingdata = $billquery->row();
        $uuid = $billingdata->subscriber_uuid;
    
	$apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
	$billtype = $billingdata->bill_type;
        $user_rowdata = $this->getcustomer_data($uuid);
        $state = $this->getstatename($user_rowdata->billing_state);
        $city = $this->getcityname($user_rowdata->billing_state,$user_rowdata->billing_city);
        $zone = $this->getzonename($user_rowdata->billing_zone);
	$mobileno = $user_rowdata->billing_mobileno;
        $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
	$gstin_number = $user_rowdata->gstin_number;
	$hsn_sac = '9984';
	$email = $user_rowdata->email;
        $isp_uid = $user_rowdata->isp_uid;
	$taxtype = $user_rowdata->taxtype;
	$tax = $this->current_taxapplicable($isp_uid);
	if($taxtype == '1'){
	    $apply_cgst_sgst_tax = 1;
	}else{
	    $apply_igst_tax = 1;
	}

        $next_bill_date = date('d-m-Y', strtotime($user_rowdata->next_bill_date));
        $billingdata = $this->user_billing_listing($uuid, $billid);
        $bill_number = $billingdata->bill_number;
        $payment_mode = $billingdata->payment_mode;
        $actual_amount = $billingdata->actual_amount;
        $discount = $billingdata->discount;
	$discounttype = $billingdata->discounttype;
	$is_advanced_paid = $billingdata->is_advanced_paid;
	$billing_with_tax = $billingdata->billing_with_tax;

        $total_amount = $billingdata->total_amount;
	$service_tax = $billingdata->service_tax;
	$swachh_bharat_tax = $billingdata->swachh_bharat_tax;
	$krishi_kalyan_tax = $billingdata->krishi_kalyan_tax;
	$cgst_tax = $billingdata->cgst_tax;
	$sgst_tax = $billingdata->sgst_tax;
	$igst_tax = $billingdata->igst_tax;
        $amount_inwords = $this->getIndianCurrency($total_amount);
	
	$hsn_sac = (($billingdata->hsn_number) != '') ? $billingdata->hsn_number : '9984';
	$bill_comments = $billingdata->bill_comments;
	if($bill_comments != ''){
	    $bill_comments = ' '.$bill_comments;
	}
	$bill_remarks = $billingdata->bill_remarks;
	if($bill_remarks != ''){
	    $bill_remarks = 'Remarks: '.$bill_remarks;
	}
	$currsymbol = $this->country_currency($isp_uid);
	
	$billremark_commnt = '<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.'</td>';
	
	$user_plan_type = $billingdata->user_plan_type;
	$bill_date = date('d-m-Y', strtotime($billingdata->bill_added_on));
	$bill_starts_from = $billingdata->bill_starts_from;
	$billduedaysArr = $this->isp_billduedays($isp_uid);
	$duedays = $billduedaysArr['duedays'];
	$late_charges = $billduedaysArr['late_charges'];
	$charges_unit = $billduedaysArr['charges_unit'];
	$latebillamount = ''; $bill_duedate = '';
	if($duedays != '0'){
	    $bill_duedate = date('d-m-Y', strtotime($bill_date ." +". $duedays ." days"));
	}
        
	if($late_charges != '0'){
	    if($charges_unit == 'percent'){
		$latebillamount = ($total_amount + round(($total_amount * $late_charges)/100));
	    }elseif($charges_unit == 'net'){
		$latebillamount = ($total_amount + $late_charges);
	    }
	}
	
        /*$lastplan_activatedate = $this->lastplan_activatedate($billid);  
        if($bill_starts_from == '0000-00-00'){
            if($lastplan_activatedate == ''){
                $plan_activation_date = date('d-m-Y', strtotime($user_rowdata->plan_activated_date));
            }else{
                $plan_activation_date = date('d-m-Y', strtotime($lastplan_activatedate));
            }
        }else{
            $plan_activation_date = date('d-m-Y', strtotime($billingdata->bill_starts_from));
        }
        
        if($user_plan_type == 'prepaid'){
            $bill_tilldate = date('d-m-Y', strtotime($plan_activation_date." +$plan_tilldays day"));
        }else{
            $bill_tilldate = $bill_date;
        }*/
	
	
	$plandetails = $this->getplan_details($billingdata->plan_id, $uuid);
        $plan_duration = $plandetails['plan_duration'];
        $plan_tilldays = $plandetails['plan_tilldays'];
	$plan_price = $plandetails['price'];
	$plan_grossamt = $plandetails['actual_price'];
	if($used_netdays < 28){
	    $plan_tilldays = ($plan_duration * 30);
	    $plan_cost_perday = round(($plan_grossamt/$plan_tilldays));
	    $plan_grossamt = round($plan_cost_perday * $used_netdays);
	}
	
	
	if($is_advanced_paid == 0){
	    $addichrgesrow = ''; $add_addicharges = 0;
	    $addichrgQ = $this->db->query("SELECT * FROM sht_billing_additional_charges WHERE bill_id='".$billid."'");
	    if($addichrgQ->num_rows() > 0){
		foreach($addichrgQ->result() as $addchrgobj){
		    $charges_details = ucwords($addchrgobj->charges_details);
		    $addiamount = $addchrgobj->amount;
		    $add_addicharges += $addiamount;
		    $addichrgesrow .= '
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">'.$charges_details.'</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($addiamount,2).'</span>
			   </p>
			</td>
		    </tr>';
		}
	    }
	    if($service_tax != '0.00'){
		$actual_billamt = $billingdata->actual_amount;
		$tax_msg .= '
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Service Tax</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($service_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Krishi Kalyan Tax</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($krishi_kalyan_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="4" style="text-align: right">
			</td>
		     </tr>
		     <tr>
			'.$billremark_commnt.'
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			      NET AMOUNT (Including Tax)
			      </span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			   </p>
			</td>
		     </tr>
		';
		
	       
	    }
	    else{
		if($apply_cgst_sgst_tax == 1){
		    /*Example:
		     *	
		     *	Total Bill Amount = 30
		     *	Plan Price = 37
		     *	Tax = 18%
		     *
		     *	30.34 - 6.66 = 37
		     *     - 7.00 (Discount)
		     *
		     *    	23.34 + 6.66 = 30
		     *	
		     */
		    $tax = (100 + $tax);
		    $actual_billamt = $billingdata->actual_amount;
		    if(($discounttype == 'percent') && ($discount != 0)){
			$discount_amt = round(($actual_billamt * $billingdata->discount)/100);
		    }elseif(($discounttype == 'flat') && ($discount != 0)){
			$discount_amt = $billingdata->discount;
		    }else{
			$discount_amt = 0;
		    }
		    $after_disc_billamt = ($actual_billamt - $discount_amt);
		    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
			$taxamt = (($after_disc_billamt * ($tax - 100)) / 100);
			$cgst_tax = round($taxamt/2);
			$sgst_tax = round($taxamt/2);
		    }
		    $tax_msg .= '
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
			       </p>
			    </td>
			</tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
			       </p>
			    </td>
			 </tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">CGST @9%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($cgst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">SGST @9%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($sgst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>'.$addichrgesrow.'
			 <tr>
			    <td colspan="4" style="text-align: right">
			    </td>
			 </tr>
			 <tr>
			    '.$billremark_commnt.'
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
				  NET AMOUNT (Including Tax)
				  </span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			       </p>
			    </td>
			 </tr>
		    ';
		}
		else{
    
		    $tax = (100 + $tax);
		    $actual_billamt = $billingdata->actual_amount;
		    if(($discounttype == 'percent') && ($discount != 0)){
			$discount_amt = round(($actual_billamt * $billingdata->discount)/100);
		    }elseif(($discounttype == 'flat') && ($discount != 0)){
			$discount_amt = $billingdata->discount;
		    }else{
			$discount_amt = 0;
		    }
		    $after_disc_billamt = ($actual_billamt - $discount_amt);
		    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
			$taxamt = (($after_disc_billamt * ($tax - 100)) / 100);
			$igst_tax = round($taxamt);
		    }
		    
		    $tax_msg .= '
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
			       </p>
			    </td>
			 </tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
			       </p>
			    </td>
			 </tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">IGST @18%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($igst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>'.$addichrgesrow.'
			 <tr>
			    <td colspan="4" style="text-align: right">
			    </td>
			 </tr>
			 <tr>
			    '.$billremark_commnt.'
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
				  NET AMOUNT (Including Tax)
				  </span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			       </p>
			    </td>
			 </tr>
		    ';
		    
		}
    
	    }
	    
	}
	else{
	    $actual_billamt = $billingdata->total_amount;
	    if($bill_comments == ''){
		$billremark_commnt = '<td colspan="2" rowspan="3" valign="middle">Payment Received in Advanced <br/> '.$bill_remarks.'</td>';
	    }
	    $tax_msg .= '
		 <tr>
		    '.$billremark_commnt.'
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	}
	
	
	if(($billtype == 'installation' || $billtype == 'security') && ($billing_with_tax == '0')){
	    $actual_billamt1 = $billingdata->total_amount;
	    if(($discounttype == 'percent') && ($discount != 0)){
		$discount_amt = round(($actual_billamt1 * $billingdata->discount)/100);
	    }elseif(($discounttype == 'flat') && ($discount != 0)){
		$discount_amt = $billingdata->discount;
	    }else{
		$discount_amt = 0;
	    }
	    $actual_billamt = $actual_billamt1 + $discount_amt; 
	    $after_disc_billamt = ($actual_billamt - $discount_amt);
	    $tax_msg = '
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
		       </p>
		    </td>
		</tr>
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    '.$billremark_commnt.'
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	}
	$ispdetailArr = $this->isp_details($isp_uid);
        $company_name = $ispdetailArr['company_name'];
        $address = $ispdetailArr['address1'];
        $helpline = $ispdetailArr['help_number1'];
        $support_email = $ispdetailArr['support_email'];
        $isp_name = ucwords($ispdetailArr['isp_name']);
        $ispgst_number = $ispdetailArr['gst_no'];
	$isp_service_tax_no = $ispdetailArr['service_tax_no'];
	$ispcin_number = $ispdetailArr['cin_number'];
	$isp_website = $ispdetailArr['website'];
        $isp_state = $this->getstatename($ispdetailArr['state']);
        $isplogo = $this->get_isplogo($isp_uid);
	$isp_signature = $this->get_ispsignaturelogo($isp_uid);
	$isppancard = $ispdetailArr['pancard'];
	$invoice_color = $ispdetailArr['invoice_color'];
	$invoice_banner1 = $ispdetailArr['invoice_banner1'];
	$invoice_banner2 = $ispdetailArr['invoice_banner2'];
	$invoice_statecode = $ispdetailArr['invoice_statecode'];
	
	$isptaxdet = '';
	$billmonth = date('m', strtotime($bill_date));
	$billyear = date('Y', strtotime($bill_date));
	if(($billmonth < 7) && ($billyear == '2017')){
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">Service Tax : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isp_service_tax_no.'</span>';
	}else{
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">GSTIN : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispgst_number.'</span>';
	}
	
	
        $message = '';
        
        $ispacctQ = $this->db->query("SELECT tb1.portal_url, tb1.decibel_account FROM sht_isp_admin as tb1 WHERE tb1.isp_uid='".$isp_uid."'");
        if($ispacctQ->num_rows() > 0){
            $isprowdata = $ispacctQ->row();
            $portal_url = $isprowdata->portal_url;
            $decibel_account = $isprowdata->decibel_account;
            if($decibel_account == 'paid'){
                $ispurl = 'https://'.$portal_url.'.shouut.com/consumer/';
            }else{
                $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/consumer/';
            }
        }
        
        if($billtype == 'topup'){
            $plandetails = $this->gettopup_details($billingdata->plan_id);
            $topuptype = $plandetails['topuptype'];
            if($topuptype == '1'){
                $topuptype = 'Data Topup';
            }elseif($topuptype == '2'){
                $topuptype = 'Data Unaccountancy Topup';
            }elseif($topuptype == '3'){
                $topuptype = 'Speed Topup';
            }
            $topupname = $plandetails['topupname'];
	    $topupprice = $plandetails['topupprice'];
	    
	    $topupdetails = '';
	    if($topuptype != 'Data Topup'){
		$appliedtopup_details = $this->appliedtopup_details($billingdata->plan_id);
		$topup_startdate = date('d-m-Y', strtotime($appliedtopup_details['topup_startdate']));
		$topup_enddate = date('d-m-Y', strtotime($appliedtopup_details['topup_enddate']));
		$applicable_days = $appliedtopup_details['applicable_days'];
		$total_topupamount = ($topupprice * $applicable_days);
		$topupdetails = $topup_startdate.' to '.$topup_enddate.' ('.$applicable_days.' days)';
	    }
        }
        
        $this->load->library('Dpdf');
        ini_set('memory_limit', '256M');
	
	$message .= '
	<!doctype html>
	    <html>
	       <head>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <title></title>
		  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=greek" rel="stylesheet">
		  <link rel="stylesheet" href="'.base_url().'assets/media/invoicebanner/css/font-awesome.min.css">
		  <style type="text/css">
		     html,  body {
		     margin: 0 !important;
		     padding: 0 !important;
		     height: 100% !important;
		     width: 100% !important;
		     }
		     * {
		     -ms-text-size-adjust: 100%;
		     -webkit-text-size-adjust: 100%;
		     }
		     .ExternalClass {
		     width: 100%;
		     }

		     div[style*="margin: 10px 0"] {
		     margin: 0 !important;
		     }

		     table,  td {
		     mso-table-lspace: 0pt !important;
		     mso-table-rspace: 0pt !important;
		     }
		     
		     table {
		     border-spacing: 0 !important;
		     border-collapse: collapse !important;
		     table-layout: fixed !important;
		     margin: 0 auto !important;
		     }
		     table table table {
		     table-layout: auto;
		     }

		     img {
		     -ms-interpolation-mode: bicubic;
		     }

		     .yshortcuts a {
		     border-bottom: none !important;
		     }

		     a[x-apple-data-detectors] {
		     color: inherit !important;
		     }
		  </style>
		  <style type="text/css">

		     .button-td,
		     .button-a {
		     transition: all 100ms ease-in;
		     }
		     .button-td:hover,
		     .button-a:hover {
		     background: #2fbdf2 !important;
		     border-color: #2fbdf2 !important;
		     }

		     .button-get-td,
		     .button-get-a {
		     transition: all 100ms ease-in;
		     }
		     .button-get-td:hover,
		     .button-get-a:hover {
		     background: #545454 !important;
		     border-color: #545454 !important;
		     }
		     /* Media Queries */
		     @media screen and (max-width: 600px) {
		     .email-container {
		     width: 100% !important;
		     }

		     .fluid,
		     .fluid-centered {
		     max-width: 100% !important;
		     height: auto !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* And center justify these ones. */
		     .fluid-centered {
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* What it does: Forces table cells into full-width rows. */
		     .stack-column,
		     .stack-column-center {
		     display: block !important;
		     width: 100% !important;
		     max-width: 100% !important;
		     direction: ltr !important;
		     }
		     /* And center justify these ones. */
		     .stack-column-center {
		     text-align: center !important;
		     }
		     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
		     .center-on-narrow {
		     text-align: center !important;
		     display: block !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     float: none !important;
		     }
		     table.center-on-narrow {
		     display: inline-block !important;
		     }
		     }
		  </style>
	       </head>
	       <body bgcolor="#ffffff"  style="margin: 0;">';
        
        if($billtype == 'installation'){
            $message .= '
	    <div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px;font-family: FontAwesome, sans-serif">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;"></span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Installation Charges</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">1. Installation</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" >
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
	       </body>
	    </html>';
            //echo $message; die;
	    $message = preg_replace('/>\s+</', "><", $message);
	    $this->dpdf->load_html($message);
	    $this->dpdf->render();
	    $output = $uuid.'_installation_' . $bill_date . '_.pdf';
	    $this->dpdf->stream($output);
        }
        elseif($billtype == 'security'){
            $message .= '
            <div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">SECURITY INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Security Deposit</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">Security Deposit</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
	       </body>
	    </html>';

	    $message = preg_replace('/>\s+</', "><", $message);
            $this->dpdf->load_html($message);
	    $this->dpdf->render();
	    $output = $uuid.'_security_' . $bill_date . '_.pdf';
	    $this->dpdf->stream($output);

        }
        elseif($billtype == 'topup'){
            $message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Topups Billing</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$topuptype.': '.$topupname.' <br/> '.$topupdetails.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
	       </body>
	    </html>';
	    $message = preg_replace('/>\s+</', "><", $message);
	    $this->dpdf->load_html($message);
	    $this->dpdf->render();
	    $output = $uuid.'_topupbill_' . $bill_date . '_.pdf';
	    $this->dpdf->stream($output);
        }
        elseif(($billtype == 'montly_pay') || ($billtype == 'midchange_plancost')){
	    
	    $message .='
	    
	    <div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>';
						if(($bill_duedate != '') && ($is_advanced_paid == 0)){
						$message .=    '
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 11px; padding: 0px;">Due Date</p>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_duedate.'</p>
						</td>';
						}
					     $message .= '
					     </tr>

					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>';
					    if(($latebillamount != '') && ($is_advanced_paid == 0)){
						$message .= '
						<td style="padding:0px" valign="top" rowspan="2">
						    <p style="margin:0px;">
						       <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">AMOUNT AFTER DUE DATE</span> 
						    </p>
						    <p style="margin:0px; padding-bottom: 5px;">
							<span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 13px; padding: 0px; margin: 0px;">'.$currsymbol.' '.number_format($latebillamount,2).'</span> 
						    </p>
						</td>
						';
					    }else{
						$message .= '
						<td style="padding:0px" valign="top" rowspan="2">
						    <p style="margin:0px;">
						       <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						    </p>
						    <p style="margin:0px; padding-bottom: 5px;">
							<span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 13px; padding: 0px; margin: 0px;">&nbsp;</span> 
						    </p>
						</td>
						';
					    }
					    $message .= '
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Monthly Plan Bill</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">Package: '.$plandetails['planname'].'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
		</body>
	    </html>';
            
	    //echo $message; die;
	    $message = preg_replace('/>\s+</', "><", $message);
	    $this->dpdf->load_html($message);
	    $this->dpdf->render();
	    $output = $uuid.'_monthlybill_' . $bill_date . '_.pdf';
	    $this->dpdf->stream($output);

        }
        elseif($billtype == 'addtowallet'){
            $message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">RECEIPT INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Wallet Receipt</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">Wallet Amount Added</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$total_amount.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($total_amount, 2).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
						<td colspan="2" style="text-align: right">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
						      NET AMOUNT
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2" style="text-align: right">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2" style="text-align: right">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"><img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
	       </body>
	    </html>';
            
	    $message = preg_replace('/>\s+</', "><", $message);
            $this->dpdf->load_html($message);
	    $this->dpdf->render();
	    $output = $uuid.'wallet' . $bill_date . '_.pdf';
	    $this->dpdf->stream($output);
        }

        
	elseif($billtype == 'advprepay'){
	    $advpayfor = 0; $freemonths = 0; $planname = '';
	    $advmonthpayQ = $this->db->query("SELECT srvid, advancepay_for_months, number_of_free_months FROM sht_advance_payments WHERE bill_id='".$billid."'");
	    if($advmonthpayQ->num_rows() > 0){
		$advrowdata = $advmonthpayQ->row();
		$advpayfor = $advrowdata->advancepay_for_months;
		$freemonths = $advrowdata->number_of_free_months;
		$srvid = $advrowdata->srvid;
		$planname = $this->planname($srvid);
	    }
	    $message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Advanced Monthly Plan Receipt</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>';
						    if($freemonths != '0'){
					$message .= '<p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$planname.' <br/>(Pay for: '.$advpayfor.' Months & Free: '.$freemonths.' Months)</span>
						    </p>';
						    }else{
							$message .= '<p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$planname.' <br/>(Pay for: '.$advpayfor.' Months)</span>
						    </p>';
						    }
				$message .=	'</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"><img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
	       </body>
	    </html>';
	    
	    $message = preg_replace('/>\s+</', "><", $message);
	    $this->dpdf->load_html($message);
	    $this->dpdf->render();
	    $output = $uuid.'_advancedpayment_' . $bill_date . '_.pdf';
	    $this->dpdf->stream($output);
	}
    }
    
    public function download_custombillpdf($billid){
	$this->load->library('Dpdf');
        ini_set('memory_limit', '256M');
	
        $billquery = $this->db->query("SELECT * FROM sht_subscriber_custom_billing WHERE id='".$billid."' AND is_deleted='0' ORDER BY id DESC");
        $billnum_rows = $billquery->num_rows();
        $billingdata = $billquery->row();
        $uuid = $billingdata->uid;
    
	$apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
        $user_rowdata = $this->getcustomer_data($uuid);
        $state = $this->getstatename($user_rowdata->billing_state);
        $city = $this->getcityname($user_rowdata->billing_state,$user_rowdata->billing_city);
        $zone = $this->getzonename($user_rowdata->billing_zone);
        $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
	$gstin_number = $user_rowdata->gstin_number;
	$mobileno = $user_rowdata->billing_mobileno;
	$email = $user_rowdata->email;
        $isp_uid = $user_rowdata->isp_uid;
	$taxtype = $user_rowdata->taxtype;
	$tax = $this->current_taxapplicable($isp_uid);
	if($taxtype == '1'){
	    $apply_cgst_sgst_tax = 1;
	}else{
	    $apply_igst_tax = 1;
	}
	
	$billingdata = $this->custom_billing_listing($billid);
	$bill_companyid = $billingdata->bill_companyid;
        $bill_number = $billingdata->bill_number;
        $payment_mode = $billingdata->payment_mode;
        $total_amount = $billingdata->total_amount;
        $discount = $billingdata->discount;
	$bill_date = date('d-m-Y', strtotime($billingdata->added_on));
        if($discount != 0){
            $discount_amt = ($total_amount * $billingdata->discount)/100;
        }else{
            $discount_amt = 0;
        }
        $amount_inwords = $this->getIndianCurrency($total_amount);
        
        $bill_date = date('d M Y', strtotime($billingdata->added_on));
	$hsn_sac =  '9984';
	$bill_comments = '';
	/*$bill_comments = $billingdata->bill_comments;
	if($bill_comments != ''){
	    $bill_comments = ' '.$bill_comments;
	}*/
	$bill_remarks = $billingdata->bill_remarks;
	if($bill_remarks != ''){
	    $bill_remarks = 'NOTE: '.$bill_remarks;
	}
	$currsymbol = $this->country_currency($isp_uid);
	
	//<td colspan="2" rowspan="5" valign="top">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
	
	$companyQ = $this->db->query("SELECT id,company_name FROM sht_isp_company_details WHERE id='".$bill_companyid."'");
	$comp_numrows = $companyQ->num_rows();
	if($comp_numrows > 0){
	    $company_name = $companyQ->row()->company_name;
	}
	
	if($apply_cgst_sgst_tax == 1){
	    $tax = (100 + $tax);
	    $actual_billamt = round(($total_amount * 100) /  $tax);
	    $taxamt = ($total_amount - $actual_billamt);
	    $cgst_tax = round($taxamt/2);
	    $sgst_tax = round($taxamt/2);
	    
	    $tax_msg .= '
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">CGST @9%</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($cgst_tax,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">SGST @9%</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($sgst_tax,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="4" style="text-align: right">
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT (Including Tax)
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	}
	else{
	    $tax = (100 + $tax);
	    $actual_billamt = round(($total_amount * 100) /  $tax);
	    $igst_tax = ($total_amount - $actual_billamt);
	    
	    $tax_msg .= '
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">IGST @18%</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($igst_tax,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="4" style="text-align: right">
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT (Including Tax)
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	    
	}
	
        $ispdetailArr = $this->isp_details($isp_uid);
        //$company_name = $ispdetailArr['company_name'];
        $address = $ispdetailArr['address1'];
        $helpline = $ispdetailArr['help_number1'];
        $support_email = $ispdetailArr['support_email'];
        $isp_name = ucwords($ispdetailArr['isp_name']);
        $ispgst_number = $ispdetailArr['gst_no'];
	$isp_service_tax_no = $ispdetailArr['service_tax_no'];
	$ispcin_number = $ispdetailArr['cin_number'];
	$isppancard = $ispdetailArr['pancard'];
	$isp_website = $ispdetailArr['website'];
        $isp_state = $this->getstatename($ispdetailArr['state']);
        $isplogo = $this->get_isplogo($isp_uid);
	$isp_signature = $this->get_ispsignaturelogo($isp_uid);
	
	$invoice_color = $ispdetailArr['invoice_color'];
	$invoice_banner1 = $ispdetailArr['invoice_banner1'];
	$invoice_banner2 = $ispdetailArr['invoice_banner2'];
	$invoice_statecode = $ispdetailArr['invoice_statecode'];
	
	
	$isptaxdet = '';
	$billmonth = date('m', strtotime($bill_date));
	$billyear = date('Y', strtotime($bill_date));
	if(($billmonth < 7) && ($billyear == '2017')){
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">Service Tax : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isp_service_tax_no.'</span>';
	}else{
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">GSTIN : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispgst_number.'</span>';
	}
	
	$fetch_custombill_invoice = $this->fetch_custombill_invoice($billid);
        $message = '';
        
	if($action == 'sendbill'){
	$message .= '
	<!doctype html>
	    <html>
	       <head>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <title></title>
		  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=greek" rel="stylesheet">
		  <link rel="stylesheet" href="./source/invoice_emailer/css/font-awesome.min.css">
		  <style type="text/css">
		     html,  body {
		     margin: 0 !important;
		     padding: 0 !important;
		     height: 100% !important;
		     width: 100% !important;
		     }
		     * {
		     -ms-text-size-adjust: 100%;
		     -webkit-text-size-adjust: 100%;
		     }
		     .ExternalClass {
		     width: 100%;
		     }

		     div[style*="margin: 10px 0"] {
		     margin: 0 !important;
		     }

		     table,  td {
		     mso-table-lspace: 0pt !important;
		     mso-table-rspace: 0pt !important;
		     }
		     
		     table {
		     border-spacing: 0 !important;
		     border-collapse: collapse !important;
		     table-layout: fixed !important;
		     margin: 0 auto !important;
		     }
		     table table table {
		     table-layout: auto;
		     }

		     img {
		     -ms-interpolation-mode: bicubic;
		     }

		     .yshortcuts a {
		     border-bottom: none !important;
		     }

		     a[x-apple-data-detectors] {
		     color: inherit !important;
		     }
		  </style>
		  <style type="text/css">

		     .button-td,
		     .button-a {
		     transition: all 100ms ease-in;
		     }
		     .button-td:hover,
		     .button-a:hover {
		     background: #2fbdf2 !important;
		     border-color: #2fbdf2 !important;
		     }

		     .button-get-td,
		     .button-get-a {
		     transition: all 100ms ease-in;
		     }
		     .button-get-td:hover,
		     .button-get-a:hover {
		     background: #545454 !important;
		     border-color: #545454 !important;
		     }
		     /* Media Queries */
		     @media screen and (max-width: 600px) {
		     .email-container {
		     width: 100% !important;
		     }

		     .fluid,
		     .fluid-centered {
		     max-width: 100% !important;
		     height: auto !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* And center justify these ones. */
		     .fluid-centered {
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* What it does: Forces table cells into full-width rows. */
		     .stack-column,
		     .stack-column-center {
		     display: block !important;
		     width: 100% !important;
		     max-width: 100% !important;
		     direction: ltr !important;
		     }
		     /* And center justify these ones. */
		     .stack-column-center {
		     text-align: center !important;
		     }
		     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
		     .center-on-narrow {
		     text-align: center !important;
		     display: block !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     float: none !important;
		     }
		     table.center-on-narrow {
		     display: inline-block !important;
		     }
		     }
		  </style>
	       </head>
	       <body bgcolor="#ffffff"  style="margin: 0;">';
	}
	$message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Custom Charges</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Qty</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>';
					     
					     foreach($fetch_custombill_invoice as $custbillobj){
						$message .=  '
						 <tr style="border-bottom: 1px solid #f1f2f2">
						    <td>
							<p style="margin:5px 0px">
							   <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$custbillobj->topic_name.'</span>
							</p>
						    </td>
						    <td>
							<p style="margin:5px 0px; text-align:center;">
							   <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$custbillobj->price.'</span>
							</p>
						    </td>
						    <td>
							<p style="margin:5px 0px; text-align:center;">
							   <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$custbillobj->quantity.'</span>
							</p>
						    </td>
						    <td>
							<p style="margin:5px 0px; text-align:right;">
							   <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol .' '.number_format($custbillobj->total_price,2).'</span>
							</p>
						    </td>
						 </tr>';
					     }
					     
					     $message .=  '
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"><img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
	       </body>
	    </html>
	
	';
	$message = preg_replace('/>\s+</', "><", $message);
        $this->dpdf->load_html($message);
	$this->dpdf->render();
	$output = $uuid.'_custombill_' . $bill_date . '_.pdf';
	$this->dpdf->stream($output);
    }
    public function print_billpdf($billid){
        $billquery = $this->db->query('SELECT * FROM sht_subscriber_billing WHERE status="1" AND is_deleted="0" AND id="'.$billid.'" ORDER BY id DESC LIMIT 1');
        $billnum_rows = $billquery->num_rows();
        $billingdata = $billquery->row();
        $uuid = $billingdata->subscriber_uuid;
    
	$apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
	$billtype = $billingdata->bill_type;
        $user_rowdata = $this->getcustomer_data($uuid);
        $state = $this->getstatename($user_rowdata->billing_state);
        $city = $this->getcityname($user_rowdata->billing_state,$user_rowdata->billing_city);
        $zone = $this->getzonename($user_rowdata->billing_zone);
	$mobileno = $user_rowdata->billing_mobileno;
        $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
	$gstin_number = $user_rowdata->gstin_number;
	$hsn_sac = '9984';
	$email = $user_rowdata->email;
        $isp_uid = $user_rowdata->isp_uid;
	$taxtype = $user_rowdata->taxtype;
	$tax = $this->current_taxapplicable($isp_uid);
	if($taxtype == '1'){
	    $apply_cgst_sgst_tax = 1;
	}else{
	    $apply_igst_tax = 1;
	}

        $next_bill_date = date('d-m-Y', strtotime($user_rowdata->next_bill_date));
        $billingdata = $this->user_billing_listing($uuid, $billid);
        $bill_number = $billingdata->bill_number;
        $payment_mode = $billingdata->payment_mode;
        $actual_amount = $billingdata->actual_amount;
        $discount = $billingdata->discount;
	$billing_with_tax = $billingdata->billing_with_tax;
	$discounttype = $billingdata->discounttype;
	$is_advanced_paid = $billingdata->is_advanced_paid;
	
        $total_amount = $billingdata->total_amount;
	$service_tax = $billingdata->service_tax;
	$swachh_bharat_tax = $billingdata->swachh_bharat_tax;
	$krishi_kalyan_tax = $billingdata->krishi_kalyan_tax;
	$cgst_tax = $billingdata->cgst_tax;
	$sgst_tax = $billingdata->sgst_tax;
	$igst_tax = $billingdata->igst_tax;
        $amount_inwords = $this->getIndianCurrency($total_amount);
	
	$hsn_sac = (($billingdata->hsn_number) != '') ? $billingdata->hsn_number : '9984';
	$bill_comments = $billingdata->bill_comments;
	if($bill_comments != ''){
	    $bill_comments = ' '.$bill_comments;
	}
	$bill_remarks = $billingdata->bill_remarks;
	if($bill_remarks != ''){
	    $bill_remarks = 'Remarks: '.$bill_remarks;
	}
	$currsymbol = $this->country_currency($isp_uid);

	$billremark_commnt = '<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.'</td>';
	
	$user_plan_type = $billingdata->user_plan_type;
	$bill_date = date('d-m-Y', strtotime($billingdata->bill_added_on));
	$bill_starts_from = $billingdata->bill_starts_from;
	$billduedaysArr = $this->isp_billduedays($isp_uid);
	$duedays = $billduedaysArr['duedays'];
	$late_charges = $billduedaysArr['late_charges'];
	$charges_unit = $billduedaysArr['charges_unit'];
	$latebillamount = ''; $bill_duedate = '';
	if($duedays != '0'){
	    $bill_duedate = date('d-m-Y', strtotime($bill_date ." +". $duedays ." days"));
	}
        
	if($late_charges != '0'){
	    if($charges_unit == 'percent'){
		$latebillamount = ($total_amount + round(($total_amount * $late_charges)/100));
	    }elseif($charges_unit == 'net'){
		$latebillamount = ($total_amount + $late_charges);
	    }
	}

	
        /*$lastplan_activatedate = $this->lastplan_activatedate($billid);  
        if($bill_starts_from == '0000-00-00'){
            if($lastplan_activatedate == ''){
                $plan_activation_date = date('d-m-Y', strtotime($user_rowdata->plan_activated_date));
            }else{
                $plan_activation_date = date('d-m-Y', strtotime($lastplan_activatedate));
            }
        }else{
            $plan_activation_date = date('d-m-Y', strtotime($billingdata->bill_starts_from));
        }
        
        if($user_plan_type == 'prepaid'){
            $bill_tilldate = date('d-m-Y', strtotime($plan_activation_date." +$plan_tilldays day"));
        }else{
            $bill_tilldate = $bill_date;
        }*/
	
	
	$plandetails = $this->getplan_details($billingdata->plan_id, $uuid);
        $plan_duration = $plandetails['plan_duration'];
        $plan_tilldays = $plandetails['plan_tilldays'];
	$plan_price = $plandetails['price'];
	$plan_grossamt = $plandetails['actual_price'];
	if($used_netdays < 28){
	    $plan_tilldays = ($plan_duration * 30);
	    $plan_cost_perday = round(($plan_grossamt/$plan_tilldays));
	    $plan_grossamt = round($plan_cost_perday * $used_netdays);
	}
	
	if($is_advanced_paid == 0){
	    $addichrgesrow = ''; $add_addicharges = 0;
	    $addichrgQ = $this->db->query("SELECT * FROM sht_billing_additional_charges WHERE bill_id='".$billid."'");
	    if($addichrgQ->num_rows() > 0){
		foreach($addichrgQ->result() as $addchrgobj){
		    $charges_details = ucwords($addchrgobj->charges_details);
		    $addiamount = $addchrgobj->amount;
		    $add_addicharges += $addiamount;
		    $addichrgesrow .= '
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">'.$charges_details.'</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($addiamount,2).'</span>
			   </p>
			</td>
		    </tr>';
		}
	    }
	    if($service_tax != '0.00'){
		$actual_billamt = $billingdata->actual_amount;
		$tax_msg .= '
		    <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Service Tax</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($service_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td>
			   <p style="margin:5px 0px">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td  style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Krishi Kalyan Tax</span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: center">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			      &nbsp;
			      </span>
			   </p>
			</td>
			<td style="border-bottom: 1px solid #f1f2f2">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($krishi_kalyan_tax,2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="4" style="text-align: right">
			</td>
		     </tr>
		     <tr>
			'.$billremark_commnt.'
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			      NET AMOUNT (Including Tax)
			      </span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
			   </p>
			</td>
		     </tr>
		     <tr>
			<td colspan="2" style="text-align: right">
			   <p style="margin:5px 0px; text-align: right">
			      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			   </p>
			</td>
		     </tr>
		';
		
	       
	    }
	    else{
		if($apply_cgst_sgst_tax == 1){
		    /*Example:
		     *	
		     *	Total Bill Amount = 30
		     *	Plan Price = 37
		     *	Tax = 18%
		     *
		     *	30.34 - 6.66 = 37
		     *     - 7.00 (Discount)
		     *
		     *    	23.34 + 6.66 = 30
		     *	
		     */
		    $tax = (100 + $tax);
		    $actual_billamt = $billingdata->actual_amount;
		    if(($discounttype == 'percent') && ($discount != 0)){
			$discount_amt = round(($actual_billamt * $billingdata->discount)/100);
		    }elseif(($discounttype == 'flat') && ($discount != 0)){
			$discount_amt = $billingdata->discount;
		    }else{
			$discount_amt = 0;
		    }
		    $after_disc_billamt = ($actual_billamt - $discount_amt);
		    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
			$taxamt = (($after_disc_billamt * ($tax - 100)) / 100);
			$cgst_tax = round($taxamt/2);
			$sgst_tax = round($taxamt/2);
		    }
		    $tax_msg .= '
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
			       </p>
			    </td>
			</tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
			       </p>
			    </td>
			 </tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">CGST @9%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($cgst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">SGST @9%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($sgst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>'.$addichrgesrow.'
			 <tr>
			    <td colspan="4" style="text-align: right">
			    </td>
			 </tr>
			 <tr>
			    '.$billremark_commnt.'
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
				  NET AMOUNT (Including Tax)
				  </span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			       </p>
			    </td>
			 </tr>
		    ';
		}
		else{
		    $tax = (100 + $tax);
		    $actual_billamt = $billingdata->actual_amount;
		    if(($discounttype == 'percent') && ($discount != 0)){
			$discount_amt = round(($actual_billamt * $billingdata->discount)/100);
		    }elseif(($discounttype == 'flat') && ($discount != 0)){
			$discount_amt = $billingdata->discount;
		    }else{
			$discount_amt = 0;
		    }
		    $after_disc_billamt = ($actual_billamt - $discount_amt);
		    if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
			$taxamt = (($after_disc_billamt * ($tax - 100)) / 100);
			$igst_tax = round($taxamt);
		    }
		    
		    $tax_msg .= '
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
			       </p>
			    </td>
			 </tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
			       </p>
			    </td>
			 </tr>
			<tr>
			    <td>
			       <p style="margin:5px 0px">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td  style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">IGST @18%</span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: center">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
				  &nbsp;
				  </span>
			       </p>
			    </td>
			    <td style="border-bottom: 1px solid #f1f2f2">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($igst_tax,2).'</span>
			       </p>
			    </td>
			 </tr>'.$addichrgesrow.'
			 <tr>
			    <td colspan="4" style="text-align: right">
			    </td>
			 </tr>
			 <tr>
			    '.$billremark_commnt.'
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
				  NET AMOUNT (Including Tax)
				  </span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
			       </p>
			    </td>
			 </tr>
			 <tr>
			    <td colspan="2" style="text-align: right">
			       <p style="margin:5px 0px; text-align: right">
				  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
			       </p>
			    </td>
			 </tr>
		    ';
		    
		}
	    }
	    
	}	
	else{
	    $actual_billamt = $billingdata->total_amount;
	    if($bill_comments == ''){
		$billremark_commnt = '<td colspan="2" rowspan="3" valign="middle">Payment Received in Advanced <br/> '.$bill_remarks.'</td>';
	    }
	    $tax_msg .= '
		 <tr>
		    '.$billremark_commnt.'
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	}
	
	if(($billtype == 'installation' || $billtype == 'security') && ($billing_with_tax == '0')){
	    $actual_billamt1 = $billingdata->total_amount;
	    if(($discounttype == 'percent') && ($discount != 0)){
		$discount_amt = round(($actual_billamt1 * $billingdata->discount)/100);
	    }elseif(($discounttype == 'flat') && ($discount != 0)){
		$discount_amt = $billingdata->discount;
	    }else{
		$discount_amt = 0;
	    }
	    $actual_billamt = $actual_billamt1 + $discount_amt; 
	    $after_disc_billamt = ($actual_billamt - $discount_amt);
	    $tax_msg = '
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Discount</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($discount_amt,2).'</span>
		       </p>
		    </td>
		</tr>
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">Total</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($after_disc_billamt,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    '.$billremark_commnt.'
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	}
	$ispdetailArr = $this->isp_details($isp_uid);
        $company_name = $ispdetailArr['company_name'];
        $address = $ispdetailArr['address1'];
        $helpline = $ispdetailArr['help_number1'];
        $support_email = $ispdetailArr['support_email'];
        $isp_name = ucwords($ispdetailArr['isp_name']);
        $ispgst_number = $ispdetailArr['gst_no'];
	$isp_service_tax_no = $ispdetailArr['service_tax_no'];
	$ispcin_number = $ispdetailArr['cin_number'];
	$isp_website = $ispdetailArr['website'];
        $isp_state = $this->getstatename($ispdetailArr['state']);
        $isplogo = $this->get_isplogo($isp_uid);
	$isp_signature = $this->get_ispsignaturelogo($isp_uid);
	$isppancard = $ispdetailArr['pancard'];
	$invoice_color = $ispdetailArr['invoice_color'];
	$invoice_banner1 = $ispdetailArr['invoice_banner1'];
	$invoice_banner2 = $ispdetailArr['invoice_banner2'];
	$invoice_statecode = $ispdetailArr['invoice_statecode'];
	
	$isptaxdet = '';
	$billmonth = date('m', strtotime($bill_date));
	$billyear = date('Y', strtotime($bill_date));
	if(($billmonth < 7) && ($billyear == '2017')){
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">Service Tax : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isp_service_tax_no.'</span>';
	}else{
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">GSTIN : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispgst_number.'</span>';
	}
	
	
        $message = '';
        
        $ispacctQ = $this->db->query("SELECT tb1.portal_url, tb1.decibel_account FROM sht_isp_admin as tb1 WHERE tb1.isp_uid='".$isp_uid."'");
        if($ispacctQ->num_rows() > 0){
            $isprowdata = $ispacctQ->row();
            $portal_url = $isprowdata->portal_url;
            $decibel_account = $isprowdata->decibel_account;
            if($decibel_account == 'paid'){
                $ispurl = 'https://'.$portal_url.'.shouut.com/consumer/';
            }else{
                $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/consumer/';
            }
        }
        
        if($billtype == 'topup'){
            $plandetails = $this->gettopup_details($billingdata->plan_id);
            $topuptype = $plandetails['topuptype'];
            if($topuptype == '1'){
                $topuptype = 'Data Topup';
            }elseif($topuptype == '2'){
                $topuptype = 'Data Unaccountancy Topup';
            }elseif($topuptype == '3'){
                $topuptype = 'Speed Topup';
            }
            $topupname = $plandetails['topupname'];
	    $topupprice = $plandetails['topupprice'];
	    
	    $appliedtopup_details = $this->appliedtopup_details($billingdata->plan_id);
	    $topup_startdate = date('d-m-Y', strtotime($appliedtopup_details['topup_startdate']));
	    $topup_enddate = date('d-m-Y', strtotime($appliedtopup_details['topup_enddate']));
	    $applicable_days = $appliedtopup_details['applicable_days'];
	    
	    $total_topupamount = ($topupprice * $applicable_days);
        }
        
        $this->load->library('Dpdf');
        ini_set('memory_limit', '256M');
	
	$message .= '
	<!doctype html>
	    <html>
	       <head>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <title></title>
		  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=greek" rel="stylesheet">
		  <link rel="stylesheet" href="'.base_url().'assets/media/invoicebanner/css/font-awesome.min.css">
		  <style type="text/css">
		     html,  body {
		     margin: 0 !important;
		     padding: 0 !important;
		     height: 100% !important;
		     width: 100% !important;
		     }
		     * {
		     -ms-text-size-adjust: 100%;
		     -webkit-text-size-adjust: 100%;
		     }
		     .ExternalClass {
		     width: 100%;
		     }

		     div[style*="margin: 10px 0"] {
		     margin: 0 !important;
		     }

		     table,  td {
		     mso-table-lspace: 0pt !important;
		     mso-table-rspace: 0pt !important;
		     }
		     
		     table {
		     border-spacing: 0 !important;
		     border-collapse: collapse !important;
		     table-layout: fixed !important;
		     margin: 0 auto !important;
		     }
		     table table table {
		     table-layout: auto;
		     }

		     img {
		     -ms-interpolation-mode: bicubic;
		     }

		     .yshortcuts a {
		     border-bottom: none !important;
		     }

		     a[x-apple-data-detectors] {
		     color: inherit !important;
		     }
		  </style>
		  <style type="text/css">

		     .button-td,
		     .button-a {
		     transition: all 100ms ease-in;
		     }
		     .button-td:hover,
		     .button-a:hover {
		     background: #2fbdf2 !important;
		     border-color: #2fbdf2 !important;
		     }

		     .button-get-td,
		     .button-get-a {
		     transition: all 100ms ease-in;
		     }
		     .button-get-td:hover,
		     .button-get-a:hover {
		     background: #545454 !important;
		     border-color: #545454 !important;
		     }
		     /* Media Queries */
		     @media screen and (max-width: 600px) {
		     .email-container {
		     width: 100% !important;
		     }

		     .fluid,
		     .fluid-centered {
		     max-width: 100% !important;
		     height: auto !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* And center justify these ones. */
		     .fluid-centered {
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* What it does: Forces table cells into full-width rows. */
		     .stack-column,
		     .stack-column-center {
		     display: block !important;
		     width: 100% !important;
		     max-width: 100% !important;
		     direction: ltr !important;
		     }
		     /* And center justify these ones. */
		     .stack-column-center {
		     text-align: center !important;
		     }
		     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
		     .center-on-narrow {
		     text-align: center !important;
		     display: block !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     float: none !important;
		     }
		     table.center-on-narrow {
		     display: inline-block !important;
		     }
		     }
		  </style>
	       </head>
	       <body bgcolor="#ffffff"  style="margin: 0;">';
        
        if($billtype == 'installation'){
            $message .= '
	    <div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="">
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="">
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="">
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Installation Charges</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">1. Installation</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
		  <script type="text/javascript" src="'.base_url().'assets/js/jquery-3.1.1.min.js"></script>
		  <script type="text/javascript">$(document).ready(function() { window.print(); });</script>
	       </body>
	    </html>';
            echo $message; die;
        }
        elseif($billtype == 'security'){
            $message .= '
            <div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">SECURITY INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Security Deposit</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">Security Deposit</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
		  <script type="text/javascript" src="'.base_url().'assets/js/jquery-3.1.1.min.js"></script>
		  <script type="text/javascript">$(document).ready(function() { window.print(); });</script>
	       </body>
	    </html>';
	    
	    echo $message; die;

        }
        elseif($billtype == 'topup'){
            $message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Topups Billing</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$topuptype.': '.$topupname.' <br/> '.$topupdetails.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
		  <script type="text/javascript" src="'.base_url().'assets/js/jquery-3.1.1.min.js"></script>
		  <script type="text/javascript">$(document).ready(function() { window.print(); });</script>
	       </body>
	    </html>';
	    
	    $this->dpdf->load_html($message);
	    $this->dpdf->render();
	    $output = $uuid.'_topupbill_' . $bill_date . '_.pdf';
	    $this->dpdf->stream($output);
        }
        elseif(($billtype == 'montly_pay') || ($billtype == 'midchange_plancost')){
	    
	    $message .='
	    
	    <div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 11px; padding: 0px;">Invoice Date</p>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>';
						if(($bill_duedate != '') && ($is_advanced_paid == 0)){
						$message .=    '
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 11px; padding: 0px;">Due Date</p>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_duedate.'</p>
						</td>';
						}
					     $message .=    '
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>';
					    if(($latebillamount != '') && ($is_advanced_paid == 0)){
						$message .= '
						<td style="padding:0px" valign="top" rowspan="2">
						    <p style="margin:0px;">
						       <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">AMOUNT AFTER DUE DATE</span> 
						    </p>
						    <p style="margin:0px; padding-bottom: 5px;">
							<span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 13px; padding: 0px; margin: 0px;">'.$currsymbol.' '.number_format($latebillamount,2).'</span> 
						    </p>
						</td>
						';
					    }else{
						$message .= '
						<td style="padding:0px" valign="top" rowspan="2">
						    <p style="margin:0px;">
						       <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						    </p>
						    <p style="margin:0px; padding-bottom: 5px;">
							<span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 13px; padding: 0px; margin: 0px;">&nbsp;</span> 
						    </p>
						</td>
						';
					    }
					    $message .= '
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Monthly Plan Bill</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">Package: '.$plandetails['planname'].'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding: 5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 	<img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
		  <script type="text/javascript" src="'.base_url().'assets/js/jquery-3.1.1.min.js"></script>
		  <script type="text/javascript">$(document).ready(function() { window.print(); });</script>
		</body>
	    </html>';
            
	    echo $message; die;

        }
        elseif($billtype == 'addtowallet'){
            $message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">RECEIPT INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Wallet Receipt</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">Wallet Amount Added</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$total_amount.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($total_amount, 2).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
						<td colspan="2" style="text-align: right">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
						      NET AMOUNT
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2" style="text-align: right">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2" style="text-align: right">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"><img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
		  <script type="text/javascript" src="'.base_url().'assets/js/jquery-3.1.1.min.js"></script>
		  <script type="text/javascript">$(document).ready(function() { window.print(); });</script>
	       </body>
	    </html>';
	    echo $message; die;
	}
        
	elseif($billtype == 'advprepay'){
	    $advpayfor = 0; $freemonths = 0; $planname = '';
	    $advmonthpayQ = $this->db->query("SELECT srvid, advancepay_for_months, number_of_free_months FROM sht_advance_payments WHERE bill_id='".$billid."'");
	    if($advmonthpayQ->num_rows() > 0){
		$advrowdata = $advmonthpayQ->row();
		$advpayfor = $advrowdata->advancepay_for_months;
		$freemonths = $advrowdata->number_of_free_months;
		$srvid = $advrowdata->srvid;
		$planname = $this->planname($srvid);
	    }
	    $message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Advanced Monthly Plan Receipt</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>';
						    if($freemonths != '0'){
					$message .= '<p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$planname.' <br/>(Pay for: '.$advpayfor.' Months & Free: '.$freemonths.' Months)</span>
						    </p>';
						    }else{
							$message .= '<p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$planname.' <br/>(Pay for: '.$advpayfor.' Months)</span>
						    </p>';
						    }
				$message .=	'</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$hsn_sac.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol.' '.$actual_billamt.'</span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol.' '.number_format($actual_billamt, 2).'</span>
						   </p>
						</td>
					     </tr>
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"><img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
		  <script type="text/javascript" src="'.base_url().'assets/js/jquery-3.1.1.min.js"></script>
		  <script type="text/javascript">$(document).ready(function() { window.print(); });</script>
	       </body>
	    </html>';
	     
	    echo $message; die;
	}
    }
    
    public function print_custombillpdf($billid){
        ini_set('memory_limit', '256M');
	
        $billquery = $this->db->query("SELECT * FROM sht_subscriber_custom_billing WHERE id='".$billid."' AND is_deleted='0' ORDER BY id DESC");
        $billnum_rows = $billquery->num_rows();
        $billingdata = $billquery->row();
        $uuid = $billingdata->uid;
    
	$apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
        $user_rowdata = $this->getcustomer_data($uuid);
        $state = $this->getstatename($user_rowdata->billing_state);
        $city = $this->getcityname($user_rowdata->billing_state,$user_rowdata->billing_city);
        $zone = $this->getzonename($user_rowdata->billing_zone);
        $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
	$gstin_number = $user_rowdata->gstin_number;
	$mobileno = $user_rowdata->billing_mobileno;
	$email = $user_rowdata->email;
        $isp_uid = $user_rowdata->isp_uid;
	$taxtype = $user_rowdata->taxtype;
	$tax = $this->current_taxapplicable($isp_uid);
	if($taxtype == '1'){
	    $apply_cgst_sgst_tax = 1;
	}else{
	    $apply_igst_tax = 1;
	}
	
	$billingdata = $this->custom_billing_listing($billid);
	$bill_companyid = $billingdata->bill_companyid;
        $bill_number = $billingdata->bill_number;
        $payment_mode = $billingdata->payment_mode;
        $total_amount = $billingdata->total_amount;
        $discount = $billingdata->discount;
	$bill_date = date('d-m-Y', strtotime($billingdata->added_on));
        if($discount != 0){
            $discount_amt = ($total_amount * $billingdata->discount)/100;
        }else{
            $discount_amt = 0;
        }
        $amount_inwords = $this->getIndianCurrency($total_amount);
        
        $bill_date = date('d M Y', strtotime($billingdata->added_on));
	$hsn_sac =  '9984';
	$bill_comments = '';
	/*$bill_comments = $billingdata->bill_comments;
	if($bill_comments != ''){
	    $bill_comments = ' '.$bill_comments;
	}*/
	$bill_remarks = $billingdata->bill_remarks;
	if($bill_remarks != ''){
	    $bill_remarks = 'NOTE: '.$bill_remarks;
	}
	$currsymbol = $this->country_currency($isp_uid);
	
	//<td colspan="2" rowspan="5" valign="top">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
	
	$companyQ = $this->db->query("SELECT id,company_name FROM sht_isp_company_details WHERE id='".$bill_companyid."'");
	$comp_numrows = $companyQ->num_rows();
	if($comp_numrows > 0){
	    $company_name = $companyQ->row()->company_name;
	}
	
	if($apply_cgst_sgst_tax == 1){
	    $tax = (100 + $tax);
	    $actual_billamt = round(($total_amount * 100) /  $tax);
	    $taxamt = ($total_amount - $actual_billamt);
	    $cgst_tax = round($taxamt/2);
	    $sgst_tax = round($taxamt/2);
	    
	    $tax_msg .= '
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">CGST @9%</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($cgst_tax,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">SGST @9%</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($sgst_tax,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="4" style="text-align: right">
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT (Including Tax)
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '.number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	}else{
	    $tax = (100 + $tax);
	    $actual_billamt = round(($total_amount * 100) /  $tax);
	    $igst_tax = ($total_amount - $actual_billamt);
	    
	    $tax_msg .= '
		<tr>
		    <td>
		       <p style="margin:5px 0px">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td  style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">IGST @18%</span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: center">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
			  &nbsp;
			  </span>
		       </p>
		    </td>
		    <td style="border-bottom: 1px solid #f1f2f2">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($igst_tax,2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="4" style="text-align: right">
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" rowspan="3" valign="middle">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
			  NET AMOUNT (Including Tax)
			  </span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">'.$currsymbol .' '. number_format($total_amount, 2).'</span>
		       </p>
		    </td>
		 </tr>
		 <tr>
		    <td colspan="2" style="text-align: right">
		       <p style="margin:5px 0px; text-align: right">
			  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">'.$amount_inwords.'</span>
		       </p>
		    </td>
		 </tr>
	    ';
	    
	}
	
        $ispdetailArr = $this->isp_details($isp_uid);
        //$company_name = $ispdetailArr['company_name'];
        $address = $ispdetailArr['address1'];
        $helpline = $ispdetailArr['help_number1'];
        $support_email = $ispdetailArr['support_email'];
        $isp_name = ucwords($ispdetailArr['isp_name']);
        $ispgst_number = $ispdetailArr['gst_no'];
	$isp_service_tax_no = $ispdetailArr['service_tax_no'];
	$ispcin_number = $ispdetailArr['cin_number'];
	$isppancard = $ispdetailArr['pancard'];
	$isp_website = $ispdetailArr['website'];
        $isp_state = $this->getstatename($ispdetailArr['state']);
        $isplogo = $this->get_isplogo($isp_uid);
	$isp_signature = $this->get_ispsignaturelogo($isp_uid);
	
	$invoice_color = $ispdetailArr['invoice_color'];
	$invoice_banner1 = $ispdetailArr['invoice_banner1'];
	$invoice_banner2 = $ispdetailArr['invoice_banner2'];
	$invoice_statecode = $ispdetailArr['invoice_statecode'];
	
	
	$isptaxdet = '';
	$billmonth = date('m', strtotime($bill_date));
	$billyear = date('Y', strtotime($bill_date));
	if(($billmonth < 7) && ($billyear == '2017')){
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">Service Tax : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isp_service_tax_no.'</span>';
	}else{
	    $isptaxdet = '<span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">GSTIN : </span> <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispgst_number.'</span>';
	}
	
	$fetch_custombill_invoice = $this->fetch_custombill_invoice($billid);
        $message = '';
        
	if($action == 'sendbill'){
	$message .= '
	<!doctype html>
	    <html>
	       <head>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <title></title>
		  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=greek" rel="stylesheet">
		  <link rel="stylesheet" href="./source/invoice_emailer/css/font-awesome.min.css">
		  <style type="text/css">
		     html,  body {
		     margin: 0 !important;
		     padding: 0 !important;
		     height: 100% !important;
		     width: 100% !important;
		     }
		     * {
		     -ms-text-size-adjust: 100%;
		     -webkit-text-size-adjust: 100%;
		     }
		     .ExternalClass {
		     width: 100%;
		     }

		     div[style*="margin: 10px 0"] {
		     margin: 0 !important;
		     }

		     table,  td {
		     mso-table-lspace: 0pt !important;
		     mso-table-rspace: 0pt !important;
		     }
		     
		     table {
		     border-spacing: 0 !important;
		     border-collapse: collapse !important;
		     table-layout: fixed !important;
		     margin: 0 auto !important;
		     }
		     table table table {
		     table-layout: auto;
		     }

		     img {
		     -ms-interpolation-mode: bicubic;
		     }

		     .yshortcuts a {
		     border-bottom: none !important;
		     }

		     a[x-apple-data-detectors] {
		     color: inherit !important;
		     }
		  </style>
		  <style type="text/css">

		     .button-td,
		     .button-a {
		     transition: all 100ms ease-in;
		     }
		     .button-td:hover,
		     .button-a:hover {
		     background: #2fbdf2 !important;
		     border-color: #2fbdf2 !important;
		     }

		     .button-get-td,
		     .button-get-a {
		     transition: all 100ms ease-in;
		     }
		     .button-get-td:hover,
		     .button-get-a:hover {
		     background: #545454 !important;
		     border-color: #545454 !important;
		     }
		     /* Media Queries */
		     @media screen and (max-width: 600px) {
		     .email-container {
		     width: 100% !important;
		     }

		     .fluid,
		     .fluid-centered {
		     max-width: 100% !important;
		     height: auto !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* And center justify these ones. */
		     .fluid-centered {
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* What it does: Forces table cells into full-width rows. */
		     .stack-column,
		     .stack-column-center {
		     display: block !important;
		     width: 100% !important;
		     max-width: 100% !important;
		     direction: ltr !important;
		     }
		     /* And center justify these ones. */
		     .stack-column-center {
		     text-align: center !important;
		     }
		     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
		     .center-on-narrow {
		     text-align: center !important;
		     display: block !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     float: none !important;
		     }
		     table.center-on-narrow {
		     display: inline-block !important;
		     }
		     }
		  </style>
	       </head>
	       <body bgcolor="#ffffff"  style="margin: 0;">';
	}
	$message .= '<div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" border="0" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" border="0" /> ';
					}
			$message .=    '</td>
				       <td style="width:70%;">
					  <p style="background-color: '.$invoice_color.'; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: '.$invoice_color.'; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; margin: 0px;">'.$company_name.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">'.$address.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$ispdetailArr['help_number1'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['support_email'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:'.$invoice_color.'; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispdetailArr['website'].'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">'.$isptaxdet.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$ispcin_number.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$isppancard.'</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">'.$bill_date.'</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->uid.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$bill_number.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Custom Charges</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.ucfirst($user_rowdata->billing_username).'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$user_rowdata->billing_flat_number.', '. $user_rowdata->billing_address.', '.$city.', '.$state.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$state.'</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$zone.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 '.$mobileno.'  </span>
						   </p>
						</td>';
						if((($billmonth > 7) && ($billyear == '2017')) || ($billyear > '2017')){
				$message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$gstin_number.'</span>
						   </p>
						</td>';
						}else{
				    $message .=	'<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">&nbsp;</span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">&nbsp;</span>
						   </p>
						</td>';
						}
			$message .=	     '</tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:10px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="'.$invoice_color.'">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Qty</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>';
					     
					     foreach($fetch_custombill_invoice as $custbillobj){
						$message .=  '
						 <tr style="border-bottom: 1px solid #f1f2f2">
						    <td>
							<p style="margin:5px 0px">
							   <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$custbillobj->topic_name.'</span>
							</p>
						    </td>
						    <td>
							<p style="margin:5px 0px; text-align:center;">
							   <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$custbillobj->price.'</span>
							</p>
						    </td>
						    <td>
							<p style="margin:5px 0px; text-align:center;">
							   <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$custbillobj->quantity.'</span>
							</p>
						    </td>
						    <td>
							<p style="margin:5px 0px; text-align:right;">
							   <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 2px; margin: 0px;">'.$currsymbol .' '.number_format($custbillobj->total_price,2).'</span>
							</p>
						    </td>
						 </tr>';
					     }
					     
					     $message .=  '
					     '.$tax_msg.'
					  </table>
				       </td>
				       <td style="width:30%;padding:10px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>';
					     if($invoice_banner1 != ''){
						$message .= '<td><img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner1.'" alt="" width="150" /></td>';
					     }else{
						$message .= '<td>&nbsp;</td>';
					     }
				$message .=  '</tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">&nbsp;</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 5px 0px">
					  <p style="margin: 0px">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="ispsignature"/>';
					    }
				$message .= '</p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">'.$company_name.'</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:5px 0px">
					  <p style="padding:0px; margin: 10px;">';
					    if($invoice_banner2 != ''){
						$message .=  '<img src="'.base_url().'assets/media/invoicebanner/'.$invoice_banner2.'" alt="ispbanner2" width="100%"/>';
					    }
			$message .=	 '</p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: '.$invoice_color.';  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>';
						   if($ispdetailArr['account_number'] != ''){ 
						    $message .= '
							<p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Acc. Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'. $ispdetailArr['account_holder_name'] .'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Bank Name : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['bank_name'].'</span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Branch : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> '.$ispdetailArr['branch_address'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  Account Number : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['account_number'].' </span>
						       </p>
						       <p style="margin:0px;">
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
							  IFSC Code : 
							  </span> 
							  <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">'.$ispdetailArr['ifsc_code'].'</span>
						       </p>';
						    }else{
						    $message .= '
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>
							 <p style="margin:0px;">
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">&nbsp;</span> 
							    <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">&nbsp;</span>
							 </p>';
						    }
				$message .=	'</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:'.$invoice_color.'; font-size:15px">
						      <img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:70px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"><img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      3. Late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to '.$state.' jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="'.base_url().'assets/media/invoicebanner/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
		  <script type="text/javascript" src="'.base_url().'assets/js/jquery-3.1.1.min.js"></script>
		  <script type="text/javascript">$(document).ready(function() { window.print(); });</script>
	       </body>
	    </html>
	
	';
	
        echo $message; die;
    }

    public function checkpdf_format(){
	$this->load->library('Dpdf');
        ini_set('memory_limit', '256M'); 
	$message = '
	    <!doctype html>
	    <html>
	       <head>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <title>EmailTemplate-Responsive</title>
		  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=greek" rel="stylesheet">
		  <link rel="stylesheet" href="./source/invoice_emailer/css/font-awesome.min.css">
		  <style type="text/css">
		     html,  body {
		     margin: 0 !important;
		     padding: 0 !important;
		     height: 100% !important;
		     width: 100% !important;
		     }
		     * {
		     -ms-text-size-adjust: 100%;
		     -webkit-text-size-adjust: 100%;
		     }
		     .ExternalClass {
		     width: 100%;
		     }

		     div[style*="margin: 10px 0"] {
		     margin: 0 !important;
		     }

		     table,  td {
		     mso-table-lspace: 0pt !important;
		     mso-table-rspace: 0pt !important;
		     }
		     
		     table {
		     border-spacing: 0 !important;
		     border-collapse: collapse !important;
		     table-layout: fixed !important;
		     margin: 0 auto !important;
		     }
		     table table table {
		     table-layout: auto;
		     }

		     img {
		     -ms-interpolation-mode: bicubic;
		     }

		     .yshortcuts a {
		     border-bottom: none !important;
		     }

		     a[x-apple-data-detectors] {
		     color: inherit !important;
		     }
		  </style>
		  <!-- Progressive Enhancements -->
		  <style type="text/css">

		     .button-td,
		     .button-a {
		     transition: all 100ms ease-in;
		     }
		     .button-td:hover,
		     .button-a:hover {
		     background: #2fbdf2 !important;
		     border-color: #2fbdf2 !important;
		     }

		     .button-get-td,
		     .button-get-a {
		     transition: all 100ms ease-in;
		     }
		     .button-get-td:hover,
		     .button-get-a:hover {
		     background: #545454 !important;
		     border-color: #545454 !important;
		     }
		     /* Media Queries */
		     @media screen and (max-width: 600px) {
		     .email-container {
		     width: 100% !important;
		     }

		     .fluid,
		     .fluid-centered {
		     max-width: 100% !important;
		     height: auto !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* And center justify these ones. */
		     .fluid-centered {
		     margin-left: auto !important;
		     margin-right: auto !important;
		     }
		     /* What it does: Forces table cells into full-width rows. */
		     .stack-column,
		     .stack-column-center {
		     display: block !important;
		     width: 100% !important;
		     max-width: 100% !important;
		     direction: ltr !important;
		     }
		     /* And center justify these ones. */
		     .stack-column-center {
		     text-align: center !important;
		     }
		     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
		     .center-on-narrow {
		     text-align: center !important;
		     display: block !important;
		     margin-left: auto !important;
		     margin-right: auto !important;
		     float: none !important;
		     }
		     table.center-on-narrow {
		     display: inline-block !important;
		     }
		     }
		  </style>
	       </head>
	       <body bgcolor="#ffffff"  style="margin: 0;">
		  <div style="width:750px; padding: 0px; margin: 0px auto;">
		     <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
			<tr>
			   <td valign="top" style="width: 100%">
			      <center style="width:100%; border:none">
				 <!-- Email Header : BEGIN -->
				 <table width="100%" border="0">
				    <tr>
				       <td rowspan="2" style="padding: 0px 20px 10px 5px; text-align: left;">
					  <img src="./source/invoice_emailer/images/logo.png" width="50%"  alt="alt_text" border="0">
				       </td>
				       <td style="width:70%;">
					  <p style="background-color: #00a2df; text-align: right; padding: 5px 20px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight: 400; font-size: 17px; border-bottom-left-radius: 25px; margin:0px;">TAX INVOICE</p>
				       </td>
				    </tr>
				    <tr>
				       <td style="width:70%; height: 20px; background-color: #ffffff; text-align: right; padding: 5px 20px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight: 600; font-size: 12px; font-style: italic;">Original for the Receipient</td>
				    </tr>
				 </table>
				 <!-- Email Header : END --> 
				 <!-- Email Body : BEGIN -->
				 <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" class="email-container">
				    <tr>
				       <td style="width:40%;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="color: #00a2df; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">SPEED4NET TECNOLOGIES PVT. LTD.</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:5px 10px" valign="top">
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 10px; padding: 0px; margin: 0px;">Plot No. SP 10 & SP 11, Mansarover Industrial Area,Jaipur 302020, Rajasthan</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:#00a2df; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/telephone.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 9266456805</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:#00a2df; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/envelope.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">info@stplnet.com</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color:#00a2df; font-size:10px">
						      <img src="'.base_url().'assets/pdf_font_icons/globe.png" alt="" />
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">www.stplnet.com</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">29AABBB9997A9AA</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      State Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin:0px;">'.$invoice_statecode.'</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      CIN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">U99999KA2000PTC999999</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px 10px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      PAN : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">ABCDE1234A</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:60%" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px;color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px;">Invoice Date</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px 0px 10px 0px; color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 20px; padding: 0px;">04 Nov 2017</p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px; text-align: left">
						      User ID :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">11111111</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Cust. Acc. No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">222222</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice No. :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">11111111</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Period :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">01/10/17 - 30/10/17</span>
						   </p>
						</td>
					     </tr>
					     <tr style="border-bottom:1px dashed #bcbec0; padding-bottom:5px;">
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Invoice Type : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Credit Note </span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px; padding-bottom: 5px;">
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Due Date :
						      </span> 
						      <span style="color: #ef4136; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">15/11/2017</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">xyz</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="2">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Address :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">840, Chirag delhi New delhi 110017</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Place of Supply :
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Delhi</span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Zone : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">New Delhi</span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      Mobile : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">+91 9266456805  </span>
						   </p>
						</td>
						<td style="padding:0px" valign="top">
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      GSTIN :  
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">29AABBB9997A9AA</span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Body : END --> 
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #f1f2f2">
				    <tr>
				       <td style="width:70%;padding:20px 5px; text-align:left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr bgcolor="#00a2df">
						<td style="width:220px;">
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center"> Service/Goods Description</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">SAC / HSN</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: center">Rate</p>
						</td>
						<td>
						   <p style="margin:2px; color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 0px 5px; text-align: right">Amount</p>
						</td>
					     </tr>
					     <tr  style="border-bottom: 1px solid #f1f2f2">
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px; margin: 0px;">
						      1.
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      Internet Pack, 2Mpbs, Unlimited
						      </span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      2222222
						      </span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      &#8377;  1,800.00
						      </span>
						   </p>
						</td>
						<td>
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
						      &#8377;  1,800.00
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      &nbsp;
						      </span>
						   </p>
						</td>
						<td  style="border-bottom: 1px solid #f1f2f2">
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">
						      CGST 
						      </span>
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">
						      @9%
						      </span>
						   </p>
						</td>
						<td style="border-bottom: 1px solid #f1f2f2">
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      &nbsp;
						      </span>
						   </p>
						</td>
						<td style="border-bottom: 1px solid #f1f2f2">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
						      &#8377;  162.00
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      &nbsp;
						      </span>
						   </p>
						</td>
						<td  style="border-bottom: 1px solid #f1f2f2">
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">
						      SGST
						      </span>
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px; margin: 0px;">
						      @9%
						      </span>
						   </p>
						</td>
						<td style="border-bottom: 1px solid #f1f2f2">
						   <p style="margin:5px 0px; text-align: center">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 2px; margin: 0px;">
						      &nbsp;
						      </span>
						   </p>
						</td>
						<td style="border-bottom: 1px solid #f1f2f2">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
						      &#8377;  162.00
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="4" style="text-align: right">
						</td>
					     </tr>
					     <tr>
						<td colspan="4" style="text-align: right">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">
						      NET AMOUNT (Including Tax)
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="4" style="text-align: right">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 15px; padding: 0px 5px; margin: 0px;">
						      &#8377;  2,124.00
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td colspan="4" style="text-align: right">
						   <p style="margin:5px 0px; text-align: right">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px 5px; margin: 0px;">
						      (Two Thousand One Hundred Twenty Four)
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td><img src="./source/invoice_emailer/images/add_s.png"/></td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:5px 0px; text-align: left">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px; margin: 0px; font-style: italic;">
						      Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh. 
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!-- Email Billing Details END --->
				 <!--- Email Add --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:25%; padding: 15px 0px">
					  <p style="margin: 0px"><img src="./source/invoice_emailer/images/sin_img.png"/></p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 10px; padding: 0px 5px; margin: 0px;">Authorised Signatory</span>
					  </p>
					  <p style="margin:0px;">
					     <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 10px; padding: 0px 5px; margin: 0px;">Speed4net Tecnologies Pvt. Ltd.</span>
					  </p>
				       </td>
				       <td style="width:75%; padding:15px 0px">
					  <p style="padding:0px; margin: 10px;">
					     <img src="./source/invoice_emailer/images/add_b.png" width="100%"/>
					  </p>
				       </td>
				    </tr>
				 </table>
				 <!--- Email Add End --->
				 <!--- invoice details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin: 10px 20px;">
				    <tr bgcolor="'.$invoice_color.'">
				       <td colspan="3" style="padding:5px 10px;">
					  <p style="color: #ffffff; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 12px; padding: 0px; margin: 0px;">WAYS TO PAY</p>
				       </td>
				    </tr>
				    <tr>
				       <td colspan="3" style="padding:10px 0px;">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					     <tr>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:#00a2df; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/credit.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay Online Now!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay securely using our online payment gateway using credit or debit cards.
						   </p>
						   <p style="background: #00a2df;  padding: 8px 10px;  font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center;  border-radius: 3px; font-weight: 600; width:120px;">
						      <a href="'.base_url().'consumer" style="color: #ffffff;text-decoration: none;" target="_blank"> 
						      Click To Pay Now!
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Additional trasaction fee may be applicable.</p>
						</td>
						<td style="width:33%; border-right:1px solid #f1f2f2; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:#00a2df; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/bank.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Pay via Netbanking</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Pay your bills via netbanking. Required details provided below:
						   </p>
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
						      Acc. Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> Speed4net Tecnologies Pvt. Ltd. </span>
						   </p>
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
						      Bank Name : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> HDFC Bank Ltd. </span>
						   </p>
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
						      Branch : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;"> Chirag Delhi </span>
						   </p>
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
						      Account Number : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">50200002403135 </span>
						   </p>
						   <p style="margin:0px;">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; margin: 0px;">
						      IFSC Code : 
						      </span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:400; font-size: 9px; padding: 0px 0px; margin: 0px;">HDFC0002870</span>
						   </p>
						</td>
						<td style="width:33%; padding:5px" valign="top">
						   <p style="margin:0px">
						      <span style="color:#00a2df; font-size:15px"><img src="'.base_url().'assets/pdf_font_icons/mobile.png" alt="" /></span> 
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 12px; padding: 0px 5px; margin: 0px;">Get the Mobile App!</span>
						   </p>
						   <p style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 9px; padding: 5px 0px; margin: 0px;">
						      Download our mobile app and keep trak of your usage and pay bills with a tap of a button.
						   </p>
						   <p style="background: #414042;  padding: 5px 10px;color: #ffffff; font-family: sans-serif; font-size: 9px; line-height: 1.1; text-align: center; text-decoration: none; border-radius: 3px; font-weight: 600; margin: 0px 5px; width:60px;">
						      <a href="#" style="color:#ffffff;text-decoration: none; display:block;"> 							<img src="'.base_url().'assets/pdf_font_icons/android.png" alt="" /> Get App
						      </a>
						   </p>
						   <p style="margin: 2px 0px;font-family: sans-serif; font-size:8px; color: #6d6e71; font-style: italic">Available for Android</p>
						</td>
					     </tr>
					  </table>
				       </td>
				    </tr>
				 </table>
				 <!--- invoice details end --->
				 <!-- Email Billing Details --->
				 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				    <tr>
				       <td style="width:70%; padding:20px 5px; text-align:left;" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					     <tr>
						<td>
						   <p style="margin:0px; color: #6d6e71; font-family: \'Open Sans\', sans-serif; font-weight:700; font-size: 9px; padding: 0px; text-align: left"> TERMS & CONDITIONS</p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px 2px; margin: 0px;">
						      1. In case of cheque bounce, Rs.500/ penalty will be levied.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px 2px; margin: 0px;">
						      2. 18% interest will be levied on overdue payments
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px 2px; margin: 0px;">
						      3. Lorem Ipsum Shall levy late fee charge in case the bill is paid after the due date.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px 2px; margin: 0px;">
						      4. In case of overdue / defaults, the right to deactivate your services, is reserved.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px 2px; margin: 0px;">
						      5. All disputes are subject to KARNATAKA jurisdiction.
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px 2px; margin: 0px;">
						      6. Unless otherwise stated,tax on this invoice is not payable under reverse charge. 
						      </span>
						   </p>
						</td>
					     </tr>
					     <tr>
						<td>
						   <p style="margin:0px">
						      <span style="color: #414042; font-family: \'Open Sans\', sans-serif; font-weight:600; font-size: 8px; padding: 0px 2px; margin: 0px;">
						      7. This Invoice is system generated hence signature and stamp is not required.
						      </span>
						   </p>
						</td>
					     </tr>
					  </table>
				       </td>
				       <td style="width:30%;padding:20px 5px;" valign="top">
					  <img src="./source/invoice_emailer/images/add_f.png" width="90%"/>
				       </td>
				    </tr>
				 </table>
			      </center>
			   </td>
			</tr>
		     </table>
		  </div>
	       </body>
	    </html>
	';
	
	//echo $message; die;
	
	$this->dpdf->load_html($message);
  	$this->dpdf->render();
  	$this->dpdf->stream("billinvoice.pdf");
	
	/*$pdf->WriteHTML($message);
        $output = 'billinvoice.pdf';
        $pdf->Output("$output", 'D'); exit();*/
    }
    
    
     public function newplanchangemail_alert($uuid, $baseplanid, $type){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        $this->load->library('email');
          $user_rowdata = $this->getcustomer_data($uuid);
	  $username=$user_rowdata->firstname .' '. $user_rowdata->lastname;
	  $email = $user_rowdata->email;
	$ispdetArr = $this->isp_details($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        $planname = $this->notify_model->getplanname($baseplanid);
        $plandate = $this->notify_model->getnextplandate($uuid);

        if($type == 'nextcycle'){   
           $email_tosend = 'Dear '.$username.'<br/><br/> Please note that your plan has been changed to '.$planname.' as per your request'.' New plan will be effective from '.$plandate. ' For any queries please call us @ '.$help_number. '<br/><br/> Team '.$isp_name;
           $sub="New Plan Changed ".$planname; 
        }elseif($type == 'changenow'){
           $email_tosend = 'Dear '.$username.'<br/><br/> Please note that your plan has been changed to '.$planname.' as per your request'.' New plan will be effective from today For any queries please call us @ '.$help_number. '<br/><br/> Team '.$isp_name;
	   $sub="New Plan Changed ".$planname;
        }elseif($type == 'applyplan'){
            $email_tosend = 'Dear '.$username.'<br/><br/> Please note that your plan has been activated to '.$planname.' as per your request'.' New plan will be effective from today For any queries please call us @ '.$help_number. '<br/><br/> Team '.$isp_name;
	     $sub="New Plan Applied ".$planname;
        }elseif($type == 'cancelplan'){
           $email_tosend = 'Dear '.$username.'<br/><br/> Please note that your plan '.$planname.' for next cycle has been cancelled as per your request.'.' For any queries please call us @ '.$help_number. '<br/><br/> Team '.$isp_name;
	    $sub="Plan Cancelled for Next Cycle ".$planname;
        }elseif($type == 'planspeedchange'){
           $email_tosend = 'Dear '.$username.'<br/><br/> Please note that your plan '.$planname.' Speed has been updated as per your request.'.' For any queries please call us @ '.$help_number. '<br/><br/> Team '.$isp_name;
	   $sub="Plan Speed Updated ".$planname;
        }
	 
        $from = ''; $fromname = '';
        $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
        
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	    $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
        
        if(count($config) > 0){
	    
            //Initialise CI mail
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject($sub);
            $this->email->message($email_tosend);	
            $this->email->send();
        }
        
        return 1;
       
    }
    
    
     public function topup_activation_emailalert($uuid, $topuptype, $topupname, $topupid){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        $user_rowdata = $this->getcustomer_data($uuid);
	  
	  $email = $user_rowdata->email;
        $username = $this->notify_model->getcustomer_fullname($uuid);
        $mobileno = $this->notify_model->getcustomer_mobileno($uuid);
        $ispdetArr = $this->notify_model->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $topuptxt = '';
        if($topuptype != 'Data'){
            $topupArr = $this->notify_model->gettopupdetails($uuid, $topupid);
            if(count($topupArr) > 0){
                $topup_startdate = date('d-m-Y', strtotime($topupArr['topup_startdate']));
                $topup_enddate = date('d-m-Y', strtotime($topupArr['topup_enddate']));
                $topuptxt .= $topup_startdate.' to '.$topup_enddate;
            }
        }else{
            $topuptxt .= 'today';
        }
        
       $sub="New Top-up Applied ".$topupname;
        $email_tosend = 'Dear '.$username.'<br/><br/>'.$topuptype.' Top-up : '.$topupname.' has been applied to your account. Top-up will be effective from '.$topuptxt.' For any queries please call us @ '.$help_number. '<br/><br/>Team '.$isp_name;
        
         $from = ''; $fromname = '';
        $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
        
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	    $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
        
        if(count($config) > 0){
	    
            //Initialise CI mail
	  
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject($sub);
            $this->email->message($email_tosend);	
            $this->email->send();
        }
        
        return 1;
    }
    
    
     public function ticket_activation_emailalert($uuid, $tickettype, $tkt_number){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        $user_rowdata = $this->getcustomer_data($uuid);
	  
	  $email = $user_rowdata->email;
        $username = $this->notify_model->getcustomer_fullname($uuid);
        $mobileno = $this->notify_model->getcustomer_mobileno($uuid);
        $ispdetArr = $this->notify_model->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        $message = urlencode('Dear '.$username.', your '.$tickettype.' #'.$tkt_number.' has been registered.').'%0a'.urlencode('Please allow us sometime to address your request, our team will ensure fastest possible action.').'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
        
        $email_tosend = 'Dear '.$username.',<br/><br/> your '.$tickettype.' #'.$tkt_number.' has been registered. Please allow us sometime to address your request, our team will ensure fastest possible action. For any queries please call us @ '.$help_number.' <br/><br/>Team '.$isp_name;
           $sub="New Ticket Created ".$tickettype." ".$tkt_number;
	   $from = ''; $fromname = '';
        $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
        
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	    $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
        
        if(count($config) > 0){
	    
            //Initialise CI mail
	
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject($sub);
            $this->email->message($email_tosend);	
            $this->email->send();
        }
        
        return 1;
       
    }
    
     public function ticket_close_emailalert($uuid, $tickettype, $tkt_number){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
          $user_rowdata = $this->getcustomer_data($uuid);
	   $email = $user_rowdata->email;
        $username = $this->notify_model->getcustomer_fullname($uuid);
        $mobileno = $this->notify_model->getcustomer_mobileno($uuid);
        $ispdetArr = $this->notify_model->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
       
        $sub="Ticket Closed ".$tickettype." ".$tkt_number;
        $email_tosend = 'Dear '.$username.',<br/><br/> your '.$tickettype.' #'.$tkt_number.' has been successfully resolved. <br/> Now top-up your account and do much more. If your issue has not been resolved, call us at '.$help_number.' <br/> For any queries please call us @ '.$help_number.' <br/><br/>Team '.$isp_name;
        
       $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
         $from = ''; $fromname = '';
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	    $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
        
        if(count($config) > 0){
	    
            //Initialise CI mail
	 
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject($sub);
            $this->email->message($email_tosend);	
            $this->email->send();
        }
        
        return 1;
    }
    
    
    public function payment_received_emailalert($uuid, $amount_received){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
          $user_rowdata = $this->getcustomer_data($uuid);
	   $email = $user_rowdata->email;
	   $subsid=$uuid;
        $username = $this->notify_model->getcustomer_fullname($uuid);
        $mobileno = $this->notify_model->getcustomer_mobileno($uuid);
        $ispdetArr = $this->notify_model->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        
        $email_tosend = 'Dear '.$username.',<br/><br/> we have received a payment of Rs. '.$amount_received.' towards your account with User ID: '.$uuid.' Thank you for your payment! For any queries please call us @ '.$help_number.' <br/><br/>Team '.$isp_name;
        $sub="Sub ID ".$subsid." : Payment received";
          $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
         $from = ''; $fromname = '';
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	    $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
        
        if(count($config) > 0){
	    
            //Initialise CI mail
	   
	 
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject($sub);
            $this->email->message($email_tosend);	
            $this->email->send();
        }
        
        return 1;
    }
    
    
    
     public function add_team_emailalert($msg,$email){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
         
	   $email = $email;
	  
       
        $ispdetArr = $this->notify_model->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        
        $email_tosend = $msg;
        $sub="Your email ID and password";
          $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
         $from = ''; $fromname = '';
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	    $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
        
        if(count($config) > 0){
	    
            //Initialise CI mail
	   
	    $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject($sub);
            $this->email->message($email_tosend);	
            $this->email->send();
        }
        
       
    }
    
     public function forgetpassword_team_emailalert($msg,$email){
        $session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
         
	   $email = $email;
	  
       
        $ispdetArr = $this->notify_model->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
        
        
        $email_tosend = $msg;
        $sub="Forgot Password Request";
          $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
         $from = ''; $fromname = '';
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	    $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
        
        if(count($config) > 0){
	    

	   
	    $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject($sub);
            $this->email->message($email_tosend);	
            $this->email->send();
        }
        
        return 1;
    }
    
    
    
    public function planautorenewalemail_alert($uuid, $planname){
	$session_data = $this->session->userdata('isp_session');
        $isp_uid = $session_data['isp_uid'];
        
	$user_rowdata = $this->getcustomer_data($uuid);
	$email = $user_rowdata->email;
        $username = $this->notify_model->getcustomer_fullname($uuid);
        $mobileno = $this->notify_model->getcustomer_mobileno($uuid);
        $ispdetArr = $this->notify_model->getispdetails($isp_uid);
        $company_name = $ispdetArr['company_name'];
        $isp_name = $ispdetArr['isp_name'];
        $help_number = $ispdetArr['help_number'];
	
	$sub = 'UserID: '.$uuid.' Plan AutoRenewal Status';
	$sms_tosend = 'Dear '.$username.'<br/> Your account with UserID: '.$uuid.' & Plan: '.$planname.' is set for AutoRenewal. <br/>For any queries please call us @ '.$help_number. '<br/> Team '.$isp_name;
	
	$from = ''; $fromname = '';
        $this->load->library('email');
        //$ispaccount = $this->getisp_accounttype($isp_uid);
        
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	    $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
	/*else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}*/
        
        if(count($config) > 0){
	    
            //Initialise CI mail
	  
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject($sub);
            $this->email->message($sms_tosend);	
            $this->email->send();
        }
        
        return 1;
    }
}
?>