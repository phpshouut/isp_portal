<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
class Cron_model extends CI_Model{
    public function __construct(){
        /*$config = Array(
            'protocol' => 'smtp',
            'smtp_host' => SMTP_HOST,
            'smtp_port' => SMTP_PORT,
            'smtp_user' => SMTP_USER,
            'smtp_pass' => SMTP_PASS,
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );
        $this->load->library('email', $config);*/
        
    }

    public function isp_details($isp_uid){
        $query = $this->db->query("SELECT * FROM sht_isp_detail WHERE isp_uid='".$isp_uid."' AND status='1'");
        if($query->num_rows() > 0){
            return  $query->row_array();
        }
    }
    public function OTP_MSG($phone,$msg) {
        $postData = array(
            'authkey' => '106103ADQeqKxOvbT856d19deb',
            'mobiles' => $phone,
            'message' => $msg,
            'sender' => 'SHOUUT',
            'route' => '4'
        );
        $url="https://control.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
    }
    public function getcustomer_data($uuid){
        $userQuery = $this->db->get_where('sht_users', array('uid' => $uuid));
        $num_rows = $userQuery->num_rows();
        return $userQuery->row();
    }
    public function user_billing_listing($uuid, $bill_type){
        $query = $this->db->query('SELECT * FROM sht_subscriber_billing WHERE subscriber_uuid="'.$uuid.'" AND status="1" AND is_deleted="0" AND bill_type="'.$bill_type.'" ORDER BY id DESC LIMIT 1');
        $num_rows = $query->num_rows();
        return $query->row();
    }
    public function getstatename($stateid){
        $name = '';
        $stateQ = $this->db->get_where('sht_states', array('id' => $stateid));
        $num_rows = $stateQ->num_rows();
        if($num_rows > 0){
                $data = $stateQ->row();
                $name = ucwords($data->state);
        }
        return $name;
    }
    public function getcityname($stateid, $cityid){
        $name = '';
        $cityQ = $this->db->get_where('sht_cities', array('city_state_id' => $stateid, 'city_id' => $cityid));
        $num_rows = $cityQ->num_rows();
        if($num_rows > 0){
                $data = $cityQ->row();
                $name = ucwords($data->city_name);
        }
        return $name;
    }
    public function getzonename($zoneid){
        $name = '';
        $zoneQ = $this->db->get_where('sht_zones', array('id' => $zoneid));
        $num_rows = $zoneQ->num_rows();
        if($num_rows > 0){
                $data = $zoneQ->row();
                $name = ucwords($data->zone_name);
        }
        return $name;
    }
    public function getplan_details($srvid, $uuid=''){
        $data = array();
        $planQ = $this->db->query("SELECT tb1.srvname, tb1.descr, tb2.gross_amt, tb1.plan_duration, tb1.isp_uid, tb1.plantype FROM sht_services as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb1.srvid='".$srvid."' AND tb1.enableplan='1'");
        if($planQ->num_rows() > 0){
            $rowdata = $planQ->row();
            $isp_uid = $rowdata->isp_uid;
            $gross_amt = $rowdata->gross_amt;
	    $actual_amt = 0;
            
            $custom_planpriceArr = $this->getcustom_planprice($isp_uid, $uuid, $srvid);
            $custom_planprice = $custom_planpriceArr['gross_amt'];
            if($custom_planprice != ''){
		$actual_amt = $custom_planprice;
                $planprice = round($custom_planprice);
                $plan_duration = $rowdata->plan_duration;
                $plan_tilldays = ($plan_duration * 30);
            }else{
		$actual_amt = $gross_amt;
                $tax = $this->current_taxapplicable($isp_uid);
                $plan_duration = $rowdata->plan_duration;
                $gross_amt = ($gross_amt * $plan_duration);
                $planprice = round($gross_amt + (($gross_amt * $tax)/100));
                $plan_tilldays = ($plan_duration * 30);
            }
            
            $planname = $rowdata->srvname;
            $plandesc = $rowdata->descr;
            
            $data['planname'] = $planname;
            $data['plandesc'] = $plandesc;
            $data['price'] = $planprice;
	    $data['actual_price'] = $actual_amt;
            $data['plantype'] = $rowdata->plantype;
            $data['plan_duration'] = $plan_duration;
            $data['plan_tilldays'] = $plan_tilldays;
            
            return $data;
        }
    }
    

    public function get_isplogo($isp_uid){
        $query = $this->db->query("select tb1.portal_url, tb2.logo_image from sht_isp_admin as tb1 INNER JOIN sht_isp_detail as tb2 ON(tb1.isp_uid=tb2.isp_uid) where tb1.is_activated='1' AND tb1.isp_uid='".$isp_uid."'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return "https://www.shouut.com/ispadmins/".$rowdata->portal_url.".shouut.com/ispmedia/logo/".$rowdata->logo_image;
        }else{
            return  0;
        }
    }
    public function get_ispsignaturelogo($isp_uid){
        $query = $this->db->query("select tb1.portal_url, tb2.logo_signature from sht_isp_admin as tb1 INNER JOIN sht_isp_detail as tb2 ON(tb1.isp_uid=tb2.isp_uid) where tb1.is_activated='1' AND tb1.isp_uid='".$isp_uid."'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return "https://www.shouut.com/ispadmins/".$rowdata->portal_url.".shouut.com/ispmedia/logo/".$rowdata->logo_signature;
        }else{
            return  0;
        }
    }
    
    
    public function getIndianCurrency($number){
        if($number != 0){
            $number = (float) $number;
            $decimal = round($number - ($no = floor($number)), 2) * 100;
            $hundred = null;
            $digits_length = strlen($no);
            $i = 0;
            $str = array();
            $words = array(0 => '', 1 => 'one', 2 => 'two',
                3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
                7 => 'seven', 8 => 'eight', 9 => 'nine',
                10 => 'ten', 11 => 'eleven', 12 => 'twelve',
                13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
                16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
                19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
                40 => 'forty', 50 => 'fifty', 60 => 'sixty',
                70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
            $digits = array('', 'hundred','thousand','lakh', 'crore');
            while( $i < $digits_length ) {
                $divider = ($i == 2) ? 10 : 100;
                $number = floor($no % $divider);
                $no = floor($no / $divider);
                $i += $divider == 10 ? 1 : 2;
                if ($number) {
                    $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                    $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                    $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
                } else $str[] = null;
            }
            $Rupees = implode('', array_reverse($str));
            $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
            return ($Rupees ? ucwords($Rupees) . 'Rupees ' : '') . $paise ;
        }else{
            return 'Zero Rupee';
        }
    }

    
    public function getisp_accounttype($isp_uid){
        $query = $this->db->query("SELECT decibel_account FROM sht_isp_admin WHERE isp_uid='".$isp_uid."' AND is_activated='1'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return $rowdata->decibel_account;
        }
    }
    public function lastplan_activatedate($billid){
        $lastplan_activatedate = '';
        $lastplandateQ = $this->db->query("SELECT lastplan_activatedate FROM sht_userplan_datechange_records WHERE bill_id='".$billid."'");
        if($lastplandateQ->num_rows() > 0){
            $rowdata = $lastplandateQ->row();
            $lastplan_activatedate = $rowdata->lastplan_activatedate;
        }
        return $lastplan_activatedate;
    }
    public function mail_format($uuid){
        
        $apply_cgst_sgst_tax = 0; $apply_igst_tax = 0; $tax_msg = '';
        $user_rowdata = $this->getcustomer_data($uuid);
        $state = $this->getstatename($user_rowdata->state);
        $city = $this->getcityname($user_rowdata->state,$user_rowdata->city);
        $zone = $this->getzonename($user_rowdata->zone);
        $expiry_date = date('d-m-Y', strtotime($user_rowdata->expiration));
	$gstin_number = $user_rowdata->gstin_number;
	$hsn_sac = '9984';
	$email = $user_rowdata->email;
        $isp_uid = $user_rowdata->isp_uid;
	$taxtype = $user_rowdata->taxtype;
	$mobileno = $user_rowdata->mobile;
	$tax = $this->current_taxapplicable($isp_uid);
	if($taxtype == '1'){
	    $apply_cgst_sgst_tax = 1;
	}else{
	    $apply_igst_tax = 1;
	}

        $next_bill_date = date('d-m-Y', strtotime($user_rowdata->next_bill_date));
        $billingdata = $this->user_billing_listing($uuid, 'montly_pay');
        $billid = $billingdata->id;
        $bill_number = $billingdata->bill_number;
        $payment_mode = $billingdata->payment_mode;
        $amount = $billingdata->actual_amount;
        $discount = $billingdata->discount;
        if($discount != 0){
            $discount_amt = ($amount * $billingdata->discount)/100;
        }else{
            $discount_amt = 0;
        }
        $total_amount = $billingdata->total_amount;
	$service_tax = $billingdata->service_tax;
	$swachh_bharat_tax = $billingdata->swachh_bharat_tax;
	$krishi_kalyan_tax = $billingdata->krishi_kalyan_tax;
	$cgst_tax = $billingdata->cgst_tax;
	$sgst_tax = $billingdata->sgst_tax;
	$igst_tax = $billingdata->igst_tax;
        $amount_inwords = $this->getIndianCurrency($total_amount);
	$bill_comments = $billingdata->bill_comments;
	if($bill_comments != ''){
	    $bill_comments = 'Other Details: '.$bill_comments;
	}
	$bill_remarks = $billingdata->bill_remarks;
	if($bill_remarks != ''){
	    $bill_remarks = 'Remarks: '.$bill_remarks;
	}
	
	if($service_tax != '0.00'){
	    $actual_billamt = $billingdata->actual_amount;
	    $tax_msg .= '
	    <td colspan="2" rowspan="5" valign="top">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
	    <tr>
		<td style="border-bottom:1px solid #000000;border-left:1px solid #000000; border-right:1px solid #000000;padding-right:10px;">
		   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Service Tax :</span></p>
		</td>
		<td style="border-bottom:1px solid #000000; border-right:none;padding-right:10px;">
		   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">'.$service_tax.'</p>
		</td>
	     </tr>
	     <tr>
		<td style="border-bottom:1px solid #000000; border-right:1px solid #000000;padding-right:10px;border-left:1px solid #000000;">
		   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Swachh Bharat Tax :</span></p>
		</td>
		<td style="border-bottom:1px solid #000000; border-right:none; padding-right:10px;">
		   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">'.$swachh_bharat_tax.'</p>
		</td>
	     </tr>
	     <tr>
		<td style="border-bottom:1px solid #000000; border-right:1px solid #000000;padding-right:10px;border-left:1px solid #000000;">
		   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Krishi Kalyan Tax :</span></p>
		</td>
		<td style="border-bottom:1px solid #000000; border-right:none; padding-right:10px;">
		   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">'.$krishi_kalyan_tax.'</p>
		</td>
	     </tr>
	     <tr>
		<td style="border-bottom:1px solid #000000; border-right:1px solid #000000; padding-left:10px; padding-top:2px;border-left:1px solid #000000;">
		   <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">Net Amount</p>
		</td>
		<td style="border-bottom:1px solid #000000; border-right:none; padding-right:10px;padding-top:2px;">
		   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">'.number_format($total_amount, 2).'</p>
		</td>
	     </tr>
	     <tr>
		<td colspan="2" style="border-bottom:none; border-right:none; padding-top:2px; padding-left: 10px;">
		   &nbsp;
		</td>
		<td colspan="2" style="border-bottom:none; border-left:1px solid #000; padding-top:2px; padding-left: 10px;">
		   <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Amount in words. : </span> '.$amount_inwords.'</p>
		</td>
	     </tr>';
	}else{
	    if($apply_cgst_sgst_tax == 1){
		$tax = (100 + $tax);
		$actual_billamt = round(($total_amount * 100) /  $tax);
		if(($cgst_tax == '0.00') || ($sgst_tax == '0.00')){
		    $taxamt = ($total_amount - $actual_billamt);
		    $cgst_tax = round($taxamt/2);
		    $sgst_tax = round($taxamt/2);
		}
	    $tax_msg .= '
	    <tr>
		<td colspan="2" rowspan="4" valign="top">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
		<td style="border-bottom:1px solid #000000; border-right:1px solid #000000;border-left:1px solid #000000;padding-right:10px;">
		   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">CGST @ 9 %</span></p>
		</td>
		<td style="border-bottom:1px solid #000000; border-right:none;padding-right:10px;">
		   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">'.number_format($cgst_tax,2).'</p>
		</td>
	     </tr>
	     <tr>
		<td style="border-bottom:1px solid #000000; border-right:1px solid #000000;padding-right:10px;border-left:1px solid #000000;">
		   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">SGST @ 9 %</span></p>
		</td>
		<td style="border-bottom:1px solid #000000; border-right:none; padding-right:10px;">
		   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">'.number_format($sgst_tax,2).'</p>
		</td>
	     </tr>';
	    }else{
		$tax = (100 + $tax);
		$actual_billamt = round(($total_amount * 100) /  $tax);
		if($igst_tax == '0.00'){
		    $igst_tax = ($total_amount - $actual_billamt);
		}
		$tax_msg .= '
		<tr>
		    <td colspan="2" rowspan="4" valign="top">'.$bill_comments.' <br/> '.$bill_remarks.'</td>
		    <td style="border-bottom:1px solid #000000; border-right:1px solid #000000;padding-right:10px;border-left:1px solid #000000;">
		       <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">IGST @ 18 %</span></p>
		    </td>
		    <td style="border-bottom:1px solid #000000; border-right:none;padding-right:10px;">
		       <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">'.number_format($igst_tax, 2).'</p>
		    </td>
		</tr>';
	    }
	    
	    $tax_msg .= '
	     <tr>
		<td style="border-bottom:1px solid #000000; border-right:1px solid #000000; padding-left:10px; padding-top:2px;border-left:1px solid #000000;">
		   <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">Net Amount</p>
		</td>
		<td style="border-bottom:1px solid #000000; border-right:none; padding-right:10px;padding-top:2px;">
		   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:0px; padding:0px;">'.number_format($total_amount, 2).'</p>
		</td>
	     </tr>
	     <tr>
		<td colspan="2" style="border-bottom:none; border-left:1px solid #000; padding-top:2px; padding-left: 10px;">
		   <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Amount in words. : </span> '.$amount_inwords.'</p>
		</td>
	     </tr>';
	}

        
        $bill_date = date('d-m-Y', strtotime($billingdata->bill_added_on));
        $plandetails = $this->getplan_details($billingdata->plan_id, $uuid);
        $plan_duration = $plandetails['plan_duration'];
        $plan_tilldays = $plandetails['plan_tilldays'];
        
        $lastplan_activatedate = $this->lastplan_activatedate($billid);
        $bill_starts_from = $billingdata->bill_starts_from;
        if($bill_starts_from == '0000-00-00'){
            if($lastplan_activatedate == ''){
                $plan_activation_date = date('d-m-Y', strtotime($user_rowdata->plan_activated_date));
            }else{
                $plan_activation_date = date('d-m-Y', strtotime($lastplan_activatedate));
            }
        }else{
            $plan_activation_date = date('d-m-Y', strtotime($billingdata->bill_starts_from));
        }
        $user_plan_type = $billingdata->user_plan_type;
        if($user_plan_type == 'prepaid'){
            $bill_tilldate = date('d-m-Y', strtotime($plan_activation_date." +$plan_tilldays day"));
        }else{
            $bill_tilldate = $bill_date;
        }
        
        $ispdetailArr = $this->isp_details($isp_uid);
        $company_name = $ispdetailArr['company_name'];
        $address = $ispdetailArr['address1'];
        $helpline = $ispdetailArr['help_number1'];
        $support_email = $ispdetailArr['support_email'];
        $isp_name = ucwords($ispdetailArr['isp_name']);
        $ispgst_number = $ispdetailArr['gst_no'];
	$ispcin_number = $ispdetailArr['cin_number'];
	$isp_website = $ispdetailArr['website'];
        $isp_state = $this->getstatename($ispdetailArr['state']);
        $isplogo = $this->get_isplogo($isp_uid);
	$isp_signature = $this->get_ispsignaturelogo($isp_uid);
        $message = '';

        $message .= '<!doctype html>
        <html>
           <head>
              <meta charset="utf-8">
              <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" >
              <meta http-equiv="x-ua-compatible" content="ie=edge">
              <link rel="icon" type="image/png" href="'.$isplogo.'"/>
              <title>'.$company_name.'</title>
	      <style type="text/css" rel="stylesheet">
		body{
		padding:0px;
		margin:0px;
		background-color:#f2f2f2;
		}
		html, body, div, span, applet, object, iframe,
		h1, h2, h3, h4, h5, h6, p, blockquote, pre,
		a, abbr, acronym, address, big, cite, code,
		del, dfn, em, img, ins, kbd, q, s, samp,
		small, strike, strong, sub, sup, tt, var,
		b, u, i, center,
		dl, dt, dd, ol, ul, li,
		fieldset, form, label, legend,
		table, caption, tbody, tfoot, thead, tr, th, td,
		article, aside, canvas, details, embed, 
		figure, figcaption, footer, header, hgroup, 
		menu, nav, output, ruby, section, summary,
		time, mark, audio, video {
		margin: 0px;
		padding: 0px;
		border: 0px;
		font-size: 100%;
		font: inherit;
		vertical-align: top;
		}
	     </style>
           </head>
           <body>';
        $message .='
	    
	    <div style="max-width:840px; height: 100%; margin: auto; background-color:#fff; padding:10px">
		<div style="width:792px; height:auto; margin: auto; border:1px solid #a3a3a3; padding: 0px;">
		   <table width="790" border="0" cellspacing="0" cellpadding="0">
		      <tbody>
			 <tr>
			    <td colspan="3" style="text-align:right">
			       <img src="'.base_url().'/assets/images/emailer/Invoice.png" width="25%"/>
			    </td>
			 </tr>
			 <tr>
			    <td width="350" style="background-color:#f4f2f3" valign="top">
			       <table width="350" border="0" cellspacing="0" cellpadding="0" style="background-color: #fff; border-bottom-right-radius:15px; padding:10px">
				  <tbody>
				     <tr>
					<td style="text-align: left;">';
					if($isplogo != '0'){
					    $message .= '<img src="'.$isplogo.'" width="50%" alt="isplogo" />';
					}else{
					    $message .= '<img src="'.base_url().'assets/images/isp_logo.png" width="50%" alt="isplogo" /> ';
					}
			$message .='	</td>
				     </tr>
				     <tr>
					<td>
					   <p style="color:#00a2e1; text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:600;  margin:5px 0px 0px 0px; padding:0px 0px 0px 10px;">'.$company_name.'</p>
					</td>
				     </tr>
				     <tr>
					<td>
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:5px 0px 0px 0px; padding:0px 0px 0px 10px;">'.$address.'</p>
					</td>
				     </tr>
				     <tr>
					<td>
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:5px 0px 0px 0px; padding:0px 0px 0px 10px;">Ph.No : +91 '.$ispdetailArr['help_number1'].'</p>
					</td>
				     </tr>
				     <tr>
					<td>
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:5px 0px 0px 0px; padding:0px 0px 0px 10px;">E-mail: '.$ispdetailArr['support_email'].'</p>
					</td>
				     </tr>
				     <tr>
					<td>
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:5px 0px 0px 0px; padding:0px 0px 0px 10px;">GSTIN: '.$ispgst_number.'</p>
					</td>
				     </tr>
				     <tr>
					<td>
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:5px 0px 0px 0px; padding:0px 0px 0px 10px;">CIN: '.$ispcin_number.'</p>
					</td>
				     </tr>
				  </tbody>
			       </table>
			    </td>
			    <td width="440" valign="top" style="background-color:#f4f2f3">
			       <table width="440" border="0" cellspacing="0" cellpadding="0" style="padding:20px;border-top-left-radius:15px;background-color:#f4f2f3">
				  <tbody>
				     <tr>
					<td width="281" scope="col">
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;margin:2px 0px 0px 0px; padding:0px 10px">Name : <span>'.ucfirst($user_rowdata->firstname).' '.ucfirst($user_rowdata->lastname).'</span></p>
					</td>
					<td width="159" scope="col">
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">User Id : '.$user_rowdata->uid.'</p>
					</td>
				     </tr>
				     <tr>
					<td>
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">
					      Address : '.$user_rowdata->flat_number.' '. $user_rowdata->address.', '.$city.', '.$state.'	
					   </p>
					</td>
					<td>&nbsp;</td>
				     </tr>
				     <tr>
					<td>
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">
					      GSTIN: '.$gstin_number.'
					   </p>
					</td>
					<td>
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">Invoice No. : '.$bill_number.'</p>
					</td>
				     </tr>
				     <tr>
					<td>
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">
					      HSN/SAC : '.$hsn_sac.'
					   </p>
					</td>
					<td>
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">Invoice Date : '.$bill_date.'</p>
					</td>
				     </tr>
				     <tr>
					<td>
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">Place of supply : '.$state.'</p>
					</td>
					<td>&nbsp;</td>
				     </tr>
				     <tr>
					<td>
					   <p style="color:#000; text-align: left; font-family: Helevetica, sans-serif; font-size:11px; font-weight:600;  margin:2px 0px 0px 0px; padding:0px 10px;">Mobile : '.$mobileno.'</p>
					</td>
					<td>&nbsp;</td>
				     </tr>
				     <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				     </tr>
				      <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				     </tr>
				  </tbody>
			       </table>
			    </td>
			 </tr>
			 <tr style="background-color:#f4f2f3">
			    <td style="padding:10px 10px 0px" colspan="2">
			       <table width="770" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #000; padding:0px; margin:15px 0px; ">
				  <tbody>
				     <tr bgcolor="#f00f64">
					<td width="103" style="border-bottom:1px solid #f00f64; border-top:1px solid #f00f64; border-right:1px solid #ffffff; padding-top:5px; text-align: center;"><span style="font-weight: 600; color:#fff; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px; ">Sr. No.</span></td>
					<td width="338" style="border-bottom:1px solid #f00f64;border-top:1px solid #f00f64; border-right:1px solid #ffffff; padding-top:5px; text-align: center">
					   <span style="font-weight: 600; color:#fff;text-align: center; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px;">Particulars</span>
					</td>
					<td colspan="2" width="109" style="border-bottom:1px solid #f00f64;border-top:1px solid #f00f64; border-right:none;text-align: center;padding-top:5px;text-align: right;">
					   <span style="font-weight: 600; color:#fff; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px;">Amount Due</span>
					</td>
				     </tr>
				     <tr>
					<td style="border-bottom:1px solid #000000; border-right:1px solid #000000; text-align: center ;padding-top:2px; ">
					   <span style="font-weight: 600; color:#000; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px; ">1.</span>
					</td>
					<td style="border-bottom:1px solid #000000; border-right:1px solid #000000;padding-top:2px;">
					   <span style="font-weight: 600; color:#000; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px; ">Package : '.$plandetails['planname'].'</span>
					</td>
					<td colspan="2" style="border-bottom:1px solid #000000; border-right:none; padding-top:2px; padding-right:5px; text-align: right">
					   <span style="font-weight: 600; color:#000; font-family: Helevetica, sans-serif; font-size:12px; padding:5px; margin:5px 0px; "> '.number_format($actual_billamt, 2).'</span>
					</td>
				     </tr>
				     
				     ';
				     
			$message .= $tax_msg . '
				  </tbody>
			       </table>
			       <table width="770" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px; margin:15px 0px 0px 0px; ">
				  <tbody>
				     <tr>
					<td width="331" scope="col">&nbsp;
					</td>
					<td width="437" scope="col" style="background-color: #fff; border-top-left-radius:15px ">
					</td>
				     </tr>';
				     if($ispdetailArr['account_number'] != ''){ 
			$message .=  '<tr>
					<td rowspan="2" valign="top">
					   <table width="406" border="0" cellspacing="0" cellpadding="0">
					      <tbody>
						 <tr>
						    <td width="83">
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Bank Name :</span></p>
						    </td>
						    <td width="323">
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">'.$ispdetailArr['bank_name'].'</p>
						    </td>
						 </tr>
						 <tr>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Branch Add :</span></p>
						    </td>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">'.$ispdetailArr['branch_address'].'</p>
						    </td>
						 </tr>
						 <tr>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Account No :</span></p>
						    </td>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">'.$ispdetailArr['account_number'].'</p>
						    </td>
						 </tr>
						 <tr>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">Acc. Name :</span></p>
						    </td>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">'.$ispdetailArr['account_holder_name'].'</p>
						    </td>
						 </tr>
						 <tr>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">IFSC CODE :</span></p>
						    </td>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">'.$ispdetailArr['ifsc_code'].'</p>
						    </td>
						 </tr>
					      </tbody>
					   </table>
					</td>
					';
				    }else{
			$message .=  '<tr>
					<td rowspan="2" valign="top" >
					   <table width="406" border="0" cellspacing="0" cellpadding="0">
					      <tbody>
						 <tr>
						    <td width="83">
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">&nbsp;</span></p>
						    </td>
						    <td width="323">
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">&nbsp;</p>
						    </td>
						 </tr>
						 <tr>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">&nbsp;</span></p>
						    </td>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">&nbsp;</p>
						    </td>
						 </tr>
						 <tr>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">&nbsp;</span></p>
						    </td>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">&nbsp;</p>
						    </td>
						 </tr>
						 <tr>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">&nbsp;</span></p>
						    </td>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">&nbsp;</p>
						    </td>
						 </tr>
						 <tr>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;"><span style="font-weight: 600">&nbsp;</span></p>
						    </td>
						    <td>
						       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">&nbsp;</p>
						    </td>
						 </tr>
					      </tbody>
					   </table>
					</td>
					';
				    }
			$message .=  '<td style="background-color: #fff; text-align: right">';
					    if($isp_signature != '0'){
						$message .=  '<img src="'.$isp_signature.'" alt="isplogo" />';
					    }
			       $message .= '
				     </td></tr>
				     
				     <tr>
					<td style="padding-right:10px; background-color: #fff" colspan="2">
					   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">Authorised Signatory</p>
					</td>
				     </tr>
				     <tr>
					<td style="border-bottom-right-radius:15px">&nbsp;</td>
					<td style="background-color: #fff">
					   <p style="color:#000;text-align: right; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:10px;">'.$company_name.'</p>
					</td>
				     </tr>
				  </tbody>
			       </table>
			    </td>
			 </tr>
		      </tbody>
		   </table>
		   <table width="770" border="0" cellspacing="0" cellpadding="0" style="padding:10px 0px; margin:10px 10px; ">
		      <tbody>
			 <tr>
			    <td style="border-top-left-radius:4px; border-top-right-radius:4px; background-color: #c1c1c1; padding:10px">
			       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:14px; font-weight:600;  margin:0px; padding:0px;">Terms and conditions:</p>
			    </td>
			 </tr>
			 <tr>
			    <td style="padding:2px 10px">
			       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">1. Please make your payments before the due date to enjoy uninterrupted service of your internet connection. </p>
			    </td>
			 </tr>
			 <tr>
			    <td style="padding:2px 10px">
			       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">2. If the service is discontinued for non-payment of dues, then a reconnection charge may be applicable.</p>
			    </td>
			 </tr>
			 <tr>
			    <td style="padding:2px 10px">
			       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">3. Late payment charges may be applicable after the payment due date.</p>
			    </td>
			 </tr>
			 <tr>
			    <td style="padding:2px 10px">
			       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">4. All invoice disputes should be highlighted within 7 days of invoice generation.</p>
			    </td>
			 </tr>
			 <tr>
			    <td style="padding:2px 10px">
			       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">5. GST charges as applicable and notified by the Govt. of India would be applicable.</p>
			    </td>
			 </tr>
			 <tr>
			    <td style="padding:2px 10px">
			       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">6. All disputes are subject to the courts of '.$state.'</p>
			    </td>
			 </tr>
			 <tr>
			    <td style="padding:2px 10px">
			       <p style="color:#000;text-align: left; font-family: Helevetica, sans-serif; font-size:12px; font-weight:400;  margin:0px; padding:0px;">7. For any queries on the invoice please call '.$helpline.' or write at '.$support_email.'.</p>
			    </td>
			 </tr>
		      </tbody>
		   </table>
		</div>
	     </div>';
        $message .=
            '</body>
        </html>
        ';

        //echo $message; die;
        
        $from = ''; $fromname = '';
        $this->load->library('email');
        $ispaccount = $this->getisp_accounttype($isp_uid);
        $emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	    $from = isset($rowdata->sendfrom) ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = isset($rowdata->sender_name) ? $rowdata->sender_name : $ispdetailArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
      
        $billmonth = date('F');
        if(count($config) > 0){
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject("E-BILL for the $billmonth Month Plan");
            $this->email->message($message);	
            $this->email->send();
        }
        
        return 1;
        
    }
    
    
    public function ill_configuration($isp_uid, $uuid){
	$this->load->library('routerlib');
	$previouslogintype = $this->db->query("SELECT logintype, inactivate_type FROM sht_users WHERE uid='".$uuid."'");
	if($previouslogintype->num_rows() > 0){
	    $rawdata = $previouslogintype->row();
	    $logintype = $rawdata->logintype;
	    $inactivate_type = $rawdata->inactivate_type;
	    
	    if($logintype == 'ill_ipuser'){
		$chk_illassigned = $this->db->query("SELECT * FROM sht_users_ill_ips_assigned WHERE  isp_uid = '".$isp_uid."' AND uid='".$uuid."' AND status='1' LIMIT 1");
		if($chk_illassigned->num_rows() > 0){
		    $nasobj = $chk_illassigned->row();
		    $router_port = '8728';
		    $router_id = $nasobj->nasname;
		    $router_user = $nasobj->username;
		    $router_password = $nasobj->password;
		    
		    if($router_port != ''){
			$router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
		    }else{
			$router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password");	
		    }
		    
		    if($router_conn !== null){
			if(($inactivate_type == 'suspended') || ($inactivate_type == 'terminate')){
			    $router_conn->set("/ip/firewall/address-list",array(".id"=>$uuid."_ILL", "disabled" => "yes"));
			    $router_conn->set("/ip/firewall/filter",array(".id"=>$uuid."_ILL", "disabled" => "yes"));
			}else{
			    $router_conn->set("/ip/firewall/address-list",array(".id"=>$uuid."_ILL", "disabled" => "no"));
			    $router_conn->set("/ip/firewall/filter",array(".id"=>$uuid."_ILL", "disabled" => "no"));
			}
		    }
		}
	    }
	    
	    return 1;
	}
    }
    
    public function delete_temp_billnumber($isp_uid, $bill_number){
	$this->db->delete("sht_temp_billnumber",  array('isp_uid' => $isp_uid, 'bill_number' => $bill_number));
	return 1;
    }
    public function checkunique_billnumber($isp_uid, $bill_number){
	$today_date = date('jny');
	$checkbillQ = $this->db->query("SELECT bill_number FROM sht_temp_billnumber WHERE isp_uid='".$isp_uid."' AND bill_number='".$bill_number."'");
	if($checkbillQ->num_rows() == 0){
	    $this->db->insert("sht_temp_billnumber",  array('isp_uid' => $isp_uid, 'bill_number' => $bill_number));
	    return $bill_number;
	}else{
	    $lastdigits = substr($bill_number, strpos($bill_number, $isp_uid)  + strlen($isp_uid));
	    $lastdigits = ($lastdigits + 1);
	    $bill_number = $today_date.$isp_uid.$lastdigits;
	    return $this->checkunique_billnumber($isp_uid, $bill_number);
	}
    }
    public function generate_billnumber($isp_uid){
	$bill_number = '';
	$today_date = date('jny');
	
	$lastbillnumberQ = $this->db->query("SELECT bill_number FROM `sht_subscriber_billing` WHERE isp_uid='".$isp_uid."' AND (bill_type='montly_pay' OR bill_type='midchange_plancost' OR bill_type='topup') ORDER BY id DESC LIMIT 1");
	if($lastbillnumberQ->num_rows() > 0){
	    $rawdata = $lastbillnumberQ->row();
	    $bill_number = $rawdata->bill_number;
	    if ((strpos($bill_number, $isp_uid)) !== FALSE) {
		$lastdigits = substr($bill_number, strpos($bill_number, $isp_uid)  + strlen($isp_uid));
		$lastdigits = ($lastdigits + 1);
		$bill_number = $today_date.$isp_uid.$lastdigits;
	    }else{
		$bill_number = $today_date.$isp_uid.'00';
	    }
	}else{
	    $bill_number = $today_date.$isp_uid.'00';
	}
	
	$bill_number = $this->checkunique_billnumber($isp_uid, $bill_number);
	return $bill_number;
    }
    /*---- BILLING GENERATION PER CYCLE STARTS ----*/
    public function userbalance_amount($uuid){
        $wallet_amt = 0;
        $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
        if($walletQ->num_rows() > 0){
                $wallet_amt = $walletQ->row()->wallet_amt;
        }
        
        $passbook_amt = 0;
        $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
        if($passbookQ->num_rows() > 0){
                $passbook_amt = $passbookQ->row()->plan_cost;
        }
        $balanceamt = $wallet_amt - $passbook_amt;
        return $balanceamt;
    }

    public function passbook_entry($uuid, $isp_uid, $billid, $srvid, $wallet_amount){
        $passbookArr = array(
	    'isp_uid' => $isp_uid,
	    'subscriber_uuid' => $uuid,
	    'billing_id' => $billid,
	    'srvid' => $srvid,
	    'plan_cost' => $wallet_amount,
	    'added_on'	=> date('Y-m-d H:i:s')
        );
        $this->db->insert('sht_subscriber_passbook', $passbookArr);
        return 1;
    }

    public function user_advpaybalance($uid){
        $query = $this->db->query("SELECT COALESCE(SUM(balance_left),0) as balance_left FROM sht_advance_payments WHERE uid='".$uid."'");
        $advobj = $query->row();
        $total_advpay = $advobj->balance_left;
        return $total_advpay;
    }
    public function update_expirationdate($uid,$user_credit_limit,$plan_cost_perday){
        $user_wallet_balance = $this->userbalance_amount($uid);
        $user_advpaybalance = $this->user_advpaybalance($uid);
        $expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $user_advpaybalance) / $plan_cost_perday);
        $actiondate = date('Y-m-d');
        $expiration_date = date('Y-m-d', strtotime($actiondate . " +".$expiration_acctdays." days"));
        $expiration_date = $expiration_date.' 00:00:00';
        $this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $uid));
        return true;
    }
    public function addtowallet($uuid, $isp_uid, $wallet_amount){
        
        $walletArr = array(
            'isp_uid' => $isp_uid,
            'subscriber_uuid' => $uuid,
            'wallet_amount' => $wallet_amount,
            'added_on'	=> date('Y-m-d H:i:s')
        );
        $this->db->insert('sht_subscriber_wallet', $walletArr);
        $wlastid = $this->db->insert_id();
        
        $wallet_billarr = array(
                'isp_uid' => $isp_uid,
                'subscriber_uuid' => $uuid,
                'wallet_id' => $wlastid,
                'bill_added_on' => date('Y-m-d H:i:s'),
                'receipt_number' => date('jnyHis'),
                'bill_type' => 'addtowallet',
                'payment_mode' => 'cash',
                'actual_amount' => $wallet_amount,
                'total_amount' => $wallet_amount,
                'receipt_received' => '1',
                'alert_user' => '0',
                'bill_generate_by' => $isp_uid,
                'bill_paid_on' => date('Y-m-d H:i:s'),
                'wallet_amount_received' => '1'
        );
        $this->db->insert('sht_subscriber_billing', $wallet_billarr);
        $wlast_billid = $this->db->insert_id();
        
        return 1;
    }

    public function user_advpayments($uid,$billid,$cost_tillnow, $isp_uid){
        $data = array();
        $pending_cost = '0';
        $query = $this->db->query("SELECT id, balance_left, bonus_amount FROM sht_advance_payments WHERE uid='".$uid."' AND (balance_left != '0.00' OR bonus_amount > 0) ORDER BY id ASC");
        $num_rows = $query->num_rows();
        if($query->num_rows() > 0){
            foreach($query->result() as $advobj){
                $balance_left = $advobj->balance_left;
                $advid = $advobj->id;
                $bonus_amount = $advobj->bonus_amount;
                
                if($balance_left >= $cost_tillnow){
                    $newbal_left = $balance_left - $cost_tillnow;
                    $this->db->update('sht_advance_payments', array('balance_left' => $newbal_left), array('id' => $advid));
                    $this->db->update('sht_subscriber_billing', array('payment_mode' => 'cash', 'receipt_received' => '1', 'receipt_number' => 'advancepay', 'is_advanced_paid' => '1', 'bill_comments' => 'Payment Received in Advanced'), array('id' => $billid));
                    // ADD PAYMENT RECEIPT
                    $this->addtowallet($uid, $isp_uid, $cost_tillnow);
                    break;
                }
                elseif(($balance_left < $cost_tillnow) && ($balance_left >= 0)){
                    $pending_cost = $cost_tillnow - $balance_left;
                    if(($bonus_amount >= $pending_cost) && $bonus_amount >= 0){
                        $bonusbal_left = $bonus_amount - $pending_cost;
                        $this->db->update('sht_advance_payments', array('balance_left' => '0.00', 'bonus_amount' => $bonusbal_left), array('id' => $advid));
                        $this->db->update('sht_subscriber_billing', array('payment_mode' => 'cash', 'receipt_received' => '1', 'receipt_number' => 'advancepay', 'is_advanced_paid' => '1', 'bill_comments' => 'Payment Received in Advanced'), array('id' => $billid));
                        // ADD PAYMENT RECEIPT
                        $this->addtowallet($uid, $isp_uid, $cost_tillnow);
                        break;
                    }
                    elseif(($bonus_amount < $pending_cost) && $bonus_amount >= 0){
                        $pending_cost = $pending_cost - $bonus_amount;
                        $this->db->update('sht_advance_payments', array('balance_left' => '0.00', 'bonus_amount' => '0.00'), array('id' => $advid));
                        if($num_rows == 1){
                            $this->db->update('sht_subscriber_billing', array('payment_mode' => '', 'receipt_received' => '0', 'receipt_number' => ''), array('id' => $billid));
                            // ADD PAYMENT RECEIPT
                            $this->addtowallet($uid, $isp_uid, $bonus_amount);
                        }else{
                            $cost_tillnow = $pending_cost;
                        }
                    }
                }
            }
        }
        
        return true;
    }
    
    public function getcustom_planprice($isp_uid, $uuid, $srvid){
        $data = array();
        $customplanQ = $this->db->query("SELECT baseplanid,gross_amt FROM sht_custom_plan_pricing WHERE isp_uid='".$isp_uid."' AND uid='".$uuid."' AND baseplanid='".$srvid."' ORDER BY id DESC LIMIT 1");
        $num_rows = $customplanQ->num_rows();
        if($num_rows > 0){
            $rowdata = $customplanQ->row();
            $data['baseplanid'] = $rowdata->baseplanid;
            $data['gross_amt'] = $rowdata->gross_amt;
        }else{
            $data['baseplanid'] = '';
            $data['gross_amt'] = '';
        }
        return $data;
    }
    
    public function updatecustom_planprice($isp_uid, $uuid, $srvid){
        $customplanQ = $this->db->query("SELECT baseplanid,gross_amt FROM sht_custom_plan_pricing WHERE isp_uid='".$isp_uid."' AND uid='".$uuid."' AND baseplanid='".$srvid."' ORDER BY id DESC LIMIT 1");
        $num_rows = $customplanQ->num_rows();
        if($num_rows > 0){
            $this->db->delete("sht_custom_plan_pricing", array('uid' => $uuid, 'isp_uid' => $isp_uid, 'action' => 'Now'));
            $this->db->update("sht_custom_plan_pricing", array('action' => 'Now', 'added_on' => date('Y-m-d H:i:s')), array('uid' => $uuid, 'isp_uid' => $isp_uid, 'baseplanid' => $srvid, 'action' => 'NextCycle'));
        }
    }
    
    public function bill_generation(){
        ini_set('max_execution_time', '0');

        $today = date('d');
        $today_date = date('Y-m-d');        
        $date = new DateTime($today_date);
        //$date->modify('+1 day');
        $nextdaydate = $date->format('d');
        $nextdaymonth = $date->format('m');
	$nextdayyear = $date->format('Y');
        
        //$nextdate = new DateTime('+1 day');
        //$nextdaydate = $nextdate->format('d');
        
        $bill_cycleQ = $this->db->query("SELECT tb1.* FROM sht_billing_cycle as tb1 LEFT JOIN sht_isp_admin as tb2
 ON(tb1.isp_uid = tb2.isp_uid) WHERE tb2.decibel_account = 'paid'");
        if($bill_cycleQ->num_rows() > 0){
            foreach($bill_cycleQ->result() as $bcobj){
                $billuserArr = array();
                $billing_cycle = sprintf("%02d",$bcobj->billing_cycle);
		if($bcobj->billing_due_date != '0'){
		    $bill_duedate = date('Y-m-d', strtotime($today_date . " +".$bcobj->billing_due_date." days"));
		}else{
		    $bill_duedate = '0000-00-00';
		}
                $isp_uid = $bcobj->isp_uid;
                
                if($billing_cycle == $nextdaydate){
                    //echo $isp_uid .'==>'. $billing_cycle .'==>'. $nextdaydate; echo '<br/><br/>'; die;
                    
                    $subscriberQ = $this->db->query("SELECT tb1.planautorenewal, tb1.uid, tb1.firstname, tb1.lastname, tb1.email, tb1.state, tb1.city, tb1.mobile, tb1.address, tb1.baseplanid, tb1.account_activated_on as active_on, tb1.paidtill_date, DATE(tb1.expiration) as expiration_date, tb1.plan_activated_date, tb1.user_credit_limit, tb1.inactivate_type, tb1.suspended_days, tb2.srvname, tb2.descr, tb2.datacalc, tb2.datalimit, tb2.plan_duration, tb3.gross_amt, tb3.tax, tb3.net_total, tb3.gross_amt FROM sht_users as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid = tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb1.enableuser='1' AND tb1.expired='0' AND tb1.isp_uid='".$isp_uid."' AND tb1.user_plan_type='postpaid'");
                    // AND tb2.plantype='".$plan_type."'
                    //echo $this->db->last_query(); echo '<br/>'; die;
                    if($subscriberQ->num_rows() > 0){
                        $this->check_updated_plandetails($isp_uid);
                        foreach($subscriberQ->result() as $subsobj){
                            $monthly_pay = 0;
                            $uid = $subsobj->uid;
                            $billuserArr[] = $uid;
			    
			    //CHECK FOR BILL ALREADY GENERATED
			    $checkforcronQ = $this->db->query("SELECT id FROM sht_subscriber_billing_cron WHERE isp_uid='".$isp_uid."' AND uid='".$uid."' AND DATE(added_on) = '".$today_date."'");
			    if($checkforcronQ->num_rows() == 0){
				$this->db->insert('sht_subscriber_billing_cron', array('isp_uid' => $isp_uid, 'uid' => $uid, 'added_on' => date('Y-m-d H:i:s')));

				//if($uid == '10102231'){
				    //echo 'hjhj'; die;
				    $plan_activated_date = $subsobj->plan_activated_date;
				    $tax = $this->current_taxapplicable($isp_uid);
				    $plan_duration = $subsobj->plan_duration;
				    $bill_cycle = sprintf("%02d", $bcobj->billing_cycle);
				    $bill_date = $nextdayyear.'-'.$nextdaymonth.'-'.$bill_cycle;
				    $srvid = $subsobj->baseplanid;
				    $billtype = 'montly_pay';
				    $plandata_calculatedon = $subsobj->datacalc;
				    $plandatalimit = $subsobj->datalimit;
				    $gross_amt = $subsobj->gross_amt;
				    $planautorenewal = $subsobj->planautorenewal;
				    
				    $nasipQ = $this->db->query("SELECT tb1.radacctid,tb1.username, tb1.nasipaddress, tb1.framedipaddress, tb2.secret FROM radacct as tb1 INNER JOIN nas as tb2 ON(tb1.nasipaddress=tb2.nasname) WHERE tb1.username='".$uid."' ORDER BY radacctid DESC LIMIT 1");
				    if($nasipQ->num_rows() > 0){
					$nasrowdata = $nasipQ->row();
					$radacctid = $nasrowdata->radacctid;
					$username = $nasrowdata->username;
					$userframedipaddress = $nasrowdata->framedipaddress;
					$usernasipaddress = $nasrowdata->nasipaddress;
					$secret = $nasrowdata->secret;
					//@exec("echo User-Name:=$username,Framed-Ip-Address=$userframedipaddress | radclient -x $usernasipaddress:3799 disconnect $secret");
					$this->disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret);
					$this->db->query("UPDATE radacct SET acctstoptime='".date('Y-m-d H:i:s')."' WHERE username='".$uid."' AND acctstoptime IS NULL");
				    }
				    
				    $custom_planpriceArr = $this->getcustom_planprice($isp_uid, $uid, $srvid);
				    $custom_planprice = $custom_planpriceArr['gross_amt'];
				    if($custom_planprice != ''){
					$gross_amt = round($custom_planprice / $plan_duration);
					$plan_price = round($custom_planprice);
				    }else{
					$gross_amt = round($gross_amt + (($gross_amt * $tax)/100));
					$plan_price = ($gross_amt * $plan_duration);	
				    }
				    
				    $plan_tilldays = ($plan_duration * 30);
				    //$user_credit_limit = $subsobj->user_credit_limit;
				    $user_credit_limit = ($plan_price + round($plan_price/2));
				    //$paidtill_date = $subsobj->paidtill_date;
				    
				    $pacttimestamp = strtotime($plan_activated_date);
				    $todtimestamp = strtotime($today_date);
				    $used_netdays = round(abs($todtimestamp - $pacttimestamp) / 86400);
				    $plan_cost_perday = ($plan_price/$plan_tilldays);
				    if($used_netdays > 27){
					if($custom_planprice != ''){
					    $actual_bill_amt = round(($custom_planprice * 100) / 118);
					}else{
					    $actual_bill_amt = round($subsobj->gross_amt);
					}
					$cost_tillnow = round($plan_price);    
				    }else{
					if($custom_planprice != ''){
					    $gross_plan_cost_perday = ($custom_planprice / 30);
					    $actual_bill_amt = round($gross_plan_cost_perday * $used_netdays);
					}else{
					    $gross_plan_cost_perday = ($subsobj->gross_amt / 30);
					    $actual_bill_amt = round($gross_plan_cost_perday * $used_netdays);
					}
					$cost_tillnow = round($plan_cost_perday * $used_netdays);
				    }
				    
				    $billnumber = $this->generate_billnumber($isp_uid);
				    $billingarr = array(
					'subscriber_uuid' => $uid,
					'plan_id' => $srvid,
					'user_plan_type' => 'postpaid',
					'bill_starts_from' => "$plan_activated_date",
					'bill_added_on' => $bill_date.' '. date('H:i:s'),
					'bill_duedate' => $bill_duedate,
					'bill_number' => $billnumber,
					'bill_type' => $billtype,
					'payment_mode' => '',
					'actual_amount' => $actual_bill_amt,
					'discount' => '0',
					'total_amount' => $cost_tillnow,
					'isp_uid' => $isp_uid
				    );
				    //print_r($billingarr); die;
				    $this->db->insert('sht_subscriber_billing', $billingarr);
				    $billid = $this->db->insert_id();
				    
				    $this->delete_temp_billnumber($isp_uid, $billnumber);
				    
				    //Bill Passbook Entry
				    $this->passbook_entry($uid, $isp_uid, $billid, $srvid, $cost_tillnow);
				    
				    $this->db->insert('sht_userplan_datechange_records', array('uid' => $uid, 'isp_uid' => $isp_uid, 'bill_id' => $billid, 'srvid' => $srvid, 'lastplan_activatedate' => $plan_activated_date, 'added_on' => date('Y-m-d H:i:s')));
				    
				    //Check for Advanced Payment
				    $user_advpayments = $this->user_advpayments($uid, $billid, $cost_tillnow, $isp_uid);
				    //Update Expiry Date
				    $this->update_expirationdate($uid,$user_credit_limit,$plan_cost_perday);
				    
				    $next_bill_date = date('Y-m-d', strtotime($bill_date.' +1 month'));
				    
				    $this->db->update('sht_users', array('downlimit' => '0', 'comblimit' => '0', 'next_bill_date' => $next_bill_date, 'bill_duedate' => $bill_duedate), array('uid' => $uid));
    
				    /*
				     *  CHECK FOR PLAN AUTO RENEWAL
				     */
				    if($planautorenewal == '0'){
					$this->db->update('sht_users', array('enableuser' => '0', 'inactivate_type' => 'suspended', 'suspended_days' => '0', 'suspendedon' => date('Y-m-d')), array('uid' => $uid));
				    }else{ 
					//CHECK FOR NEXT CYCLE PLAN
					$plandatarr = array();
					$plandatarr['enableuser'] = '1';
					$plandatarr['inactivate_type'] = '';
					$plandatarr['suspended_days'] = '0';
					$plandatarr['suspendedon'] = '0000-00-00';
					$plandatarr['user_credit_limit'] = $user_credit_limit;
					$plandatarr['plan_activated_date'] = $bill_date;
					$plandatarr['plan_changed_datetime'] = date('Y-m-d H:i:s');
					$nextPlanQ = $this->db->query("SELECT tb1.id, tb1.baseplanid, tb1.user_plan_type, tb2.datacalc, tb2.datalimit, tb2.plan_duration, tb3.net_total, tb3.gross_amt FROM sht_nextcycle_userplanassoc as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid = tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE uid='".$uid."' AND status='0' ORDER BY id DESC LIMIT 1");
					if($nextPlanQ->num_rows() > 0){
					    $nextPlan_rowdata = $nextPlanQ->row();
					    $nid = $nextPlan_rowdata->id;
					    $newsrvid = $nextPlan_rowdata->baseplanid;
					    $newsrvid_calcon = $nextPlan_rowdata->datacalc;
					    $newsrvid_datalimt = $nextPlan_rowdata->datalimit;
					    $newuser_plan_type = $nextPlan_rowdata->user_plan_type;
					    
					    $newplan_duration = $nextPlan_rowdata->plan_duration;
					    $npgross_amt = $nextPlan_rowdata->gross_amt;
					    $nextplan_tilldays = ($newplan_duration * 30);
	    
					    $custom_planpriceArr = $this->getcustom_planprice($isp_uid, $uid, $newsrvid);
					    $custom_planprice = $custom_planpriceArr['gross_amt'];
					    if($custom_planprice != ''){
						$this->updatecustom_planprice($isp_uid, $uid, $newsrvid);
						//$actual_npgross_amt = round($custom_planprice / $newplan_duration);
						$actual_npgross_amt = round(($custom_planprice * 100) / 118);
						$nextsrvid_price = round($custom_planprice);
					    }else{
						$actual_npgross_amt = $nextPlan_rowdata->gross_amt;
						$npgross_amt = round($npgross_amt + (($npgross_amt * $tax)/100));
						$nextsrvid_price = ($npgross_amt * $newplan_duration);
					    }
					    
					    $nphalf_gross_amt = round($nextsrvid_price / 2);
					    $nextsrvid_cost_perday = round($nextsrvid_price/$nextplan_tilldays);
					    $user_newcreditlimit = ($nextsrvid_price + $nphalf_gross_amt);
					    $plandatarr['user_credit_limit'] = $user_newcreditlimit;
	    
					    // CHANGE TO PREPAID PLAN
					    if($newuser_plan_type == 'prepaid'){
						$user_walletamt = $this->userbalance_amount($uid); //check again wallet balance
						$payment_mode = ''; $receipt_received = '0'; $adjusted_amount = 0;
						
						$billnumber = $this->generate_billnumber($isp_uid);
						$prebillingarr = array(
							'subscriber_uuid' => $uid,
							'plan_id' => $newsrvid,
							'user_plan_type' => 'prepaid',
							'bill_added_on' => $bill_date.' '.date('H:i:s'),
							'bill_duedate' => $bill_duedate,
							'bill_number' => $billnumber,
							'bill_type' => 'montly_pay',
							'payment_mode' => '',
							'receipt_received' => '0',
							'actual_amount' => $actual_npgross_amt,
							'adjusted_amount' => '0',
							'discount' => '0',
							'total_amount' => $nextsrvid_price,
							'isp_uid' => $isp_uid
						);
						$this->db->insert('sht_subscriber_billing', $prebillingarr);
						$prebillid = $this->db->insert_id();
						$prePassbookArr = array(
							'isp_uid' => $isp_uid,
							'subscriber_uuid' => $uid,
							'billing_id' => $prebillid,
							'srvid' => $newsrvid,
							'plan_cost' => $nextsrvid_price,
							'added_on'	=> $bill_date.' '.date('H:i:s')
						);
						$this->db->insert('sht_subscriber_passbook', $prePassbookArr);
						
						$this->delete_temp_billnumber($isp_uid, $billnumber);
						
						//$pnext_bill_date = date('Y-m-d',strtotime("+$nextplan_tilldays day"));
						$pnextdate = date('Y-m-d', strtotime('+1 month'));
						$pnextdate = new DateTime($pnextdate);
						$pnextdate->setDate($pnextdate->format('Y'), $pnextdate->format('m'), $bcobj->billing_cycle);
						$pnext_month = $pnextdate->format('Y-m-d');
						$pnext_bill_date = $pnext_month;
						$plandatarr['next_bill_date'] = $pnext_bill_date;
					    }
					    
					    $plandatarr['user_plan_type'] = $newuser_plan_type;
					    if($newsrvid_calcon == '1'){
						$plandatarr['downlimit'] = $newsrvid_datalimt;
						$plandatarr['comblimit'] = '0';
					    }elseif($newsrvid_calcon == '2'){
						$plandatarr['downlimit'] = '0';
						$plandatarr['comblimit'] = $newsrvid_datalimt;
					    }else{
						$plandatarr['downlimit'] = '0';
						$plandatarr['comblimit'] = '0';
					    }
					    $plandatarr['baseplanid'] = $newsrvid;                                
					    $this->db->update('sht_users', $plandatarr, array('uid' => $uid));
					    
					    //SET BONUS AMT & Balance Amt TO 0 OF PREVIOUS PLAN & Add Amt to Wallet.
					    $total_advpay = $this->user_advpaybalance($uid);
					    if($total_advpay > 0){
						$this->addtowallet($uid, $isp_uid, $total_advpay);
						$this->db->update('sht_advance_payments', array('bonus_amount' => '0', 'balance_left' => '0'), array('uid' => $uid, 'srvid' => $srvid));
					    }
					    $this->update_expirationdate($uid,$user_newcreditlimit,$nextsrvid_cost_perday);
					    $this->db->update('sht_nextcycle_userplanassoc', array('status' => '1'), array('id' => $nid));
					    
					}else{
					    if($plandata_calculatedon == '1'){
						$plandatarr['downlimit'] = $plandatalimit;
						$plandatarr['comblimit'] = '0';
					    }elseif($plandata_calculatedon == '2'){
						$plandatarr['downlimit'] = '0';
						$plandatarr['comblimit'] = $plandatalimit;
					    }else{
						$plandatarr['downlimit'] = '0';
						$plandatarr['comblimit'] = '0';
					    }
					    
					    $this->db->update('sht_users', $plandatarr, array('uid' => $uid));
					}
				    }
				    $custfupQ = $this->db->query("SELECT id FROM sht_customize_plan_speed WHERE uid='".$uid."' AND isp_uid='".$isp_uid."' AND status='1'");
				    if($custfupQ->num_rows() > 0){
					$this->db->update('sht_customize_plan_speed', array('status' => '0', 'modified_on' => date('Y-m-d H:i:s'), 'modified_by' => 'SuperAdmin'), array('uid' => $uid));
				    }
				    $this->db->update('sht_usertopupassoc', array('status' => '0', 'terminate' => '1'), array('uid' => $uid, 'topuptype' => '1'));
				    //sleep(60);
				    
				    //$wallet_bal = $this->userbalance_amount($uid);                                
				    //$this->mail_format($uid);
				//}
			    }
			    sleep(3);
                        }
                    }
                    
                    //Send mail to ISP
                    /*if(count($billuserArr) > 0){
                        $this->mailtoisp_format($billuserArr);
                    }*/
                }
                
                sleep(5);
            }
            
        }
    }
    
    public function check_updated_plandetails($isp_uid){
        $plandetailsQ = $this->db->query("SELECT id, downrate, uprate, datalimit, timelimit, fupuprate, fupdownrate, enableburst, dlburstlimit, ulburstlimit, dlburstthreshold, ulburstthreshold, priority, bursttime, is_private, plan_duration, srvid FROM sht_plannextcycle WHERE isp_uid='".$isp_uid."'");
        if($plandetailsQ->num_rows() > 0){
            foreach($plandetailsQ->result() as $plandataobj){
                $plandetarr = array(
                    'downrate' => $plandataobj->downrate,
                    'uprate' => $plandataobj->uprate,
                    'datalimit' => $plandataobj->datalimit,
                    'timelimit' => $plandataobj->timelimit,
                    'fupuprate' => $plandataobj->fupuprate,
                    'fupdownrate' => $plandataobj->fupdownrate,
                    'enableburst' => $plandataobj->enableburst,
                    'dlburstlimit' => $plandataobj->dlburstlimit,
                    'ulburstlimit' => $plandataobj->ulburstlimit,
                    'dlburstthreshold' => $plandataobj->dlburstthreshold,
                    'ulburstthreshold' => $plandataobj->ulburstthreshold,
                    'priority' => $plandataobj->priority,
                    'bursttime' => $plandataobj->bursttime,
                    'is_private' => $plandataobj->is_private,
                    'plan_duration' => $plandataobj->plan_duration,
                    'planchange_isnextcycle' => ''
                );
                
                $srvid = $plandataobj->srvid;
                $pid = $plandataobj->id;
                $this->db->update('sht_services', $plandetarr, array('srvid' => $srvid));
                $this->db->delete('sht_plannextcycle', array('id' => $pid));
            }
        }
        return 1;
    }
    
    public function mailtoisp_format($billuserArr){
        //echo '<pre>'; print_r($billuserArr);
        $sno = 1; $gen = ''; $isp_uid = ''; $company_name=''; $address=''; $helpline=''; $support_email='';

        foreach($billuserArr as $uuid){
            $user_rowdata = $this->getcustomer_data($uuid);
            $state = $this->getstatename($user_rowdata->state);
            $city = $this->getstatename($user_rowdata->city);
            $zone = $this->getstatename($user_rowdata->zone);
            $billingdata = $this->user_billing_listing($uuid, 'montly_pay');
            $bill_number = $billingdata->bill_number;
            $payment_mode = $billingdata->payment_mode;
            $amount = $billingdata->actual_amount;
            $discount = $billingdata->discount;
            $total_amount = $billingdata->total_amount;
            $plandetails = $this->getplan_details($billingdata->plan_id);
            $bill_date = date('d/m/Y');
            $email = $user_rowdata->email;
            $isp_uid = $user_rowdata->isp_uid;
            
            $ispdetailArr = $this->isp_details($isp_uid);
            $company_name = $ispdetailArr['company_name'];
            $address = $ispdetailArr['address1'];
            $helpline = $ispdetailArr['help_number1'];
            $support_email = $ispdetailArr['support_email'];
            
            $gen .= '<tr align="center" bgcolor="#FFFFFF">
                        <td width="10%" style="font-family: Open Sans, sans-serif; font-size:12px;border-right: 1px solid #F00F64;border-top: 1px solid #F00F64;">'.$sno.'</td>
                        <td width="10%" style="font-family: Open Sans, sans-serif; font-size:12px;border-right: 1px solid #F00F64;border-top: 1px solid #F00F64;">'.$bill_number.'</td>
                        <td width="21%" style="font-family: Open Sans, sans-serif; font-size:12px;border-top: 1px solid #F00F64;">Payment for subscriber - '.$user_rowdata->uid.'</td>
                        <td width="11%" style="font-family: Open Sans, sans-serif; font-size:12px;border-right: 1px solid #F00F64;border-top: 1px solid #F00F64;">'.ucfirst($user_rowdata->firstname).' '.ucfirst($user_rowdata->lastname).'</td>
                        <td width="11%" style="font-family: Open Sans, sans-serif; font-size:12px;border-right: 1px solid #F00F64;border-top: 1px solid #F00F64;">'.$user_rowdata->mobile.'</td>
                        <td width="27%" style="font-family: Open Sans, sans-serif; font-size:12px;border-right: 1px solid #F00F64;border-top: 1px solid #F00F64;">'.$plandetails->srvname.'</td>
                        <td width="12%" align="right" style="font-family: Open Sans, sans-serif; font-size:12px;border-right: 1px solid #F00F64;border-top: 1px solid #F00F64;">&#8377; '.$total_amount.'</td>
                    </tr>';
                    
            $sno++;
        }
            
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" /><meta name="apple-mobile-web-app-capable" content="yes" /></head><body style="margin: 0; padding: 0;">
        <table width="920" align="center" style="border:1px solid #A3A3A3;" cellpadding="0" cellspacing="0">
           <tr>
              <td>
                 <table width="911" align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                       <td width="219" valign="top">
                          <table width="100%" border="0" style="border-bottom-right-radius:15px; background-color:#FFFFFF;">
                             <tr>
                                <td height="30" width="50px;" />
                             </tr>
                             <tr>
                                <td valign="top" align="left" style="padding-left:20px;">';
                                $isplogo = $this->get_isplogo($isp_uid);
                                if($isplogo != '0'){
                                   $message .=  '<img src="'.$isplogo.'" class="img-responsive" width="20%"/>';
                                }else{
                                   $message .= '<img src="'.base_url().'assets/images/isp_logo.png" class="img-responsive" width="20%"/>';
                                }
        $message .=             '</td>
                             </tr>
                             <tr>
                                <td style="font-family: Open Sans, sans-serif; font-size:12px; padding-left:20px;font-weight:bold;">
                                   <span style="color:#F00F64;font-weight:bold;"><br /> '.$company_name.',</span><br />
                                   <table width="100%" border="0">
                                      <tr>
                                         <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:bold;">'.$address.',</td>
                                      </tr>
                                      <tr>
                                         <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:bold;">Ph.No : +91 '.$helpline.'</td>
                                      </tr>
                                      <tr>
                                         <td style="font-family: Open Sans, sans-serif; font-size:12px;font-weight:bold;">E-mail : '.$support_email.' </td>
                                      </tr>
                                   </table>
                                </td>
                             </tr>
                          </table>
                       </td>
                    </tr>
                    <tr>
                       <td colspan="2" bgcolor="#F4F2F3" height="20px" />
                    </tr>
                    <tr>
                       <td colspan="2" bgcolor="#F4F2F3">
                          <table width="100%" border="0" cellpadding="5px;" cellspacing="0px;" style=" border-top-left-radius:5px;border-top-right-radius:5px;border: 1px solid #F00F64;">
                             <tr style="background-color:#F00F64; color:#FFFFFF; font-weight:700;" align="center" height="25px;">
                                <td style="font-family: Open Sans, sans-serif; font-size:12px;border-right:1px solid white;border-bottom:1px  solid white;">S.No.</td>
                                <td style="font-family: Open Sans, sans-serif; font-size:12px;border-right:1px solid white;border-bottom:1px  solid white;">Bill Number</td>
                                <td style="font-family: Open Sans, sans-serif; font-size:12px;border-right:1px solid white;border-bottom:1px  solid white;">Customer ID</td>
                                <td style="font-family: Open Sans, sans-serif; font-size:12px;border-right:1px solid white;border-bottom:1px  solid white;">Customer Name</td>
                                <td style="font-family: Open Sans, sans-serif; font-size:12px;border-right:1px solid white;border-bottom:1px  solid white;">Customer Mobile</td>
                                <td style="font-family: Open Sans, sans-serif; font-size:12px;border-right:1px solid white;border-bottom:1px  solid white;">Active Plan</td>
                                <td style="font-family: Open Sans, sans-serif; font-size:12px;border-right:1px solid white;border-bottom:1px  solid white;">Total</td>
                                <td style="font-family: Open Sans, sans-serif; font-size:12px;border-bottom:1px  solid white;">Remarks</td>
                             </tr>';
            $message .=     $gen. '</table>
                       </td>
                    </tr>
                    <tr>
                       <td colspan="2" style="background-color:#F4F2F3;"><br /></td>
                    </tr>                        
                    <tr bgcolor="#F4F2F3">
                       <td colspan="2" height="20px;" />
                    </tr>
                    <tr bgcolor="#F4F2F3">
                       <td colspan="2" height="20px;" />
                    </tr>
                 </table>
              </td>
           </tr>
           <tr bgcolor="#F4F2F3">
              <td height="10px;" />
           </tr>
        </table>
        </body>
        </html>
        ';
 
        //echo $message; die;
        $this->load->library('email');
        $from = SMTP_USER;
        $fromname = 'DECIBEL';
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => SMTP_HOST,
            'smtp_port' => SMTP_PORT,
            'smtp_user' => SMTP_USER,
            'smtp_pass' => SMTP_PASS,
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1',
            'wordwrap'  => FALSE,
            'crlf'      => "\r\n",
            'newline'   => "\r\n"
        );
        
        //Initialise CI mail
        $this->email->initialize($config);
        $this->email->from($from, $fromname);
        $this->email->to($support_email);
        //$this->email->cc('rajiv@shouut.com,praveer@shouut.com,vikas@shouut.com,prashant@shouut.com,deepak@shouut.com,sandeep@shouut.com');
        //$this->email->cc('praveer@shouut.com,vikas@shouut.com');
        $this->email->subject('MONTHLY PLAN USER BILLING');
        $this->email->message($message);	
        $this->email->send();
        return 1;
    }
    
    public function disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret){
        $data = array("apikey" => "n|A~ok4Y3D>&{U&S(A@G", 'username' => $username, 'userframedipaddress' => $userframedipaddress, 'usernasipaddress' => $usernasipaddress, 'secret' => $secret);
        $data_string = array('requestData' => json_encode($data));
        $ch = curl_init('http://103.20.213.150/decibel/decibelapis/disconnect_user_session');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($ch);
        if ($curl_response === false) {
            $info = curl_getinfo($ch);
            curl_close($ch);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($ch);
        return $curl_response;
    }
    
    /*---------------- TOPUP EXPIRATION FUNCTIONS -----------------------------*/
    public function check_topup_expiration(){
        $today_date = date('Y-m-d H:i:s');
        $query = $this->db->query("SELECT id,topup_enddate FROM sht_usertopupassoc WHERE status='1' AND terminate='0' AND (topuptype='2' OR topuptype='3') AND curdate() > date(topup_enddate)");
        if($query->num_rows() > 0){
            foreach($query->result() as $topobj){
                $id = $topobj->id;
                //$topup_enddate = $topobj->topup_enddate.' 23:59:59';
                //if(strtotime($today_date) == strtotime($topup_enddate)){
                    $this->db->update('sht_usertopupassoc', array('status' => '0', 'terminate' => '1'), array('id' => $id));
                //}
            }
        }
    }
    
    /*------------ CHECK FOR TEMPORARILY SUSPENSION ------------------------*/
    
    public function activate_tempsuspended_users(){
        $today_date = date('Y-m-d');
        $query = $this->db->query("SELECT uid,suspended_days,suspendedon FROM sht_users WHERE inactivate_type='suspended'");
        if($query->num_rows() > 0){
            foreach($query->result() as $qobj){
                $suspendedon = $qobj->suspendedon;
                $suspended_days = $qobj->suspended_days;
                $uuid = $qobj->uid;
                
                $suspended_till = date('Y-m-d', strtotime("+$suspended_days days"));
                if(strtotime($today_date) == strtotime($suspended_till)){
                    $this->db->update('sht_users', array('enableuser' => '1', 'inactivate_type' =>'', 'suspended_days' => '0', 'suspendedon' => '0000-00-00'), array('uid' => $uuid));
                }
                
            }
        }
        
    }
    
    
    public function current_taxapplicable($isp_uid){
        $query = $this->db->query("SELECT tax FROM sht_tax WHERE isp_uid='".$isp_uid."'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return $rowdata->tax;
        }
    }
    public function plan_pricing($srvid, $isp_uid){
        $planQ = $this->db->query("SELECT gross_amt FROM sht_plan_pricing WHERE srvid='".$srvid."' ");
        if($planQ->num_rows() > 0){
            $rowdata = $planQ->row();
            $tax = $this->current_taxapplicable($isp_uid);
            $gross_amt = $rowdata->gross_amt;
            $planprice = round($gross_amt + (($gross_amt * $tax)/100));
            
            return $planprice;
        }
    }
    public function updated_expirydates(){
        $user_currplanQ = $this->db->query("SELECT uid, baseplanid, user_credit_limit, isp_uid FROM sht_users WHERE isp_uid='130' AND enableuser='1'");
        $numrows = $user_currplanQ->num_rows();
        if($numrows > 0){
            foreach($user_currplanQ->result() as $uobj){
                $uid = $uobj->uid;
                $baseplanid = $uobj->baseplanid;
                $credit_limit = $uobj->user_credit_limit;
                $isp_uid = $uobj->isp_uid;
                $plan_pricing = $this->plan_pricing($baseplanid, $isp_uid);
                $plan_cost_perday = round($plan_pricing / 30);
                
		echo $uid. '=>'. $plan_pricing. '=>'. $plan_cost_perday. '<br/>';
		//$this->update_expirationdate($uid,$credit_limit,$plan_cost_perday);
            }
        }
    }
   
    public function updated_bill_amount(){
	ini_set('max_execution_time', '0');
        $billuserArr = array();
        $lastbillQ = $this->db->query("SELECT id, isp_uid, plan_id, subscriber_uuid, bill_starts_from, DATE(bill_added_on) as last_billdate, discounttype, discount FROM sht_subscriber_billing WHERE bill_type='montly_pay'  AND DATE(bill_added_on) = '2018-10-01' AND isp_uid = '100' ORDER BY id DESC");
        if($lastbillQ->num_rows() > 0){
            foreach($lastbillQ->result() as $ldataobj){
                $uid = $ldataobj->subscriber_uuid;
                //if($uid == '10002304'){
                    $isp_uid = $ldataobj->isp_uid;
		    $srvid = $ldataobj->plan_id;
		    $bill_starts_from = $ldataobj->bill_starts_from;
		    $bill_added_on = $ldataobj->last_billdate;
		    $pacttimestamp = strtotime($bill_starts_from);
		    $todtimestamp = strtotime($bill_added_on);
		    $used_netdays = round(abs($todtimestamp - $pacttimestamp) / 86400);
		    
		    $custom_planpriceArr = $this->getcustom_planprice($isp_uid, $uid, $srvid);
		    $custom_planprice = $custom_planpriceArr['gross_amt'];
		    if($custom_planprice != ''){
			$gross_plan_cost_perday = ($custom_planprice / 30);
			$actual_amount = round($gross_plan_cost_perday * $used_netdays);
		    }else{
			$checkplanpriceQ = $this->db->query("SELECT tb1.gross_amt FROM `sht_plan_pricing` as tb1 LEFT JOIN `sht_services` as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb2.`srvid` = '".$srvid."'");
			$srvidobj = $checkplanpriceQ->row();
			$gross_plan_cost_perday = ($srvidobj->gross_amt / 30);
			$actual_amount = round($gross_plan_cost_perday * $used_netdays);
		    }
		    
		    $billdiscounttype = $ldataobj->discounttype;
		    $total_discount = $ldataobj->discount;
		    if($billdiscounttype == 'percent'){
			$netamt = $actual_amount - (($actual_amount * $total_discount)/100);
		    }else{
			$netamt = ($actual_amount - $total_discount);
		    }
		    
		    $total_billamt = round($netamt + ($netamt * 0.18));
		    
		    $this->db->update('sht_subscriber_billing', array('actual_amount' => $actual_amount, 'total_amount' => $total_billamt) ,array('id' => $ldataobj->id));
		    $this->db->update('sht_subscriber_passbook', array('plan_cost' => $total_billamt) ,array('billing_id' => $ldataobj->id, 'subscriber_uuid' => $uid));
		    echo $uid . ' => updated'; echo '<br/>';
               // }
            }
        }
    }
    
    public function update_plan_changed_datetime(){
        $plandatarr = array();
        $query = $this->db->query("SELECT * FROM (SELECT bill_added_on,subscriber_uuid FROM sht_subscriber_billing WHERE bill_type='montly_pay'  ORDER BY id DESC) as temp GROUP BY subscriber_uuid");
        if($query->num_rows() > 0){
            $i = 0;
            foreach($query->result() as $pobj){
                $uuid = $pobj->subscriber_uuid;
                $bill_added_datetime = $pobj->bill_added_on;
                $bill_added_date = date('Y-m-d', strtotime($pobj->bill_added_on));
                
                $userQ = $this->db->query("SELECT plan_activated_date FROM sht_users WHERE uid='".$uuid."'");
                $prevplandate = $userQ->row()->plan_activated_date;
                
                $plandatarr[$i]['uid'] = $uuid;
                $plandatarr[$i]['prevplandate'] = $prevplandate;
                $plandatarr[$i]['plan_activated_date'] = $bill_added_date;
                $plandatarr[$i]['plan_changed_datetime'] = $bill_added_datetime;
                //$this->db->update('sht_users', array('plan_activated_date' => $bill_added_date, 'plan_changed_datetime' => $bill_added_datetime), array('uid' => $uuid));
                
                $i++;
            }
            
            echo '<pre>';print_r($plandatarr); echo '</pre>';
        }
    }

    /*---------------- PREPAID USER BILLING ---------------------------*/
    
    public function isp_billing_date($isp_uid){
        $billing_cycle = '';
        $query = $this->db->query("SELECT billing_cycle FROM sht_billing_cycle WHERE isp_uid='".$isp_uid."'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            $billing_cycle = $rowdata->billing_cycle;
        }
        return $billing_cycle;
    }
    public function prepaid_bill_generation(){
        ini_set('max_execution_time', '0');
        $today = date('d');
        $today_date = date('Y-m-d');        
        $date = new DateTime($today_date);
        //$date->modify('+1 day');
        $nextdaydate = $date->format('Y-m-d');
        
        //$nextdate = new DateTime('+1 day');
        //$nextdaydate = $nextdate->format('Y-m-d');
        
        //$bill_cycleQ = $this->db->query("SELECT isp_uid FROM sht_isp_admin WHERE decibel_account = 'paid'");
	$bill_cycleQ = $this->db->query("SELECT tb1.isp_uid, tb1.billing_due_date FROM sht_billing_cycle as tb1 LEFT JOIN sht_isp_admin as tb2 ON(tb1.isp_uid = tb2.isp_uid) WHERE tb2.decibel_account = 'paid'");
        if($bill_cycleQ->num_rows() > 0){
            foreach($bill_cycleQ->result() as $bcobj){
                $isp_uid = $bcobj->isp_uid;
		if($bcobj->billing_due_date != '0'){
		    $bill_duedate = date('Y-m-d', strtotime($today_date . " +".$bcobj->billing_due_date." days"));
		}else{
		    $bill_duedate = '0000-00-00';
		}
		
                $subscriberQ = $this->db->query("SELECT tb1.planautorenewal, tb1.next_bill_date, tb1.user_plan_type, tb1.isp_uid, tb1.uid, tb1.firstname, tb1.lastname, tb1.email, tb1.state, tb1.city, tb1.mobile, tb1.address, tb1.baseplanid, tb1.account_activated_on as active_on, tb1.paidtill_date, DATE(tb1.expiration) as expiration_date, tb1.plan_activated_date, tb1.user_credit_limit, tb1.inactivate_type, tb1.suspended_days,   tb2.plan_duration, tb2.srvname, tb2.descr, tb2.datacalc, tb2.datalimit, tb3.gross_amt, tb3.tax, tb3.net_total FROM sht_users as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid = tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb1.enableuser='1' AND tb1.expired='0' AND tb1.user_plan_type='prepaid' AND tb1.isp_uid='".$isp_uid."' AND tb1.next_bill_date='".$nextdaydate."'");
                //echo $this->db->last_query(); die;
                $num_rows = $subscriberQ->num_rows();
                if($num_rows > 0){
                    foreach($subscriberQ->result() as $subsobj){
                        $uid = $subsobj->uid;
                        $isp_uid = $subsobj->isp_uid;
			
			//CHECK FOR BILL ALREADY GENERATED
			$checkforcronQ = $this->db->query("SELECT id FROM sht_subscriber_billing_cron WHERE isp_uid='".$isp_uid."' AND uid='".$uid."' AND DATE(added_on) = '".$today_date."'");
			if($checkforcronQ->num_rows() == 0){
			    $this->db->insert('sht_subscriber_billing_cron', array('isp_uid' => $isp_uid, 'uid' => $uid, 'added_on' => date('Y-m-d H:i:s')));
			
			    $plan_duration = $subsobj->plan_duration;
			    $plan_tilldays = ($plan_duration * 30);
			    $plan_activated_date = $subsobj->plan_activated_date;
			    $next_bill_date = $subsobj->next_bill_date;
			    
			    if(strtotime($nextdaydate) == strtotime($next_bill_date)){
				
				$nasipQ = $this->db->query("SELECT tb1.radacctid, tb1.username, tb1.nasipaddress, tb1.framedipaddress, tb2.secret FROM radacct as tb1 INNER JOIN nas as tb2 ON(tb1.nasipaddress=tb2.nasname) WHERE tb1.username='".$uid."' ORDER BY radacctid DESC LIMIT 1");
				if($nasipQ->num_rows() > 0){
				    $nasrowdata = $nasipQ->row();
				    $radacctid = $nasrowdata->radacctid;
				    $username = $nasrowdata->username;
				    $userframedipaddress = $nasrowdata->framedipaddress;
				    $usernasipaddress = $nasrowdata->nasipaddress;
				    $secret = $nasrowdata->secret;
				    //@exec("echo User-Name:=$username,Framed-Ip-Address=$userframedipaddress | radclient -x $usernasipaddress:3799 disconnect $secret");
				    $this->disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret);
				    $this->db->query("UPDATE radacct SET acctstoptime='".date('Y-m-d H:i:s')."' WHERE username='".$uid."' AND acctstoptime IS NULL");
				}
				
				$tax = $this->current_taxapplicable($isp_uid);
				$gross_amt = $subsobj->gross_amt;
				
				$srvid = $subsobj->baseplanid;
				$billtype = 'montly_pay';
				$custom_planpriceArr = $this->getcustom_planprice($isp_uid, $uid, $srvid);
				$custom_planprice = $custom_planpriceArr['gross_amt'];
				if($custom_planprice != ''){
				    $gross_amt = round($custom_planprice / $plan_duration);
				    $actual_bill_amt = round(($custom_planprice * 100) / 118);
				    $plan_price = round($custom_planprice);
				}else{
				    $gross_amt = round($gross_amt + (($gross_amt * $tax)/100));
				    $plan_price = ($gross_amt * $plan_duration);
				    $actual_bill_amt = round(($plan_price * 100) / 118);
				}
				
				$user_credit_limit = $plan_price + round($plan_price / 2);
				$plan_cost_perday = round($plan_price/$plan_tilldays); 
				$plandata_calculatedon = $subsobj->datacalc;
				$plandatalimit = $subsobj->datalimit;
				$user_plan_type = $subsobj->user_plan_type;
				$planautorenewal = $subsobj->planautorenewal;
				
				/*
				*  CHECK FOR PLAN AUTO RENEWAL
				*/
				if($planautorenewal == '0'){
				    $this->db->update('sht_users', array('enableuser' => '0', 'inactivate_type' => 'suspended', 'suspended_days' => '0', 'suspendedon' => date('Y-m-d'), 'downlimit' => '0', 'comblimit' => '0'), array('uid' => $uid));
				}else{
				    
				    /**************************************************
				     *  CHECK FOR NEXT CYCLE PLAN
				     *************************************************/
				    $plandatarr = array();
				    $plandatarr['enableuser'] = '1';
				    $plandatarr['inactivate_type'] = '';
				    $plandatarr['suspended_days'] = '0';
				    $plandatarr['suspendedon'] = '0000-00-00';
				    $plandatarr['user_credit_limit'] = $user_credit_limit;
				    $plandatarr['plan_activated_date'] = $nextdaydate;
				    $plandatarr['plan_changed_datetime'] = date('Y-m-d H:i:s');
				    $nextPlanQ = $this->db->query("SELECT tb1.id, tb1.baseplanid, tb1.user_plan_type, tb1.prebill_oncycle,  tb2.datacalc, tb2.datalimit,  tb2.plan_duration, tb3.net_total, tb3.gross_amt FROM sht_nextcycle_userplanassoc as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid = tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE uid='".$uid."' AND status='0' ORDER BY id DESC LIMIT 1");
				    //echo $this->db->last_query(); die;
				    if($nextPlanQ->num_rows() > 0){
					$nextPlan_rowdata = $nextPlanQ->row();
					$nid = $nextPlan_rowdata->id;
					$newsrvid = $nextPlan_rowdata->baseplanid;
					$newsrvid_calcon = $nextPlan_rowdata->datacalc;
					$newsrvid_datalimt = $nextPlan_rowdata->datalimit;
					$newuser_plan_type = $nextPlan_rowdata->user_plan_type;
					$nxtprebill_oncycle = $nextPlan_rowdata->prebill_oncycle;
					
					$newplan_duration = $nextPlan_rowdata->plan_duration;
					$npgross_amt = $nextPlan_rowdata->gross_amt;
					$nextplan_tilldays = ($newplan_duration * 30);
		
					$custom_planpriceArr = $this->getcustom_planprice($isp_uid, $uid, $newsrvid);
					$custom_planprice = $custom_planpriceArr['gross_amt'];					
					if($custom_planprice != ''){
					    $this->updatecustom_planprice($isp_uid, $uid, $newsrvid);
					    //$actual_npgross_amt = round($custom_planprice / $newplan_duration);
					    //$actual_npgross_amt = round(($custom_planprice * 100) / 118);
					    $nextsrvid_price = round($custom_planprice);
					}else{
					    //$actual_npgross_amt = $nextPlan_rowdata->gross_amt;
					    $npgross_amt = round($npgross_amt + (($npgross_amt * $tax)/100));
					    $nextsrvid_price = ($npgross_amt * $newplan_duration);
					}
					
					$nphalf_gross_amt = round($nextsrvid_price / 2);
					$nextsrvid_cost_perday = round($nextsrvid_price/$nextplan_tilldays);
					$user_newcreditlimit = ($nextsrvid_price + $nphalf_gross_amt);
					$plandatarr['user_credit_limit'] = $user_newcreditlimit;
					$plandatarr['user_plan_type'] = $newuser_plan_type;
					
					/**** CHANGE TO PREPAID PLAN ****/
					if($newuser_plan_type == 'prepaid'){
					    $user_walletamt = $this->userbalance_amount($uid); //check again wallet balance
					    $payment_mode = ''; $receipt_received = '0'; $adjusted_amount = 0;
					    
					    $billing_cycle_date = $this->isp_billing_date($isp_uid);
					    if($nxtprebill_oncycle == 1){
						$nextbdate = date('Y-m-d', strtotime('+1 month'));
						$nextbdate = new DateTime($nextbdate);
						$nextbdate->setDate($nextbdate->format('Y'), $nextbdate->format('m'), $billing_cycle_date);
						$next_month_billdate = $nextbdate->format('Y-m-d');
						
						$nxttimestamp = strtotime($next_month_billdate);
						$todtimestamp = strtotime(date('Y-m-d'));
						$netdays_alloted = round(abs($nxttimestamp - $todtimestamp) / 86400);
						$plancost_per_day = round(($nextsrvid_price/$nextplan_tilldays));
						
						if($netdays_alloted > 27){
						    if($custom_planprice != ''){
							$actual_npgross_amt = round(($custom_planprice * 100) / 118);
						    }else{
							$actual_npgross_amt = round($nextPlan_rowdata->gross_amt);
						    }
						    $nxtplancost_fornow = round($nextsrvid_price);
						}else{
						    if($custom_planprice != ''){
							$gross_plan_cost_perday = ($custom_planprice / 30);
							$actual_npgross_amt = round($gross_plan_cost_perday * $netdays_alloted);
						    }else{
							$gross_plan_cost_perday = ($nextPlan_rowdata->gross_amt / 30);
							$actual_npgross_amt = round($gross_plan_cost_perday * $netdays_alloted);
						    }
						    $nxtplancost_fornow = round($plancost_per_day * $netdays_alloted);
						}
					    }else{
						if($custom_planprice != ''){
						    $actual_npgross_amt = round(($custom_planprice * 100) / 118);
						}else{
						    $actual_npgross_amt = round($nextPlan_rowdata->gross_amt);
						}
						$nxtplancost_fornow = round($nextsrvid_price);
					    }
					    
					    $billnumber = $this->generate_billnumber($isp_uid);
					    $prebillingarr = array(
						    'subscriber_uuid' => $uid,
						    'plan_id' => $newsrvid,
						    'user_plan_type' => 'prepaid',
						    'bill_starts_from' => "$plan_activated_date",
						    'bill_added_on' => $nextdaydate.' '.date('H:i:s'),
						    'bill_duedate' => $bill_duedate,
						    'bill_number' => $billnumber,
						    'bill_type' => 'montly_pay',
						    'payment_mode' => $payment_mode,
						    'receipt_received' => $receipt_received,
						    'actual_amount' => $actual_npgross_amt,
						    'adjusted_amount' => $adjusted_amount,
						    'discount' => '0',
						    'total_amount' => $nxtplancost_fornow,
						    'isp_uid' => $isp_uid
					    );
					    $this->db->insert('sht_subscriber_billing', $prebillingarr);
					    $prebillid = $this->db->insert_id();
					    $prePassbookArr = array(
						    'isp_uid' => $isp_uid,
						    'subscriber_uuid' => $uid,
						    'billing_id' => $prebillid,
						    'srvid' => $newsrvid,
						    'plan_cost' => $nxtplancost_fornow,
						    'added_on' => $nextdaydate.' '.date('H:i:s')
					    );
					    $this->db->insert('sht_subscriber_passbook', $prePassbookArr);
					    
					    $this->delete_temp_billnumber($isp_uid, $billnumber);
					    
					    if($nxtprebill_oncycle == 1){
						$nextbdate = date('Y-m-d', strtotime('+1 month'));
						$nextbdate = new DateTime($nextbdate);
						$nextbdate->setDate($nextbdate->format('Y'), $nextbdate->format('m'), $billing_cycle_date);
						$plandatarr['next_bill_date'] = $nextbdate->format('Y-m-d');
					    }else{
						$plandatarr['next_bill_date'] = date('Y-m-d',strtotime("+$nextplan_tilldays day"));
					    }
					}else{
					    $isp_billing_date = $this->isp_billing_date($isp_uid);
					    $nextdate = date('Y-m-d', strtotime('+1 month'));
					    $nextdate = new DateTime($nextdate);
					    $nextdate->setDate($nextdate->format('Y'), $nextdate->format('m'), $isp_billing_date);
					    $next_month = $nextdate->format('Y-m-d');
					    $next_bill_date = $next_month;
					    $plandatarr['next_bill_date'] = $next_bill_date;
					}
					
					
					
					if($newsrvid_calcon == '1'){
					    $plandatarr['downlimit'] = $newsrvid_datalimt;
					    $plandatarr['comblimit'] = '0';
					}elseif($newsrvid_calcon == '2'){
					    $plandatarr['downlimit'] = '0';
					    $plandatarr['comblimit'] = $newsrvid_datalimt;
					}else{
					    $plandatarr['downlimit'] = '0';
					    $plandatarr['comblimit'] = '0';
					}
					$plandatarr['baseplanid'] = $newsrvid;
					$plandatarr['bill_duedate'] = $bill_duedate;
					
					$this->db->update('sht_users', $plandatarr, array('uid' => $uid));
					$this->update_expirationdate($uid,$user_newcreditlimit,$nextsrvid_cost_perday);
					$this->db->update('sht_nextcycle_userplanassoc', array('status' => '1'), array('id' => $nid));
				    }
				    
				    /**************************************************
				     *  IF NO PLAN FOR NEXT CYCLE PLAN
				     *************************************************/
				    
				    else{
					$billnumber = $this->generate_billnumber($isp_uid);
					$payment_mode = ''; $receipt_received = '0'; $adjusted_amount = 0;
					$billingarr = array(
						'subscriber_uuid' => $uid,
						'plan_id' => $srvid,
						'user_plan_type' => 'prepaid',
						'bill_starts_from' => "$plan_activated_date",
						'bill_added_on' => $nextdaydate.' '.date('H:i:s'),
						'bill_number' => $billnumber,
						'bill_duedate' => $bill_duedate,
						'bill_type' => 'montly_pay',
						'payment_mode' => $payment_mode,
						'receipt_received' => $receipt_received,
						'actual_amount' => $actual_bill_amt,
						'adjusted_amount' => $adjusted_amount,
						'discount' => '0',
						'total_amount' => $plan_price,
						'isp_uid' => $isp_uid
					);
					$this->db->insert('sht_subscriber_billing', $billingarr);
					$billid = $this->db->insert_id();
					
					$PassbookArr = array(
					    'isp_uid' => $isp_uid,
					    'subscriber_uuid' => $uid,
					    'billing_id' => $billid,
					    'srvid' => $srvid,
					    'plan_cost' => $plan_price,
					    'added_on'	=> $nextdaydate.' '.date('H:i:s')
					);
					$this->db->insert('sht_subscriber_passbook', $PassbookArr);
					
					$this->delete_temp_billnumber($isp_uid, $billnumber);
					
					//CHECK FOR ADVANCED PAYMENT
					$user_advpayments = $this->user_advpayments($uid, $billid, $plan_price, $isp_uid);
					
					$custfupQ = $this->db->query("SELECT id FROM sht_customize_plan_speed WHERE uid='".$uid."' AND isp_uid='".$isp_uid."' AND status='1'");
					if($custfupQ->num_rows() > 0){
					    $this->db->update('sht_customize_plan_speed', array('status' => '0', 'modified_on' => date('Y-m-d H:i:s'), 'modified_by' => 'SuperAdmin'), array('uid' => $uid));
					}
					
					$plandatarr['next_bill_date'] = date('Y-m-d',strtotime("+$plan_tilldays days"));
					$plandatarr['user_plan_type'] = $user_plan_type;
					if($plandata_calculatedon == '1'){
					    $plandatarr['downlimit'] = $plandatalimit;
					    $plandatarr['comblimit'] = '0';
					}elseif($plandata_calculatedon == '2'){
					    $plandatarr['downlimit'] = '0';
					    $plandatarr['comblimit'] = $plandatalimit;
					}else{
					    $plandatarr['downlimit'] = '0';
					    $plandatarr['comblimit'] = '0';
					}
					$plandatarr['baseplanid'] = $srvid;
					$plandatarr['bill_duedate'] = $bill_duedate;
					$this->db->update('sht_users', $plandatarr, array('uid' => $uid));
					$this->update_expirationdate($uid,$user_credit_limit,$plan_cost_perday);
				    }
				}
				//$this->mail_format($uid);
				
				sleep(5);
			    }
			}
			sleep(60);
                    }
                }
		sleep(120);
            }
        }
    }
    
    
    public function getispdetails($isp_uid){
        $data = array();
        $ispdetQ = $this->db->query("SELECT company_name,isp_name,help_number1 FROM sht_isp_detail WHERE isp_uid='".$isp_uid."'");
        $rdata = $ispdetQ->row();
        $data['company_name'] = $rdata->company_name;
        $data['isp_name'] = $rdata->isp_name;
        $data['help_number'] = $rdata->help_number1;
        
        return $data;
    }
    public function creditlimit_reached(){
	$query=$this->db->query("select uid,isp_uid,email,mobile,user_credit_limit from sht_users where enableuser=1 and expired=0 and curdate()<date(expiration)");
	$activeuserarr=array();
	$uidarr=array();
	foreach($query->result() as $val){
	   $activeuserarr[$val->uid]=$val ;
	   $uidarr[]=$val->uid;
	}
	// echo "<pre>"; print_R($activeuserarr);
	$userids = '"' . implode('", "', $uidarr) . '"';
	$duesarr=$this->dues_list($userids);
	 
	$perc75arr=array();
	$perc90arr=array();
	$userid75above=array();
	foreach($activeuserarr as $key => $val){
	    if(array_key_exists($key,$duesarr)){
                $percentage=(str_replace("-","",$duesarr[$key])/$val->user_credit_limit)*100;
                if($percentage>=75 && $percentage<90){
                   $perc75arr[$key]=$val;
                   $userid75above[]=$key;
                }else if($percentage>90){
                   $perc90arr[$key]=$val;
                   $userid75above[]=$key;
                }
	    }
	}
	 
	$expirydata=$this->userassoc_expirydata($userid75above);
	
	foreach($expirydata as $vald){
            $isp_uid = $vald['isp_uid'];
            $ispdetArr = $this->getispdetails($isp_uid);
            $company_name = $ispdetArr['company_name'];
            $isp_name = $ispdetArr['isp_name'];
            $help_number = $ispdetArr['help_number'];
        
	    if($vald['expiration_days']==2){
		
		$sub = 'Sub ID '.$vald['uid'].' : Service disconnection notice';
		$emailmsg = 'Dear '.$vald['name'].'<br/><br/> Payment for your account with UserID: '.$vald['uid'].' is overdue. Your account will be suspended in 48 hours. Please make the payment at the earlist to enjoy uninterrupted services. <br/><br/> Team '.$isp_name;
		$alert_message = urlencode('Dear '.$vald['name']).'%0a'.urlencode('Payment for your account with UserID: '.$vald['uid'].' is overdue. Your account will be suspended in 48 hours. Please make the payment at the earlist to enjoy uninterrupted services.').'%0a'.urlencode('Team '.$isp_name);
		$db_message =  'Dear '.$vald['name'].'<br/> Payment for your account with UserID: '.$vald['uid'].' is overdue. Your account will be suspended in 48 hours. Please make the payment at the earlist to enjoy uninterrupted services. <br/> Team '.$isp_name;
		$this->creditlimit_emailer($vald['uid'],$vald['email'],$vald['isp_uid'],$sub,$emailmsg);
		$this->notifications_model->creditlimit_alert($vald['uid'],$vald['isp_uid'],$alert_message,$db_message);
	    }
	    else{
		if(array_key_exists($vald['uid'],$perc75arr)){
                    $sub='Sub ID '.$vald['uid'].' : 75% Credit limit reached';
                    $emailmsg = 'Dear '.$vald['name'].'<br/><br/> Payment for your account with UserID: '.$vald['uid'].' is due. Your account will be suspended in '.$vald['expiration_days'].' days. Please make the payment at the earlist to enjoy uninterrupted services. <br/><br/> Team '.$isp_name;
                    
                    $this->creditlimit_emailer($vald['uid'],$vald['email'],$vald['isp_uid'],$sub,$emailmsg);
                    
                }else if(array_key_exists($vald['uid'],$perc90arr)){
	
                    $sub='Sub ID '.$vald['uid'].' : 90% Credit limit reached';
                    $emailmsg = 'Dear '.$vald['name'].'<br/><br/> Payment for your account with UserID: '.$vald['uid'].' is due. Your account will be suspended in '.$vald['expiration_days'].' days. Please make the payment at the earlist to enjoy uninterrupted services. <br/><br/> Team '.$isp_name;
                    $alert_message = urlencode('Dear '.$vald['name']).'%0a'.urlencode('Payment for your account with UserID: '.$vald['uid'].' is due. Your account will be suspended in '.$vald['expiration_days'].' days. Please make the payment at the earlist to enjoy uninterrupted services.').'%0a'.urlencode('Team '.$isp_name);
                    $db_message =  'Dear '.$vald['name'].'<br/> Payment for your account with UserID: '.$vald['uid'].' is due. Your account will be suspended in '.$vald['expiration_days'].' days. Please make the payment at the earlist to enjoy uninterrupted services. <br/> Team '.$isp_name;
                
                    $this->creditlimit_emailer($vald['uid'],$vald['email'],$vald['isp_uid'],$sub,$emailmsg);
                    $this->notifications_model->creditlimit_alert($vald['uid'],$vald['isp_uid'],$alert_message,$db_message);
                      // echo $vald['uid'].":expures in 90 percent ::".$vald['expiration_days']."<br/>";
                }
	    }   
	}
	echo "credit limit";
    }
    
    
    
    public function userassoc_expirydata($subscriber_uuidarr=array()){
	 $subscriber_uuid = '"' . implode('", "', $subscriber_uuidarr) . '"';
		$data = array();
		$user_currplanQ = $this->db->query("SELECT tb2.datacalc,tb1.isp_uid,tb1.firstname,tb1.lastname,tb1.uid, tb2.datalimit, tb2.plantype,tb2.plan_duration, tb1.baseplanid,tb1.email,tb1.mobile, tb1.user_plan_type, tb1.user_credit_limit, DATE(tb1.account_activated_on) as account_activated_on, tb1.paidtill_date, DATE(tb1.expiration) as expiration_date, tb3.net_total, tb3.gross_amt FROM sht_users as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid=tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb1.uid in($subscriber_uuid)");
		//$current_planrow = $user_currplanQ->row();
		foreach($user_currplanQ->result() as $current_planrow)
		{
		$tax = $this->current_taxapplicable($current_planrow->isp_uid);
		$plan_duration = $current_planrow->plan_duration;
		$srvid = $current_planrow->baseplanid;
		$uid=$current_planrow->uid;
		$isp_uid=$current_planrow->isp_uid;
		
		$custom_planpriceArr = $this->getcustom_planprice($isp_uid,$subscriber_uuid, $srvid);
		$custom_planprice = $custom_planpriceArr['gross_amt'];
		if($custom_planprice != ''){
			$gross_amt = round($custom_planprice);
			$plan_price = round($custom_planprice);
		}else{
			$gross_amt = ($current_planrow->gross_amt * $plan_duration);
			$plan_price = round($gross_amt + (($gross_amt * $tax)/100));
		}
		$data[$uid]['uid'] = $uid;
		$data[$uid]['name'] = $current_planrow->firstname." ".$current_planrow->lastname;
		$data[$uid]['isp_uid'] = $isp_uid;
		$data[$uid]['email'] = $current_planrow->email;
		$data[$uid]['mobile'] = $current_planrow->mobile;
		$data[$uid]['plan_duration'] = $plan_duration;
		$data[$uid]['datacalc'] = $current_planrow->datacalc;
		$data[$uid]['datalimit'] = $current_planrow->datalimit;
		$data[$uid]['planid'] = $current_planrow->baseplanid;
		$data[$uid]['plan_price'] = $plan_price;
		$data[$uid]['plantype'] = $current_planrow->plantype;
		$data[$uid]['user_credit_limit'] = $current_planrow->user_credit_limit;
		$data[$uid]['account_activated_on'] = $current_planrow->account_activated_on;
		$currentdate=date("Y-m-d");
		//$data['paidtill_date'] = $current_planrow->paidtill_date;
		$expirydays= abs(strtotime($currentdate) - strtotime($current_planrow->expiration_date) )/ 86400;
		$plan_tilldays = ($plan_duration * 30);
		$data[$uid]['plan_tilldays'] = $plan_tilldays;
		$data[$uid]['plan_cost_perday'] = round($plan_price/$plan_tilldays);
		$data[$uid]['expiration_date'] = $current_planrow->expiration_date;
		$data[$uid]['expiration_days'] = $expirydays;
		$data[$uid]['date_days'] = $currentdate."::".$current_planrow->expiration_date;
		$data[$uid]['user_plan_type'] = $current_planrow->user_plan_type;
		
		}
		
		return $data;
	}
	
	
    public function creditlimit_emailer($uuid,$email,$isp_uid,$sub,$msg){
        $this->load->library('email');
        $data = array();
        
        $message = $msg;
        //echo $message; die;
        $from = ''; $fromname = '';
        $ispaccount = $this->getisp_accounttype($isp_uid);
        $ispdetailArr = $this->isp_details($isp_uid);
	$emailQ = $this->db->get_where('sht_email_setup', array('isp_uid' => $isp_uid, 'status' => '1'));
	if($emailQ->num_rows() > 0){
	    $rowdata = $emailQ->row();
	    //echo "<pre>"; print_R($rowdata);
	    $from = (isset($rowdata->sendfrom) && $rowdata->sendfrom!='')  ? $rowdata->sendfrom : $rowdata->user;
	    $fromname = (isset($rowdata->sender_name) && $rowdata->sender_name!='') ? $rowdata->sender_name : $ispdetailArr['company_name'];
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://'.$rowdata->host,
	    'smtp_port' => $rowdata->port,
	    'smtp_user' => $rowdata->user,
	    'smtp_pass' => $rowdata->password,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}else{
	    $from = SMTP_USER;
	    $fromname = 'DECIBEL';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
	}
        
        if(count($config) > 0){
	  //  echo "<pre>"; print_R($config);
	 //   die;
	
            //Initialise CI mail
            $this->email->initialize($config);
            $this->email->from($from, $fromname);
            $this->email->to($email);
            $this->email->subject($sub);
            $this->email->message($message);	
            $this->email->send();
        }
        
        return 1;
    }
    
    public function dues_list($uidcond){
	$passbookQ = $this->db->query("SELECT subscriber_uuid, SUM(plan_cost) AS expense_cost FROM sht_subscriber_passbook WHERE subscriber_uuid in($uidcond) GROUP BY `subscriber_uuid` ");
	$data = array();
	if ($passbookQ->num_rows() > 0){
	    foreach($passbookQ->result() as $pbobj){
		$wallet_amt = 0;
                $passbook_amt = $pbobj->expense_cost;
                $uuid = $pbobj->subscriber_uuid;
                $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='" . $uuid . "'");
                if ($walletQ->num_rows() > 0){
                    $wallet_amt = $walletQ->row()->wallet_amt;
                }
                $balanceamt = $wallet_amt - $passbook_amt;
                if ($balanceamt < 0)
                {
                    $data[$pbobj->subscriber_uuid] = str_replace("-", "", $balanceamt);
                }
	    }
	}
        return $data;
    }


    
    public function account_expiration_alert(){
	$useracctQ = $this->db->query("SELECT uid, firstname, isp_uid FROM sht_users WHERE curdate() = DATE(expiration) AND enableuser = '1'");
        $numrows = $useracctQ->num_rows();
        if($numrows > 0){
	    foreach($useracctQ->result() as $userobj){
		$isp_uid = $userobj->isp_uid;
		$uuid = $userobj->uid;
		$firstname = $userobj->firstname;
		
		$ispdetArr = $this->getispdetails($isp_uid);
		$company_name = $ispdetArr['company_name'];
		$isp_name = $ispdetArr['isp_name'];
		$help_number = $ispdetArr['help_number'];
		
		$alert_message = urlencode('Dear '.$firstname).'%0a'.urlencode('Your account is getting expired today. Please make the payment at the earlist to enjoy uninterrupted services.').'%0a'.urlencode('Team '.$isp_name);
		$db_message =  'Dear '.$firstname.'<br/> Your account is getting expired today. Please make the payment at the earlist to enjoy uninterrupted services. <br/> Team '.$isp_name;
		$this->notifications_model->acctexpiration_alert($uuid,$isp_uid,$alert_message,$db_message);
		sleep(1);
	    }
	}
    }
    /*----------------- CUSTOM BILL CRON -------------------------------------*/
    public function custom_bill_generation(){
	ini_set('max_execution_time', '0');
        $today = date('d');
        $today_date = date('Y-m-d');        
        
	$bill_cycleQ = $this->db->query("SELECT isp_uid FROM sht_isp_admin WHERE decibel_account = 'paid'");
        if($bill_cycleQ->num_rows() > 0){
            foreach($bill_cycleQ->result() as $bcobj){
                $isp_uid = $bcobj->isp_uid;
		$custombills = $this->db->query("SELECT id, uid, discount, DATE(added_on) as billdate, billgenerated_afterdays, billgenerated_aftermonth FROM sht_subscriber_custom_billing WHERE isp_uid='".$isp_uid."' AND (billgenerated_afterdays != '0' OR billgenerated_aftermonth != '0') AND isnext_billgenerated='0'");
		if($custombills->num_rows() > 0){
		    foreach($custombills->result() as $custobj){
			$billdate = $custobj->billdate;
			$billid = $custobj->id;
			$uuid = $custobj->uuid;
			$billgenerated_afterdays = $custobj->billgenerated_afterdays;
			$billgenerated_aftermonth = $custobj->billgenerated_aftermonth;
			if($billgenerated_afterdays != '0'){
			    $billdate = date('Y-m-d', strtotime($billdate ."+$billgenerated_afterdays days"));
			}elseif($billgenerated_aftermonth != '0'){
			    $calculateddays = ($billgenerated_aftermonth * 30);
			    $billdate = date('Y-m-d', strtotime($billdate ."+$calculateddays days"));
			}
			$discount = $custobj->discount;
			
			//CHECK FOR BILL DATE
			if(strtotime($today_date) == strtotime($billdate)){
			    $this->db->update('sht_subscriber_custom_billing', array('isnext_billgenerated' => '1'), array('id' => $billid));
			    $billtax = '18'; //18%
			    $bill_companyid = 0;
			    $custominvoiceQ = $this->db->query("SELECT * FROM sht_custom_invoice_list WHERE bill_id='".$billid."'");
			    if($custominvoiceQ->num_rows() > 0){
				foreach($custominvoiceQ->result() as $custinvobj){
				    $uuid = $custinvobj->uid;
				    $topic_id = $custinvobj->topic_id;
				    $topic_name = $custinvobj->topic_name;
				    $bill_companyid = $custinvobj->bill_companyid;
				    $price = $custinvobj->price;
				    $quantity = $custinvobj->quantity;
				    $total_price = $custinvobj->total_price;
				    
				    $billinvoice = array(
					'isp_uid' => $isp_uid,
					'uid' => $uuid,
					'bill_companyid' => $bill_companyid,
					'topic_id' => $topic_id,
					'topic_name' => $topic_name,
					'price' => $price,
					'quantity' => $quantity,
					'total_price' => $total_price,
					'added_on' => date('Y-m-d H:i:s')
				    );
				    $this->db->insert('sht_custom_invoice_list', $billinvoice);
				    
				}
			    }
			    
			    $companyQ = $this->db->query("SELECT tax FROM sht_isp_company_details WHERE isp_uid='".$isp_uid."' AND id='".$bill_companyid."'");
			    $comp_numrows = $companyQ->num_rows();
			    if($comp_numrows > 0){
				$billtax = $companyQ->row()->tax;
			    }
			    $custominvoiceQ = $this->db->query("SELECT COALESCE( SUM(total_price), 0 ) as total_bill, GROUP_CONCAT(topic_name)  as bill_details FROM sht_custom_invoice_list WHERE bill_id='' AND isp_uid='".$isp_uid."' AND uid='".$uuid."'");
			    if($custominvoiceQ->num_rows() > 0){
				$rowdata = $custominvoiceQ->row();
				$total_bill = $rowdata->total_bill;
				if($total_bill != '0.00'){
				    $total_billpay = round( $total_bill + (($total_bill * $billtax) / 100) );
				    $total_billpay = ($total_billpay - $discount);
				
				    $custombillArr = array(
					'isp_uid' => $isp_uid,
					'uid' => $uuid,
					'receipt_received' => '0',
					'bill_companyid' => $bill_companyid,
					'bill_number' => date('jnyHis'),
					'bill_type' => 'custom_bill',
					'bill_details' => $rowdata->bill_details,
					'payment_mode' => '',
					'discount' => $discount,
					'total_amount' => $total_billpay,
					'receipt_received' => '',
					'added_on' => date('Y-m-d H:i:s'),
					'bill_comments' => '',
					'bill_remarks' => '',
					'receipt_number' => '',
					'is_deleted' => '0',
					'billgenerated_aftermonth' => $billgenerated_aftermonth,
					'billgenerated_afterdays' => $billgenerated_afterdays,
					'isnext_billgenerated' => '0'
				    );	
				    $this->db->insert('sht_subscriber_custom_billing', $custombillArr);
				    $nxtbillid = $this->db->insert_id();
				}
				
				$this->db->update("sht_subscriber_custom_billing", array('bill_uniqid' => 'cb'.$nxtbillid), array('id' => $nxtbillid));
				$this->db->update("sht_custom_invoice_list", array('bill_id' => $nxtbillid), array('isp_uid' => $isp_uid, 'uid' => $uuid, 'bill_id' => ''));
			    }
			}
		    }
		}
		
		sleep(10);
	    }
	}
	
    }
    
    
    
    /*----------------- BILL DUES CRON -----------------------------------*/
    public function bill_dues_generation(){
	$today_date = date('Y-m-d');
    }
    
    /*----------------- CRON TESTING -------------------------------------*/
    public function due_prepaid_bill_generation(){
	ini_set('max_execution_time', '0');
        $today = date('d');
        $today_date = date('Y-m-d');        
        //$date = new DateTime($today_date);
        //$date->modify('+1 day');
        //$nextdaydate = $date->format('Y-m-d');
        $currmonth = date('m');
        
        $nextdate = new DateTime('+1 day');
        $nextdaydate = $nextdate->format('Y-m-d');
        
        $pendingUsersQ = $this->db->query("SELECT MONTH( tb1.next_bill_date ) AS lastmonthbill, tb1.next_bill_date, tb1.isp_uid, tb1.uid FROM sht_users as tb1 WHERE tb1.enableuser='1' AND tb1.expired='0' AND tb1.user_plan_type='prepaid' AND curdate() > tb1.next_bill_date AND tb1.isp_uid='129'");
        echo $this->db->last_query(); die;
        $num_rows = $pendingUsersQ->num_rows();
        if($num_rows > 0){
            foreach($pendingUsersQ->result() as $puobj){
                $lastmonthbill = $puobj->lastmonthbill;
                $uid = $puobj->uid;
                
                //if($uid == '12902183'){
                    
                    for($m=$lastmonthbill; $m<=$currmonth; $m++){
                        $subscriberQ = $this->db->query("SELECT MONTH( tb1.next_bill_date ) AS lastmonthbill, tb1.next_bill_date, tb1.user_plan_type, tb1.isp_uid, tb1.uid, tb1.firstname, tb1.lastname, tb1.email, tb1.state, tb1.city, tb1.mobile, tb1.address, tb1.baseplanid, tb1.account_activated_on as active_on, tb1.paidtill_date, DATE(tb1.expiration) as expiration_date, tb1.plan_activated_date, tb1.user_credit_limit, tb1.inactivate_type, tb1.suspended_days,   tb2.plan_duration, tb2.srvname, tb2.descr, tb2.datacalc, tb2.datalimit, tb3.gross_amt, tb3.tax, tb3.net_total, tb3.gross_amt FROM sht_users as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid = tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb1.uid='".$uid."'");
                        $subsobj = $subscriberQ->row();
                        
                        $plandatarr = array();
                        $isp_uid = $subsobj->isp_uid;
                        $plan_duration = $subsobj->plan_duration;
                        $plan_tilldays = ($plan_duration * 30);
                        $plan_activated_date = $subsobj->plan_activated_date;
                        //$next_bill_date = date('Y-m-d',strtotime($plan_activated_date." +$plan_tilldays days"));
                        $bill_date = $subsobj->next_bill_date;
                        
                        echo $bill_date . '#######' . $isp_uid . '###########' .$uid . '#########' . '<br/>';
                        
                        if(strtotime($today_date) > strtotime($bill_date)){
                            $nasipQ = $this->db->query("SELECT tb1.radacctid, tb1.username, tb1.nasipaddress, tb1.framedipaddress, tb2.secret FROM radacct as tb1 INNER JOIN nas as tb2 ON(tb1.nasipaddress=tb2.nasname) WHERE tb1.username='".$uid."' ORDER BY radacctid DESC LIMIT 1");
                            if($nasipQ->num_rows() > 0){
                                $nasrowdata = $nasipQ->row();
                                $radacctid = $nasrowdata->radacctid;
                                $username = $nasrowdata->username;
                                $userframedipaddress = $nasrowdata->framedipaddress;
                                $usernasipaddress = $nasrowdata->nasipaddress;
                                $secret = $nasrowdata->secret;
                                //@exec("echo User-Name:=$username,Framed-Ip-Address=$userframedipaddress | radclient -x $usernasipaddress:3799 disconnect $secret");
                                $this->disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret);
                                $this->db->query("UPDATE radacct SET acctstoptime='".date('Y-m-d H:i:s')."' WHERE username='".$uid."' AND acctstoptime IS NULL");
                            }
                            
                            $tax = $this->current_taxapplicable($isp_uid);
                            $gross_amt = $subsobj->gross_amt;
                            
                            $srvid = $subsobj->baseplanid;
                            $billtype = 'montly_pay';
                            $custom_planpriceArr = $this->getcustom_planprice($isp_uid, $uid, $srvid);
                            $custom_planprice = $custom_planpriceArr['gross_amt'];
                            if($custom_planprice != ''){
                                $gross_amt = round($custom_planprice / $plan_duration);
                                $plan_price = round($custom_planprice);
                            }else{
                                $gross_amt = round($gross_amt + (($gross_amt * $tax)/100));
                                $plan_price = ($gross_amt * $plan_duration);	
                            }
                            
                            $user_credit_limit = $plan_price + round($gross_amt / 2);
                            $plan_cost_perday = round($plan_price/$plan_tilldays); 
                            $plandata_calculatedon = $subsobj->datacalc;
                            $plandatalimit = $subsobj->datalimit;
                            $user_plan_type = $subsobj->user_plan_type;
                            
                            
                            /**************************************************
                             *  CHECK FOR NEXT CYCLE PLAN
                             *************************************************/
                            
			    $plandatarr = array();
			    $plandatarr['enableuser'] = '1';
			    $plandatarr['inactivate_type'] = '';
			    $plandatarr['suspended_days'] = '0';
			    $plandatarr['suspendedon'] = '0000-00-00';
			    $plandatarr['user_credit_limit'] = $user_credit_limit;
                            $plandatarr['plan_activated_date'] = $bill_date;
                            $plandatarr['plan_changed_datetime'] = $bill_date. ' 00:00:00';
                            $nextPlanQ = $this->db->query("SELECT tb1.id, tb1.baseplanid, tb1.user_plan_type, tb1.prebill_oncycle, tb2.datacalc, tb2.datalimit,  tb2.plan_duration, tb3.net_total, tb3.gross_amt FROM sht_nextcycle_userplanassoc as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid = tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE uid='".$uid."' AND status='0' ORDER BY id DESC LIMIT 1");
                            //echo $this->db->last_query(); die;
                            if($nextPlanQ->num_rows() > 0){
                                $nextPlan_rowdata = $nextPlanQ->row();
                                $nid = $nextPlan_rowdata->id;
                                $newsrvid = $nextPlan_rowdata->baseplanid;
                                $newsrvid_calcon = $nextPlan_rowdata->datacalc;
                                $newsrvid_datalimt = $nextPlan_rowdata->datalimit;
                                $newuser_plan_type = $nextPlan_rowdata->user_plan_type;
				$nxtprebill_oncycle = $nextPlan_rowdata->prebill_oncycle;
                                
                                $newplan_duration = $nextPlan_rowdata->plan_duration;
                                $npgross_amt = $nextPlan_rowdata->gross_amt;
                                $nextplan_tilldays = ($newplan_duration * 30);
        
                                $custom_planpriceArr = $this->getcustom_planprice($isp_uid, $uid, $newsrvid);
                                $custom_planprice = $custom_planpriceArr['gross_amt'];
                                if($custom_planprice != ''){
                                    $this->updatecustom_planprice($isp_uid, $uid, $newsrvid);
                                    $npgross_amt = round($custom_planprice / $newplan_duration);
                                    $nextsrvid_price = round($custom_planprice);
                                }else{
                                    $npgross_amt = round($npgross_amt + (($npgross_amt * $tax)/100));
                                    $nextsrvid_price = ($npgross_amt * $newplan_duration);
                                }
                                
                                $nphalf_gross_amt = round($nextsrvid_price / 2);
                                $nextsrvid_cost_perday = round($nextsrvid_price/$nextplan_tilldays);
                                $user_newcreditlimit = ($nextsrvid_price + $nphalf_gross_amt);
                                $plandatarr['user_credit_limit'] = $user_newcreditlimit;
                                $plandatarr['user_plan_type'] = $newuser_plan_type;
                                
                                /**** CHANGE TO PREPAID PLAN ****/
                                if($newuser_plan_type == 'prepaid'){
				    $user_walletamt = $this->userbalance_amount($uid); //check again wallet balance
				    $payment_mode = ''; $receipt_received = '0'; $adjusted_amount = 0;
				    
				    $billing_cycle_date = $this->isp_billing_date($isp_uid);
				    if($nxtprebill_oncycle == 1){
					$nextbdate = date('Y-m-d', strtotime('+1 month'));
					$nextbdate = new DateTime($nextbdate);
					$nextbdate->setDate($nextbdate->format('Y'), $nextbdate->format('m'), $billing_cycle_date);
					$next_month_billdate = $nextbdate->format('Y-m-d');
					
					$nxttimestamp = strtotime($next_month_billdate);
					$todtimestamp = strtotime(date('Y-m-d'));
					$netdays_alloted = round(abs($nxttimestamp - $todtimestamp) / 86400);
					
					$plancost_per_day = round(($nextsrvid_price/$nextplan_tilldays));
					$nxtplancost_fornow = round($plancost_per_day * $netdays_alloted);
					
				    }else{
					$nxtplancost_fornow = $nextsrvid_price;
				    }
				    
				    $prebillingarr = array(
					    'subscriber_uuid' => $uid,
					    'plan_id' => $newsrvid,
					    'user_plan_type' => 'prepaid',
					    'bill_starts_from' => "$plan_activated_date",
					    'bill_added_on' => $nextdaydate.' '.date('H:i:s'),
					    'bill_number' => date('jnyHis'),
					    'bill_type' => 'montly_pay',
					    'payment_mode' => $payment_mode,
					    'receipt_received' => $receipt_received,
					    'actual_amount' => $nxtplancost_fornow,
					    'adjusted_amount' => $adjusted_amount,
					    'discount' => '0',
					    'total_amount' => $nxtplancost_fornow,
					    'isp_uid' => $isp_uid
				    );
				    $this->db->insert('sht_subscriber_billing', $prebillingarr);
				    $prebillid = $this->db->insert_id();
				    $prePassbookArr = array(
					    'isp_uid' => $isp_uid,
					    'subscriber_uuid' => $uid,
					    'billing_id' => $prebillid,
					    'srvid' => $newsrvid,
					    'plan_cost' => $nxtplancost_fornow,
					    'added_on' => $nextdaydate.' '.date('H:i:s')
				    );
				    $this->db->insert('sht_subscriber_passbook', $prePassbookArr);
				    
				    if($nxtprebill_oncycle == 1){
					$nextbdate = date('Y-m-d', strtotime('+1 month'));
					$nextbdate = new DateTime($nextbdate);
					$nextbdate->setDate($nextbdate->format('Y'), $nextbdate->format('m'), $billing_cycle_date);
					$plandatarr['next_bill_date'] = $nextbdate->format('Y-m-d');
				    }else{
					$plandatarr['next_bill_date'] = date('Y-m-d',strtotime("+$nextplan_tilldays day"));
				    }
				}else{
				    $isp_billing_date = $this->isp_billing_date($isp_uid);
				    $nextdate = date('Y-m-d', strtotime('+1 month'));
				    $nextdate = new DateTime($nextdate);
				    $nextdate->setDate($nextdate->format('Y'), $nextdate->format('m'), $isp_billing_date);
				    $next_month = $nextdate->format('Y-m-d');
				    $next_bill_date = $next_month;
				    $plandatarr['next_bill_date'] = $next_bill_date;
				}
                                
                                
                                
                                if($newsrvid_calcon == '1'){
                                    $plandatarr['downlimit'] = $newsrvid_datalimt;
                                    $plandatarr['comblimit'] = '0';
                                }elseif($newsrvid_calcon == '2'){
                                    $plandatarr['downlimit'] = '0';
                                    $plandatarr['comblimit'] = $newsrvid_datalimt;
                                }else{
                                    $plandatarr['downlimit'] = '0';
                                    $plandatarr['comblimit'] = '0';
                                }
                                $plandatarr['baseplanid'] = $newsrvid;                                
                                $this->db->update('sht_users', $plandatarr, array('uid' => $uid));
                                $this->update_expirationdate($uid,$user_newcreditlimit,$nextsrvid_cost_perday);
                                $this->db->update('sht_nextcycle_userplanassoc', array('status' => '1'), array('id' => $nid));
                            }
                            
                            /**************************************************
                             *  IF NO PLAN FOR NEXT CYCLE PLAN
                             *************************************************/
                            
                            else{
                                $payment_mode = ''; $receipt_received = '0'; $adjusted_amount = 0;
                                $billingarr = array(
                                        'subscriber_uuid' => $uid,
                                        'plan_id' => $srvid,
                                        'user_plan_type' => 'prepaid',
                                        'bill_starts_from' => "$plan_activated_date",
                                        'bill_added_on' => $bill_date.' 00:00:00',
                                        'bill_number' => date('jnyHis'),
                                        'bill_type' => 'montly_pay',
                                        'payment_mode' => $payment_mode,
                                        'receipt_received' => $receipt_received,
                                        'actual_amount' => $plan_price,
                                        'adjusted_amount' => $adjusted_amount,
                                        'discount' => '0',
                                        'total_amount' => $plan_price,
                                        'isp_uid' => $isp_uid
                                );
                                $this->db->insert('sht_subscriber_billing', $billingarr);
                                $billid = $this->db->insert_id();
                                
                                $PassbookArr = array(
                                    'isp_uid' => $isp_uid,
                                    'subscriber_uuid' => $uid,
                                    'billing_id' => $billid,
                                    'srvid' => $srvid,
                                    'plan_cost' => $plan_price,
                                    'added_on'	=> $bill_date.' 00:00:00'
                                );
                                $this->db->insert('sht_subscriber_passbook', $PassbookArr);
                                
                                
                                //CHECK FOR ADVANCED PAYMENT
                                $user_advpayments = $this->user_advpayments($uid, $billid, $plan_price, $isp_uid);
                                
                                $custfupQ = $this->db->query("SELECT id FROM sht_customize_plan_speed WHERE uid='".$uid."' AND isp_uid='".$isp_uid."' AND status='1'");
                                if($custfupQ->num_rows() > 0){
                                    $this->db->update('sht_customize_plan_speed', array('status' => '0', 'modified_on' => date('Y-m-d H:i:s'), 'modified_by' => 'SuperAdmin'), array('uid' => $uid));
                                }
                                
                                $plandatarr['next_bill_date'] = date('Y-m-d',strtotime("$bill_date +$plan_tilldays days"));
                                $plandatarr['user_plan_type'] = $user_plan_type;
                                if($plandata_calculatedon == '1'){
                                    $plandatarr['downlimit'] = $plandatalimit;
                                    $plandatarr['comblimit'] = '0';
                                }elseif($plandata_calculatedon == '2'){
                                    $plandatarr['downlimit'] = '0';
                                    $plandatarr['comblimit'] = $plandatalimit;
                                }else{
                                    $plandatarr['downlimit'] = '0';
                                    $plandatarr['comblimit'] = '0';
                                }
                                $plandatarr['baseplanid'] = $srvid;                                
                                $this->db->update('sht_users', $plandatarr, array('uid' => $uid));
                                $this->update_expirationdate($uid,$user_credit_limit,$plan_cost_perday);
                            }
                            
                            //$this->mail_format($uid);
                        }
                        sleep(10);
                    }
                //}
                echo '@@@@@@@@@@@@@@@@@@@'; echo '<br/><br/>';
            }
        }
    }

    public function check_duplicate_billnumber(){
	ini_set('max_execution_time', '0');
        $today = date('d');
        $today_date = date('Y-m-d');        
        //SELECT isp_uid, subscriber_uuid, old_bill_number, bill_number,bill_added_on  FROM `sht_subscriber_billing` WHERE `old_bill_number` != '0' AND old_bill_number != bill_number ORDER BY old_bill_number 
	
	$bill_cycleQ = $this->db->query("SELECT isp_uid FROM sht_isp_admin WHERE decibel_account = 'paid'");
        if($bill_cycleQ->num_rows() > 0){
            foreach($bill_cycleQ->result() as $bcobj){
                $isp_uid = $bcobj->isp_uid;
		$userarray = array();
		$userbillarray = array();
		
		if($isp_uid == '100'){
		    $gstinusersQ = $this->db->query("SELECT uid  FROM `sht_users` WHERE `isp_uid` = '".$isp_uid."' AND `gstin_number` != ''");
		    if($gstinusersQ->num_rows() > 0){
			foreach($gstinusersQ->result() as $gstuid){
			    $userarray[] = $gstuid->uid;
			}
		    }
		    
		    
		    $usersbillQ = $this->db->query("SELECT id, subscriber_uuid, sht_subscriber_billing.bill_number, bill_added_on
FROM sht_subscriber_billing INNER JOIN( SELECT bill_number FROM sht_subscriber_billing GROUP BY bill_number HAVING COUNT(id) >1 )temp ON sht_subscriber_billing.bill_number= temp.bill_number WHERE sht_subscriber_billing.bill_number != '' AND isp_uid='".$isp_uid."' AND (bill_type='montly_pay' OR bill_type='midchange_plancost')");
		    if($usersbillQ->num_rows() > 0){
			$userbillarray = $usersbillQ->result_array();
			/*foreach($usersbillQ->result() as $ubuidobj){
			    $uuid = $ubuidobj->subscriber_uuid;
			    $bill_number = $ubuidobj->bill_number;
			    $billid = $ubuidobj->id;
			    
			    if(in_array($uuid, $userarray)){
				$duplicate = $this->checkduplicate_recursive($billid, $bill_number, $bill_number);
			    }else{
				$duplicate = $this->checkduplicate_recursive($billid, $bill_number, $bill_number);
			    }
			}*/
		    }
		    
		    echo '<pre>'; print_r($userarray); echo '</pre>';
		    echo '<pre>'; print_r($userbillarray); echo '</pre>';
		}
	    }
	}
    }
    
    public function checkduplicate_recursive($billid, $prevbill_number, $billtryno){
	$checkforduplicate = $this->db->query("SELECT id FROM sht_subscriber_billing WHERE bill_number='".$billtryno."' AND id != '".$billid."'");
	if($checkforduplicate->num_rows() > 0){
	    $newtrybillno = $billtryno + 1;
	    $this->checkduplicate_recursive($billid, $prevbill_number, $newtrybillno);
	}else{
	    $this->db->update('sht_subscriber_billing', array('old_bill_number' => $prevbill_number, 'bill_number' => $billtryno), array('id' => $billid));
	    return 1;
	}
    }
    
    public function delete_duplicatebills(){
	$activebill = array();
	$query = $this->db->query("SELECT id,subscriber_uuid,isp_uid,plan_id,total_amount FROM sht_subscriber_billing WHERE DATE(bill_added_on) = '2018-02-01' AND bill_type='montly_pay' AND isp_uid='130' ORDER BY id ASC");
	//echo $this->db->last_query(); die;
	if($query->num_rows() > 0){
	    foreach($query->result() as $qobj){
		$billid = $qobj->id;
		$uuid = $qobj->subscriber_uuid;
		$isp_uid = $qobj->isp_uid;
		$prev_srvid = $qobj->plan_id;
		$billcost = $qobj->total_amount;
		
		//if($uuid == '10500827'){
		    $subquery = $this->db->query("SELECT id FROM sht_subscriber_billing WHERE DATE(bill_added_on) = '2018-02-01' AND id != '".$billid."' AND subscriber_uuid='".$uuid."' AND plan_id='".$prev_srvid."' AND bill_type='montly_pay' AND isp_uid='130'");
		    if($subquery->num_rows() > 0){
			$row_array = array();
			foreach($subquery->result() as $subobj){
			    $row_array[] = $subobj->id;
			}
			$delids = implode(',', $row_array);
			
			if(array_key_exists("$uuid", $activebill)){
			}else{
			    $activebill["$uuid"] = $billid;
			    echo '<pre>'; print_r($activebill);
			    echo $uuid.'=>'.$billid.'=>'.$delids . '<br/>';
			    
			    $passbookQ = $this->db->query("SELECT id FROM sht_subscriber_passbook WHERE billing_id='".$billid."'");
			    if($passbookQ->num_rows() == 0){
				$passbookArr = array(
				    'isp_uid' => $isp_uid,
				    'subscriber_uuid' => $uuid,
				    'billing_id' => $billid,
				    'srvid' => $prev_srvid,
				    'plan_cost' => $billcost,
				    'added_on'	=> date('Y-m-d H:i:s')
				);
				//print_r($passbookArr); die;
				//$this->db->insert('sht_subscriber_passbook', $passbookArr);
			    }
			    
			    //$this->db->query("DELETE FROM sht_subscriber_billing WHERE id IN($delids)");
			    //$this->db->query("DELETE FROM sht_subscriber_passbook WHERE billing_id IN($delids)");
			}
		    }
		//} 
		//sleep(1);
	    }
	}
    }
    public function delete_advancedbills(){
	
	
	
    }


    public function user_session_restart(){
	$today = date('Y-m-d');
	$bill_cycleQ = $this->db->query("SELECT isp_uid FROM sht_isp_admin WHERE decibel_account = 'paid'");
	if($bill_cycleQ->num_rows() > 0){
	    foreach($bill_cycleQ->result() as $billobj){
		$isp_uid = $billobj->isp_uid;
		
		$ispuserQ = $this->db->query("SELECT uid FROM sht_users WHERE isp_uid='".$isp_uid."' AND (enableuser='1' OR (enableuser='0' AND inactivate_type != ''))");
		if($ispuserQ->num_rows() > 0){
		    foreach($ispuserQ->result() as $ispuobj){
			$uuid = $ispuobj->uid;
			$checksessionQ = $this->db->query("SELECT id,added_on FROM sht_user_session_restart WHERE isp_uid='".$isp_uid."' AND uid='".$uuid."'");
			if($checksessionQ->num_rows() > 0){
			    $rowdata = $checksessionQ->row();
			    $sessadded_on = $rowdata->added_on;
			    $date_addedon = date('Y-m-d', strtotime($sessadded_on));
			    if(strtotime($today) != strtotime($date_addedon)){
				$this->db->update("sht_user_session_restart", array('isp_uid' => $isp_uid, 'uid' => $uuid, 'added_on' => date('Y-m-d H:i:s')), array('id' => $rowdata->id));
				$nasipQ = $this->db->query("SELECT tb1.radacctid, tb1.username, tb1.nasipaddress, tb1.framedipaddress, tb2.secret FROM radacct as tb1 INNER JOIN nas as tb2 ON(tb1.nasipaddress=tb2.nasname) WHERE tb1.username='".$uuid."' ORDER BY radacctid DESC LIMIT 1");
				if($nasipQ->num_rows() > 0){
				    $nasrowdata = $nasipQ->row();
				    $radacctid = $nasrowdata->radacctid;
				    $username = $nasrowdata->username;
				    $userframedipaddress = $nasrowdata->framedipaddress;
				    $usernasipaddress = $nasrowdata->nasipaddress;
				    $secret = $nasrowdata->secret;
				    $this->disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret);
				    $this->db->query("UPDATE radacct SET acctstoptime='".date('Y-m-d H:i:s')."' WHERE username='".$uuid."' AND acctstoptime IS NULL");
				}
			    }
			}else{
			    $this->db->insert("sht_user_session_restart", array('isp_uid' => $isp_uid, 'uid' => $uuid, 'added_on' => date('Y-m-d H:i:s')));
			    $nasipQ = $this->db->query("SELECT tb1.radacctid, tb1.username, tb1.nasipaddress, tb1.framedipaddress, tb2.secret FROM radacct as tb1 INNER JOIN nas as tb2 ON(tb1.nasipaddress=tb2.nasname) WHERE tb1.username='".$uuid."' ORDER BY radacctid DESC LIMIT 1");
			    if($nasipQ->num_rows() > 0){
				$nasrowdata = $nasipQ->row();
				$radacctid = $nasrowdata->radacctid;
				$username = $nasrowdata->username;
				$userframedipaddress = $nasrowdata->framedipaddress;
				$usernasipaddress = $nasrowdata->nasipaddress;
				$secret = $nasrowdata->secret;
				$this->disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret);
				$this->db->query("UPDATE radacct SET acctstoptime='".date('Y-m-d H:i:s')."' WHERE username='".$uuid."' AND acctstoptime IS NULL");
			    }
			}
		    }
		}
	    }
	}
    }
    public function bill_generation_foruser(){
        ini_set('max_execution_time', '0');

	$isp_uid = '105'; $uuid = '10502070';
        $today = date('d');
        $today_date = '2018-10-01';        
        $date = new DateTime($today_date);
        //$date->modify('+1 day');
        $nextdaydate = $date->format('d');
        $nextdaymonth = $date->format('m');
	$nextdayyear = $date->format('Y');
	$bill_cycleQ = $this->db->query("SELECT * FROM sht_billing_cycle WHERE isp_uid='".$isp_uid."'");
	if($bill_cycleQ->num_rows() > 0){
	    $bcobj = $bill_cycleQ->row();
	    $bill_duedate = date('Y-m-d', strtotime($today_date . " +".$bcobj->billing_due_date." days"));
	}else{
	    $bill_duedate = '0000-00-00';
	}
	
        $subscriberQ = $this->db->query("SELECT tb1.planautorenewal, tb1.uid, tb1.firstname, tb1.lastname, tb1.email, tb1.state, tb1.city, tb1.mobile, tb1.address, tb1.baseplanid, tb1.account_activated_on as active_on, tb1.paidtill_date, DATE(tb1.expiration) as expiration_date, tb1.plan_activated_date, tb1.user_credit_limit, tb1.inactivate_type, tb1.suspended_days, tb2.srvname, tb2.descr, tb2.datacalc, tb2.datalimit, tb2.plan_duration, tb3.gross_amt, tb3.tax, tb3.net_total, tb3.gross_amt FROM sht_users as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid = tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb1.enableuser='1' AND tb1.expired='0' AND tb1.isp_uid='".$isp_uid."' AND tb1.user_plan_type='postpaid' AND tb1.uid='".$uuid."'");
	// AND tb2.plantype='".$plan_type."'
	
	//echo $bill_duedate; echo '<br/>';
	echo $this->db->last_query(); echo '<br/>'; die;
	if($subscriberQ->num_rows() > 0){
	    $this->check_updated_plandetails($isp_uid);
	    foreach($subscriberQ->result() as $subsobj){
		$monthly_pay = 0;
		$uid = $subsobj->uid;
		$billuserArr[] = $uid;
		
		//CHECK FOR BILL ALREADY GENERATED
		$checkforcronQ = $this->db->query("SELECT id FROM sht_subscriber_billing_cron WHERE isp_uid='".$isp_uid."' AND uid='".$uid."' AND DATE(added_on) = '".$today_date."'");
		if($checkforcronQ->num_rows() == 0){
		    $this->db->insert('sht_subscriber_billing_cron', array('isp_uid' => $isp_uid, 'uid' => $uid, 'added_on' => $today_date.' '.date('H:i:s')));

		    //if($uid == '10102231'){
			//echo 'hjhj'; die;
			$plan_activated_date = $subsobj->plan_activated_date;
			$tax = $this->current_taxapplicable($isp_uid);
			$plan_duration = $subsobj->plan_duration;
			$bill_cycle = sprintf("%02d", $bcobj->billing_cycle);
			$bill_date = $nextdayyear.'-'.$nextdaymonth.'-'.$bill_cycle;
			$srvid = $subsobj->baseplanid;
			$billtype = 'montly_pay';
			$plandata_calculatedon = $subsobj->datacalc;
			$plandatalimit = $subsobj->datalimit;
			$gross_amt = $subsobj->gross_amt;
			$planautorenewal = $subsobj->planautorenewal;
			
			$nasipQ = $this->db->query("SELECT tb1.radacctid,tb1.username, tb1.nasipaddress, tb1.framedipaddress, tb2.secret FROM radacct as tb1 INNER JOIN nas as tb2 ON(tb1.nasipaddress=tb2.nasname) WHERE tb1.username='".$uid."' ORDER BY radacctid DESC LIMIT 1");
			if($nasipQ->num_rows() > 0){
			    $nasrowdata = $nasipQ->row();
			    $radacctid = $nasrowdata->radacctid;
			    $username = $nasrowdata->username;
			    $userframedipaddress = $nasrowdata->framedipaddress;
			    $usernasipaddress = $nasrowdata->nasipaddress;
			    $secret = $nasrowdata->secret;
			    //@exec("echo User-Name:=$username,Framed-Ip-Address=$userframedipaddress | radclient -x $usernasipaddress:3799 disconnect $secret");
			    $this->disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret);
			    $this->db->query("UPDATE radacct SET acctstoptime='".date('Y-m-d H:i:s')."' WHERE username='".$uid."' AND acctstoptime IS NULL");
			}
			
			$custom_planpriceArr = $this->getcustom_planprice($isp_uid, $uid, $srvid);
			$custom_planprice = $custom_planpriceArr['gross_amt'];
			if($custom_planprice != ''){
			    $gross_amt = round($custom_planprice / $plan_duration);
			    $plan_price = round($custom_planprice);
			}else{
			    $gross_amt = round($gross_amt + (($gross_amt * $tax)/100));
			    $plan_price = ($gross_amt * $plan_duration);	
			}
			
			$plan_tilldays = ($plan_duration * 30);
			//$user_credit_limit = $subsobj->user_credit_limit;
			$user_credit_limit = ($plan_price + round($plan_price/2));
			//$paidtill_date = $subsobj->paidtill_date;
			
			$pacttimestamp = strtotime($plan_activated_date);
			$todtimestamp = strtotime($today_date);
			$used_netdays = round(abs($todtimestamp - $pacttimestamp) / 86400);
			$plan_cost_perday = round(($plan_price/$plan_tilldays));			
			if($used_netdays > 27){
			    if($custom_planprice != ''){
				$actual_bill_amt = round(($custom_planprice * 100) / 118);
			    }else{
				$actual_bill_amt = round($subsobj->gross_amt);
			    }
			    $cost_tillnow = round($plan_price);    
			}else{
			    if($custom_planprice != ''){
				$gross_plan_cost_perday = ($custom_planprice / 30);
				$actual_bill_amt = round($gross_plan_cost_perday * $used_netdays);
			    }else{
				$gross_plan_cost_perday = ($subsobj->gross_amt / 30);
				$actual_bill_amt = round($gross_plan_cost_perday * $used_netdays);
			    }
			    $cost_tillnow = round($plan_cost_perday * $used_netdays);
			}
			
			
			$billingarr = array(
			    'subscriber_uuid' => $uid,
			    'plan_id' => $srvid,
			    'user_plan_type' => 'postpaid',
			    'bill_starts_from' => "$plan_activated_date",
			    'bill_added_on' => $bill_date.' '. date('H:i:s'),
			    'bill_duedate' => $bill_duedate,
			    'bill_number' => date('jnyHis'),
			    'bill_type' => $billtype,
			    'payment_mode' => '',
			    'actual_amount' => $actual_bill_amt,
			    'discount' => '0',
			    'total_amount' => $cost_tillnow,
			    'isp_uid' => $isp_uid
			);
			//print_r($billingarr); die;
			$this->db->insert('sht_subscriber_billing', $billingarr);
			$billid = $this->db->insert_id();
			
			//Bill Passbook Entry
			$this->passbook_entry($uid, $isp_uid, $billid, $srvid, $cost_tillnow);
			
			$this->db->insert('sht_userplan_datechange_records', array('uid' => $uid, 'isp_uid' => $isp_uid, 'bill_id' => $billid, 'srvid' => $srvid, 'lastplan_activatedate' => $plan_activated_date, 'added_on' => $bill_date.' '. date('H:i:s')));
			
			//Check for Advanced Payment
			$user_advpayments = $this->user_advpayments($uid, $billid, $cost_tillnow, $isp_uid);
			//Update Expiry Date
			$this->update_expirationdate($uid,$user_credit_limit,$plan_cost_perday);
			
			$next_bill_date = date('Y-m-d', strtotime($bill_date.' +1 month'));
			
			$this->db->update('sht_users', array('downlimit' => '0', 'comblimit' => '0', 'next_bill_date' => $next_bill_date, 'bill_duedate' => $bill_duedate), array('uid' => $uid));

			/*
			 *  CHECK FOR PLAN AUTO RENEWAL
			 */
			if($planautorenewal == '0'){
			    $this->db->update('sht_users', array('enableuser' => '0', 'inactivate_type' => 'suspended', 'suspended_days' => '0', 'suspendedon' => date('Y-m-d')), array('uid' => $uid));
			}else{ 
			    //CHECK FOR NEXT CYCLE PLAN
			    $plandatarr = array();
			    $plandatarr['enableuser'] = '1';
			    $plandatarr['inactivate_type'] = '';
			    $plandatarr['suspended_days'] = '0';
			    $plandatarr['suspendedon'] = '0000-00-00';
			    $plandatarr['user_credit_limit'] = $user_credit_limit;
			    $plandatarr['plan_activated_date'] = $bill_date;
			    $plandatarr['plan_changed_datetime'] = date('Y-m-d H:i:s');
			    $nextPlanQ = $this->db->query("SELECT tb1.id, tb1.baseplanid, tb1.user_plan_type, tb2.datacalc, tb2.datalimit, tb2.plan_duration, tb3.net_total, tb3.gross_amt FROM sht_nextcycle_userplanassoc as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid = tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE uid='".$uid."' AND status='0' ORDER BY id DESC LIMIT 1");
			    if($nextPlanQ->num_rows() > 0){
				$nextPlan_rowdata = $nextPlanQ->row();
				$nid = $nextPlan_rowdata->id;
				$newsrvid = $nextPlan_rowdata->baseplanid;
				$newsrvid_calcon = $nextPlan_rowdata->datacalc;
				$newsrvid_datalimt = $nextPlan_rowdata->datalimit;
				$newuser_plan_type = $nextPlan_rowdata->user_plan_type;
				
				$newplan_duration = $nextPlan_rowdata->plan_duration;
				$npgross_amt = $nextPlan_rowdata->gross_amt;
				$nextplan_tilldays = ($newplan_duration * 30);

				$custom_planpriceArr = $this->getcustom_planprice($isp_uid, $uid, $newsrvid);
				$custom_planprice = $custom_planpriceArr['gross_amt'];
				if($custom_planprice != ''){
				    $this->updatecustom_planprice($isp_uid, $uid, $newsrvid);
				    $npgross_amt = round($custom_planprice / $newplan_duration);
				    $nextsrvid_price = round($custom_planprice);
				}else{
				    $npgross_amt = round($npgross_amt + (($npgross_amt * $tax)/100));
				    $nextsrvid_price = ($npgross_amt * $newplan_duration);
				}
				
				$nphalf_gross_amt = round($nextsrvid_price / 2);
				$nextsrvid_cost_perday = round($nextsrvid_price/$nextplan_tilldays);
				$user_newcreditlimit = ($nextsrvid_price + $nphalf_gross_amt);
				$plandatarr['user_credit_limit'] = $user_newcreditlimit;

				// CHANGE TO PREPAID PLAN
				if($newuser_plan_type == 'prepaid'){
				    $user_walletamt = $this->userbalance_amount($uid); //check again wallet balance
				    $payment_mode = ''; $receipt_received = '0'; $adjusted_amount = 0;
				    $prebillingarr = array(
					    'subscriber_uuid' => $uid,
					    'plan_id' => $newsrvid,
					    'user_plan_type' => 'prepaid',
					    'bill_added_on' => $bill_date.' '.date('H:i:s'),
					    'bill_duedate' => $bill_duedate,
					    'bill_number' => date('jnyHis'),
					    'bill_type' => 'montly_pay',
					    'payment_mode' => '',
					    'receipt_received' => '0',
					    'actual_amount' => $nextsrvid_price,
					    'adjusted_amount' => '0',
					    'discount' => '0',
					    'total_amount' => $nextsrvid_price,
					    'isp_uid' => $isp_uid
				    );
				    $this->db->insert('sht_subscriber_billing', $prebillingarr);
				    $prebillid = $this->db->insert_id();
				    $prePassbookArr = array(
					    'isp_uid' => $isp_uid,
					    'subscriber_uuid' => $uid,
					    'billing_id' => $prebillid,
					    'srvid' => $newsrvid,
					    'plan_cost' => $nextsrvid_price,
					    'added_on'	=> $bill_date.' '.date('H:i:s')
				    );
				    $this->db->insert('sht_subscriber_passbook', $prePassbookArr);
				    
				    //$pnext_bill_date = date('Y-m-d',strtotime("+$nextplan_tilldays day"));
				    $pnextdate = date('Y-m-d', strtotime('+1 month'));
				    $pnextdate = new DateTime($pnextdate);
				    $pnextdate->setDate($pnextdate->format('Y'), $pnextdate->format('m'), $bcobj->billing_cycle);
				    $pnext_month = $pnextdate->format('Y-m-d');
				    $pnext_bill_date = $pnext_month;
				    $plandatarr['next_bill_date'] = $pnext_bill_date;
				}
				
				$plandatarr['user_plan_type'] = $newuser_plan_type;
				if($newsrvid_calcon == '1'){
				    $plandatarr['downlimit'] = $newsrvid_datalimt;
				    $plandatarr['comblimit'] = '0';
				}elseif($newsrvid_calcon == '2'){
				    $plandatarr['downlimit'] = '0';
				    $plandatarr['comblimit'] = $newsrvid_datalimt;
				}else{
				    $plandatarr['downlimit'] = '0';
				    $plandatarr['comblimit'] = '0';
				}
				$plandatarr['baseplanid'] = $newsrvid;                                
				$this->db->update('sht_users', $plandatarr, array('uid' => $uid));
				
				//SET BONUS AMT & Balance Amt TO 0 OF PREVIOUS PLAN & Add Amt to Wallet.
				$total_advpay = $this->user_advpaybalance($uid);
				if($total_advpay > 0){
				    $this->addtowallet($uid, $isp_uid, $total_advpay);
				    $this->db->update('sht_advance_payments', array('bonus_amount' => '0', 'balance_left' => '0'), array('uid' => $uid, 'srvid' => $srvid));
				}
				$this->update_expirationdate($uid,$user_newcreditlimit,$nextsrvid_cost_perday);
				$this->db->update('sht_nextcycle_userplanassoc', array('status' => '1'), array('id' => $nid));
				
			    }else{
				if($plandata_calculatedon == '1'){
				    $plandatarr['downlimit'] = $plandatalimit;
				    $plandatarr['comblimit'] = '0';
				}elseif($plandata_calculatedon == '2'){
				    $plandatarr['downlimit'] = '0';
				    $plandatarr['comblimit'] = $plandatalimit;
				}else{
				    $plandatarr['downlimit'] = '0';
				    $plandatarr['comblimit'] = '0';
				}
				
				$this->db->update('sht_users', $plandatarr, array('uid' => $uid));
			    }
			}
			$custfupQ = $this->db->query("SELECT id FROM sht_customize_plan_speed WHERE uid='".$uid."' AND isp_uid='".$isp_uid."' AND status='1'");
			if($custfupQ->num_rows() > 0){
			    $this->db->update('sht_customize_plan_speed', array('status' => '0', 'modified_on' => $bill_date.' '. date('H:i:s'), 'modified_by' => 'SuperAdmin'), array('uid' => $uid));
			}
			$this->db->update('sht_usertopupassoc', array('status' => '0', 'terminate' => '1'), array('uid' => $uid, 'topuptype' => '1'));
			//sleep(60);
			
			//$wallet_bal = $this->userbalance_amount($uid);                                
			//$this->mail_format($uid);
		    //}
		}
		sleep(5);
	    }
	}
    }
    

    public function disconnect_alluserineternet(){
	$bill_cycleQ = $this->db->query("SELECT tb2.isp_uid FROM sht_isp_admin as tb2 WHERE tb2.decibel_account = 'paid'");
	if($bill_cycleQ->num_rows() > 0){
	    foreach($bill_cycleQ->result() as $bobj){
		$isp_uid = $bobj->isp_uid;
		
		$usersQ = $this->db->query("SELECT uid FROM sht_users WHERE isp_uid='".$isp_uid."'");
		if($usersQ->num_rows() > 0){
		    foreach($usersQ->result() as $uobj){
			$uuid = $uobj->uid;
			echo $isp_uid .' => '.$uuid. '<br/>';
			$nasipQ = $this->db->query("SELECT tb1.radacctid,tb1.username, tb1.nasipaddress, tb1.framedipaddress, tb2.secret FROM radacct as tb1 INNER JOIN nas as tb2 ON(tb1.nasipaddress=tb2.nasname) WHERE tb1.username='".$uuid."' ORDER BY radacctid DESC LIMIT 1");
			if($nasipQ->num_rows() > 0){
			    $nasrowdata = $nasipQ->row();
			    $radacctid = $nasrowdata->radacctid;
			    $username = $nasrowdata->username;
			    $userframedipaddress = $nasrowdata->framedipaddress;
			    $usernasipaddress = $nasrowdata->nasipaddress;
			    $secret = $nasrowdata->secret;
			    $this->disconnectUserApi($username, $userframedipaddress, $usernasipaddress, $secret);
			    $this->db->query("UPDATE radacct SET acctstoptime='".date('Y-m-d H:i:s')."' WHERE username='".$uuid."' AND acctstoptime IS NULL");
			}
		    }
		}
		sleep(10);
	    }
	}
    }
}
?>