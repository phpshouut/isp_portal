<?php

class plan_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function misc_data($misctype) {
        $condarr = array('misc_type' => $misctype);
        $query = $this->db->get_where(SHTMISC, $condarr);
        $rowarr = $query->row_array();
        $misc_id = $rowarr['id'];
        $condarr1 = array('misc_id' => $misc_id);
        $query1 = $this->db->select('id,misc_name')->get_where(SHTMISCDETAIL, $condarr1);
        return $query1->result();
        // echo "<pre>"; print_R($query1->result());die;
        //$this->db->query("select id from ")
    }

    public function countrydetails(){
	$data = array();
	$ispcountryQ = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".ISPID."'");
	if($ispcountryQ->num_rows() > 0){
	    $crowdata = $ispcountryQ->row();
	    $countryid = $crowdata->country_id;
    
	    $countryQ = $this->db->query("SELECT * FROM sht_countries WHERE id='".$countryid."'");
	    if($countryQ->num_rows() > 0){
		$rowdata = $countryQ->row();
		$currid = $rowdata->currency_id;
		
		$currencyQ = $this->db->get_where('sht_currency', array('currency_id' => $currid));
		if($currencyQ->num_rows() > 0){
		    $currobj = $currencyQ->row();
		    $currsymbol = $currobj->currency_symbol;
		    $data['currency'] = $currsymbol;
		}
		$data['demo_cost'] = $rowdata->demo_cost;
		$data['cost_per_user'] = $rowdata->cost_per_user;
		$data['cost_per_location'] = $rowdata->cost_per_location;
	    }
	}
	return $data;
    }

    
    public function listing_plan() {
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";
        $cond = $this->dept_plan_condition();
     //   echo $cond;die;
        $data = array();
        $qlmt = LIMIT; $qofset = SOFFSET;
		$limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
        if ($cond != '') {

            $cond = ($cond == "allindia") ? "" : $cond;
            //echo "====>>>".$cond; die;

            $query = $this->db->query("SELECT ss.plantype,ss.is_private,ss.datalimit,ss.srvid,ss.srvname,ss.downrate,ss.uprate,ss.enableplan
				      ,spp.net_total,spp.gross_amt,spp.region_type,(SELECT COUNT(id) FROM sht_users su WHERE su.baseplanid=ss.srvid AND su.enableuser='1') as usercount FROM `sht_services` ss
                        LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE ss.is_deleted='0' and ss.plantype!='0' {$cond} {$ispcond}  order by srvid desc LIMIT $qofset,$qlmt");
            // echo $this->db->last_query(); //die;
            $data = $query->result();
        } else {
            $data == array();
        }
        return $data;
    }

    public function listing_topup() {
      $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";
        $cond = $this->dept_topup_condition();
         $qlmt = LIMIT; $qofset = SOFFSET;
		$limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
        $data = array();
        if ($cond != '') {
            $cond = ($cond == "allindia") ? "" : $cond;
            $query = $this->db->query("SELECT ss.topuptype,ss.is_private,ss.datalimit,ss.srvid,ss.downrate,ss.uprate,ss.enableplan,ss.srvname
				      ,spp.net_total,spp.gross_amt,spp.payment_type,(SELECT COUNT(distinct(su.uid)) FROM sht_usertopupassoc su inner join sht_users sut on (su.uid=sut.uid) WHERE su.topup_id=ss.srvid AND su.terminate='0' and sut.enableuser='1') AS usercount FROM `sht_services` ss
                        LEFT JOIN `sht_topup_pricing` spp ON (ss.srvid=spp.srvid) WHERE ss.is_deleted='0' AND ss.topuptype!='0' {$cond} {$ispcond} order by srvid desc LIMIT $qofset,$qlmt");
            // echo $this->db->last_query(); die;
            $data = $query->result();
        } else {
            $data == array();
        }
        return $data;
    }
    
    public function topup_getdays($topupid,$type)
    {
        $data=array();
        if($type=="speed")
        {
            $query=$this->db->query("select days from sht_speedtopup where srvid='".$topupid."' order by id asc");
            foreach($query->result() as $val)
            {
                $daysarr[]=substr($val->days,0,1);
            }
            $days=implode(",",$daysarr);
            $data['days']=$days;
            $data['unaccounacy']="";
        }
        else
        {
             $query=$this->db->query("select days,datacalcpercent from sht_dataunaccountancy where srvid='".$topupid."' order by id asc");
            foreach($query->result() as $val)
            {
                $unacct = $val->datacalcpercent;
                $daysarr[]=substr($val->days,0,1);
            }
            $days=implode(",",$daysarr);
            $data['days']=$days;
            $data['unaccounacy']=$unacct;
        }
        return $data;
        
    }

    public function add_plan_detail() {
        $postdata = $this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
        if ($postdata['plan_type_radio'] == "UP") {
            $dwnld_rate = $this->convertToBytes($postdata['dwnld_rate'] . "KB");
            $upld_rate = $this->convertToBytes($postdata['upld_rate'] . "KB");

            //`plan_name``plan_desc``plan_validity``is_usable``plan_type``dwnld_rate``upld_rate``time_limit``time_calculated_on``data_limit`
            //`data_calculated_on``is_post_fup``postfup_dwnld_rate``postfup_upld_rate`
            $tabledata = array("srvname" => $postdata['plan_name'], "descr" => $postdata['plan_desc'],"plan_duration"=>$postdata['plan_duration'],
                "enableplan" => 0, "plantype" => 1, "datacalc" => 0, "timecalc" => 0
                , "fupuprate" => 0, "fupdownrate" => 0, "datalimit" => 0, "timelimit" => 0, "downrate" => $dwnld_rate,"isp_uid"=>$isp_uid,
                "uprate" => $upld_rate,"is_private"=>$postdata['access_type']);
        } else if ($postdata['plan_type_radio'] == "TP") {

            $timelimit = $this->timeToSeconds($postdata['timelimit']);
            $dwnld_rate = $this->convertToBytes($postdata['dwnld_rate'] . "KB");
            $upld_rate = $this->convertToBytes($postdata['upld_rate'] . "KB");
            // `downrate``uprate``isspeedtopup``isdataunacnttopup``isunlimitedplan``istimeplan``isfupplan``isdataplan``timecalc``datacalc``fupuprate``fupdownrate``timelimit``datalimit`

            $timecal = (isset($postdata['time_calcn_radio']) && $postdata['time_calcn_radio'] != '') ? ($postdata['time_calcn_radio'] == "LT") ? 1 : 2 : 0;
            $tabledata = array("srvname" => $postdata['plan_name'], "descr" => $postdata['plan_desc'],"plan_duration"=>$postdata['plan_duration'],
                "enableplan" => 0, "plantype" => 2, "datacalc" => 0,
                "fupuprate" => 0, "fupdownrate" => 0, "datalimit" => 0, "downrate" => $dwnld_rate,"isp_uid"=>$isp_uid,
                "uprate" => $upld_rate, "timelimit" => $timelimit, "timecalc" => $timecal,"is_private"=>$postdata['access_type']
            );
        } else if ($postdata['plan_type_radio'] == "FP") {
            $datalimit = $this->convertToBytes($postdata['data_limit'] . "GB");
            $dwnld_rate = $this->convertToBytes($postdata['dwnld_rate'] . "KB");
            $upld_rate = $this->convertToBytes($postdata['upld_rate'] . "KB");
            $fupdwnld_rate = $this->convertToBytes($postdata['fup_dwnl_rate'] . "KB");
            $fupupld_rate = $this->convertToBytes($postdata['fup_upld_rate'] . "KB");
            $datecal = (isset($postdata['data_calcn_radio']) && $postdata['data_calcn_radio'] != '') ? ($postdata['data_calcn_radio'] == "DO") ? 1 : 2 : 0;
            $tabledata = array("srvname" => $postdata['plan_name'], "descr" => $postdata['plan_desc'],"plan_duration"=>$postdata['plan_duration'],
                "enableplan" => 0, "plantype" => 3, "downrate" => $dwnld_rate,
                "uprate" => $upld_rate, "datalimit" => $datalimit, "timecalc" => 0, "datacalc" => $datecal,"isp_uid"=>$isp_uid
                , "fupuprate" => $fupupld_rate, "fupdownrate" => $fupdwnld_rate,"is_private"=>$postdata['access_type']);
        } else {
            $datalimit = $this->convertToBytes($postdata['data_limit'] . "GB");
            $dwnld_rate = $this->convertToBytes($postdata['dwnld_rate'] . "KB");
            $upld_rate = $this->convertToBytes($postdata['upld_rate'] . "KB");
             $datecal = (isset($postdata['data_calcn_radio']) && $postdata['data_calcn_radio'] != '') ? ($postdata['data_calcn_radio'] == "DO") ? 1 : 2 : 0;
            $tabledata = array("srvname" => $postdata['plan_name'], "descr" => $postdata['plan_desc'],"plan_duration"=>$postdata['plan_duration'],
                "enableplan" => 0, "plantype" => 4, "fupuprate" => 0, "fupdownrate" => 0, "downrate" => $dwnld_rate,"isp_uid"=>$isp_uid,
                "uprate" => $upld_rate, "datalimit" => $datalimit,"datacalc" => $datecal,"is_private"=>$postdata['access_type']);
        }
        if (isset($postdata['plan_id']) && $postdata['plan_id'] != '') {
	    if($postdata['change_cycle']==1)
	    {
		$tabledata=array("srvname" => $postdata['plan_name'], "descr" => $postdata['plan_desc'],"planchange_isnextcycle"=>1);
		$this->add_next_cycleplandata($postdata);
	    }
	    else
	    {
		$tabledata1=array("planchange_isnextcycle"=>0);
		$tabledata=array_merge($tabledata,$tabledata1);
		if($postdata['plan_type_radio'] == "FP" || $postdata['plan_type_radio'] == "DP")
		{
		$this->add_planuserbalancing($postdata);
		}
		$this->db->where('srvid', $postdata['plan_id']);
                    $this->db->delete('sht_plannextcycle');
		    
		    $this->db->where('baseplanid', $postdata['plan_id']);
                    $this->db->delete('sht_customize_plan_speed');
		
	    }
            $this->db->update(SHTSERVICES, $tabledata, array('srvid' => $postdata['plan_id']));
            $this->plan_enable_setting($postdata['plan_id'], 'PLANDETAIL');
            return $postdata['plan_id'];
        } else {
            $this->db->insert(SHTSERVICES, $tabledata);
            $id = $this->db->insert_id();
            $this->plan_enable_setting($id, 'PLANDETAIL');
            $this->add_system_serviceid($id);
            return $id;
        }
    }
    
    public function add_planuserbalancing($postdata)
    {
	//echo "<pre>"; print_R($postdata);//die;
	$prevdata=$this->convertTodata($postdata['prvdatalimit'] . "GB");
	$column=(isset($postdata['data_calcn_radio']) && $postdata['data_calcn_radio'] != '') ? ($postdata['data_calcn_radio'] == "DO") ? "downlimit" : "comblimit" : "comblimit";
	//echo $prevdata.":::".$postdata['data_limit'];
	if(isset($postdata['data_calcn_radio']) && $postdata['data_calcn_radio'] != '')
	{
	if($prevdata!=$postdata['data_limit'])
	{
	    $sign='+';
	    $newdata=$postdata['data_limit']-$prevdata;
	    if($newdata<0)
	    {
		$sign="-";
	    }
	    //echo $newdata; //die;
	    $comblimit=$this->convertToBytes($newdata . "GB");
	    $query=$this->db->query("select uid from sht_users where baseplanid='".$postdata['plan_id']."'");
	    
	    foreach($query->result() as $vald)
	    {
		$query1=$this->db->query("update sht_users set $column=$column+{$comblimit} where uid='".$vald->uid."'");
		
	    }
	}
	}
	return true;
	
	
    }
    
    
    public function add_next_cycleplandata($postdata)
    {
	 $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
	 if ($postdata['plan_type_radio'] == "UP") {
            $dwnld_rate = $this->convertToBytes($postdata['dwnld_rate'] . "KB");
            $upld_rate = $this->convertToBytes($postdata['upld_rate'] . "KB");
	$tabledata = array("plan_duration"=>$postdata['plan_duration']
                 
                , "fupuprate" => 0, "fupdownrate" => 0, "datalimit" => 0,"srvid"=>$postdata['plan_id'], "timelimit" => 0, "downrate" => $dwnld_rate,"isp_uid"=>$isp_uid,
                "uprate" => $upld_rate,"is_private"=>$postdata['access_type']);
        } else if ($postdata['plan_type_radio'] == "TP") {

            $timelimit = $this->timeToSeconds($postdata['timelimit']);
            $dwnld_rate = $this->convertToBytes($postdata['dwnld_rate'] . "KB");
            $upld_rate = $this->convertToBytes($postdata['upld_rate'] . "KB");
            // `downrate``uprate``isspeedtopup``isdataunacnttopup``isunlimitedplan``istimeplan``isfupplan``isdataplan``timecalc``datacalc``fupuprate``fupdownrate``timelimit``datalimit`

            $timecal = (isset($postdata['time_calcn_radio']) && $postdata['time_calcn_radio'] != '') ? ($postdata['time_calcn_radio'] == "LT") ? 1 : 2 : 0;
            $tabledata = array("plan_duration"=>$postdata['plan_duration'],
                
                "fupuprate" => 0, "fupdownrate" => 0, "datalimit" => 0, "downrate" => $dwnld_rate,"isp_uid"=>$isp_uid,
                "uprate" => $upld_rate, "timelimit" => $timelimit,"srvid"=>$postdata['plan_id'], "timecalc" => $timecal,"is_private"=>$postdata['access_type']
            );
        } else if ($postdata['plan_type_radio'] == "FP") {
            $datalimit = $this->convertToBytes($postdata['data_limit'] . "GB");
            $dwnld_rate = $this->convertToBytes($postdata['dwnld_rate'] . "KB");
            $upld_rate = $this->convertToBytes($postdata['upld_rate'] . "KB");
            $fupdwnld_rate = $this->convertToBytes($postdata['fup_dwnl_rate'] . "KB");
            $fupupld_rate = $this->convertToBytes($postdata['fup_upld_rate'] . "KB");
            $datecal = (isset($postdata['data_calcn_radio']) && $postdata['data_calcn_radio'] != '') ? ($postdata['data_calcn_radio'] == "DO") ? 1 : 2 : 0;
            $tabledata = array("plan_duration"=>$postdata['plan_duration'],
                "downrate" => $dwnld_rate,
                "uprate" => $upld_rate, "datalimit" => $datalimit, "isp_uid"=>$isp_uid
                , "fupuprate" => $fupupld_rate,"srvid"=>$postdata['plan_id'], "fupdownrate" => $fupdwnld_rate,"is_private"=>$postdata['access_type']);
        } else {
            $datalimit = $this->convertToBytes($postdata['data_limit'] . "GB");
            $dwnld_rate = $this->convertToBytes($postdata['dwnld_rate'] . "KB");
            $upld_rate = $this->convertToBytes($postdata['upld_rate'] . "KB");
             $datecal = (isset($postdata['data_calcn_radio']) && $postdata['data_calcn_radio'] != '') ? ($postdata['data_calcn_radio'] == "DO") ? 1 : 2 : 0;
            $tabledata = array("plan_duration"=>$postdata['plan_duration'],
                 "fupuprate" => 0, "fupdownrate" => 0, "downrate" => $dwnld_rate,"isp_uid"=>$isp_uid,
                "uprate" => $upld_rate,"srvid"=>$postdata['plan_id'], "datalimit" => $datalimit,"is_private"=>$postdata['access_type']);
        }
	
	$query=$this->db->query("select id from sht_plannextcycle where srvid='".$postdata['plan_id']."'");
	
	 if ($query->num_rows()>0) {
	    $this->db->update('sht_plannextcycle', $tabledata, array('srvid' => $postdata['plan_id']));
	    return $postdata['plan_id'];
        } else {
            $this->db->insert('sht_plannextcycle', $tabledata);
           
           return $postdata['plan_id'];
        }
	
	
	
	
    }

    public function add_topup_detail() {
        $postdata = $this->input->post();
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
       // echo "<pre>"; print_R($postdata); die;
        if ($postdata['topup_type_radio'] == 1) {
            // $dwnld_rate=$this->convertToBytes($postdata['dwnld_rate']."KB");
            // $upld_rate=$this->convertToBytes($postdata['upld_rate']."KB");
            $datalimit = $this->convertToBytes($postdata['topup_data'] . "GB");
            // `topup_name``topup_desc``topup_validity``is_usable``topup_type``data``data_type_added`
            $tabledata = array("srvname" => $postdata['topup_name'], "descr" => $postdata['topup_desc'], "topuptype" => 1,
                "enableplan" => 0, "datacalc" => 0, "datalimit" => $datalimit,"isp_uid"=>$isp_uid,"is_private"=>$postdata['access_type']
            );
            if (isset($postdata['topup_id']) && $postdata['topup_id'] != '') {
                $this->db->update(SHTSERVICES, $tabledata, array('srvid' => $postdata['topup_id']));
                $this->topup_enable_setting($postdata['topup_id'], 'TOPUPDETAIL');
                return $postdata['topup_id'];
            } else {
                $this->db->insert(SHTSERVICES, $tabledata);
                $id = $this->db->insert_id();
                $this->topup_enable_setting($id, 'TOPUPDETAIL');
                return $id;
            }
        } else if ($postdata['topup_type_radio'] == 2) {
            $days = implode(",", array_filter($postdata['unacctng_days']));
            $tabledata = array("srvname" => $postdata['topup_name'], "descr" => $postdata['topup_desc'],
                "enableplan" => 0, "topuptype" => 2,"isp_uid"=>$isp_uid,"is_private"=>$postdata['access_type']);
            if (isset($postdata['topup_id']) && $postdata['topup_id'] != '') {
                $this->db->update(SHTSERVICES, $tabledata, array('srvid' => $postdata['topup_id']));
                $this->topup_enable_setting($postdata['topup_id'], 'TOPUPDETAIL');
                $days = array_filter($postdata['unacctng_days']);
                $alreadyarr = array();
                $query1 = $this->db->query("select id from sht_dataunaccountancy where  srvid='" . $postdata['topup_id'] . "'");
                foreach ($query1->result() as $val1) {
                    $alreadyarr[] = $val1->id;
                }

                $newarr = array();
                foreach ($days as $val) {
                    //  `srvid``days``starttime``stoptime``datacalcpercent``created_on`
                    $query = $this->db->query("select id from sht_dataunaccountancy where days='" . $val . "' and srvid='" . $postdata['topup_id'] . "'");
                    if ($query->num_rows() > 0) {
                        $rowarr = $query->row_array();
                        $newarr[] = $rowarr['id'];
                        $tabledata1 = array("srvid" => $postdata['topup_id'], "days" => $val, "starttime" => $postdata['unacctstart_time'].":00:00", "stoptime" => $postdata['unacctstop_time'].":00:00",
                            "datacalcpercent" => $postdata['unaccounting']);
                        $this->db->update(SHTDATACCTN, $tabledata1, array('days' => $val, "srvid" => $postdata['topup_id']));
                    } else {
                        $tabledata1 = array("srvid" => $postdata['topup_id'], "days" => $val, "starttime" => $postdata['unacctstart_time'].":00:00", "stoptime" => $postdata['unacctstop_time'].":00:00",
                            "datacalcpercent" => $postdata['unaccounting'], "created_on" => date("Y-m-d H:i:s"));
                        $this->db->insert(SHTDATACCTN, $tabledata1);
                        $newarr[] = $this->db->insert_id();
                        ;
                    }
                }


                $delarr = array();
                $delarr = array_diff($alreadyarr, $newarr);

                foreach ($delarr as $val) {
                    $this->db->where('id', $val);
                    $this->db->delete(SHTDATACCTN);
                }
                return $postdata['topup_id'];
            } else {
                $this->db->insert(SHTSERVICES, $tabledata);
                $id = $this->db->insert_id();
                $this->topup_enable_setting($id, 'TOPUPDETAIL');
                $days = array_filter($postdata['unacctng_days']);
                foreach ($days as $val) {
                    //  `srvid``days``starttime``stoptime``datacalcpercent``created_on`
                    $tabledata1 = array("srvid" => $id, "days" => $val, "starttime" => $postdata['unacctstart_time'].":00:00", "stoptime" => $postdata['unacctstop_time'].":00:00",
                        "datacalcpercent" => $postdata['unaccounting'], "created_on" => date("Y-m-d H:i:s"));
                    $this->db->insert(SHTDATACCTN, $tabledata1);
                }
                return $id;
            }
        } else if ($postdata['topup_type_radio'] == 3) {

            $days = array_filter($postdata['speedboost_days']);
            $downrate = $this->convertToBytes($postdata['dwnld_speed'] . "KB");
            $uprate = $this->convertToBytes($postdata['upld_speed'] . "KB");
            $tabledata = array("srvname" => $postdata['topup_name'], "descr" => $postdata['topup_desc'],
                "enableplan" => 0, "downrate" => $downrate, "uprate" => $uprate, "topuptype" => 3,"isp_uid"=>$isp_uid,"is_private"=>$postdata['access_type']);
            if (isset($postdata['topup_id']) && $postdata['topup_id'] != '') {
                $this->db->update(SHTSERVICES, $tabledata, array('srvid' => $postdata['topup_id']));

                $this->topup_enable_setting($postdata['topup_id'], 'TOPUPDETAIL');

                $alreadyarr = array();
                $query1 = $this->db->query("select id from sht_speedtopup where  srvid='" . $postdata['topup_id'] . "'");
                foreach ($query1->result() as $val1) {
                    $alreadyarr[] = $val1->id;
                }
                $newarr = array();
                foreach ($days as $val) {

                    $query = $this->db->query("select id from sht_speedtopup where days='" . $val . "' and srvid='" . $postdata['topup_id'] . "'");
                    if ($query->num_rows() > 0) {
                        $rowarr = $query->row_array();
                        $newarr[] = $rowarr['id'];
                        $tabledata1 = array("srvid" => $postdata['topup_id'], "days" => $val, "starttime" => $postdata['speed_boost_start'].":00:00", "stoptime" => $postdata['speed_boost_stop'].":00:00");
                        $this->db->update(SHTSPEEDTOPUP, $tabledata1, array('days' => $val, "srvid" => $postdata['topup_id']));
                    } else {
                        $tabledata1 = array("srvid" => $postdata['topup_id'], "days" => $val, "starttime" => $postdata['speed_boost_start'].":00:00", "stoptime" => $postdata['speed_boost_stop'].":00:00",
                            "created_on" => date("Y-m-d H:i:s"));
                        $this->db->insert(SHTSPEEDTOPUP, $tabledata1);
                        $newarr[] = $this->db->insert_id();
                        ;
                    }
                }
                $delarr = array();
                $delarr = array_diff($alreadyarr, $newarr);
                foreach ($delarr as $val) {
                    $this->db->where('id', $val);
                    $this->db->delete(SHTSPEEDTOPUP);
                }
                return $postdata['topup_id'];
            } else {
                $this->db->insert(SHTSERVICES, $tabledata);
                $id = $this->db->insert_id();
                $this->topup_enable_setting($id, 'TOPUPDETAIL');
                foreach ($days as $val) {

                    $tabledata1 = array("srvid" => $id, "days" => $val, "starttime" => $postdata['speed_boost_start'].":00:00", "stoptime" => $postdata['speed_boost_stop'].":00:00",
                        "created_on" => date("Y-m-d H:i:s"));
                    $this->db->insert(SHTSPEEDTOPUP, $tabledata1);
                }
                return $id;
            }
        }
    }

    public function add_plan_setting() {
        $postdata = $this->input->post();
	  $dlburstlimit = $this->convertToBytes($postdata['dwnld_burst'] . "KB");
        $dlburstthreshold = $this->convertToBytes($postdata['dwnld_threshold'] . "KB");
        $ulburstlimit = $this->convertToBytes($postdata['upld_burst'] . "KB");
        $ulburstthreshold = $this->convertToBytes($postdata['upld_threshold'] . "KB");
        $bursttime = $postdata['burst_time'];
	 $tabledata = array("dlburstlimit" => $dlburstlimit, "dlburstthreshold" => $dlburstthreshold, "ulburstlimit" => $ulburstlimit,
            "ulburstthreshold" => $ulburstthreshold, "bursttime" => $bursttime, "priority" => $postdata['burst_priority'],
            "enableburst" => $postdata['is_burst_enable']);
        // echo "<pre>"; print_R($postdata);die;
	$query=$this->db->query("select planchange_isnextcycle from sht_services where srvid='".$postdata['plan_id']."'");
	$rowarr=$query->row_array();
	if($rowarr['planchange_isnextcycle']==1)
	{
	      $this->db->update('sht_plannextcycle', $tabledata, array('srvid' => $postdata['plan_id']));
	     return $postdata['plan_id'];
	}
	else{
      
        //
       
        if (isset($postdata['plan_id']) && $postdata['plan_id'] != '') {
            $this->db->update(SHTSERVICES, $tabledata, array('srvid' => $postdata['plan_id']));
            $this->plan_enable_setting($postdata['plan_id'], 'PLANSETTING');
            return $postdata['plan_id'];
        }
	}
    }
    
    
    public function add_pricing() {
        $postdata = $this->input->post();
        $tabledata = array('srvid' => $postdata['plan_id'], "gross_amt" => $postdata['gross_amt'], "tax" => $postdata['tax'], "net_total" => $postdata['net_amount'], "created_on" => date("Y-m-d H:i:s")
        );
        if (isset($postdata['plan_id']) && $postdata['plan_id'] != '') {
            $query = $this->db->query("select gross_amt from " . SHTPLANPRICING . " where srvid='" . $postdata['plan_id'] . "'");
            if ($query->num_rows() > 0) {
                $this->db->update(SHTPLANPRICING, $tabledata, array('srvid' => $postdata['plan_id']));
                $this->plan_enable_setting($postdata['plan_id'], 'PLANPRICING');
            } else {
                $this->db->insert(SHTPLANPRICING, $tabledata);
                $this->plan_enable_setting($postdata['plan_id'], 'PLANPRICING');
            }

            return $postdata['plan_id'];
        }
    }

    public function add_topuppricing() {
        /*  $postdata=$this->input->post();
          $tabledata=array("pricing_type"=>$postdata['plan_pricing_radio'],"gross_amt"=>$postdata['gross_amt'],"tax"=>$postdata['tax'],"net_amt"=>$postdata['net_amount']
          );
          if(isset($postdata['topup_id']) && $postdata['topup_id'] != ''){
          $this->db->update(SHTTOPUP, $tabledata, array('id' => $postdata['topup_id']));
          return  $postdata['topup_id'];
          } */


        $postdata = $this->input->post();
        $gross_amt = (isset($postdata['gross_amt'])) ? $postdata['gross_amt'] : "";
        $tax = (isset($postdata['tax'])) ? $postdata['tax'] : "";
        $net_amount = (isset($postdata['net_amount'])) ? $postdata['net_amount'] : "";
        $tabledata = array('srvid' => $postdata['topup_id'], "gross_amt" => $gross_amt,
            "payment_type" => $postdata['plan_pricing_radio'], "tax" => $tax, "net_total" => $net_amount, "created_on" => date("Y-m-d H:i:s")
        );
        if (isset($postdata['topup_id']) && $postdata['topup_id'] != '') {
            $query = $this->db->query("select gross_amt from " . SHTTOPUPPRICING . " where srvid='" . $postdata['topup_id'] . "'");
            if ($query->num_rows() > 0) {
                $this->db->update(SHTTOPUPPRICING, $tabledata, array('srvid' => $postdata['topup_id']));
                // echo 'sssssssssssaaaa'; die;
                $this->topup_enable_setting($postdata['topup_id'], 'TOPUPPRICING');
            } else {
                $this->db->insert(SHTTOPUPPRICING, $tabledata);
                // echo 'ssssssss'; die;
                $this->topup_enable_setting($postdata['topup_id'], 'TOPUPPRICING');
            }

            return $postdata['topup_id'];
        }
    }

    public function convertToBytes($from) {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number * 1024;
            case "MB":
                return $number * 1024 * 1024;
            case "GB":
                return $number * 1024 * 1024 * 1024;
            case "TB":
                return $number * 1024 * 1024 * 1024 * 1024;
            case "PB":
                return $number * 1024 * 1024 * 1024 * 1024 * 1024;
            default:
                return $from;
        }
    }

    public function convertTodata($from) {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number / 1024;
            case "MB":
                return $number / pow(1024, 2);
            case "GB":
                return $number / pow(1024, 3);
            case "TB":
                return $number / pow(1024, 4);
            case "PB":
                return $number / pow(1024, 5);
            default:
                return $from;
        }
    }

    function timeToSeconds($time) {
        $timeExploded = explode(':', $time);
        if (isset($timeExploded[2])) {
            return $timeExploded[0] * 3600 + $timeExploded[1] * 60 + $timeExploded[2];
        }
        return $timeExploded[0] * 3600 + $timeExploded[1] * 60;
    }

    public function plan_data($id) {
    $data=array();
        //  $condarr=array('id'=>$id);
        $query = $this->db->query("SELECT ss.isp_uid,ss.planchange_isnextcycle,ss.srvid,ss.is_private,ss.plantype,ss.srvname,ss.descr,ss.plan_duration,ss.enableplan,ss.downrate,ss.uprate,ss.timelimit,
				  ss.timecalc,ss.datalimit,ss.datacalc,ss.fupdownrate,ss.fupuprate,ss.enableburst,ss.dlburstlimit,
				  ss.ulburstlimit,ss.dlburstthreshold,ss.ulburstthreshold,ss.bursttime,ss.priority,spp.net_total,spp.gross_amt,spp.tax,spp.region_type FROM `sht_services` ss
                        LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE  ss.srvid='" . $id . "'");
        $rowarr=$query->row_array();
	if($rowarr['planchange_isnextcycle']==1)
	{
	    
	 $data=$rowarr;
	 $query1=$this->db->query("select downrate,uprate,datalimit,timelimit,fupuprate,fupdownrate,enableburst,
				  dlburstlimit,ulburstlimit,dlburstthreshold,ulburstthreshold,priority,bursttime,
				  is_private,plan_duration from sht_plannextcycle WHERE  srvid='" . $id . "'");
	 $rowarr1=$query1->row_array();
	 $data['downrate']=$rowarr1['downrate'];
	 $data['uprate']=$rowarr1['uprate'];
	 $data['datalimit']=$rowarr1['datalimit'];
	  $data['timelimit']=$rowarr1['timelimit'];
	 $data['fupuprate']=$rowarr1['fupuprate'];
	 $data['fupdownrate']=$rowarr1['fupdownrate'];
	 $data['enableburst']=$rowarr1['enableburst'];
	 $data['dlburstlimit']=$rowarr1['dlburstlimit'];
	   $data['ulburstlimit']=$rowarr1['ulburstlimit'];
	 $data['dlburstthreshold']=$rowarr1['dlburstthreshold'];
	 $data['ulburstthreshold']=$rowarr1['ulburstthreshold'];
	 $data['priority']=$rowarr1['priority'];
	 $data['bursttime']=$rowarr1['bursttime'];
	 $data['is_private']=$rowarr1['is_private'];
	 $data['plan_duration']=$rowarr1['plan_duration'];

	}
	else
	{
	    $data=$rowarr;
	}
	
	return $data;
    }

    public function plan_topup($id) {

        $query = $this->db->query("SELECT ss.isp_uid,ss.srvid,ss.is_private,ss.topuptype,ss.srvname,ss.descr,ss.enableplan,ss.datalimit
				  ,ss.datacalc,ss.downrate,ss.uprate,spp.net_total,spp.gross_amt,spp.tax,spp.region_type,spp.payment_type FROM `sht_services` ss
                        LEFT JOIN `sht_topup_pricing` spp ON (ss.srvid=spp.srvid) WHERE  ss.srvid='" . $id . "'");
        return $query->row_array();
    }

    public function data_unacctn_data($id) {
        $condarr = array("srvid" => $id);
        $query = $this->db->get_where(SHTDATACCTN, $condarr);

        return $query->result();
    }

    public function data_speedtopup_data($id) {
        $condarr = array("srvid" => $id);
        $query = $this->db->get_where(SHTSPEEDTOPUP, $condarr);

        return $query->result();
    }

    public function state_list($stateid = '') {
        $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
      //  echo "<pre>"; print_R($sessiondata); die;
        $dept_id = $sessiondata['dept_id'];
        $isp_uid = $sessiondata['isp_uid'];
        $regiontype = $this->dept_region_type();
        if ($superadmin == 1) {
	    
	    
             $query = $this->db->query("select state from sht_isp_admin_region where isp_uid='" . $isp_uid . "'");
            $statearr = array();
            foreach ($query->result() as $val) {
                $statearr[] = $val->state;
            }
            if (in_array('all', $statearr)) {
                $stateQ = $this->db->get('sht_states');
            } else {
                $sid = '"' . implode('", "', $statearr) . '"';
                $stateQ = $this->db->query("select id,state from sht_states where id IN ({$sid}) ");
            }
            $num_rows = $stateQ->num_rows();
            if ($num_rows > 0) {
                $gen = '';
                foreach ($stateQ->result() as $stobj) {
                    $sel = '';
                    if ($stateid == $stobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $stobj->id . '" ' . $sel . '>' . $stobj->state . '</option>';
                }
            }
        } else {
            $query = $this->db->query("select state_id from sht_dept_region where dept_id='" . $dept_id . "'");
            $statearr = array();
            foreach ($query->result() as $val) {
                $statearr[] = $val->state_id;
            }
            if (in_array('all', $statearr)) {
                $stateQ = $this->db->get('sht_states');
            } else {
                $sid = '"' . implode('", "', $statearr) . '"';
                $stateQ = $this->db->query("select id,state from sht_states where id IN ({$sid}) ");
            }
            $num_rows = $stateQ->num_rows();
            if ($num_rows > 0) {
                $gen = '';
                foreach ($stateQ->result() as $stobj) {
                    $sel = '';
                    if ($stateid == $stobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $stobj->id . '" ' . $sel . '>' . $stobj->state . '</option>';
                }
            }
        }
        echo $gen;
    }

    public function dept_region_type() {
        $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
        $region_type = '';
        if ($superadmin == 1) {
            $region_type = 'region';
        } else {
            $query = $this->db->get_where('sht_department', array('id' => $dept_id));
            if ($query->num_rows() > 0) {
                $rowarr = $query->row_array();
                $region_type = $rowarr['region_type'];
            }
        }
        

        return $region_type;
    }

    public function getcitylist($stateid, $cityid = '') {
        $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
        $isp_uid = $sessiondata['isp_uid'];
        $regiontype = $this->dept_region_type();
        $postdata = $this->input->post();
        $not_addable = (isset($postdata['is_addable']) && $postdata['is_addable'] == 0) ? 1 : 0;
        $allcity = '<option value="all">All Cities</option>';

        $gen = '';
        if ($superadmin == 1) {
	    $citycond="";
	    if($sessiondata['is_franchise']==1)
	    {
		
		$franchiseQ=$this->db->query("select city from sht_isp_admin_region where isp_uid='".$isp_uid."' and state='".$stateid."'");
		//echo $this->db->last_query();
		if($franchiseQ->num_rows()>0)
		{
		    $cityidarr=array();
		    foreach($franchiseQ->result() as $franchO)
		    {
			$cityidarr[]=$franchO->city;
		    }
		 //   echo "<pre>"; print_R($cityidarr);die;
		    if(in_array('all',$cityidarr))
		    {
			$citycond="";
		    }
		    else
		    {
			 $cfrid = '"' . implode('", "', $cityidarr) . '"';
			 $citycond="and city_id IN ({$cfrid})";
		    }
		    $ispq=$this->db->query("select parent_isp from sht_isp_admin where isp_uid='".$isp_uid."'");
		$isparr=$ispq->row_array();
		$isp_uid=$isparr['parent_isp'];
			
		}
		
	    }
	    else
	    {
		$citycond="";
	    }

          $cityQ = $this->db->query("SELECT city_id,city_name FROM sht_cities WHERE  ((isp_uid='".$isp_uid."' AND city_state_id='".$stateid."') OR (isp_uid='0'  AND  city_state_id='".$stateid."')) {$citycond}");
        
	    $num_rows = $cityQ->num_rows();
            $gen .= $allcity;
            if ($num_rows > 0) {

                foreach ($cityQ->result() as $ctobj) {
                    $sel = '';
                    if ($cityid == $ctobj->city_id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $ctobj->city_id . '" ' . $sel . '>' . $ctobj->city_name . '</option>';
                }
            }
            if ($not_addable == 0) {
                $gen .= '<option id="addcityfly" class="addcityfly" value="addc">--Add City--</option>';
            }
        } else {
            $query = $this->db->query("select city_id from sht_dept_region where dept_id='" . $dept_id . "' and state_id='" . $stateid . "'");
            $cityarr = array();
            foreach ($query->result() as $val) {
                $cityarr[] = $val->city_id;
            }
            $isaddcity = 0;
            if (in_array('all', $cityarr)) {
                $allcity = '<option value="all">All Cities</option>';
                $cityQ = $this->db->query("SELECT city_id,city_name FROM sht_cities WHERE  (isp_uid='".$isp_uid."' AND city_state_id='".$stateid."') OR (isp_uid='0'  AND  city_state_id='".$stateid."')");
                $isaddcity = 1;
            } else {
                $allcity = '<option value="">Select City</option>';
                $cid = '"' . implode('", "', $cityarr) . '"';
                $cityQ = $this->db->query("select city_id,city_name from sht_cities where city_id IN ({$cid})");
                $isaddcity = 0;
            }
            $num_rows = $cityQ->num_rows();
            if ($num_rows > 0) {
                $gen .= $allcity;
                foreach ($cityQ->result() as $ctobj) {
                    $sel = '';
                    if ($cityid == $ctobj->city_id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $ctobj->city_id . '" ' . $sel . '>' . $ctobj->city_name . '</option>';
                }
            } else {
                $allcity = '<option value="all">All Cities</option>';
                $gen .= $allcity;
            }

            if ($isaddcity == 1) {
                if ($not_addable == 0) {
                    $gen .= '<option id="addcityfly" class="addcityfly" value="addc">--Add City--</option>';
                }
            }
        }
        echo $gen;
    }

    public function getzonelist($cityid, $zoneid = '', $stateid = '') {

        $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
          $isp_uid = $sessiondata['isp_uid'];
        $regiontype = $this->dept_region_type();
        $not_addable = (isset($postdata['is_addable']) && $postdata['is_addable'] == 0) ? 1 : 0;
        // $stateid=$this->input->post('stateid');
        $allzone = '<option value="all">All Zone</option>';
        $gen = '';
        if ($superadmin == 1) {
	     $zonecond="";
	    if($sessiondata['is_franchise']==1)
	    {
		$franchiseQ=$this->db->query("select zone from sht_isp_admin_region where isp_uid='".$isp_uid."' and city='".$cityid."'");
		//echo $this->db->last_query();
		if($franchiseQ->num_rows()>0)
		{
		    $zoneidarr=array();
		    foreach($franchiseQ->result() as $franchO)
		    {
			$zoneidarr[]=$franchO->zone;
		    }
		  //  echo "<pre>"; print_R($zoneidarr);//die;
		    if(in_array('all',$zoneidarr))
		    {
			$zonecond="";
		    }
		    else
		    {
			 $zfrid = '"' . implode('", "', $zoneidarr) . '"';
			 $zonecond="and id IN ({$zfrid})";
		    }
		    $ispq=$this->db->query("select parent_isp from sht_isp_admin where isp_uid='".$isp_uid."'");
		$isparr=$ispq->row_array();
		$isp_uid=$isparr['parent_isp'];
			
		}
		
	    }
	    else
	    {
		$zonecond="";
	    }
            $zoneQ = $this->db->query("SELECT id,zone_name FROM `sht_zones` WHERE  ((isp_uid='".$isp_uid."' AND city_id='".$cityid."') OR (isp_uid='0'  AND city_id='".$cityid." ')) {$zonecond}");
           // echo $this->db->last_query(); die;
	    $num_rows = $zoneQ->num_rows();
            $gen .= $allzone;
            if ($num_rows > 0) {

                foreach ($zoneQ->result() as $znobj) {
                    $sel = '';
                    if ($zoneid == $znobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $znobj->id . '" ' . $sel . '>' . $znobj->zone_name . '</option>';
                }
            }
            if ($not_addable == 0) {
                $gen .= '<option id="addzonefly" class="addzonefly" value="addz">--Add Zone--</option>';
            }
        } else {

            $query = $this->db->query("select zone_id from sht_dept_region where dept_id='" . $dept_id . "' and state_id='" . $stateid . "'");
            $zonearr = array();
            foreach ($query->result() as $val) {
                $zonearr[] = $val->zone_id;
            }
            $isaddzone = 0;
            if (in_array('all', $zonearr)) {
                //   echo "sssss"; die;
                $isaddzone = 1;
                $allzone = '<option value="all">All Zone</option>';
                $zoneQ = $this->db->query("SELECT id,zone_name FROM `sht_zones` WHERE  (isp_uid='".$isp_uid."' AND city_id='".$cityid."') OR (isp_uid='0'  AND city_id='".$cityid."')");
            } else {
                $isaddzone = 0;
                $allzone = '<option value="">Select Zone</option>';
                $zid = '"' . implode('", "', $zonearr) . '"';
                $zoneQ = $this->db->query("select id,zone_name from sht_zones where id IN ({$zid})");
            }
            $num_rows = $zoneQ->num_rows();

            if ($num_rows > 0) {
                $gen .= $allzone;
                foreach ($zoneQ->result() as $znobj) {
                    $sel = '';
                    if ($zoneid == $znobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $znobj->id . '" ' . $sel . '>' . $znobj->zone_name . '</option>';
                }
            } else {
                $allzone = '<option value="all">All Zone</option>';
                $gen .= $allzone;
            }
            if ($isaddzone == 1) {
                if ($not_addable == 0) {
                    $gen .= '<option id="addzonefly" class="addzonefly" value="addz">--Add Zone--</option>';
                }
            }
        }
        echo $gen;
    }

    public function add_region() {
        $postdata = $this->input->post();
        //echo "<pre>"; print_R($postdata);die;
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
        $tabledata = array("region_type" => $postdata['region']);
        if (isset($postdata['plan_id']) && $postdata['plan_id'] != '') {
            $query = $this->db->query("select region_type from sht_plan_pricing where srvid='" . $postdata['plan_id'] . "'");
            if ($query->num_rows() > 0) {
                $this->db->update(SHTPLANPRICING, $tabledata, array('srvid' => $postdata['plan_id']));
                $this->plan_enable_setting($postdata['plan_id'], 'PLANREGION');
            } else {
                $tabledata = array("region_type" => $postdata['region'], 'srvid' => $postdata['plan_id']);
                $this->db->insert(SHTPLANPRICING, $tabledata);
                $this->plan_enable_setting($postdata['plan_id'], 'PLANREGION');
            }


            if ($postdata['region'] == "region") {

                $listarr = array_filter(explode(",", $postdata['regiondat']));

                foreach ($listarr as $vald) {
                    $datarr = explode("::", $vald);
                    //  `plan_id`,`state_id`,`city_id`,`zone_id`,
                    $tabledata = array("plan_id" => $postdata['plan_id'], "state_id" => $datarr[0],
                        "city_id" => $datarr['1'], "zone_id" => $datarr[2],
                        "created_on" => date("Y-m-d H:i:s"),"isp_uid"=>$isp_uid);

                    if (isset($datarr[3]) && $datarr[3] != '') {
                        $this->db->update(SHTPLANREGION, $tabledata, array('id' => $datarr[3]));
                    } else {
                        $this->db->insert(SHTPLANREGION, $tabledata);
                    }
                }
                $this->plan_nas_association($postdata['plan_id']);
            } else {
                $this->db->delete(SHTPLANREGION, array("plan_id" => $postdata['plan_id']));
                $this->plan_nas_association($postdata['plan_id']);
            }

            return $postdata['plan_id'];
        }
    }

    public function add_region_topup() {
        $postdata = $this->input->post();
        // echo "<pre>"; print_R($postdata); die;
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
        $tabledata = array("region_type" => $postdata['region']);
        if (isset($postdata['topup_id']) && $postdata['topup_id'] != '') {
            //$this->db->update(SHTTOPUP, $tabledata, array('id' => $postdata['topup_id']));
            $query = $this->db->query("select region_type from sht_topup_pricing where srvid='" . $postdata['topup_id'] . "'");
            if ($query->num_rows() > 0) {
                $this->db->update(SHTTOPUPPRICING, $tabledata, array('srvid' => $postdata['topup_id']));
                $this->topup_enable_setting($postdata['topup_id'], 'TOPUPREGION');
            } else {
                $tabledata = array("region_type" => $postdata['region'], 'srvid' => $postdata['topup_id']);
                $this->topup_enable_setting($postdata['topup_id'], 'TOPUPREGION');
                $this->db->insert(SHTTOPUPPRICING, $tabledata);
            }
            if ($postdata['region'] == "region") {

                $listarr = array_filter(explode(",", $postdata['regiondat']));

                foreach ($listarr as $vald) {
                    $datarr = explode("::", $vald);
                    //  `plan_id`,`state_id`,`city_id`,`zone_id`,
                    $tabledata = array("topup_id" => $postdata['topup_id'], "state_id" => $datarr[0],
                        "city_id" => $datarr['1'], "zone_id" => $datarr[2],
                        "created_on" => date("Y-m-d H:i:s"),"isp_uid"=>$isp_uid);

                    if (isset($datarr[3]) && $datarr[3] != '') {
                        $this->db->update(SHTTOPUPREGION, $tabledata, array('id' => $datarr[3]));
                    } else {
                        $this->db->insert(SHTTOPUPREGION, $tabledata);
                    }
                }
            } else {
                $this->db->delete(SHTTOPUPREGION, array("topup_id" => $postdata['topup_id']));
            }

            return $postdata['topup_id'];
        }
    }

    public function plan_region_data($id) {
        $condarr = array('plan_id' => $id, 'is_deleted' => 0);
        $query = $this->db->get_where(SHTPLANREGION, $condarr);
        return $query->result();
    }

    public function topup_region_data($id) {
        $condarr = array('topup_id' => $id, 'is_deleted' => 0);
        $query = $this->db->get_where(SHTTOPUPREGION, $condarr);
        return $query->result();
    }

    public function delete_region_plan() {
        $postdata = $this->input->post();
        $tabledata = array("is_deleted" => 1);
        $this->db->update(SHTPLANREGION, $tabledata, array('id' => $postdata['id']));
       
        return $postdata['id'];
    }

    public function delete_region_topup() {
        $postdata = $this->input->post();
        $tabledata = array("is_deleted" => 1);
        $this->db->update(SHTTOPUPREGION, $tabledata, array('id' => $postdata['id']));
        return $postdata['id'];
    }

    function sec2hms($secs) {
        $secs = round($secs);
        $secs = abs($secs);
        $hours = floor($secs / 3600) . ':';
        if ($hours == '0:')
            $hours = '';
        $minutes = substr('00' . floor(($secs / 60) % 60), -2);
        //$seconds = substr('00' . $secs % 60, -2);
        return ltrim($hours . $minutes, '0');
    }

    public function viewplan_search_results() {
        $data = array();
        $dataaind = array();
        $gen = '';
        $total_search = 0;


        $cond1 = "";
        $cond2 = "";
        $cond3 = "";
        $cond4 = "";
        
        $limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
        
        /* Condition for search filter */
        $search_user = $this->input->post('search_user');
        $user_state = $this->input->post('state');
        $user_city = $this->input->post('city');
        $user_zone = $this->input->post('zone');
        
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";

        $condarr = array();
        if ($user_state != 'all' && $user_city != 'all' && $user_zone != 'all') {
            $condarr[] = "(state_id='" . $user_state . "' and city_id='" . $user_city . "' and zone_id='" . $user_zone . "')";
        } else if ($user_state != 'all' && $user_city == 'all') {
            $condarr[] = "(state_id='" . $user_state . "')";
        } else if ($user_state != 'all' && $user_city != 'all' && $user_zone == 'all') {
            $condarr[] = "(state_id='" . $user_state . "' and city_id='" . $user_city . "')";
        }
        $cond1 = implode('OR', $condarr);



        if ($cond1 != "") {
            $cond1 = "and ($cond1)";
        }


        $query = $this->db->query("SELECT distinct(plan_id) FROM sht_plan_region WHERE status='1' AND is_deleted='0' $cond1");
	//echo $this->db->last_query()."<br/>";

        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $pobj) {
                $data[$i] = $pobj->plan_id;
                $i++;
            }
        }

        $query1 = $this->db->query("SELECT ss.srvid FROM sht_services ss
INNER JOIN `sht_plan_pricing` spp ON(ss.srvid=spp.srvid) AND spp.region_type='allindia' {$ispcond} AND ss.`enableplan`=1 and ss.is_deleted='0'");
	//echo $this->db->last_query()."<br/>";
        if ($query1->num_rows() > 0) {
            $i = 0;
            foreach ($query1->result() as $val) {
                $dataaind[$i] = $val->srvid;
                $i++;
            }
        }
        $pidarr = array_merge($data, $dataaind);
        $pid = '"' . implode('", "', $pidarr) . '"';
// echo $pid;
        $cond = "";
        if ($pid != '') {
            $cond .= "and ss.srvid IN ({$pid})";
        }

        if ($search_user != '') {
            $cond .= "and srvname LIKE '%" . $search_user . "%'";
        }
        //conditions for department filter
        $cond2 = $this->dept_plan_condition();
        $cond2 = ($cond2 == "allindia") ? "" : $cond2;
        
        $ispcodet = $this->countrydetails();
	$cocurrency = $ispcodet['currency'];
	
        /*total count*/
        $countQ=$this->db->query("SELECT ss.srvid FROM `sht_services` ss
                        LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE  ss.enableplan='1' and ss.is_deleted='0'"
                . " and ss.plantype!='0' {$ispcond} {$cond}  {$cond2}  ");
                $result_count = $countQ->num_rows();
		//echo $this->db->last_query()."<br/>";

        $query3 = $this->db->query("SELECT ss.plantype,ss.is_private,ss.datalimit,ss.enableplan,ss.srvid,ss.srvname,ss.downrate,ss.uprate,spp.net_total,spp.gross_amt,(SELECT COUNT(id) FROM sht_users su WHERE su.baseplanid=ss.srvid AND su.enableuser='1') as usercount FROM `sht_services` ss
                        LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE  ss.enableplan='1' and ss.is_deleted='0'"
                . " and ss.plantype!='0' {$ispcond} {$cond}  {$cond2} order by ss.srvid desc LIMIT $qofset,$qlmt ");
//echo $this->db->last_query()."<br/>";
	$tax=$this->get_tax();
        $total_search = $query3->num_rows();
        $i = $qofset+1;
        $is_editable = $this->plan_model->is_permission(PLANS, 'EDIT');
        $is_deletable = $this->plan_model->is_permission(PLANS, 'DELETE');
        foreach ($query3->result() as $sobj) {
	$net_total=ceil($sobj->gross_amt+($tax['tax']/100*$sobj->gross_amt));
            $plandata = "";
	    $accestype=($sobj->is_private==1)?"Private":"Public";
            if ($sobj->plantype == 1) {
                $plandata = "Unlimited";
                 $plantype="Unlimited Plan";
            } else if ($sobj->plantype == 2) {
                $plandata = "Timeplan";
                 $plantype="Time Plan";
            } else if ($sobj->plantype == 3) {
                $plandata = round($this->convertTodata($sobj->datalimit . "GB"),3) . " GB";
                $plantype="Fup Plan";
            } else {
                $plandata = round($this->convertTodata($sobj->datalimit . "GB"),3) . " GB";
                $plantype="Data Plan";
            }

            $status = ($sobj->enableplan == 1) ? 'active' : 'inactive';
            if ($sobj->enableplan == 1) {
                $class = 'class="inactive delete"';
                $status = '<img src="' . base_url() . 'assets/images/on2.png" rel="disable">';
                $stat1 = "Active";
                // $task="disable";
            } else {
                $class = 'class=" delete"';
                $status = '<img src="' . base_url() . 'assets/images/off2.png" rel="enable">';
                $stat1 = "Inactive";
                // $task="enable";
            }

            $gen .= '
				<tr>
					<td>' . $i . '.</td>
					
					<td><a href="' . base_url() . 'plan/edit_plan/' . $sobj->srvid . '">' . $sobj->srvname . '</a></td>
                                            <td>' . $plantype . '</td><td>' . $accestype . '</td>
					<td>' . $this->convertTodata($sobj->downrate . "KB") . '</td>
					<td>' . $this->convertTodata($sobj->uprate . "KB") . '</td>
					<td>' . $plandata . '</td>
					<td>' . $cocurrency.' '.$net_total . '</td>
					<td><a href="' . base_url() . 'plan/user_stat/' . $sobj->srvid . '">' . $sobj->usercount . '</a></td><td><a href="javascript:void(0)" onclick="showburstmode('.$sobj->srvid.')"><i class="fa fa-eye" aria-hidden="true"></i></a></td>';
            if ($is_editable) {
                $gen .= '<td><a ' . $class . '  onclick = "change_planstatus(\'' . $sobj->srvid . '\')" href="javascript:void(0)" id="' . $sobj->srvid . '"> ' . $status . ' </a></td>';
            } else {
                $gen .= '<td>' . $stat1 . '</td>';
            }
            $gen .= '<td><a href="' . base_url() . 'plan/edit_plan/' . $sobj->srvid . '">EDIT</a></td>';
            $delclass = ($is_deletable) ? "deletplan" : "";

            $gen .= ' <td><a href="javascript:void(0);" class="' . $delclass . '"  rel="' . $sobj->srvid . '">Delete</a></td>';

            $gen .= '</tr>
				';
            $i++;
        }


        $data['total_results'] = $total_search;
        $data['search_results'] = $gen;
         $data['result_count'] = $result_count;
        if(($qofset + $total_search) == $result_count){
			$data['loadmore'] = 0;
		}else{
			$data['loadmore'] = 1;
		}
		$data['limit'] = $qlmt;
		$data['offset'] = $qlmt+$qofset;

        echo json_encode($data);
    }

    public function viewtopup_search_results() {
        $data = array();
        $dataaind = array();
        $gen = '';
        $total_search = 0;


        $cond1 = "";
        $cond2 = "";
        $cond3 = "";
        $cond4 = "";
        
         $limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
        

        $search_user = $this->input->post('search_user');
        $user_state = $this->input->post('state');
        $user_city = $this->input->post('city');
        $user_zone = $this->input->post('zone');
        
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";

        $condarr = array();
        if ($user_state != 'all' && $user_city != 'all' && $user_zone != 'all') {
            $condarr[] = "(state_id='" . $user_state . "' and city_id='" . $user_city . "' and zone_id='" . $user_zone . "')";
        } else if ($user_state != 'all' && $user_city == 'all') {
            $condarr[] = "(state_id='" . $user_state . "')";
        } else if ($user_state != 'all' && $user_city != 'all' && $user_zone == 'all') {
            $condarr[] = "(state_id='" . $user_state . "' and city_id='" . $user_city . "')";
        }
        $cond1 = implode('OR', $condarr);



        if ($cond1 != "") {
            $cond1 = "and ($cond1)";
        }



        $query = $this->db->query("SELECT distinct(topup_id) FROM sht_topup_region WHERE status='1' AND is_deleted='0' $cond1  ");
	//echo $this->db->last_query()."<br/>";
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $pobj) {
                $data[$i] = $pobj->topup_id;
                $i++;
            }
        }

        $query1 = $this->db->query("SELECT ss.srvid FROM sht_services ss
INNER JOIN `sht_topup_pricing` spp ON(ss.srvid=spp.srvid) AND spp.region_type='allindia' {$ispcond} AND ss.`enableplan`=1 and ss.is_deleted='0'");
	//echo $this->db->last_query()."<br/>";
        if ($query1->num_rows() > 0) {
            $i = 0;
            foreach ($query1->result() as $val) {
                $dataaind[$i] = $val->srvid;
                $i++;
            }
        }
        $pidarr = array_merge($data, $dataaind);
        $pid = '"' . implode('", "', $pidarr) . '"';
// echo $pid;
        $cond = "";
        if ($pid != '') {
            $cond .= "and ss.srvid IN ({$pid})";
        }

        if ($search_user != '') {
            $cond .= "and srvname LIKE '%" . $search_user . "%'";
        }
        //conditions for department filter
        $cond2 = $this->dept_topup_condition();
        $cond2 = ($cond2 == "allindia") ? "" : $cond2;
        
	$ispcodet = $this->countrydetails();
	$cocurrency = $ispcodet['currency'];
	
         /*total count*/
        $countQ=$this->db->query("SELECT ss.srvid FROM `sht_services` ss
                        LEFT JOIN `sht_topup_pricing` spp ON (ss.srvid=spp.srvid) WHERE ss.enableplan='1' "
                . " and ss.topuptype!='0' {$ispcond} {$cond}  {$cond2} ");
	//echo $this->db->last_query()."<br/>";
                $result_count = $countQ->num_rows();
        
        $query3 = $this->db->query("SELECT ss.datalimit,ss.srvid,ss.is_private,ss.downrate,ss.uprate,ss.topuptype,ss.enableplan,ss.srvname,
				   ss.datalimit,spp.net_total,spp.gross_amt,spp.payment_type,(SELECT COUNT(distinct(su.uid)) FROM sht_usertopupassoc su inner join sht_users sut on (su.uid=sut.uid) WHERE su.topup_id=ss.srvid AND su.terminate='0' and sut.enableuser='1') AS usercount FROM `sht_services` ss
                        LEFT JOIN `sht_topup_pricing` spp ON (ss.srvid=spp.srvid) WHERE ss.enableplan='1' and ss.is_deleted='0' "
                . " and ss.topuptype!='0' {$ispcond} {$cond}  {$cond2} order by ss.srvid desc LIMIT $qofset,$qlmt ");
	//echo $this->db->last_query()."<br/>";
$tax=$this->get_tax();
        $total_search = $query3->num_rows();

        $i = $qofset+1;
        $is_editable = $this->plan_model->is_permission(TOPUP, 'EDIT');
        $is_deletable = $this->plan_model->is_permission(TOPUP, 'DELETE');
        foreach ($query3->result() as $sobj) {
	    $accestype=($sobj->is_private==1)?"Private":"Public";
	$net_total=ceil($sobj->gross_amt+($tax['tax']/100*$sobj->gross_amt));
            $topuptype = "";
            $payment = ($sobj->payment_type == "Paid") ? round($sobj->net_total) : "Free";
            if ($sobj->topuptype == 1) {
                $topuptype = "Data Top-Up";
                 $datalimit= round($this->convertTodata($sobj->datalimit . "GB"),3) . " GB";
                 $uprate="-";
                 $downrate="-";
                 $days="-";
                 $unacctn="-";
            } else if ($sobj->topuptype == 2) {
                $topuptype="Data un-accountancy";
                  $datalimit="-";
                 $uprate="-";
                 $downrate="-";
                $datadays=$this->topup_getdays($sobj->srvid,'unaacct');
                $days=$datadays['days'];
                $unacctn=$datadays['unaccounacy'];
            } else {
                $topuptype="Speed Top-Up";
                $datalimit="-";
               $downrate=$this->convertTodata($sobj->downrate . "KB");
               $uprate= $this->convertTodata($sobj->uprate . "KB");
                $datadays=$this->topup_getdays($sobj->srvid,'speed');
                 $days=$datadays['days'];
                $unacctn=$datadays['unaccounacy'];              
            }

            $status = ($sobj->enableplan == 1) ? 'active' : 'inactive';
            if ($sobj->enableplan == 1) {
                $class = 'class="inactive delete"';
                $status = '<img src="' . base_url() . 'assets/images/on2.png" rel="disable">';
                $stat1 = "Active";
                // $task="disable";
            } else {
                $class = 'class=" delete"';
                $status = '<img src="' . base_url() . 'assets/images/off2.png" rel="enable">';
                $stat1 = "Inactive";
                // $task="enable";
            }

            $gen .= '
				<tr>
					<td>' . $i . '.</td>
					
					<td><a href="' . base_url() . 'plan/edit_topup/' . $sobj->srvid . '">' . $sobj->srvname . '</a></td>
					<td>' . $topuptype . '</td><td>' . $accestype . '</td>
                                        <td>' . $datalimit . '</td>
                                        <td>' . $uprate . '</td>
                                         <td>' . $downrate . '</td>
                                         <td>' . $unacctn . '</td>   
                                         <td>' . $days . '</td>        
					<td>' . $cocurrency.' '.$payment . '</td>
					<td><a href="' . base_url() . 'plan/topupuser_stat/' . $sobj->srvid . '">' . $sobj->usercount . '</a></td>';


            if ($is_editable) {
                $gen .= '<td><a ' . $class . '  onclick = "change_topupstatus(\'' . $sobj->srvid . '\')" href="javascript:void(0)" id="' . $sobj->srvid . '"> ' . $status . ' </a></td>';
            } else {
                $gen .= '<td>' . $stat1 . '</td>';
            }
            $gen .= '<td><a href="' . base_url() . 'plan/edit_topup/' . $sobj->srvid . '">EDIT</a></td>';


            $delclass = ($is_deletable) ? "delettopup" : "";

            $gen .= ' <td><a href="javascript:void(0);" class="' . $delclass . '"  rel="' . $sobj->srvid . '">Delete</a></td>';



            $gen .= '</tr>
				';
            $i++;
        }


        $data['total_results'] = $total_search;
        $data['search_results'] = $gen;
         $data['result_count'] = $result_count;
        if(($qofset + $total_search) == $result_count){
			$data['loadmore'] = 0;
		}else{
			$data['loadmore'] = 1;
		}
		$data['limit'] = $qlmt;
		$data['offset'] = $qlmt+$qofset;

        echo json_encode($data);
    }

    public function check_plan_deletable() {
        $planid = $this->input->post('delplanid');
        $query = $this->db->query("select id from sht_users where baseplanid='" . $planid . "' and enableuser='1'");
        if ($query->num_rows() > 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public function check_topup_deletable() {
        $topupid = $this->input->post('deltopupid');
        $query = $this->db->query("select id from sht_usertopupassoc where topup_id='" . $topupid . "' and status='1' and terminate='0'");
        if ($query->num_rows() > 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public function delete_plan() {
        $postdata = $this->input->post();
        $tabledata = array("is_deleted" => 1,"enableplan"=>0);
        $this->db->update(SHTSERVICES, $tabledata, array('srvid' => $postdata['planid']));
        $this->disable_system_serviceid($postdata['planid']);
        return $postdata['planid'];
    }

    public function delete_topup() {
        $postdata = $this->input->post();
        $tabledata = array("is_deleted" => 1,"enableplan"=>0);
        $this->db->update(SHTSERVICES, $tabledata, array('srvid' => $postdata['topupid']));
        return $postdata['topupid'];
    }

    public function plan_enable_setting($plan_id, $form) {
        $query = $this->db->query("select id from sht_plan_activation_panel where plan_id='" . $plan_id . "'");
        if ($query->num_rows() > 0) {
            if ($form == "PLANDETAIL") {
                $tabledata = array("plan_detail" => 1);
                $this->db->update(PLANACTIVATIONTAB, $tabledata, array('plan_id' => $plan_id));
            } else if ($form == "PLANSETTING") {
                $tabledata = array("plan_setting" => 1);
                $this->db->update(PLANACTIVATIONTAB, $tabledata, array('plan_id' => $plan_id));
            } else if ($form == "PLANPRICING") {
                $tabledata = array("plan_pricing" => 1);
                $this->db->update(PLANACTIVATIONTAB, $tabledata, array('plan_id' => $plan_id));
            } else {
                $tabledata = array("plan_region" => 1);
                $this->db->update(PLANACTIVATIONTAB, $tabledata, array('plan_id' => $plan_id));
            }
        } else {
            if ($form == "PLANDETAIL") {
                $tabledata = array("plan_detail" => 1, 'plan_id' => $plan_id);
                $this->db->insert(PLANACTIVATIONTAB, $tabledata);
            } else if ($form == "PLANSETTING") {
                $tabledata = array("plan_setting" => 1, 'plan_id' => $plan_id);
                $this->db->insert(PLANACTIVATIONTAB, $tabledata);
            } else if ($form == "PLANPRICING") {
                $tabledata = array("plan_pricing" => 1, 'plan_id' => $plan_id);
                $this->db->insert(PLANACTIVATIONTAB, $tabledata);
            } else {
                $tabledata = array("plan_region" => 1, 'plan_id' => $plan_id);
                $this->db->insert(PLANACTIVATIONTAB, $tabledata);
            }
        }
        $conarr = array("plan_id" => $plan_id, "plan_detail" => 1, "plan_setting" => 1, "plan_pricing" => 1, "plan_region" => 1, "forced_disable" => 0);
        $query1 = $this->db->get_where(PLANACTIVATIONTAB, $conarr);
        if ($query1->num_rows() > 0) {
            $tabledata = array("enableplan" => 1);
            $this->db->update(SHTSERVICES, $tabledata, array('srvid' => $plan_id));
        }

        return $plan_id;
    }

    public function topup_enable_setting($topup_id, $form) {
        $query = $this->db->query("select id from sht_topup_activation_panel where topup_id='" . $topup_id . "'");
        if ($query->num_rows() > 0) {
            if ($form == "TOPUPDETAIL") {
                $tabledata = array("topup_detail" => 1);
                $this->db->update(TOPUPACTIVATIONTAB, $tabledata, array('topup_id' => $topup_id));
            } else if ($form == "TOPUPPRICING") {
                $tabledata = array("topup_pricing" => 1);
                $this->db->update(TOPUPACTIVATIONTAB, $tabledata, array('topup_id' => $topup_id));
            } else {
                $tabledata = array("topup_region" => 1);
                $this->db->update(TOPUPACTIVATIONTAB, $tabledata, array('topup_id' => $topup_id));
            }
        } else {
            if ($form == "TOPUPDETAIL") {
                $tabledata = array("topup_detail" => 1, 'topup_id' => $topup_id);
                $this->db->insert(TOPUPACTIVATIONTAB, $tabledata);
            } else if ($form == "TOPUPPRICING") {
                $tabledata = array("topup_pricing" => 1, 'topup_id' => $topup_id);
                $this->db->insert(TOPUPACTIVATIONTAB, $tabledata);
            } else {
                $tabledata = array("topup_region" => 1, 'topup_id' => $topup_id);
                $this->db->insert(TOPUPACTIVATIONTAB, $tabledata);
            }
        }
        $conarr = array("topup_id" => $topup_id, "topup_detail" => 1, "topup_pricing" => 1, "topup_region" => 1, "forced_disable" => 0);
        $query1 = $this->db->get_where(TOPUPACTIVATIONTAB, $conarr);
        if ($query1->num_rows() > 0) {
            $tabledata = array("enableplan" => 1);
            $this->db->update(SHTSERVICES, $tabledata, array('srvid' => $topup_id));
        }

        return $topup_id;
    }

    public function check_plan_disable() {
        $planid = $this->input->post('id');
        $query = $this->db->query("select id from sht_users where baseplanid='" . $planid . "' and enableuser='1'");
        if ($query->num_rows() > 0) {
            return 0;
        } else {
            return 1;
        }
    }
    
     public function check_plan_editable($id) {
        $planid = $id;
        $query = $this->db->query("select id from sht_users where baseplanid='" . $planid . "' and enableuser='1'");
        if ($query->num_rows() > 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public function check_plan_enable() {
        $planid = $this->input->post('id');
        $conarr = array("plan_id" => $planid, "plan_detail" => 1, "plan_setting" => 1, "plan_pricing" => 1, "plan_region" => 1, "forced_disable" => 0);
        $query1 = $this->db->get_where(PLANACTIVATIONTAB, $conarr);


        if ($query1->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function check_topup_disable() {
        $topupid = $this->input->post('id');
        $query = $this->db->query("select id from sht_usertopupassoc where topup_id='" . $topupid . "' and status='1' and terminate='0'");
        if ($query->num_rows() > 0) {
            return 0;
        } else {
            return 1;
        }
    }
    
    public function check_topup_editable($id) {
        $topupid = $id;
        $query = $this->db->query("select id from sht_usertopupassoc where topup_id='" . $topupid . "' and status='1' and terminate='0'");
        if ($query->num_rows() > 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public function check_topup_enable() {
        $topupid = $this->input->post('id');
        $conarr = array("topup_id" => $topupid, "topup_detail" => 1, "topup_pricing" => 1, "topup_region" => 1, "forced_disable" => 0);
        $query1 = $this->db->get_where(TOPUPACTIVATIONTAB, $conarr);


        if ($query1->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function status_approval($id) {
        $query = $this->db->query("select `enableplan` from sht_services where srvid='" . $id . "'");
        $record = $query->row_array();
        if (!empty($record)) {
            $stat = '';
            $statText = '';
            $status = $record['enableplan'];
            if ($status == 0) {
                $stat = '1';
                $statText = '<img src="' . base_url() . 'assets/images/on2.png" rel="disable">';
                $this->enable_system_serviceid($id);
            } else {
                $stat = '0';
                $statText = '<img src="' . base_url() . 'assets/images/off2.png" rel="enable">';
                $this->disable_system_serviceid($id);
            }
            $tabledata = array("enableplan" => $stat);
            $this->db->where('srvid', $id);
            $this->db->update('sht_services', $tabledata);
        }
        return $statText;
    }

    public function get_plan_usercount() {

         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";
        
        $cond = $this->dept_plan_condition();
        $datarr = array();
        $planarr = array("unlimited" => 1, "Time" => 2, "FUP" => 3, "Data" => 4);
        $totalcount = 0;
        foreach ($planarr as $key => $val1) {

            if ($cond != '') {
                $cond1 = ($cond == "allindia") ? "" : $cond;

                $query = $this->db->query("select ss.srvid from " . SHTSERVICES . " ss where ss.plantype='" . $val1 . "' and ss.enableplan='1' and ss.is_deleted='0' {$cond1} {$ispcond}");
                //   echo "======>>>".$this->db->last_query()."<br/>";
                $plancount = $query->num_rows();
                 $query2 = $this->db->query('SELECT ss.srvname,ss.srvid, COUNT(su.id) AS usercount        
                FROM sht_services ss
                LEFT JOIN sht_users su
                ON (ss.srvid = su.baseplanid) WHERE ss.plantype="' . $val1 . '" '.$cond1.' '.$ispcond.'
                GROUP BY
                ss.srvid ORDER BY usercount DESC LIMIT 1');
				
		   //  echo "======>>>".$this->db->last_query()."<br/>";
            $rowarr = $query2->row_array();
	    if($query2->num_rows()>0)
	    {
		$srvid = $rowarr['srvid'];
            $query1 = $this->db->query("select id from sht_users where baseplanid ='" . $srvid . "' and enableuser='1' and expired='0'");
            //   echo "======>>>".$this->db->last_query()."<br/>";
	    $datarr[$key]['usercount'] = $query1->num_rows();
               $datarr[$key]['plantyoe'] = $val1;
            $datarr[$key]['planname'] = $rowarr['srvname'];
            $datarr[$key]['planid'] = $rowarr['srvid'];
            $totalcount = $totalcount + $query1->num_rows();
	    }
	    else
	    {
	     $datarr[$key]['usercount'] = 0;
               $datarr[$key]['plantyoe'] = '';
            $datarr[$key]['planname'] = '';
            $datarr[$key]['planid'] = '';
            $totalcount = $totalcount +0;
	    }
            
            } else {
                $plancount = 0;
                 $datarr[$key]['usercount'] = 0;
               $datarr[$key]['plantyoe'] = $val1;
            $datarr[$key]['planname'] = "";
            $datarr[$key]['planid'] = "";
            $totalcount = 0;
            }



            $datarr[$key]['plancount'] = $plancount;

           
         
        }
        //  echo "<pre>"; print_R($datarr); die;
        $datarr['totalusercount'] = $totalcount;
        return $datarr;



        //$this->db->query('select * from ')
    }

    public function plan_list_filter($plantype) {
          $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";
         $qlmt = LIMIT; $qofset = SOFFSET;
		$limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
        $cond = $this->dept_plan_condition();
        $cond = ($cond == "allindia") ? "" : $cond;
        $planarr = array("unlimited" => 1, "time" => 2, "fup" => 3, "data" => 4);
        //$condarr=array("enableplan"=>1);
        $query = $this->db->query("SELECT ss.enableplan,ss.plantype,ss.is_private,ss.datalimit,ss.srvid,ss.srvname,ss.downrate,ss.uprate,spp.net_total,spp.gross_amt,(SELECT COUNT(id) FROM sht_users su WHERE su.baseplanid=ss.srvid AND su.enableuser='1') as usercount FROM `sht_services` ss
                        LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE ss.is_deleted='0' and ss.plantype!='0' and plantype='" . $planarr[$plantype] . "' {$cond} {$ispcond} order by ss.srvid desc LIMIT $qofset,$qlmt");
          // echo "======>>>".$this->db->last_query()."<br/>";
	return $query->result();
    }

    public function viewplan_searchfilter() {
        $data = array();
        $dataaind = array();
        $gen = '';
        $total_search = 0;
        $planarr = array("unlimited" => 1, "time" => 2, "fup" => 3, "data" => 4);

        $cond1 = "";
        $cond2 = "";
        $cond3 = "";
        $cond4 = "";
        
        
           $limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
        
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";

        $search_user = $this->input->post('search_user');
        $user_state = $this->input->post('state');
        $user_city = $this->input->post('city');
        $user_zone = $this->input->post('zone');
        $filtertype = $this->input->post('filtertype');
        $plantype = $planarr[$filtertype];
        $condarr = array();
        if ($user_state != 'all' && $user_city != 'all' && $user_zone != 'all') {
            $condarr[] = "(state_id='" . $user_state . "' and city_id='" . $user_city . "' and zone_id='" . $user_zone . "')";
        } else if ($user_state != 'all' && $user_city == 'all') {
            $condarr[] = "(state_id='" . $user_state . "')";
        } else if ($user_state != 'all' && $user_city != 'all' && $user_zone == 'all') {
            $condarr[] = "(state_id='" . $user_state . "' and city_id='" . $user_city . "')";
        }
        $cond1 = implode('OR', $condarr);



        if ($cond1 != "") {
            $cond1 = "and ($cond1)";
        }


        $query = $this->db->query("SELECT distinct(plan_id) FROM sht_plan_region WHERE status='1' AND is_deleted='0' $cond1   ");
          //echo $this->db->last_query()."<br/>";
//echo "<pre>"; print_R($query->result()); die;
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $pobj) {
                $datap[$i] = $pobj->plan_id;
                $i++;
            }
        }

        $query1 = $this->db->query("SELECT ss.srvid FROM sht_services ss
INNER JOIN `sht_plan_pricing` spp ON(ss.srvid=spp.srvid) AND spp.region_type='allindia' {$ispcond} AND ss.`enableplan`=1 and ss.is_deleted='0'");
	//echo $this->db->last_query()."<br/>";
        if ($query1->num_rows() > 0) {
            $i = 0;
            foreach ($query1->result() as $val) {
                $dataaind[$i] = $val->srvid;
                $i++;
            }
        }
        $pidarr = array_merge($datap, $dataaind);
        $pid = '"' . implode('", "', $pidarr) . '"';
// echo $pid;
        $cond = "";
        if ($pid != '') {
            $cond .= "and ss.srvid IN ({$pid})";
        }

        if ($search_user != '') {
            $cond .= "and srvname LIKE '%" . $search_user . "%'";
        }

        /*   $query3 = $this->db->query("SELECT ss.*,spp.net_total FROM `sht_services` ss
          LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE  ss.enableplan='1'"
          . " and ss.plantype!='0' {$cond} and plantype='".$plantype."'"); */
        
         /*total count*/
        $countQ=$this->db->query("SELECT ss.srvid FROM `sht_services` ss
                        LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE  ss.enableplan='1'"
                . " and ss.plantype!='0'  {$ispcond} {$cond} and ss.plantype='" . $plantype . "' ");
                $result_count = $countQ->num_rows();
				
				$i = $qofset+1;
      //  echo $this->db->last_query()."<br/>";
        
	$ispcodet = $this->countrydetails();
	$cocurrency = $ispcodet['currency'];

        $query3 = $this->db->query("SELECT ss.datalimit,ss.is_private,ss.srvid,ss.srvname,ss.downrate,ss.uprate,ss.plantype,ss.enableplan,spp.net_total,spp.gross_amt,(SELECT COUNT(id) FROM sht_users su WHERE su.baseplanid=ss.srvid AND su.enableuser='1') as usercount FROM `sht_services` ss
                        LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE  ss.enableplan='1' and ss.is_deleted='0' "
                . " and ss.plantype!='0' {$ispcond}  {$cond}  and ss.plantype='" . $plantype . "'  order by ss.srvid desc LIMIT $qofset,$qlmt");
	$tax=$this->get_tax();
		//echo $this->db->last_query()."<br/>";
        //   echo $this->db->last_query(); die;
        $total_search = $query3->num_rows();
       	$i = $qofset+1;
        $is_editable = $this->plan_model->is_permission(PLANS, 'EDIT');
        $is_deletable = $this->plan_model->is_permission(PLANS, 'DELETE');
        foreach ($query3->result() as $sobj) {
	$net_total=ceil($sobj->gross_amt+($tax['tax']/100*$sobj->gross_amt));
            $plandata = "";
	    $accestype=($sobj->is_private==1)?"Private":"Public";
            if ($sobj->plantype == 1) {
                $plandata = "Unlimited";
                $plantype="Unlimited Plan";
            } else if ($sobj->plantype == 2) {
                $plandata = "Timeplan";
                $plantype="Time Plan";
            } else if ($sobj->plantype == 3) {
                $plandata = round($this->convertTodata($sobj->datalimit . "GB")) . " GB";
                 $plantype="Fup Plan";
            } else {
                $plandata = round($this->convertTodata($sobj->datalimit . "GB")) . " GB";
                 $plantype="Data Plan";
            }

            $status = ($sobj->enableplan == 1) ? 'active' : 'inactive';
            if ($sobj->enableplan == 1) {
                $class = 'class="inactive delete"';
                $status = '<img src="' . base_url() . 'assets/images/on2.png" rel="disable">';
                $stat1 = "Active";
                // $task="disable";
            } else {
                $class = 'class=" delete"';
                $status = '<img src="' . base_url() . 'assets/images/off2.png" rel="enable">';
                $stat1 = "Inactive";
                // $task="enable";
            }

            $gen .= '
				<tr>
					<td>' . $i . '.</td>
					
					<td><a href="' . base_url() . 'plan/edit_plan/' . $sobj->srvid . '">' . $sobj->srvname . '</a></td>
                                            <td>' . $plantype . '</td><td>' . $accestype . '</td>
					<td>' . $this->convertTodata($sobj->downrate . "KB") . '</td>
					<td>' . $this->convertTodata($sobj->uprate . "KB") . '</td>
					<td>' . $plandata . '</td>
					<td>' . $cocurrency.' '.$net_total . '</td>
					<td><a href="' . base_url() . 'plan/user_stat/' . $sobj->srvid . '">' . $sobj->usercount . '</a></td>';

            if ($is_editable) {
                $gen .= '<td><a ' . $class . '  onclick = "change_planstatus(\'' . $sobj->srvid . '\')" href="javascript:void(0)" id="' . $sobj->srvid . '"> ' . $status . ' </a></td>';
            } else {
                $gen .= '<td>' . $stat1 . '</td>';
            }
            $gen .= '<td><a href="' . base_url() . 'plan/edit_plan/' . $sobj->srvid . '">EDIT</a></td>';


            $delclass = ($is_deletable) ? "deletplan" : "";

            $gen .= ' <td><a href="javascript:void(0);" class="' . $delclass . '"  rel="' . $sobj->srvid . '">Delete</a></td>';

            $gen .= '</tr>
				';
            $i++;
        }


        $data['total_results'] = $total_search;
        $data['search_results'] = $gen;
         $data['result_count'] = $result_count;
        if(($qofset + $total_search) == $result_count){
			$data['loadmore'] = 0;
		}else{
			$data['loadmore'] = 1;
		}
		$data['limit'] = $qlmt;
		$data['offset'] = $qlmt+$qofset;

        echo json_encode($data);
    }

    public function active_inactive_userslist($searchtype) {
        // echo "===--->>".$searchtype;
        $data = array();
        $gen = '';
        $total_search = 0;
        $cwhere = 'enableuser=1';
           $qlmt = LIMIT; $qofset = SOFFSET;
		$limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }

        $sessiondata = $this->session->userdata('isp_session');
        
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
        $regiontype = $this->dept_region_type();
         $isp_uid = $sessiondata['isp_uid'];
        $permicond = '';
        $sczcond = '';
        if ($regiontype == "region") {
            $dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='" . $dept_id . "' AND status='1' AND is_deleted='0'");
          // echo $this->db->last_query()."<br/>";
	    $total_deptregion = $dept_regionQ->num_rows();
            if ($dept_regionQ->num_rows() > 0) {
                $c = 1;
                foreach ($dept_regionQ->result() as $deptobj) {
                    $stateid = $deptobj->state_id;
                    $cityid = $deptobj->city_id;
                    $zoneid = $deptobj->zone_id;
                    if ($cityid == 'all') {
                        $permicond .= " (state='" . $stateid . "') ";
                    } elseif ($zoneid == 'all') {
                        $permicond .= " (state='" . $stateid . "' AND city='" . $cityid . "') ";
                    } else {
                        $permicond .= " (state='" . $stateid . "' AND city='" . $cityid . "' AND zone='" . $zoneid . "') ";
                    }
                    if ($c != $total_deptregion) {
                        $permicond .= ' OR ';
                    }
                    $c++;
                }
            }
            if($permicond!='')
            {
            $sczcond .= ' AND (' . $permicond . ')';
            }
        }
	 $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
          $cwhere .= $ispcond;
       $cwhere .= $sczcond;
         if (isset($searchtype)) {
            $cwhere .= " AND baseplanid='" . $searchtype . "'  and expired=0";
            // echo "-------------000000".$searchtype;
        }
        $search_user = $this->input->post('search_user');
        //$searchtype = $this->input->post('searchtype');
        $state = $this->input->post('state');
        $city = $this->input->post('city');
        $zone = $this->input->post('zone');
        $locality = $this->input->post('locality');
        if (isset($state) && $state != '') {
            if ($state != 'all') {
                $cwhere .= " AND state='" . $state . "'";
            } else {
                $cwhere .= '';
            }
        }
        if (isset($city) && $city != '') {
            if ($city != 'all') {
                $cwhere .= " AND city='" . $city . "'";
            } else {
                $cwhere .= '';
            }
        }
        if (isset($zone) && $zone != '') {
            if ($zone != 'all') {
                $cwhere .= " AND zone='" . $zone . "'";
            } else {
                $cwhere .= '';
            }
        }
        if (isset($locality) && $locality != '') {
            $cwhere .= " AND usuage_locality='" . $locality . "'";
        }

        if (isset($search_user) && $search_user!='') {
            $cwhere .= " AND ((username LIKE '%" . $search_user . "%') OR (firstname LIKE '%" . $search_user . "%') OR (mobile LIKE '%" . $search_user . "%')) ";
        }
      
	$ispcodet = $this->countrydetails();
	$cocurrency = $ispcodet['currency'];
       

        if ($cwhere != '') {
            // echo $cwhere;
            $this->db->where($cwhere);
	 
             
        }
	
	
        $this->db->select('id,state,city,zone,enableuser,account_activated_on,paidtill_date,expiration,uid,firstname,lastname');
        $this->db->from('sht_users');
        //

        $tquery = $this->db->get();
	
	 if ($cwhere != '') {
            // echo $cwhere;
            $this->db->where($cwhere);
	    $this->db->limit($qlmt, $qofset);
             
        }
	//$tquery=  $this->db->get();
	
        $this->db->select('id,state,city,zone,enableuser,account_activated_on,paidtill_date,expiration,uid,firstname,lastname');
        $this->db->from('sht_users');
        //

        $cquery = $this->db->get();
	
        $total_csearch = $tquery->num_rows();
        if ($total_csearch > 0) {
            $i = 1;
            foreach ($cquery->result() as $sobj) {
                $ticket_count = $this->user_model->customer_tickets_count($sobj->id);
                $state = $this->user_model->getstatename($sobj->state);
                $city = $this->user_model->getcityname($sobj->state, $sobj->city);
                $zone = $this->user_model->getzonename($sobj->zone);
                $status = ($sobj->enableuser == 1) ? 'active' : 'inactive';
                $activeplan = $this->user_model->userassoc_planname($sobj->id);
                $active_from = date('d-m-Y', strtotime($sobj->account_activated_on));
                $renewal_date = date('d-m-Y', strtotime($sobj->paidtill_date));
                $expiration = $sobj->expiration;
                if ($expiration == '0000-00-00 00:00:00') {
                    $expiration = '-';
                } else {
                    $expiration = date('d-m-Y', strtotime($sobj->expiration));
                }

                $gen .= "<tr><td>&nbsp;</td><td><div class='checkbox' style='margin-top:0px; margin-bottom:0px;'><label><input type='checkbox' class='collapse_checkbox' value='" . $sobj->id . "'></label></div></td><td><a href='" . base_url() . "user/edit/" . $sobj->id . "'>" . $sobj->uid . "</a></td><td>" . $sobj->firstname . " " . $sobj->lastname . "</td><td>" . $state . "</td><td>" . $city . "</td><td>" . $zone . "</td><td>" . $activeplan . "</td><td>" . $active_from . "</td><td>" . $renewal_date . "</td><td>" . $expiration . "</td><td>" . $status . "</td><td><span>".$cocurrency."</span> 0</td><td class='mui--text-right'>" . $ticket_count . "</td></tr>";
                $i++;
            }
        }

        if ($total_csearch == 0) {
            $gen .= "<tr><td style='text-align:center' colspan='14'> No Result Found !!</td></tr>";
        }

        $data['total_results'] = $total_csearch;
        $data['search_results'] = $gen;

        return $data;
    }

    public function get_topup_usercount() {
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";
        $cond = $this->dept_topup_condition();

        $datarr = array();
        $planarr = array("datatopup" => 1, "unacctncy" => 2, "speed" => 3);
        $totalcount = 0;
        foreach ($planarr as $key => $val1) {
            // $cond = $this->dept_topup_condition();
            if ($cond != '') {
                $cond1 = ($cond == "allindia") ? "" : $cond;
                $query = $this->db->query("select ss.srvid from " . SHTSERVICES . " ss where ss.topuptype='" . $val1 . "' and ss.enableplan='1' and ss.is_deleted='0' {$cond1} {$ispcond}");
             //  echo $this->db->last_query()."<br/>";
	        $topupcount = $query->num_rows();
                 $query2 = $this->db->query('SELECT ss.srvname,ss.srvid, COUNT(su.id) AS usercount        
                FROM sht_services ss
                LEFT JOIN sht_usertopupassoc su
                ON (ss.srvid = su.topup_id) WHERE ss.topuptype="' . $val1 . '" '.$cond1.' '.$ispcond.'
                GROUP BY
                ss.srvid ORDER BY usercount DESC LIMIT 1');
            $rowarr = $query2->row_array();
	    
	   // echo $this->db->last_query()."<br/>";

            $srvid = $rowarr['srvid'];
            $query1 = $this->db->query("select sut.id,sut.uid from sht_usertopupassoc sut inner join sht_users su on (su.uid=sut.uid) where su.enableuser='1' and sut.topup_id ='" . $srvid . "' and terminate='0' group by uid");
            //echo $this->db->last_query()."<br/>";
	    $datarr[$key]['usercount'] = $query1->num_rows();
            $datarr[$key]['topuptype'] = $val1;
            $datarr[$key]['topupname'] = $rowarr['srvname'];
            $datarr[$key]['topupid'] = $rowarr['srvid'];
            $totalcount = $totalcount + $query1->num_rows();
            } else {
                $topupcount = 0;
                $datarr[$key]['usercount'] = 0;
            $datarr[$key]['topuptype'] = $val1;
            $datarr[$key]['topupname'] = "";
            $datarr[$key]['topupid'] ="";
            $totalcount = 0;
            }
             $datarr[$key]['topupcount'] = $topupcount;



         
        }
	//echo "<pre>"; print_R($datarr); die;
	//echo "<pre>"; print_R($datarr); die;
        $datarr['totalusercount'] = $totalcount;
        return $datarr;



        //$this->db->query('select * from ')
    }

    public function viewtopup_searchfilter() {
        
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";
        
        $data = array();
        $dataaind = array();
        $gen = '';
        $total_search = 0;

         $limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }

        $cond1 = "";
        $cond2 = "";
        $cond3 = "";
        $cond4 = "";

        $planarr = array("datatopup" => 1, "unacctncy" => 2, "speed" => 3);

        $search_user = $this->input->post('search_user');
        $user_state = $this->input->post('state');
        $user_city = $this->input->post('city');
        $user_zone = $this->input->post('zone');
        $topuptype = $planarr[$this->input->post('filtertype')];

        $condarr = array();
        if ($user_state != 'all' && $user_city != 'all' && $user_zone != 'all') {
            $condarr[] = "(state_id='" . $user_state . "' and city_id='" . $user_city . "' and zone_id='" . $user_zone . "')";
        } else if ($user_state != 'all' && $user_city == 'all') {
            $condarr[] = "(state_id='" . $user_state . "')";
        } else if ($user_state != 'all' && $user_city != 'all' && $user_zone == 'all') {
            $condarr[] = "(state_id='" . $user_state . "' and city_id='" . $user_city . "')";
        }
        $cond1 = implode('OR', $condarr);



        if ($cond1 != "") {
            $cond1 = "and ($cond1)";
        }



        $query = $this->db->query("SELECT distinct(topup_id) FROM sht_topup_region WHERE status='1' AND is_deleted='0' $cond1   ");
    //echo $this->db->last_query()."<br/>";
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $pobj) {
                $data[$i] = $pobj->topup_id;
                $i++;
            }
        }

        $query1 = $this->db->query("SELECT ss.srvid FROM sht_services ss
INNER JOIN `sht_topup_pricing` spp ON(ss.srvid=spp.srvid) AND spp.region_type='allindia' {$ispcond} AND ss.`enableplan`=1 and ss.is_deleted='0'");
        if ($query1->num_rows() > 0) {
            $i = 0;
            foreach ($query1->result() as $val) {
                $dataaind[$i] = $val->srvid;
                $i++;
            }
        }
	//echo $this->db->last_query()."<br/>";
        $pidarr = array_merge($data, $dataaind);
        $pid = '"' . implode('", "', $pidarr) . '"';
// echo $pid;
        $cond = "";
        if ($pid != '') {
            $cond .= "and ss.srvid IN ({$pid})";
        }

        if ($search_user != '') {
            $cond .= "and srvname LIKE '%" . $search_user . "%'";
        }

        $cond2 = $this->dept_topup_condition();
        $cond2 = ($cond2 == "allindia") ? "" : $cond2;
        
	$ispcodet = $this->countrydetails();
	$cocurrency = $ispcodet['currency'];
	
         /*total count*/
        $countQ=$this->db->query("SELECT ss.srvid FROM `sht_services` ss
                        LEFT JOIN `sht_topup_pricing` spp ON (ss.srvid=spp.srvid) WHERE ss.enableplan='1' "
                . " and ss.topuptype!='0' {$ispcond} and topuptype='" . $topuptype . "' {$cond} {$cond2}   ");
                $result_count = $countQ->num_rows();
				
				
        
        $query3 = $this->db->query("SELECT ss.topuptype,ss.datalimit,ss.is_private,ss.srvid,ss.downrate,ss.uprate,ss.srvid,ss.srvname,ss.enableplan,spp.net_total,spp.gross_amt,spp.payment_type,(SELECT COUNT(id) FROM sht_usertopupassoc su WHERE su.topup_id=ss.srvid AND su.terminate='0') AS usercount FROM `sht_services` ss
                        LEFT JOIN `sht_topup_pricing` spp ON (ss.srvid=spp.srvid) WHERE ss.enableplan='1' and ss.is_deleted='0'"
                . " and ss.topuptype!='0' {$ispcond} and topuptype='" . $topuptype . "' {$cond} {$cond2}     order by ss.srvid desc LIMIT $qofset,$qlmt");
//echo $this->db->last_query()."<br/>";
        //  echo "<pre>"; print_R($query3->result());
        //     die;
	$tax=$this->get_tax();
       $i = $qofset+1;
        $is_editable = $this->plan_model->is_permission(TOPUP, 'EDIT');
        $is_deletable = $this->plan_model->is_permission(TOPUP, 'DELETE');
        foreach ($query3->result() as $sobj) {
	$accestype=($sobj->is_private==1)?"Private":"Public";
            $topuptype = "";
            $payment = ($sobj->payment_type == "Paid") ? ceil($sobj->gross_amt+($tax['tax']/100*$sobj->gross_amt)) : "Free";
            if ($sobj->topuptype == 1) {
                $topuptype = "Data Top-Up";
                 $datalimit= round($this->convertTodata($sobj->datalimit . "GB"),3) . " GB";
                  $uprate="-";
                  $downrate="-";
                  $days="-";
                  $unacctn="-";
            } else if ($sobj->topuptype == 2) {
                $topuptype = "Data un-accountancy";
                 $datalimit="-";
                 $uprate="-";
                 $downrate="-";
                 $datadays=$this->topup_getdays($sobj->srvid,'unaacct');
                 $days=$datadays['days'];
                 $unacctn=$datadays['unaccounacy'];
            } else {
                $topuptype = "Speed Top-Up";
                 $datalimit="-";
                  $downrate=$this->convertTodata($sobj->downrate . "KB");
                 $uprate= $this->convertTodata($sobj->uprate . "KB");
                 $datadays=$this->topup_getdays($sobj->srvid,'speed');
                 $days=$datadays['days'];
                 $unacctn=$datadays['unaccounacy'];
            }

            $status = ($sobj->enableplan == 1) ? 'active' : 'inactive';
            if ($sobj->enableplan == 1) {
                $class = 'class="inactive delete"';
                $status = '<img src="' . base_url() . 'assets/images/on2.png" rel="disable">';
                $stat1 = "Active";
                // $task="disable";
            } else {
                $class = 'class="active delete"';
                $status = '<img src="' . base_url() . 'assets/images/off2.png" rel="enable">';
                $stat1 = "Inactive";
                // $task="enable";
            }

            $gen .= '
				<tr>
					<td>' . $i . '.</td>
					
					<td><a href="' . base_url() . 'plan/edit_topup/' . $sobj->srvid . '">' . $sobj->srvname . '</a></td>
					<td>' . $topuptype . '</td><td>' . $accestype . '</td>
                                        <td>' . $datalimit . '</td>
                                        <td>' . $uprate . '</td>
                                        <td>' . $downrate . '</td>
                                        <td>' . $unacctn . '</td>
                                         <td>' . $days . '</td>
					<td>' . $cocurrency.' '.$payment . '</td>
					<td><a href="' . base_url() . 'plan/topupuser_stat/' . $sobj->srvid . '">' . $sobj->usercount . '</a></td>';



            if ($is_editable) {
                $gen .= '<td><a ' . $class . '  onclick = "change_topupstatus(\'' . $sobj->srvid . '\')" href="javascript:void(0)" id="' . $sobj->srvid . '"> ' . $status . ' </a></td>';
            } else {
                $gen .= '<td>' . $stat1 . '</td>';
            }
            $gen .= '<td><a href="' . base_url() . 'plan/edit_topup/' . $sobj->srvid . '">EDIT</a></td>';

            $delclass = ($is_deletable) ? "delettopup" : "";

            $gen .= ' <td><a href="javascript:void(0);" class="' . $delclass . '"  rel="' . $sobj->srvid . '">Delete</a></td>';



            $gen .= '</tr>
				';
            $i++;
        }


        $data['total_results'] = $total_search;
        $data['search_results'] = $gen;
         $data['result_count'] = $result_count;
        if(($qofset + $total_search) == $result_count){
			$data['loadmore'] = 0;
		}else{
			$data['loadmore'] = 1;
		}
		$data['limit'] = $qlmt;
		$data['offset'] = $qlmt+$qofset;

        echo json_encode($data);
    }

    public function topup_list_filter($topuptype) {
        
        $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";
         $qlmt = LIMIT; $qofset = SOFFSET;
		$limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
        $cond = $this->dept_topup_condition();
        $cond = ($cond == "allindia") ? "" : $cond;

        $planarr = array("datatopup" => 1, "unacctncy" => 2, "speed" => 3);
        //$condarr=array("enableplan"=>1);
        $query = $this->db->query("SELECT ss.topuptype,ss.is_private,ss.datalimit,ss.srvid,ss.downrate,ss.uprate,ss.srvname,ss.enableplan,spp.net_total,spp.gross_amt,spp.payment_type
            ,(SELECT COUNT(su.id) FROM sht_usertopupassoc su inner join sht_users sut on (su.uid=sut.uid) WHERE su.topup_id=ss.srvid AND su.terminate='0' and sut.enableuser='1') AS usercount FROM `sht_services` ss
                        LEFT JOIN `sht_topup_pricing` spp ON (ss.srvid=spp.srvid) WHERE ss.is_deleted='0' and ss.topuptype!='0' {$ispcond} and topuptype='" . $planarr[$topuptype] . "' {$cond}  order by ss.srvid desc LIMIT $qofset,$qlmt");
      // echo $this->db->last_query();
        return $query->result();
    }

    public function topupfilter_userslist($searchtype) {
        // echo "===--->>".$searchtype;
        $data = array();
        $gen = '';
        $total_search = 0;
        $cwhere = 'enableuser=1';

        $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
        $regiontype = $this->dept_region_type();
        
          $qlmt = LIMIT; $qofset = SOFFSET;
		$limit = $this->input->post('limit');
		$offset = $this->input->post('offset');
		if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
        
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and sht_users.isp_uid='".$isp_uid."' and sht_usertopupassoc.terminate=0";
        $permicond = '';
        $sczcond = '';
        if ($regiontype == "region") {
            $dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='" . $dept_id . "' AND status='1' AND is_deleted='0'");
            $total_deptregion = $dept_regionQ->num_rows();
            if ($dept_regionQ->num_rows() > 0) {
                $c = 1;
                foreach ($dept_regionQ->result() as $deptobj) {
                    $stateid = $deptobj->state_id;
                    $cityid = $deptobj->city_id;
                    $zoneid = $deptobj->zone_id;
                    if ($cityid == 'all') {
                        $permicond .= " (state='" . $stateid . "') ";
                    } elseif ($zoneid == 'all') {
                        $permicond .= " (state='" . $stateid . "' AND city='" . $cityid . "') ";
                    } else {
                        $permicond .= " (state='" . $stateid . "' AND city='" . $cityid . "' AND zone='" . $zoneid . "') ";
                    }
                    if ($c != $total_deptregion) {
                        $permicond .= ' OR ';
                    }
                    $c++;
                }
            }
            if($permicond!='')
            {
            $sczcond .= ' AND (' . $permicond . ')';
            }
        }
      
     $cwhere.= $ispcond;
       $cwhere .= $sczcond;
        $search_user = $this->input->post('search_user');
        //$searchtype = $this->input->post('searchtype');
        $state = $this->input->post('state');
        $city = $this->input->post('city');
        $zone = $this->input->post('zone');
        $locality = $this->input->post('locality');
        if (isset($state) && $state != '') {
            if ($state != 'all') {
                $cwhere .= " AND state='" . $state . "'";
            } else {
                $cwhere .= '';
            }
        }
        if (isset($city) && $city != '') {
            if ($city != 'all') {
                $cwhere .= " AND city='" . $city . "'";
            } else {
                $cwhere .= '';
            }
        }
        if (isset($zone) && $zone != '') {
            if ($zone != 'all') {
                $cwhere .= " AND zone='" . $zone . "'";
            } else {
                $cwhere .= '';
            }
        }
        if (isset($locality) && $locality != '') {
            $cwhere .= " AND usuage_locality='" . $locality . "'";
        }

        if (isset($search_user)) {
            $cwhere .= " AND ((username LIKE '%" . $search_user . "%') OR (firstname LIKE '%" . $search_user . "%') OR (mobile LIKE '%" . $search_user . "%')) ";
        }
       
	$ispcodet = $this->countrydetails();
	$cocurrency = $ispcodet['currency'];

        if ($cwhere != '') {
            // echo $cwhere;
            $this->db->where($cwhere);
            $this->db->group_by("sht_usertopupassoc.uid ");
            $this->db->limit($qlmt, $qofset);
        }
        $this->db->select('sht_users.id,sht_users.state,sht_users.city,sht_users.enableuser,sht_users.zone,sht_users.account_activated_on,sht_users.paidtill_date,sht_users.uid,sht_users.expiration,sht_users.firstname,sht_users.lastname');
        $this->db->from('sht_users inner join sht_usertopupassoc on (sht_usertopupassoc.uid=sht_users.uid and sht_usertopupassoc.topup_id="' . $searchtype . '") ');
        //

        $cquery = $this->db->get();
       // echo $this->db->last_query();die;
        // echo "<pre>"; print_R($cquery->result()); die;
        //    echo $this->db->last_query();die;
        $total_csearch = $cquery->num_rows();
        if ($total_csearch > 0) {
            $i = 1;
            foreach ($cquery->result() as $sobj) {
                $ticket_count = $this->user_model->customer_tickets_count($sobj->id);
                $state = $this->user_model->getstatename($sobj->state);
                $city = $this->user_model->getcityname($sobj->state, $sobj->city);
                $zone = $this->user_model->getzonename($sobj->zone);
                $status = ($sobj->enableuser == 1) ? 'active' : 'inactive';
                $activeplan = $this->user_model->userassoc_planname($sobj->id);
                $active_from = date('d-m-Y', strtotime($sobj->account_activated_on));
                $renewal_date = date('d-m-Y', strtotime($sobj->paidtill_date));
                $balance = $this->user_model->userbalance_amount($sobj->uid);
                $expiration = $sobj->expiration;
                if ($expiration == '0000-00-00 00:00:00') {
                    $expiration = '-';
                } else {
                    $expiration = date('d-m-Y', strtotime($sobj->expiration));
                }

                $gen .= "<tr><td>&nbsp;</td><td><div class='checkbox' style='margin-top:0px; margin-bottom:0px;'><label><input type='checkbox' class='collapse_checkbox' value='" . $sobj->id . "'></label></div></td><td><a href='" . base_url() . "user/edit/" . $sobj->id . "'>" . $sobj->uid . "</a></td><td>" . $sobj->firstname . " " . $sobj->lastname . "</td><td>" . $state . "</td><td>" . $city . "</td><td>" . $zone . "</td><td>" . $activeplan . "</td><td>" . $active_from . "</td><td>" . $renewal_date . "</td><td>" . $expiration . "</td><td>" . $status . "</td><td><span>".$cocurrency."</span> " . $balance . "</td><td class='mui--text-right'>" . $ticket_count . "</td></tr>";
                $i++;
            }
        }

        if ($total_csearch == 0) {
            $gen .= "<tr><td style='text-align:center' colspan='14'> No Result Found !!</td></tr>";
        }

        $data['total_results'] = $total_csearch;
        $data['search_results'] = $gen;

        return $data;
    }

    public function is_permission($slug, $permtype = "RO") {

        $userid = $this->session->userdata['isp_session']['userid'];
       $sessiondata = $this->session->userdata('isp_session');
      
       $query=$this->db->query("select dept_id from sht_isp_users where id='".$userid."'");
       $rowarr=$query->row_array();
         $isp_uid = $sessiondata['isp_uid'];
        if ($sessiondata['super_admin'] == 1) {
            return true;
        } else {
            $deptid = $rowarr['dept_id'];
            $query1 = $this->db->query("select id from sht_tab_menu where slug='" . $slug . "' ");
            $tabarr = $query1->row_array();
            $tabid = $tabarr['id'];
            //  echo $deptid."::".$tabid;

            $field = '';
            if ($permtype == "ADD") {
                $field = 'is_add';
            } else if ($permtype == "EDIT") {
                $field = 'is_edit';
            } else if ($permtype == "DELETE") {
                $field = 'is_delete';
            } else if ($permtype == "HIDE") {
                $field = 'is_hide';
            } else {
                $field = 'is_readonly';
            }

            $query2 = $this->db->query("select " . $field . " as perm from sht_isp_permission where isp_deptid='" . $deptid . "' and tabid='" . $tabid . "'");
            if ($query2->num_rows() > 0) {
                $permarr = $query2->row_array();
                if ($permarr['perm'] == 1) {
                    if($permtype=="HIDE")
                    {
                         return false;
                    }
                    else
                    {
                         return true;
                    }
                   
                } else {
                     if($permtype=="HIDE")
                    {
                         return true;
                    }
                    else
                    {
                         return false;
                    }
                    //return false;
                }
            } else {
                return false;
            }
        }
    }

    public function leftnav_permission() {

        $userid = $this->session->userdata['isp_session']['userid'];
        $query = $this->db->query("select id,super_admin,dept_id from sht_isp_users where id='" . $userid . "'");
        $tabarr = array();
        $rowarr = $query->row_array();
        $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];

        $deptid = $rowarr['dept_id'];


        if ($sessiondata['super_admin'] == 1) {
            $query = $this->db->query("select slug from sht_tab_menu where status='1'");
            foreach ($query->result() as $val) {
                $tabarr[$val->slug] = 0;
            }
        } else {
            $query1 = $this->db->query("SELECT sip.is_hide,stm.slug FROM `sht_isp_permission` sip
INNER JOIN `sht_tab_menu` stm ON (stm.id=sip.tabid) WHERE isp_deptid='" . $deptid . "' ");
            foreach ($query1->result() as $val) {
                $tabarr[$val->slug] = ($sessiondata['super_admin'] == 1) ? 0 : $val->is_hide;
            }
        }
        $tabarr['PUBLICWIFI']=$this->isp_publicwifi_perm();
        //  echo $deptid."::".$tabid;
      
       
        return $tabarr;
    }
    
    public function isp_publicwifi_perm()
    {
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $query=$this->db->query("select id from sht_isp_admin_modules where isp_uid='".$isp_uid."' and module='7' and status='1'");
      //   echo $this->db->last_query();die;
         if($query->num_rows()>0)
         {
             return 1;
         }
         else
         {
             return 0;
         }
         
    }

	
    public function plan_nas_association($plan_id) {
	$condarr = array();
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_regionQ = $this->db->query("select state_id,city_id,zone_id from sht_plan_region where plan_id='" . $plan_id . "'");
         $total_deptregion= $dept_regionQ->num_rows();
         $permicond='';
         $sczcond='';
          if ($dept_regionQ->num_rows() > 0) {
                $c = 1;
         foreach ($dept_regionQ->result() as $deptobj) {
                    $stateid = $deptobj->state_id;
                    $cityid = $deptobj->city_id;
                    $zoneid = $deptobj->zone_id;
                    if ($cityid == 'all') {
                        $permicond .= " (nasstate='" . $stateid . "') ";
                    } elseif ($zoneid == 'all') {
                        $permicond .= " (nasstate='" . $stateid . "' AND nascity='" . $cityid . "') ";
                    } else {
                        $permicond .= " (nasstate='" . $stateid . "' AND nascity='" . $cityid . "' AND naszone='" . $zoneid . "') ";
                    }
                    if ($c != $total_deptregion) {
                        $permicond .= ' OR ';
                    }
                    $c++;
                }
          }
            
            if($permicond!='')
            {
            $sczcond .= '(' . $permicond . ')';
            }
              $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
            
             $query1 = $this->db->query("select distinct(id) from nas where {$sczcond} {$ispcond}");
       // echo $this->db->last_query();die;

        foreach ($query1->result() as $val) {
            $query1 = $this->db->query("select srvid from sht_plannasassoc where srvid='" . $plan_id . "' and nasid='" . $val->id . "'");
            if ($query1->num_rows() == 0) {
                $tabledata = array('srvid' => $plan_id, 'nasid' => $val->id);
                $this->db->insert('sht_plannasassoc', $tabledata);
            }
        }
    }

    public function dept_plan_condition() {
        $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
        $regiontype = $this->dept_region_type();
        $cond = '';
        if ( $superadmin == 1) {

            $cond = 'allindia';
        } else {
            $query = $this->db->query("select state_id,city_id,zone_id from sht_dept_region where dept_id='" . $dept_id . "'");
            if ($query->num_rows() > 0) {
                $condarr = array();





                foreach ($query->result() as $val) {

                    if ($val->state_id != 'all' && $val->city_id != 'all' && $val->zone_id != 'all') {
                        $condarr[] = "(state_id='" . $val->state_id . "' and city_id='" . $val->city_id . "' and zone_id='" . $val->zone_id . "')";
                    } else if ($val->state_id != 'all' && $val->city_id == 'all') {
                        $condarr[] = "(state_id='" . $val->state_id . "')";
                    } else if ($val->state_id != 'all' && $val->city_id != 'all' && $val->zone_id == 'all') {
                        $condarr[] = "(state_id='" . $val->state_id . "' and city_id='" . $val->city_id . "')";
                    }
                }







                $cond1 = implode('OR', $condarr);
                $query2 = $this->db->query("select srvid from sht_plan_pricing where region_type='allindia'");
                $paramarr1 = array();
                if ($query2->num_rows() > 0) {
                    foreach ($query2->result() as $val2) {
                        $paramarr1[] = $val2->srvid;
                    }
                }


                $query1 = $this->db->query("select distinct(plan_id) from sht_plan_region where ({$cond1}) and status='1' and is_deleted='0'");
                //   echo $this->db->last_query(); die;

                $paramarr = array();
                if ($query1->num_rows() > 0) {
                    foreach ($query1->result() as $val1) {
                        $paramarr[] = $val1->plan_id;
                    }
                    $paramarr = array_merge($paramarr, $paramarr1);
                    $paramid = '"' . implode('", "', $paramarr) . '"';
                    $cond .= "AND ss.srvid IN ({$paramid})";
                } else {
                    $paramarr = array_merge($paramarr, $paramarr1);
                    if (!empty($paramarr)) {
                        $paramid = '"' . implode('", "', $paramarr) . '"';
                        $cond .= "AND ss.srvid IN ({$paramid})";
                    } else {
                        $cond .= "allindia";
                    }
                }
            }
        }
        // echo $cond; die;
        return $cond;
    }

    public function dept_topup_condition() {
        $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
        $regiontype = $this->dept_region_type();
        $cond = '';
        if ( $superadmin == 1) {
            $cond = 'allindia';
        } else {
            $query = $this->db->query("select state_id,city_id,zone_id from sht_dept_region where dept_id='" . $dept_id . "'");
            if ($query->num_rows() > 0) {
                $condarr = array();
                foreach ($query->result() as $val) {
                    if ($val->state_id != 'all' && $val->city_id != 'all' && $val->zone_id != 'all') {
                        $condarr[] = "(state_id='" . $val->state_id . "' and city_id='" . $val->city_id . "' and zone_id='" . $val->zone_id . "')";
                    } else if ($val->state_id != 'all' && $val->city_id == 'all') {
                        $condarr[] = "(state_id='" . $val->state_id . "')";
                    } else if ($val->state_id != 'all' && $val->city_id != 'all' && $val->zone_id == 'all') {
                        $condarr[] = "(state_id='" . $val->state_id . "' and city_id='" . $val->city_id . "')";
                    }
                }
                $cond1 = implode('OR', $condarr);
                $query2 = $this->db->query("select srvid from sht_topup_pricing where region_type='allindia'");
                $paramarr1 = array();
                if ($query2->num_rows() > 0) {
                    foreach ($query2->result() as $val2) {
                        $paramarr1[] = $val2->srvid;
                    }
                }

                $query1 = $this->db->query("select distinct(topup_id) from sht_topup_region where ({$cond1}) and status='1' and is_deleted='0'");
                //   echo $this->db->last_query();die;
                $paramarr = array();
                if ($query1->num_rows() > 0) {
                    foreach ($query1->result() as $val1) {
                        $paramarr[] = $val1->topup_id;
                    }
                    $paramarr = array_merge($paramarr, $paramarr1);
                    $paramid = '"' . implode('", "', $paramarr) . '"';
                    $cond .= "AND ss.srvid IN ({$paramid})";
                } else {
                    $paramarr = array_merge($paramarr, $paramarr1);
                    if (!empty($paramarr)) {
                        $paramid = '"' . implode('", "', $paramarr) . '"';
                        $cond .= "AND ss.srvid IN ({$paramid})";
                    } else {
                        $cond .= "allindia";
                    }
                }
            }
        }
        return $cond;
    }
    
    public function add_system_serviceid($srvid)
    {
       $tabledata=array("srvid"=>$srvid,"disabledservice"=>0,"expiredservice"=>0,"usermacauth"=>0);
        $this->db->insert("sht_system",$tabledata);
        
    }
    
    public function disable_system_serviceid($srvid)
    {
         $tabledata=array("disabledservice"=>1);
          $this->db->update("sht_system", $tabledata, array('srvid' => $srvid));
    }
    
     public function enable_system_serviceid($srvid)
    {
         $tabledata=array("disabledservice"=>0);
          $this->db->update("sht_system", $tabledata, array('srvid' => $srvid));
    }
    
      public function get_tax()
        {
           $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
              $data=array();
           $query=$this->db->query("select tax from sht_tax where status=1 {$ispcond}");
         //  echo $this->db->last_query(); die;
           if($query->num_rows()>0)
           {
               $rowarr=$query->row_array();
               $data['tax']=$rowarr['tax'];
           }
           else
           {
               $data['tax']=0;
           }
           return $data;
        }
        
        public function check_taxfilled()
        {
            $sessiondata = $this->session->userdata('isp_session');
             $isp_uid = $sessiondata['isp_uid'];
             $query=$this->db->query("select tax from sht_tax where  status=1 and isp_uid='".$isp_uid."'");
            // echo $this->db->last_query();die;
             if($query->num_rows()>0)
             {
                 return 1;
             }
             else
             {
                  return 0;
             }
        } 
        
    public function check_ispmodules(){
	$moduleArr = array();
	$query = $this->db->query("SELECT module FROM sht_isp_admin_modules WHERE isp_uid='".ISPID."' AND status='1' AND (module='1' OR module='7')");
	if($query->num_rows() > 0){
	    foreach($query->result() as $mobj){
		$moduleArr[] = $mobj->module;
	    }
	}
	if(count($moduleArr) > 0){
	    if((count($moduleArr) == 1) && (in_array('7' , $moduleArr))){
		redirect(base_url().'publicwifi');
	    }
	}
    }
    
    public function syslogdata()
    {
	 $this->DB2 = $this->load->database('db2_syslog', TRUE);
    $postdata=$this->input->post();
    $cond="";
    $limit = $this->input->post('limit');
    $offset = $this->input->post('offset');
    $data=array();
    if($postdata['startdate']!='')
    {
	$startdate=date("Y-m-d",strtotime($postdata['startdate']));
	$enddate=date("Y-m-d",strtotime($postdata['endate']));
    $cond.="AND date BETWEEN '".$startdate."' AND '".$enddate."'";
    }
    if($postdata['starttime']!='')
    {
     $cond.="AND time BETWEEN '".$postdata['starttime']."' AND '".$postdata['endtime']."'";
    }
    if(isset($limit) && $limit != ''){ $qlmt = $limit; }
		if(isset($offset) && $offset != ''){ $qofset = $offset; }
    
    $query= $this->DB2->query("select `date`,`time`,`nasip`,`nasname`,`conntype`,`username`,`ipproto`,`srcip`,`srcport`,`dstip`,`dstport` from decibel_logs
		     where username='".$postdata['user']."' {$cond} order by id desc LIMIT $qofset,$qlmt");
    
    $gen='';
   $i=1;
    foreach($query->result() as $vald)
    {
     $gen .= '
				<tr>
					
					<td>'.$vald->nasip.'</td>
					<td>' . $vald->nasname. '</td>
                                        <td>' . $vald->conntype. '</td>
                                        <td>' . $vald->ipproto . '</td>
                                        <td>' . $vald->srcip . '</td>
                                        <td>' . $vald->srcport . '</td>
                                         <td>' . $vald->dstip . '</td>
					<td> ' . $vald->dstport . '</td>
					<td> ' . $vald->date." ".$vald->time . '</td>
					</tr>';

    $i++;
    }
    $data['html']=$gen;
    $data['limit'] = $qlmt;
    $data['offset'] = $qlmt+$qofset;
return $data;  
    
    }
    
    
    public function add_syslogconn()
    {
	$data=array();
	$postdata=$this->input->post();
	/*$postdata['hostname']="localhost";
	$postdata['username']="root";
	$postdata['password']="";
	$postdata['dbname']="isp_portal";*/
	//echo "../../application/config/database.php";die;
	//echo APPPATH;die;
	$editorpath="/../";
	
	 $reading = fopen(APPPATH."config/database.php", 'r');
        $writing = fopen(APPPATH."config/database.tmp", 'w');
        $replaced = false;
	//echo "<pre>"; print_R($reading);die;
        while (!feof($reading)) {
            $line = fgets($reading);
	  
		
                if(strpos( $line,"syshostname=")){
		   
                    $line = '$syshostname="'.$postdata['hostname'].'";'."\n";
                }
		//echo $line."<br/>";
                if(strpos( $line,"sysusername=")){
		  //   echo "sysusername<br/>";
                   $line = '$sysusername="'.$postdata['username'].'";'."\n";
                }
		  if(strpos( $line,"syspwd=")){
		  //  echo "syspwd<br/>";
                   $line = '$syspwd="'.$postdata['password'].'";'."\n";
                }
		 if(strpos( $line,"sysdbname=")){
		   //  echo "sysdbname<br/>";
                   $line = '$sysdbname="'.$postdata['dbname'].'";'."\n";
                }
         //   }
            
            if(feof($reading) && $replaced !== true){
            //  $line = 'define("ISPID", "'.$isp_uid.'");';
              $replaced = true;  
            }
            fputs($writing, $line);
        }
        fclose($reading); fclose($writing);
        if ($replaced) {
	    
            rename(APPPATH."config/database.tmp", APPPATH."config/database.php");
        } else {
	    
            unlink(APPPATH."config/database.tmp");
        }
	$conn=$this->check_sysdatabase();
	if($conn)
	{
	    $data['resultcode']=1;
	}
	else
	{
	   $data['resultcode']=0;
	}
	return $data;
	
	
    
    }
    
     public function check_sysdatabase()
    {
        
        ini_set('display_errors', 'Off');
        
        //  Load the database config file.
        if(file_exists($file_path = APPPATH.'config/database.php'))
        {
            include($file_path);
        }
        
        $config = $db['db2_syslog'];
	
         if( $config['dbdriver'] === 'mysqli' )
        {
            $mysqli = new mysqli( $config['hostname'] , $config['username'] , $config['password'] , $config['database'] );
            if( !$mysqli->connect_error )
            {
                return true;
            }
            else{
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    public function isp_license_data(){
		$wallet_amt = 0; $passbook_amt = 0;
		$walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='".ISPID."' AND is_paid='1'");
		if($walletQ->num_rows() > 0){
		    $wallet_amt = $walletQ->row()->wallet_amt;
		}
		
		$passbookQ = $this->db->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost FROM sht_isp_passbook WHERE isp_uid='".ISPID."'");
		if($passbookQ->num_rows() > 0){
		    $passrowdata = $passbookQ->row();
		    $passbook_amt = $passrowdata->activeusers_cost;
		}
		$balanceamt = $wallet_amt - $passbook_amt;
		if($balanceamt < 0){
			return 0;
		}else{
			return 1;
		}
	}
	
	  public function add_inventory_category(){
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
		$cat_name = $this->input->post('cat_name');
		
		 $catq=$this->db->query("select id,category_name from sht_inventory_category where category_name='".$cat_name."'
					 and isp_uid ='".$isp_uid."'
					 ");
		 if($catq->num_rows()>0)
		 {
		    $rowarr=$catq->row_array();
		 $data['html']="<option value='".$rowarr['id']."' selected>".$rowarr['category_name']."</option>";
                $data['cat_id']=$rowarr['id'];
		 }
		 else
		 {
		  $tabledata=array("category_name"=>$cat_name,"status"=>1,"added_on"=>date("Y-m-d H:i:s"),"isp_uid"=>$isp_uid);
		$this->db->insert("sht_inventory_category",$tabledata);
                $id = $this->db->insert_id();
                $data=array();
                $data['html']="<option value='".$id."' selected>".$cat_name."</option>";
                $data['city_id']=$id;
		 }
               
             //   $this->db->last_query(); die;
		return $data;
	}
	
	public function inventory_category_list()
	{
	      $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
	$query=$this->db->query("select `id`,`category_name` from sht_inventory_category where status='1' and is_deleted='0' and isp_uid='".$isp_uid."'");
	return $query->result();
	}
	
	public function inventory_brand_list()
	{
	$sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
	$query=$this->db->query("select `id`,`brand_name` from sht_inventory_brand where status='1' and is_deleted='0' and isp_uid='".$isp_uid."'");
	return $query->result();
	}
	
	public function inventory_product_list()
	{
	$sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
	$query=$this->db->query("select `id`,`product_name` from sht_inventory_product where status='1' and is_deleted='0' and isp_uid='".$isp_uid."'");
	return $query->result();
	}
	
	
	 public function add_inventory_brand(){
	    $postdata=$this->input->post();
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
		$brand_name = $this->input->post('brand_name');
		
		 $catq=$this->db->query("select id,brand_name from sht_inventory_brand where brand_name='".$brand_name."'
					 and isp_uid ='".$isp_uid."'
					 ");
		 if($catq->num_rows()>0)
		 {
		    $rowarr=$catq->row_array();
		 $data['html']="<option value='".$rowarr['id']."' selected>".$rowarr['brand_name']."</option>";
               
		 }
		 else
		 {
		  $tabledata=array("category_id"=>$postdata['category_name'],"brand_name"=>$postdata['brand_name'],
				   "status"=>1,"added_on"=>date("Y-m-d H:i:s"),"isp_uid"=>$isp_uid);
		$this->db->insert("sht_inventory_brand",$tabledata);
                $id = $this->db->insert_id();
                $data=array();
                $data['html']="<option value='".$id."' selected>".$brand_name."</option>";
               
		 }
               
             //   $this->db->last_query(); die;
		return $data;
	}
	
	
	
	 public function brand_list(){
	    $postdata=$this->input->post();
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
		$cat_id = $this->input->post('catid');
		
		$catq=$this->db->query("select id,brand_name from sht_inventory_brand where category_id='".$cat_id."'
					 and isp_uid ='".$isp_uid."'
					 
					 ");
		// echo $this->db->last_query(); die;
		 if($catq->num_rows()>0)
		 {
		   // $rowarr=$catq->row_array();
		    $gen="";
		    //$rowarr=$catq->row_array();
		    foreach($catq->result() as $val)
		    {
			$gen.="<option value='".$val->id."' >".$val->brand_name."</option>";;
		    }
		     $data['html']=$gen;
		 
               
		 }
		 else
		 {
		 $data['html']="";
               
		 }
               
             //   $this->db->last_query(); die;
		return $data;
	}
	
	public function product_list(){
	    $postdata=$this->input->post();
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
		$brand_id = $this->input->post('brandid');
		
		 $catq=$this->db->query("select id,product_name from sht_inventory_product where brand_id='".$brand_id."'
					 and isp_uid ='".$isp_uid."'
					 
					 ");
		// echo $this->db->last_query(); die;
		 if($catq->num_rows()>0)
		 {
		    $gen="";
		    //$rowarr=$catq->row_array();
		    foreach($catq->result() as $val)
		    {
			$gen.="<option value='".$val->id."' >".$val->product_name."</option>";;
		    }
		    
		 $data['html']=$gen;
               
		 }
		 else
		 {
		 $data['html']="";
               
		 }
               
             //   $this->db->last_query(); die;
		return $data;
	}
	
	public function add_inventory_product(){
	    $postdata=$this->input->post();
	    //echo "<pre>"; print_R($postdata); die;
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
		$product_name = $this->input->post('product_name');
		
		 $catq=$this->db->query("select id,product_name from sht_inventory_product where product_name='".$product_name."'
					 and isp_uid ='".$isp_uid."'
					 ");
		 if($catq->num_rows()>0)
		 {
		    $rowarr=$catq->row_array();
		 $data['html']="<option value='".$rowarr['id']."' selected>".$rowarr['product_name']."</option>";
               
		 }
		 else
		 {
		    
		  $tabledata=array("product_name"=>$postdata['product_name'],"category_id"=>$postdata['category_name'],
				   "brand_id"=>$postdata['brand_name'],"model_no"=>$postdata['brand_name'],
				   "cost_price"=>$postdata['cost_price'],"sales_price"=>$postdata['sale_price'],
				   "unit_type"=>$postdata['unit'],
				  
				   "status"=>1,"added_on"=>date("Y-m-d H:i:s"),"isp_uid"=>$isp_uid);
		$this->db->insert("sht_inventory_product",$tabledata);
                $id = $this->db->insert_id();
                $data=array();
                $data['html']="<option value='".$id."' selected>".$product_name."</option>";
               
		 }
               
             //   $this->db->last_query(); die;
		return $data;
	}
	
	public function add_user_inventory()
	{
	    $postdata=$this->input->post();
	    $sessiondata = $this->session->userdata('isp_session');
	    $isp_uid = $sessiondata['isp_uid'];
	    $tabledata=array("user_id"=>$postdata['subscriber_uid'],"category_id"=>$postdata['category_name'],
			     "brand_id"=>$postdata['brand_name'],"product_id"=>$postdata['prod_name'],
			     "unit_no"=>$postdata['unit'],"status"=>1,"serial_number"=>$postdata['serial_number'],
			     "added_on"=>date("Y-m-d H:i:s"));
	    $this->db->insert("sht_inventory_user",$tabledata);
                $id = $this->db->insert_id();
		
		 $query=$this->db->query("SELECT siu.id,siu.user_id,siu.is_replaced,sic.category_name,sib.brand_name,
				      sip.product_name,siu.serial_number,sip.price,sip.`cost_price`,sip.`sales_price`,siu.unit_no,siu.is_replaced FROM `sht_inventory_user` siu
	    INNER JOIN `sht_inventory_category` sic ON (sic.id=siu.category_id)
	    INNER JOIN `sht_inventory_brand` sib ON (sib.id=siu.brand_id)
	    INNER JOIN `sht_inventory_product` sip ON (sip.id=siu.product_id)
	    WHERE siu.status='1' AND siu.is_deleted='0' AND siu.id='".$id."'");
	      $rowarr=$query->row_array();
	      $isreplaced=($rowarr['is_replaced']==1)?"Replaced":"-";
	      $gen="";
	      
	      $gen.='<tr>
										     <td class="rcat">'.$rowarr['category_name'].'</td>
										     <td class="rbrand">'.$rowarr['brand_name'].'</td>
										     <td class="rproduct">'.$rowarr['product_name'].'</td>
										     <td class="rserial">'.$rowarr['serial_number'].'</td>
										     <td class="rcostprice">'.$rowarr['cost_price']*$rowarr['unit_no'].'</td>
										     <td class="rsaleprice">'.$rowarr['sales_price']*$rowarr['unit_no'].'</td>
										       <td class="status">'.$isreplaced.'</td>
										     <td> <a href="javascript:void(0);" class="edituser" id="edituser_'.$id.'" rel="'.$id.'">Edit</a></td> 
                                                                        <td><a href="javascript:void(0);" class="deletuser"  rel="'.$id.'">Delete</a></td>';
									if($rowarr['is_replaced']==0){
						 $gen.=			'<td><a href="javascript:void(0);" class="replaceuser"  rel="'.$id.'">Replace</a></td>'; 
									}
							$gen.=		   ' </tr>';
			$data['html']=$gen;
			return $data;
		
	}
	
	public function edit_user_inventory_submit()
	{
	     
	      $postdata=$this->input->post();
	    $sessiondata = $this->session->userdata('isp_session');
	    $isp_uid = $sessiondata['isp_uid'];
	    $tabledata=array("category_id"=>$postdata['category_name'],
			     "brand_id"=>$postdata['brand_name'],"product_id"=>$postdata['prod_name'],
			     "unit_no"=>$postdata['unit'],"status"=>1,"serial_number"=>$postdata['serial_number'],
			     "added_on"=>date("Y-m-d H:i:s"));
	     $this->db->update('sht_inventory_user', $tabledata, array('id' => $postdata['userid']));
	      $query=$this->db->query("SELECT siu.id,siu.user_id,siu.is_replaced,sic.category_name,sib.brand_name,
				      sip.product_name,siu.serial_number,sip.price,sip.`cost_price`,sip.`sales_price`,siu.unit_no,siu.is_replaced FROM `sht_inventory_user` siu
	    INNER JOIN `sht_inventory_category` sic ON (sic.id=siu.category_id)
	    INNER JOIN `sht_inventory_brand` sib ON (sib.id=siu.brand_id)
	    INNER JOIN `sht_inventory_product` sip ON (sip.id=siu.product_id)
	    WHERE siu.status='1' AND siu.is_deleted='0' AND siu.id='".$postdata['userid']."'");
	      $rowarr=$query->row_array();
	      $data['rcat']=$rowarr['category_name'];
	      $data['rbrand']=$rowarr['brand_name'];
	      $data['rproduct']=$rowarr['product_name'];
	      $data['rserial']=$rowarr['serial_number'];
	      $data['rprice']=$rowarr['cost_price']*$rowarr['unit_no'];
	       $data['saleprice']=$rowarr['sales_price']*$rowarr['unit_no'];
	       $data['id']=$postdata['userid'];
	      return $data;
               
	}
	
	 public function delete_inventoryuser()
        {
	    $postdata=$this->input->post();
           $id=$postdata['userid'];
	   $tabledata=array("is_deleted"=>1);
	    $this->db->update('sht_inventory_user', $tabledata, array('id' => $postdata['userid']));
	    $data['result']=1;
            return $data;
        }
	
	 public function replace_inventoryuser()
        {
	    $postdata=$this->input->post();
           $id=$postdata['userid'];
	   $tabledata=array("is_replaced"=>1);
	   $this->db->update('sht_inventory_user', $tabledata, array('id' => $postdata['userid']));
	    $data['result']=1;
            return $data;
        }
	
	public function inventory_list($uid)
	{
	   
	    $sessiondata = $this->session->userdata('isp_session');
	     $isp_uid = $sessiondata['isp_uid'];
	    $query=$this->db->query("SELECT siu.id,siu.user_id,siu.is_replaced,sic.category_name,sib.brand_name,
				      sip.product_name,siu.serial_number,sip.price,sip.`cost_price`,sip.`sales_price`,siu.unit_no,siu.is_replaced FROM `sht_inventory_user` siu
	    INNER JOIN `sht_inventory_category` sic ON (sic.id=siu.category_id)
	    INNER JOIN `sht_inventory_brand` sib ON (sib.id=siu.brand_id)
	    INNER JOIN `sht_inventory_product` sip ON (sip.id=siu.product_id)
	    WHERE siu.status='1' AND siu.is_deleted='0' AND siu.user_id='".$uid."'");
	return $query->result();
	
	}
	
	public function edit_user_inventory()
	{
	    $postdata=$this->input->post();
	    $catgorylist=$this->inventory_category_list();
	    $brandlist=$this->inventory_brand_list();
	    $productlist=$this->inventory_product_list();
	    $query=$this->db->query("select `user_id`,`category_id`,`brand_id`,`product_id`,`unit_no`,serial_number from sht_inventory_user where id='".$postdata['id']."'");
	   $rowarr = $query->row_array();
	    $gen="";
	    $gen.=' <form action="" method="post" id="edit_user_inventory" autocomplete="off" onsubmit="edit_submituser_inventory(); return false;">
		
		  <input type="hidden" name="userid" value="'.$postdata['id'].'">
               <div class="row">
	       <div class="col-lg-2 col-md-2 col-sm-2"></div>
                  <div class="col-lg-3 col-md-3 col-sm-3">
                     <div class="row" style="margin-top:12px">
                        <div class="col-sm-12 col-xs-12">
                           <h4 class="toggle_heading">Select Category</h4>
                        </div>
                     </div>
                  </div>
		  
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <div class="row">
                        
                            <select class="category_name form-control" name="category_name" required>
                               <option value="" >Select Category</option>';
			       foreach($catgorylist as $val){
				$selected=($val->id==$rowarr['category_id'])?"selected":"";
			$gen.=      ' <option value="'.$val->id.'" '.$selected.'>'.$val->category_name.'</option>';
			        } 
                                
                           $gen.=     ' </select>
                             
                            </div>
                      </div>
                  <div class="col-lg-2 col-md-2 col-sm-2"></div>
               </div>
               <div class="row">
	       <div class="col-lg-2 col-md-2 col-sm-2"></div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                     <div class="row" style="margin-top:12px">
                        <div class="col-sm-12 col-xs-12">
                           <h4 class="toggle_heading">Select Brand</h4>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4 col-xs-4">
                    <div class="row">
                        
                            <select class="brand_name form-control" name="brand_name" required>
                            <option value="">Select Brand</option>';
			      foreach($brandlist as $val){
				$selected=($val->id==$rowarr['brand_id'])?"selected":"";
			$gen.=      ' <option value="'.$val->id.'" '.$selected.'>'.$val->brand_name.'</option>';
			        } 
                               $gen.= ' </select>
                              
                            </div>
                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2"></div>
               </div>
               
               <div class="row">
	       <div class="col-lg-2 col-md-2 col-sm-2"></div>
                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                     <div class="row" style="margin-top:12px">
                        <div class="col-sm-12 col-xs-12">
                           <h4 class="toggle_heading">Select Product</h4>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4 col-xs-4">
                    <div class="row">
                   
                            <select class="prod_name form-control" name="prod_name" required>
                               <option value="">Select Product</option>';
			        foreach($productlist as $val){
				$selected=($val->id==$rowarr['product_id'])?"selected":"";
			$gen.=      ' <option value="'.$val->id.'" '.$selected.'>'.$val->product_name.'</option>';
			        } 
                             
                                
                                $gen.= '</select>
                              
                            </div>
                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2"></div>
               </div>
               
               <div class="row">
	       <div class="col-lg-2 col-md-2 col-sm-2"></div>
                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                     <div class="row" style="margin-top:20px">
                        <div class="col-sm-12 col-xs-12">
                           <h4 class="toggle_heading">Select Quantity</h4>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4 col-xs-4">
                    <div class="row">
                       <div class="mui-textfield mui-textfield--float-label">
                        <input class="mui--is-empty mui--is-untouched mui--is-pristine" name="unit" type="text" value="'.$rowarr['unit_no'].'" required>
                        <label>Enter units/length/weight</label>
                     </div>
                            </div>
                  </div>
		  <div class="col-lg-2 col-md-2 col-sm-2"></div>
               </div>
	         <div class="row">
		  <div class="col-lg-2 col-md-2 col-sm-2"></div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                     <div class="row" style="margin-top:20px">
                        <div class="col-sm-12 col-xs-12">
                           <h4 class="toggle_heading">Serial Number:</h4>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4 col-xs-4">
                    <div class="row">
                       <div class="mui-textfield mui-textfield--float-label">
                        <input class="mui--is-empty mui--is-untouched mui--is-pristine" name="serial_number" value="'.$rowarr['serial_number'].'" type="text" required>
                        <label>Serial Number<sup>*</sup></label>
                     </div>
                            </div>
                  </div>
		  <div class="col-lg-2 col-md-2 col-sm-2"></div>
               </div>
                <div class="form-group">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <div class="row">
			     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>

                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left:0px;">
                               <input type="submit" class="mui-btn mui-btn--accent btn-lg" value="Add Product">
                               </div>
                              </div>
                              </div>
			      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                          </div>
	     </form>';
	     
	     
	     $data['html']=$gen;
	     return $data;
	    
	    
	
	}
	
	
	public function check_configuration()
	{
	    $data=array();
	    $baseplanid=0;
	     $postdata=$this->input->post();
	    $sessiondata = $this->session->userdata('isp_session');
	    $isp_uid = $sessiondata['isp_uid'];
	    
	    $query=$this->db->query("select baseplanid from sht_users where uid='".$postdata['uid']."'");
	    if($query->num_rows()>0)
	    {
	    $rowarr=$query->row_array();
		if($rowarr['baseplanid']==0 || $rowarr['baseplanid']=="" )
		{
		    $data['is_plan_setup']=0;
		}
		else
		{
		    $baseplanid=$rowarr['baseplanid'];
		    $data['is_plan_setup']=1;
		}
	    }
	    else
	    {
		$data['is_plan_setup']=0;
	    }
	    
	     $nasquery=$this->db->query("select nasname from nas where isp_uid='".$isp_uid."'");
	    if($nasquery->num_rows()>0)
	    {
		 $data['is_nas_setup']=1;
	    }
	    else
	    {
		$data['is_nas_setup']=0;
	    }
	    
	    if($baseplanid!=0)
	    {
		$nasquery=$this->db->query("select id from sht_plannasassoc where srvid='".$baseplanid."'");
	    if($nasquery->num_rows()>0)
	    {
		 $data['is_plan_nas_setup']=1;
	    }
	    else
	    {
		$data['is_plan_nas_setup']=0;
	    }
	    }
	    else
	    {
	    $data['is_plan_nas_setup']=0;
	    }
	    
	    return $data;
	    
	    
	}
	
    public function listing_nas() {
        //$condarr=array('status'=>1,"is_deleted"=>0);
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        $ispcond='';
        $ispcond=" and isp_uid='".$isp_uid."'";
        $this->db->order_by("id","desc");
        $condarr=array("isp_uid"=>$isp_uid);
        $query = $this->db->get_where(SHTNAS,$condarr);
        // echo $this->db->last_query(); die;
        
	$planid = $this->input->post('planid');
	$data = array();
	$gen = '<ul>';
	$gen .= '<li style="list-style:none"><input type="checkbox" class="allnasids" value="all" /> &nbsp; Check All</li>';
	foreach($query->result() as $nasobj){
	    $state = $this->user_model->getstatename($nasobj->nasstate);
            $city = $this->user_model->getcityname($nasobj->nasstate, $nasobj->nascity);
            $zone = $this->user_model->getzonename($nasobj->naszone);
	    $shortname = $nasobj->shortname;
	    $nastype = ($nasobj->nastype==1)?"Microtik":"Others";
	    $nasname = $nasobj->nasname;
	    $nasstatus = '<img src="'.base_url().'assets/images/loader.svg" width="2%" class="nasimg">';
	    //$data['nas_ips'][] = $nasname;
	    
	    $chk = '';
	    $nasid = $nasobj->id;
	    $query1 = $this->db->query("select srvid from sht_plannasassoc where srvid='" .$planid. "' and nasid='" .$nasid. "'");
	    if ($query1->num_rows() != 0) {
		$chk = ' checked="checked"';
	    }
	    
	    $gen .= '<li style="list-style:none; background-color:#f1f1f1; padding:0px 15px;"><input type="checkbox" name="plannasids" class="nasidsopt" value="'.$nasobj->id.'" '.$chk.'/> &nbsp; '.$shortname.' &nbsp; '.$nastype.' &nbsp; '.$nasname.' &nbsp; '.$state.' &nbsp; '.$city.' &nbsp; '.$zone.' &nbsp; '.$nasstatus.' &nbsp; <input type="hidden" class="nasnameip"  value="'.$nasname.'"></li>';
	}
	$gen .= '</ul>';
	
	$data['nas_listing'] = $gen;
	echo json_encode($data);
    }
    public function nas_status($host){
	exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host)), $res, $rval);
	return $rval;
    }
    public function plannas_configuration(){
	$selnasids = $this->input->post('selnasids');
	$planid = $this->input->post('planid');
	
	$selidsarr = explode(',', $selnasids);
	if(count($selidsarr) > 0){
	    foreach($selidsarr as $nasid){
		$query1 = $this->db->query("select srvid from sht_plannasassoc where srvid='" .$planid. "' and nasid='" .$nasid. "'");
		if ($query1->num_rows() == 0) {
		    $tabledata = array('srvid' => $planid, 'nasid' => $nasid);
		    $this->db->insert('sht_plannasassoc', $tabledata);
		}
	    }
	    
	    $this->db->query("DELETE FROM sht_plannasassoc WHERE srvid='" .$planid. "' and nasid NOT IN (" .$selnasids. ")");
	}
	echo json_encode(true);
    }
    public function burstmode_details(){
	$data = array();
	$srvid = $this->input->post('srvid');
	$query = $this->db->query("SELECT enableburst, dlburstlimit, ulburstlimit, dlburstthreshold, ulburstthreshold, priority, bursttime FROM sht_services WHERE srvid='".$srvid."'");
	if($query->num_rows() > 0){
	    $rowarray = $query->row();
	    $data['enableburst'] = $rowarray->enableburst;
	    $data['dlburstlimit'] = $this->convertTodata($rowarray->dlburstlimit . "KB") .' Kbps';
	    $data['ulburstlimit'] = $this->convertTodata($rowarray->ulburstlimit . "KB") .' Kbps';
	    $data['dlburstthreshold'] = $this->convertTodata($rowarray->dlburstthreshold . "KB") .' Kbps';
	    $data['ulburstthreshold'] = $this->convertTodata($rowarray->ulburstthreshold . "KB") .' Kbps';
	    $data['bursttime'] = $rowarray->bursttime;
	    $data['priority'] = $rowarray->priority;
	}
	echo json_encode($data);
    }
    
}

?>
