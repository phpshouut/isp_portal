<?php
class Setting_model extends CI_Model{

	// function for billing tab start
	public function billing_cycle(){
             $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" where sbc.isp_uid='".$isp_uid."'";
            
		$data = array();
		$query = $this->db->query("select sbc.id, sbc.billing_cycle, sbc.billing_due_date, sbc.late_payment_charges, sbc.charges_unit from sht_billing_cycle as sbc {$ispcond}");
		if($query->num_rows() > 0){
			foreach($query->result() as $row){
				$data[] = $row;
			}
		}
		return $data;
	}
	public function add_billing_cycle(){
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
		$billing_cycle = $this->input->post('billing_cycle');
		
		
			$insert = $this->db->query("insert into sht_billing_cycle(billing_cycle,created_on,isp_uid) VALUES ('$billing_cycle', now(),'$isp_uid')");
		

		return true;
	}

	public function miscellaneous_list(){
		//ger already created
		$planarray = array('Unlimited' => '1', 'Time' => '2', 'FUP' => '3', 'Data' => '4');
		$get_ids = $this->db->query("select plan_type from sht_billing_cycle");
		$miscellaneous_id = array();
		foreach($get_ids->result() as $get_ids1){
			$miscellaneous_id[] = $get_ids1->plan_type;
		}
		$gen = '';
		$result = array_diff($planarray, $miscellaneous_id);
		if(count($result) > 0){
			$gen .= '<option value="" selected>Select Plan Type</option>';
			foreach($result as $plan => $plan_type){
				$gen .= '<option value="'.$plan_type.'" >'.$plan.'</option>';
			}
		}else{
			$gen .= '<option value="">All Plan Type Created</option>';

		}
		return $gen;
	}

	public function update_billing_cycle(){
		$billing_cycle_id = $this->input->post("billing_cycle_id");
		$billing_cycle = $this->input->post('billing_cycle');
		$billing_duedate = $this->input->post('billing_duedate');
		$billing_duedate = isset($billing_duedate) ? $billing_duedate : '0';
		
		$billing_latecharges = $this->input->post('billing_latecharges');
		$billing_latecharges = isset($billing_latecharges) ? $billing_latecharges : '0';
		$billing_chargesunit = $this->input->post('billing_chargesunit');
		$billing_chargesunit = isset($billing_chargesunit) ? $billing_chargesunit : 'percent';
		$this->db->query("update sht_billing_cycle set billing_cycle = '$billing_cycle', billing_due_date = '$billing_duedate', late_payment_charges = '$billing_latecharges', charges_unit = '$billing_chargesunit', updated_on = now() WHERE id = '$billing_cycle_id'");
		//$this->db->query("update sht_billing_cycle set billing_cycle = '$billing_cycle', billing_due_date = '$billing_duedate', updated_on = now() WHERE id = '$billing_cycle_id'");
		return true;
	}
	// function for billing tab start

	// function for location tab start
	public function city_list_dropdown($state_id){
	    $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
	 
	 $ispcond="and (isp_uid='".$isp_uid."' or isp_uid='0')";
		$data = array();
			if($state_id != ''){
				$city_list = $this->db->query("select `city_id`,`city_name`,`city_state_id`,`isp_uid` from sht_cities WHERE city_state_id = '$state_id' $ispcond ORDER BY city_name ");
				if($city_list->num_rows() > 0){
					foreach($city_list->result() as $city_list1){
						$data[] = $city_list1;
					}
				}
			}
			return $data;
	}
	public function state_list_dropdown(){
	  
	    $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        
        $dept_id = $sessiondata['dept_id'];
        $isp_uid = $sessiondata['isp_uid'];
	
        $regiontype = $this->plan_model->dept_region_type();
	//echo $regiontype; die;
        $permicond = '';
        $sczcond = '';
    // echo $regiontype;
        $deptarr=array();
        if ($regiontype == "region") {
           if($superadmin==1)
           {
              $dept_regionQ = $this->db->query("SELECT state as state_id,city,zone FROM sht_isp_admin_region WHERE isp_uid='" . $isp_uid . "' AND status='1'"); 
           }
           else
           {
               $dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='" . $dept_id . "' AND status='1' AND is_deleted='0'");  
           }
          
         //  echo $this->db->last_query();die;
            $total_deptregion = $dept_regionQ->num_rows();
	   
            if ($dept_regionQ->num_rows() > 0) {
                $c = 1;
                foreach ($dept_regionQ->result() as $deptobj) {
                   $statearr[]=$deptobj->state_id;
                }
               
                $statearr=array_unique($statearr);
		
                if(!empty($statearr))
                {
                    $paramid = '"' . implode('", "', $statearr) . '"';
                  $city_list = $this->db->query("select `id`,`state` from sht_states where id IN ({$paramid}) ORDER BY state");
		return $city_list;
                                      
                }
                
            }
           
      
        }
	  
		//$city_list = $this->db->query("select `id`,`state` from sht_states ORDER BY state");
		//return $city_list;
	}
	public function zones_listing($state_id, $city_id, $zones){
		//echo $city_id;
		 $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
	 
	 $ispcond="and (sz.isp_uid='".$isp_uid."' or sz.isp_uid='0')";
		$data = array();
                $cond=$this->city_zone_cond('zone');
		$state_condition = '';
		$city_condition = '';
				if($state_id != ''){
					$state_condition .= " AND sc.city_state_id = '$state_id'";
				}
				if($city_id != ''){
					$city_condition .= "  AND sz.city_id = '$city_id'" ;
				}

		$query = $this->db->query("SELECT sz.zone_name,sz.id, sc.city_name,ss.state FROM sht_zones AS sz INNER JOIN sht_cities AS sc ON (sz.city_id = sc.city_id)
INNER JOIN sht_states AS ss ON (sc.city_state_id = ss.id) WHERE sz.zone_name LIKE '%$zones%' $state_condition $city_condition $cond $ispcond");
		
		
		if($query->num_rows() > 0){
			foreach($query->result() as $row){
				$data[] = $row;
			}
		}
		return $data;
	}
        
        
         public function city_zone_cond($type)
        {
            $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        
        $dept_id = $sessiondata['dept_id'];
        $isp_uid = $sessiondata['isp_uid'];
        $regiontype = $this->plan_model->dept_region_type();
        $permicond = '';
        $sczcond = '';
    // echo $regiontype;
        $deptarr=array();
        if ($regiontype == "region") {
           if($superadmin==1)
           {
              $dept_regionQ = $this->db->query("SELECT state as state_id,city,zone FROM sht_isp_admin_region WHERE isp_uid='" . $isp_uid . "' AND status='1'"); 
           }
           else
           {
               $dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='" . $dept_id . "' AND status='1' AND is_deleted='0'");  
           }
          
         //  echo $this->db->last_query();die;
            $total_deptregion = $dept_regionQ->num_rows();
            if ($dept_regionQ->num_rows() > 0) {
                $c = 1;
                foreach ($dept_regionQ->result() as $deptobj) {
                   $statearr[]=$deptobj->state_id;
                }
               
                $statearr=array_unique($statearr);
                if(!empty($statearr))
                {
                    $paramid = '"' . implode('", "', $statearr) . '"';
                  //   echo "<pre>"; print_R($statearr);die;
                     $queryz=$this->db->query("SELECT GROUP_CONCAT(`city_id`) as cityid FROM sht_cities WHERE `city_state_id` IN ({$paramid})");
                     $zonarr=$queryz->row_array();
                     $cityid=$zonarr['cityid'];
                     $permicond="sz.city_id in ($cityid)";
                     if($type=="zone")
                     {
                           $sczcond .= ' AND (' . $permicond . ')';
                     }
                    else {
                        $sczcond.="AND sc.city_state_id IN ($paramid)";
                    }
                   
                                      
                }
                
            }
           
      //   echo $sczcond;die;
        return $sczcond;
        }
        }
        
         public function zones_search_listing(){
		//echo $city_id;
		 $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
	 
	 $ispcond="and (sz.isp_uid='".$isp_uid."' or sz.isp_uid='0')";
                $state_id =$this->input->post('state');
                $city_id =$this->input->post('city');
                $zone=$this->input->post('search_user');
		$data = array();
		$state_condition = '';
		$city_condition = '';
                $zones='';
                   $cond=$this->city_zone_cond('zone');
				if($state_id != '' ){
                                    if($state_id!="all")
                                    {
                                      $state_condition .= " AND sc.city_state_id = '$state_id'";  
                                    }
                                    else {
                                        $state_condition .= " ";
                                    }
					//$state_condition .= " AND sc.city_state_id = '$state_id'";
				}
				if($city_id != ''){
                                      if($city_id!="all")
                                    {
                                     $city_condition .= "  AND sz.city_id = '$city_id'" ;
                                    }
                                    else {
                                        $city_condition .= " ";
                                    }
					
				}
                                if($zone!='')
                                {
                                    $zones.=$zone;
                                }
                                    
 $gen="";
		$query = $this->db->query("SELECT sz.zone_name,sz.id, sc.city_name,ss.state FROM sht_zones AS sz INNER JOIN sht_cities AS sc ON (sz.city_id = sc.city_id)
INNER JOIN sht_states AS ss ON (sc.city_state_id = ss.id) WHERE sz.zone_name LIKE '%$zones%' $state_condition $city_condition $cond $ispcond");
                
                
              //  echo $this->db->last_query();die;
		if($query->num_rows() > 0){
                   
                    $i=1;
			foreach($query->result() as $sobj){
				$gen .= '
				<tr>
					<td>'.$i.'</td>
					<td colspan="2">'.$sobj->zone_name.'</td>
					<td>'.$sobj->city_name.'</td>
					<td>'.$sobj->state.'</td>
					<td onclick = edit_zone(\''.$sobj->state.'\',\''.$sobj->city_name.'\',\''.$sobj->zone_name.'\',\''.$sobj->id.'\')><a
                                                                                    href="#">Edit</a></td>
					
					
				</tr>
				';
                                $i++;
			}
                        
		}
 $data['search_results'] = $gen;
		return $data;
	}
        public function city_listing($state_id,  $cityname){
		 $sessiondata = $this->session->userdata('isp_session');
	       $isp_uid = $sessiondata['isp_uid'];
	        $ispcond="and (sc.isp_uid='".$isp_uid."' or sc.isp_uid='0')";
		$data = array();
		$state_condition = '';
		$city_condition = '';
                $cond=$this->city_zone_cond('city');
				if($state_id != ''){
					$state_condition .= " AND sc.city_state_id = '$state_id'";
				}
				

		$query = $this->db->query("SELECT sc.`city_id`,sc.`city_name`,sc.`city_state_id`, ss.state,(SELECT COUNT(id) FROM `sht_zones` sz WHERE sc.city_id=sz.city_id) AS zonecount FROM sht_cities sc
INNER JOIN sht_states AS ss ON (sc.city_state_id = ss.id) WHERE sc.city_name LIKE '%$cityname%'  $cond $state_condition $ispcond");
	//	echo $this->db->last_query();
		if($query->num_rows() > 0){
			foreach($query->result() as $row){
				$data[] = $row;
			}
		}
		return $data;
	}
        
        
         public function city_search_listing(){
	  $sessiondata = $this->session->userdata('isp_session');
	       $isp_uid = $sessiondata['isp_uid'];
	        $ispcond="and (sc.isp_uid='".$isp_uid."' or sc.isp_uid='0')";
		$state_id =$this->input->post('state');
                $city=$this->input->post('search_user');
		$data = array();
		$state_condition = '';
		$city_condition = '';
                  $cond=$this->city_zone_cond('city');
				if($state_id != ''){
                                    if($state_id!='all')
                                    {
                                        $state_condition .= " AND sc.city_state_id = '$state_id'";
                                    }
                                    else
                                    {
                                        $state_condition .= "";
                                    }
					
				}
                               $cityname=''; 
                                if($city!='')
                                {
                                  $cityname=$city;  
                                }
				

		$query = $this->db->query("SELECT sc.`city_id`,sc.`city_name`,sc.`city_state_id`, ss.state,(SELECT COUNT(id) FROM `sht_zones` sz WHERE sc.city_id=sz.city_id) AS zonecount FROM sht_cities sc
INNER JOIN sht_states AS ss ON (sc.city_state_id = ss.id) WHERE sc.city_name LIKE '%$cityname%' $state_condition $cond $ispcond");
                $gen="";
		if($query->num_rows() > 0){
			  $i=1;
			foreach($query->result() as $sobj){
				$gen .= '
				<tr>
					<td>'.$i.'</td>
					<td colspan="2">'.$sobj->city_name.'</td>
					<td>'.$sobj->state.'</td>
					<td>'.$sobj->zonecount.'</td>
					<td onclick = edit_city(\''.$sobj->city_name.'\',\''.$sobj->city_state_id.'\',\''.$sobj->city_id.'\')><a
                                                                                    href="#">Edit</a></td>
					
					
				</tr>
				';
                                $i++;
			}
                        
		}
                 $data['search_results'] = $gen;
		return $data;
	}
        
        
        
  
	public function state_city_list($state_id){
		$city_list = $this->db->query("select city_id,city_name from sht_cities WHERE city_state_id = '$state_id' ORDER BY city_name ");
		$gen = '';
		if($city_list->num_rows() > 0){
			$gen .= '<option value="" selected>Select City</option>';
			foreach($city_list->result() as $data){
				$gen .= '<option value="'.$data->city_id.'">'.$data->city_name.'</option>';
			}
		}else{
			$gen .= '<option value="">Select city</option>';

		}
		return $gen;

	}
	public function city_zone_list($city_id){
		$city_list = $this->db->query("select id,zone_name from sht_zones WHERE city_id = '$city_id' ORDER BY zone_name ");
		$gen = '';
		if($city_list->num_rows() > 0){
			$gen .= '<option value="" selected>Select Zone</option>';
			foreach($city_list->result() as $data){
				$gen .= '<option value="'.$data->id.'">'.$data->zone_name.'</option>';
			}
		}else{
			$gen .= '<option value="">Select Zone</option>';

		}
		return $gen;

	}
	public function add_zone(){
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
		$zone_name = $this->input->post('zone_name');
		$city_id = $this->input->post('add_zone_city');
		$zoneq=$this->db->query("select id,zone_name from sht_zones where zone_name='".$zone_name."'
					 and city_id ='".$city_id."'
					 and (isp_uid='".$isp_uid."' or isp_uid='0')");
		 if($zoneq->num_rows()==0)
		 {
                $tabledata=array("zone_name"=>$zone_name,"city_id"=>$city_id,"isp_uid"=>$isp_uid);
		$this->db->insert("sht_zones",$tabledata);
		 }
             //   $this->db->last_query(); die;
		return true;
	}
        
        public function add_zonedata(){
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
		$zone_name = $this->input->post('zone_name');
		$city_id = $this->input->post('add_zone_city');
		 $zoneq=$this->db->query("select id,zone_name from sht_zones where zone_name='".$zone_name."'
					 and city_id ='".$city_id."'
					 and (isp_uid='".$isp_uid."' or isp_uid='0')");
		 if($zoneq->num_rows()>0)
		 {
		    $rowarr=$zoneq->row_array();
		 $data['html']="<option value='".$rowarr['id']."' selected>".$rowarr['zone_name']."</option>";
                $data['city_id']=$city_id;
		 }
		 else
		 {
		  $tabledata=array("zone_name"=>$zone_name,"city_id"=>$city_id,"isp_uid"=>$isp_uid);
		$this->db->insert("sht_zones",$tabledata);
                $id = $this->db->insert_id();
                $data=array();
                $data['html']="<option value='".$id."' selected>".$zone_name."</option>";
                $data['city_id']=$city_id;
		 }
               
             //   $this->db->last_query(); die;
		return $data;
	}
	public function update_zone(){
		$zone_name = $this->input->post('zone_name');
		$zone_id = $this->input->post('zone_id');
                $tabledata=array("zone_name"=>$zone_name);
                $this->db->update('sht_zones', $tabledata, array('id' => $zone_id)); 
		
		return true;
	}
	public function add_city(){
              $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
		$city_name = $this->input->post('city_add');
		$state_id = $this->input->post('add_city_state');
		 $cityq=$this->db->query("select city_id,city_name from sht_cities where city_name='".$city_name."' and city_state_id ='".$state_id."'
					 and (isp_uid='".$isp_uid."' or isp_uid='0')");
                 $tabledata=array("city_name"=>$city_name,"city_state_id"=>$state_id,"isp_uid"=>$isp_uid);
		  if($cityq->num_rows()==0){
		$this->db->insert("sht_cities",$tabledata);
		  }
		return true;
	}
        public function add_citydata(){
               $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
		$city_name = $this->input->post('city_add');
		$state_id = $this->input->post('add_city_state');
		// die;
		 $cityq=$this->db->query("select city_id,city_name from sht_cities where city_name='".$city_name."' and city_state_id ='".$state_id."'
					 and (isp_uid='".$isp_uid."' or isp_uid='0')");
		// echo $cityq->num_rows();die;
		 if($cityq->num_rows()>0)
		 {
		    $rowarr=$cityq->row_array();
		    $data['html']="<option value='".$rowarr['city_id']."' selected>".$rowarr['city_name']."</option>";
                $data['state_id']=$state_id;
		 }
		 else
		 {
		 
                 $tabledata=array("city_name"=>$city_name,"city_state_id"=>$state_id,"isp_uid"=>$isp_uid);
		$this->db->insert("sht_cities",$tabledata);
              
                $id = $this->db->insert_id();
                $data=array();
                $data['html']="<option value='".$id."' selected>".$city_name."</option>";
                $data['state_id']=$state_id;
		 }
		 
		return $data;
	}
        
        public function update_city(){
            $postdata=$this->input->post();
           // echo "<pre>"; print_R($postdata); die;
            $tabledata=array('city_name'=>$postdata['city_add'],'city_state_id'=>$postdata['add_city_state']);
             $this->db->update('sht_cities', $tabledata, array('city_id' => $postdata['cityid'])); 
		
		return true;
	}
        
        public function add_tax()
        {
             $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          
            $postdata=$this->input->post();
            $query=$this->db->query("select * from sht_tax where status=1 and isp_uid='".$isp_uid."'");
            
            if($query->num_rows()>0)
            {
                $tabledata=array("tax"=>$postdata['tax'],"updated_on"=>date("Y-m-d H:i:s"),"status"=>1,"isp_uid"=>$isp_uid);
                  $this->db->update('sht_tax', $tabledata,array("isp_uid"=>$isp_uid));
            }
            else{
                $tabledata=array("tax"=>$postdata['tax'],"created_on"=>date("Y-m-d H:i:s"),"status"=>1,"isp_uid"=>$isp_uid);
                 $this->db->insert('sht_tax', $tabledata); 
            }
            return 1;
         }
         
          public function get_tax()
        {
               $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and  isp_uid='".$isp_uid."'";
              $data=array();
           $query=$this->db->query("select tax from sht_tax where status=1 {$ispcond}");
           if($query->num_rows()>0)
           {
               $rowarr=$query->row_array();
               $data['tax']=$rowarr['tax'];
           }
           else
           {
               $data['tax']=0;
           }
           return $data;
        }
        
        public function add_citrus_data()
        {
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
            $postdata=$this->input->post();
            $query=$this->db->query("select id from sht_merchant_account where status=1 and isp_uid='".$isp_uid."'");
                  $tabledata=array("citrus_secret_key"=>$postdata['citruskey'],"citrus_post_url"=>$postdata['citrusposturl'],
                        "citrus_vanity_url"=>$postdata['citrusvanurl'],"status"=>1,"isp_uid"=>$isp_uid);
            if($query->num_rows()>0)
            {
          
                  $this->db->update('sht_merchant_account', $tabledata,array("isp_uid"=>$isp_uid));
            }
            else{
                
                 $this->db->insert('sht_merchant_account', $tabledata); 
            }
            return 1;
         }
	 
	 
	   public function add_ebs_data()
        {
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
            $postdata=$this->input->post();
            $query=$this->db->query("select id from sht_merchant_account where status=1 and isp_uid='".$isp_uid."'");
                  $tabledata=array("ebs_accountid"=>$postdata['ebs_accountid'],"ebs_secretkey"=>$postdata['ebs_secretkey'],
                        "ebs_salt"=>$postdata['ebs_salt'],"status"=>1,"isp_uid"=>$isp_uid);
            if($query->num_rows()>0)
            {
          
                  $this->db->update('sht_merchant_account', $tabledata,array("isp_uid"=>$isp_uid));
            }
            else{
                
                 $this->db->insert('sht_merchant_account', $tabledata); 
            }
            return 1;
         }
	 
	 
	   public function add_payu_data()
        {
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
            $postdata=$this->input->post();
            $query=$this->db->query("select id from sht_merchant_account where status=1 and isp_uid='".$isp_uid."'");
                  $tabledata=array("payu_user"=>$postdata['payuuser'],"payu_pwd"=>$postdata['payupwd'],
                        "payu_merchantid"=>$postdata['payumerchantid'],"payu_merchantkey"=>$postdata['merchantkey'],
			"payu_merchantsalt"=>$postdata['merchantsalt'],"payu_authheader"=>$postdata['authheader'],"status"=>1,"isp_uid"=>$isp_uid);
            if($query->num_rows()>0)
            {
	       $this->db->update('sht_merchant_account', $tabledata,array("isp_uid"=>$isp_uid));
            }
            else{
               $this->db->insert('sht_merchant_account', $tabledata); 
            }
            return 1;
         }
	 
	   public function add_gupshup_data()
        {
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
            $postdata=$this->input->post();
            $query=$this->db->query("select id from sht_sms_gateway where status=1 and isp_uid='".$isp_uid."'");
                  $tabledata=array("gupshup_user"=>$postdata['gupuser'],"gupshup_pwd"=>$postdata['guppwd'],
                        "status"=>1,"gateway"=>"gupshup","isp_uid"=>$isp_uid);
            if($query->num_rows()>0)
            {
	       $this->db->update('sht_sms_gateway', $tabledata,array("isp_uid"=>$isp_uid));
            }
            else{
               $this->db->insert('sht_sms_gateway', $tabledata); 
            }
            return 1;
         }
	 
	   public function add_rightsms_data()
        {
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
            $postdata=$this->input->post();
            $query=$this->db->query("select id from sht_sms_gateway where status=1 and isp_uid='".$isp_uid."'");
                  $tabledata=array("rightsms_user"=>$postdata['rightuser'],"rightsms_pwd"=>$postdata['rightpwd'],
                       "rightsms_sender"=>$postdata['rightsender'],"gateway"=>"rightsms", "status"=>1,"isp_uid"=>$isp_uid);
            if($query->num_rows()>0)
            {
	       $this->db->update('sht_sms_gateway', $tabledata,array("isp_uid"=>$isp_uid));
	       
            }
            else{
               $this->db->insert('sht_sms_gateway', $tabledata); 
            }
            return 1;
         }
	 
	  public function add_textmsg91_data()
        {
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
            $postdata=$this->input->post();
            $query=$this->db->query("select id from sht_sms_gateway where status=1 and isp_uid='".$isp_uid."'");
                  $tabledata=array("msg91authkey"=>$postdata['smsauthkey'],
                        "status"=>1,"scope"=>$postdata['scope'],"gateway"=>"msg91","isp_uid"=>$isp_uid);
            if($query->num_rows()>0)
            {
	       $this->db->update('sht_sms_gateway', $tabledata,array("isp_uid"=>$isp_uid));
            }
            else{
               $this->db->insert('sht_sms_gateway', $tabledata); 
            }
            return 1;
         }
	 
	  public function add_otpmsg91_data()
        {
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
            $postdata=$this->input->post();
            $query=$this->db->query("select id from sht_otpsms_gateway where status=1 and isp_uid='".$isp_uid."'");
                  $tabledata=array("msg91_authkey"=>$postdata['authapikey'],
                        "status"=>1,"scope"=>$postdata['scope'],"gateway"=>"msg91","isp_uid"=>$isp_uid);
            if($query->num_rows()>0)
            {
	       $this->db->update('sht_otpsms_gateway', $tabledata,array("isp_uid"=>$isp_uid));
            }
            else{
               $this->db->insert('sht_otpsms_gateway', $tabledata); 
            }
            return 1;
         }
	 
	  public function add_email_data()
        {
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
            $postdata=$this->input->post();
            $query=$this->db->query("select id from sht_email_setup where status=1 and isp_uid='".$isp_uid."'");
                  $tabledata=array("host"=>$postdata['smtphost'],"port"=>$postdata['smtport'],
				   "user"=>$postdata['smtpuser'],"password"=>$postdata['smtpwd'],
                        "status"=>1,"isp_uid"=>$isp_uid);
            if($query->num_rows()>0)
            {
	       $this->db->update('sht_email_setup', $tabledata,array("isp_uid"=>$isp_uid));
            }
            else{
               $this->db->insert('sht_email_setup', $tabledata); 
            }
            return 1;
         }
         
           public function add_paytm_data()
        {
                $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
               $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
            $postdata=$this->input->post();
            $query=$this->db->query("select id from sht_merchant_account where status=1 and isp_uid='".$isp_uid."'");
                  $tabledata=array("paytm_merchant_key"=>$postdata['pmrchntkey'],"paytm_merchant_mid"=>$postdata['pmrchntmid'],
                        "paytm_merchant_web"=>$postdata['pmrchntweb'],"paytm_environment"=>"PROD","status"=>1,"isp_uid"=>$isp_uid);
            if($query->num_rows()>0)
            {
          
                  $this->db->update('sht_merchant_account', $tabledata,array("isp_uid"=>$isp_uid));
            }
            else{
                
                 $this->db->insert('sht_merchant_account', $tabledata); 
            }
            return 1;
         }
         
         public function get_merchant_info()
         {
             $data=array();
             $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
             $query=$this->db->query("select `id`,`citrus_secret_key`,`citrus_post_url`,`citrus_vanity_url`,`paytm_merchant_key`,`paytm_merchant_mid`,`paytm_merchant_web`,
`payu_user`,`payu_pwd`,`payu_merchantid`,`payu_merchantkey`,`payu_merchantsalt`,`payu_authheader`,ebs_accountid,ebs_secretkey,ebs_salt,`status`,`isp_uid` from sht_merchant_account where status=1  and isp_uid='".$isp_uid."'");
             
              return $query->result();
                   
         }
	 
	   public function get_smsgateway_info()
         {
             $data=array();
             $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
             $query=$this->db->query("select `id`,`gupshup_user`,`gupshup_pwd`,msg91authkey,rightsms_user,rightsms_pwd,rightsms_sender,scope,bulksmsuser,bulksms_pwd,cloudplace_user,cloudplace_pwd from sht_sms_gateway where status=1  and isp_uid='".$isp_uid."'");
             
              return $query->result();
                   
         }
	 
	    public function get_otpsmsgateway_info()
         {
             $data=array();
             $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
             $query=$this->db->query("select `id`,`msg91_authkey`,scope from sht_otpsms_gateway where status=1  and isp_uid='".$isp_uid."'");
             
              return $query->result();
                   
         }
	 
	 public function get_email_info()
         {
             $data=array();
             $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
             $query=$this->db->query("select `id`,`host`,`port`,`user`,`password` from sht_email_setup where status=1  and isp_uid='".$isp_uid."'");
             
              return $query->result();
                   
         }
         
          public function get_ispdetail_info()
         {
             $data=array();
              $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
             $query=$this->db->query("select id,`isp_name`,`company_name`,`tin_number`,`service_tax_no`,`gst_no`,`address1`,`address2`,`pincode`,`city`,`state`,
`help_number1`,`help_number2`,`help_number3`,`support_email`,`sales_email`,`website`,`original_image`,`small_image`,
`logo_image`,`about_us`,`services`,`geoaddress`,`place_id`,`lat`,`longitude` as `long`,`isp_uid`,logo_signature, cin_number, pancard, sac_number, hsn_number, bank_name, branch_address, ifsc_code, account_holder_name, account_number,invoice_color, invoice_banner1, invoice_banner2, apk_theme_color, apk_bgimage from sht_isp_detail where status=1 and isp_uid='".$isp_uid."'");
             
              return $query->result();
                   
         }
         
         public function get_ispbase_info()
         {
             
             $data=array();
              $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
             $query=$this->db->query("select isp_name,legal_name from sht_isp_admin where  isp_uid='".$isp_uid."'");
             
              return $query->result();
         }
         
      
         
         
            public function add_ispdetail_data($imagedata,$signimagedata)
        {
                $postdata=$this->input->post();
                $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
               
                if(isset($imagedata['original']) && $imagedata['original']!='')
                {
                 
                   $tabledata=array("isp_name"=>$postdata['ispname'],"company_name"=>$postdata['company_name'],"tin_number"=>$postdata['tin_number'],
                        "service_tax_no"=>$postdata['service_tax_number'],"gst_no"=>$postdata['gstnumber'],"address1"=>$postdata['address_1'],"address2"=>$postdata['address_2'],
                       "pincode"=>$postdata['pincode'],"city"=>$postdata['city'],"state"=>$postdata['state'],
                       "help_number1"=>$postdata['help_number1'],"help_number2"=>$postdata['help_number2'],"help_number3"=>$postdata['help_number3'],
                        "support_email"=>$postdata['supp_email'],"sales_email"=>$postdata['sale_email'],"website"=>$postdata['web'],
                        "original_image"=>$imagedata['original'],"small_image"=>$imagedata['small'],"logo_image"=>$imagedata['logo'],
                       "created_on"=>date("Y-m-d H:i:s"),"geoaddress"=>$postdata['geoaddr'],"place_id"=>$postdata['placeid']
                           ,"lat"=>$postdata['lat'],"longitude"=>$postdata['long'],"status"=>1,"isp_uid"=>$isp_uid, "cin_number" => $postdata['cin_number'], "pancard" => $postdata['pancard'], "hsn_number" => $postdata['hsn_number'],"sac_number" => $postdata['sac_number']);
                }
                else {
                 
                $tabledata=array("isp_name"=>$postdata['ispname'],"company_name"=>$postdata['company_name'],"tin_number"=>$postdata['tin_number'],
                        "service_tax_no"=>$postdata['service_tax_number'],"gst_no"=>$postdata['gstnumber'],"address1"=>$postdata['address_1'],"address2"=>$postdata['address_2'],
                       "pincode"=>$postdata['pincode'],"city"=>$postdata['city'],"state"=>$postdata['state'],
                       "help_number1"=>$postdata['help_number1'],"help_number2"=>$postdata['help_number2'],"help_number3"=>$postdata['help_number3'],
                        "support_email"=>$postdata['supp_email'],"sales_email"=>$postdata['sale_email'],"website"=>$postdata['web'],
                       "update_on"=>date("Y-m-d H:i:s"),"created_on"=>date("Y-m-d H:i:s"),"geoaddress"=>$postdata['geoaddr'],"place_id"=>$postdata['placeid']
                           ,"lat"=>$postdata['lat'],"longitude"=>$postdata['long'],"status"=>1,"isp_uid"=>$isp_uid, "cin_number" => $postdata['cin_number'], "pancard" => $postdata['pancard'], "hsn_number" => $postdata['hsn_number'],"sac_number" => $postdata['sac_number']);

                }
		 if(isset($signimagedata['original']) && $signimagedata['original']!='')
                {
		    $tabledata1=array("original_signature"=>$signimagedata['original'],"logo_signature"=>$signimagedata['logo']);
		    $tabledata=array_merge($tabledata,$tabledata1);
		}
            $postdata=$this->input->post();
            $query=$this->db->query("select id from sht_isp_detail where status=1 and isp_uid='".$isp_uid."'");
                 
            if($query->num_rows()>0)
            {
          
                  $this->db->update('sht_isp_detail', $tabledata,array("isp_uid"=>$isp_uid));
            }
            else{
                
                 $this->db->insert('sht_isp_detail', $tabledata); 
            }
           
            return 1;
         }
         
         public function add_aboutus()
         {
              $postdata=$this->input->post();
              $sessiondata = $this->session->userdata('isp_session');
	       $isp_uid = $sessiondata['isp_uid'];
               $tabledata=array("about_us"=>$postdata['about_us']);
                  $this->db->update('sht_isp_detail', $tabledata,array("isp_uid"=>$isp_uid));
                    return 1;
         }
         
          public function add_services()
         {
               $postdata=$this->input->post();
			     $sessiondata = $this->session->userdata('isp_session');
	       $isp_uid = $sessiondata['isp_uid'];
               $tabledata=array("services"=>$postdata['services']);
                  $this->db->update('sht_isp_detail', $tabledata,array("isp_uid"=>$isp_uid));
                    return 1;
         }
	 
	 
	 public function isp_license_data(){
		$wallet_amt = 0; $passbook_amt = 0;
		$walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='".ISPID."' AND is_paid='1'");
		if($walletQ->num_rows() > 0){
		    $wallet_amt = $walletQ->row()->wallet_amt;
		}
		
		$passbookQ = $this->db->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost FROM sht_isp_passbook WHERE isp_uid='".ISPID."'");
		if($passbookQ->num_rows() > 0){
		    $passrowdata = $passbookQ->row();
		    $passbook_amt = $passrowdata->activeusers_cost;
		}
		$balanceamt = $wallet_amt - $passbook_amt;
		if($balanceamt < 0){
			return 0;
		}else{
			return 1;
		}
	}
	
	public function get_deletezone_data()
	{
	    $postdata=$this->input->post();
              $sessiondata = $this->session->userdata('isp_session');
	       $isp_uid = $sessiondata['isp_uid'];
	  $dataarr=array();
	  $postdata=$this->input->post();
	  $zone=$postdata['zone_id'];
	  $cityq=$this->db->query("select city_id,zone_name from sht_zones WHERE id = '".$zone."'");
	  $cityarr=$cityq->row_array();
	  $city_id=$cityarr['city_id'];
	  $zonename=$cityarr['zone_name'];
	 // echo "===>>>".$city_id; die;
	  $zoneused=0;
	  $query=$this->db->query("select id from sht_dept_region where zone_id='".$zone."'");
	  if($query->num_rows()>0)
	  {
	    $zoneused=1;   
	  }
	   $query1=$this->db->query("select id from sht_plan_region where zone_id='".$zone."'");
	  if($query1->num_rows()>0)
	  {
	    $zoneused=1;   
	  }
	  
	  $query2=$this->db->query("select id from sht_topup_region where zone_id='".$zone."'");
	  if($query2->num_rows()>0)
	  {
	    $zoneused=1;   
	  }
	  $query3=$this->db->query("select id from nas where naszone='".$zone."'");
	  if($query3->num_rows()>0)
	  {
	    $zoneused=1;   
	  }
	   $query4=$this->db->query("select id from sht_users where zone='".$zone."'");
	  if($query4->num_rows()>0)
	  {
	    $zoneused=1;   
	  }
	  
	  if($zoneused==1)
	  {
	        $city_list = $this->db->query("select id,zone_name from sht_zones WHERE city_id = '$city_id' and (isp_uid='".$isp_uid."' or
					      isp_uid='0') ORDER BY zone_name ");
		$gen = '';
		if($city_list->num_rows() > 0){
			$gen .= '<option value="" selected>Select Zone</option>';
			foreach($city_list->result() as $data){
			 if($data->id!=$zone)
			 {
				$gen .= '<option value="'.$data->id.'">'.$data->zone_name.'</option>';
			 }
			}
		}else{
			$gen .= '<option value="">Select Zone</option>';

		}
		$dataarr['is_deletable']=0;
		$dataarr['html']=$gen;
		$dataarr['zonename']=$zonename;
		$dataarr['zone_id']=$zone;
		
		
	  }
	  else
	  {
	       $dataarr['is_deletable']=1;
		$dataarr['html']='';
		$dataarr['zonename']=$zonename;
		$dataarr['zone_id']=$zone;
	  }
	  return $dataarr;
	  
     }
     
     
     public function delete_zone()
     {
	  $postdata=$this->input->post();
	  if(isset($postdata['newzone_id']) && $postdata['newzone_id']!='')
	  {
	       $tabledata=array("zone_id"=>$postdata['newzone_id']);
	       $this->db->update('sht_dept_region',$tabledata,array("zone_id"=>$postdata['zoneid']));
	       $this->db->update('sht_plan_region',$tabledata,array("zone_id"=>$postdata['zoneid']));
	       $this->db->update('sht_topup_region',$tabledata,array("zone_id"=>$postdata['zoneid']));
	       $this->db->update('nas',array("naszone"=>$postdata['newzone_id']),array("naszone"=>$postdata['zoneid']));
	       $this->db->update('sht_users',array("zone"=>$postdata['newzone_id']),array("zone"=>$postdata['zoneid']));
	     
	  $this->db->delete('sht_zones',array("id"=>$postdata['zoneid'])); 
	       
	       
	  }
	  else
	  {
	        $this->db->delete('sht_zones',array("id"=>$postdata['zoneid'])); 
	  }
	  
	  return true;
	  
	  
     }
        
	
     public function add_ispbanking(){
          $data=array();
          $sessiondata = $this->session->userdata('isp_session');
	  $isp_uid = $sessiondata['isp_uid'];
	  
	  $bankdetails = array(
	    'bank_name' => $this->input->post('isp_bankname'),
	    'branch_address' => $this->input->post('isp_bankaddress'),
	    'ifsc_code' => $this->input->post('isp_bankifsc_code'),
	    'account_holder_name' => $this->input->post('isp_bankholder_name'),
	    'account_number' => $this->input->post('isp_bankaccount_number')
	  );
	  
	  $this->db->update("sht_isp_detail", $bankdetails, array('isp_uid' => $isp_uid));
	  echo json_encode(true);
     }	
	// function for location tab end
	
     public function add_bulksmsindia_data(){
          $sessiondata = $this->session->userdata('isp_session');
	  $isp_uid = $sessiondata['isp_uid'];
          $postdata=$this->input->post();
          $query=$this->db->query("select id from sht_sms_gateway where status=1 and isp_uid='".$isp_uid."'");
          $tabledata=array("bulksmsuser"=>$postdata['bulksmsuser'],"bulksms_pwd"=>$postdata['bulksmspwd'],"status"=>1,"gateway"=>"bulksmsindia","isp_uid"=>$isp_uid);
            if($query->num_rows()>0)
            {
	       $this->db->update('sht_sms_gateway', $tabledata,array("isp_uid"=>$isp_uid));
            }
            else{
               $this->db->insert('sht_sms_gateway', $tabledata); 
            }
            return 1;
     }
     
     public function add_cloudplace_data(){
          $sessiondata = $this->session->userdata('isp_session');
	  $isp_uid = $sessiondata['isp_uid'];
          $postdata=$this->input->post();
          $query=$this->db->query("select id from sht_sms_gateway where status=1 and isp_uid='".$isp_uid."'");
          $tabledata=array("cloudplace_user"=>$postdata['cloudplaceuser'],"cloudplace_pwd"=>$postdata['cloudplacepwd'],"scope"=>$postdata['cloudplacesender'],"status"=>1,"gateway"=>"cloudplace","isp_uid"=>$isp_uid);
            if($query->num_rows()>0)
            {
	       $this->db->update('sht_sms_gateway', $tabledata,array("isp_uid"=>$isp_uid));
            }
            else{
               $this->db->insert('sht_sms_gateway', $tabledata); 
            }
            return 1;
     }
     
     public function getExtension($str){
	  $i = strrpos($str,".");
	  if (!$i) { return ""; }
	  $l = strlen($str) - $i;
	  $ext = substr($str,$i+1,$l);
	  return $ext;
     }

     public function customizeinvoice(){
	  $data = array();
	  $session_data = $this->session->userdata('isp_session');
	  $isp_uid = $session_data['isp_uid'];
	  $target_path = "./assets/media/";
	  $directory_name = 'invoicebanner';
	  if (!file_exists($target_path.$directory_name)) {
		  mkdir($target_path.$directory_name, 0777, true);
	  }
	  
	  $data['invoice_color'] = $this->input->post('billinvoicecolor');
	  $ispdetQ = $this->db->query("SELECT invoice_color, invoice_banner1, invoice_banner2 FROM sht_isp_detail WHERE isp_uid='".$isp_uid."'");
	  $ispdetrowdata = $ispdetQ->row();
	  $invoice_banner1 = $ispdetrowdata->invoice_banner1;
	  $invoice_banner2 = $ispdetrowdata->invoice_banner2;
	  
	  if(isset($_POST['billbanner1_values'])){
		  @unlink($target_path.$directory_name.'/'.$invoice_banner1);
		  $billbanner1_path = json_decode($_POST['billbanner1_values']);
		  $image_name =  $billbanner1_path->name;
		  $image_src = $billbanner1_path->data;
		  $base_url = $target_path.$directory_name.'/';
		  $original = "";
		  $small = "";
		  $logo = "";
		  
		  $file_ext = $this->getExtension($image_name);
		  $explode = explode(',',$image_src);
		  $image_src = $explode['1'];
		  $image_src = str_replace(' ', '+', $image_src);
		  $data_img = base64_decode($image_src);
		  $filename = uniqid() . '.'.$file_ext;
		  $file = $base_url . $filename;
		  $success = file_put_contents($file, $data_img);
		  $fname1=date("Ymdhisv").rand().".".$file_ext;
		  rename($base_url.$filename,$base_url.$fname1);
		  $data['invoice_banner1'] = $fname1;
	  }
	  if(isset($_POST['billbanner2_values'])){
		  @unlink($target_path.$directory_name.'/'.$invoice_banner2);
		  $billbanner1_path = json_decode($_POST['billbanner2_values']);
		  $image_name =  $billbanner1_path->name;
		  $image_src = $billbanner1_path->data;
		  $base_url = $target_path.$directory_name.'/';
		  $original = "";
		  $small = "";
		  $logo = "";
		  
		  $file_ext = $this->getExtension($image_name);
		  $explode = explode(',',$image_src);
		  $image_src = $explode['1'];
		  $image_src = str_replace(' ', '+', $image_src);
		  $data_img = base64_decode($image_src);
		  $filename = uniqid() . '.'.$file_ext;
		  $file = $base_url . $filename;
		  $success = file_put_contents($file, $data_img);
		  $fname2=date("Ymdhisv").rand().".".$file_ext;
		  rename($base_url.$filename,$base_url.$fname2);
		  $data['invoice_banner2'] = $fname2;
	  }
	  
	  
	  $this->db->update('sht_isp_detail', $data, array('isp_uid' => $isp_uid));
	  return 1;
     }
     
     public function apksetup(){
	  $data = array();
	  $session_data = $this->session->userdata('isp_session');
	  $isp_uid = $session_data['isp_uid'];
	  $target_path = "./assets/media/";
	  $directory_name = 'apksetup';
	  if (!file_exists($target_path.$directory_name)) {
	       mkdir($target_path.$directory_name, 0777, true);
	  }
	  
	  $data['apk_theme_color'] = $this->input->post('apk_theme_color');
	  $ispdetQ = $this->db->query("SELECT apk_theme_color, apk_bgimage FROM sht_isp_detail WHERE isp_uid='".$isp_uid."'");
	  $ispdetrowdata = $ispdetQ->row();
	  $apk_bgimage = $ispdetrowdata->apk_bgimage;
	  
	  if(isset($_POST['apk_bgimage_values'])){
		  @unlink($target_path.$directory_name.'/'.$apk_bgimage);
		  $billbanner1_path = json_decode($_POST['apk_bgimage_values']);
		  $image_name =  $billbanner1_path->name;
		  $image_src = $billbanner1_path->data;
		  $base_url = $target_path.$directory_name.'/';
		  $original = "";
		  $small = "";
		  $logo = "";
		  
		  $file_ext = $this->getExtension($image_name);
		  $explode = explode(',',$image_src);
		  $image_src = $explode['1'];
		  $image_src = str_replace(' ', '+', $image_src);
		  $data_img = base64_decode($image_src);
		  $filename = uniqid() . '.'.$file_ext;
		  $file = $base_url . $filename;
		  $success = file_put_contents($file, $data_img);
		  $fname1=date("Ymdhisv").rand().".".$file_ext;
		  rename($base_url.$filename,$base_url.$fname1);
		  $data['apk_bgimage'] = $fname1;
	  }
	  
	  $this->db->update('sht_isp_detail', $data, array('isp_uid' => $isp_uid));
	  return 1;
     }
}


?>
