<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Team extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('team_model');
        $this->load->model('user_model');
         $this->load->model('setting_model');
         $this->load->model('mgmt_model');
         $this->load->model('plan_model');
	 $this->load->model('Emailer_model', 'emailer_model');
	 $this->load->model('Notifications_model', 'notify_model');
          $this->load->library('form_validation');
          $this->load->library('email');
           $this->load->helper('string');
           if(!$this->session->has_userdata('isp_session')){
			redirect(base_url().'login'); exit;
		}
		$isp_license_data = $this->team_model->isp_license_data(); 
		if($isp_license_data != 1){
			redirect(base_url().'manageLicense'); exit;
		}
                $this->user_model->check_ispmodules();
    }
    
    public function index()
    {
            if(!$this->plan_model->is_permission(DEPARTMENT,'HIDE'))
        {
            redirect(base_url());
        }
            $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
       $data['teamlist']=$this->team_model->list_team();
         $data['deptlist']=$this->mgmt_model->list_department();
	 $data['franchlist']=$this->mgmt_model->list_franchise();
       $this->load->view('mgmt/Dept_view',$data);
    }
    
    public function add_team()
    {
           if(!$this->plan_model->is_permission(DEPARTMENT,'HIDE'))
        {
            redirect(base_url());
        }
        if(!$this->plan_model->is_permission(DEPARTMENT,'ADD')){
            redirect(base_url()."mgmt");
        }
      $data['dept']=$this->team_model->get_departments();
        $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
        //  echo "<pre>"; print_R($data);die;
        $this->load->view("mgmt/Add_team",$data);
        
    }
    
    public function edit_team($id)
    {
           if(!$this->plan_model->is_permission(DEPARTMENT,'HIDE'))
        {
            redirect(base_url());
        }
         if(!$this->plan_model->is_permission(DEPARTMENT,'EDIT')){
                 if($this->plan_model->is_permission(DEPARTMENT,'RO'))
              {
                  $data['ro']=1;
              }
              else
              {
                  redirect(base_url()."mgmt");
              }
        }
	
          $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
        $data['userdata']=$this->team_model->get_team_user_data($id);
        $data['dept']=$this->team_model->get_departments();
	//echo $data['userdata']['isp_uid'];
	//echo "<pre>"; print_R($data);
     
        if(!$this->check_dept_ispasscoc($data['userdata']['isp_uid']))
        {
	    //echo "xxxx";die;
            redirect(base_url()."mgmt");
        }
        $this->load->view("mgmt/Edit_team",$data);
        
        
    }
    
       public function check_dept_ispasscoc($deptisp)
    {
        $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
        // echo $isp_uid.":::".$deptisp;
         if($isp_uid==$deptisp)
         {
            return true;
         }
         else
         {
            return false;
         }
    }
    
    public function add_team_data()
    {
        $postdata=$this->input->post();
          $data['dept']=$this->team_model->get_departments();
          $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
    // echo "<pre>"; print_R($postdata); die;
         if(!isset($postdata['user_id'])){ 
        $this->form_validation->set_rules('username', 'User Name', 'required|valid_email|callback_username_check');
        } 
        $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
          $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
        
        
          $this->form_validation->set_rules('phone', 'phone no', 'trim|required|min_length[10]|max_length[10]|numeric');
       // $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
         if ($this->form_validation->run() == FALSE) {
             $this->load->view("mgmt/Add_team",$data);
        } else {
            $userid= $this->team_model->add_team_member();
           
            redirect(base_url()."team");
         
           // die;
        }
    }
    
      public function username_check($str) {
        $userexist = $this->team_model->check_user_exist($str);
        if ($userexist == 0) {

 
            $this->form_validation->set_message('username_check', 'This {field}  already exist');
            return FALSE;
        } else {
           
            return TRUE;
        }
    }
    
     public function delete_user()
    {
        $id=$this->team_model->delete_user();
         echo $id;
    }
    
    public function update_userpwd()
    {
	 $data=$this->team_model->update_userpwd();
         echo json_encode($data);
    }
    
    
    public function my_account()
    {
         $session_data = $this->session->userdata('isp_session');
        $id=$session_data['userid'];
         $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
      $data['userdata']=$this->team_model->get_user_data($id);
    // echo "<pre>"; print_R($data); die;
      $this->load->view("mgmt/My_Account",$data);
    }
    
    
     public function edit_account_data()
    {
        $postdata=$this->input->post();
       
          $data['dept']=$this->team_model->get_departments();
    
         if(isset($postdata['user_id'])){ 
        
        $this->form_validation->set_rules('password', 'password', 'callback_password_check');
        } 
       
        $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('phone', 'phone no', 'trim|required|min_length[10]|max_length[10]|numeric');
        if ($this->form_validation->run() == FALSE) {
            $session_data = $this->session->userdata('isp_session');
        $id=$session_data['userid'];
      $data['userdata']=$this->team_model->get_user_data($id);
       $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
             $this->load->view("mgmt/My_Account",$data);
        } else {
             // echo "<pre>"; print_R($postdata); die;
            $userid= $this->team_model->edit_team_member();
            if($userid)
            {
                 //echo "<pre>"; print_R($postdata); die;
                redirect(base_url()."team/My_Account");
            }
           // die;
        }
    }
    
    
         public function password_check($str) {
             $postdata=$this->input->post();
           //  echo "ssss<pre>"; print_R($postdata);die;
            // echo 'ssss';die;
        $userexist = $this->team_model->check_password_correct($str);
        if ($userexist == 0) {

 
            $this->form_validation->set_message('password_check', 'Old password is Incorrect');
            return FALSE;
        } else {
           
            return TRUE;
        }
    }
    
   
  
    
  
    
    
    
}

?>