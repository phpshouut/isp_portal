<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mgmt extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('mgmt_model');
        $this->load->model('user_model');
        $this->load->model('plan_model');
        $this->load->model('team_model');
        $this->load->model('setting_model');
        if(!$this->session->has_userdata('isp_session')){
	    redirect(base_url().'login'); exit;
	}
	else{
	    $sessiondata = $this->session->userdata('isp_session');
	    // echo "<pre>"; print_R($sessiondata); die;
	    if(!isset($sessiondata['is_franchise'])){
		redirect(base_url().'login'); exit;
	    }
	}

	$isp_license_data = $this->mgmt_model->isp_license_data(); 
	if($isp_license_data != 1){
	    redirect(base_url().'manageLicense'); exit;
	}
        $this->user_model->check_ispmodules();
    }
    
    public function index(){
        if(!$this->plan_model->is_permission(DEPARTMENT,'HIDE'))
        {
            redirect(base_url());
        }
        $arr=$this->setting_model->get_ispdetail_info();
        $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
	$data['deptlist']=$this->mgmt_model->list_department();
        $data['teamlist']=$this->team_model->list_team();
	$data['franchlist']=$this->mgmt_model->list_franchise();
	$this->load->view('mgmt/Dept_view',$data);
    }
    
    public function add_dept(){
        if(!$this->plan_model->is_permission(DEPARTMENT,'HIDE'))
        {
            redirect(base_url());
        }
        if(!$this->plan_model->is_permission(DEPARTMENT,'ADD')){
            redirect(base_url()."mgmt");
        } 
        $arr=$this->setting_model->get_ispdetail_info();
        $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
	$data['dashboard_tabdata']=$this->mgmt_model->get_tab_data('DASHBOARD');
        $data['user_tabdata']=$this->mgmt_model->get_tab_data('USERMANAGEMENT');
        $data['left_tabdata']=$this->mgmt_model->get_tab_data('LEFT');
        $data['setting_tabdata']=$this->mgmt_model->get_tab_data('SETTINGS');
        //  echo "<pre>"; print_R($data);die;
        $this->load->view("mgmt/Add_dept",$data);
        
    }
    
    public function add_franchise(){
		
	if(!$this->plan_model->is_permission(DEPARTMENT,'HIDE'))
        {
            redirect(base_url());
        }
        if(!$this->plan_model->is_permission(DEPARTMENT,'ADD')){
            redirect(base_url()."mgmt");
        }
		  $arr=$this->setting_model->get_ispdetail_info();
        $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
	$this->load->view("mgmt/Add_franchise",$data);
    }
    
    public function add_franchise_data(){
	$postdata=$this->input->post();
        //echo "<pre>"; print_R($postdata); die;
        
	$franchid= $this->mgmt_model->add_franchise_data();
	echo json_encode($franchid);
    }
	
    
    public function add_perm_data(){
        $postdata=$this->input->post();
        //echo "<pre>"; print_R($postdata); die;
        
	$deptid= $this->mgmt_model->add_permission();
	echo json_encode($deptid);
    }
    
    
    
    
    public function edit_dept($id) {
        // echo $id; die;
        if(!$this->plan_model->is_permission(DEPARTMENT,'HIDE'))
        {
            redirect(base_url());
        }
        if(!$this->plan_model->is_permission(DEPARTMENT,'EDIT')){
            if($this->plan_model->is_permission(DEPARTMENT,'RO'))
            {
                $data['ro']=1;
            }
            else
            {
                redirect(base_url()."mgmt");
            }
           // redirect(base_url()."mgmt");
        } 
        $datarr = array();
        $datarr = $this->mgmt_model->dept_data($id);
        if(!$this->check_dept_ispasscoc($datarr['isp_uid']))
        { 
            redirect(base_url()."mgmt");
        }
       // echo "<pre>"; print_R($datarr);die;
        $arr=$this->setting_model->get_ispdetail_info();
        $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
        $data['dept'] = $datarr;
	$data['dashboard_tabdata']=$this->mgmt_model->get_tab_data('DASHBOARD');
        $data['user_tabdata']=$this->mgmt_model->get_tab_data('USERMANAGEMENT');
        $data['left_tabdata']=$this->mgmt_model->get_tab_data('LEFT');
        $data['setting_tabdata']=$this->mgmt_model->get_tab_data('SETTINGS');
        $data['dept_region'] = $this->mgmt_model->dept_region_data($id);
        $data['dept_permission'] = $this->mgmt_model->dept_permission_data($id);
       //echo "<pre>"; print_R($data); die;
        $this->load->view('mgmt/Edit_dept', $data);
    }
    
    public function edit_franchise($id) {
        // echo $id; die;
        if(!$this->plan_model->is_permission(DEPARTMENT,'HIDE'))
        {
            redirect(base_url());
        }
        if(!$this->plan_model->is_permission(DEPARTMENT,'EDIT')){
            if($this->plan_model->is_permission(DEPARTMENT,'RO'))
            {
                $data['ro']=1;
            }
            else
            {
                redirect(base_url()."mgmt");
            }
           // redirect(base_url()."mgmt");
        } 
        $datarr = array();
        $datarr = $this->mgmt_model->franchise_data($id);
   
        	  $arr=$this->setting_model->get_ispdetail_info();
        $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
	
        $data['franchise'] = $datarr;
	$data['franchise_region'] = $this->mgmt_model->franchise_region_data($datarr['isp_uid']);
      
       //echo "<pre>"; print_R($data); die;
        $this->load->view('mgmt/Edit_franchise', $data);
    }
    
    
    public function check_dept_ispasscoc($deptisp){
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
       //  echo $isp_uid.":::".$srvisp;
        if($isp_uid==$deptisp)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function delete_region_dept(){
        $id=$this->mgmt_model->delete_region_dept();
        echo $id;
    }
    
    public function delete_region_franchise(){
        $id=$this->mgmt_model->delete_region_franchise();
        echo $id;
    }
    
    public function delete_dept(){
        $id=$this->mgmt_model->delete_dept();
         echo $id;
    }
    
    public function delete_franchise(){
        $id=$this->mgmt_model->delete_franchise();
         echo $id;
    }
    
    
    
    public function check_dept_deletable(){
        $deletable = $this->mgmt_model->check_dept_deletable();
        echo $deletable;
    }
    
    
    
}

?>