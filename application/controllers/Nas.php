<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nas extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->model('nas_model');
                $this->load->model('user_model');
                  $this->load->model('plan_model');
                  $this->load->model('setting_model');
		if(!$this->session->has_userdata('isp_session')){
			redirect(base_url().'login'); exit;
		}
		$isp_license_data = $this->nas_model->isp_license_data(); 
		if($isp_license_data != 1){
			redirect(base_url().'manageLicense'); exit;
		}
                $this->user_model->check_ispmodules();
    }
    
    
    public function nas_curlhit($url,$postdata,$ip)
    {
        $postData = array(
            'mobile' => $postdata['mobileno'],
            'clientip' => $ip,
            'shortname' => $postdata['nas_name'],
            'secret'=>$postdata['nas_secret']
            
        );
        $url=$url;
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
    }
    
    public function index()
    {
        if(!$this->plan_model->is_permission(NAS,'HIDE'))
        {
            redirect(base_url());
        }
         $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
         $data['nas_listing']=$this->nas_model->listing_nas();
		 $data['pool_listing']=$this->nas_model->pool_listing();
        
        $this->load->view('nas/nas_view',$data);
    }
    
    public function add_nas_data()
    {
    $postdata=$this->input->post();
      if(isset($postdata['ip']) && $postdata['ip']!='' )
        {
           // $url="http://103.206.180.82/clientsconf/index.php";
             $nasip=$postdata['ip'];
           //  $this->nas_curlhit($url,$postdata,$nasip);
             //$url="http://103.206.180.82/clientsconf/script.php";
                $url=NASCONF;

               $this->nas_curlhit($url,$postdata,$nasip);
        }
      
        
 
       
        $id=$this->nas_model->add_nas_data();
        echo json_encode($id);
    }
    
    public function add_pool_data()
    {
         $id=$this->nas_model->add_pool_data();
    }
    
    public function viewall_search_results(){
		$this->nas_model->viewall_search_results();
	}
        
      public function delete_nas()
      {
          $id=$this->nas_model->delete_nas();
          echo json_encode($id);
      }
      
      public function edit_nas()
      {
          if(!$this->plan_model->is_permission(NAS,'HIDE'))
        {
            redirect(base_url());
        }
          $ro=0;
           if(!$this->plan_model->is_permission(NAS,'EDIT')){
                if($this->plan_model->is_permission(NAS,'RO'))
              {
            
                  $ro=1;
              }
              else
              {
                 redirect(base_url()."plan/topup");
              }
       }
       $postdata=$this->input->post();
       if(isset($postdata['ip']) && $postdata['ip']!='' )
        {
          
           // $url="http://103.206.180.82/clientsconf/index.php";
             $nasip=$postdata['ip'];
           //  $this->nas_curlhit($url,$postdata,$nasip);
             $url="http://103.206.180.82/clientsconf/script.php";
               $this->nas_curlhit($url,$postdata,$nasip);
        }
           $data=$this->nas_model->edit_nas($ro); 
           echo $data;
      }
      
        public function getzonelist() {
        $cityid = $this->input->post('cityid');
        echo $this->nas_model->zone_list($cityid);
    }
    
    public function checknas_exist()
    {
        $result=$this->nas_model->checknas_exist();
        echo json_encode($result);
    }
    
     public function checknasip_exist()
    {
       $result= $this->nas_model->checknasip_exist();
        echo json_encode($result);
    }
    
    public function status_nasip()
    {
        $postdata=$this->input->post();
        $up = $this->nas_model->ping($postdata['nasip']);
        echo $up;
    }
    
    /**********************
     *	IIL CONFIGURATION
     ***********************/
    public function getnasdetails(){
	$this->nas_model->getnasdetails();
    }
    
    public function start_configuration(){
	$this->nas_model->start_configuration();
    }
    
    public function interface_netmask_configuration(){
	$this->nas_model->interface_netmask_configuration();
    }
    public function add_ill_iprange(){
	$this->nas_model->add_ill_iprange();
    }
    public function test_ill_config(){
	$this->nas_model->test_ill_config();
    }
    public function getill_iprange(){
	$this->nas_model->getill_iprange();
    }
  
    
}

?>
