<?php
defined('BASEPATH') OR exit('No direct script access allowed');

error_reporting(0);
class Setting extends CI_Controller {
	
	public function __construct(){
		parent :: __construct();
		$this->load->library('form_validation');
		$this->load->model('setting_model');
                $this->load->model('user_model');
                 $this->load->model('plan_model');
                $paramsthree=array("accessKey"=>awsAccessKey,"secretKey"=>awsSecretKey);
                 $this->load->library('s3',$paramsthree);
		if(!$this->session->has_userdata('isp_session')){
			redirect(base_url().'login'); exit;
		}
		$isp_license_data = $this->setting_model->isp_license_data(); 
		if($isp_license_data != 1){
			redirect(base_url().'manageLicense'); exit;
		}
               $this->user_model->check_ispmodules();
	}
// function for billing tag start
	public function index(){
            if(!$this->plan_model->is_permission(BILLINGCYCLE,'HIDE'))
        {
            redirect(base_url());
        }
         $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
                $data['tax']=$this->setting_model->get_tax();
                 $data['minfo']=$this->setting_model->get_merchant_info();
		 $data['sinfo']=$this->setting_model->get_smsgateway_info();
		 $data['otpsinfo']=$this->setting_model->get_otpsmsgateway_info();
		  $data['einfo']=$this->setting_model->get_email_info();
		$data['billing_cycle'] = $this->setting_model->billing_cycle();
		//echo "<pre>"; print_R($data); die;
		$this->load->view('setting/Billing',$data);
	}
	
	public function smssetup()
	{
		 if(!$this->plan_model->is_permission(BILLINGCYCLE,'HIDE'))
        {
            redirect(base_url());
        }
         $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
               
                 
		 $data['sinfo']=$this->setting_model->get_smsgateway_info();
		 $data['otpsinfo']=$this->setting_model->get_otpsmsgateway_info();
		
		//echo "<pre>"; print_R($data); die;
		$this->load->view('setting/Sms_gateway',$data);
	
	}
	
	public function add_billing_cycle(){
                if(!$this->plan_model->is_permission(BILLINGCYCLE,'HIDE'))
        {
            redirect(base_url());
        }
		$this->setting_model->add_billing_cycle();
		redirect(base_url().'setting');
	}
	public function miscellaneous_list(){
		$miscellaneous_list = $this->setting_model->miscellaneous_list();
		echo $miscellaneous_list;
	}
	public function update_billing_cycle(){
		$this->setting_model->update_billing_cycle();
		redirect(base_url().'setting');
	}
// function for billing tag end

	// function for location tab start
	public function locations(){
		$state_id = '';
		if(isset($_POST['state']) && $_POST['state'] != ''){
			$state_id = $_POST['state'];
		}
		$city_id = '';
		if(isset($_POST['city']) && $_POST['city'] != ''){
			$city_id = $_POST['city'];
		}
		$zones_search = '';
		if(isset($_POST['zones_search']) && $_POST['zones_search'] != ''){
			$zones_search = $_POST['zones_search'];
		}
                $city_search='';
                if(isset($_POST['city_search']) && $_POST['city_search'] != ''){
			$city_search = $_POST['city_search'];
		}
		$data['zones'] = $this->setting_model->zones_listing($state_id, $city_id, $zones_search);
                $data['city'] = $this->setting_model->city_listing($state_id, $city_search);
                 $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
                
               // echo "<pre>"; print_R($data['city']);die;
		$this->load->view('setting/Locations', $data);
	}
        
        public function view_searchzone_list()
        {
            $data=$this->setting_model->zones_search_listing();
            echo json_encode($data);
        }
        
        public function view_searchcity_list()
        {
            $data=$this->setting_model->city_search_listing();
            echo json_encode($data);
        }
        
	public function state_city_list(){
		$state_id = $this->input->post('state_id');
		$city_list = $this->setting_model->state_city_list($state_id);
		echo $city_list;
	}
	public function city_zone_list(){
		$city_id = $this->input->post('city_id');
		$zone_list = $this->setting_model->city_zone_list($city_id);
		echo $zone_list;
	}
	public function add_zone(){
		$add_zone = $this->setting_model->add_zone();
		redirect(base_url()."setting/Locations");
	}
        
        public function add_zonedata()
        {
            $add_zone = $this->setting_model->add_zonedata();
            echo json_encode($add_zone);
        }
        
        public function add_citydata(){
		$add_city = $this->setting_model->add_citydata();
		 echo json_encode($add_city);
	}

	public function update_zone(){
		$update_zone = $this->setting_model->update_zone();
		redirect(base_url()."setting/Locations");
	}
	public function add_city(){
		$add_zone = $this->setting_model->add_city();
		redirect(base_url()."setting/Locations");
	}
        
        public function update_city(){
		$add_zone = $this->setting_model->update_city();
		redirect(base_url()."setting/Locations");
	}
	// function for location tab send

	// function for team management tab start
	public function team_management(){
		$this->load->view('setting/team_mgnt');
	}
        
        public function add_tax()
        {
            $id=$this->setting_model->add_tax();
            echo json_encode($id);
        }
        
        public function add_citrus_data()
        {
              $id=$this->setting_model->add_citrus_data();
               echo json_encode($id);
        }
	
	  public function add_ebs_data()
        {
              $id=$this->setting_model->add_ebs_data();
               echo json_encode($id);
        }
	

	
	 public function add_payu_data()
        {
              $id=$this->setting_model->add_payu_data();
               echo json_encode($id);
        }
	 public function add_gupshup_data()
        {
              $id=$this->setting_model->add_gupshup_data();
               echo json_encode($id);
        }
	
	 public function add_rightsms_data()
        {
              $id=$this->setting_model->add_rightsms_data();
               echo json_encode($id);
        }
	
	 public function add_textmsg91_data()
        {
              $id= $this->setting_model->add_textmsg91_data();
               echo json_encode($id);
        }
	
	 public function add_otpmsg91_data()
        {
              $id= $this->setting_model->add_otpmsg91_data();
               echo json_encode($id);
        }
        
           public function add_paytm_data()
        {
              $id=$this->setting_model->add_paytm_data();
               echo json_encode($id);
        }
	
	public function add_email_data()
        {
              $id=$this->setting_model->add_email_data();
               echo json_encode($id);
        }
        
        public function isp_detail()
        {
		if(!$this->plan_model->is_permission(ISPSETUP,'HIDE'))
	   {
	       redirect(base_url());
	   }
		if(!$this->plan_model->is_permission(ISPSETUP,'EDIT')){
		 if($this->plan_model->is_permission(ISPSETUP,'RO'))
		 {
		     $data['ro']=1;
		 }
		 else
		 {
		     redirect(base_url());
		 }
	  
	  }
	       $arr=$this->setting_model->get_ispdetail_info();
	      $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
	      $data['ispbaseinfo']=$this->setting_model->get_ispbase_info();
	      $this->load->view('setting/Isp_detail',$data);
        }
        
     
        
        
        public function add_aboutus()
        {
            $id=$this->setting_model->add_aboutus();
            if($id)
            {
                redirect(base_url()."setting/isp_detail");
            }
            
        }
        
          public function add_ispdetail()
        {
           
            $imagedata=$this->image_upload();
	    $signimagedata=$this->image_signature_upload();
            $id=$this->setting_model->add_ispdetail_data($imagedata,$signimagedata);
            if($id)
            {
                redirect(base_url()."setting/isp_detail");
            }
         }
        
         public function add_services()
        {
            $id=$this->setting_model->add_services();
            if($id)
            {
                redirect(base_url()."setting/isp_detail");
            }
            
        }
        public function getExtension($str){
       $i = strrpos($str,".");
       if (!$i) { return ""; }
       $l = strlen($str) - $i;
       $ext = substr($str,$i+1,$l);
       return $ext;
   }
        
        public function image_upload()
        {
		
		if(isset($_POST['isp_logo_values'])){
            $logo_image_path = json_decode($_POST['isp_logo_values']);
            $image_name =  $logo_image_path->name;
            $image_src = $logo_image_path->data;
		$base_url=NEWPATH;
	$original="";
	 $small="";
	$logo="";
    if (!file_exists($base_url . 'original/')) {
        mkdir($base_url . 'original/', 0777, TRUE);
    }
    $file_ext = $this->getExtension($image_name);
    $explode = explode(',',$image_src);
    $image_src = $explode['1'];
    $image_src = str_replace(' ', '+', $image_src);
    $data_img = base64_decode($image_src);
    $filename = uniqid() . '.'.$file_ext;
    $file = $base_url . $filename;
    $success = file_put_contents($file, $data_img);
    $newname=date("Ymdhisv").rand().".".$file_ext;
    rename($base_url.$filename,$base_url.'original/'.$newname);
    if (!file_exists($base_url . 'logo/')) {
        mkdir($base_url . 'logo/', 0777, TRUE);
    }
    if (!file_exists($base_url . 'small/')) {
        mkdir($base_url . 'small/', 0777, TRUE);
    }
    $this->load->library('image_lib');
    $config1['image_library'] = 'gd2';
    $config1['source_image'] = $base_url . 'original/'.$newname;
    $config1['new_image'] = $base_url . 'logo/'."300_".$newname;

    $config1['maintain_ratio'] = TRUE;
    $config1['width'] = 300;
    $config1['height'] = 100;
    $this->image_lib->initialize($config1);
    $this->image_lib->resize();

    $this->image_lib->clear();
    $config2['image_library'] = 'gd2';
    $config2['source_image'] = $base_url . 'original/'.$newname;
     $config2['new_image'] = $base_url . 'small/'."600_".$newname;

    $config2['maintain_ratio'] = TRUE;
    $config2['width'] = 600;
    $config2['height'] = 200;
    $this->image_lib->initialize($config2);
    $this->image_lib->resize();

    $original=$newname;
    $small="600_".$newname;
    $logo="300_".$newname;
    
    $fname = $base_url.'original/'.$newname;
    $fname1 = $base_url . 'logo/'."300_".$newname;
    $fname2 = $base_url . 'small/'."600_".$newname;
       
    $famazonname = AMAZONPATHISP.'original/'.$newname;
    $famazonname1 = AMAZONPATHISP.'logo/300_'. $newname;
    $famazonname2 = AMAZONPATHISP.'small/600_'. $newname;
    
      $data['original']=$newname;
       $data['small']="600_".$newname;
       $data['logo']="300_".$newname;

    $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
    $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
    $this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
    $this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
		
		
	}
            else
            {
                    
                          $data['original']="";
                          $data['small']="";
                          $data['logo']="";
            }
            return $data;
        
        }
	
	
	
	
	 public function image_signature_upload()
        {
           /* $base_url=NEWPATH;
          //  echo "<pre>"; print_R($_FILES);die;
             if(isset($_FILES['file1']['name']) && $_FILES['file1']['name']!='')
            {
                     if (!file_exists($base_url . 'original/')) {
                            mkdir($base_url . 'original/', 0777, TRUE);
                            }
                 $filename=$_FILES['file1']['name'];
                $fileupload= move_uploaded_file($_FILES['file1']['tmp_name'], $base_url.$filename);
                 $file_ext = end(explode('.',$filename));
		 $ispdetail=$this->setting_model->get_ispdetail_info();
		 if(count($ispdetail)>0)
		 {
			if($ispdetail[0]->original_signature!='')
			{
				$newname=$ispdetail[0]->original_signature;   
			}
			else{
				$newname=date("Ymdhisv").rand().".".$file_ext;   
			}
		 }
		 else{
			 $newname=date("Ymdhisv").rand().".".$file_ext;    
		 }
                             // $newname=date("Ymdhisv").rand().".".$file_ext;    
                              rename($base_url.$filename,$base_url.'original/'.$newname);
                              
                             
                
                  if (!file_exists($base_url . 'logo/')) {
                            mkdir($base_url . 'logo/', 0777, TRUE);
                            }
                             if (!file_exists($base_url . 'small/')) {
                            mkdir($base_url . 'small/', 0777, TRUE);
                            }
                              $this->load->library('image_lib');
                            $config1['image_library'] = 'gd2';
                            $config1['source_image'] = $base_url . 'original/'.$newname;
                            $config1['new_image'] = $base_url . 'logo/'."200_".$newname;
                         
                            $config1['maintain_ratio'] = TRUE;
                            $config1['width'] = 200;
                            $config1['height'] = 100;
                            $this->image_lib->initialize($config1);
                            $this->image_lib->resize();
                            
                            $this->image_lib->clear();
                           
                              
                          $data['original']=$newname;
                         
                          $data['logo']="200_".$newname;
                          
                          
                           $fname = $base_url.'original/'.$newname;
                           $fname1 = $base_url . 'logo/'."200_".$newname;
                         
	   
		
                    $famazonname = AMAZONPATHISP.'original/'.$newname;
                     $famazonname1 = AMAZONPATHISP.'logo/200_'. $newname;
                    
	  
                          
                          $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
                            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
                           
                           
                          
            }*/
	   if(isset($_POST['isp_signature_values'])){
            $logo_image_path = json_decode($_POST['isp_signature_values']);
            $image_name =  $logo_image_path->name;
            $image_src = $logo_image_path->data;
		$base_url=NEWPATH;
	$original="";
	 $small="";
	$logo="";
    if (!file_exists($base_url . 'original/')) {
        mkdir($base_url . 'original/', 0777, TRUE);
    }
    $file_ext = $this->getExtension($image_name);
    $explode = explode(',',$image_src);
    $image_src = $explode['1'];
    $image_src = str_replace(' ', '+', $image_src);
    $data_img = base64_decode($image_src);
    $filename = uniqid() . '.'.$file_ext;
    $file = $base_url . $filename;
    $success = file_put_contents($file, $data_img);
    $newname=date("Ymdhisv").rand().".".$file_ext;
    rename($base_url.$filename,$base_url.'original/'.$newname);
    if (!file_exists($base_url . 'logo/')) {
        mkdir($base_url . 'logo/', 0777, TRUE);
    }
    if (!file_exists($base_url . 'small/')) {
        mkdir($base_url . 'small/', 0777, TRUE);
    }
    $this->load->library('image_lib');
    $config1['image_library'] = 'gd2';
    $config1['source_image'] = $base_url . 'original/'.$newname;
    $config1['new_image'] = $base_url . 'logo/'."200_".$newname;

    $config1['maintain_ratio'] = TRUE;
    $config1['width'] = 200;
    $config1['height'] = 100;
    $this->image_lib->initialize($config1);
    $this->image_lib->resize();

    $this->image_lib->clear();
    $config2['image_library'] = 'gd2';
    $config2['source_image'] = $base_url . 'original/'.$newname;
     $config2['new_image'] = $base_url . 'small/'."200_".$newname;

    $config2['maintain_ratio'] = TRUE;
    $config2['width'] = 200;
    $config2['height'] = 100;
    $this->image_lib->initialize($config2);
    $this->image_lib->resize();

    $original=$newname;
    $small="200_".$newname;
    $logo="200_".$newname;
    
    $fname = $base_url.'original/'.$newname;
    $fname1 = $base_url . 'logo/'."200_".$newname;
    $fname2 = $base_url . 'small/'."200_".$newname;
       
    $famazonname = AMAZONPATHISP.'original/'.$newname;
    $famazonname1 = AMAZONPATHISP.'logo/320_'. $newname;
    $famazonname2 = AMAZONPATHISP.'small/200_'. $newname;
    
      $data['original']=$newname;
       $data['small']="200_".$newname;
       $data['logo']="200_".$newname;

    $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
    $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
    $this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
    $this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
		
		
	}
            else
            {
                    
                          $data['original']="";
                          $data['small']="";
                          $data['logo']="";
            }
            return $data;
        
        }
	
	public function get_deletezone_data()
	{
		 $data=$this->setting_model->get_deletezone_data();
		 echo json_encode($data);
	}
	
	public function delete_zone()
	{
		 $data=$this->setting_model->delete_zone();
		if($data)
		{
			redirect(base_url()."setting/locations");
		}
	}
        
	public function add_ispbanking(){
		$this->setting_model->add_ispbanking();
	}
       
	// function for team management tab end
	
	public function add_bulksmsindia_data()
        {
              $id=$this->setting_model->add_bulksmsindia_data();
               echo json_encode($id);
        }
	
	public function add_cloudplace_data()
        {
              $id=$this->setting_model->add_cloudplace_data();
               echo json_encode($id);
        }
	
	public function customizeinvoice(){
		$subid = $this->setting_model->customizeinvoice();
		if($subid){
			redirect(base_url()."setting/isp_detail");
		}
	}
	
	public function apksetup(){
		$subid = $this->setting_model->apksetup();
		if($subid){
			redirect(base_url()."setting/isp_detail");
		}
	}
}
