<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL);
class Ttask extends CI_Controller {
    
    public function __construct(){
        parent :: __construct();
        $this->load->library('form_validation');
        $this->load->model('TTask_model', 'ttask_model');
        $this->load->model('Notifications_model', 'notify_model');
        if(!$this->session->has_userdata('isp_session')){
            redirect(base_url().'login'); exit;
        }
        $isp_license_data = $this->ttask_model->isp_license_data(); 
        if($isp_license_data != 1){
            redirect(base_url().'manageLicense'); exit;
        }
        $this->ttask_model->check_ispmodules();
    }
    
    public function index(){
        $this->load->view('ttask/ttask_view');
    }
    
    public function add_task(){
        $this->ttask_model->add_task();
    }
    
    public function open_tasklist(){
        $this->ttask_model->open_tasklist();
    }
    
    public function taskdetails(){
        $this->ttask_model->taskdetails();
    }
    
    public function edit_task(){
        $this->ttask_model->edit_task();
    }
    
    public function addtask_comment(){
        $this->ttask_model->addtask_comment();
    }
    
    public function showtask_comment(){
        $this->ttask_model->showtask_comment();
    }
    
    public function filtertask(){
        $this->ttask_model->filtertask_listing();
    }
}
?>