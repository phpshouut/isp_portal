<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		$this->load->model('login_model');
		
	}

	
	public function index(){
		$this->load->view('terms_view');
	}
	public function check_terms_condition_update(){
		$this->login_model->check_terms_condition_update();
		$ispmodules = $this->login_model->check_ispmodules();
		if($ispmodules == '1'){
			redirect(base_url().'user');
		}elseif($ispmodules == '2'){
			redirect(base_url().'publicwifi');
		}
	}
	
	
	
}
