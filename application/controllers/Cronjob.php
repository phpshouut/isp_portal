<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjob extends CI_Controller {
	
	public function __construct(){
		parent :: __construct();
		$this->load->model('Cron_model', 'cron_model');
		$this->load->model('User_model', 'user_model');
		$this->load->model('Notifications_model', 'notifications_model');
		
	}
	
	public function index(){
		echo 'hii';
	}

	public function mail_format(){
		$abc = $this->cron_model->mail_format('10600536');
		print_r($abc);
	}
	
	public function bill_generation(){
		$this->cron_model->bill_generation();
	}
	
	public function prepaid_bill_generation(){
		$this->cron_model->prepaid_bill_generation();
	}
	
	public function custom_bill_generation(){
		$this->cron_model->custom_bill_generation();
	}
	
	public function check_topup_expiration(){
		$this->cron_model->check_topup_expiration();
	}
	
	public function activate_tempsuspended_users(){
		$this->cron_model->activate_tempsuspended_users();
	}
	
	public function updated_expirydates(){
		$this->cron_model->updated_expirydates();
	}
	
	public function updated_bill_amount(){
		$this->cron_model->updated_bill_amount();
	}
	
	public function update_plan_changed_datetime(){
		$this->cron_model->update_plan_changed_datetime();
	}
	
	public function plandetails(){
		//$this->cron_model->check_updated_plandetails('258');
	}
	
	public function creditlimit_reached()
	{
		$this->cron_model->creditlimit_reached();
	}
	
	public function account_expiration_alert(){
		$this->cron_model->account_expiration_alert();
	}
	
	public function due_prebills(){
		//$this->cron_model->due_prepaid_bill_generation();
	}
	public function check_duplicate_billnumber(){
		$this->cron_model->check_duplicate_billnumber();
	}
	public function delete_duplicatebills(){
		$this->cron_model->delete_duplicatebills();
	}
	
	public function user_session_restart(){
		$this->cron_model->user_session_restart();
	}
	
	public function bill_generation_foruser(){
		$this->cron_model->bill_generation_foruser();
	}
	
	public function disconnect_alluserineternet(){
		$this->cron_model->disconnect_alluserineternet();
	}
}

