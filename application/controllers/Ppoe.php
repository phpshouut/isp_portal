<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');
class Ppoe extends CI_Controller {

	 public function __construct()
         {
            parent::__construct();
            $this->load->model('ppoe_model');
	      $this->load->model('plan_model');
	       $this->load->model('user_model');
		   $this->load->model('setting_model');
         }
	public function index()
	{
		 $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
		$this->load->view('setting/Ppoe_view',$data);
	}
	
	
        
        public function add_connection()
        {
           $data= $this->ppoe_model->add_connection();
	   echo json_encode($data);
        }
        
       public function get_etherlist()
       {
	 $data=$this->ppoe_model->get_etherlist();
	 echo json_encode($data);
       }
       
       public function checkvlanid()
       {
	  $data=$this->ppoe_model->checkvlanid();
	 echo json_encode($data);
       }
       
        public function checkvlanname()
       {
	  $data=$this->ppoe_model->checkvlanname();
	 echo json_encode($data);
       }
       
       public function check_nassetup()
       {
	  $data=$this->ppoe_model->check_nassetup();
	 echo json_encode($data);
       }
       
       public function set_routervlan()
       {
	 
	  $data=$this->ppoe_model->setup_vlan();
	 echo json_encode($data);
	 
       }
       
       public function ppoe_listdata()
       {
	 $data=$this->ppoe_model->ppoe_listdata();
	 echo json_encode($data);
       }
       
       public function execute_cmnd()
       {
	 $data=$this->ppoe_model->execute_cmnd();
	 echo json_encode($data);
       }
       
       public function execute_cmnd_ppp()
       {
	  $data=$this->ppoe_model->execute_cmnd_ppp();
	 echo json_encode($data);
       }
       

       
       
       
}
