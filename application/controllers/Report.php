<?php defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL); 
class Report extends CI_Controller{

    public function __construct() {
	parent::__construct();
	$this->load->model('report_model');
	$this->load->model('user_model');
	$this->load->model('plan_model');
	$this->load->model('setting_model');
	$this->load->model('Notifications_model', 'notify_model');
	$this->load->library('excel');
	$this->load->helper('download');

	if(!$this->session->has_userdata('isp_session')){
	    redirect(base_url().'login'); exit;
	} else{
	    $sessiondata = $this->session->userdata('isp_session');
	    // echo "<pre>"; print_R($sessiondata); die;
	    if(!isset($sessiondata['is_franchise'])) {
		redirect(base_url().'login'); exit;
	    }
	}
	$isp_license_data = $this->report_model->isp_license_data();
	if($isp_license_data != 1){
	    redirect(base_url().'manageLicense'); exit;
	}
	$this->plan_model->check_ispmodules();
    }

    public function index() {
	$arr=$this->setting_model->get_ispdetail_info();
	$data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
	//$complaintarr=array("change_address"=>"Address Change","change_plan"=>"Plan Change","terminate_service"=>"Terminate Service","suspend_service"=>"Suspend Service","service_down"=>"Service Down","router_not_working"=>"Router Issues","other_request"=>"Other Request");
	$data['complaint_type']=$this->report_model->gettkttype();
	$data['perspective'] = $this->report_model->persp_user();
	// echo "<pre>"; print_R($data);die;
	$this->load->view('report/Report_view',$data);
    }

    public function persp_user_filter() {
	$this->report_model->persp_user_filter();
	//echo $data;
    }

    public function persp_user_filter_excel() {
	$this->report_model->persp_user_filter_excel();
	//echo $data;
    }

    public function persp_user_filter_excel_dwnld(){
	header('Content-Type:	application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
	header('Content-Disposition:	attachment;filename="perspectiveuser_reports.xlsx"');
	header('Cache-Control: max-age=0'); 
	ob_clean(); 
	flush();
	readfile('reports/perspectiveuser_reports.xlsx'); exit;
    }


    public function active_user() {
	$this->report_model->active_user();
    }

    public function active_user_excel(){
	$this->report_model->active_user_excel();
    }

    public function active_user_filter_excel_dwnld(){
	header('Content-Type:	application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
	header('Content-Disposition:	attachment;filename="activeuser_reports.xlsx"');
	header('Cache-Control:	max-age=0');
	ob_clean(); 
	flush();
	readfile('reports/activeuser_reports.xlsx'); exit;
    }

    public function compaint_request() {
	$this->report_model->compaint_request();
    }

    public function compaint_request_excel(){
	$this->report_model->compaint_request_excel();
    }
    
    public function compaint_request_excel_dwnld(){
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
	header('Content-Disposition: attachment;filename="complaint_report.xlsx"'); header('Cache-Control: max-age=0');
	ob_clean(); flush();
	readfile('reports/complaint_report.xlsx'); exit;
    }

    public function plan_topup_report() {
	$this->report_model->plan_topup_report();
    }

    public function plan_topup_report_excel(){
	$this->report_model->plan_topup_report_excel();
    }

    public function plan_topup_report_excel_dwnld(){
	 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
	header('Content-Disposition:	attachment;filename="plantopup_report.xlsx"');
	header('Cache-Control:	max-age=0'); 
	ob_clean();
	flush();
	readfile('reports/plantopup_report.xlsx'); exit;

    }

    public function billing_report() {
	 $this->report_model->billing_report();
    }

    public function billing_report_excel() {
	echo $this->report_model->billing_report_excel();
    }
    
    public function billing_report_excel_download() {
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
	header('Content-Disposition: attachment;filename="billing_reports.xlsx"');
	header('Cache-Control: max-age=0');
	ob_clean(); 
	flush();
	readfile('reports/billing_reports.xlsx'); exit;
    }


/**************** CUSTOM REPORT SECTION ******************************/
    public function custom_reportsheet(){
	$report_type = $this->input->post('customreport_type');
	if($report_type == 'addtowallet_sheet'){
	    $this->report_model->addtowallet_sheet();
	}elseif(($report_type == 'newusers_sheet') || ($report_type == 'suspendedusers_sheet') || ($report_type == 'terminatedusers_sheet')){
	    $this->report_model->userslist_sheet($report_type);
	}elseif($report_type == 'onoffusers_sheet'){
	    $this->report_model->userstatus_sheet();
	}elseif($report_type == 'usersinvoice_sheet'){
	    $this->report_model->usersinvoice_sheet();
	}elseif($report_type == 'onlinepayments_sheet'){
	    $this->report_model->onlinepayments_sheet();
	}elseif($report_type == 'userstickets_sheet'){
	    $this->report_model->userstickets_sheet();
	}elseif($report_type == 'pendingpayment_sheet'){
	    $this->report_model->pendingpayment_sheet();
	}elseif($report_type == 'smsreports_sheet'){
	    $this->report_model->smsreports_sheet();
	}
    }

    public function exportcustom_reportsheet(){
	$report_type = $this->input->post('customreport_type');
	if($report_type == 'addtowallet_sheet'){
	    $this->report_model->exportaddtowallet_sheet();
	}elseif(($report_type == 'newusers_sheet') || ($report_type == 'suspendedusers_sheet') || ($report_type == 'terminatedusers_sheet')){
	    $this->report_model->exportuserslist_sheet($report_type);
	}elseif($report_type == 'onoffusers_sheet'){
	    $this->report_model->exportuserstatus_sheet();
	}elseif($report_type == 'usersinvoice_sheet'){
	    $this->report_model->exportusersinvoice_sheet();
	}elseif($report_type == 'onlinepayments_sheet'){
	    $this->report_model->exportonlinepayments_sheet();
	}elseif($report_type == 'userstickets_sheet'){
	    $this->report_model->exportuserstickets_sheet();
	}elseif($report_type == 'pendingpayment_sheet'){
	    $this->report_model->exportpendingpayment_sheet();
	}elseif($report_type == 'smsreports_sheet'){
	    $this->report_model->exportsmsreports_sheet();
	}
    }
    
    public function send_payment_duealert(){
	$uuid = $this->input->post('uuid');
	$smstosend = $this->input->post('smstosend');
	$data = $this->notify_model->send_payment_duealert($uuid, $smstosend);
	echo $data;
    }
}

?>