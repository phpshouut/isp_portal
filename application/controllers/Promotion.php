<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Promotion extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('promotion_model');
         $this->load->model('plan_model');
         $this->load->model('setting_model');
		  $this->load->model('user_model');
        if(!$this->session->has_userdata('isp_session')){
	    redirect(base_url().'login'); exit;
	}
	$isp_license_data = $this->promotion_model->isp_license_data(); 
		if($isp_license_data != 1){
			redirect(base_url().'manageLicense'); exit;
		}
        $this->promotion_model->check_ispmodules();
	$paramsthree=array("accessKey"=>awsAccessKey,"secretKey"=>awsSecretKey);
                 $this->load->library('s3',$paramsthree);
    }
    
    public function index()
    {
            if(!$this->plan_model->is_permission(PROMOTION,'HIDE'))
        {
            redirect(base_url());
        }
          if(!$this->plan_model->is_permission(PROMOTION,'EDIT')){
              if($this->plan_model->is_permission(PROMOTION,'RO'))
              {
                  $data['ro']=1;
              }
              else
              {
                  redirect(base_url());
              }
       
       }
       $data['service_list'] = $this->promotion_model->service_list();
       $data['banner_list'] = $this->promotion_model->banner_list();
        $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
       $this->load->view('promotion/home_promotion', $data);
    }

    public function add_home_banner(){
	$this->promotion_model->add_home_banner();
	redirect(base_url().'promotion');
    }
    
    public function marketing_promotion(){
          if(!$this->plan_model->is_permission(PROMOTION,'HIDE'))
        {
            redirect(base_url());
        }
            if(!$this->plan_model->is_permission(PROMOTION,'EDIT')){
              if($this->plan_model->is_permission(PROMOTION,'RO'))
              {
                  $data['ro']=1;
              }
              else
              {
                  redirect(base_url());
              }
       
       }
	$data['marketing_promotion_list'] = $this->promotion_model->marketing_promotion_list();
         $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
	$this->load->view('promotion/marketing_promotion', $data);
    }
    
    public function add_marketing_promotion_banner(){
	$this->promotion_model->add_marketing_promotion_banner();
	redirect(base_url().'promotion/marketing_promotion');
    }
    
    public function delete_marketing_promotion(){
	$this->promotion_model->delete_marketing_promotion();
	redirect(base_url().'promotion/marketing_promotion');
    }
    
    public function edit_marketing_promotion_view(){
	$id = $this->input->post('promotion_id');
	$data = $this->promotion_model->edit_marketing_promotion_view($id);
	echo json_encode($data);
    }
    public function update_marketing_promotion_banner(){
	$data = $this->promotion_model->update_marketing_promotion_banner();
	redirect(base_url().'promotion/marketing_promotion');
    }
    
}

?>