<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class User extends CI_Controller {
	public function __construct(){
		parent :: __construct();
		$this->load->library('form_validation');
		$this->load->model('User_model', 'user_model');
		$this->load->model('Emailer_model', 'emailer_model');
		$this->load->model('Plan_model', 'plan_model');
		$this->load->model('Notifications_model', 'notify_model');
		$this->load->library('Dpdf');
		$this->load->library('Excel');
		if(!$this->session->has_userdata('isp_session')){
			redirect(base_url().'login'); exit;
		}
		$isp_license_data = $this->user_model->isp_license_data(); 
		if($isp_license_data != 1){
			redirect(base_url().'manageLicense'); exit;
		}
		$this->user_model->check_ispmodules();
		$paramsthree=array("accessKey"=>awsAccessKey,"secretKey"=>awsSecretKey);
		$this->load->library('s3',$paramsthree);
	}

	public function emailer(){
		//$this->emailer_model->installation_billing('vtnandbl121', '1217');
		//$this->emailer_model->security_billing('vtnandbl121', '1218');
		//$this->emailer_model->activate_user_emailer('vtnandbl121', '12345');
		//$this->emailer_model->monthly_billing('10600536', '1740');
		//$this->emailer_model->topups_billing('10600536', '1768');
		$this->emailer_model->checkemailserver();
	}
	
	public function activate_emailer(){
		$radchk_pwd = random_string('alnum', 7);
		$this->emailer_model->activate_user_emailer('110000000003',$radchk_pwd);
	}
	public function checkpdf_format(){
		$this->emailer_model->checkpdf_format();
	}
	
	public function send_lastbill_tousers(){
		$this->notify_model->send_lastbill_tousers();
	}
	
/*------------------- USER DASHBOARD STARTS ----------------------------------------------------------------*/	
	public function index(){
		$data = $this->user_model->user_dashboard();
		$data['ispcodet'] = $this->user_model->countrydetails();
		$this->load->view('users/User_view', $data);
	}
	public function status($type = NULL){
		if($type == 'act' || $type == 'inact'){
			$data = $this->user_model->user_dashboard();
			$data['active_inactive'] = $this->user_model->active_inactive_userslist($type);
			$data['filtertype'] = $type;
			$data['ispcodet'] = $this->user_model->countrydetails();
			$this->load->view('users/CustomerFilter',$data);
		}else{
			redirect(base_url().'user');
		}
	}
	public function active_inactive_usersfilter(){
		$type = $this->input->post('filtertype');
		$filterby = $this->input->post('netstatus');
		if($filterby == 'postfup'){
			$data = $this->user_model->postfup_userslisting();
		}else{
			$data = $this->user_model->active_inactive_userslist($type);
		}
		echo json_encode($data);
		
	}
	public function filtersearch(){
		$search_user = $this->input->post('search_user');
		if($search_user != ''){
			$this->user_model->search_user();
		}else{
			redirect(base_url().'user');
		}
	}
	public function viewall_search_results(){
		$this->user_model->viewall_search_results();
	}
	public function lead_enquiry(){
		$data = $this->user_model->user_dashboard();
		$data['lead_enquiry'] = $this->user_model->lead_enquiry_userslist();
		$data['ispcodet'] = $this->user_model->countrydetails();
		$this->load->view('users/Lead_Enquiry',$data);
	}
	public function lead_enquiry_filter(){
		$data = $this->user_model->lead_enquiry_userslist();
		echo json_encode($data);
	}
	public function complaints(){
		$data = $this->user_model->user_dashboard();
		$data['ispcodet'] = $this->user_model->countrydetails();
		$data['complaints_list'] = $this->user_model->complaints_list();
		$this->load->view('users/Complaints',$data);
	}
	public function complaints_filter(){
		$data = $this->user_model->complaints_list();
		echo json_encode($data);
	}
	public function advanced_search(){
		$this->user_model->advanced_search();
	}
	
	
	public function pay_overdue(){
		$data = array();
		$data = $this->user_model->user_dashboard();
		$data['payoverdue'] = $this->user_model->payoverdue_list();
		$data['ispcodet'] = $this->user_model->countrydetails();
		$this->load->view('users/Payoverdue',$data);
	}
	public function payoverdue_userlist(){
		$data = $this->user_model->payoverdue_list();
		echo json_encode($data);
	}
	public function updateuser_profilestatus(){
		$this->user_model->updateuser_profilestatus();
	}
	public function resetuser_toactivate(){
		$this->user_model->resetuser_toactivate();
	}
	public function setupfor_adduser(){
		$setup = $this->user_model->setupfor_adduser();
		echo $setup;
	}
	public function check_taxfilled(){
		$id =  $this->user_model->check_taxfilled();
		echo $id;
	}

	public function delete_inactiveUser(){
		$this->user_model->delete_inactiveUser();
	}
	public function sendbulksms_tousers(){
		$this->notify_model->sendbulksms_tousers();
	}
	public function otherdues(){
		$data = $this->user_model->user_dashboard();
		$data['ispcodet'] = $this->user_model->countrydetails();
		$data['OTC'] = $this->user_model->otcdue_list();
		$this->load->view('users/Otherdues', $data);
	}
	public function getotcdues(){
		$data = $this->user_model->otcdue_list();
		echo json_encode($data);
	}
	public function getwalletdues(){
		$data = $this->user_model->walletdue_list();
		echo json_encode($data);
	}
	public function getcustbilldues(){
		$data = $this->user_model->custombilldue_list();
		echo json_encode($data);
	}
	public function send_logincredentials_tousers(){
		$this->notify_model->send_logincredentials_tousers();
	}
	
/*------------------- PERSONAL DETAILS STARTS ----------------------------------------------------------------*/

	public function add($leid = NULL){
		$addperm = $this->user_model->is_permission(CREATEUSER,'ADD');
		if($addperm == true){
			$data = array();
			$setup = $this->user_model->setupfor_adduser();
			if($setup == 1){
				if($leid != NULL){
					$data['letoc_dataid'] = $leid;
					$data['record'] = $this->user_model->lepersonal_details($leid);
					$data['ispcodet'] = $this->user_model->countrydetails();
					if($data['record'] == 0){
						redirect(base_url().'user/add');
					}else{
						$this->load->view('users/Add_user', $data);
					}
				}else{
					$data['letoc_dataid'] = 0;
					$data['record'] = 0;
					$data['ispcodet'] = $this->user_model->countrydetails();
					$this->load->view('users/Add_user', $data);
				}
			}else{
				redirect(base_url().'user');
			}
		}else{
			redirect(base_url().'user');
		}
	}
	public function add_lead_enquiry(){
		$this->user_model->add_subscriber();
	}
	public function add_customer(){
		$this->user_model->add_subscriber();
	}
	public function getcitylist(){
		$stateid = $this->input->post('stateid');
		echo $this->user_model->getcitylist($stateid);
	}
	public function getzonelist(){
		$stateid = $this->input->post('stateid');
		$cityid = $this->input->post('cityid');
		$zoneid = $this->input->post('zoneid');
		echo $this->user_model->zone_list($cityid, $zoneid, $stateid);
	}

	public function edit($sid='',$tab=''){
		$data = array();
		if($sid == ''){ $sid = 0; }
		$setup = $this->user_model->setupfor_adduser();
		if($setup == 1){
			/*$isterminate = $this->user_model->check_userterminate($sid);
			if($isterminate == '0'){
				$data['record'] = $this->user_model->personal_details($sid);
				$data['activate_tab'] = $tab;
				$data['ispcodet'] = $this->user_model->countrydetails();
				$this->load->view('users/Edit_user', $data);
			}else{
				redirect(base_url().'user');
			}*/
			
			$data['record'] = $this->user_model->personal_details($sid);
			$data['activate_tab'] = $tab;
			$data['ispcodet'] = $this->user_model->countrydetails();
			$this->load->view('users/Edit_user', $data);
		}else{
			redirect(base_url().'user');
		}
	}
	public function personaldetails_listing(){
		$sid = $this->input->post('sid');
		$data['record'] = $this->user_model->personal_details($sid);
		echo json_encode($data);
	}
	public function edit_personaldetails(){
		$this->user_model->edit_personaldetails();
	}

	
	public function delete_subscriber(){
		$this->user_model->delete_subscriber();
	}
	
	

	public function add_newcity(){
		$this->user_model->add_newcity();
	}
	public function add_newzone(){
		$this->user_model->add_newzone();
	}
	public function taxapplicable(){
		$tax = $this->user_model->current_taxapplicable();
		echo $tax;
	}
	public function enquiryfeedback(){
		$enq_userid = $this->input->post('enq_userid');
		$this->user_model->enquiryfeedback($enq_userid);
	}
	public function addenquiryfeedback(){
		$enq_userid = $this->input->post('enq_userid');
		$this->user_model->addenquiryfeedback($enq_userid);
	}
	public function disconnect_userineternet(){
		$uuid = $this->input->post('uuid');
		$this->notify_model->add_user_activitylogs($uuid, 'Disconnect User Internet');
		$this->user_model->disconnect_userineternet($uuid);
	}
/*------------------- KYC DETAILS STARTS ----------------------------------------------------------------*/
	
	public function upload_documents(){
		$kyc_documents_arr = $_POST['kyc_documents_arr'][0];
		$kyc_docarr = explode(',',$kyc_documents_arr);
		$kycdocnumber_arr = $_POST['kycdocnumber_arr'][0];
		$kyc_docnum = explode(',',$kycdocnumber_arr);
		
		$idproof_doctype_arr = $_POST['idproof_doctype_arr'][0];
		$idp_docarr = explode(',',$idproof_doctype_arr);
		$idproof_docnumber_arr = $_POST['idproof_docnumber_arr'][0];
		$idp_docnum = explode(',',$idproof_docnumber_arr);
		
		$corporate_doctype_arr = $_POST['corporate_doctype_arr'][0];
		$corp_docarr = explode(',',$corporate_doctype_arr);
		$corporate_docnumber_arr = $_POST['corporate_docnumber_arr'][0];
		$corp_docnum = explode(',',$corporate_docnumber_arr);
		
		$target_path = "assets/media/documents/";
		$subscriber_userid = $this->input->post('subscriber_userid');
		$directory_name = $this->user_model->customer_uuid($subscriber_userid);
		if (!file_exists($target_path.$directory_name)) {
			mkdir($target_path.$directory_name, 0777, true);
		}

		//echo '<pre>'; print_r($_POST);
		//echo '<pre>'; print_r($_FILES); die;
		
		$filename_array = array();
		if(count($_FILES) > 0){
			$filearr = $_FILES;
			foreach($filearr as $fkey => $fobj){
				$ext = explode('.', basename($filearr[$fkey]['name']));
				$file_extension = end($ext);
				$tmp = $filearr[$fkey]['tmp_name'];
				$file_name = date('Ymd').md5(uniqid()).'.'.$file_extension;
				$filename_array[$fkey] = $file_name;
				move_uploaded_file($tmp,$target_path.$directory_name.'/'.$file_name);
				
				$fname = $target_path.$directory_name.'/'.$file_name;
				$s3folder_exists = $this->s3->getObjectInfo(bucket, AMAZON_HOMEUSERS_PATH.'kyc_docs/'.$directory_name);
				if (!$s3folder_exists){
					$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
				}
				$famazonname = AMAZON_HOMEUSERS_PATH.'kyc_docs/'.$directory_name.'/'.$file_name;
				$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
				unlink($fname);
			}
		
			/*echo '<pre>'; print_r($filename_array);
			echo '<pre>'; print_r($kyc_docarr);
			echo '<pre>'; print_r($kyc_docnum);
			echo '<pre>'; print_r($idp_docarr);
			echo '<pre>'; print_r($idp_docnum);
			echo '<pre>'; print_r($corp_docarr);
			echo '<pre>'; print_r($corp_docnum);
			die;*/
			
			$this->user_model->add_subscriber_documents($filename_array,$kyc_docarr,$kyc_docnum, $idp_docarr,$idp_docnum, $corp_docarr,$corp_docnum);
		}else{
			$subscriber_userid = $this->input->post('subscriber_userid');
			echo $subscriber_userid;
		}
		
	}
	public function edit_allkyc_details(){
		$this->user_model->edit_allkyc_details();
	}
	
	public function edit_docdelete(){
		$this->user_model->edit_docdelete();
	}

	public function downloadkyc($subid = ''){
		$this->user_model->downloadkyc($subid);
	}
	
	public function upload_allimages_s3server(){
		$this->user_model->upload_allimages_s3server();
	}
/*------------------- TICKETS DETAILS STARTS ----------------------------------------------------------------*/	
	
	public function ticket_sorters_list(){
		$this->user_model->ticket_sorters_list();
	}
	public function add_ticket_request(){
		$this->user_model->add_ticket_request();
	}
	public function fetch_allopenticket(){
		$this->user_model->fetch_allopenticket();
	}
	public function fetch_allclosedticket(){
		$this->user_model->fetch_allclosedticket();
	}
	public function edit_ticket_request(){
		$this->user_model->edit_ticket_request();
	}
	public function send_otp_tktclose(){
		$phone = $this->input->post('phone');
		$this->user_model->OTP_MSG($phone);
	}
	public function verify_confirm_tktclose(){
		$phone = $this->input->post('phone');
		$otp = $this->input->post('otp');
		$tktid = $this->input->post('tktid');
		$verify = $this->user_model->verifyBySendOtp($phone,$otp);
		if($verify == '1'){
			$this->user_model->close_subscriber_ticket($tktid);
		}else{
			echo json_encode(0);
		}
	}
	
	public function tktclose_byadmin(){
		$tktid = $this->input->post('tktid');
		$this->user_model->close_subscriber_ticket($tktid);
	}

	
	public function getticketstype(){
		$this->user_model->getticketstype();
	}
	public function ticketstype_optionslist(){
		echo $this->user_model->ticketstype_optionslist();
	}
	public function add_tickettypes(){
		$this->user_model->add_tickettypes();
	}
	public function tickettype_iddetails(){
		$this->user_model->tickettype_iddetails();
	}
	public function delete_tickettypes(){
		$this->user_model->delete_tickettypes();
	}
	public function changeticket_statusconfirmation(){
		$this->user_model->changeticket_statusconfirmation();
	}
	public function show_ticket_summary(){
		$this->user_model->show_ticket_summary();
	}
	public function update_ticketassigner(){
		//$this->user_model->update_ticketassigner();
	}
/*------------------- PLAN DETAILS STARTS ----------------------------------------------------------------*/

	public function subscriber_plan_details(){
		$this->user_model->getplan_details();
	}
	public function planassoc_withuser(){
		$this->user_model->planassoc_withuser();
	}
	public function userassoc_plan_pricing(){
		$this->user_model->userassoc_plan_pricing();
	}
	public function plandetails(){
		$this->user_model->getplan_details();
	}
	
	public function add_subscriber_plan(){
		$this->user_model->add_subscriber_plan();
	}



	public function nextcycle_planlist(){
		$uuid = $this->input->post('uuid');
		$this->user_model->nextcycle_planlist($uuid);
	}
	public function add_nextcycleplan(){
		$this->user_model->add_nextcycleplan();
	}
	public function updateTicketMember(){
		$this->user_model->updateTicketMember();
	}
	public function calcPrevPlanCost(){
		$this->user_model->calcPrevPlanCost();
	}
	public function apply_planfornow(){
		$this->user_model->apply_planfornow();
	}
	public function current_user_plan_type(){
		$this->user_model->current_user_plan_type();
	}
	public function cancel_nextcycleplan(){
		$this->user_model->cancel_nextcycleplan();
	}
	public function getcurr_plan_pricing(){
		$uuid = $this->input->post('uuid');
		$srvid = $this->input->post('srvid');
		$planpricing = $this->user_model->current_plan_pricing($srvid, $uuid);
		echo json_encode($planpricing);
	}
	public function update_planspeed(){
		$this->user_model->update_planspeed();
	}
	public function change_planautorenewal(){
		$this->user_model->change_planautorenewal();
	}
	public function user_planautorenewal_status(){
		$this->user_model->user_planautorenewal_status();
	}
/*------------------- TOPUP DETAILS STARTS -----------------------------------------------------------------*/

	//to enable/disable TOPUP Tab
	public function check_topup_authenticy(){ 
		$this->user_model->check_topup_authenticy();
	}
	
	public function datatopup_check(){
		$this->user_model->active_topuplist();
	}
	public function assingtopup_touser(){
		$this->user_model->assingtopup_touser();
	}
	public function userassoc_topuplist(){
		$this->user_model->userassoc_topuplist();
	}
	public function topup_pricing(){
		$data = $this->user_model->topup_pricing();
		echo json_encode($data);
	}

/*------------------- BILLING DETAILS STARTS ----------------------------------------------------------------*/

	public function add_installation_charges(){
		$this->user_model->add_installation_charges();
	}
	public function add_security_charges(){
		$this->user_model->add_security_charges();
	}
	public function user_billing_listing(){
		$this->user_model->user_billing_listing();
	}
	
	public function user_amtcredit_listing(){
		$this->user_model->user_amtcredit_listing();
	}
	public function advance_prepaid_charges(){
		$this->user_model->advance_prepaid_charges();
	}
	public function user_activate_check(){
		$this->user_model->user_activate_check();
	}
	public function active_user_confirmation(){
		$this->user_model->active_user_confirmation();
	}
	public function addtowallet(){
		$this->user_model->addtowallet();
	}
	public function update_receipt_number(){
		$this->user_model->update_receipt_number();
	}
	public function getbilldetail(){
		$this->user_model->getbilldetail();
	}
	public function delete_billentry(){
		$this->user_model->delete_billentry();
	}
	public function update_walletLimit(){
		$this->user_model->update_walletLimit();
	}
	
	public function updatebilling_charges(){
		//echo '<pre>'; print_r($_POST); echo '</pre>'; die;
		$this->user_model->updatebilling_charges();
	}
	public function billcopy_actions(){
		$this->user_model->billcopy_actions();
	}
	public function download_billpdf($billid = NULL){
		if($billid != NULL){
			$this->emailer_model->download_billpdf($billid);
		}else{
			redirect(base_url());
		}
	}
	public function print_billpdf($billid = NULL){
		if($billid != NULL){
			$this->emailer_model->print_billpdf($billid);
		}else{
			redirect(base_url());
		}
	}

	public function download_custombillpdf($billid = NULL){
		if($billid != NULL){
			$this->emailer_model->download_custombillpdf($billid);
		}else{
			redirect(base_url());
		}
	}
	public function print_custbillpdf($billid = NULL){
		if($billid != NULL){
			$this->emailer_model->print_custombillpdf($billid);
		}else{
			redirect(base_url());
		}
	}
	public function delete_custbillentry(){
		$this->user_model->delete_custombillentry();
	}
	public function getcustombill_details(){
		$this->user_model->getcustombill_details();
	}
	public function delete_custbillinvoice(){
		$this->user_model->delete_custbillinvoice();
	}
	public function update_custombilling(){
		$this->user_model->update_custombilling();
	}

	public function getdetails_custombilling(){
		$this->user_model->getdetails_custombilling();
	}
	public function addisp_company(){
		$this->user_model->addisp_company();
	}
	public function addisp_topic(){
		$this->user_model->addisp_topic();
	}
	public function gettopicdetails(){
		$this->user_model->gettopicdetails();
	}
	public function addcustombill_invoice(){
		$this->user_model->addcustombill_invoice();
	}
	
	public function generate_custombilling(){
		$this->user_model->generate_custombilling();
	}
	public function customBillHandler(){
		$this->user_model->customBillHandler();
	}
	public function custombilling_listing(){
		$this->user_model->custombilling_listing();
	}
	public function pastreceiptbill_listing(){
		$this->user_model->pastreceiptbill_listing();
	}
	public function deletebill_receiptentry(){
		$this->user_model->deletebill_receiptentry();
	}
	public function getuser_walletbalance(){
		$cust_uuid = $this->input->post('cust_uuid');
		$wbalance = $this->user_model->userwallet_amount($cust_uuid);
		echo $wbalance['wallet_leftamt'];
	}
	public function payinstall_securitybill(){
		$this->user_model->payinstall_securitybill();
	}
	public function show_wallethistory(){
		$this->user_model->show_wallethistory();
	}
	public function delete_walletentry(){
		$this->user_model->delete_walletentry();
	}
	public function show_advprepaylisting(){
		$this->user_model->user_advanced_payment_listing();
	}
	public function updatecustombill_invoice(){
		$this->user_model->updatecustombill_invoice();
	}
	public function calculate_custbilltax(){
		$this->user_model->calculate_custbilltax();
	}
	public function addcustombillrcpt(){
		$this->user_model->addcustombillrcpt();
	}
	public function delete_custbillrcptentry(){
		$this->user_model->delete_custbillrcptentry();
	}
	public function getwallet_billdetails(){
		$this->user_model->getwallet_billdetails();
	}
	
	public function manage_instsecuritybills(){
		$this->user_model->manage_instsecuritybills();
	}
	public function viewadded_topiclist(){
		$this->user_model->viewadded_topiclist();
	}
	public function delete_isptopics(){
		$this->user_model->delete_isptopics();
	}
	public function delete_advprepaid_rcpt(){
		$this->user_model->delete_advprepaid_rcpt();
	}

/*----------------- SETTING DETAILS HERE -----------------------------------------------------------------*/
	public function update_syssetting(){
		$this->user_model->update_syssetting();
	}
	public function view_syssetting(){
		$this->user_model->view_syssetting();
	}
	public function resetUserMac(){
		$this->user_model->resetUserMac();
	}
	public function ippooladdr_list(){
		echo $this->user_model->ippooladdr_list();
	}
	
	public function loadmore_content(){
		$page = $this->input->post('page');
		$param = $this->input->post('param');
		if($page == 'customerfilter'){
			$filterby = $this->input->post('netstatus');
			if($filterby == 'postfup'){
				$data = $this->user_model->postfup_userslisting();
			}else{
				$data = $this->user_model->active_inactive_userslist($param);
			}
			echo json_encode($data);
		}elseif($page == 'customerbilling'){
			$data = $this->user_model->user_billing_listing($param);
			//echo json_encode($data);
		}elseif($page == 'receiptbilling'){
			$data = $this->user_model->user_amtcredit_listing($param);
			//echo json_encode($data);
		}
	}
	
	public function editrouterpassword(){
		$this->user_model->editrouterpassword();
	}
	public function usuage_logs(){
		$this->user_model->datausuage_logs();
	}
	
	public function today_uniquesession(){
		$this->user_model->today_uniquesession();
	}
	
	public function check_sysdatabase(){
		echo $this->user_model->check_sysdatabase();
	}
	
	public function isplicense_budgetmanage(){
		$this->user_model->isplicense_budgetmanage();
	}
	
	public function data_logs(){
		$this->user_model->data_logs();
	}

/*------------------- NOTIFICATIONS ALERTS ---------------------------------------------------------------*/
	public function savenotification_alerts(){
		$this->user_model->savenotification_alerts();
	}
	public function notification_listing(){
		$this->user_model->notification_listing();
	}
	public function send_txtsms_touser(){
		$this->notify_model->isp_txtsmsgateway();
	}
	public function list_user_activitylogs(){
		$this->notify_model->list_user_activitylogs();
	}
	
/*------------------- Diagnotics -----------------------------------------------------------------------*/
	public function radius_diagnostic(){
		$user_uid = $this->input->post('cust_uuid');
		$date_range = $this->input->post('date_range');
		$data = $this->user_model->radius_diagnostic($user_uid,$date_range);
		echo $data;
	}
	public function radiuslogs_rptexport(){
		$this->user_model->radiuslogs_rptexport();
	}
	public function mikrotik_check_connection(){
		$user_uid = $this->input->post('cust_uuid');
		$router_ip = $this->input->post('router_ip');
		$router_username = $this->input->post('router_username');
		$router_password = $this->input->post('router_password');
		$this->load->library('routerlib');
		$this->router_conn = RouterOS::connect("$router_ip", "$router_username", "$router_password") or die("couldn't connectto router");
		
	}
	public function mikrotik_log_detail(){
		$user_uid = $this->input->post('cust_uuid');
		$router_ip = $this->input->post('router_ip');
		$router_username = $this->input->post('router_username');
		$router_password = $this->input->post('router_password');
		$data = $this->user_model->mikrotik_log_detail($user_uid,$router_ip,$router_username,$router_password);
		echo $data;
	}
	public function router_authlogs(){
		$this->user_model->router_authlogs();
	}


	public function check_user_onlineoffline(){
		$uuid = $this->input->post('cust_uuid');
		$onoffstatus = $this->user_model->check_user_onlineoffline($uuid);
		echo json_encode($onoffstatus);
	}
/*--------------------- FILTERS ALERTS ----------------------------------------------------------------*/

	public function exportexcel_tousers(){
		$this->load->model('Xcelpdf', 'xcelpdf');
		$this->xcelpdf->excelexport_userlisting();
	}
	public function pdfexport_userbilling(){
		$this->load->model('Xcelpdf', 'xcelpdf');
		$this->xcelpdf->pdfexport_userbilling();
	}
	
	public function excelexport_userbilling(){
		$this->load->model('Xcelpdf', 'xcelpdf');
		$this->xcelpdf->excelexport_userbilling();
	}

	public function update_expirationdate_forall(){
		$this->user_model->update_expirationdate_forall();
	}
	
	public function update_comblimit_forall(){
		$this->user_model->update_comblimit_forall();
	}
/*------------------- EXTRA FUNCTIONS ---------------------------------------------------*/
	public function assign_new_permission() {
		//$this->user_model->assign_new_permission();
	}
	public function illnaslisting(){
		$this->user_model->illnaslisting();
	}
	public function get_ipranges_Ill(){
		$this->user_model->get_ipranges_Ill();
	}
	
	public function refresh_data(){
		$data=$this->user_model->refresh_data();
		echo json_encode($data);
		
	}
}
