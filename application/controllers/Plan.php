<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Plan extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('plan_model');
        $this->load->model('user_model');
        $this->load->model('setting_model');
        if (!$this->session->has_userdata('isp_session')) {
            redirect(base_url() . 'login');
            exit;
        }
        $isp_license_data = $this->plan_model->isp_license_data(); 
	if($isp_license_data != 1){
	    redirect(base_url().'manageLicense'); exit;
	}
        $this->user_model->check_ispmodules();
    }

    public function index() {
        if (!$this->plan_model->is_permission(PLANS, 'HIDE')) {
            redirect(base_url());
        }
        $arr=$this->setting_model->get_ispdetail_info();
        $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
        $data['region_type']=$this->plan_model->dept_region_type();
        $data['plan_listing'] = $this->plan_model->listing_plan();
        $data['plan_count']=$this->plan_model->get_plan_usercount();
        $data['tax']=$this->plan_model->get_tax();
	$data['ispcodet'] = $this->plan_model->countrydetails();
        // echo "<pre>"; print_R($data); die;
        $this->load->view('plan/plan_view', $data);
    }

    public function topup() {
        if (!$this->plan_model->is_permission(TOPUP, 'HIDE')) {
            redirect(base_url());
        }
        $arr=$this->setting_model->get_ispdetail_info();
        $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
        $data['topup_listing'] = $this->plan_model->listing_topup();
        $data['topup_count']=$this->plan_model->get_topup_usercount();
        $data['tax']=$this->plan_model->get_tax();
	$data['ispcodet'] = $this->plan_model->countrydetails();
        //echo "<pre>"; print_R($data); die;
        $this->load->view('plan/topup_view', $data);
    }

    public function add_topup() {
        if (!$this->plan_model->is_permission(TOPUP, 'HIDE')) {
            redirect(base_url());
        }
        if(!$this->plan_model->is_permission(TOPUP,'ADD')){
	    redirect(base_url()."plan/topup");
	}
        $arr=$this->setting_model->get_ispdetail_info();
        $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
	$data['tax']=$this->plan_model->get_tax();
	$data['region_type']=$this->plan_model->dept_region_type();
        $data['topup_type'] = $this->plan_model->misc_data(TOPUPTYPE);
	$data['ispcodet'] = $this->plan_model->countrydetails();
      //  echo "<pre>"; print_R($data); die;
        $this->load->view('plan/add_topup', $data);
    }

    public function add_topup_data() {
        $post = $this->input->post();
        //  echo "<pre>"; print_R($_POST);die;
        $id = $this->plan_model->add_topup_detail();
        echo $id;
    }

    public function add_plan() {
        if (!$this->plan_model->is_permission(PLANS, 'HIDE')) {
            redirect(base_url());
        }  
	if(!$this->plan_model->is_permission(PLANS,'ADD')){
	    redirect(base_url()."plan");
	}
        $arr=$this->setting_model->get_ispdetail_info();
        $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
        $data['tax']=$this->plan_model->get_tax();
        $data['region_type']=$this->plan_model->dept_region_type();
        $data['plan_type'] = $this->plan_model->misc_data(PLANTYPE);
        $data['time_calcn'] = $this->plan_model->misc_data(TIMECALCULATION);
        $data['data_calcn'] = $this->plan_model->misc_data(DATACALCULATION);
	$data['ispcodet'] = $this->plan_model->countrydetails();
        $this->load->view('plan/add_plan', $data);
    }

    public function add_plan_data() {
        $id = $this->plan_model->add_plan_detail();
        echo $id;
    }

    public function add_plan_setting() {
        // echo "<pre>"; print_R($this->input->post());    die;
        $id = $this->plan_model->add_plan_setting();
        echo $id;
    }

    public function add_pricing() {
        // echo "<pre>"; print_R($this->input->post());    die;
        $id = $this->plan_model->add_pricing();
        echo $id;
    }

    public function add_topuppricing() {
        // echo "<pre>"; print_R($this->input->post());    die;
        $id = $this->plan_model->add_topuppricing();
        echo $id;
    }

    public function edit_plan($id) {
        if (!$this->plan_model->is_permission(PLANS, 'HIDE')) {
            redirect(base_url());
        }
	if($this->plan_model->check_plan_editable($id)==0){
	    $data['ro']=0;
	}
        if(!$this->plan_model->is_permission(PLANS,'EDIT')){
            if($this->plan_model->is_permission(PLANS,'RO')){
                $data['ro']=1;
            }else{
                redirect(base_url()."plan");
            }
	}
	$datarr = array();
        $datarr = $this->plan_model->plan_data($id);
        if(!$this->check_plan_ispasscoc($datarr['isp_uid']))
        { 
            redirect(base_url()."plan");
        }
        $arr=$this->setting_model->get_ispdetail_info();
        $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
        $data['tax']=$this->plan_model->get_tax();
        $data['deptregion_type']=$this->plan_model->dept_region_type();
        $data['plan'] = $datarr;
	$data['plan_region'] = $this->plan_model->plan_region_data($id);
	$data['ispcodet'] = $this->plan_model->countrydetails();
//echo "<pre>"; print_R($data);die;
        $this->load->view('plan/edit_plan', $data);
    }

    public function edit_topup($id) {
        if (!$this->plan_model->is_permission(TOPUP, 'HIDE')) {
            redirect(base_url());
        }  
        if(!$this->plan_model->is_permission(TOPUP,'EDIT')){
            if($this->plan_model->is_permission(TOPUP,'RO')){
                $data['ro']=1;
            }
            else
            {
                redirect(base_url()."plan/topup");
            }
	}
        if($this->plan_model->check_topup_editable($id)==0){
           $data['ro']=1;
	}
        $datarr = $this->plan_model->plan_topup($id);
        $data['topup'] = $datarr;
        if(!$this->check_plan_ispasscoc($datarr['isp_uid']))
        { 
            redirect(base_url()."plan/topup");    
        }
        $arr=$this->setting_model->get_ispdetail_info();
        $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
        $datarr = array();
       
        $data['tax']=$this->plan_model->get_tax();
        $data['unacctn_data'] = $this->plan_model->data_unacctn_data($id);
        $data['speedtopup_data'] = $this->plan_model->data_speedtopup_data($id);
        $data['topup_region'] = $this->plan_model->topup_region_data($id);
	$data['ispcodet'] = $this->plan_model->countrydetails();
        $this->load->view('plan/edit_topup', $data);
      
    }
    
    public function check_plan_ispasscoc($srvisp){
        $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
       //  echo $isp_uid.":::".$srvisp;
         if($isp_uid==$srvisp)
         {
            return true;
         }
         else
         {
            return false;
         }
    }

    public function getcitylist() {
        $stateid = $this->input->post('stateid');
        echo $this->plan_model->getcitylist($stateid);
    }

    public function getzonelist() {
        $cityid = $this->input->post('cityid');
        $stateid=$this->input->post('stateid');
        echo $this->plan_model->getzonelist($cityid,'',$stateid);
    }

    public function add_region() {
        $postdata = $this->input->post();
        echo $this->plan_model->add_region();
        // echo "<pre>"; print_R($postdata);
        // die;
    }

    public function add_region_topup() {
        $postdata = $this->input->post();
        echo $this->plan_model->add_region_topup();
    }

    public function delete_region_plan() {
        echo $this->plan_model->delete_region_plan();
    }

    public function delete_region_topup() {
        echo $this->plan_model->delete_region_topup();
    }

    public function viewplan_search_results() {
        $this->plan_model->viewplan_search_results();
    }

    public function viewtopup_search_results() {
        $this->plan_model->viewtopup_search_results();
    }

    public function delete_plan() {
        $id = $this->plan_model->delete_plan();
        echo json_encode($id);
    }
    
     public function delete_topup() {
        $id = $this->plan_model->delete_topup();
        echo json_encode($id);
    }

    public function check_plan_deletable() {
        $deletable = $this->plan_model->check_plan_deletable();
        echo $deletable;
    }
    
      public function check_topup_deletable() {
        $deletable = $this->plan_model->check_topup_deletable();
        echo $deletable;
    }
    
    public function changeplan_status(){
        $data=array();
        $postdata=$this->input->post();
        if($postdata['task']=="disable")
        {
            $deletable = $this->plan_model->check_plan_disable();
            $msg="Plan can't be disabled User assigned with plan";
        }
        else
        {
            $deletable = $this->plan_model->check_plan_enable();
            $msg="Plan can't be enabled Complete the full Plan";
        }
        
        if($deletable==1)
        {
           $data['status']=1;
             $data['msg']=$this->plan_model->status_approval($postdata['id']); 
        }
        else
        {
            $data['status']=0;
             $data['msg']=$msg;
        }
        echo json_encode($data);
        
    }
    
    public function changetopup_status(){
        $data=array();
        $postdata=$this->input->post();
        if($postdata['task']=="disable")
        {
            $deletable = $this->plan_model->check_topup_disable();
            $msg="Top-Up can't be disabled User assigned with Top-Up";
        }
        else
        {
            $deletable = $this->plan_model->check_topup_enable();
            $msg="Top-Up can't be enabled Complete the full Top-Up";
        }
        
        if($deletable==1)
        {
           $data['status']=1;
             $data['msg']=$this->plan_model->status_approval($postdata['id']); 
        }
        else
        {
            $data['status']=0;
             $data['msg']=$msg;
        }
        echo json_encode($data);
        
    }
    
    
    public function plan_stat($plantype = NULL){
	if($plantype == 'unlimited' || $plantype == 'data' || $plantype == 'fup' || $plantype == 'time'){
	     $data['plan_count']=$this->plan_model->get_plan_usercount();
		//$data = $this->plan_model->user_dashboard();
	      $arr=$this->setting_model->get_ispdetail_info();
	     $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
		$data['plan_listing'] = $this->plan_model->plan_list_filter($plantype);
		$data['filtertype'] = $plantype;
		$data['tax']=$this->plan_model->get_tax();
	       // echo ""
		$this->load->view('plan/planFilter',$data);
	}else{
		redirect(base_url().'plan');
	}
    }
    public function viewplan_searchfilter(){
	     $this->plan_model->viewplan_searchfilter();
	    
    }
    
    
    public function user_stat($id = NULL){
	    if(isset($id) && $id!=''){
		
	      //  echo "=====>>>".$id;
		 $arr=$this->setting_model->get_ispdetail_info();
       $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
		    $data = $this->user_model->user_dashboard();
		     $data['plan_count']=$this->plan_model->get_plan_usercount();
		    $data['active_inactive'] = $this->plan_model->active_inactive_userslist($id);
		    $data['filtertype'] = $id;
		    $data['tax']=$this->plan_model->get_tax();
		    $this->load->view('plan/userFilter',$data);
	    }else{
		    redirect(base_url().'user');
	    }
    }
    public function active_inactive_usersfilter(){
	    $id = $this->input->post('filtertype');
	    $data = $this->plan_model->active_inactive_userslist($id);
	    echo json_encode($data);
	    
    }
    
     public function topup_stat($topuptype = NULL){
	    if($topuptype == 'datatopup' || $topuptype == 'unacctncy' || $topuptype == 'speed' ){
		 $arr=$this->setting_model->get_ispdetail_info();
       $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
		 $data['topup_count']=$this->plan_model->get_topup_usercount();
		    //$data = $this->plan_model->user_dashboard();
		     $data['topup_listing'] = $this->plan_model->topup_list_filter($topuptype);
		    $data['filtertype'] = $topuptype;
		    $data['tax']=$this->plan_model->get_tax();
		   // echo ""
		    $this->load->view('plan/topupFilter',$data);
	    }else{
		    redirect(base_url().'plan/topup');
	    }
    }
    public function viewtopup_searchfilter(){
	     $this->plan_model->viewtopup_searchfilter();
	    
    }
    
	public function topupuser_stat($id = NULL){
	  //  echo "sssss"; die;
	    if(isset($id) && $id!=''){
		
		     $arr=$this->setting_model->get_ispdetail_info();
       $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
		    $data = $this->user_model->user_dashboard();
		     $data['topup_count']=$this->plan_model->get_topup_usercount();
		    $data['active_inactive'] = $this->plan_model->topupfilter_userslist($id);
		    $data['filtertype'] = $id;
		    $data['tax']=$this->plan_model->get_tax();
		   // echo "<pre>"; print_R($data); die;
		    $this->load->view('plan/usertopupFilter',$data);
	    }else{
		    redirect(base_url().'user');
	    }
    }
    public function active_inactive_topupusersfilter(){
	    $id = $this->input->post('filtertype');
	    $data = $this->plan_model->topupfilter_userslist($id);
	    echo json_encode($data);
	    
    }
    
    public function check_taxfilled(){
      $id=  $this->plan_model->check_taxfilled();
      echo $id;
    }
    
    public function syslogdata(){
    
	    $data = $this->plan_model->syslogdata();
	    echo json_encode($data);
    }
    
    public function add_syslogconn(){
	
       $data = $this->plan_model->add_syslogconn();
       echo json_encode($data);
    }
    
    public function add_inventory_category(){
	$data = $this->plan_model->add_inventory_category();
	echo json_encode($data);
    }
    
    public function add_inventory_brand(){
	$data = $this->plan_model->add_inventory_brand();
	echo json_encode($data);
    }
    
    public function brand_list(){
	$data = $this->plan_model->brand_list();
	echo json_encode($data);
    }
    
    public function product_list(){
	$data = $this->plan_model->product_list();
	echo json_encode($data);
    }
    
    public function add_inventory_product(){
	$data = $this->plan_model->add_inventory_product();
	echo json_encode($data);
    }
    
    public function add_user_inventory(){
	$data = $this->plan_model->add_user_inventory();
	echo json_encode($data);
    }
    
    public function edit_user_inventory(){
       $data = $this->plan_model->edit_user_inventory();
	echo json_encode($data);
    }
    
    public function edit_user_inventory_submit(){
	$data = $this->plan_model->edit_user_inventory_submit();
	echo json_encode($data);
    }
    
    public function delete_inventoryuser(){
	$data = $this->plan_model->delete_inventoryuser();
	echo json_encode($data);
    }
    
    public function replace_inventoryuser(){
	$data = $this->plan_model->replace_inventoryuser();
	echo json_encode($data);
    }
    
    
    public function check_configuration(){
	 $data = $this->plan_model->check_configuration();
	echo json_encode($data);
    }

    
    

    public function listing_nas(){
	$this->plan_model->listing_nas();
    }
    public function listing_nas_status(){
	$this->plan_model->nas_status();
    }
    public function plannas_configuration(){
	$this->plan_model->plannas_configuration();
    }
    public function burstmode_details(){
	$this->plan_model->burstmode_details();
    }
}

?>
