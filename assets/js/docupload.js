var idp_filecount = 0; var idp_filearr = 0; var idp_filedoctype = []; var iddocnumb = [];

/***********************************************************************************************/
$('body').on('click', '#trigger_userimage', function(){
    $( '#user_profile_image' ).trigger( 'click' );
});
$('body').on('change', '#user_profile_image', function(){
    if (this.files && this.files[0]) {
	$('#docpreview').append("<div id='user_imagediv' class='hide'><center><img class='userimg_previewimg' id='userimg_previewx' src='' style='width:70%'/></center></div>");
        
	var reader = new FileReader();
	reader.onload = profileIsLoaded;
	reader.readAsDataURL(this.files[0]);
	
	var filename = this.files[0].name;
	$('#user_image').append('<div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'+filename+' &nbsp;&nbsp;<span class="label label-default" onclick="profilepreview()"><i class="fa fa-eye" aria-hidden="true"></i></span> <span class="label label-default" onclick="profiledelete(this)"><i class="fa fa-trash" aria-hidden="true"></i></span> <span class="label label-default"><i class="fa fa-repeat" aria-hidden="true"></i></span></div></div>');
    }
});

function profileIsLoaded(e) {
    $('#userimg_previewx').attr('src', e.target.result);
}

function profilepreview() {
    $('#documentModal').modal('show');
    $('.idp_abcd').addClass('hide');
    $('.abcd').addClass('hide');
    $('#user_imagediv').removeClass('hide');
}

function profiledelete(elem) {
    $('#user_image').html('');
    $('#user_profile_image').val('');
    $('#user_imagediv').remove();
}
/*******************************************************************************************/

$('body').on('click', '#trigger_signatureimage', function(){
    $( '#user_signature_image' ).trigger( 'click' );
});
$('body').on('change', '#user_signature_image', function(){
    if (this.files && this.files[0]) {
	$('#docpreview').append("<div id='user_signdiv' class='hide'><center><img class='usersign_previewimg' id='usersign_previewx' src='' style='width:70%'/></center></div>");
        
	var reader = new FileReader();
	reader.onload = signatureIsLoaded;
	reader.readAsDataURL(this.files[0]);
	
	var filename = this.files[0].name;
	$('#user_signature').append('<div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'+filename+' &nbsp;&nbsp;<span class="label label-default" onclick="signaturepreview()"><i class="fa fa-eye" aria-hidden="true"></i></span> <span class="label label-default" onclick="signaturedelete(this)"><i class="fa fa-trash" aria-hidden="true"></i></span> <span class="label label-default"><i class="fa fa-repeat" aria-hidden="true"></i></span></div></div>');
    }
});

function signatureIsLoaded(e) {
    $('#usersign_previewx').attr('src', e.target.result);
}

function signaturepreview() {
    $('#documentModal').modal('show');
    $('.idp_abcd').addClass('hide');
    $('.abcd').addClass('hide');
    $('#user_signdiv').removeClass('hide');
}

function signaturedelete(elem) {
    $('#user_signdiv').remove();
    $('#user_signature_image').val('');
    $('#user_signature').html('');
}


/*********************************************************************************************/
$('body').on('change','.idproof_docnumber', function(){
    var docnumber = $('.idproof_docnumber').val();
    if (docnumber != '') {
	activate_iddocupload();
    }
});

function activate_iddocupload() {
    var option = $('select[name="idproof_doctype"]').val();
    var docnumber = $('.idproof_docnumber').val();
    var idproof_doctype_arr = $('#idproof_doctype_arr').val();
    
    if ((option == '') || (docnumber == '')) {
	$('.idproofdocdiv').addClass('hide');
        $('#default_iddocdiv').removeClass('hide');
    }else{
	$('#idproof_documents'+idp_filearr).parent().removeClass('hide');
	$('#default_iddocdiv').addClass('hide');
	if (idproof_doctype_arr.length == 0) {
	    idp_filearr = 1;
	    $('.idproofdocdiv').remove();
	    $('.idpfileperoption').append('<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 idproofdocdiv"><input type="file" name="idproof_documents[]" id="idproof_documents'+idp_filearr+'" accept="image/*" class="idproof_documents hide" /><span onclick="trigger_idpfile('+idp_filearr+')" class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600">CHOOSE FILE</span></div>');
	    $( '#idproof_documents'+idp_filearr ).trigger( 'click' );
	}
    }
}

var idp_abc = 0; var idp_sno = 0;
$('body').on('change', '.idproof_documents', function(){
    if (this.files && this.files[0]) {
	idp_sno += 1;
        $('#docpreview').append("<div id='idp_abcd"+ idp_abc +"' class='idp_abcd'><center><img class='idp_previewimg' id='idp_previewimg" + idp_abc + "' src='' style='width:70%'/></center></div>");
        
	
	
	var reader = new FileReader();
	reader.onload = idp_imageIsLoaded;
	reader.readAsDataURL(this.files[0]);
	var docnumb = $('.idproof_docnumber').val();
        var docname = '<i class="fa fa-newspaper-o" aria-hidden="true"></i> &nbsp;'+ $('select[name="idproof_doctype"]').find('option:selected').text();    
        var option = $('select[name="idproof_doctype"]').val();
	iddocnumb[idp_filecount] = docnumb;
        idp_filedoctype[idp_filecount] = option;
	idp_filecount += 1;
        idp_filearr += 1;
        $('#idproof_doctype_arr').val(JSON.parse(JSON.stringify(idp_filedoctype)));
	$('#idproof_docnumber_arr').val(JSON.parse(JSON.stringify(iddocnumb)));
        $('.idproofdocdiv').addClass('hide');
	
	$('#idprooflisting').append('<div class="row"><div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><label>'+docname+'</label></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">'+docnumb+'</div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="label label-default" onclick="idpdocpreview('+idp_abc+')"><i class="fa fa-eye" aria-hidden="true"></i></span> <span class="label label-default" id="'+idp_filecount+'" onclick="idpdocdelete(this)"><i class="fa fa-trash" aria-hidden="true"></i></span> <span class="label label-default"><i class="fa fa-repeat" aria-hidden="true"></i></span></div></div></div>');
	
        $('.idpfileperoption').append('<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 idproofdocdiv"><input type="file" name="idproof_documents[]" id="idproof_documents'+idp_filearr+'" accept="image/*" class="idproof_documents hide" /><span onclick="trigger_idpfile('+idp_filearr+')" class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600">CHOOSE FILE</span></div>');
	
	$('select[name="idproof_doctype"]').val('');
	$('.idproof_docnumber').val('');
	
	$('select[name="idproof_doctype"]').attr('required',false);
	$('.idproof_docnumber').attr('required',false);
    }
});

function idp_imageIsLoaded(e) {
    $('#idp_previewimg' + idp_abc).attr('src', e.target.result);
    idp_abc += 1;
}

function idpdocpreview(pid) {
    $('#documentModal').modal('show');
    $('.corp_abcd').addClass('hide');
    $('.idp_abcd').addClass('hide');
    $('.abcd').addClass('hide');
    $('#idp_abcd'+pid).removeClass('hide');
}

function idpdocdelete(elem) {
    var id = $(elem).attr("id");
    $(elem).parent().parent().remove();
    $('#idproof_documents'+id).remove();
    $('#idp_abcd'+id).remove();
    indx = id-1;
	
    idp_filedoctype.splice(indx,1);
    $('#idproof_doctype_arr').val(JSON.parse(JSON.stringify(idp_filedoctype)));
    
    iddocnumb.splice(indx,1);
    $('#idproof_docnumber_arr').val(JSON.parse(JSON.stringify(iddocnumb)));
    idp_filecount = idp_filecount - 1;
}

function trigger_idpfile(fileid) {
    $( '#idproof_documents'+fileid ).trigger( 'click' );
}