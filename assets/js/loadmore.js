function loadmore_customer(controller,method,pageinfo, param='') {
    $('.loadmore').addClass('hide');
    $('.loadmore_loader').removeClass('hide');
    var limit = $('#limit').val();
    var offset = $('#offset').val();
    
    var formdata = '';
    var searchtext = $('#searchtext').val();
    var state = $('select[name="filter_state"]').val();
    var city = $('select[name="filter_city"]').val();
    var zone = $('select[name="filter_zone"]').val();
    var locality = '';
    var netstatus = $('select[name="filter_netstatus"]').val();
    if (typeof netstatus != 'undefined') {
       netstatus = $('select[name="filter_netstatus"]').val();
    }else{
       netstatus = '';
    }
    formdata += '&state='+state+ '&city='+city+'&zone='+zone+'&locality='+locality+'&netstatus='+netstatus;
    
    $.ajax({
        url : base_url+controller+'/'+method,
        type : 'POST',
        data: 'page='+pageinfo+'&limit='+limit+'&offset='+offset+'&param='+param+formdata+'&search_user='+searchtext,
        dataType: 'json',
        success: function(result){
            $('.loadmore_loader').addClass('hide');
            var nxtlimit = result.limit;
            var nxtofset = result.offset;
            $('#limit').val(nxtlimit); $('#offset').val(nxtofset);
            $('#search_gridview').append(result['search_results']);
            if (result['loadmore'] == 1) {
               $('.loadmore').removeClass('hide');
            }else{
               $('.loadmore').addClass('hide');
            }
        }
    })
}

function loadmore_billing(controller,method,pageinfo, param='') {
    $('.bill_loadmore').addClass('hide');
    $('.bill_loadmore_loader').removeClass('hide');
    var limit = $('#bill_limit').val();
    var offset = $('#bill_offset').val();
    var cust_uuid = $('.subscriber_uuid').val();
    
    $.ajax({
        url : base_url+controller+'/'+method,
        type : 'POST',
        data: 'page='+pageinfo+'&limit='+limit+'&offset='+offset+'&param='+param+'&subscriber_uuid='+cust_uuid,
        dataType: 'json',
        success: function(result){
            $('.bill_loadmore_loader').addClass('hide');
            var nxtlimit = result.limit;
            var nxtofset = result.offset;
            $('#bill_limit').val(nxtlimit); $('#bill_offset').val(nxtofset);
            $('#debitbill_summary').append(result.debitbill_listing);
            if (result.loadmore == 1) {
               $('.bill_loadmore').removeClass('hide');
            }else{
               $('.bill_loadmore').addClass('hide');
            }
        }
    })
}

function loadmore_rcptbilling(controller,method,pageinfo, param='') {
    $('.rcptbill_loadmore').addClass('hide');
    $('.rcptbill_loadmore_loader').removeClass('hide');
    var limit = $('#rcptbill_limit').val();
    var offset = $('#rcptbill_offset').val();
    var cust_uuid = $('.subscriber_uuid').val();
    
    $.ajax({
        url : base_url+controller+'/'+method,
        type : 'POST',
        data: 'page='+pageinfo+'&limit='+limit+'&offset='+offset+'&param='+param+'&subscriber_uuid='+cust_uuid,
        dataType: 'json',
        success: function(result){
            $('.rcptbill_loadmore_loader').addClass('hide');
            var nxtlimit = result.limit;
            var nxtofset = result.offset;
            $('#rcptbill_limit').val(nxtlimit); $('#rcptbill_offset').val(nxtofset);
            $('#creditbill_summary').append(result.creditbill_listing);
            if (result.loadmore == 1) {
               $('.rcptbill_loadmore').removeClass('hide');
            }else{
               $('.rcptbill_loadmore').addClass('hide');
            }
        }
    })
}
