 $(document).ready(function(){
    
        $('input:radio[name="contype"]').change(function(){
            $('.loader').removeClass('hide');
            var ip= $('#hdnip').val();
             var username=  $('#hdnusername').val();
             var password=  $('#hdnpwd').val();
                                       
	  if ($(this).val()=="vlan") {
            
                 $.ajax({
		     type: "POST",
		     url: base_url+"ppoe/get_etherlist",
		     data: "ip="+ip+"&username="+username+"&password="+password,
                       dataType: 'json',
		     success: function(data){
                            if (data.resultcode==1) {
                                   $('#intrflist').html(data.html);
                                 $('.radiovlan').css('display','none');
                                   $('.vlandet').css('display','block');
                                   $('.ethrnetdet').css('display','none');
                                 
                            }
                            else{
                                  $('.radiovlan').css('display','block');
                                  $('.ethrnetdet').css('display','none');
                                   $('.vlandet').css('display','none');
                                   $('#vlanerr').html('No Interface found');
                            }
                            $('.loader').addClass('hide');
			
			
		     }
	       });
            
	  
            
	  }
          
          else
          {
                  $.ajax({
		     type: "POST",
		     url: base_url+"ppoe/get_etherlist",
		     data: "ip="+ip+"&username="+username+"&password="+password,
                       dataType: 'json',
		     success: function(data){
                            if (data.resultcode==1) {
                                   $('#ethrlist').html(data.html);
                                   $('.ethrnetdet').css('display','block');
                                 $('.radiovlan').css('display','none');
                                   $('.vlandet').css('display','none');    
                            }
                            else{
                                  $('.radiovlan').css('display','none');
                                   $('.ethrnetdet').css('display','block');
                                   $('.vlandet').css('display','none');
                                   $('#etherr').html('No Interface found');
                            }
                             $('.loader').addClass('hide');
			
			
		     }
	       });
          }
	});
        
        
        
        $(document).on('change','input:radio[name="interfaceether"]',function(){
        // $('input:radio[name="interfaceether"]').change(function(){
          $('.loader').removeClass('hide');
             var vlanid=$('#vlanid').val();
      if (vlanid=="") {
        $('.errorvlanid').html('please enter Vlan Id');
         $('.loader').addClass('hide');
        return false;
      }
        var ip = $("#hdnip").val();
        var username = $("#hdnusername").val();
        var password = $("#hdnpwd").val();
      var ether=$("input[name='interfaceether']:checked"). val();
       $.ajax({
		     type: "POST",
		     url: base_url+"ppoe/checkvlanid",
		     data: "ip="+ip+"&username="+username+"&password="+password+"&vlanid="+vlanid+"&ether="+ether,
                     dataType: 'json',
		     success: function(data){
			if(data.resultcode==0)
                        {
                            $('.errorvlanid').html('VlanID already mapped');
                              $('.setup').prop('disabled',true);
                        }
                        else
                        {
                            if (data.resultcode==2) {
                                  $('.errorvlanid').html('router Connection not established');
                                    $('.setup').prop('disabled',true);
                            }
                            else
                            {
                                $('.errorvlanid').html('');   
                            }
                            
                        }
                          $('.loader').addClass('hide');
			
		     }
	       });
	});
        
        
        $(document).on('click','.setup',function(){
                $('.loader').removeClass('hide');
            var vlanname=$('#vlan').val();
            var vlanid=$('#vlanid').val();
            var type=$(this).attr('rel');
              var ether=$("input[name='interfaceether']:checked"). val();
              $('#hdninterface').val(ether);
              $('#hdnvlanid').val(vlanid);
              $('#hdnseltype').val(type);
              $('#hdnvlanname').val(vlanname);
               $('#successsetup').html('Vlan Added Successfully');
       var ip = $("#hdnip").val();
        var username = $("#hdnusername").val();
        var password = $("#hdnpwd").val();
       
        $.ajax({
		     type: "POST",
		     url: base_url+"ppoe/ppoe_listdata",
		     data: "ip="+ip+"&username="+username+"&password="+password,
                     dataType: 'json',
		     success: function(data){
			if (data.resultcode==1) {
                            $('.ppoelist').css('display','block');
                             $('#ppoelistd').html(data.html);
                             $('.ppoeerr').html("");
                             $('.vlandet').css('display','none');
                             $('.ethrnetdet').css('display','none');
                             
                            
                        }
                        else{
                            $('.ppoelist').css('display','block');
                            ('.vlandet').css('display','none');
                             $('.ethrnetdet').css('display','none');
                          
                           $('.ppoeerr').html('Unable to get Iplist');
                            
                        }
			  $('.loader').addClass('hide');
		     }
	       });
              
        });
        
        $(document).on('click','.nextppoe',function(){
                $('.loader').removeClass('hide');
             var ppoeip=$("input[name='ppoeip']:checked"). val();
              var items = ppoeip.split('/');
              $('#hdnpublicip').val(items[0]);
              $('.iprangecust').css('display','block');
              $('.ppoelist').css('display','none');
                $('.loader').addClass('hide');
              
        })
        
        
        
        $(document).on("click",'toggle',function(){
              var trgtclass=$(this).attr('rel');
             // alert(trgtclass);
              $('.'+trgtclass).css('display','block');
              $(this).parent().css('display','none');
              
              
              });
        
        
        $(document).on('click','.nextcustip',function(){
              $('.loader').removeClass('hide');
              var ip = $("#hdnip").val();
        var username = $("#hdnusername").val();
        var password = $("#hdnpwd").val();
        var vlanname= $('#hdnvlanname').val();
        var interf= $('#hdninterface').val();
        var vlanid=$('#hdnvlanid').val();
        var hdnseltype=$('#hdnseltype').val();
        var publicppoe=$('#hdnpublicip').val();
        var iprange=$('#rangeip').val();
        var connid=$('#hdnconnid').val();
        
         $.ajax({
		     type: "POST",
		     url: base_url+"ppoe/execute_cmnd",
		     data: {ip:ip,username:username,password:password,vlanname:vlanname,interf:interf,vlanid:vlanid,hdnseltype:hdnseltype,publicppoe:publicppoe,iprange:iprange,connid:connid},
                     dataType: 'json',
		     success: function(data){
			//alert(data);
                        if (data.resultcode==0) {
                           $('.rangeiperr').html(data.resultmsg);
                           $('.successsetop').html('');
                        }
                        else{
                            $('.rangeiperr').html('');
                            $('.successsetop').html(data.resultmsg);
                            $('.nextcustip').prop('disabled',false);
                            $('.noofseesion').css('display','block');
                            $('.iprangecust').css('display','none');
                        }
                         $('.loader').addClass('hide');
		     }
	       });
        
              
       })
        
        
        $(document).on('click','.sessioncustip',function(){
               $('.loader').removeClass('hide');
              var ip = $("#hdnip").val();
        var username = $("#hdnusername").val();
        var password = $("#hdnpwd").val();
        var vlanname= $('#hdnvlanname').val();
        var interf= $('#hdninterface').val();
        var vlanid=$('#hdnvlanid').val();
        var hdnseltype=$('#hdnseltype').val();
        var publicppoe=$('#hdnpublicip').val();
        var iprange=$('#rangeip').val();
        var connid=$('#hdnconnid').val();
        var session_num=$('#nosession').val();
        
         $.ajax({
		     type: "POST",
		     url: base_url+"ppoe/execute_cmnd_ppp",
		     data: {ip:ip,username:username,password:password,vlanname:vlanname,interf:interf,vlanid:vlanid,hdnseltype:hdnseltype,publicppoe:publicppoe,iprange:iprange,connid:connid,session_num:session_num},
                     dataType: 'json',
		     success: function(data){
			//alert(data);
                        if (data.resultcode==0) {
                           $('.sessionerr').html(data.resultmsg);
                           $('.suucessset').html('');
                        }
                        else{
                            $('.sessionerr').html('');
                            $('.suucessset').html(data.resultmsg);
                            $('.sessioncustip').prop('disabled',true);
                            //$('.noofseesion').css('display','block');
                            //$('.iprangecust').css('display','none');
                        }
                        $('.loader').addClass('hide');
		     }
	       });
        
              
       })
    
    });
 
 
 function check_vlanid() {
         $('.loader').removeClass('hide');
         $('.setup').prop('disabled',false);
      var vlanid=$('#vlanid').val();
      if (vlanid=="") {
        $('.errorvlanid').html('please enter Vlan Id');
         $('.loader').addClass('hide');
        return false;
      }
        var ip = $("#hdnip").val();
        var username = $("#hdnusername").val();
        var password = $("#hdnpwd").val();
      var ether=$("input[name='interfaceether']:checked"). val();
       $.ajax({
		     type: "POST",
		     url: base_url+"ppoe/checkvlanid",
		     data: "ip="+ip+"&username="+username+"&password="+password+"&vlanid="+vlanid+"&ether="+ether,
                     dataType: 'json',
		     success: function(data){
			if(data.resultcode==0)
                        {
                            $('.errorvlanid').html('VlanID already mapped');
                              $('.setup').prop('disabled',true);
                        }
                        else
                        {
                            if (data.resultcode==2) {
                                  $('.errorvlanid').html('router Connection not established');
                                    $('.setup').prop('disabled',true);
                            }
                            else
                            {
                                $('.errorvlanid').html('');   
                            }
                            
                        }
                          $('.loader').addClass('hide');
			
		     }
	       });
      
      
 }
 
 
  function checkvlanname() {
         $('.loader').removeClass('hide');
         $('.setup').prop('disabled',false);
      var vlanname=$('#vlan').val();
      if (vlanid=="") {
        $('.errorvlan').html('please enter Vlan Id');
         $('.loader').addClass('hide');
        return false;
      }
        var ip = $("#hdnip").val();
        var username = $("#hdnusername").val();
        var password = $("#hdnpwd").val();
      var ether=$("input[name='interfaceether']:checked"). val();
       $.ajax({
		     type: "POST",
		     url: base_url+"ppoe/checkvlanname",
		     data: "ip="+ip+"&username="+username+"&password="+password+"&vlanname="+vlanname+"&ether="+ether,
                     dataType: 'json',
		     success: function(data){
			if(data.resultcode==0)
                        {
                            $('.errorvlan').html('Vlan already exist');
                            $('.setup').prop('disabled',true);
                        }
                        else
                        {
                            if (data.resultcode==2) {
                                  $('.errorvlan').html('router Connection not established');
                                  $('.setup').prop('disabled',true);
                            }
                            else
                            {
                                $('.errorvlan').html('');
                                $('.setup').prop('disabled',false);
                            }
                            
                        }
                          $('.loader').addClass('hide');
			
		     }
	       });
      
      
 }
 
 
 function check_nassetup() {
         $('.loader').removeClass('hide');
         $('.lgin').prop('disabled',false);
      var ip=$('#ip').val();
  
       $.ajax({
		     type: "POST",
		     url: base_url+"ppoe/check_nassetup",
		     data: "ip="+ip,
                     dataType: 'json',
		     success: function(data){
			if(data.resultcode==0)
                        {
                            $('.naserr').html('Please configure the nas');
                            $('.lgin').prop('disabled',true);
                        }
                        else
                        {
                             $('.naserr').html('');
                            $('.lgin').prop('disabled',false);
                            
                        }
                          $('.loader').addClass('hide');
			
		     }
	       });
      
      
 }
 
 function validate_iprange() {
       $('.loader').removeClass('hide');
      var iprange=$('#rangeip').val();
      if (iprange == '') {
          $(".rangeiperr").html("please Enter IP Range");
           $('.loader').addClass('hide');
        }
        else{
              if (/^((\b|\.)(1|2(?!5(?=6|7|8|9)|6|7|8|9))?\d{1,2}){4}(-((\b|\.)(1|2(?!5(?=6|7|8|9)|6|7|8|9))?\d{1,2}){4}|\/((1|2|3(?=1|2))\d|\d))\b$/.test(iprange)){
              $(".rangeiperr").html("");
              $('.nextcustip').prop('disabled',false);
              $('.loader').addClass('hide');
              }
              else{
                  $(".rangeiperr").html("please Enter  valid IP Range");
                  $('.nextcustip').prop('disabled',true);
                  $('.loader').addClass('hide');
              }
        }
      
      
 }
 
 function submit_form() {
         $('.loader').removeClass('hide');
        var ip = $("#ip").val();
        var username = $("#username").val();
        var password = $("#password").val();
        if (ip == '' || username == '' ) {
          $("#login_error").html("please fill Ip and Username fields");
           $('.loader').addClass('hide');
        }else{
          if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip)){
           
              // ajax
              $.ajax({
		     type: "POST",
		     url: base_url+"ppoe/add_connection",
		     data: "ip="+ip+"&username="+username+"&password="+password+"&type=postdata",
                     dataType: 'json',
		     success: function(data){
				// alert(data);
				 if(data.resultcode==1)
				 {
					 //window.location = 'index.php';
                                         $('#login_error').html('');
                                        $("#success").html("Connection successful!!!");
                                         $('#hdnip').val(ip);
                                        $('#hdnusername').val(username);
                                        $('#hdnpwd').val(password);
                                        $('#hdnconnid').val(data.connid);
                                        $('.radiovlan').css('display','block');
                                        $('.mtconnection').css('display','none');
                                 }
				 else{
					  $("#login_error").html("Please check credential");
                                           $("#success").html("");
				 }
                                   $('.loader').addClass('hide');
			//window.location = 'welcome.php';
			
		     }
	       });
           
          }else{
            $("#login_error").html("Please enter valid ip");
             $('#hdnip').val('');
                                        $('#hdnusername').val('');
                                        $('#hdnpwd').val('');
                                         $('.loader').addClass('hide');
          }
        }
      }
      
      
    