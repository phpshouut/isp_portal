var corp_filecount = 0; var corp_filearr = 0; var corp_filedoctype = []; var corpdocnumb = [];

/*********************************************************************************************/
$('body').on('change','.corporate_docnumber', function(){
    var docnumber = $('.corporate_docnumber').val();
    if (docnumber != '') {
	activate_corpdocupload();
    }
});

function activate_corpdocupload() {
    var option = $('select[name="corporate_doctype"]').val();
    var docnumber = $('.corporate_docnumber').val();
    var corporate_doctype_arr = $('#corporate_doctype_arr').val();
    
    if ((option == '') || (docnumber == '')) {
	$('.corpproofdocdiv').addClass('hide');
        $('#default_corporatedocdiv').removeClass('hide');
    }else{
	$('#corpproof_documents'+corp_filearr).parent().removeClass('hide');
	$('#default_corporatedocdiv').addClass('hide');
	if (corporate_doctype_arr.length == 0) {
	    corp_filearr = 1;
	    $('.corpproofdocdiv').remove();
	    $('.corpfileperoption').append('<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 corpproofdocdiv"><input type="file" name="corpproof_documents[]" id="corpproof_documents'+corp_filearr+'" accept="image/*" class="corpproof_documents hide" /><span onclick="trigger_corpfile('+corp_filearr+')" class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600">CHOOSE FILE</span></div>');
	    $( '#corpproof_documents'+corp_filearr ).trigger( 'click' );
	}
    }
}

var corp_abc = 0; var corp_sno = 0;
$('body').on('change', '.corpproof_documents', function(){
    if (this.files && this.files[0]) {
	corp_sno += 1;
        $('#docpreview').append("<div id='corp_abcd"+ corp_abc +"' class='corp_abcd'><center><img class='corp_previewimg' id='corp_previewimg" + corp_abc + "' src='' style='width:70%'/></center></div>");
        
	
	
	var reader = new FileReader();
	reader.onload = corp_imageIsLoaded;
	reader.readAsDataURL(this.files[0]);
	var docnumb = $('.corporate_docnumber').val();
        var docname = '<i class="fa fa-newspaper-o" aria-hidden="true"></i> &nbsp;'+ $('select[name="corporate_doctype"]').find('option:selected').text();    
        var option = $('select[name="corporate_doctype"]').val();
	corpdocnumb[corp_filecount] = docnumb;
        corp_filedoctype[corp_filecount] = option;
	corp_filecount += 1;
        corp_filearr += 1;
        $('#corporate_doctype_arr').val(JSON.parse(JSON.stringify(corp_filedoctype)));
	$('#corporate_docnumber_arr').val(JSON.parse(JSON.stringify(corpdocnumb)));
        $('.corpproofdocdiv').addClass('hide');
	
	$('#corporatelisting').append('<div class="row"><div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><label>'+docname+'</label></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">'+docnumb+'</div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="label label-default" onclick="corpdocpreview('+corp_abc+')"><i class="fa fa-eye" aria-hidden="true"></i></span> <span class="label label-default" id="'+corp_filecount+'" onclick="corpdocdelete(this)"><i class="fa fa-trash" aria-hidden="true"></i></span> <span class="label label-default"><i class="fa fa-repeat" aria-hidden="true"></i></span></div></div></div>');
	
        $('.corpfileperoption').append('<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 corpproofdocdiv"><input type="file" name="corpproof_documents[]" id="corpproof_documents'+corp_filearr+'" accept="image/*" class="corpproof_documents hide" /><span onclick="trigger_corpfile('+corp_filearr+')" class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600">CHOOSE FILE</span></div>');
	
	$('select[name="corporate_doctype"]').val('');
	$('.corporate_docnumber').val('');
	
	$('select[name="corporate_doctype"]').attr('required',false);
	$('.corporate_docnumber').attr('required',false);
    }
});

function corp_imageIsLoaded(e) {
    $('#corp_previewimg' + corp_abc).attr('src', e.target.result);
    corp_abc += 1;
}

function corpdocpreview(pid) {
    $('#documentModal').modal('show');
    $('.corp_abcd').addClass('hide');
    $('.idp_abcd').addClass('hide');
    $('.abcd').addClass('hide');
    $('#corp_abcd'+pid).removeClass('hide');
}

function corpdocdelete(elem) {
    var id = $(elem).attr("id");
    $(elem).parent().parent().remove();
    $('#corpproof_documents'+id).remove();
    $('#corp_abcd'+id).remove();
    indx = id-1;
	
    corp_filedoctype.splice(indx,1);
    $('#corporate_doctype_arr').val(JSON.parse(JSON.stringify(corp_filedoctype)));
    
    corpdocnumb.splice(indx,1);
    $('#corporate_docnumber_arr').val(JSON.parse(JSON.stringify(corpdocnumb)));
    corp_filecount = corp_filecount - 1;
}

function trigger_corpfile(fileid) {
    $( '#corpproof_documents'+fileid ).trigger( 'click' );
}