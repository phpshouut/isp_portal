function setup_adduser() {
    $('.loading').removeClass('hide');
    $('#billerrmsg').html('');
    $.ajax({
        url: base_url+'user/setupfor_adduser',
        type: 'POST',
        success: function(data){
            $('.loading').addClass('hide');
            if (data == 0){
                $('#billerrmsg').html('Please setup the Billing Cycle before creating User');
                $('#billingerr').modal('show');
            }else{
                window.location = base_url+'user/add';
            }
        }
    });
}

function check_tax_plan(){
    $('.loading').removeClass('hide');
    $('#taxmsg').html('');
    var id=1;
    $.ajax({
        type: "POST",
        url: base_url+"user/check_taxfilled",
        cache: false,
        data: "id="+id,
        success: function(data){
            $('.loading').addClass('hide');
            if(data==0){
                $('#taxmsg').html('Please enter Tax before creating Plan');
                $('#taxerr').modal('show');
            }else{
                window.location = base_url+'plan/add_plan';
            }
        }
    });
}

function check_tax_topup(){
    $('#taxmsg').html('');
    $('.loading').removeClass('hide');
    var id=1;
    $.ajax({
        type: "POST",
        url: base_url+"user/check_taxfilled",
        cache: false,
        data: "id="+id,
        success: function(data){
            $('.loading').addClass('hide');
            if(data==0){
                $('#taxmsg').html('Please enter Tax before creating Topup');
                $('#taxerr').modal('show');
            }else{
                window.location = base_url+'plan/add_topup';
            }
        }
    });
}



$('body').on('click', '#editclick_personal_details', function(){
    var idclass = $(this).is('[disabled=disabled]');
    if(idclass !== true){
        personaldetails_listing();
    }
});
$('body').on('click', '#editclick_kyc_details', function(){
    var idclass = $(this).is('[disabled=disabled]');
    if(idclass !== true){
        $('#subscriber_documents_form')[0].reset();
        $('#default_iddocdiv').removeClass('hide');
        $('.idproofdocdiv ').remove();
        $('#idprooflisting').html('');
        $('#defaultdocdiv').removeClass('hide');
        $('.subsdocdiv').remove();
        $('#kyclisting').html('');
        $('#docpreview').html('');
        $('.idproof_docnumber').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched');
        $('.kycdocnumber').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched');
        $('#user_image').html('');
        $('#user_signature').html('');
        
        $('#default_corporatedocdiv').removeClass('hide');
        $('.corpproofdocdiv ').remove();
        $('#corporatelisting').html('');
        
        $('#idproof_docnumber_arr').val('');
        $('#idproof_doctype_arr').val('');
        $('#kyc_documents_arr').val('');
        $('#kycdocnumber_arr').val('');
        $('#corporate_docnumber_arr').val('');
        $('#corporate_doctype_arr').val('');
        
        filecount = 0; filearr = 0; filedoctype = []; kycdocnumb = []; abc = 0; sno = 0;
        idp_filecount = 0; idp_filearr = 0; idp_filedoctype = []; iddocnumb = []; idp_abc = 0; idp_sno = 0;
        corp_filecount = 0; corp_filearr = 0; corp_filedoctype = []; corpdocnumb = []; corp_abc = 0; corp_sno = 0;
        
        edit_allkyc_details();
    }
});
$('body').on('click', '#editclick_plan_details', function(){
    var idclass = $(this).is('[disabled=disabled]');
    if(idclass !== true){
        $('#active_plan').css('color', '#424143');
        $('#next_cycle_plan').css('color', '#29abe2');
        $('#subscriber_plandetail_form').removeClass('hide');
        $('#nextcycle_planform').addClass('hide');
        $('.custom_planid').val('');
        $('.custom_planprice').val('');
        subscriber_plan_details();
    }
});


/********************* PLAN TABBING ************************/
$('body').on('click', '.plantabb', function(){
    var tabclick = $(this).attr('id');
    if (tabclick == 'active_plan') {
        $(this).css('color', '#424143');
        $('#next_cycle_plan').css('color', '#29abe2');
        $('#subscriber_plandetail_form').removeClass('hide');
        $('#nextcycle_planform').addClass('hide');
        subscriber_plan_details();
    }else if (tabclick == 'next_cycle_plan') {
        $(this).css('color', '#424143');
        $('#active_plan').css('color', '#29abe2');
        $('#subscriber_plandetail_form').addClass('hide');
        $('#nextcycle_planform').removeClass('hide');
        $('.custom_planid').val('');
        $('.custom_planprice').val('');
        nextcycle_planlist();
    }
});


function getcitylist(stateid, subscriber='', cityid='', bstate='') {
    if (bstate != '') {
	$('.bzonelist').empty().append('<option value="">Zone*</option>');
    }else{
	$('.zonelist').empty().append('<option value="">Zone*</option>');
    }
    $('.search_zonelist').empty().append('<option value="">All Zones</option>');
    var pin = '';
    if (subscriber != '') {
        pin = $( "#state_customer option:selected" ).attr('data-pincode');
    }
    $.ajax({
        url: base_url+'user/getcitylist',
        type: 'POST',
        dataType: 'text',
	async: false,
        data: 'stateid='+stateid+'&pinid='+pin+'&cityid='+cityid,
        success: function(dataa){
            if (dataa.indexOf("~~~") != -1 ) {
                var dataarr = dataa.split('~~~');
                if (subscriber != '') {
                    $('input[name="subscriber_uuid"]').val(dataarr[0]);
                    $('input[name="subscriber_username"]').val(dataarr[0]);
                }
		if (bstate != '') {
		    $('.bcitylist').empty().append('<option value="">City*</option>');
		    $('.bcitylist').append(dataarr[1]);
		}else{
		    $('.citylist').empty().append('<option value="">City*</option>');
		    $('.citylist').append(dataarr[1]);
		}
                $('.search_citylist').empty().append('<option value="">All Cities</option>');
                $('.search_citylist').append(dataarr[1]);
                $('.search_citylist option[value="addc"]').remove();
            }else{
		if (bstate != '') {
		    $('.bcitylist').empty().append('<option value="">City*</option>');
		    $('.bcitylist').append(dataa);
		}else{
		    $('.citylist').empty().append('<option value="">City*</option>');
		    $('.citylist').append(dataa);
		}
                
                $('.search_citylist').empty().append('<option value="">All Cities</option>');
                $('.search_citylist').append(dataa);
                $('.search_citylist option[value="addc"]').remove();
            }
        }
    });
}

function getzonelist(cityid, subscriber='',  zoneid='', bcity='') {
    if (subscriber != '') {
	var state = $('#state_customer option:selected').val();
    }else{
        var state = $('#state_leadenquiry option:selected').val();
    }
    if (cityid == 'addc') {
        $('input[name="add_city_state"]').val(state);
        $('.city_text').val('');
        $('#add_cityModal').modal('show');
    }else{
        $.ajax({
            url: base_url+'user/getzonelist',
            type: 'POST',
            dataType: 'text',
	    async: false,
            data: 'stateid='+state+'&cityid='+cityid+'&zoneid='+zoneid,
            success: function(data){
		if (bcity != '') {
		    $('.bzonelist').empty().append('<option value="">Zone*</option>');
		    $('.bzonelist').append(data);
		}else{
		    $('.zonelist').empty().append('<option value="">Zone*</option>');
		    $('.zonelist').append(data);
		}
                $('.search_zonelist').empty().append('<option value="">All Zones</option>');
                $('.search_zonelist').append(data);
                $('.search_zonelist option[value="addz"]').remove();
            }
        });
    }
}

function check_toadd_newzone(zoneid, type) {
    if (type == 'leadenquiry') {
        var state = $('#state_leadenquiry option:selected').val();
        var city = $('#city_leadenquiry option:selected').val();    
    }else{
        var state = $('#state_customer option:selected').val();
        var city = $('#city_customer option:selected').val();
    }
    
    //alert(state+'@@@@'+city);
    if (zoneid == 'addz') {
        $('.zone_text').val('');
        $('input[name="state_id"]').val(state);
        $('input[name="add_zone_city"]').val(city);
        $('#add_zoneModal').modal('show');
    }
}

function add_newcity() {
    var formdata = $('#addcity_fly').serialize();
    $.ajax({
        url: base_url+'user/add_newcity',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
            $('input[name="add_city_state"]').val('');
            $('#add_cityModal').modal('hide');
            var addoption = data.html;
            //alert('"' + addoption + '"');
            //$(".citylist option:last").before('"' + addoption + '"');
            $(".citylist").append('"' + addoption + '"');
            $(".zonelist").append('<option class="addzonefly" value="addz">--Add Zone--</option>');
        }
    })
}

function add_newzone() {
    var formdata = $('#addzone_fly').serialize();
    $.ajax({
        url: base_url+'user/add_newzone',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
            $('input[name="state_id"]').val('');
            $('input[name="add_zone_city"]').val('');
            $('#add_zoneModal').modal('hide');
            var addoption = data.html;
            //$(".zonelist option:last").before('"' + addoption + '"');
            $(".zonelist").append('"' + addoption + '"');
        }
    })
}

function resetcityvalue() {
    $('.citylist').val('');
}
function resetzonevalue() {
    $('.zonelist').val('');
}

function add_customer(event) {
    $('.loading').removeClass('hide');
    var formdata = $('#user_type_customer').serialize();
    $.ajax({
        url: base_url+'user/add_customer',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
            //alert(data);
            var subsid = data.customer_id;
            var letoc_dataid = $('input[name="letoc_dataid"]').val();
            //alert('le='+letoc_dataid);
            if ((typeof letoc_dataid != 'undefined') && (letoc_dataid != '0')) {
                window.location = base_url+'user/edit/'+subsid;
            }else{
                $('.subscriber_userid').val(data.customer_id);
                $('.tab_menu').css({'cursor' : 'pointer'});
                $('.tab_menu').removeAttr('disabled');
                $('#usertype_customer').attr('checked','checked');
                $('.radio-inline').css('color', '#b3b3b3');
                $('input[type="radio"][name="user_type_radio"]').attr('disabled','disabled');
                
                $('input[name="customer_uuid"]').val(data.customer_uuid);
                $('.subscriber_uuid').val(data.customer_uuid);
                $('.customer_name').html(data.customer_name);
                $('.customer_email').html(data.customer_email);
                $('.customer_mobile').html('+91 '+data.customer_mobile);
                
                $('.loading').addClass('hide');
                $('.active_addc').removeClass('hide');
                window.scrollTo(0,0);
                mui.tabs.activate('KYC_details');
            }
        }
    })
}

function add_subscriber_documents(event) {
    $('.loading').removeClass('hide');
    var fd = new FormData();
    var atleast_onefile = 0; var atleast_oneidpfile = 0; var atleast_onecorpfile = 0;
    var totalfile = $('input[name="subscriber_documents[]"]').length;
    if (totalfile > 0) {
        atleast_onefile = $('input[name^="subscriber_documents"]')[0].files.length;
    }
    
    var total_idpfile = $('input[name="idproof_documents[]"]').length;
    if (total_idpfile > 0) {
        atleast_oneidpfile = $('input[name^="idproof_documents"]')[0].files.length;       
    }
    
    var total_corpfile = $('input[name="corpproof_documents[]"]').length;
    if (total_corpfile > 0) {
        atleast_onecorpfile = $('input[name^="corpproof_documents"]')[0].files.length;       
    }
    
    var user_profile_image = $('input[name="user_profile_image"]')[0].files[0];
    var user_signature_image = $('input[name="user_signature_image"]')[0].files[0];
    
    //alert(totalfile +'#######'+ total_idpfile);
    //alert(atleast_onefile +'@@@@@@@'+ atleast_oneidpfile);
    //alert(user_profile_image);
    if ((atleast_onefile == 0) && (atleast_oneidpfile == 0)) {
        $('.loading').addClass('hide');
        $('.subsdocdiv').addClass('hide');
        $('#defaultdocdiv').removeClass('hide');
        $('.idproofdocdiv').addClass('hide');
        $('#default_iddocdiv').removeClass('hide');
        alert('Please upload all related documents.');
    }else if (atleast_onefile == 0) {
        $('.loading').addClass('hide');
        //$('input[name^="subscriber_documents"]').remove();
        //$('[id^=abcd]').remove();
        //$('select[name="kyc_documents"]').val('');
        $('.subsdocdiv').addClass('hide');
        $('#defaultdocdiv').removeClass('hide');
        alert('Please upload atleast one KYC document.');
    }else if (atleast_oneidpfile == 0) {
        $('.loading').addClass('hide');
        //$('input[name^="idproof_documents"]').remove();
        //$('[id^=abcd]').remove();
        //$('select[name="kyc_documents"]').val('');
        $('.idproofdocdiv').addClass('hide');
        $('#default_iddocdiv').removeClass('hide');
        alert('Please upload atleast one PHOTO ID document.');
    }
    else if(typeof user_profile_image == 'undefined'){
        $('.loading').addClass('hide');
        alert('Please upload user image.');
    }else{
        //alert('ok');
        for (var j=0; j< total_idpfile; j++) {
            $.each($('input[name^="idproof_documents"]')[j].files, function(l, file) {
                fd.append(j, file);
            });
        }

        j = j-1;
        for (var k=0; k< totalfile; k++) {
            $.each($('input[name^="subscriber_documents"]')[k].files, function(l, ifile) {
                fd.append(j, ifile);
                j++;
            });
        }

        if (atleast_onecorpfile > 0) {
            for (var c=0; c< total_corpfile; c++) {
                $.each($('input[name^="corpproof_documents"]')[c].files, function(l, cfile) {
                    fd.append(j, cfile);
                    j++;
                });
            }
        }
        if (typeof user_profile_image != 'undefined') {
            fd.append('profile_image', user_profile_image);
            j++;
        }
        if (typeof user_signature_image != 'undefined') {
            fd.append('signature', user_signature_image);
            j++;
        }
        
        
        var other_data = $('#subscriber_documents_form').serializeArray();
        $.each(other_data,function(key,input){
            fd.append(input.name,input.value);
        });
        
        $.ajax({
            url: base_url+'user/upload_documents',
            data: fd,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
                $('.loading').addClass('hide');
                //alert(data);
                //$('.subscriber_userid').val(data);
                $('#usertype_customer').attr('checked','checked');
                $('.radio-inline').css('color', '#b3b3b3');
                $('input[type="radio"][name="user_type_radio"]').attr('disabled','disabled');
                
                //$('select[name="kyc_documents"]').val('');
                //$('input[name^="subscriber_documents"]').remove();
                $('.subsdocdiv').addClass('hide');
                $('#defaultdocdiv').removeClass('hide');
                $('.idproofdocdiv').addClass('hide');
                $('#default_iddocdiv').removeClass('hide');
                $('.corpproofdocdiv').addClass('hide');
                $('#default_corporatedocdiv').removeClass('hide');
                
                $('#doclisting').html('');
                $('.loading').addClass('hide');
                subscriber_plan_details();
                //mui.tabs.activate('Plan_details');
                
            }
        });
    }
}

function edit_subscriber_documents(event) {
    var fd = new FormData();
    var atleast_onefile = 0; var atleast_oneidpfile = 0; var atleast_onecorpfile = 0;
    var totalfile = $('input[name="subscriber_documents[]"]').length;
    if (totalfile > 0) {
        atleast_onefile = $('input[name^="subscriber_documents"]')[0].files.length;
    }
    
    var total_idpfile = $('input[name="idproof_documents[]"]').length;
    if (total_idpfile > 0) {
        atleast_oneidpfile = $('input[name^="idproof_documents"]')[0].files.length;       
    }
    
    var total_corpfile = $('input[name="corpproof_documents[]"]').length;
    if (total_corpfile > 0) {
        atleast_onecorpfile = $('input[name^="corpproof_documents"]')[0].files.length;       
    }
    
    var user_profile_image = $('input[name="user_profile_image"]')[0].files[0];
    var user_signature_image = $('input[name="user_signature_image"]')[0].files[0];
    
    //alert(totalfile +'#######'+ total_idpfile);
    //alert(atleast_onefile +'@@@@@@@'+ atleast_oneidpfile);
    //alert(user_profile_image);
    /*if ((atleast_onefile == 0) && (atleast_oneidpfile == 0)) {
        $('.loading').addClass('hide');
        $('.subsdocdiv').addClass('hide');
        $('#defaultdocdiv').removeClass('hide');
        $('.idproofdocdiv').addClass('hide');
        $('#default_iddocdiv').removeClass('hide');
        alert('Please upload all related documents.');
    }else if (atleast_onefile == 0) {
        $('.loading').addClass('hide');
        //$('input[name^="subscriber_documents"]').remove();
        //$('[id^=abcd]').remove();
        //$('select[name="kyc_documents"]').val('');
        $('.subsdocdiv').addClass('hide');
        $('#defaultdocdiv').removeClass('hide');
        alert('Please upload atleast one KYC document.');
    }else if (atleast_oneidpfile == 0) {
        $('.loading').addClass('hide');
        //$('input[name^="idproof_documents"]').remove();
        //$('[id^=abcd]').remove();
        //$('select[name="kyc_documents"]').val('');
        $('.idproofdocdiv').addClass('hide');
        $('#default_iddocdiv').removeClass('hide');
        alert('Please upload atleast one PHOTO ID document.');
    }
    else if(typeof user_profile_image == 'undefined'){
        $('.loading').addClass('hide');
        alert('Please upload user image.');
    }else{*/
        //alert('ok');
	
	if (atleast_oneidpfile > 0) {
	    for (var j=0; j< total_idpfile; j++) {
		$.each($('input[name^="idproof_documents"]')[j].files, function(l, file) {
		    fd.append(j, file);
		});
	    }
	    j = j-1;
	}else{
	    j = 0;
	}
        
	if (atleast_onefile > 0) { 
	    for (var k=0; k< totalfile; k++) {
		$.each($('input[name^="subscriber_documents"]')[k].files, function(l, ifile) {
		    fd.append(j, ifile);
		    j++;
		});
	    }
	}
        if (atleast_onecorpfile > 0) {
            for (var c=0; c< total_corpfile; c++) {
                $.each($('input[name^="corpproof_documents"]')[c].files, function(l, cfile) {
                    fd.append(j, cfile);
                    j++;
                });
            }
        }
        if (typeof user_profile_image != 'undefined') {
            fd.append('profile_image', user_profile_image);
            j++;
        }
        if (typeof user_signature_image != 'undefined') {
            fd.append('signature', user_signature_image);
            j++;
        }
        
        
        var other_data = $('#subscriber_documents_form').serializeArray();
        $.each(other_data,function(key,input){
            fd.append(input.name,input.value);
        });
        
	//alert(jQuery.hasData( fd ));
	$('.loading').removeClass('hide');
        $.ajax({
            url: base_url+'user/upload_documents',
            data: fd,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
                $('.loading').addClass('hide');
                //alert(data);
                $('.subscriber_userid').val(data);
                $('#usertype_customer').attr('checked','checked');
                $('.radio-inline').css('color', '#b3b3b3');
                $('input[type="radio"][name="user_type_radio"]').attr('disabled','disabled');
                
                //$('select[name="kyc_documents"]').val('');
                //$('input[name^="subscriber_documents"]').remove();
                $('.subsdocdiv').addClass('hide');
                $('#defaultdocdiv').removeClass('hide');
                $('.idproofdocdiv').addClass('hide');
                $('#default_iddocdiv').removeClass('hide');
                $('.corpproofdocdiv').addClass('hide');
                $('#default_corporatedocdiv').removeClass('hide');
                
                $('#doclisting').html('');
                $('.loading').addClass('hide');
                defaultkyctab_details();
            }
        });
    //}
}


function edit_allkyc_details() {
    $('#doclisting').html('');
    $('.loading').removeClass('hide');
    var cust_uuid = $('.subscriber_userid').val();
    $.ajax({
        url: base_url+'user/edit_allkyc_details',
        type: 'POST',
        dataType: 'json',
        data: "cust_uuid="+cust_uuid,
        success: function(data){
            var gen = '';
            var readonlyperm = data.isreadonly_permission;
            if (readonlyperm == '1') {
                $('#subscriber_documents_form input').prop('disabled', true);
		$('#subscriber_documents_form select').prop('disabled', true);
            }
            if (typeof data.kycdocs != 'undefined' && (data.kycdocs.length > 0)) {
                $.each(data.kycdocs, function(index, element) {
                    //alert(index +' => '+ element['doctype'] + ' => ' + element['filename']);
                    var doctype = element['doctype'];
                    var docid = element['docid'];
                    var uuid = element['subscriber_uuid'];
                    var filename = element['filename'];
                    var document_number = element['document_number'];
                    var user_profile_image = element['user_profile_image'];
                    var user_signature_image = element['user_signature_image'];

                    if ((typeof user_profile_image != 'undefined') && (user_profile_image != '')) {
                        gen += '<div class="row" style="margin-top:20px"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>User Image</label></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>&nbsp;</label></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="label label-default" onclick="edit_docpreview(\''+user_profile_image+'\', \''+uuid+'\')"><i class="fa fa-eye" aria-hidden="true"></i></span>&nbsp;&nbsp;<span class="label label-default" onclick="edit_docdelete(\'userprofile\', \''+uuid+'\')"><i class="fa fa-trash" aria-hidden="true"></i></span></div></div>';
                    }
                    if ((typeof user_signature_image != 'undefined') && (user_signature_image != '')) {
                        gen += '<div class="row" style="margin-top:20px"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>User Signature</label></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>&nbsp;</label></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="label label-default" onclick="edit_docpreview(\''+user_signature_image+'\', \''+uuid+'\')"><i class="fa fa-eye" aria-hidden="true"></i></span>&nbsp;&nbsp;<span class="label label-default" onclick="edit_docdelete(\'usersign\', \''+uuid+'\')"><i class="fa fa-trash" aria-hidden="true"></i></span></div></div>';
                    }
                    if ((typeof user_profile_image == 'undefined') || (typeof user_signature_image == 'undefined')) {
                        gen += '<div class="row" style="margin-top:20px"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>'+element['doctype']+'</label></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>'+document_number+'</label></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="label label-default" onclick="edit_docpreview(\''+filename+'\', \''+uuid+'\')"><i class="fa fa-eye" aria-hidden="true"></i></span>&nbsp;&nbsp;<span class="label label-default" onclick="edit_docdelete(\''+docid+'\', \''+uuid+'\')"><i class="fa fa-trash" aria-hidden="true"></i></span></div></div>';
                    //<span class="label label-default"  onclick="edit_docdelete(\''+docid+'\', \''+uuid+'\')"><i class="fa fa-trash" aria-hidden="true"></i></span>
                    }
                });
            }
            $('#uploaded_docs').html(gen);
            window.scrollTo(0,0);

            mui.tabs.activate('KYC_details');
            $('.loading').addClass('hide');
        }
    });
}

function edit_docpreview(filename, uuid) {
    //var imgpath = base_url+"assets/media/documents/"+uuid+"/"+filename;
    var imgpath = "https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp_homeusers/kyc_docs/"+uuid+"/"+filename;
    $('#edit_documentModal').modal('show');
    $('#edit_docpreview').html("<center><img class='previewimg' src='"+imgpath+"' style='width:70%'/></center>");
}

function edit_docdelete(docid, uuid) {
    var c = confirm('Are you sure, you want to delete this ?');
    if (c) {
        $.ajax({
            url: base_url+'user/edit_docdelete',
            type: 'POST',
            dataType: 'json',
            data: "cust_uuid="+uuid+"&docid="+docid,
            success: function(data){
                edit_allkyc_details();
            }
        })
    }
}


/***************************** TICKETS TABBING JS START ************************/

function add_ticket_request() {
    $('.loading').removeClass('hide');
    $('#serviceRequestModal').modal('hide');
    var formdata = $('#ticket_request_form').serialize();
    $.ajax({
        url: base_url+'user/add_ticket_request',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
            $('.loading').addClass('hide');
            var cust_uuid = $('input[name="subscriber_uuid"]').val();
            $.ajax({
                url: base_url+'user/fetch_allopenticket',
                type: 'POST',
                data: "cust_uuid="+cust_uuid,
                success: function(data){
                    $('.loading').addClass('hide');
                    $('#allticketdiv').html(data);
		    mui.tabs.activate('opentickets_pane');
                }
            });
        }
    });
}

function show_edit_ticket_request(tktid='') {
    $('.loading').removeClass('hide');
    $('#edit_ticket_request_form')[0].reset();
    
    var formdata = $('#edit_ticket_request_form').serialize();
    $.ajax({
        url: base_url+'user/edit_ticket_request',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: formdata+'&tktid='+tktid,
        success: function(data){
            
            var cust_uuid = data.subscriber_uuid;
            $.ajax({
                url: base_url+'user/ticket_sorters_list',
                type: 'POST',
                async: false,
                data: 'uuid='+cust_uuid,
                success: function(data){
                   $('select[name="edticket_assignto"]').empty().append('<option value="">Assign Ticket</option>');
                   $('select[name="edticket_assignto"]').append(data);
                }
            });
            
	    $.ajax({
		url: base_url+'user/ticketstype_optionslist',
		type: 'POST',
		async: false,
		success: function(data){
		   $('select[name="edticket_type"]').empty().append(data);
		}
	     });
	    
            $('input[name="edtktid"]').val(data.tid);
            $('input[name="edticketid"]').val(data.ticket_id);
            $('input[name="edcustomer_id"]').val(data.subscriber_id);
            $('input[name="edcustomer_uuid"]').val(data.subscriber_uuid);
            $('select[name="edticket_type"]').val(data.ticket_type);
            $('select[name="edticket_priority"]').val(data.ticket_priority);
            if (data.ticket_assign_to == '0') {
                $('select[name="edticket_assignto"]').prop('disabled', false);
            }else{
                $('select[name="edticket_assignto"]').prop('disabled', true);
                $('select[name="edticket_assignto"]').val(data.ticket_assign_to);
            }
            $('textarea[name="edticket_description"]').val(data.ticket_description);
            
            var tcarr = data.ticket_comment;
            if (tcarr.length > 0) {
                $('.pre_tcomment').remove();
                for (i=0; i < tcarr.length; i++) {
                    $('#edit_tcomment_list').append("<input type='text' class='pre_tcomment' value='"+tcarr[i]+"' readonly >")
                }
            }else{
		$('.pre_tcomment').remove();
	    }
            //$('input[name="edticket_comment"]').val(data.ticket_comment);
            $('input[name="edticket_datetime"]').val(data.ticket_added_on);
            $('.loading').addClass('hide');            
            $('#editserviceRequestModal').modal('show');
        }
    });
}

function edit_ticket_request() {
    $('.loading').removeClass('hide');
    var formdata = $('#edit_ticket_request_form').serialize();
    tktid = $('input[name="edtktid"]').val();
    $.ajax({
        url: base_url+'user/edit_ticket_request',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: formdata+'&tktid='+tktid,
        success: function(data){
            $('.loading').addClass('hide');
            $('#editserviceRequestModal').modal('hide');
            fetch_allopenticket();
        }
    });
}

function fetch_allopenticket() {
    $('.loading').removeClass('hide');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    $.ajax({
        url: base_url+'user/fetch_allopenticket',
        type: 'POST',
        data: "cust_uuid="+cust_uuid,
        success: function(data){
            $('.loading').addClass('hide');
            $('#allticketdiv').html(data);
	    mui.tabs.activate('opentickets_pane');
        }
    });
}


$('body').on('change', '#changetktstatus', function(){
   var statusval = $(this).val();
   var tktid = $(this).find(':selected').attr('data-tktid');
   $('form#changeticket_statusform')[0].reset();
   $('input[name="chngtktstatus"]').val(statusval);
   $('input[name="chngtktstatusid"]').val(tktid);
   $('#changeticket_statusconfirmation_alert').modal('show');
});

function changeticket_statusconfirmation() {
    $('#changeticket_statusconfirmation_alert').modal('hide');
   var formdata = $('form#changeticket_statusform').serialize();
   $('.loading').removeClass('hide');
   $.ajax({
	url: base_url+'user/changeticket_statusconfirmation',
	type: 'POST',
	data: formdata,
	async:false,
	success: function(result){
	   fetch_allopenticket();
	   $('.loading').addClass('hide');
	}
   });
}

function verify_confirm_tktclose() {
   var cust_phone = $('input[name="subscriber_phone"]').val();
   var otp = $('input[name="tktclose_otp"]').val();
   var tktid =  $('input[name="tktcloseid"]').val();
   $.ajax({
      url: base_url+'user/verify_confirm_tktclose',
      type: 'POST',
      data: 'phone='+cust_phone+'&otp='+otp+'&tktid='+tktid,
      async: false,
      success: function(data){
	 if (data == 0) {
	    $('#tktclose_otp_err').html('OTP you have entered not get matched.');
	 }else{
	    $('#ticket_closedModal').modal('hide');
	    $('.loading').removeClass('hide');
	    setTimeout(function (){
	       $('.loading').addClass('hide');
	       fetch_allopenticket();
	       mui.tabs.activate('Requests');
	    },1000);
	 }
      }
   });
}

function add_ticket_types() {
   var formdata = $('form#ticket_types_form').serialize();
   $.ajax({
      url: base_url+'user/add_tickettypes',
      type: 'POST',
      data: formdata,
      dataType: 'json',
      async: false,
      success: function(data){
	 $('input[name="tickettypeid"]').val('');
	 $('input[name="tickettype_name"]').val('');
	 $('input[name="tickettype_name"]').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched');
	 $('input[name="tickettype_name"]').addClass('mui--is-empty mui--is-pristine mui--is-untouched');
	 $.ajax({
	    url: base_url+'user/getticketstype',
	    type: 'POST',
	    async: false,
	    dataType: 'json',
	    success: function(data){
	       $('#alltickettypesdiv').html(data);
	    }
	 });
      }
   });
}

function delete_ticket_types(tktypeid) {
   var c = confirm('Are you sure, you want to delete this entry.');
   if (c) {
      $.ajax({
	 url: base_url+'user/delete_tickettypes',
	 type: 'POST',
	 data: 'tktypeid='+tktypeid,
	 dataType: 'json',
	 async: false,
	 success: function(data){
	    $('input[name="tickettypeid"]').val('');
	    $('input[name="tickettype_name"]').val('');
	    $('input[name="tickettype_name"]').removeClass('mui--is-touched');
	    $('input[name="tickettype_name"]').addClass('mui--is-untouched');
	    $.ajax({
	       url: base_url+'user/getticketstype',
	       type: 'POST',
	       dataType: 'json',
	       success: function(data){
		  $('#alltickettypesdiv').html(data);
	       }
	    });
	 }
      });
   }
}

function formedit_ticket_types(tktypeid) {
   $('input[name="tickettypeid"]').val('');
   $('input[name="tickettype_name"]').val('');
   $('input[name="tickettype_name"]').removeClass('mui--is-touched');
   $('input[name="tickettype_name"]').addClass('mui--is-untouched');
   $.ajax({
      url: base_url+'user/tickettype_iddetails',
      type: 'POST',
      data: 'tktypeid='+tktypeid,
      dataType: 'json',
      async: false,
      success: function(data){
	 $('input[name="tickettypeid"]').val(tktypeid);
	 $('input[name="tickettype_name"]').val(data.ticket_type);
      }
   });
}

$('body').on('click', '#tickettypeli', function(){
   $('.loading').removeClass('hide');
   $('input[name="tickettypeid"]').val('');
   $('input[name="tickettype_name"]').val('');
   $('input[name="tickettype_name"]').removeClass('mui--is-touched');
   $('input[name="tickettype_name"]').addClass('mui--is-untouched');
   $.ajax({
      url: base_url+'user/getticketstype',
      type: 'POST',
      dataType: 'json',
      success: function(data){
	 $('#alltickettypesdiv').html(data);
	 $('.loading').addClass('hide');
      }
   });
});

$('body').on('click', '#ticketclosedli', function(){
    $('.loading').removeClass('hide');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    $.ajax({
        url: base_url+'user/fetch_allclosedticket',
        type: 'POST',
        data: "cust_uuid="+cust_uuid,
	async: false,
        success: function(data){
            $('.loading').addClass('hide');
            $('#allclosedticketdiv').html(data);
        }
    });
    $.ajax({
       url: base_url+'user/ticketstype_optionslist',
       type: 'POST',
       success: function(data){
	  $('select[name="ticket_type"]').empty().append(data);
       }
    });
});


$('body').on('click','#create_newticket', function(){
    $('.loading').removeClass('hide');
    $('select[name="ticket_type"]').val('');
    $('select[name="ticket_priority"]').val('');
    $('select[name="ticket_assignto"]').val('');
    $('textarea[name="ticket_description"]').val('');
    $('input[name="ticket_comment"]').val('');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    $.ajax({
       url: base_url+'user/ticket_sorters_list',
       type: 'POST',
       async: false,
       data: 'uuid='+cust_uuid,
       success: function(data){
	  $('select[name="ticket_assignto"]').empty().append('<option value="">Assign Ticket</option>');
	  $('select[name="ticket_assignto"]').append(data);
       }
    });
    $.ajax({
       url: base_url+'user/ticketstype_optionslist',
       type: 'POST',
       async: false,
       success: function(data){
	  $('select[name="ticket_type"]').empty().append(data);
       }
    });
    $('input[name="customer_uuid"]').val(cust_uuid);
    var d = new Date();
    var time = ' '+d.toLocaleTimeString();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var currdate = ((''+day).length<2 ? '0' : '')+day + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + d.getFullYear() ;
    var seconds = d.getSeconds();
    var tkttime = d.getHours() +""+ d.getMinutes() +""+ ((''+seconds).length<2 ? '0' : '')+seconds ;
    var tktdate = day +""+ month +""+ d.getFullYear().toString().substr(-2) ;
    var dbtktdatetime =  d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + d.getHours() +":"+ d.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;;
    $('input[name="ticket_datetime"]').remove();
    $('input[name="ticket_datetimeformat"]').remove();
    $('input[name="ticketid"]').val(tktdate+tkttime);
    $('#tktdtme').append('<input name="ticket_datetimeformat" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="'+currdate+time+'">');
    $('#tktdtme').append('<input name="ticket_datetime"  class="mui--is-empty mui--is-untouched mui--is-pristine" type="hidden" value="'+dbtktdatetime+'">');
    //$('#serviceRequestModal').modal('show');
    mui.tabs.activate('addnewtickets_pane');
    $('.loading').addClass('hide');
});

function show_ticket_summary(tktid) {
    $.ajax({
        url: base_url+'user/show_ticket_summary',
        type: 'POST',
        dataType: 'json',
        data: 'tktid='+tktid,
	async: false,
        success: function(data){
	    $('#ticketsummary_modal').modal('show');
            $('ul#tktsummarylist').html(data);
        }
    });
}

function assign_ticket_member(tktid) {
    $('#assignTicketModal').modal('hide');
    $('.loading').removeClass('hide');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    $.ajax({
       url: base_url+'user/ticket_sorters_list',
       type: 'POST',
       async: false,
       data: 'uuid='+cust_uuid,
       success: function(data){
          $('select[name="team_member"]').empty().append('<option value="">Select Team Member</option>');
          $('select[name="team_member"]').append(data);
          $('.loading').addClass('hide');
          $('#tktid_toupdate').val(tktid);
          $('#assignTicketModal').modal('show');
       }
    });
}

function updateTicketMember() {
    $('#assignTicketModal').modal('hide');
    $('.loading').removeClass('hide');
    var formdata =  $('#assignTicketMember_form').serialize();
    $.ajax({
        url: base_url+'user/updateTicketMember',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        async: false,
        success: function(data){
            $('#assignTicketModal').modal('hide');
            $('.loading').addClass('hide');
            fetch_allopenticket();
	    mui.tabs.activate('opentickets_pane');
        }
    });
}

/***************************** TICKETS TABBING JS END ****************************/

function personaldetails_listing() {
    $('.loading').removeClass('hide');
    var cust_uuid = $('.subscriber_userid').val();
    $.ajax({
        url: base_url+'user/personaldetails_listing',
        type: 'POST',
        dataType: 'json',
        data: "sid="+cust_uuid,
	async: false,
        success: function(data){
            if (data.record.length != 0) {
                for (var i=0; i<data.record.length; i++) {
                    var firstname = data.record[i].firstname;
                    var lastname = data.record[i].lastname;
                    var middlename = data.record[i].middlename;
                    var email = data.record[i].email;
                    var mobile = data.record[i].mobile;
                    var dob = data.record[i].dob;
                    var flat_number = data.record[i].flat_number;
                    var address1 = data.record[i].address;
                    var address2 = data.record[i].address2;
                    var state = data.record[i].state;
                    var city = data.record[i].city;
                    var zone = data.record[i].zone;
                    var usuage_locality = data.record[i].usuage_locality;
                    var username = data.record[i].username;
                    var uid = data.record[i].uid;
                    var priority_account = data.record[i].priority_account;
                    
		    getcitylist(state);
		    getzonelist(city);
		    
                    $('input[name="subscriber_fname"]').val(firstname);
                    $('input[name="subscriber_mname"]').val(middlename);
                    $('input[name="subscriber_lname"]').val(lastname);
                    $('input[name="subscriber_email"]').val(email);
                    $('input[name="subscriber_phone"]').val(mobile);
                    $('input[name="subscriber_dob"]').val(dob);
                    $('input[name="subscriber_flatno"]').val(flat_number);
                    $('input[name="subscriber_address1"]').val(address1);
                    $('input[name="subscriber_address2"]').val(address2);
                    $('select[name="state"]').val(state);
                    $('select[name="city"]').val(city);
                    $('select[name="zone"]').val(zone);
                    $('input[name="usuage_locality"]').val(usuage_locality);
                    $('input[name="subscriber_username"]').val(username);
                    $('input[name="subscriber_uuid"]').val(uid);
                    $('input[name="priority_account"]').val(priority_account);
                    
		    var billaddr = data.record[i].billaddr;
		    var billing_username = data.record[i].billing_username;
		    var billing_mobileno = data.record[i].billing_mobileno;
		    var billing_email = data.record[i].billing_email;
		    var billing_flat_number = data.record[i].billing_flat_number;
		    var billing_address = data.record[i].billing_address;
		    var billing_address2 = data.record[i].billing_address2;
		    var billing_state = data.record[i].billing_state;
		    var billing_city = data.record[i].billing_city;
		    var billing_zone = data.record[i].billing_zone;
		    
		    getcitylist(billing_state,'','','bstate');
		    getzonelist(billing_city,'','','bcity');
		    
		    $('input[name="billing_subscriber_name"]').val(billing_username);
		    $('input[name="billing_subscriber_phone"]').val(billing_mobileno);
		    $('input[name="billing_subscriber_email"]').val(billing_email);
		    $('input[name="billing_subscriber_flatno"]').val(billing_flat_number);
		    $('input[name="billing_subscriber_address1"]').val(billing_address);
		    $('input[name="billing_subscriber_address2"]').val(billing_address2);
		    $('select[name="billing_state"]').val(billing_state);
                    $('select[name="billing_city"]').val(billing_city);
                    $('select[name="billing_zone"]').val(billing_zone);
		    
                }
		
		$.ajax({
		    url: base_url+'user/check_user_onlineoffline',
		    type: 'POST',
		    data: 'cust_uuid='+uid,
		    async: false,
		    dataType: 'json',
		    success: function(userstatus){
			$('.loading').addClass('hide');
			if(userstatus == 'online'){
			    status_indicator = "<img src='"+base_url+"assets/images/green.png' alt='' />";
			}else{
			    status_indicator = "<img src='"+base_url+"assets/images/red.png' alt='' />";
			}
			$('.user_onlineoffline_status').html(status_indicator);
		    }
		});
            }
	    
            $('.loading').addClass('hide');
            window.scrollTo(0,0);
            mui.tabs.activate('Personal_details');
        }
    });
}

function edit_customer() {
    $('.loading').removeClass('hide');
    var formdata = $('#edit_subscriber_form').serialize();
    //alert(formdata);
    $.ajax({
        url: base_url+'user/edit_personaldetails',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
            //alert(data);
            if (data.customer_id != null && data.customer_uuid != '') {
                $('.subscriber_userid').val(data.customer_id);
                $('.tab_menu').css({'cursor' : 'pointer'});
                $('.tab_menu').removeAttr('disabled');
                $('#usertype_customer').attr('checked','checked');
                $('.radio-inline').css('color', '#b3b3b3');
                $('input[type="radio"][name="user_type_radio"]').attr('disabled','disabled');
                
                $('input[name="customer_uuid"]').val(data.customer_uuid);
                $('.customer_name').html(data.customer_name);
                $('.customer_email').html(data.customer_email);
                $('.customer_mobile').html('+91 '+data.customer_mobile);
            }
            $('.loading').addClass('hide');
            window.scrollTo(0,0);
            mui.tabs.activate('Personal_details');
        }
    });
    
}

function getplandetails(planid) {
    if (planid != '') {
        $.ajax({
            url: base_url+'user/plandetails',
            type: 'POST',
            dataType: 'json',
            data: "planid="+planid,
            success: function(data){
                //alert(data);
                $('#plan_details').removeClass('hide');
                $('.timeplan').css('display', 'none');
                $('.dataplan').css('display', 'none');
                $('.fupclass').css('display', 'none');
		$('.nonfup_custspeed').css('display', 'none');
                
                $('.detplan_duration').html(data.plan_duration + " Month");
                $('.plan_duration').val(data.plan_duration);
                $('.detplan_name').val(data.plan_name);
                $('.detplan_desc').val(data.plan_desc);
                $('.detplan_validity').val(data.plan_validity+' days');
                $('.detplan_type').html(data.plan_type);
                $('.detdwnld_rate').val(data.plan_dwnld_rate);
                $('.detupld_rate').val(data.plan_upld_rate);
                $('.detplan_name_text').html(data.plan_name);
                var cocurr = data.currency;
                
                if (data.custom_planprice != '') {
                    $('#displaycustom_planprice').html('('+cocurr+' '+data.custom_planprice+')');
                    $('#plan_price').css('text-decoration', 'line-through');
                    $('.custom_planid').val(planid);
                    $('.custom_planprice').val(data.custom_planprice);
                }else{
                    $('#displaycustom_planprice').html('');
                    $('#plan_price').css('text-decoration', 'none');
                    $('.custom_planid').val('');
                    $('.custom_planprice').val('');
                }

                $('#plan_price').html('('+cocurr+' '+data.plan_net_amount+')');
                $('#plan_type_id').val(data.plan_type_id);
                if (data.plan_type_id == '2') {
                    $('.timeplan').css('display', 'block');
                    $('.fupclass').css('display', 'none');
                    $('.dataplan').css('display', 'none');
		    $('.nonfup_custspeed').css('display', 'block');
                    
                    $('.detplan_timelimit').val(data.plan_timelimit);
                    $('.dettime_calculated_on').html(data.plan_time_calculated_on)
                }else if (data.plan_type_id == '4') {
                    $('.timeplan').css('display', 'none');
                    $('.dataplan').css('display', 'block');
                    $('.fupclass').css('display', 'none');
		    $('.nonfup_custspeed').css('display', 'block');
                    
                    $('.detdata_limit').val(data.plan_data_limit);
                    $('#plandatalimit').val(data.plandatalimit);
                    $('#plandata_calculatedon').val(data.plandata_calculatedon);
                    $('.detdata_calcn').html(data.plan_data_calculated_on);
                }else if (data.plan_type_id == '3') {
                    $('.timeplan').css('display', 'none');
                    $('.dataplan').css('display', 'block');
                    $('.fupclass').css('display', 'block');
		    $('.nonfup_custspeed').css('display', 'none');
                    
                    $('.detdata_limit').val(data.plan_data_limit);
                    $('#plandatalimit').val(data.plandatalimit);
                    $('#plandata_calculatedon').val(data.plandata_calculatedon);
                    $('.detdata_calcn').html(data.plan_data_calculated_on);
                    $('.detfup_dwnl_rate').val(data.plan_postfup_dwnld_rate);
                    $('.detfup_upld_rate').val(data.plan_postfup_upld_rate)
                }else{
                    $('#plandatalimit').val('0');
                    $('#plandata_calculatedon').val('0');
		    $('.nonfup_custspeed').css('display', 'block');
                    
                }
                
                $('#plan_start_date').html(data.plan_activatedon);
                $('#plan_payment_till').html(data.renewal_date);
                $('#plan_expiry_on').html(data.expiration_date);
                $('#next_billing_date').html(data.next_bill_date);
                
            }
        });
    }else{
        $('#plan_details').addClass('hide');
    }
}

function add_subscriber_plan(){
    $('.loading').removeClass('hide');
    
    var editplan = $('.editplan').val();
    if ((typeof editplan != 'undefined') && (editplan == '1') ) {
        //$('#plan_change').modal('show');
    }else{
        //$('#plan_change').modal('hide');
    }
    
    var user_plan_type = $('input[name="user_plan_type"]:checked').val();
    var plan_duration = $('input[name="plan_duration"]').val();
    if (user_plan_type == 'postpaid') {
	$('input[name="prebill_oncycle"]').prop('checked', false);
    }
    
    
    var formdata =  $('#subscriber_plandetail_form').serialize();
    if ((plan_duration > 1) && (user_plan_type == 'postpaid')) {
        $('#alertMsgTxt').html("Multiple Month Plan Can't be applied on Postpaid.");
        $('#alertModal').modal('show');
        $('.loading').addClass('hide');
    }else{
        $.ajax({
            url: base_url+'user/add_subscriber_plan',
            type: 'POST',
            dataType: 'json',
            data: formdata,
            async: false,
            success: function(data){
                $('#wallet_credit_limit').html(data.currency+' '+data.user_creditlimit+'.00');
                window.scrollTo(0,0);
                
                var subsid = $('.subscriber_userid').val();
                var subs_uuid = $('.subscriber_uuid').val();
                $.ajax({
                    url: base_url+'user/planassoc_withuser',
                    type: 'POST',
                    dataType: 'json',
                    async: false,
                    data: 'subsid='+subsid+'&subs_uuid='+subs_uuid,
                    success: function(data){
                        if (data == 0) {
                            $('#inactive_billing').removeClass('hide');
                            $('#active_billing').addClass('hide');
                        }else{
                            $('#inactive_billing').addClass('hide');
                            $('#active_billing').removeClass('hide');
                            //user_billing_listing();
                        }
                    }
                });
                mui.tabs.activate('Billing');
                $('.loading').addClass('hide');
            }
        });
    }
}

function subscriber_plan_details() {
    $('.loading').removeClass('hide');
    var cust_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/subscriber_plan_details',
        type: 'POST',
        dataType: 'json',
        data: "cust_uuid="+cust_uuid,
        success: function(data){
            if (data != '') {
                $('#plan_details').addClass('hide');
                $('#assign_plan option').remove();
                $('#assign_plan').append(data.active_planlist);

                if (typeof data.plan_id != 'undefined') {
                    $('#plan_details').removeClass('hide');
                    $('.timeplan').css('display', 'none');
                    $('.dataplan').css('display', 'none');
                    $('.fupclass').css('display', 'none');
		    $('.nonfup_custspeed').css('display', 'none');
                
                    if (typeof data.next_cycle_plan != 'undefined') {
                        $('.next_cycle_plan').removeClass('hide');
                    }
                
                    $('.detplan_duration').html(data.plan_duration + " Month");
                    $('input[name="user_plan_type"][value="'+data.user_plan_type+'"]').attr('checked', 'checked');
                    $('select[name="assign_plan"]').val(data.plan_id);
                    $('.detplan_name').val(data.plan_name);
                    $('.detplan_name_text').html(data.plan_name);
                    $('.detplan_desc').val(data.plan_desc);
                    $('.detplan_validity').val(data.plan_validity+' days');
                    $('.detplan_type').html(data.plan_type);
                    $('.detdwnld_rate').val(data.plan_dwnld_rate);
                    $('.detupld_rate').val(data.plan_upld_rate);
                    
                    var cocurr = data.currency; 
                    if (data.enableuser != 0) {
                        $('#custplanbtnopt').addClass('hide');
                    }
                    if (data.custom_planprice != '') {
                        $('#displaycustom_planprice').html('('+cocurr+' '+data.custom_planprice+')');
                        $('#plan_price').css('text-decoration', 'line-through');
                        $('.custom_planid').val(data.plan_id);
                        $('.custom_planprice').val(data.custom_planprice);
                    }else{
                        $('#displaycustom_planprice').html('');
                        $('#plan_price').css('text-decoration', 'none');
                        $('.custom_planid').val('');
                        $('.custom_planprice').val('');
                    }
                    
                    $('#plan_price').html('('+cocurr+' '+data.plan_net_amount+')');
                    $('#plan_type_id').val(data.plan_type_id);
                    if (data.plan_type_id == '2') {
                        $('.timeplan').css('display', 'block');
                        $('.fupclass').css('display', 'none');
                        $('.dataplan').css('display', 'none');
                        $('.nonfup_custspeed').css('display', 'block');
			
                        $('.detplan_timelimit').val(data.plan_timelimit);
                        $('.dettime_calculated_on').html(data.plan_time_calculated_on)
                    }else if (data.plan_type_id == '4') {
                        $('.timeplan').css('display', 'none');
                        $('.dataplan').css('display', 'block');
                        $('.fupclass').css('display', 'none');
			$('.nonfup_custspeed').css('display', 'block');
                        
                        $('.detdata_limit').val(data.plan_data_limit);
                        $('#plandatalimit').val(data.plandatalimit);
                        $('#plandata_calculatedon').val(data.plandata_calculatedon);
                        $('.detdata_calcn').html(data.plan_data_calculated_on);
                    }else if (data.plan_type_id == '3') {
                        $('.timeplan').css('display', 'none');
                        $('.dataplan').css('display', 'block');
                        $('.fupclass').css('display', 'block');
			$('.nonfup_custspeed').css('display', 'none');
                        
                        $('.detdata_limit').val(data.plan_data_limit);
                        $('#plandatalimit').val(data.plandatalimit);
                        $('#plandata_calculatedon').val(data.plandata_calculatedon);
                        $('.detdata_calcn').html(data.plan_data_calculated_on);
                        $('.detfup_dwnl_rate').val(data.plan_postfup_dwnld_rate);
                        $('.detfup_upld_rate').val(data.plan_postfup_upld_rate)
                    }else{
                        $('#plandatalimit').val('0');
                        $('#plandata_calculatedon').val('0');
			$('.nonfup_custspeed').css('display', 'block');
                        
                    }
                    
                    
                    $('input[name="user_plan_type"]').prop('checked', false);
                    $('input[name="user_plan_type"][value="'+data.user_plan_type+'"]').prop('checked', true);
                    $('#plan_start_date').html(data.plan_activatedon);
                    $('#plan_payment_till').html(data.paidtill_date);
                    $('#plan_expiry_on').html(data.expiration_date);
                    $('#next_billing_date').html(data.next_bill_date);
		    if (data.user_plan_type == 'prepaid') {
			$('#prebillmonthlyopt').removeClass('hide');
			if (data.prebill_oncycle == '1') {
			    $('input[name="prebill_oncycle"]').prop('checked',true);
			}else{
			    $('input[name="prebill_oncycle"]').prop('checked',false);
			}
			
		    }else{
			$('#prebillmonthlyopt').addClass('hide');
		    }
		    
		    if (data.planautorenewal == 1) {
			$('.planautorenewal').html('<img src="'+base_url+'assets/images/on2.png" rel="disable">');
		    }else{
			$('.planautorenewal').html('<img src="'+base_url+'assets/images/off2.png" rel="enable">');
		    }
                    
                }
                window.scrollTo(0,0);
                mui.tabs.activate('Plan_details');
                $('.loading').addClass('hide');
            }else{
                window.scrollTo(0,0);
                mui.tabs.activate('Plan_details');
                $('.loading').addClass('hide');
            }
        }
    });
}

function nextplan_details(planid, optionchanged='') {
    var cust_uuid = $('.subscriber_uuid').val();
    if (planid != '') {
        $.ajax({
            url: base_url+'user/plandetails',
            type: 'POST',
            dataType: 'json',
            data: "planid="+planid+"&cust_uuid="+cust_uuid+"&nextplancycle=1",
            success: function(data){
                if (data != '') {
                    $('#nextplan_details').removeClass('hide');
                    $('.nxt_timeplan').css('display', 'none');
                    $('.nxt_dataplan').css('display', 'none');
                    $('.nxt_fupclass').css('display', 'none');
                    
                    if (typeof data.next_cycle_plan != 'undefined') {
                        $('.next_cycle_plan').removeClass('hide');
                    }
                    $('.nxt_detplan_duration').html(data.plan_duration + " Month");
                    $('#plandur_alert').html('');
                    if (data.plan_duration > 1) {
                        $('#plandur_alert').html('NOTE: <br/> ITS A SPECIAL PLAN OF '+data.plan_duration+' MONTH. SO BILLING WILL BE CALCULATED ACCORDING TO IT.');
                    }
                    $('.nxt_plan_name').html(data.plan_name);
                    $('.nxt_detplan_desc').val(data.plan_desc);
                    $('.nxt_detplan_validity').val(data.plan_validity+' days');
                    $('.nxt_detplan_type').html(data.plan_type);
                    $('.nxt_detdwnld_rate').val(data.plan_dwnld_rate);
                    $('.nxt_detupld_rate').val(data.plan_upld_rate);
                    
                    var cocurr = data.currency;
                    var custom_planprice = $('.custom_planprice').val();
                    if(custom_planprice != ''){
                        $('#nxt_custom_planprice').html('('+cocurr+' '+custom_planprice+')');
                        $('#nxt_plan_price').css('text-decoration', 'line-through');
                        $('.custom_planid').val(planid);
                        $('.custom_planprice').val(custom_planprice);
                    
                    }else if (data.custom_planprice != '') {
                        $('#nxt_custom_planprice').html('('+cocurr+' '+data.custom_planprice+')');
                        $('#nxt_plan_price').css('text-decoration', 'line-through');
                        $('.custom_planid').val(planid);
                        $('.custom_planprice').val(data.custom_planprice);
                    
                    }else{
                        $('#nxt_custom_planprice').html('');
                        $('#nxt_plan_price').css('text-decoration', 'none');
                        $('.custom_planid').val('');
                        $('.custom_planprice').val('');
                    }
                    $('#nxt_plan_price').html('('+cocurr+' '+data.plan_net_amount+')');
                    $('#plan_type_id').val(data.plan_type_id);
                    if (data.plan_type_id == '2') {
                        $('.nxt_timeplan').css('display', 'block');
                        $('.nxt_fupclass').css('display', 'none');
                        $('.nxt_dataplan').css('display', 'none');
                        
                        $('.nxt_detplan_timelimit').val(data.plan_timelimit);
                        $('.nxt_dettime_calculated_on').html(data.plan_time_calculated_on)
                    }else if (data.plan_type_id == '4') {
                        $('.nxt_timeplan').css('display', 'none');
                        $('.nxt_dataplan').css('display', 'block');
                        $('.nxt_fupclass').css('display', 'none');
                        
                        $('.nxt_detdata_limit').val(data.plan_data_limit);
                        $('.nxt_detdata_calcn').html(data.plan_data_calculated_on);
                    }else if (data.plan_type_id == '3') {
                        $('.nxt_timeplan').css('display', 'none');
                        $('.nxt_dataplan').css('display', 'block');
                        $('.nxt_fupclass').css('display', 'block');
                        
                        $('.nxt_detdata_limit').val(data.plan_data_limit);
                        $('.nxt_detdata_calcn').html(data.plan_data_calculated_on);
                        $('.nxt_detfup_dwnl_rate').val(data.plan_postfup_dwnld_rate);
                        $('.nxt_detfup_upld_rate').val(data.plan_postfup_upld_rate)
                    }
		    
                    window.scrollTo(0,0);
                    mui.tabs.activate('Plan_details');
                    if (optionchanged != '0') {
                        var nextuser_plan_type = $('input[name="nextuser_plan_type"]:checked').val();
                        if ((data.plan_duration > 1) && (nextuser_plan_type == 'postpaid')) {
                            $('#alertModal').modal('show');
                            $('#alertMsgTxt').html("Multiple Month Plan Can't be applied on Postpaid.");
                        }else{
                            $('#alertModal').modal('hide');
                            $('#planChangeAlertModal').modal('show');
                        }
                    }
                    
                }
            }
        });
    }else{
        $('#nextplan_details').addClass('hide');
    }
}

function applyplan_fornextcycle() {
    var nextassign_plan = $('select[name="nextassign_plan"] option:selected').val();
    if (nextassign_plan != '') {
        nextplan_details(nextassign_plan);
    }
}

$('body').on('change', 'input[name="nextuser_plan_type"]', function(){
    var seltype = $(this).val();
    if (seltype == 'prepaid') {
	$('#nextprebillmonthlyopt').removeClass('hide');
    }else{
	$('#nextprebillmonthlyopt').addClass('hide');
    }
    /*var nextassign_plan = $('select[name="nextassign_plan"] option:selected').val();
    if (nextassign_plan != '') {
        nextplan_details(nextassign_plan);
    }*/
    
});

$('body').on('change', 'input[name="user_plan_type"]', function(){
    var seltype = $(this).val();
    if (seltype == 'prepaid') {
	$('#prebillmonthlyopt').removeClass('hide');
    }else{
	$('#prebillmonthlyopt').addClass('hide');
    }
    /*var nextassign_plan = $('select[name="nextassign_plan"] option:selected').val();
    if (nextassign_plan != '') {
        nextplan_details(nextassign_plan);
    }*/
    
});

function active_user_confirmation() {
    $('.loading').removeClass('hide');
    var subsid = $('.subscriber_userid').val();
    var subs_uuid = $('.subscriber_uuid').val();
    var otp_to_activate_user = $('#otp_to_activate_user').val();
    var enter_otp_to_activate_user = $('#enter_otp_to_activate_user').val();
    var phone = $('#phone_to_activate_user').val();
    
    var formdata = 'subsid='+subsid+'&subs_uuid='+subs_uuid+'&send_otp='+otp_to_activate_user+'&enter_otp='+enter_otp_to_activate_user+'&phone='+phone;
    
    if (otp_to_activate_user == 'sendotp') {
	$.ajax({
	    url: base_url+'user/verify_otpsms',
	    type: 'POST',
	    dataType: 'json',
	    async: false,
	    data: 'phone='+phone+'&enter_otp='+enter_otp_to_activate_user,
	    success: function(response){
		if (response == '1') {
		    $('#user_activatedModal').modal('hide');
		    $.ajax({
			url: base_url+'user/active_user_confirmation',
			type: 'POST',
			dataType: 'json',
			async: false,
			data: formdata,
			success: function(data){
			    window.location = base_url+'user/edit/'+subsid;
			}
		    });
		}else{
		    $('#activate_otp_err').html('Otp not matching. Please Try Again.');
		    $('.loading').addClass('hide');
		}
	    }
	});
    }else{
	
	if (otp_to_activate_user != enter_otp_to_activate_user) {
	    $('#activate_otp_err').html('Otp not matching. Please Try Again.');
	    $('.loading').addClass('hide');
	}else{
	    $('#user_activatedModal').modal('hide');
	    $.ajax({
		url: base_url+'user/active_user_confirmation',
		type: 'POST',
		dataType: 'json',
		data: formdata,
		success: function(data){
		    window.location = base_url+'user/edit/'+subsid;
		}
	    });
	}
    }
}

function nextcycle_planlist() {
    $('.loading').removeClass('hide');
    $('#nextplan_details').addClass('hide');
    var subs_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/nextcycle_planlist',
        type: 'POST',
        dataType: 'json',
        data: 'uuid='+subs_uuid,
        success: function(data){
            $('#nextassign_plan option').remove();
            $('#nextassign_plan').append(data.active_planlist);
            $('#curr_user_plan_type').val(data.curr_user_plan_type);
            var advance_prepay = data.advance_prepay;
            if (advance_prepay == '1') {
                //$('input[name="nextuser_plan_type"][value="prepaid"]').prop('disabled', 'true');
                $('.disable_onadvprepay').addClass('hide');
            }
	    
	    if (data.planautorenewal == '0') {
		$('.disable_onadvprepay').addClass('hide');
	    }
	    
            $('input[name="nextuser_plan_type"]').prop('checked', false);
            $('input[name="nextuser_plan_type"][value="'+data.nxtuser_plan_type+'"]').prop('checked', true);
	    if (data.nxtuser_plan_type == 'prepaid') {
		$('#nextprebillmonthlyopt').removeClass('hide');
	    }else{
		$('#nextprebillmonthlyopt').addClass('hide');
	    }
	    if (data.nxtprebill_oncycle == 1) {
		$('input[name="nextprebill_oncycle"]').prop('checked', true);
	    }else{
		$('input[name="nextprebill_oncycle"]').prop('checked', false);
	    }
            if (data.nextplanid != 0) {
                nextplan_details(data.nextplanid, '0');
            }
            $('.loading').addClass('hide');
        }
    });
}

function add_nextcycleplan() {
    var nxtcycleplan = $('select[name="nextassign_plan"] option:selected').val();
    var nextuser_plan_type = $('input[name="nextuser_plan_type"]:checked').val();
    if (nextuser_plan_type == 'postpaid') {
	$('input[name="nextprebill_oncycle"]').prop('checked', false);
    }
    var custom_srvid = $('.custom_planid').val();
    var custom_planprice = $('.custom_planprice').val();
    if (nxtcycleplan != '') {
        $('.loading').removeClass('hide');
        var formdata =  $('#nextcycle_planform').serialize();
	//alert(formdata);
        $.ajax({
            url: base_url+'user/add_nextcycleplan',
            type: 'POST',
            dataType: 'json',
            data: formdata+'&custom_planprice='+custom_planprice,
            async: false,
            success: function(data){
                if (data.is_expired != 1) {
                    $('#planChangeAlertModal').modal('hide');
                    $('.modal-backdrop').remove();
                    $('.loading').addClass('hide');
                    nextplan_details(data.nextplanid, '0');
                }else{
                    $('.loading').addClass('hide');
                    $('#planChangeAlertModal').modal('hide');
                    $('#alertMsgTxt').html("User get expired. Please recharge.");
                    $('#alertModal').modal('show');
                }
            }
        });
    }else{
        $('select[name="nextassign_plan"]').attr('required', true);
    }
}

function change_planautorenewal(uuid=''){
    $('.loading').removeClass('hide');
    $('#alertModal').modal('hide');
    $('#alertMsgTxt').html('');
    if (uuid == '') {
	uuid = $('.subscriber_uuid').val();
    }
    $.ajax({
	url: base_url+'user/change_planautorenewal',
	type: 'POST',
	data: 'uuid='+uuid,
	dataType: 'json',
	success: function(result){
	    if (result.advance_prepay == '1') {
		if (result.renewaltype == 1) {
		    $('.planautorenewal').html('<img src="'+base_url+'assets/images/on2.png" rel="disable">');
		}else{
		    $('.planautorenewal').html('<img src="'+base_url+'assets/images/off2.png" rel="enable">');
		}
		$('#alertModal').modal('show');
		$('#alertMsgTxt').html("User have credits in Advance Payment. So, AutoRenewal can't be Inactive.");
	    }else{
		if (result.renewaltype == 1) {
		    $('.planautorenewal').html('<img src="'+base_url+'assets/images/on2.png" rel="disable">');
		}else{
		    $('.planautorenewal').html('<img src="'+base_url+'assets/images/off2.png" rel="enable">');
		}
	    }
	    $('.loading').addClass('hide');
	}
    });
}

/**************************************** SETTING TABS STARTS HERE ************************************/

$('body').on('change', '.ipaddrtype', function(){
    if (this.value == 0) {
        $('.addstaticip').removeClass('hide');
        $('#ipaddress').attr('required',true);
        $('.addippoolip').addClass('hide');
        $('#ipaddress_pool').attr('required',false);
    }else if (this.value == 1) {
        $('.addstaticip').addClass('hide');
        $('#ipaddress').attr('required',false);
        $('.addippoolip').addClass('hide');
        $('#ipaddress_pool').attr('required',false);
    }else if (this.value == 2) {
        $('.addstaticip').addClass('hide');
        $('#ipaddress').attr('required',false);
        $('.addippoolip').removeClass('hide');
        $('#ipaddress_pool').attr('required',true);
        ippooladdr_list();
    }
});

$('body').on('change', '.logintype', function(){
    if (this.value == 'hotspot') {
        $('.addhotspotMacID').removeClass('hide');
        $('#hotspotMacID').attr('required',true);
	$('.ill_userpanel').addClass('hide');
	$('.not_ill_userpanel').removeClass('hide');
	$('select[name="nas_illoptions"]').empty().append('<option value="">Select NAS</option>');
	$('select[name="nas_iprange[]"]').empty();
	$('select[name="nas_illoptions"]').attr('required',false);
	$('select[name="nas_iprange[]"]').attr('required',false);
	
    }else if (this.value == 'pppoe') {
        $('.addhotspotMacID').addClass('hide');
        $('#hotspotMacID').attr('required',false);
	$('.ill_userpanel').addClass('hide');
	$('.not_ill_userpanel').removeClass('hide');
	$('select[name="nas_illoptions"]').empty().append('<option value="">Select NAS</option>');
	$('select[name="nas_iprange[]"]').empty();
	$('select[name="nas_illoptions"]').attr('required',false);
	$('select[name="nas_iprange[]"]').attr('required',false);
	
    }else if (this.value == 'ill_ipuser') {
	$('.addhotspotMacID').addClass('hide');
        $('#hotspotMacID').attr('required',false);
	$('.ill_userpanel').removeClass('hide');
	$('.not_ill_userpanel').addClass('hide');
	$('#ipaddress').attr('required',false);
        $('#ipaddress_pool').attr('required',false);
	$('select[name="nas_illoptions"]').attr('required',true);
	$('select[name="nas_iprange[]"]').attr('required',true);
	get_illnaslisting();
	get_ipranges();
    }
});

function get_illnaslisting(nasname = '') {
    $.ajax({
	url: base_url+'user/illnaslisting',
	type: 'POST',
	data: 'nasname='+nasname,
	success: function(data){
	    $('select[name="nas_illoptions"]').empty().append(data);
	}
    });
}

$('body').on('change', 'input[type=radio][name=ill_iptype]', function(){
    get_ipranges();
});

$('select[name=nas_illoptions]').on('change', function(){
    get_ipranges();
});

function get_ipranges(ip_assigned = '') {
    var nasid = $('select[name="nas_illoptions"] option:selected').val();
    var ill_iptype = $('input[name="ill_iptype"]:checked').val();
    var uuid = $('.subscriber_uuid').val();
    if ((nasid != '') && (ill_iptype != '') ) {
	$('.loading').removeClass('hide');
	$.ajax({
	    url: base_url+'user/get_ipranges_Ill',
	    type: 'POST',
	    data: 'nasid='+nasid+'&iptype='+ill_iptype+'&ip_assigned='+ip_assigned+'&uuid='+uuid,
	    dataType: 'json',
	    success: function(data){
		$('.loading').addClass('hide');
		$('select[name="nas_iprange[]"]').empty().append(data.content);
	    }
	});
    }else{
	$('select[name="nas_iprange[]"]').empty();
    }
}
$('body').on('click', '#settingclick_details', function(){
    $('.loading').removeClass('hide');
    $('#staticipaddr_err').html('');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    $.ajax({
	url: base_url+'user/view_syssetting',
	type: 'POST',
	dataType: 'json',
	data: 'uuid='+cust_uuid,
	success: function(data){
	    $("input[name=ipaddrtype][value=" + data.ipaddrtype + "]").attr('checked', 'checked');
	    $('#total_poolips').html(data.total_poolips);
            if (data.ipaddrtype == 0) {
                $('.addstaticip').removeClass('hide');
                $('#ipaddress').attr('required',true);
                $('.addippoolip').addClass('hide');
                $('#ipaddress_pool').attr('required',false);
            }else if (data.ipaddrtype == 1) {
                $('.addstaticip').addClass('hide');
                $('#ipaddress').attr('required',false);
                $('.addippoolip').addClass('hide');
                $('#ipaddress_pool').attr('required',false);
            }else if (data.ipaddrtype == 2) {
                $('.addstaticip').addClass('hide');
                $('#ipaddress').attr('required',false);
                $('.addippoolip').removeClass('hide');
                $('#ipaddress_pool').attr('required',true);
                ippooladdr_list();
            }
          
	    if (data.ipaddrtype == 2) {
	      $('#poolip_assigned').html(data.ipaddress);  
	    }else{
	      $('input[name="ipaddress"]').val(data.ipaddress);
	    }
	    if (typeof data.mac != 'undefined') {
	      $('#curr_macid').html(data.mac);  
	    }
          
	    if (typeof data.radchk_clearid != 'undefined') {
	      $('#radchk_clearid').val(data.radchk_clearid);
	    }else{
	      $('.ifauthsetting').addClass('hide');
	    }
	    if (typeof data.radchk_simuseid != 'undefined') {
	      $('#radchk_simuseid').val(data.radchk_simuseid);
	    }else{
	      $('.ifauthsetting').addClass('hide');
	    }
          
	    $("input[name=bindmaclock][value=" + data.maclockedstatus + "]").attr('checked', 'checked');
	    $("input[name=logintype][value=" + data.logintype + "]").prop('checked', true);
	    if (data.logintype == 'hotspot') {
	      $('.addhotspotMacID').removeClass('hide');
	      $('#hotspotMacID').attr('required',true);
	    }else{
		$('.addhotspotMacID').addClass('hide');
		$('#hotspotMacID').prop('required',false);
	    }
	    $("input[name=hotspotMacID]").val(data.hotspotMac);
	    
	    if (data.logintype == 'ill_ipuser') {
		$('.addhotspotMacID').addClass('hide');
		$('#hotspotMacID').attr('required',false);
		$('.ill_userpanel').removeClass('hide');
		$('.not_ill_userpanel').addClass('hide');
		$('input[name="ill_iptype"][value="'+data.iprange_type+'"]').prop('checked', true);
		get_illnaslisting(data.nasname);
		get_ipranges(data.ip_assigned);
	    }else{
		$('.ill_userpanel').addClass('hide');
		$('.not_ill_userpanel').removeClass('hide');
		$('select[name="nas_illoptions"]').empty().append('<option value="">Select NAS</option>');
		$('select[name="nas_iprange[]"]').empty();
		$('select[name="nas_illoptions"]').attr('required',false);
		$('select[name="nas_iprange[]"]').attr('required',false);
	    }
	    
	    $('.loading').addClass('hide');
	}
    });
});
function system_setting() {
    var cfm = confirm("Are you sure, you want to save (if any changes done) ? \n Note: Changes may affect the Router Setting for this UID ");
    if (cfm) {
	var logintype = $('input[name="logintype"]:checked').val();
	if (logintype == 'ill_ipuser') {
	    $('#ipaddress').attr('required',false);
	    $('#ipaddress_pool').attr('required',false);
	}
	var staticIP = $("#ipaddress").val();
	validIP = ValidateIPaddress(staticIP);
	if (validIP) {
	    $('.loading').removeClass('hide');
	    var formdata  = $('#syssetting_form').serialize();
	    $.ajax({
		url: base_url+'user/update_syssetting',
		type: 'POST',
		dataType: 'json',
		data: formdata,
		success: function(data){
		    if (data.ipexists == '1') {
			$('#staticipaddr_err').html('This IP Address is alreay associated.');
		    }else{
			$("input[name=ipaddrtype][value=" + data.ipaddrtype + "]").attr('checked', 'checked');
			if (data.ipaddrtype == 0) {
			    $('.addstaticip').removeClass('hide');
			    $('#ipaddress').attr('required',true);
			    $('.addippoolip').addClass('hide');
			    $('#ipaddress_pool').attr('required',false);
			}else if (data.ipaddrtype == 1) {
			    $('.addstaticip').addClass('hide');
			    $('#ipaddress').attr('required',false);
			    $('.addippoolip').addClass('hide');
			    $('#ipaddress_pool').attr('required',false);
			}else if (data.ipaddrtype == 2) {
			    $('.addstaticip').addClass('hide');
			    $('#ipaddress').attr('required',false);
			    $('.addippoolip').removeClass('hide');
			    $('#ipaddress_pool').attr('required',true);
			    ippooladdr_list();
			}
			if (data.ipaddrtype == 2) {
			    $('#poolip_assigned').html(data.ipaddress);  
			}else{
			    $('input[name="ipaddress"]').val(data.ipaddress);
			}
			if (typeof data.mac != 'undefined') {
			  $('#curr_macid').html(data.mac);  
			}
			$("input[name=bindmaclock][value=" + data.maclockedstatus + "]").attr('checked', 'checked');
			$("input[name=logintype][value=" + data.logintype + "]").attr('checked', 'checked');
			$("input[name=hotspotMacID]").val(data.hotspotMac);
		    }
		    $('.loading').addClass('hide');
		}
	    });
	}else{
	    $('#staticipaddr_err').html('Please enter valid IP Address.');
	}
    }
}

function ippooladdr_list() {
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    $.ajax({
        url: base_url+'user/ippooladdr_list',
        type: 'POST',
        data: 'uuid='+cust_uuid,
        success: function(data){
           $('#ipaddress_pool').empty().append(data);
        }
    });
}

function ValidateIPaddress(ipaddress){
    if (ipaddress != '') { 
        if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress))  {
            return true;  
        }else{
            return false;
        }
    }else{
        return true;
    }
}

function defaultsettingtab_details() {
    $('.loading').removeClass('hide');
    $('#staticipaddr_err').html('');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    $.ajax({
       url: base_url+'user/view_syssetting',
       type: 'POST',
       dataType: 'json',
       data: 'uuid='+cust_uuid,
       success: function(data){
          $("input[name=ipaddrtype][value=" + data.ipaddrtype + "]").attr('checked', 'checked');
            if (data.ipaddrtype == 0) {
                $('.addstaticip').removeClass('hide');
                $('#ipaddress').attr('required',true);
                $('.addippoolip').addClass('hide');
                $('#ipaddress_pool').attr('required',false);
            }else if (data.ipaddrtype == 1) {
                $('.addstaticip').addClass('hide');
                $('#ipaddress').attr('required',false);
                $('.addippoolip').addClass('hide');
                $('#ipaddress_pool').attr('required',false);
            }else if (data.ipaddrtype == 2) {
                $('.addstaticip').addClass('hide');
                $('#ipaddress').attr('required',false);
                $('.addippoolip').removeClass('hide');
                $('#ipaddress_pool').attr('required',true);
                ippooladdr_list();
            }
          
          if (data.ipaddrtype == 2) {
            $('#poolip_assigned').html(data.ipaddress);  
          }else{
            $('input[name="ipaddress"]').val(data.ipaddress);
          }
          if (typeof data.mac != 'undefined') {
            $('#curr_macid').html(data.mac);  
          }
          
          if (typeof data.radchk_clearid != 'undefined') {
            $('#radchk_clearid').val(data.radchk_clearid);
          }
          if (typeof data.radchk_simuseid != 'undefined') {
            $('#radchk_simuseid').val(data.radchk_simuseid);
          }
          $("input[name=bindmaclock][value=" + data.maclockedstatus + "]").attr('checked', 'checked');
          $("input[name=logintype][value=" + data.logintype + "]").attr('checked', 'checked');
          $("input[name=hotspotMacID]").val(data.hotspotMac);
          $('.loading').addClass('hide');
       }
    });
}


/**************************************** SETTING TABS ENDS HERE ************************************/

function defaultkyctab_details() {
    $('#subscriber_documents_form')[0].reset();
    $('#default_iddocdiv').removeClass('hide');
    $('.idproofdocdiv ').remove();
    $('#idprooflisting').html('');
    $('#defaultdocdiv').removeClass('hide');
    $('.subsdocdiv').remove();
    $('#kyclisting').html('');
    $('#docpreview').html('');
    $('.idproof_docnumber').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched');
    $('.kycdocnumber').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched');
    $('#user_image').html('');
    $('#user_signature').html('');
    
    $('#default_corporatedocdiv').removeClass('hide');
    $('.corpproofdocdiv ').remove();
    $('#corporatelisting').html('');
    
    $('#idproof_docnumber_arr').val('');
    $('#idproof_doctype_arr').val('');
    $('#kyc_documents_arr').val('');
    $('#kycdocnumber_arr').val('');
    $('#corporate_docnumber_arr').val('');
    $('#corporate_doctype_arr').val('');
    
    filecount = 0; filearr = 0; filedoctype = []; kycdocnumb = []; abc = 0; sno = 0;
    idp_filecount = 0; idp_filearr = 0; idp_filedoctype = []; iddocnumb = []; idp_abc = 0; idp_sno = 0;
    corp_filecount = 0; corp_filearr = 0; corp_filedoctype = []; corpdocnumb = []; corp_abc = 0; corp_sno = 0;
    
    edit_allkyc_details();
}

function defaultplantab_details() {
    $('#active_plan').css('color', '#424143');
    $('#next_cycle_plan').css('color', '#29abe2');
    $('#subscriber_plandetail_form').removeClass('hide');
    $('#nextcycle_planform').addClass('hide');
    subscriber_plan_details();
}




$('body').on('change', '#user_activate_check', function(){
    var user_activate_check = $(this).val();
    var subsid = $('.subscriber_userid').val();
    $('#user_activate_check').val(0);
    $('#user_activate_check').parent().removeClass('btn-success');
    $('#user_activate_check').parent().addClass('btn-danger off');
    $('#userstatus_changealert').val(user_activate_check);
    if (subsid == '') {
        $('#user_inactiveAlert').modal('show');
    }else{
	$('#userverifcation_toActiveModal').modal('show');
    }
});



$('body').on('change', '#userstatus_changealert', function(){
    var currtstatus = $('#userstatus_changealert').val();
    var account_activated_on = $('#account_activated_on').val();
    if ((currtstatus == 1) && (account_activated_on != '')) {
        $('#userstatus_changealert').parent().removeClass('btn-danger off');
        $('#userstatus_changealert').parent().addClass('btn-success on');
        $('#userstatus_changealertModal').modal('show');
    }else if ((currtstatus == 0) && (account_activated_on != '')) {
        $('#userstatus_changealert').parent().removeClass('btn-success');
        $('#userstatus_changealert').parent().addClass('btn-danger off');
        $('#userstat_toActiveModal').modal('show');
    }else{
	 $('#userstatus_changealert').parent().removeClass('btn-success');
        $('#userstatus_changealert').parent().addClass('btn-danger off');
	$('#userverifcation_toActiveModal').modal('show');
       
    }
});


$(document).on('click','.verifyuser',function(){
    
      var user_activate_check = $('#userstatus_changealert').val();
         var subsid = $('.subscriber_userid').val();
        $.ajax({
           url: base_url+'user/user_activate_check',
           type: 'POST',
           data: 'subsid='+subsid+'&is_user_activated='+user_activate_check+'&otp_verify=1',
           dataType: 'json',
           success: function(data){
                var otp = data.useract_otp;
                var personal_details = data.personal_details;
                var kyc_details = data.kyc_details;
                var plan_details = data.plan_details;
                var installation_details = data.installation_details;
                var security_details = data.security_details;
                
                if (otp != '0') {
		    $('.loading').removeClass('hide');
                    $('#user_inactiveAlert').modal('hide');
                    $('#otp_to_activate_user').val(otp);
                    $('#activate_username').html(data.username);
                    var custphone = data.phone;
                    $('#phone_to_activate_user').val(custphone);
                    for (idx=2; idx < 7 ; idx++) {
                        custphone = custphone.substring(0, idx) + '*' + custphone.substring(idx+1);
                    }
                    $('#activate_userphone').html(custphone);
                    
                    $('#userstatus_changealert').parent().removeClass('btn-success');
                    $('#userstatus_changealert').parent().addClass('btn-danger off');
		    $('#userverifcation_toActiveModal').modal('hide');
                    $('#user_activatedModal').modal('show');
		  
                }else{
                    $('#userstatus_changealert').val(0);
                    $('#userstatus_changealert').parent().removeClass('btn-success');
                    $('#userstatus_changealert').parent().addClass('btn-danger off');
                     $('#userverifcation_toActiveModal').modal('hide');
                    if (personal_details == 0) {
                        $('#pedactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                    }else{
                        $('#pedactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                    }
                    if (kyc_details == 0) {
                        $('#kycactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                    }else{
                        $('#kycactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                    }
                    if (plan_details == 0) {
                        $('#planactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                    }else{
                        $('#planactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                    }
                    if ((installation_details == 0) || (security_details == 0)) {
                        $('#inscactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                    }else{
                        $('#inscactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                    }
                    
                    $('#user_inactiveAlert').modal('show');
                }
           }
        });
    
});




$(document).on('click','.notverifyuser',function(){
    var user_activate_check = $('#userstatus_changealert').val();
    var subsid = $('.subscriber_userid').val();
    $.ajax({
       url: base_url+'user/user_activate_check',
       type: 'POST',
       data: 'subsid='+subsid+'&is_user_activated='+user_activate_check+'&otp_verify=0',
       dataType: 'json',
       success: function(data){
	    var otp = data.useract_otp;
	    var personal_details = data.personal_details;
	    var kyc_details = data.kyc_details;
	    var plan_details = data.plan_details;
	    var installation_details = data.installation_details;
	    var security_details = data.security_details;
	    
	    if (otp != '0') {
		$('.loading').removeClass('hide');
		$('#user_inactiveAlert').modal('hide');
		$('#otp_to_activate_user').val(otp);
		$('#activate_username').html(data.username);
		var custphone = data.phone;
		$('#phone_to_activate_user').val(custphone);
		for (idx=2; idx < 7 ; idx++) {
		    custphone = custphone.substring(0, idx) + '*' + custphone.substring(idx+1);
		}
		$('#activate_userphone').html(custphone);
		
		$('#userstatus_changealert').parent().removeClass('btn-success');
		$('#userstatus_changealert').parent().addClass('btn-danger off');
		//$('#user_activatedModal').modal('show');
		 $('#userverifcation_toActiveModal').modal('hide');
		var subsid = $('.subscriber_userid').val();
		var subs_uuid = $('.subscriber_uuid').val();
		var phone = $('#phone_to_activate_user').val();

		var formdata = 'subsid='+subsid+'&subs_uuid='+subs_uuid+'&phone='+phone;
		$('#user_activatedModal').modal('hide');
		$.ajax({
		    url: base_url+'user/active_user_confirmation',
		    type: 'POST',
		    dataType: 'json',
		    async: false,
		    data: formdata,
		    success: function(data){
			window.location = base_url+'user/edit/'+subsid;
		    }
		});
	    }else{
		$('#userstatus_changealert').val(0);
		$('#userstatus_changealert').parent().removeClass('btn-success');
		$('#userstatus_changealert').parent().addClass('btn-danger off');
		 $('#user_activatedModal').modal('hide');
		  $('#userverifcation_toActiveModal').modal('hide');
		if (personal_details == 0) {
		    $('#pedactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
		}else{
		    $('#pedactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
		}
		if (kyc_details == 0) {
		    $('#kycactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
		}else{
		    $('#kycactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
		}
		if (plan_details == 0) {
		    $('#planactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
		}else{
		    $('#planactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
		}
		if ((installation_details == 0) || (security_details == 0)) {
		    $('#inscactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
		}else{
		    $('#inscactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
		}
		
		$('#user_inactiveAlert').modal('show');
	    }
       }
    });
    
});


$('body').on('change', '.inactivate_type', function(){
    var inactivate_type = $('input[name="inactivate_type"]:checked').val();
    var suspend_days = $('select[name="suspend_days"]').val();
    if ((inactivate_type == 'suspended') && (suspend_days == '')) {
        $('select[name="suspend_days"]').attr('required', true);
    }else if(inactivate_type == 'terminate'){
        $('select[name="suspend_days"]').attr('required', false);
    }
    
});
function update_userstatus(){
    var inactivate_type = $('input[name="inactivate_type"]:checked').val();
   // var suspend_days = $('select[name="suspend_days"]').val();
    var suspend_days = 0;
    var uuid = $('.subscriber_uuid').val();
    var userid = $('.subscriber_userid').val();
    
    $.ajax({
        url: base_url+'user/updateuser_profilestatus',
        type: 'POST',
        data: 'uuid='+uuid+'&inactivate_type='+inactivate_type+'&suspend_days='+suspend_days,
        success: function(){
	    if (inactivate_type == 'terminate') {
		window.location = base_url+'user/status/act';
	    }else{
		window.location = base_url+'user/edit/'+userid;
	    }
        }
    });
}
function resetuser_activate() {
    var uuid = $('.subscriber_uuid').val();
    var userid = $('.subscriber_userid').val();
    
    $.ajax({
        url: base_url+'user/resetuser_toactivate',
        type: 'POST',
        data: 'uuid='+uuid,
        success: function(){
           window.location = base_url+'user/edit/'+userid;
        }
    });
}

function inactiveuser_pendingdocs(subsid) {
    $.ajax({
        url: base_url+'user/user_activate_check',
        type: 'POST',
        data: 'subsid='+subsid+'&is_user_activated=0',
        dataType: 'json',
        success: function(data){
             var otp = data.useract_otp;
             var personal_details = data.personal_details;
             var kyc_details = data.kyc_details;
             var plan_details = data.plan_details;
             var installation_details = data.installation_details;
             var security_details = data.security_details;
             
             if (otp != '0') {
		$('.loading').removeClass('hide');
                 $('#user_inactiveAlert').modal('hide');
                 $('#otp_to_activate_user').val(otp);
                 $('#activate_username').html(data.username);
                 var custphone = data.phone;
                 $('#phone_to_activate_user').val(custphone);
                 for (idx=2; idx < 7 ; idx++) {
                     custphone = custphone.substring(0, idx) + '*' + custphone.substring(idx+1);
                 }
                 $('#activate_userphone').html(custphone);
                 
                 $('#user_activate_check').parent().removeClass('btn-danger off');
                 $('#user_activate_check').parent().addClass('btn-success on');
                 //$('#user_activatedModal').modal('show');
		 
		var subsid = $('.subscriber_userid').val();
		var subs_uuid = $('.subscriber_uuid').val();
		var phone = $('#phone_to_activate_user').val();

		var formdata = 'subsid='+subsid+'&subs_uuid='+subs_uuid+'&phone='+phone;
		$('#user_activatedModal').modal('hide');
		$.ajax({
		    url: base_url+'user/active_user_confirmation',
		    type: 'POST',
		    dataType: 'json',
		    async: false,
		    data: formdata,
		    success: function(data){
			window.location = base_url+'user/edit/'+subsid;
		    }
		});
             }else{
                 $('#user_activate_check').val(0);
                 $('#user_activate_check').parent().removeClass('btn-success');
                 $('#user_activate_check').parent().addClass('btn-danger off');
                 
                 if (personal_details == 0) {
                     $('#pedactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                 }else{
                     $('#pedactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                 }
                 if (kyc_details == 0) {
                     $('#kycactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                 }else{
                     $('#kycactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                 }
                 if (plan_details == 0) {
                     $('#planactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                 }else{
                     $('#planactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                 }
                 if ((installation_details == 0) || (security_details == 0)) {
                     $('#inscactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                 }else{
                     $('#inscactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                 }
                 
                 $('#user_inactiveAlert').modal('show');
             }
        }
     });
}

function resetUserMac() {
    $('#resetMacModal').modal('hide');
    $('.modal-backdrop').remove();
    $('.loading').removeClass('hide');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    $.ajax({
       url: base_url+'user/resetUserMac',
       type: 'POST',
       dataType: 'json',
       data: 'uuid='+cust_uuid,
       success: function(data){
            $('.loading').addClass('hide');
            //defaultsettingtab_details();
       }
    });
}

function show_editrouterpasswordModal(radiuspassword) {
    $('#editrouterpasswordModal').modal('show');
    $('#routerpassword').val(radiuspassword);
}

function editrouterpassword() {
    $('#editrouterpasswordModal').modal('hide');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    var custid = $('input[name="subscriber_userid"]').val();
    var radiuspassword = $('#routerpassword').val();
    $.ajax({
        url: base_url+'user/editrouterpassword',
        type: 'POST',
        dataType: 'json',
        data: 'uuid='+cust_uuid+'&radiuspassword='+radiuspassword,
        success: function(result){
            window.location = base_url+'user/edit/'+custid;
        }
    });
}


function usuage_logs() {
   
    $('.loading').removeClass('hide');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    var date_range = $('input[name="datefilter"]').val();
    $.ajax({
       url: base_url+'user/usuage_logs',
       type: 'POST',
       dataType: 'json',
       async: false,
       data: 'uuid='+cust_uuid+"&date_range="+date_range,
       success: function(result){
	    $('.loading').addClass('hide');
	    $("#total_used").html(result.total_used);
	    $("#total_download").html(result.total_download);
	    $("#total_upload").html(result.total_upload);
	 
	    var logdate_range = $('input[name="activitylogfilter"]').val();
	    $.ajax({
		url: base_url+'user/list_user_activitylogs',
		type: 'POST',
		async: false,
		dataType: 'json',
		data: 'uuid='+cust_uuid+'&start=0&offset=10&date_range='+logdate_range,
		success: function(result1){
		    if (result1.recnum != 0) {
			var lidiv = '';
			var currpgno = $('.curr_pgno').text();
			var nxtpgno = parseInt(currpgno) + 1;
			var total_pages = result1.total_pages;
			lidiv += '<nav aria-label="Page navigation" class="pull-right"><ul class="pagination pagination-lg">';
			lidiv += '<li class="page-item disabled"><a class="page-link" href="#" tabindex="-1">Previous</a></li>';
			for (var i=1; i<=total_pages; i++) {
			    var bgcolor = '';
			    if (i == 1) {
				bgcolor = 'style="background-color:#f1f1f1"';
			    }
			    lidiv += '<li class="page-item"><a class="page-link" href="javascript:void(0)" onclick="activitylogs_pagination('+i+')" '+bgcolor+' >'+i+'</a></li>';
			}
			lidiv += '<li class="page-item"><a class="page-link" href="javascript:void(0)" onclick="activitylogs_pagination('+nxtpgno+')">Next</a></li>';
			lidiv += '</ul></nav>';
			$('#pgnavli').html(lidiv);
		    }else{
			$('#pgnavli').html('');
		    }
		    $('#useractivitylogs').html(result1.activitylogs);
		}
	    });
	    
	    if (result.superadmin_perm != 1) {
		if ((typeof result.isreadonly_permission != 'undefined') && (result.isreadonly_permission == 1)) {
		    $('.activity_logstab').find('input').prop('disabled', true);
		}else{
		    $('.activity_logstab').find('input').prop('disabled', false);
		}
	    }
	 
	    var d = new Date();
	    var months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
	    var currmonth = months[d.getMonth()];
	    setTimeout(function(){
	    var chart = new CanvasJS.Chart("chartContainer", {
		animationEnabled: true,
		title: {
		    text: "DATA USUAGE OF "+currmonth.toUpperCase(),
		     fontSize:18,
		},
		axisX:{
		    title: "Number of Days",
		},
		axisY:{
		    title: "Data Usuage",
		    //interval:1024,
		    suffix: " MB"
		},
		dataPointMaxWidth: 40,
		data: [
		    {
			type: "stackedColumn",
			legendText: "Between 00:00 to 11:59",
			showInLegend: "true",
			dataPoints: result.firsthalfdata
		    },
		    {
			type: "stackedColumn",
			legendText: "Between 12:00 to 23:59",
			showInLegend: "true",
			dataPoints: result.secondhalfdata
		    }
		]
	    });
	    chart.render();
	    },1000);
	    
       }
    });	
   
}

function activitylogs_pagination(pgno) {
    $('#useractivitylogs').html('<img src="'+base_url+'assets/images/loader.svg"/>');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    var start = 0;
    var prevclass = 'disabled';
    var prevaction = '';
    var nxtpgno = parseInt(pgno) + 1;
    var nxtclass = '';
    var nxtaction = 'onclick="activitylogs_pagination('+nxtpgno+')"';
    
    if (pgno > 1) {
	start = ((pgno - 1) * 10);
	prevclass = '';
	prevpgno = parseInt(pgno) - 1;
	prevaction = 'onclick="activitylogs_pagination('+prevpgno+')"';
    }
    var logdate_range = $('input[name="activitylogfilter"]').val();
    $.ajax({
	url: base_url+'user/list_user_activitylogs',
	type: 'POST',
	async: false,
	dataType: 'json',
	data: 'uuid='+cust_uuid+'&start='+start+'&offset=10&date_range='+logdate_range,
	success: function(result){
	    if (result.recnum != 0) {
		var lidiv = '';
		var total_pages = result.total_pages;
		lidiv += '<nav aria-label="Page navigation" class="pull-right"><ul class="pagination pagination-lg">';
		lidiv += '<li class="page-item '+prevclass+'"><a class="page-link" href="javascript:void(0)" '+prevaction+' tabindex="-1">Previous</a></li>';
		for (var i=1; i<=total_pages; i++) {
		    var bgcolor = '';
		    if (i == pgno) {
			bgcolor = 'style="background-color:#f1f1f1"';
		    }
		    lidiv += '<li class="page-item"><a class="page-link" href="javascript:void(0)" onclick="activitylogs_pagination('+i+')" '+bgcolor+'>'+i+'</a></li>';
		}
		if (total_pages == pgno) {
		    nxtclass = 'disabled';
		    nxtaction = '';
		}
		lidiv += '<li class="page-item '+nxtclass+'"><a class="page-link" href="javascript:void(0)" '+nxtaction+'>Next</a></li>';
		lidiv += '</ul></nav>';
		$('#pgnavli').html(lidiv);
	    }else{
		$('#pgnavli').html('');
	    }
	    $('.curr_pgno').html(pgno);
	    $('#useractivitylogs').html(result.activitylogs);
	}
    });
}

function data_logs() {
   
   $('.loading').removeClass('hide');
   var cust_uuid = $('input[name="subscriber_uuid"]').val();
   var date_range = $('#data_graph_value').val();
   $.ajax({
      url: base_url+'user/data_logs',
      type: 'POST',
      dataType: 'json',
      data: 'uuid='+cust_uuid+"&date_range="+date_range,
      success: function(result){
        $('.loading').addClass('hide');
          var chart = new CanvasJS.Chart("DatachartContainer",
        {
            title:{
                text: "Speed Graph",
                 fontSize:18,
            },  
            animationEnabled: true,  
            axisY: {
                           //interval:100,
                           suffix: " KB"
                /*title: "Units Sold",
                valueFormatString: "#0,,.",
                suffix: " m"*/
            },
            data: [
            {        
                
                type: "splineArea",
                color: "#90b040",
                markerSize: 1,
                dataPoints: result.input
            }  ,{        
                type: "splineArea",
                color: "#8080c0",
                markerSize: 1,
                dataPoints: result.output
            }            
            ]
        });                chart.render();
      }
   });
}




function showconfirmplan_modal() {
    var nxtcycleplan = $('select[name="nextassign_plan"] option:selected').val();
    if (nxtcycleplan != '') {
        var cust_uuid = $('input[name="subscriber_uuid"]').val();
        $.ajax({
            url: base_url+'user/calcPrevPlanCost',
            type: 'POST',
            data: 'uuid='+cust_uuid,
            dataType: 'json',
            success: function(data){
                if (data.is_expired != 1) {
                    $('#datacostxt').html(data.currency+' '+data.total_data_cost+' for '+data.total_data);
                    $('#dayscostxt').html(data.currency+' '+data.total_days_cost+' for '+data.total_days+' days');
                    $('#datacost').val(data.total_data_cost);
                    $('#dayscost').val(data.total_days_cost);
                    $('#prev_srvid').val(data.srvid);
                    $('#prev_billid').val(data.prev_billid);
                    $('#prev_billpaid').val(data.prev_billpaid);
                    $('#prev_user_plantype').val(data.user_plan_type);
                    $('#changePlanModal').modal('show');    
                }else{
                    $('#planChangeAlertModal').modal('hide');
                    $('#alertMsgTxt').html("User get expired. Please recharge.");
                    $('#alertModal').modal('show');
                }
            }
        });
    }else{
        $('select[name="nextassign_plan"]').attr('required', true);
    }
}

function apply_planfornow() {
    $('#planChangeAlertModal').modal('hide');
    $('#changePlanModal').modal('hide');
    $('.loading').removeClass('hide');
    var subsid = $('.subscriber_userid').val();
    var nxtcycleplan = $('select[name="nextassign_plan"] option:selected').val();
    var nextuser_plan_type = $('input[name="nextuser_plan_type"]:checked').val();
    var custom_srvid = $('.custom_planid').val();
    var custom_planprice = $('.custom_planprice').val();
    
    var nxtprebill_oncycle = 0;
    if (nextuser_plan_type == 'postpaid') {
	$('input[name="nextprebill_oncycle"]').prop('checked', false);
    }else if (nextuser_plan_type == 'prepaid'){
	if($('input[name="nextprebill_oncycle"]').is(':checked')){
	    nxtprebill_oncycle = 1;
	}
    }
    var formdata  = $('#changePlanForm').serialize();
    
    //alert(formdata+'&nextplanid='+nxtcycleplan+'&nextuser_plan_type='+nextuser_plan_type+'&custom_planprice='+custom_planprice+'&nextprebill_oncycle='+nxtprebill_oncycle);
    if (nxtcycleplan != '') {
        var formdata  = $('#changePlanForm').serialize();
        $.ajax({
            url: base_url+'user/apply_planfornow',
            type: 'POST',
            data: formdata+'&nextplanid='+nxtcycleplan+'&nextuser_plan_type='+nextuser_plan_type+'&custom_planprice='+custom_planprice+'&nextprebill_oncycle='+nxtprebill_oncycle,
            dataType: 'json',
            success: function(data){
                window.location = base_url+'user/edit/'+subsid;
            }
        });
    }
}

function cancel_nextcycleplan() {
    var subs_uuid = $('.subscriber_uuid').val();
    var subsid = $('.subscriber_userid').val();
    $.ajax({
        url: base_url+'user/nextcycle_planlist',
        type: 'POST',
        dataType: 'json',
        data: 'uuid='+subs_uuid,
        async: false,
        success: function(data){
            var srvid = data.nextplanid;
            if (srvid != 0) {
                var c = confirm("Are you sure, you want to cancel the scheduled plan for next cycle ?");
                if (c) {
                    $('.loading').removeClass('hide');
                    $.ajax({
                        url: base_url+'user/cancel_nextcycleplan',
                        type: 'POST',
                        dataType: 'json',
                        data: 'uuid='+subs_uuid+'&srvid='+srvid,
                        async: false,
                        success: function(data){
                            window.location = base_url+'user/edit/'+subsid;
                        }
                    });
                }
            }
        }
    });
}

function delete_inactiveUser(userid) {
    var c = confirm("Are you sure, you want to delete this user ?");
    if (c) {
        $.ajax({
            url: base_url+'user/delete_inactiveUser',
            type: 'POST',
            dataType: 'json',
            data: 'userid='+userid,
            success: function(data){
                window.location = base_url+'user/status/inact';
            }
        })
    }
}

function show_customize_plan_pricing_modal(){
    $('#custom_plan_price').val('');
    $('.custplanbtn').removeClass('hide');
    var srvid = $('select[name="nextassign_plan"] option:selected').val();
    if (typeof srvid == 'undefined') {
        var srvid = $('select[name="assign_plan"] option:selected').val();
    }
    var subs_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/getcurr_plan_pricing',
        type: 'POST',
        dataType: 'json',
        data: 'uuid='+subs_uuid+'&srvid='+srvid,
        success: function(data){
            $('#displaycurr_plan_price').html(data.currency+' '+data.plan_price);
            $('#custom_srvid_duration').val(data.plan_duration);
            $('#custom_srvid').val(srvid);
            $('#customPlanPriceModal').modal('show');
        }
    });
}

function addcustom_planprice(){
    $('.loading').removeClass('hide');
    $('.custplanbtn').addClass('hide');
    
    var custom_srvid = $('#custom_srvid').val();
    var custom_planprice = $('#custom_plan_price').val();
    var custom_srvid_duration = $('#custom_srvid_duration').val();
    var total_planprice = (custom_planprice * custom_srvid_duration);
    $('.custom_planid').val(custom_srvid);
    $('.custom_planprice').val(total_planprice);
    $('#nxt_plan_price').css('text-decoration', 'line-through');
    var cocurr = $('#ispcountry_currency').val();
    $('#nxt_custom_planprice').html('('+cocurr+' '+total_planprice+')');
    /* for first time user */
    $('#plan_price').css('text-decoration', 'line-through');
    $('#displaycustom_planprice').html('('+cocurr+' '+total_planprice+')');
    
    $('#customPlanPriceModal').modal('hide');
    $('.loading').addClass('hide');
}

function showenquiryfeedback() {
   $('#enquiryFeedbackModal').modal('show');
   $('#feedback_message').val('');
   var enq_userid = $('.enq_userid').val();
   $.ajax({
        url: base_url+'user/enquiryfeedback',
        type: 'POST',
        dataType: 'json',
        data: 'enq_userid='+enq_userid,
        async: false,
        success: function(data){
            $('#feedback_history').html(data);
        }
    });
}

function addenquiryfeedback() {
    var enq_userid = $('.enq_userid').val();
    var feedback_message = $('#feedback_message').val();
    if (feedback_message != '') {
        $.ajax({
            url: base_url+'user/addenquiryfeedback',
            type: 'POST',
            dataType: 'json',
            data: 'enq_userid='+enq_userid+'&feedback_message='+feedback_message,
            async: false,
            success: function(data){
                var feedback_message = $('#feedback_message').val('');
                $('#feedback_history').html(data);
            }
        });
    }
}

function radius_diagnostic() {
    $("#radius_diag_data_range").show();
    $('#authlogs_data').html('');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    var date_range = $('input[name="radius_datefilter"]').val();
     $('.loading').removeClass('hide');
    $.ajax({
	url: base_url+'user/radius_diagnostic',
	type: 'POST',
	
	data: 'cust_uuid='+cust_uuid+"&date_range="+date_range,
	async: false,
	dataType: 'json',
	success: function(data){
	   $("#radius_diag_data").html(data.radius_logs);
	   $('#total_download_logs').html(data.total_download);
	   $('#total_upload_logs').html(data.total_upload);
	   $('.loading').addClass('hide');
	}
    });
}

function mikrotik_logs_modal_open() {
    $("#radius_diag_data_range").hide();
    $("#radius_diag_data").html('');
    $("#mikrotik_log_modal").modal("show");
}

function mikrotik_log_detail() {
    $("#radius_diag_data_range").hide();
    $('.loading').removeClass('hide');
     var cust_uuid = $('input[name="subscriber_uuid"]').val();
     var router_ip = $('select[name="microtik_router_ip"] option:selected').val();
     var router_username = $('input[name="microtik_router_username"]').val();
     var router_password = $('input[name="microtik_router_password"]').val();
    $.ajax({
            url: base_url+'user/mikrotik_check_connection',
            type: 'POST',
            
            data: 'cust_uuid='+cust_uuid+"&router_ip="+router_ip+"&router_username="+router_username+"&router_password="+router_password,
            async: false,
            success: function(data){
		if (data != '') {
		   $('.loading').addClass('hide');
		    $("#mikrotic_router_error").html(data);  
		}else{
		    $("#mikrotic_router_error").html('');  
		    $.ajax({
			url: base_url+'user/mikrotik_log_detail',
			data: 'cust_uuid='+cust_uuid+"&router_ip="+router_ip+"&router_username="+router_username+"&router_password="+router_password,
			type: 'POST',
			async: false,
			success: function(data){
			    $("#mikrotik_log_modal").modal("hide");
			    $("#radius_diag_data").html(data);
			    $('.loading').addClass('hide');
			}
		    });
		}
               
	       
            }
    });
}

function router_authlogs(subs_uuid){
    $("#radius_diag_data_range").hide();
    $("#radius_diag_data").html('');
    $('.loading').removeClass('hide');
    $.ajax({
	type: "POST",
	url: base_url+"user/router_authlogs",
	data: "uid="+subs_uuid,
	success: function(data){
	    $('.loading').addClass('hide');
	    $('#authlogs_data').html(data);
	}
    });
}

function show_customize_planspeed_modal() {
    $('#editplanspeedModal').modal('show');
    $('input[name="editplan_uploadspeed"]').val('');
    $('input[name="editplan_downloadspeed"]').val('');
    var assigned_activeplan = $('select[name="assign_plan"] option:selected').val();
    $('#assigned_activeplan').val(assigned_activeplan);
}

function update_planspeed() {
    var formdata = $('#editplanspeed_form').serialize();
    $.ajax({
	type: "POST",
	url: base_url+"user/update_planspeed",
	data: formdata,
	success: function(data){
	    subscriber_plan_details();
	    $('#editplanspeedModal').modal('hide');
	}
    });
}

$('input[name="billaddr"]').on('change', function(){
    var billaddr = $(this).is(':checked');
    if (!billaddr) {
	$('.billingaddr_div').removeClass('hide');
	$('.requiredforbilling').attr('required', 'required');
    }else{
	$('.billingaddr_div').addClass('hide');
	$('.requiredforbilling').removeAttr('required');
    }
});

function radiuslogs_rptexport() {
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    var date_range = $('input[name="radius_datefilter"]').val();
    $('.loading').removeClass('hide');
    $.ajax({
	url: base_url+'user/radiuslogs_rptexport',
	type: 'POST',
	data: 'cust_uuid='+cust_uuid+"&date_range="+date_range,
	async: false,
	dataType: 'json',
	success: function(data){
	    $('.loading').addClass('hide');
	    window.location = base_url+"reports/radiuslogs_report.xlsx";
	}
    });
}

$('body').on('click', '#user_diagnostic', function(){
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    $('.loading').removeClass('hide');
    $.ajax({
	url: base_url+'user/check_user_onlineoffline',
	type: 'POST',
	data: 'cust_uuid='+cust_uuid,
	async: false,
	dataType: 'json',
	success: function(userstatus){
	    $('.loading').addClass('hide');
	    if(userstatus == 'online'){
		status_indicator = "<img src='"+base_url+"assets/images/green.png' alt='' />";
	    }else{
		status_indicator = "<img src='"+base_url+"assets/images/red.png' alt='' />";
	    }
	    $('.user_onlineoffline_status').html(status_indicator);
	}
    });
});