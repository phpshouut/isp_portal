$(document).ready(function(){
    
    $(document).on('change','.persp_filter',function(){
        var filter=$(this).val();
	  var isp_uid= $('.franchise').find(":selected").val();
        $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/persp_user_filter",
            type:'POST',
             dataType:'json',
            data:{filter:filter,isp_uid:isp_uid},
           
            success:function (data){
               $('.loading').addClass('hide');
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#search_perspview').html(data.html);
		initialise_perspective_datatable();
               // $('#searchtext').removeClass('serch_loading');
            }
            
            
            
        });
     
    });
    
    
     $(document).on('click','.perspfltr',function(){
	
	 $('.persp_filter').find('option:selected').prop("selected", false);
	// $('.persp_filter').prop('selected', false);
        var filter=$(this).attr('rel');
	 var isp_uid= $('.franchise').find(":selected").val();
	 $('#perspclickfltr').val(filter);
        $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/persp_user_filter",
            type:'POST',
             dataType:'json',
            data:{filter:filter,isp_uid:isp_uid},
           
            success:function (data){
               $('.loading').addClass('hide');
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#search_perspview').html(data.html);
				initialise_perspective_datatable();
               // $('#searchtext').removeClass('serch_loading');
            }
            
            
            
        });
     
    });
     $(document).on('click','#perspuser',function(){
 
   /* $('.loading').removeClass('hide');
     setTimeout(function (){
			$('.loading').addClass('hide');
			
		     },1000);*/
   perspective_user_filter_default();
     });
     
     
      $(document).on('click','.perspexcel',function(){
	
	var filterclick =$('#perspclickfltr').val();
	var filterdrop=$('.persp_filter option:selected').val();
	if (filterclick=="all" && filterdrop=="all") {
	    //code
	    $('.perspexcelerr').html("Please Select Filter");
	    return false;
	}
	else
	{
	    if (filterdrop!="all") {
		var filter= filterdrop;
		 $('.perspexcelerr').html("");
	    }
	    else
	    {
		
		var filter= filterclick;
		 $('.perspexcelerr').html("");
	    }
	    
	}
        
	 var isp_uid= $('.franchise').find(":selected").val();
        $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/persp_user_filter_excel",
            type:'POST',
             dataType:'json',
            data:{filter:filter,isp_uid:isp_uid},
           
              success:function (data){
		if (data.resultcode==0) {
		   $('.perspexcelerr').html("No Data Found.");
		   $(".loading").addClass('hide');
		}
		else
		{
		     window.location = base_url+"report/persp_user_filter_excel_dwnld";
                        $(".loading").addClass('hide');
		}
	     }
            
            
            
        });
     
    });
     
    
    //Active user start
    
    $(document).on('click','#actvuser',function(){
        
        active_user_default();
        
    });
    
    
    
      $(document).on('click','.daysfiletr',function(){
        
        var filter=$(this).attr('rel');
	 var isp_uid= $('.franchise').find(":selected").val();
        $('#daysfilter').val(filter);
        $('.daysfiletr').removeClass('textactive');
        $(this).addClass('textactive');
          $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/active_user",
            type:'POST',
             dataType:'json',
            data:{daysfilter:filter,isp_uid:isp_uid},
           
            success:function (data){
              //clear the table, if it exists
         
               
         
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#totalactive').html(data.total_count);
                $('#last30days').html(data.last30count);
                $('#last180days').html(data.last180count);
                $('#more180days').html(data.more180count);
                $('#totalsusp').html(data.totaloffsuspon);
                 $('#onlinecount').html(data.online);
                $('#offlinecount').html(data.offline);
                $('#suspcount').html(data.suspended);
                $('#search_activegrid').html(data.html);
                
                        
                        
               // $('#searchtext').removeClass('serch_loading');
               CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#36465F",
                "#E4AF23",
                "#F4474A"
                              
                ]);
               
               var chart = new CanvasJS.Chart("activeuserContainer",
	{
             colorSet: "brownShades",
		
		data: [
		{
			/*type: "pie",
                        toolTipContent: " #percent %",
                        indexLabel: "#percent%",*/
                         //showInLegend: true,
                         type: "doughnut",
			indexLabelFontFamily: "'Open Sans', sans-serif;",       
			indexLabelFontSize: 20,
			indexLabelFontWeight: "bold",
			//startAngle:0,
			indexLabelFontColor: "MistyRose",       
			indexLabelLineColor: "darkgrey", 
			indexLabelPlacement: "inside", 
			toolTipContent: "#percent %",
			//showInLegend: true,
			indexLabel: "#percent%", 
			dataPoints: [
                            { y: data.online},
				{ y: data.offline},
				{ y: data.suspended}
				
			]
		}
		]
	});
	chart.render();
         $('.loading').addClass('hide');
        initialise_active_datatable();
       
               
            }
              
            
            
        });
        
    });
    
    
      $('.filterstatus').change(function(){
        var daysfilter=$('#daysfilter').val() ;
        var filter=$(this).val();
	 var isp_uid= $('.franchise').find(":selected").val();
        $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/active_user",
            type:'POST',
             dataType:'json',
            data:{daysfilter:daysfilter,filter:filter,isp_uid:isp_uid},
           
            success:function (data){
                
             
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#totalactive').html(data.total_count);
                $('#last30days').html(data.last30count);
                $('#last180days').html(data.last180count);
                $('#more180days').html(data.more180count);
                $('#totalsusp').html(data.totaloffsuspon);
                 $('#onlinecount').html(data.online);
                $('#offlinecount').html(data.offline);
                $('#suspcount').html(data.suspended);
                $('#search_activegrid').html(data.html);
               
                        
                        
               // $('#searchtext').removeClass('serch_loading');
               CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#36465F",
                "#E4AF23",
                "#F4474A"
                              
                ]);
               
               var chart = new CanvasJS.Chart("activeuserContainer",
	{
             colorSet: "brownShades",
		
		data: [
		{
			/*type: "pie",
                        toolTipContent: " #percent %",
                        indexLabel: "#percent%",*/
                         //showInLegend: true,
                         type: "doughnut",
			indexLabelFontFamily: "'Open Sans', sans-serif;",       
			indexLabelFontSize: 20,
			indexLabelFontWeight: "bold",
			//startAngle:0,
			indexLabelFontColor: "MistyRose",       
			indexLabelLineColor: "darkgrey", 
			indexLabelPlacement: "inside", 
			toolTipContent: "#percent %",
			//showInLegend: true,
			indexLabel: "#percent%", 
			dataPoints: [
                            { y: data.online},
				{ y: data.offline},
				{ y: data.suspended}
				
			]
		}
		]
	});
	chart.render();
        $('.loading').addClass('hide');
        initialise_active_datatable();
        
               
            }
              
            
            
        });
        
    });
    
     $(document).on('click','.actvfilter',function(){
        var daysfilter=$('#daysfilter').val();
        var filter=$(this).attr('rel');
	 var isp_uid= $('.franchise').find(":selected").val();
        $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/active_user",
            type:'POST',
             dataType:'json',
            data:{daysfilter:daysfilter,filter:filter,isp_uid:isp_uid},
           
            success:function (data){
                
             
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#totalactive').html(data.total_count);
                $('#last30days').html(data.last30count);
                $('#last180days').html(data.last180count);
                $('#more180days').html(data.more180count);
                $('#totalsusp').html(data.totaloffsuspon);
                 $('#onlinecount').html(data.online);
                $('#offlinecount').html(data.offline);
                $('#suspcount').html(data.suspended);
                $('#search_activegrid').html(data.html);
               
                        
                        
               // $('#searchtext').removeClass('serch_loading');
               CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#36465F",
                "#E4AF23",
                "#F4474A"
                              
                ]);
               
               var chart = new CanvasJS.Chart("activeuserContainer",
	{
             colorSet: "brownShades",
		
		data: [
		{
			/*type: "pie",
                        toolTipContent: " #percent %",
                        indexLabel: "#percent%",*/
                         //showInLegend: true,
                         type: "doughnut",
			indexLabelFontFamily: "Garamond",       
			indexLabelFontSize: 20,
			indexLabelFontWeight: "bold",
			//startAngle:0,
			indexLabelFontColor: "MistyRose",       
			indexLabelLineColor: "darkgrey", 
			indexLabelPlacement: "inside", 
			toolTipContent: "#percent %",
			//showInLegend: true,
			indexLabel: "#percent%", 
			dataPoints: [
                            { y: data.online},
				{ y: data.offline},
				{ y: data.suspended}
				
			]
		}
		]
	});
	chart.render();
        $('.loading').addClass('hide');
        initialise_active_datatable();
        
               
            }
              
            
            
        });
        
    });
    
    
       $(document).on('click','.activeexcel',function(){
	
	var filterclick =$('#hnactiveusr').val();
	var filterdrop=$('.filterstatus option:selected').val();
	 var daysfilter=$('#daysfilter').val();
	if (filterclick=="all" && filterdrop=="all") {
	    //code
	    $('.activeexcelerr').html("Please Select Filter");
	    return false;
	}
	else
	{
	    if (filterdrop!="all") {
		var filter= filterdrop;
		 $('.activeexcelerr').html("");
	    }
	    else
	    {
		
		var filter= filterclick;
		 $('.activeexcelerr').html("");
	    }
	    
	}
        
	 var isp_uid= $('.franchise').find(":selected").val();
        $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/active_user_excel",
            type:'POST',
             dataType:'json',
            data:{filter:filter,isp_uid:isp_uid,daysfilter:daysfilter},
           
              success:function (data){
		if (data.resultcode==0) {
		   $('.activeexcelerr').html("No Data Found.");
		   $(".loading").addClass('hide');
		}
		else
		{
		     window.location = base_url+"report/active_user_filter_excel_dwnld";
                        $(".loading").addClass('hide');
		}
	     }
            
            
            
        });
     
    });
    
    
    
    
    
    
    //Complaint user start
    
    $(document).on('click','#complaint_report',function(){
        
	complaint_report_default();
    });
    
    
    
    
    
    




 $(document).on('click','.filter_comp',function(){
        
        var filter='all';
       var isp_uid= $('.franchise').find(":selected").val();
        var complaint_type=$(this).attr('rel');
        $('.filter_compchange').val(complaint_type);
         $('.loading').removeClass('hide');
          $('.filter_comp').removeClass('donut-btn-active');
          $(this).addClass('donut-btn-active');
        $.ajax({
            url:base_url+"report/compaint_request",
            type:'POST',
             dataType:'json',
            data:{filter:filter,complaint_type:complaint_type,isp_uid:isp_uid},
           
            success:function (data){
             
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                
                $('#search_compgrid').html(data.html);
                  $('#opent').html(data.openticket);
                 var abc =[];
          $.each(data.graph, function(k, v) {
             //display the key and value pair
              var objects = {label:k, y:v};
                abc.push(objects);
            
        })   
               
                  
                var chart = new CanvasJS.Chart("compuserContainer",
   {      
      theme:"theme2",
      title:{
        text: ""
      },
      animationEnabled: true,
      axisY :{
        includeZero: true,
        // suffix: " k",
        valueFormatString: "",
        suffix: ""
        
      },
      toolTip: {
        shared: "true"
      },
      data: [
      { 
            
        type: "spline", 
        // showInLegend: true,
        // markerSize: 0,
        name: "",
        dataPoints: abc
      } 
      

      ],
      legend:{
        cursor:"pointer",
        itemclick : function(e) {
          if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
          	e.dataSeries.visible = false;
          }
          else {
            e.dataSeries.visible = true;
          }
          chart.render();
        }
        
      },
    });

chart.render();
$('.loading').addClass('hide');
        initialise_comp_datatable();
        //
               
            }
              
            
            
        });
        
 });
      //Active Plan Top up start
    
    $(document).on('click','#plantopup',function(){
       
     plan_topup_default();
        
    });
    
    
      $(document).on('click','.pdaysfiletr',function(){
       var isp_uid= $('.franchise').find(":selected").val();
      var daysfilter=$(this).attr('rel'); 
         var planfilter="all"; 
           $('.pdaysfiletr').removeClass('textactive');
        $(this).addClass('textactive');
        $('#pdaysfiletr').val(daysfilter);
          $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/plan_topup_report",
            type:'POST',
             dataType:'json',
            data:{daysfilter:daysfilter,planfilter:planfilter,isp_uid:isp_uid},
           
            success:function (data){
             
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#ptotalactive').html(data.total_count);
                $('#plast30days').html(data.last30count);
                $('#plast180days').html(data.last180count);
                $('#pmore180days').html(data.more180count);
               
                $('#psearch_activegrid').html(data.html);
                   $('select[name="filterplant"]').html('').append('<option value="all" rel="all">all</option>');
                 $.each(data.plandropdown, function(k, v) {
             
              var result = k.split('::');
             $('select[name="filterplant"]').append('<option  value="'+result[0]+'" rel="'+result[1]+'">'+v+'</option>');
             
           });  
                
               
                    var i=1;
                    var j=1;
                     var abc =[];
                     var color=[];
                    $.each(data.plangraph, function(k, v) {
             //display the key and value pair
             if(i<=5){
              var objects = {label:k, y:v};
              var col="#0F485F";
                abc.push(objects);
                color.push(col);
            }
                i++;
            
        })       
        
         $.each(data.topupgraph, function(k, v) {
             //display the key and value pair
             if(j<=2){
              var objects = {label:k, y:v};
              var col="#FFBB00";
                abc.push(objects);
                color.push(col);
            }
                j++;
            
        })     
                        
               // $('#searchtext').removeClass('serch_loading');
               CanvasJS.addColorSet("brownShades",color
                );
               
               var chart = new CanvasJS.Chart("planuserContainer",
	{
             colorSet: "brownShades",
		
		data: [
		{
			
                         //showInLegend: true,
                         type: "column",

			dataPoints: abc
		}
		]
	});
	chart.render();
         $('.loading').addClass('hide');
        initialise_plan_datatable();
       
               
            }
              
            
            
        });
        
    });
      
        $(document).on('click','.plantopupexcel',function(){
       var isp_uid= $('.franchise').find(":selected").val();
      var daysfilter=$('#pdaysfilter').val(); 
         var planfilter=$('.filterplant option:selected').val();
	 if (planfilter=="all") {
	    $('.plantopuperr').html("Please Select Plan Filter");
	    return false;
	 }
	 else
	 {
	    $('.plantopuperr').html("");
	 }
	 
           //$('.pdaysfiletr').removeClass('textactive');
       // $(this).addClass('textactive');
      
          $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/plan_topup_report_excel",
            type:'POST',
             dataType:'json',
            data:{daysfilter:daysfilter,planfilter:planfilter,isp_uid:isp_uid},
           
            success:function (data){
             if (data.resultcode==0) {
		   $('.plantopuperr').html("No Data Found.");
		   $(".loading").addClass('hide');
		}
		else
		{
		     window.location = base_url+"report/plan_topup_report_excel_dwnld";
                        $(".loading").addClass('hide');
		}
               
            }
              
            
            
        });
        
    });
      
    
      $(document).on('change','.filterplant',function(){
       //alert('aaa');
       var isp_uid= $('.franchise').find(":selected").val();
      var daysfilter=$('#pdaysfilter').val(); 
         var planfilter=$(this).find('option:selected').attr('rel'); 
        
         var id=$(this).val();
        //  alert(daysfilter+":::"+planfilter+":::"+id);
         //  $('.pdaysfiletr').removeClass('textactive');
       // $(this).addClass('textactive');
       // $('#pdaysfiletr').val(daysfilter);
          $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/plan_topup_report",
            type:'POST',
             dataType:'json',
            data:{daysfilter:daysfilter,planfilter:planfilter,id:id,isp_uid:isp_uid},
           
            success:function (data){
             
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#ptotalactive').html(data.total_count);
                $('#plast30days').html(data.last30count);
                $('#plast180days').html(data.last180count);
                $('#pmore180days').html(data.more180count);
               
                $('#psearch_activegrid').html(data.html);
                
               
                
               
                    var i=1;
                    var j=1;
                     var abc =[];
                     var color=[];
                    $.each(data.plangraph, function(k, v) {
             //display the key and value pair
             if(i<=5){
              var objects = {label:k, y:v};
              var col="#0F485F";
                abc.push(objects);
                color.push(col);
            }
                i++;
            
        })       
        
         $.each(data.topupgraph, function(k, v) {
             //display the key and value pair
             if(j<=2){
              var objects = {label:k, y:v};
              var col="#FFBB00";
                abc.push(objects);
                color.push(col);
            }
                j++;
            
        })     
                        
               // $('#searchtext').removeClass('serch_loading');
               CanvasJS.addColorSet("brownShades",color
                );
               
               var chart = new CanvasJS.Chart("planuserContainer",
	{
             colorSet: "brownShades",
		
		data: [
		{
			
                         //showInLegend: true,
                         type: "column",

			dataPoints: abc
		}
		]
	});
	chart.render();
         $('.loading').addClass('hide');
        initialise_plan_datatable();
       
               
            }
              
            
            
        });
        
    });
    
    //Actibe Billing
    $(document).on('click','#billreport',function(){
       
     billing_report_default();
        
    });
    
    
     $(document).on('click','.bdaysfiletr',function(){
       
      var daysfilter=$(this).attr('rel'); 
         var filter="all";
	 var isp_uid= $('.franchise').find(":selected").val();
          $('.bdaysfiletr').removeClass('textactive');
        $(this).addClass('textactive');
        $('#bdaysfiletr').val(daysfilter);
          $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/billing_report",
            type:'POST',
             dataType:'json',
            data:{daysfilter:daysfilter,filter:filter,isp_uid:isp_uid},
           
            success:function (data){
             
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#btotalactive').html(data.total_count);
                $('#blast30days').html(data.last30count);
                $('#blast180days').html(data.last180count);
                $('#bmore180days').html(data.more180count);
               
                $('#bsearch_activegrid').html(data.html);
                $('#avgr').html("₹ "+data.avgrevenue+".00 /");
                
               
                
               
                    CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#0F485F",
                "#0F485F",
                "#0F485F",
                 "#0F485F",
                  "#0F485F"
                              
                ]); 
                        
                var chart = new CanvasJS.Chart("bperContainer",
	{
             colorSet: "brownShades",
                  title:{
			text: "CREDIT LIMIT"
		},
		
		data: [
		{
			 type: "column",
                      dataPoints: [
                                { label: "25%", y:data.tfperc},
				{ label: "50%", y: data.fzperc},
				{ label: "75%", y: data.sfperc },
                                { label: "90%", y: data.nzperc },
                                { label: "100%", y: data.ozzperc },
				
			]
		}
		]
	});
	chart.render();
        
        
           CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#FFBB00",
                "#FFBB00",
                "#FFBB00",
                 "#FFBB00"
                  
                              
                ]); 
                        
                var chart = new CanvasJS.Chart("bpayContainer",
	{
             colorSet: "brownShades",
                  title:{
			text: "PENDING PAYMENT"
		},
		
		data: [
		{
			 type: "column",
                      dataPoints: [
                                { label: "<₹500", y:data.pf},
				{ label: "₹500 - ₹1000", y: data.fpo},
				{ label: "₹1000 - ₹2000", y: data.opt },
                                { label: ">₹2000", y: data.pt },
                              
				
			]
		}
		]
	});
	chart.render();
         $('.loading').addClass('hide');
        initialise_bill_datatable();
       
               
            }
              
            
            
        });
        
    });
    
      $(document).on('change','.filterpercstatus',function(){
         $('.filterpaymstatus').find('option:selected').prop("selected", false);
	 $('.expirydays').find('option:selected').prop("selected", false);
      var daysfilter=$('#bdaysfiletr').val(); 
         var filter="perc"; 
         var filtertype=$(this).val();
	  var isp_uid= $('.franchise').find(":selected").val();
       
          $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/billing_report",
            type:'POST',
             dataType:'json',
            data:{daysfilter:daysfilter,filter:filter,filtertype:filtertype,isp_uid:isp_uid},
           
            success:function (data){
             
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#btotalactive').html(data.total_count);
                $('#blast30days').html(data.last30count);
                $('#blast180days').html(data.last180count);
                $('#bmore180days').html(data.more180count);
               
                $('#bsearch_activegrid').html(data.html);
                $('#avgr').html("₹ "+data.avgrevenue+".00 /");
                
               
                
               
                    CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#0F485F",
                "#0F485F",
                "#0F485F",
                 "#0F485F",
                  "#0F485F"
                              
                ]); 
                        
                var chart = new CanvasJS.Chart("bperContainer",
	{
             colorSet: "brownShades",
                  title:{
			text: "CREDIT LIMIT"
		},
		
		data: [
		{
			 type: "column",
                      dataPoints: [
                                { label: "25%", y:data.tfperc},
				{ label: "50%", y: data.fzperc},
				{ label: "75%", y: data.sfperc },
                                { label: "90%", y: data.nzperc },
                                { label: "100%", y: data.ozzperc },
				
			]
		}
		]
	});
	chart.render();
        
        
           CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#FFBB00",
                "#FFBB00",
                "#FFBB00",
                 "#FFBB00"
                  
                              
                ]); 
                        
                var chart = new CanvasJS.Chart("bpayContainer",
	{
             colorSet: "brownShades",
                  title:{
			text: "PENDING PAYMENT"
		},
		
		data: [
		{
			 type: "column",
                      dataPoints: [
                                { label: "<₹500", y:data.pf},
				{ label: "₹500 - ₹1000", y: data.fpo},
				{ label: "₹1000 - ₹2000", y: data.opt },
                                { label: ">₹2000", y: data.pt },
                              
				
			]
		}
		]
	});
	chart.render();
        $('.loading').addClass('hide');
        initialise_bill_datatable();
        
               
            }
              
            
            
        });
        
    });
    
    
     $(document).on('change','.filterpaymstatus',function(){
      
      var daysfilter=$('#bdaysfiletr').val(); 
         var filter="number"; 
         var filtertype=$(this).val();
	 var isp_uid= $('.franchise').find(":selected").val();
        $('.filterpercstatus').find('option:selected').prop("selected", false);
	$('.expirydays').find('option:selected').prop("selected", false);
          $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/billing_report",
            type:'POST',
             dataType:'json',
            data:{daysfilter:daysfilter,filter:filter,filtertype:filtertype,isp_uid:isp_uid},
           
            success:function (data){
             
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#btotalactive').html(data.total_count);
                $('#blast30days').html(data.last30count);
                $('#blast180days').html(data.last180count);
                $('#bmore180days').html(data.more180count);
               
                $('#bsearch_activegrid').html(data.html);
                $('#avgr').html("₹ "+data.avgrevenue+".00 /");
                
               
                
               
                    CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#0F485F",
                "#0F485F",
                "#0F485F",
                 "#0F485F",
                  "#0F485F"
                              
                ]); 
                        
                var chart = new CanvasJS.Chart("bperContainer",
	{
             colorSet: "brownShades",
                  title:{
			text: "CREDIT LIMIT"
		},
		
		data: [
		{
			 type: "column",
                      dataPoints: [
                                { label: "25%", y:data.tfperc},
				{ label: "50%", y: data.fzperc},
				{ label: "75%", y: data.sfperc },
                                { label: "90%", y: data.nzperc },
                                { label: "100%", y: data.ozzperc },
				
			]
		}
		]
	});
	chart.render();
        
        
           CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#FFBB00",
                "#FFBB00",
                "#FFBB00",
                 "#FFBB00"
                  
                              
                ]); 
                        
                var chart = new CanvasJS.Chart("bpayContainer",
	{
             colorSet: "brownShades",
                  title:{
			text: "PENDING PAYMENT"
		},
		
		data: [
		{
			 type: "column",
                      dataPoints: [
                                { label: "<₹500", y:data.pf},
				{ label: "₹500 - ₹1000", y: data.fpo},
				{ label: "₹1000 - ₹2000", y: data.opt },
                                { label: ">₹2000", y: data.pt },
                              
				
			]
		}
		]
	});
	chart.render();
         $('.loading').addClass('hide');
        initialise_bill_datatable();
       
               
            }
              
            
            
        });
        
    });
     
     
      
     $(document).on('change','.expirydays',function(){
      
         var daysfilter = $('#bdaysfiletr').val(); 
         var filter="days"; 
         var filtertype=$(this).val();
	
	 var isp_uid= $('.franchise').find(":selected").val();
        $('.filterpercstatus').find('option:selected').prop("selected", false);
	  $('.filterpaymstatus').find('option:selected').prop("selected", false);
          $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/billing_report",
            type:'POST',
             dataType:'json',
            data:{daysfilter:daysfilter,filter:filter,filtertype:filtertype,isp_uid:isp_uid},
           
            success:function (data){
             
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#btotalactive').html(data.total_count);
                $('#blast30days').html(data.last30count);
                $('#blast180days').html(data.last180count);
                $('#bmore180days').html(data.more180count);
               
                $('#bsearch_activegrid').html(data.html);
                $('#avgr').html("₹ "+data.avgrevenue+".00 /");
                
               
                
               
                    CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#0F485F",
                "#0F485F",
                "#0F485F",
                 "#0F485F",
                  "#0F485F"
                              
                ]); 
                        
                var chart = new CanvasJS.Chart("bperContainer",
	{
             colorSet: "brownShades",
                  title:{
			text: "CREDIT LIMIT"
		},
		
		data: [
		{
			 type: "column",
                      dataPoints: [
                                { label: "25%", y:data.tfperc},
				{ label: "50%", y: data.fzperc},
				{ label: "75%", y: data.sfperc },
                                { label: "90%", y: data.nzperc },
                                { label: "100%", y: data.ozzperc },
				
			]
		}
		]
	});
	chart.render();
        
        
           CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#FFBB00",
                "#FFBB00",
                "#FFBB00",
                 "#FFBB00"
                  
                              
                ]); 
                        
                var chart = new CanvasJS.Chart("bpayContainer",
	{
             colorSet: "brownShades",
                  title:{
			text: "PENDING PAYMENT"
		},
		
		data: [
		{
			 type: "column",
                      dataPoints: [
                                { label: "<₹500", y:data.pf},
				{ label: "₹500 - ₹1000", y: data.fpo},
				{ label: "₹1000 - ₹2000", y: data.opt },
                                { label: ">₹2000", y: data.pt },
                              
				
			]
		}
		]
	});
	chart.render();
         $('.loading').addClass('hide');
        initialise_bill_datatable();
       
               
            }
              
            
            
        });
        
    });
    
    
    
    //billing excel
    $(document).on('click','.billingexcel',function(){
	 $(".loading").removeClass('hide');
	 var filterpay=$('.filterpaymstatus option:selected').val();
	 var filterperc=$('.filterpercstatus option:selected').val();
	 var filterdays=$('.expirydays option:selected').val();
	  var daysfilter=$('#bdaysfiletr').val();
	   var isp_uid= $('.franchise').find(":selected").val();
	 if (filterpay=="all" && filterperc=="all" && filterdays=="all") {
	   $('.excelerr').html('Please Select Filter');
	    $(".loading").addClass('hide');
	   return false;
	 }
	else{
	    if (filterpay!="all") {
	     var filter="number";
	      var filtertype=$('.filterpaymstatus option:selected').val();
	  }
	   if (filterperc!="all") {
	     var filter="perc";
	      var filtertype=$('.filterpercstatus option:selected').val();
	  }
	  if (filterdays!="all") {
	    var filter="days";
	      var filtertype=$('.expirydays option:selected').val();
	  }
	   
	 }
	  
	    $.ajax({
            url:base_url+"report/billing_report_excel",
            type:'POST',
             dataType:'json',
            data:{daysfilter:daysfilter,filter:filter,filtertype:filtertype,isp_uid:isp_uid},
	     success:function (data){
		if (data.resultcode==0) {
		   $('.excelerr').html("No Data Found.");
		   $(".loading").addClass('hide');
		}
		else
		{
		     window.location = base_url+"report/billing_report_excel_download";
                        $(".loading").addClass('hide');
		}
	     }
	    });
        // var filter="number"; 
         //var filtertype=$(this).val();
	// alert("paymt:"+filterpay+",perc:"+filterperc+",daysfilter:"+daysfilter+",isp_uid:"+isp_uid);
	 
	
	});
    
    
    //franchise change
    
    $(document).on('change','.franchise',function(){
	
	var reporttype=$('.mui-tabs__bar').find('.mui--is-active').attr('rel');
	
	if (reporttype=="perspective") {
	   perspective_user_filter_default(); 
	}
	else if(reporttype=="active"){
	    active_user_default();
	}
	else if(reporttype=="billing"){
	    billing_report_default();
	}
	
	else if(reporttype=="complaint"){
	   complaint_report_default(); 
	}
	else if(reporttype=="plan"){
	    plan_topup_default();
	}
	
	
	
	});
    
    
    //complaint request Excel
    
    $(document).on('click','.compexcel',function(){
	
	   var filter=$('.filter_complaint option:selected').val();
	 var isp_uid= $('.franchise').find(":selected").val();
        var complaint_type=$('.filter_compchange').val();
         $('.loading').removeClass('hide');
        
        $.ajax({
            url:base_url+"report/compaint_request_excel",
            type:'POST',
             dataType:'json',
            data:{filter:filter,complaint_type:complaint_type,isp_uid:isp_uid},
           
            success:function (data){
              if (data.resultcode==0) {
		   $('.comperr').html("No Data Found.");
		   $(".loading").addClass('hide');
		}
		else
		{
		     window.location = base_url+"report/compaint_request_excel_dwnld";
                        $(".loading").addClass('hide');
		}
        
               
            }
              
            
            
        });
	
    });
    
    
   
    
});

function filter_complaint(filter)
{
    
        var filter=filter;
	 var isp_uid= $('.franchise').find(":selected").val();
        var complaint_type=$('.filter_compchange').val();
         $('.loading').removeClass('hide');
        
        $.ajax({
            url:base_url+"report/compaint_request",
            type:'POST',
             dataType:'json',
            data:{filter:filter,complaint_type:complaint_type,isp_uid:isp_uid},
           
            success:function (data){
              $('#search_compgrid').html(data.html);
                 var abc =[];
          $.each(data.graph, function(k, v) {
             //display the key and value pair
              var objects = {label:k, y:v};
                abc.push(objects);
            
        })   
               
                  
                var chart = new CanvasJS.Chart("compuserContainer",
   {      
      theme:"theme2",
      title:{
        text: ""
      },
      animationEnabled: true,
      axisY :{
        includeZero: true,
        // suffix: " k",
        valueFormatString: "",
        suffix: ""
        
      },
      toolTip: {
        shared: "true"
      },
      data: [
      { 
            
        type: "spline", 
        // showInLegend: true,
        // markerSize: 0,
        name: "",
        dataPoints: abc
      } 
      

      ],
      legend:{
        cursor:"pointer",
        itemclick : function(e) {
          if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
          	e.dataSeries.visible = false;
          }
          else {
            e.dataSeries.visible = true;
          }
          chart.render();
        }
        
      },
    });

chart.render();
$('.loading').addClass('hide');
        initialise_active_datatable();
        //
               
            }
              
            
            
        });
}

function initialise_perspective_datatable()
{
      $('#persp_table').DataTable({
      
      "bLengthChange": false,//for num of record in one page
      "bFilter": false,// for search box
      "bInfo": false,// for num of record show in page hide
      /*"aoColumnDefs": [{  // column shorting by column number
    'bSortable': false,
    'aTargets': [ 1,2,3,4,5,6,7,8]
      }],*/
      "bSort": false ,// column shorting
      "bDestroy": true,
      "pageLength": 50,
      drawCallback: function(settings) {
    var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
    pagination.toggle(this.api().page.info().pages > 1);
      }
        });
}

function initialise_active_datatable()
{
      $('#activeuser_table').DataTable({
      
      "bLengthChange": false,//for num of record in one page
      "bFilter": false,// for search box
      "bInfo": false,// for num of record show in page hide
      /*"aoColumnDefs": [{  // column shorting by column number
    'bSortable': false,
    'aTargets': [ 1,2,3,4,5,6,7,8]
      }],*/
      "bSort": false ,// column shorting
      "bDestroy": true,
      "pageLength": 50,
      drawCallback: function(settings) {
    var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
    pagination.toggle(this.api().page.info().pages > 1);
      }
        });
}


function initialise_comp_datatable()
{
      $('#compuser_table').DataTable({
      
      "bLengthChange": false,//for num of record in one page
      "bFilter": false,// for search box
      "bInfo": false,// for num of record show in page hide
      /*"aoColumnDefs": [{  // column shorting by column number
    'bSortable': false,
    'aTargets': [ 1,2,3,4,5,6,7,8]
      }],*/
      "bSort": false ,// column shorting
      "bDestroy": true,
      "pageLength": 50,
      drawCallback: function(settings) {
    var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
    pagination.toggle(this.api().page.info().pages > 1);
      }
        });
}

function initialise_bill_datatable()
{
      $('#billuser_table').DataTable({
      
      "bLengthChange": false,//for num of record in one page
      "bFilter": false,// for search box
      "bInfo": false,// for num of record show in page hide
      /*"aoColumnDefs": [{  // column shorting by column number
    'bSortable': false,
    'aTargets': [ 1,2,3,4,5,6,7,8]
      }],*/
      "bSort": false ,// column shorting
      "bDestroy": true,
      "pageLength": 50,
      drawCallback: function(settings) {
    var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
    pagination.toggle(this.api().page.info().pages > 1);
      }
        });
}

function initialise_plan_datatable()
{
      $('#planuser_table').DataTable({
      
      "bLengthChange": false,//for num of record in one page
      "bFilter": false,// for search box
      "bInfo": false,// for num of record show in page hide
      /*"aoColumnDefs": [{  // column shorting by column number
    'bSortable': false,
    'aTargets': [ 1,2,3,4,5,6,7,8]
      }],*/
      "bSort": false ,// column shorting
      "bDestroy": true,
      "pageLength": 50,
      drawCallback: function(settings) {
    var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
    pagination.toggle(this.api().page.info().pages > 1);
      }
        });
}

function active_user_default() {
    var isp_uid= $('.franchise').find(":selected").val();
   
    var filter="all"; 
          $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/active_user",
            type:'POST',
             dataType:'json',
            data:{filter:filter,isp_uid:isp_uid},
           
            success:function (data){
             
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#totalactive').html(data.total_count);
                $('#last30days').html(data.last30count);
                $('#last180days').html(data.last180count);
                $('#more180days').html(data.more180count);
                $('#totalsusp').html(data.totaloffsuspon);
                 $('#onlinecount').html(data.online);
                $('#offlinecount').html(data.offline);
                $('#suspcount').html(data.suspended);
                $('#search_activegrid').html(data.html);
               
                        
                        
               // $('#searchtext').removeClass('serch_loading');
               CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#36465F",
                "#E4AF23",
                "#F4474A"
                              
                ]);
               
               var chart = new CanvasJS.Chart("activeuserContainer",
	{
             colorSet: "brownShades",
		
		data: [
		{
			/*type: "pie",
                        toolTipContent: " #percent %",
                        indexLabel: "#percent%",*/
                         //showInLegend: true,
                         type: "doughnut",
			indexLabelFontFamily: "'Open Sans', sans-serif;",       
			indexLabelFontSize: 20,
			indexLabelFontWeight: "bold",
			//startAngle:0,
			indexLabelFontColor: "MistyRose",       
			indexLabelLineColor: "darkgrey", 
			indexLabelPlacement: "inside", 
			toolTipContent: "#percent %",
			//showInLegend: true,
			indexLabel: "#percent%", 
			dataPoints: [
                            { y: data.online},
				{ y: data.offline},
				{ y: data.suspended}
				
			]
		}
		]
	});
	chart.render();
           $('.loading').addClass('hide');
        initialise_active_datatable();
     
               
            }
              
            
            
        });
}

function perspective_user_filter_default() {
   var filter='all';
	 var isp_uid= $('.franchise').find(":selected").val();
        $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/persp_user_filter",
            type:'POST',
             dataType:'json',
            data:{filter:filter,isp_uid:isp_uid},
           
            success:function (data){
		
               $('.loading').addClass('hide');
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
	        $('#search_perspview').html('');
		var total=parseInt(data.count.inactivecount)+parseInt(data.count.leadcount)+parseInt(data.count.enquirycount);
		$('.pleads').html(data.count.leadcount+" Leads");
		$('.penq').html(data.count.enquirycount+" Enquiries");
		$('.pinact').html(data.count.inactivecount+" Inactive");
		$('.ptot').html(total);
                $('#search_perspview').html(data.html);
		initialise_perspective_datatable();
               // $('#searchtext').removeClass('serch_loading');
            }
            
            
            
        });
}

function billing_report_default() {
    var daysfilter="all"; 
         var filter="all";
	  var isp_uid= $('.franchise').find(":selected").val();
          $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/billing_report",
            type:'POST',
             dataType:'json',
            data:{daysfilter:daysfilter,filter:filter,isp_uid:isp_uid},
           
            success:function (data){
             
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#btotalactive').html(data.total_count);
                $('#blast30days').html(data.last30count);
                $('#blast180days').html(data.last180count);
                $('#bmore180days').html(data.more180count);
               
                $('#bsearch_activegrid').html(data.html);
                $('#avgr').html("₹ "+data.avgrevenue+".00 /");
                
               
                
               
                    CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#0F485F",
                "#0F485F",
                "#0F485F",
                 "#0F485F",
                  "#0F485F"
                              
                ]); 
                        
                var chart = new CanvasJS.Chart("bperContainer",
	{
             colorSet: "brownShades",
                  title:{
			text: "CREDIT LIMIT"
		},
		
		data: [
		{
			 type: "column",
                      dataPoints: [
                                { label: "25%", y:data.tfperc},
				{ label: "50%", y: data.fzperc},
				{ label: "75%", y: data.sfperc },
                                { label: "90%", y: data.nzperc },
                                { label: "100%", y: data.ozzperc },
				
			]
		}
		]
	});
	chart.render();
        
        
           CanvasJS.addColorSet("brownShades",
                [//colorSet Array

                "#FFBB00",
                "#FFBB00",
                "#FFBB00",
                 "#FFBB00"
                  
                              
                ]); 
                        
                var chart = new CanvasJS.Chart("bpayContainer",
	{
             colorSet: "brownShades",
                  title:{
			text: "PENDING PAYMENT"
		},
		
		data: [
		{
			 type: "column",
                      dataPoints: [
                                { label: "<₹500", y:data.pf},
				{ label: "₹500 - ₹1000", y: data.fpo},
				{ label: "₹1000 - ₹2000", y: data.opt },
                                { label: ">₹2000", y: data.pt },
                              
				
			]
		}
		]
	});
	chart.render();
         $('.loading').addClass('hide');
        initialise_bill_datatable();
       
               
            }
              
            
            
        }); 
}

function complaint_report_default() {
     var filter="all";
        var complaint_type='all';
	  var isp_uid= $('.franchise').find(":selected").val();
          $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/compaint_request",
            type:'POST',
             dataType:'json',
            data:{filter:filter,complaint_type:complaint_type,isp_uid:isp_uid},
           
            success:function (data){
             
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                
                $('#search_compgrid').html(data.html);
                 $('#opent').html(data.openticket);
                 var abc =[];
          $.each(data.graph, function(k, v) {
             //display the key and value pair
              var objects = {label:k, y:v};
                abc.push(objects);
            
        })   
               
                  
                var chart = new CanvasJS.Chart("compuserContainer",
   {      
      theme:"theme2",
      title:{
        text: ""
      },
      animationEnabled: true,
      axisY :{
        includeZero: true,
        // suffix: " k",
        valueFormatString: "",
        suffix: ""
        
      },
      toolTip: {
        shared: "true"
      },
      data: [
      { 
            
        type: "spline", 
        // showInLegend: true,
        // markerSize: 0,
        name: "",
        dataPoints: abc
      } 
      

      ],
      legend:{
        cursor:"pointer",
        itemclick : function(e) {
          if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
          	e.dataSeries.visible = false;
          }
          else {
            e.dataSeries.visible = true;
          }
          chart.render();
        }
        
      },
    });

chart.render();
$('.loading').addClass('hide');
        initialise_comp_datatable();
        
               
            }
              
            
            
        });
}

 function plan_topup_default()
{
     var daysfilter="all"; 
         var planfilter="all";
	  var isp_uid= $('.franchise').find(":selected").val();
          $('.loading').removeClass('hide');
        $.ajax({
            url:base_url+"report/plan_topup_report",
            type:'POST',
             dataType:'json',
            data:{daysfilter:daysfilter,planfilter:planfilter,isp_uid:isp_uid},
           
            success:function (data){
             
               // $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#ptotalactive').html(data.total_count);
                $('#plast30days').html(data.last30count);
                $('#plast180days').html(data.last180count);
                $('#pmore180days').html(data.more180count);
               
                $('#psearch_activegrid').html(data.html);
                
                  $('select[name="filterplant"]').html('').append('<option value="all" rel="all">all</option>');
                 $.each(data.plandropdown, function(k, v) {
             
              var result = k.split('::');
             $('select[name="filterplant"]').append('<option  value="'+result[0]+'" rel="'+result[1]+'">'+v+'</option>');
             
           });  
                
               
                    var i=1;
                    var j=1;
                     var abc =[];
                     var color=[];
                    $.each(data.plangraph, function(k, v) {
             //display the key and value pair
             if(i<=5){
              var objects = {label:k, y:v};
              var col="#0F485F";
                abc.push(objects);
                color.push(col);
            }
                i++;
            
        })       
        
         $.each(data.topupgraph, function(k, v) {
             //display the key and value pair
             if(j<=2){
              var objects = {label:k, y:v};
              var col="#FFBB00";
                abc.push(objects);
                color.push(col);
            }
                j++;
            
        })     
                        
               // $('#searchtext').removeClass('serch_loading');
               CanvasJS.addColorSet("brownShades",color
                );
               
               var chart = new CanvasJS.Chart("planuserContainer",
	{
             colorSet: "brownShades",
		
		data: [
		{
			
                         //showInLegend: true,
                         type: "column",

			dataPoints: abc
		}
		]
	});
	chart.render();
          $('.loading').addClass('hide');
        initialise_plan_datatable();
      
               
            }
              
            
            
        });
}
