$('body').on('keyup', '.bill_amount', function(){
    var billamt = $(this).val();
    var billtype = $(this).data('billtype');
    
    if (billtype == 'installation') {
        var disctype = $('input[type=radio][name=install_discounttype]:checked').val();
        if (disctype == 'percent') {
            var amtdisc = $('select[name="install_bill_percentdiscount"] option:selected').val();
            billamt = Math.round(billamt - ((billamt * amtdisc) / 100));
        }else{
            var amtdisc = $('input[name="install_bill_flatdiscount"]').val();
            billamt = Math.round(billamt -  amtdisc);
        }
    }
    else if (billtype == 'security') {
        var disctype = $('input[type=radio][name=security_discounttype]:checked').val();
        if (disctype == 'percent') {
            var amtdisc = $('select[name="security_bill_percentdiscount"] option:selected').val();
            billamt = Math.round(billamt - ((billamt * amtdisc) / 100));
        }else{
            var amtdisc = $('input[name="security_bill_flatdiscount"]').val();
            billamt = Math.round(billamt -  amtdisc);
        }
    }

    var tax = $('.current_taxapplicable').val();
    if (billtype == 'installation') {
        var taxratio = $('input[type=radio][name=install_bill_withtax]:checked').val();
    }else if (billtype == 'security') {
        var taxratio = $('input[type=radio][name=security_bill_withtax]:checked').val();
    }
    if (taxratio == '1') {
        billamt = Math.round(billamt) + Math.round((billamt * tax)/100);
    }
    $('.bill_total_amount').val(billamt);
});

$('body').on('change', 'select[name="install_bill_percentdiscount"]', function(){
    var amtdisc = $(this).val();
    var billamt = $('#install_bill_amount').val();
    if (amtdisc != '' && billamt != '') {
        billamt = Math.round(billamt - ((billamt * amtdisc) / 100));
    }
    var tax = $('.current_taxapplicable').val();
    var taxratio = $('input[type=radio][name=install_bill_withtax]:checked').val();
    if (taxratio == '1') {
        billamt = parseInt(billamt) + parseInt((billamt * tax)/100);
    }
    $('input[name="install_bill_total_amount"]').val(billamt);
});

$('body').on('keyup', 'input[name="install_bill_flatdiscount"]', function(){
    var billamount = $('#install_bill_amount').val();
    var discount = $(this).val();
    var billamt = Math.round(billamount - discount);
    var tax = $('.current_taxapplicable').val();
    var taxratio = $('input[type=radio][name=install_bill_withtax]:checked').val();
    if (taxratio == '1') {
        billamt = parseInt(billamt) + parseInt((billamt * tax)/100);
    }
    $('input[name="install_bill_total_amount"]').val(billamt);
});

$('body').on('change', 'select[name="security_bill_percentdiscount"]', function(){
    var amtdisc = $(this).val();
    var billamt = $('#security_bill_amount').val();
    if (amtdisc != '' && billamt != '') {
        billamt = Math.round(billamt - ((billamt * amtdisc) / 100));
    }
    var tax = $('.current_taxapplicable').val();
    var taxratio = $('input[type=radio][name=security_bill_withtax]:checked').val();
    if (taxratio == '1') {
        billamt = parseInt(billamt) + parseInt((billamt * tax)/100);
    }
    $('input[name="security_bill_total_amount"]').val(billamt);
});

$('body').on('keyup', 'input[name="security_bill_flatdiscount"]', function(){
    var billamount = $('#security_bill_amount').val();
    var discount = $(this).val();
    var billamt = Math.round(billamount - discount);
    var tax = $('.current_taxapplicable').val();
    var taxratio = $('input[type=radio][name=security_bill_withtax]:checked').val();
    if (taxratio == '1') {
        billamt = parseInt(billamt) + parseInt((billamt * tax)/100);
    }
    $('input[name="security_bill_total_amount"]').val(billamt);
});

$('body').on('change', 'input[type=radio][name=install_bill_withtax]', function(){
    var taxratio = $(this).val();
    var isptax = $('#current_taxapplicable').val();
    var install_bill_amount = $('#install_bill_amount').val();
    var install_discounttype = $('input[name="install_discounttype"]:checked').val();
    if (install_discounttype == 'percent') {
        var discount = $('select[name="install_bill_percentdiscount"] option:selected').val();
        var amt_afterdisc = parseInt(install_bill_amount) - parseInt((install_bill_amount * discount) / 100);
    }else if (install_discounttype == 'flat') {
        var discount = $('input[name="install_bill_flatdiscount"]').val();
        var amt_afterdisc = parseInt(install_bill_amount) - parseInt(discount);
    }else{
        var amt_afterdisc = install_bill_amount;
    }
    
    if (taxratio == 1) {
        var install_bill_total_amount = Math.round(amt_afterdisc) + Math.round((amt_afterdisc * isptax) / 100);
    }else{
        var install_bill_total_amount = Math.round(amt_afterdisc);
    }
    $('input[name="install_bill_total_amount"]').val(install_bill_total_amount);
});

$('body').on('change', 'input[type=radio][name=security_bill_withtax]', function(){
    var taxratio = $(this).val();
    var isptax = $('#current_taxapplicable').val();
    var security_bill_amount = $('#security_bill_amount').val();
    var security_discounttype = $('input[name="security_discounttype"]:checked').val();
    if (security_discounttype == 'percent') {
        var discount = $('select[name="security_bill_percentdiscount"] option:selected').val();
        var amt_afterdisc = parseInt(security_bill_amount) - parseInt((security_bill_amount * discount) / 100);
    }else if (security_discounttype == 'flat') {
        var discount = $('input[name="security_bill_flatdiscount"]').val();
        var amt_afterdisc = parseInt(security_bill_amount) - parseInt(discount);
    }else{
        var amt_afterdisc = security_bill_amount;
    }
    
    if (taxratio == 1) {
        var security_bill_total_amount = Math.round(amt_afterdisc) + Math.round((amt_afterdisc * isptax) / 100);
    }else{
        var security_bill_total_amount = Math.round(amt_afterdisc);
    }
    $('input[name="security_bill_total_amount"]').val(security_bill_total_amount);
});

$('body').on('click', '#editclick_billing_details', function(){
    var idclass = $(this).is('[disabled=disabled]');
    if(idclass !== true){
        $('.loading').removeClass('hide');
        var subsid = $('.subscriber_userid').val();
        var subs_uuid = $('.subscriber_uuid').val();
        $.ajax({
            url: base_url+'user/planassoc_withuser',
            type: 'POST',
            dataType: 'json',
            data: 'subsid='+subsid+'&subs_uuid='+subs_uuid,
            success: function(data){
                $('.loading').addClass('hide');
                if (data == 0) {
                    $('#exceedlimit').addClass('hide');
                    $('#inactive_billing').removeClass('hide');
                    $('#active_billing').addClass('hide');
                }else{
                    $('#inactive_billing').addClass('hide');
                    $('#active_billing').removeClass('hide');
                    mui.tabs.activate('active_billing_pane');
                    user_billing_listing();
                }
                //$('#inactive_billing').addClass('hide');
                //$('#active_billing').removeClass('hide');
            }
        });
    }
});

$('body').on('click', '#click_custombillingtab', function(){
    $('.listcustombilldiv').removeClass('hide');
    $('.addcustombilldiv').addClass('hide');
    $('.addcustbillbtn').removeClass('hide');
    $('#custformtitle').html('');
    $('.loading').removeClass('hide');
    var subsid = $('.subscriber_userid').val();
    var subs_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/custombilling_listing',
        type: 'POST',
        dataType: 'json',
        data: 'subsid='+subsid+'&subs_uuid='+subs_uuid,
        success: function(data){
            $('.loading').addClass('hide');
            $('#custombill_summary').html(data.custombill_list);
            $('#custombill_rcptsummary').html(data.customrcpt_list);
            
            $('#showtotalcustbillamt').html('Total Bill: '+data.totalbillamt);
            $('#showtotalcustbillrcpt').html('Amount Received: '+data.totalrcptamt);
            $('#showbalcustbillamt').html(data.totalbalamt);
            if (data.superadmin_perm != 1) {
                if (data.custbilltab_readperm == 1) {
                    $('#custom_billing_pane').find('a').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                    $('#custom_billing_pane').find('button, input').prop('disabled', true);
                }else{
                    if ((typeof data.custbill_addperm != 'undefined') && (data.custbill_addperm == '1')) {
                        $('#custom_billing_pane').find('button, input').prop('disabled', false);
                    }else{
                        $('#custom_billing_pane').find('button, input').prop('disabled', true);
                    }
                    if (((typeof data.custbill_editperm != 'undefined') && (data.custbill_editperm == '1')) && ((typeof data.custbill_deleteperm != 'undefined') && (data.custbill_deleteperm == '1'))) {
                    }else{
                        if ((typeof data.custbill_editperm != 'undefined') && (data.custbill_editperm == '1')) {
                            $('#custom_billing_pane a.delete_cbillperm').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                        }
                        if ((typeof data.custbill_deleteperm != 'undefined') && (data.custbill_deleteperm == '1')) {
                            $('#custom_billing_pane a.edit_cbillperm').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                        }
                    }
                }
            }
        }
    });
});

$('body').on('change', 'input[name="installcharges_applicable"]', function(){
    var charges = $(this).val();
    if (charges == 'waivedoff') {
        $('#installation_billing_form')[0].reset();
        $('.visible_oncharge').find('input, textarea, button, select').attr('disabled','disabled');
        $('input[name="install_bill_datetime"]').remove();
        $('input[name="install_bill_datetimeformat"]').remove();
        $('input[name="installcharges_applicable"][value="'+charges+'"]').attr('checked', 'checked');
    }else if (charges == 'applycharges') {
        $('.visible_oncharge').find('input, textarea, button, select').removeAttr('disabled');
        var d = new Date();
        var time = '   '+d.toLocaleTimeString();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var currdate = ((''+day).length<2 ? '0' : '')+day + '-' +
                     ((''+month).length<2 ? '0' : '')+month + '-' +
                     d.getFullYear() ;
        var seconds = d.getSeconds();
        var tkttime = d.getHours() +""+ d.getMinutes() +""+ ((''+seconds).length<2 ? '0' : '')+seconds ;
        var tktdate = day +""+ month +""+ d.getFullYear().toString().substr(-2) ;
        var dbtktdatetime =  d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + d.getHours() +":"+ d.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;;
        $('input[name="install_bill_datetime"]').remove();
        $('input[name="install_bill_datetimeformat"]').remove();
        $('input[name="install_bill_number"]').val(tktdate+tkttime);
        $('#install_billdtme').append('<input name="install_bill_datetimeformat" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="'+currdate+time+'">');
        $('#install_billdtme').append('<input name="install_bill_datetime" type="hidden" value="'+dbtktdatetime+'">');
    }
})

$('body').on('click', '#_installationModal', function(){
    $('.formsubmit').removeClass('hide');
    $('.formloader').addClass('hide');
    $('#installation_billing_form')[0].reset();
    $('.visible_oncharge').find('input, textarea, button, select').attr('disabled','disabled');
    $('.bill_amount').val('');
    $('.bill_discount').val('');
    $('input').removeClass('mui--is-touched mui--is-not-empty');
    $('input[name="install_bill_datetime"]').remove();
    $('input[name="install_bill_datetimeformat"]').remove();
    $('input[name="installation_billid"]').val('');
    
    $('#installationModal').modal('show');
});

$('body').on('click', '#_securityModal', function(){
    $('.formsubmit').removeClass('hide');
    $('.formloader').addClass('hide');
    $('#security_billing_form')[0].reset();
    $('.security_visible_oncharge').find('input, textarea, button, select').attr('disabled','disabled');
    $('.bill_amount').val('');
    $('.bill_discount').val('');
    $('input').removeClass('mui--is-touched mui--is-not-empty');
    $('input[name="security_bill_datetime"]').remove();
    $('input[name="security_bill_datetimeformat"]').remove();
    $('input[name="security_billid"]').val('');
        
    $('#securityModal').modal('show');
});

$('body').on('change', 'input[name="securitycharges_applicable"]', function(){
    var charges = $(this).val();
    if (charges == 'waivedoff') {
        $('#security_billing_form')[0].reset();
        $('.security_visible_oncharge').find('input, textarea, button, select').attr('disabled','disabled');
        $('input[name="security_bill_datetime"]').remove();
        $('input[name="security_bill_datetimeformat"]').remove();
        $('input[name="securitycharges_applicable"][value="'+charges+'"]').attr('checked', 'checked');
    }else if (charges == 'applycharges') {
        $('.security_visible_oncharge').find('input, textarea, button, select').removeAttr('disabled');
        var d = new Date();
        var time = '    '+d.toLocaleTimeString();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var currdate = ((''+day).length<2 ? '0' : '')+day + '-' +
                     ((''+month).length<2 ? '0' : '')+month + '-' +
                     d.getFullYear() ;
        var seconds = d.getSeconds();
        var tkttime = d.getHours() +""+ d.getMinutes() +""+ ((''+seconds).length<2 ? '0' : '')+seconds ;
       var tktdate = day +""+ month +""+ d.getFullYear().toString().substr(-2) ;
        var dbtktdatetime =  d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + d.getHours() +":"+ d.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;;
        $('input[name="security_bill_datetime"]').remove();
        $('input[name="security_bill_datetimeformat"]').remove();
        //$('input[name="security_receipt_number"]').val(tktdate+tkttime);
        $('#security_billdtme').append('<input name="security_bill_datetimeformat" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="'+currdate+time+'">');
        $('#security_billdtme').append('<input name="security_bill_datetime" type="hidden" value="'+dbtktdatetime+'">');
    }
})

$('body').on('click', '#_advance_prepayModal', function(){
    $('#advancedpay_billing_form')[0].reset();
    $('.advprepay_addbtn').removeClass('hide');
    $('.adverr').addClass('hide');
    $('.bill_amount').val('');
    $('.bill_discount').val('');
    $('#alertModal').modal('hide');
    $subs_uuid = $('.subscriber_uuid').val();
    /*
    var curr_user_plan_type = ''; var nextuser_plan_type = '';
    $.ajax({
        url: base_url+'user/current_user_plan_type',
        type: 'POST',
        dataType: 'json',
        data: 'subs_uuid='+$subs_uuid,
        async: false,
        success: function(data){
            curr_user_plan_type = data.user_plan_type;
            nextuser_plan_type = data.nextuser_plan_type;
        }
    });
    */
    var planautorenewal = '';
    $.ajax({
        url: base_url+'user/user_planautorenewal_status',
        type: 'POST',
        dataType: 'json',
        data: 'uuid='+$subs_uuid,
        async: false,
        success: function(data){
            planautorenewal = data.renewaltype;
        }
    });
    $('input[name="prevadvpayid"]').val('');
    $('select[name="advprepay_bill_percentdiscount"]').removeClass('hide');
        $('input[name="advprepay_bill_flatdiscount"]').addClass('hide');
    if ((planautorenewal != '') && (planautorenewal != '0')) {
        $.ajax({
            url: base_url+'user/userassoc_plan_pricing',
            type: 'POST',
            dataType: 'json',
            data: 'subs_uuid='+$subs_uuid,
            async: false,
            success: function(data){
                $('#userassoc_planid').val(data.planid);
                $('#plan_monthly_cost').val(data.price);
                $('select[name="advprepay_month_count"]').val(1);
                $('input[name="advprepay_bill_total_amount"]').val(data.price);
                $('input[name="advprepay_bill_amount"]').val(data.price);
            }
        });
        var d = new Date();
        var time = '    '+d.toLocaleTimeString();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var currdate = ((''+day).length<2 ? '0' : '')+day + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + d.getFullYear() ;
        var seconds = d.getSeconds();
        var tkttime = d.getHours() +""+ d.getMinutes() +""+ ((''+seconds).length<2 ? '0' : '')+seconds ;
        var tktdate = day +""+ month +""+ d.getFullYear().toString().substr(-2) ;
        var dbtktdatetime =  d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + d.getHours() +":"+ d.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;;
        $('input[name="advprepay_bill_datetime"]').remove();
        $('input[name="advprepay_bill_datetimeformat"]').remove();
        $('input[name="advprepay_bill_number"]').val(tktdate+tkttime);
        $('#advprepay_billdtme').append('<input name="advprepay_bill_datetimeformat" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="'+currdate+time+'">');
        $('#advprepay_billdtme').append('<input name="advprepay_bill_datetime" type="hidden" value="'+dbtktdatetime+'">');
        $('#advance_prepayModal').modal('show');
    }else{
        $('#alertMsgTxt').html('Advanced Prepay can not applied as AutoRenewal is Inactive for this user.');
        $('#alertModal').modal('show');
    }
});

$('body').on('click', '#_addtowallet', function(){
    $('#addtowallet_billing_form')[0].reset();
    var d = new Date();
    var time = '    '+d.toLocaleTimeString();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var currdate = ((''+day).length<2 ? '0' : '')+day + '-' +
                 ((''+month).length<2 ? '0' : '')+month + '-' +
                 d.getFullYear() ;
    var seconds = d.getSeconds();
    var tkttime = d.getHours() +""+ d.getMinutes() +""+ ((''+seconds).length<2 ? '0' : '')+seconds ;
    var tktdate = day +""+ month +""+ d.getFullYear().toString().substr(-2) ;
    var dbtktdatetime =  d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + d.getHours() +":"+ d.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;;
    
    var randnumb = Math.floor(Math.random() * 9000000) + 1000000;
    $('input[id="addtowallet_bill_number"]').val(randnumb);
    //$('input[name="addtowallet_bill_datetime"]').remove();
    //$('input[name="addtowallet_bill_datetimeformat"]').remove();
    //$('#addtowallet_billdtme').append('<input name="addtowallet_bill_datetimeformat" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="'+currdate+time+'">');
    //$('#addtowallet_billdtme').append('<input name="addtowallet_bill_datetime" type="hidden" value="'+dbtktdatetime+'">');
    $('.chqddpaytm').addClass('hide');
    $('.bill_bankdetails').addClass('hide');
    $('input[name="walletid_foredit"]').val('');
    $('input[name="walletbillid_foredit"]').val('');
    $('#addtowalletModal').modal('show');
});

$('select[name="addtowallet_payment_mode"]').on('change', function() {
    var payoption = $(this).val();
    if ((payoption == 'cheque') || (payoption == 'dd')) {
        $('.chqddpaytm').removeClass('hide');
        $('.bill_bankdetails').removeClass('hide');
        $('.req_cheque_dd').attr('required', 'required');
    }
    else{
        $('.chqddpaytm').addClass('hide');
        if ((payoption == 'viapaytm') || (payoption == 'netbanking')){
            $('.chqddpaytm').removeClass('hide');
            $('input[name="update_cheque_dd_paytm"]').attr('required', 'required');
        }else{
            $('input[name="update_cheque_dd_paytm"]').removeAttr('required');
            $('input[name="update_cheque_dd_paytm"]').val('');
            $('.req_cheque_dd').val('');
        }
        $('.bill_bankdetails').addClass('hide');
        $('.req_cheque_dd').removeAttr('required');
    }
});

$('body').on('change', 'input[type=radio][name=install_discounttype]', function(){
    var disctype = $(this).val();
    $('select[name="install_bill_percentdiscount"] option[value="0"]').prop('selected', true);
    $('input[name="install_bill_flatdiscount"]').val('0');
    
    var billamt = $('#install_bill_amount').val();
    var tax = $('.current_taxapplicable').val();
    billamt = parseInt(billamt) + parseInt((billamt * tax)/100);
    $('input[name="install_bill_total_amount"]').val(billamt);
    
    if (disctype == 'percent') {
        $('select[name="install_bill_percentdiscount"]').removeClass('hide');
        $('input[name="install_bill_flatdiscount"]').addClass('hide');
    }else if (disctype == 'flat') {
        $('select[name="install_bill_percentdiscount"]').addClass('hide');
        $('input[name="install_bill_flatdiscount"]').removeClass('hide');
    }
});

$('body').on('change', 'input[type=radio][name=security_discounttype]', function(){
    var disctype = $(this).val();
    $('select[name="security_bill_percentdiscount"] option[value="0"]').prop('selected', true);
    $('input[name="security_bill_flatdiscount"]').val('0');
    var billamt = $('#security_bill_amount').val();
    var tax = $('.current_taxapplicable').val();
    billamt = parseInt(billamt) + parseInt((billamt * tax)/100);
    $('input[name="security_bill_total_amount"]').val(billamt);
    
    if (disctype == 'percent') {
        $('select[name="security_bill_percentdiscount"]').removeClass('hide');
        $('input[name="security_bill_flatdiscount"]').addClass('hide');
    }else if (disctype == 'flat') {
        $('select[name="security_bill_percentdiscount"]').addClass('hide');
        $('input[name="security_bill_flatdiscount"]').removeClass('hide');
    }
});

function installation_charges() {
    $('.formsubmit').addClass('hide');
    $('.formloader').removeClass('hide');
    $('#installationModal').modal('hide');
    $('.loading').removeClass('hide');
    var formdata =  $('#installation_billing_form').serialize();
    $.ajax({
        url: base_url+'user/add_installation_charges',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        async: false,
        success: function(data){
            $.ajax({
                url: base_url+'user/user_billing_listing',
                type: 'POST',
                dataType: 'json',
                data: 'subscriber_uuid='+data,
                async: false,
                success: function(result){
                    $('.loading').addClass('hide');
                    $('#instsecu_bill_summary').html(result.instsecugen);
                }
            });
        }
    }); 
}

function show_instsecuReceiptModal(billid, billtype) {
    //$('#payinstsecurity_form')[0].reset();
    //$('#payinstsecurity_form input[name="billid"]').val(billid);
    //$('#payinstsecurityModal').modal('show');
    $('.loading').removeClass('hide');
    $.ajax({
        url: base_url+'user/manage_instsecuritybills',
        type: 'POST',
        data: 'billid='+billid+'&billtype='+billtype,
        dataType: 'json',
        async: false,
        success: function(data){
            $('.formsubmit').removeClass('hide');
            $('.formloader').addClass('hide');
            if (billtype == 'installation') {
                $('#installation_billing_form')[0].reset();
                $('input[name="installation_billid"]').val(billid);
                if (data.payment_mode == 'NA') {
                    $('.visible_oncharge').find('input, textarea, button, select').attr('disabled','disabled');
                    $('input[name="install_bill_datetime"]').remove();
                    $('input[name="install_bill_datetimeformat"]').remove();
                    $('input[name="installcharges_applicable"]').prop('checked', false);
                    $('input[name="installcharges_applicable"][value="waivedoff"]').prop('checked', true);
                }else{
                    $('.visible_oncharge').find('input, textarea, button, select').removeAttr('disabled');
                    $('input[name="install_bill_datetime"]').remove();
                    $('input[name="install_bill_datetimeformat"]').remove();                    
                    $('input[name="installcharges_applicable"]').prop('checked', false);
                    $('input[name="installcharges_applicable"][value="applycharges"]').prop('checked', true);

                    
                    $('input[name="install_bill_number"]').val(data.bill_number);
                    $('#install_billdtme').append('<input name="install_bill_datetimeformat" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="'+data.datetimeformat+'">');
                    $('#install_billdtme').append('<input name="install_bill_datetime" type="hidden" value="'+data.bill_added_on+'">');
                    $('select[name="install_bill_payment_mode"] option[value="'+data.payment_mode+'"]').attr('selected', 'selected');
                    $('input[name="install_chqddrecpt"]').val(data.receipt_number);
                    $('input[name="install_bill_amount"]').val(data.actual_amount);
                    
                    $('input[name="install_discounttype"]').prop('checked', false);
                    $('input[name="install_discounttype"][value="'+data.discounttype+'"]').prop('checked', true);
                    $('select[name="install_bill_percentdiscount"]').addClass('hide');
                    $('input[name="install_bill_flatdiscount"]').addClass('hide');
                    if (data.discounttype == 'percent') {
                        $('select[name="install_bill_percentdiscount"]').removeClass('hide');
                        $('input[name="install_bill_flatdiscount"]').addClass('hide');
                        $('select[name="install_bill_percentdiscount"]').val(data.discount);
                    }else{
                        $('select[name="install_bill_percentdiscount"]').addClass('hide');
                        $('input[name="install_bill_flatdiscount"]').removeClass('hide');
                        $('input[name="install_bill_flatdiscount"]').val(data.discount);
                    }

                    $('input[name="install_bill_withtax"][value="'+data.billing_with_tax+'"]').attr('checked', 'checked');
                    $('input[name="install_bill_total_amount"]').val(data.total_amount);
                    $('input[name="install_bill_payment_received"][value="'+data.billpaid+'"]').attr('checked', 'checked');
                    $('textarea[name="install_bill_comments"]').val(data.bill_comments);
                    $('textarea[name="install_bill_remarks"]').val(data.bill_remarks);
                    
                }
                $('#installationModal').modal('show');
            }
            else if (billtype == 'security') {
                $('#security_billing_form')[0].reset();
                $('input[name="security_billid"]').val(billid);
                if (data.payment_mode == 'NA') {
                    $('.security_visible_oncharge').find('input, textarea, button, select').attr('disabled','disabled');
                    $('input[name="security_bill_datetime"]').remove();
                    $('input[name="security_bill_datetimeformat"]').remove();
                    $('input[name="securitycharges_applicable"]').prop('checked', false);
                    $('input[name="securitycharges_applicable"][value="waivedoff"]').prop('checked', true);
                }else{
                    $('.security_visible_oncharge').find('input, textarea, button, select').removeAttr('disabled');
                    $('input[name="security_bill_datetime"]').remove();
                    $('input[name="security_bill_datetimeformat"]').remove();
                    $('input[name="securitycharges_applicable"]').prop('checked', false);
                    $('input[name="securitycharges_applicable"][value="applycharges"]').prop('checked', true);
                    
                    $('input[name="security_receipt_number"]').val(data.receipt_number);
                    $('#security_billdtme').append('<input name="security_bill_datetimeformat" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="'+data.datetimeformat+'">');
                    $('#security_billdtme').append('<input name="security_bill_datetime" type="hidden" value="'+data.bill_added_on+'">');
                    $('select[name="security_bill_payment_mode"] option[value="'+data.payment_mode+'"]').attr('selected', 'selected');
                    $('input[name="security_bill_amount"]').val(data.actual_amount);
                    
                    $('input[name="security_discounttype"]').prop('checked', false);
                    $('input[name="security_discounttype"][value="'+data.discounttype+'"]').prop('checked', true);
                    $('select[name="security_bill_percentdiscount"]').addClass('hide');
                    $('input[name="security_bill_flatdiscount"]').addClass('hide');
                    if (data.discounttype == 'percent') {
                        $('select[name="security_bill_percentdiscount"]').removeClass('hide');
                        $('input[name="security_bill_flatdiscount"]').addClass('hide');
                        $('select[name="security_bill_percentdiscount"]').val(data.discount);
                    }else{
                        $('select[name="security_bill_percentdiscount"]').addClass('hide');
                        $('input[name="security_bill_flatdiscount"]').removeClass('hide');
                        $('input[name="security_bill_flatdiscount"]').val(data.discount);
                    }

                    $('input[name="security_bill_withtax"][value="'+data.billing_with_tax+'"]').attr('checked', 'checked');
                    $('input[name="security_bill_total_amount"]').val(data.total_amount);
                    $('input[name="security_bill_payment_received"][value="'+data.billpaid+'"]').attr('checked', 'checked');
                    $('textarea[name="security_bill_comments"]').val(data.bill_comments);
                    $('textarea[name="security_bill_remarks"]').val(data.bill_remarks);
                }
                $('#securityModal').modal('show');
            }
            $('.loading').addClass('hide');
        }
    });
}

function payinstsecuritybill() {
    var formdata = $('#payinstsecurity_form').serialize();
    $.ajax({
        url: base_url+'user/payinstall_securitybill',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
            $('#payinstsecurityModal').modal('hide');
            user_billing_listing();
        }
    })
}

function security_charges() {
    $('.formsubmit').addClass('hide');
    $('.formloader').removeClass('hide');
    $('#securityModal').modal('hide');
    $('.loading').removeClass('hide');
    var formdata =  $('#security_billing_form').serialize();
    $.ajax({
        url: base_url+'user/add_security_charges',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        async: false,
        success: function(data){
            $.ajax({
                url: base_url+'user/user_billing_listing',
                type: 'POST',
                dataType: 'json',
                data: 'subscriber_uuid='+data,
                async: false,
                success: function(result){
                    $('.loading').addClass('hide');
                    $('#instsecu_bill_summary').html(result.instsecugen);
                }
            });
        }
    }); 
}


$('body').on('change', 'input[type=radio][name=advprepay_discounttype]', function(){
    var disctype = $(this).val();
    $('select[name="advprepay_bill_percentdiscount"] option[value="0"]').prop('selected', true);
    $('input[name="advprepay_bill_flatdiscount"]').val('0');
    
    var plan_monthly_cost = $('#plan_monthly_cost').val();
    var month_count = $('.advprepay_month_count').val();
    var total_cost = plan_monthly_cost * month_count;
    $('input[name="advprepay_bill_total_amount"]').val(total_cost);
    
    if (disctype == 'percent') {
        $('select[name="advprepay_bill_percentdiscount"]').removeClass('hide');
        $('input[name="advprepay_bill_flatdiscount"]').addClass('hide');
    }else if (disctype == 'flat') {
        $('select[name="advprepay_bill_percentdiscount"]').addClass('hide');
        $('input[name="advprepay_bill_flatdiscount"]').removeClass('hide');
    }
});

function advance_prepaid(add_receipt = '') {
    var free_months = $('select[name="advprepay_freemonth_count"] option:selected').val();
    //var discount = $('select[name="advprepay_bill_discount"] option:selected').val();
    var disctype = $('input[type=radio][name=advprepay_discounttype]').val();
    if (disctype == 'percent') {
        var discount = $('select[name="advprepay_bill_percentdiscount"] option:selected').val();
    }else{
        var discount = $('input[name="advprepay_bill_flatdiscount"]').val();
    }

    if ((free_months != '0') && (discount != '0')) {
        $('.adverr').removeClass('hide');
        return false;
    }else{
        $('.advprepay_addbtn').addClass('hide');
        $('.adverr').addClass('hide');
        $('.loading').removeClass('hide');
        $('#advance_prepayModal').modal('hide');
        $('#editadvance_prepayModal').modal('hide');
        if (add_receipt != ''){
            var formdata =  $('#advancedpay_receipt_form').serialize();
        }else{
            var formdata =  $('#advancedpay_billing_form').serialize();
        }
        $.ajax({
            url: base_url+'user/advance_prepaid_charges',
            type: 'POST',
            dataType: 'json',
            data: formdata,
            async: false,
            success: function(data){
                var subscid = data.subscid;
                $.ajax({
                    url: base_url+'user/user_billing_listing',
                    type: 'POST',
                    dataType: 'json',
                    data: 'subscriber_uuid='+subscid,
                    async: false,
                    success: function(result){
                        $('.loading').addClass('hide');
                        var cocurr = $('#ispcountry_currency').val();
                        $('#wallet_credit_limit').html(cocurr+' '+result.user_creditlimit+'.00');
                        $('#user_wallet_amt').html(cocurr+' '+result.wallet_amt);
                        $('#net_walletamt').val(result.wallet_amt);
                        $('#plan_blocked_amt').html(cocurr+' '+result.adv_totalpay);
                        
                        $('#debitbill_summary').html(result.debitbill_listing);
                        $('#instsecu_bill_summary').html(result.instsecugen);
                        $('#totalbill_amt').html('Total Bill: '+result.totalbill_amt)
                        user_amtcredit_listing();
                    }
                });
            }
        });
        return true;
    }
}

function showadvancedpayment_details() {
    $('#advprepaydetailsModal').modal('show');
    $('#advprepay_listing').html('<img src="'+base_url+'assets/images/loader.svg"/>');
    var uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/show_advprepaylisting',
        type: 'POST',
        data: 'uuid='+uuid,
        dataType: 'json',
        success: function(data){
            $('#advprepay_listing').html(data.advprepay_listing);
        }
    })
}

function delete_advpaymentrcpt(billid) {
    $('.del_advpaybtn').removeClass('hide');
    $('form#deladvpayform')[0].reset();
    $('form#deladvpayform input[name="advprepayid"]').val(billid);
    $('#delete_advpaymentrcptModal').modal('show');
}

function delete_advprepaid_rcpt() {
    $('.del_advpaybtn').addClass('hide');
    $('#delete_advpaymentrcptModal').modal('hide');
    var formdata = $('form#deladvpayform').serialize();
    $.ajax({
        url: base_url+'user/delete_advprepaid_rcpt',
        type: 'POST',
        data: formdata,
        dataType: 'json',
        success: function(data){
            showadvancedpayment_details();
            user_billing_listing();
        }
    })
}

$('body').on('change', '.advprepay_month_count', function(){
    var plan_monthly_cost = $('#plan_monthly_cost').val();
    //var amtdisc = $('.advprepay_bill_discount').val();
    
    var month_count = $(this).val();
    var total_cost = plan_monthly_cost * month_count;
    $('input[name="advprepay_bill_amount"]').val(total_cost);
    
    var disctype = $('input[type=radio][name=advprepay_discounttype]:checked').val();
    if (disctype == 'percent') {
        var amtdisc = $('select[name="advprepay_bill_percentdiscount"] option:selected').val();
        total_cost = Math.round(total_cost - ((total_cost * amtdisc) / 100));
    }else if (disctype == 'flat') {
        var amtdisc = $('input[name="advprepay_bill_flatdiscount"]').val();
        total_cost = Math.round(total_cost - amtdisc);
    }
    
    $('input[name="advprepay_bill_total_amount"]').val(total_cost);
});

$('body').on('change', 'select[name="advprepay_bill_percentdiscount"]', function(){
    var plan_monthly_cost = $('#plan_monthly_cost').val();
    var month_count = $('.advprepay_month_count').val();
    var total_cost = plan_monthly_cost * month_count;
    var amtdisc = $(this).val();
    if (amtdisc != '' && total_cost != '') {
        total_cost = Math.round(total_cost - ((total_cost * amtdisc) / 100));
    }
    $('input[name="advprepay_bill_total_amount"]').val(total_cost);
});

$('body').on('keyup', 'input[name="advprepay_bill_flatdiscount"]', function(){
    var plan_monthly_cost = $('#plan_monthly_cost').val();
    var month_count = $('.advprepay_month_count').val();
    var total_cost = plan_monthly_cost * month_count;
    var amtdisc = $(this).val();
    if (amtdisc != '' && total_cost != '') {
        total_cost = Math.round(total_cost - amtdisc);
    }
    $('input[name="advprepay_bill_total_amount"]').val(total_cost);
});


function user_billing_listing() {
    $('.loading').removeClass('hide');
    $('.billpdfexport').addClass('hide');
    $('.billexcelexport').addClass('hide');
    var cust_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/user_billing_listing',
        type: 'POST',
        dataType: 'json',
        data: 'subscriber_uuid='+cust_uuid,
        async: false,
        success: function(result){
            //alert(result.change_limit_upto);
            $('.loading').addClass('hide');
            var cocurr = $('#ispcountry_currency').val();
            $('#wallet_credit_limit').html(cocurr+' '+result.user_creditlimit+'.00');
            $('#user_wallet_amt').html(cocurr+' '+result.wallet_amt);
            $('#net_walletamt').val(result.wallet_amt);
            $('input[name="update_walletLimit"]').attr("min", result.change_limit_upto);
            $('#plan_blocked_amt').html(cocurr+' '+result.adv_totalpay);
            var nxtlimit = result.limit;
            var nxtofset = result.offset;
            $('#bill_limit').val(nxtlimit); $('#bill_offset').val(nxtofset);
            if (result.loadmore == 1) {
               $('.bill_loadmore').removeClass('hide');
            }else{
               $('.bill_loadmore').addClass('hide');
            }
            $('#debitbill_summary').html(result.debitbill_listing);
            $('#instsecu_bill_summary').html(result.instsecugen);
            $('#totalbill_amt').html('Total Bill: '+result.totalbill_amt);
            user_amtcredit_listing();
            
            if (result.superadmin_perm != 1) {
                if (result.billtab_readperm == 1) {
                    $('#active_billing_pane').find('a').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                    $('#setup_billing_pane').find('a').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                    $('#setup_billing_pane').find('button').prop('disabled', true);
                }else{
                    if ((typeof result.creditlimit_readperm != 'undefined') && (result.creditlimit_readperm == '1')) {
                        $('#active_billing_pane a#exceedlimit').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                    }else if ((typeof result.creditlimit_hideperm != 'undefined') && result.creditlimit_hideperm == '1') {
                        $('#active_billing_pane a#exceedlimit').addClass('hide');
                    }
                    if ((typeof result.wallet_readperm != 'undefined') && (result.wallet_readperm == '1')) {
                        $('#active_billing_pane a#_addtowallet').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                    }else if ((typeof result.wallet_hideperm != 'undefined') && result.wallet_hideperm == '1') {
                        $('#active_billing_pane a#_addtowallet').addClass('hide');
                    }
                    if ((typeof result.advprepay_readperm != 'undefined') && (result.advprepay_readperm == '1')) {
                        $('#active_billing_pane a#_advance_prepayModal').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                        $('#active_billing_pane a#_advanced_prepaydetails').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                    }else if ((typeof result.advprepay_hideperm != 'undefined') && result.advprepay_hideperm == '1') {
                        $('#active_billing_pane a#_advance_prepayModal').addClass('hide');
                        $('#active_billing_pane a#_advanced_prepaydetails').addClass('hide');
                    }
                    if ((typeof result.editbill_readperm != 'undefined') && (result.editbill_readperm == '1')) {
                        $('#active_billing_pane a.edit_mbillperm').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                    }else if ((typeof result.editbill_hideperm != 'undefined') && result.editbill_hideperm == '1') {
                        $('#active_billing_pane a.edit_mbillperm').addClass('hide');
                    }
                    if ((typeof result.sendbill_readperm != 'undefined') && (result.sendbill_readperm == '1')) {
                        $('#active_billing_pane a.send_mbillperm').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                    }else if ((typeof result.sendbill_hideperm != 'undefined') && result.sendbill_hideperm == '1') {
                        $('#active_billing_pane a.send_mbillperm').addClass('hide');
                    }
                    if ((typeof result.dwnldbill_readperm != 'undefined') && (result.dwnldbill_readperm == '1')) {
                        $('#active_billing_pane a.dwlnd_mbillperm').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                    }else if ((typeof result.dwnldbill_hideperm != 'undefined') && result.dwnldbill_hideperm == '1') {
                        $('#active_billing_pane a.dwlnd_mbillperm').addClass('hide');
                    }
                    if ((typeof result.printbill_readperm != 'undefined') && (result.printbill_readperm == '1')) {
                        $('#active_billing_pane a.print_mbillperm').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                    }else if ((typeof result.printbill_hideperm != 'undefined') && result.printbill_hideperm == '1') {
                        $('#active_billing_pane a.print_mbillperm').addClass('hide');
                    }
                    
                    if ((typeof result.setupbill_readperm != 'undefined') && (result.setupbill_readperm == '1')) {
                        $('#setup_billing_pane').find('a').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                        $('#setup_billing_pane').find('button').prop('disabled', true);
                    }else{
                        if ((typeof result.setupbill_addperm != 'undefined') && (result.setupbill_addperm == '1')) {
                            $('#setup_billing_pane').find('button').prop('disabled', false);
                        }else{
                            $('#setup_billing_pane').find('button').prop('disabled', true);
                        }
                        if ((typeof result.setupbill_editperm != 'undefined') && (result.setupbill_editperm == '1')) {
                            $('#setup_billing_pane').find('a').removeAttr('style');
                        }else{
                            $('#setup_billing_pane').find('a').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                        }
                    }
                    
                }
            }
        }
    });
}

function user_amtcredit_listing() {
    //$('.loading').removeClass('hide');
    var cust_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/user_amtcredit_listing',
        type: 'POST',
        dataType: 'json',
        data: 'subscriber_uuid='+cust_uuid,
        async: false,
        success: function(result){
            var nxtlimit = result.limit;
            var nxtofset = result.offset;
            $('#rcptbill_limit').val(nxtlimit); $('#rcptbill_offset').val(nxtofset);
            if (result.loadmore == 1) {
               $('.rcptbill_loadmore').removeClass('hide');
            }else{
               $('.rcptbill_loadmore').addClass('hide');
            }
            $('#totalwallet_amt').html('Amount Received: '+result.totalwallet_amt);
            $('#creditbill_summary').html(result.creditbill_listing);
            
            if (result.superadmin_perm != 1) {
                if ((typeof result.delrcpt_readperm != 'undefined') && (result.delrcpt_readperm == '1')) {
                    $('#active_billing_pane a.delete_mbillperm').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                }else if ((typeof result.delrcpt_hideperm != 'undefined') && result.delrcpt_hideperm == '1') {
                    $('#active_billing_pane a.delete_mbillperm').addClass('hide');
                }
            }
        }
    });
}

function addtowallet(add_receipt = '') {
    $('.loading').removeClass('hide');
    $('#addtowalletModal').modal('hide');
    $('#addwallet_receiptModal').modal('hide');
    var formdata =  $('#addtowallet_billing_form').serialize();


    $.ajax({
        url: base_url+'user/addtowallet',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        async: false,
        success: function(data){
            $.ajax({
                url: base_url+'user/user_billing_listing',
                type: 'POST',
                dataType: 'json',
                data: 'subscriber_uuid='+data,
                async: false,
                success: function(result){
                    $('.loading').addClass('hide');
                    var cocurr = $('#ispcountry_currency').val();
                    $('#wallet_credit_limit').html(cocurr+' '+result.user_creditlimit+'.00');
                    $('#user_wallet_amt').html(cocurr+' '+result.wallet_amt);
                    $('#net_walletamt').val(result.wallet_amt);
                    
                    $('#debitbill_summary').html(result.debitbill_listing);
                    $('#instsecu_bill_summary').html(result.instsecugen);
                    $('#totalbill_amt').html('Total Bill: '+result.totalbill_amt)
                    user_amtcredit_listing();
                }
            });
        }
    }); 
}

function edit_walletbillentry(billid, billtype){
    $('.loading').removeClass('hide');
    $.ajax({
        url: base_url+'user/getwallet_billdetails',
        type: 'POST',
        data: 'billid='+billid,
        dataType: 'json',
        success: function(data){
            $('input[name="addtowallet_bill_datetimeformat"]').val(data.bill_datetimeformat);
            $('input[name="addtowallet_bill_datetime"]').val(data.bill_added_on);
            $('input[name="addtowallet_receipt_number"]').val(data.receipt_number);
            $('select[name="addtowallet_payment_mode"] option[value="'+data.payment_mode+'"]').prop('selected', true);
            $('input[name="addtowallet_amount"]').val(data.total_amount);
            $('input[name="wallet_amount_received"][value="'+data.wallet_amount_received+'"]').prop('checked', true);
            
            $('.chqddpaytm').addClass('hide');
            $('.bill_bankdetails').addClass('hide');
            if ((data.payment_mode == 'dd') || (data.payment_mode == 'viapaytm') || (data.payment_mode == 'netbanking')) {
                if (typeof data.cheque_dd_paytm_number != 'undefined') {
                    $('input[name="update_cheque_dd_paytm"]').val(data.cheque_dd_paytm_number);
                    $('.chqddpaytm').removeClass('hide');
                }
            }
            else if (data.payment_mode == 'cheque') {
                if (typeof data.cheque_dd_paytm_number != 'undefined') {
                    $('input[name="update_cheque_dd_paytm"]').val(data.cheque_dd_paytm_number);
                    $('.chqddpaytm').removeClass('hide');
                }
                $('input[name="bill_bankaccount_number"]').val(data.account_number);
                $('input[name="bill_bankaddress"]').val(data.branch_address);
                $('input[name="bill_bankissued_date"]').val(data.bankslip_issued_date);
                $('input[name="bill_bankname"]').val(data.branch_name);
                $('.bill_bankdetails').removeClass('hide');
            }
            
            $('input[name="walletid_foredit"]').val(data.wallet_id);
            $('input[name="walletbillid_foredit"]').val(billid);
            
            $('#addtowalletModal').modal('show');
            $('.loading').addClass('hide');
        }
    })
}

function delete_walletentry(billid){
    var uuid = $('.subscriber_uuid').val();
    var c = confirm("Are you sure, you want to delete this receipt ?");
    if (c) {
        $('.loading').removeClass('hide');
        $.ajax({
            url: base_url+'user/delete_walletentry',
            type: 'POST',
            dataType: 'json',
            data: 'billid='+billid,
            success: function(data){
                user_billing_listing();
                $('.loading').addClass('hide');
            }
        });
    }
}

function delete_billentry(billid) {
    var uuid = $('.subscriber_uuid').val();
    var c = confirm("Are you sure, You want to delete this bill ?");
    if (c) {
        $('.loading').removeClass('hide');
        $.ajax({
            url: base_url+'user/delete_billentry',
            type: 'POST',
            dataType: 'json',
            data: 'billid='+billid+'&uid='+uuid,
            success: function(data){
                user_billing_listing();
                $('.loading').addClass('hide');
            }
        });
    }
}

function delete_custbillentry(billid,isnextbillgen) {
    var uuid = $('.subscriber_uuid').val();
    
    if (isnextbillgen == 0) {
        var c = confirm("This is your latest bill invoice. If you delete, then your recurring bill cron will stop for this product. \n Are you still sure, You want to delete this bill ?");
    }else{
        var c = confirm("Are you sure, You want to delete this bill ?");
    }
    if (c) {
        $('.loading').removeClass('hide');
        $.ajax({
            url: base_url+'user/delete_custbillentry',
            type: 'POST',
            dataType: 'json',
            data: 'billid='+billid+'&uid='+uuid,
            success: function(data){
                custombill_listing();
                $('.loading').addClass('hide');
            }
        });
    }
}

$('body').on('change', 'input[type=radio][name=change_walletlimit_option]', function(){
    var selopt = $(this).val();
    if (selopt == 'bycash') {
        $('.bycashopt').removeClass('hide');
        $('.bydaysopt').addClass('hide');
        $('input[name="update_walletLimit_bycash"]').prop('required', true);
        $('select[name="update_walletLimit_bydays"]').prop('required', false);
    }else{
        $('.bycashopt').addClass('hide');
        $('.bydaysopt').removeClass('hide');
        $('input[name="update_walletLimit_bycash"]').prop('required', false);
        $('select[name="update_walletLimit_bydays"]').prop('required', true);
    }
});

function exceed_walletlimit(slug='') {
    if (slug == 'modalshow') {
        $('#exceedWalletLimitModal').modal('show');
        $('form#exceed_walletLimit_form')[0].reset();
        $('input[name="update_walletLimit_bycash"]').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched');
        $('input[name="update_walletLimit_bycash"]').val('');
        $('select[name="update_walletLimit_bydays"] option[value=""]').val();
        $('.bycashopt').addClass('hide');
        $('.bydaysopt').addClass('hide');
    }
    
    var walletLimit = '';
    var optionsel = $('input[type=radio][name=change_walletlimit_option]:checked').val();
    if (optionsel == 'bycash') {
        walletLimit = $('input[name="update_walletLimit_bycash"]').val();
    }else if (optionsel == 'bydays') {
        walletLimit = $('select[name="update_walletLimit_bydays"] option:selected').val();
    }
    
    var uuid = $('.subscriber_uuid').val();

    if (walletLimit != '') {
        $.ajax({
            url: base_url+'user/update_walletLimit',
            type: 'POST',
            dataType: 'json',
            data: 'walletLimit='+walletLimit+'&uuid='+uuid+'&optionsel='+optionsel,
            success: function(data){
                $('#exceedWalletLimitModal').modal('hide');
                if (data.user_suspended == '1') {
                    $('#userstatus_changealert').val(0);
                    $('#userstatus_changealert').parent().removeClass('btn-success');
                    $('#userstatus_changealert').parent().addClass('btn-danger off');
                }
                user_billing_listing();
                $('.loading').addClass('hide');
            }
        });
    }
}

function defaultbilltab_details() {
    $('.loading').removeClass('hide');
    var subsid = $('.subscriber_userid').val();
    var subs_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/planassoc_withuser',
        type: 'POST',
        dataType: 'json',
        data: 'subsid='+subsid+'&subs_uuid='+subs_uuid,
        success: function(data){
            $('.loading').addClass('hide');
            if (data == 0) {
                $('#exceedlimit').addClass('hide');
                $('#inactive_billing').removeClass('hide');
                $('#active_billing').addClass('hide');
            }else{
                $('#inactive_billing').addClass('hide');
                $('#active_billing').removeClass('hide');
                user_billing_listing();
            }
            //$('#inactive_billing').addClass('hide');
            //$('#active_billing').removeClass('hide');
        }
    });
}

$('body').on('keyup', '.dbbill_amount', function(){
    var billamt = $(this).val();
    var amtdisc = $('.dbbill_discount').val();
    if (amtdisc != '') {
        billamt = Math.round(billamt - ((billamt * amtdisc) / 100));
    }
    $('.dbbill_total_amount').val(billamt);
});

$('body').on('change', '#dbinstall_bill_discount', function(){
    //alert('hi');
    var amtdisc = $(this).val();
    var billamt = $('#dbinstall_bill_amount').val();
    if (amtdisc != '' && billamt != '') {
        billamt = Math.round(billamt - ((billamt * amtdisc) / 100));
    }
    $('#dbinstall_bill_total_amount').val(billamt);
});

$('body').on('change', '#dbsecurity_bill_discount', function(){
    var amtdisc = $(this).val();
    var billamt = $('#dbsecurity_bill_amount').val();
    if (amtdisc != '' && billamt != '') {
        billamt = Math.round(billamt - ((billamt * amtdisc) / 100));
    }
    $('#dbsecurity_bill_total_amount').val(billamt);
});


function billcopy_actions(billid,billtype,uid,action) {
    $('.loading').removeClass('hide');
    $.ajax({
        url: base_url+'user/billcopy_actions',
        type: 'POST',
        data: 'billid='+billid+'&billtype='+billtype+'&uid='+uid+'&action='+action,
        async: false,
        success: function(data){
            if ((billtype != 'downloadbill') || (billtype != 'sendbill')) {
                $('#billhtmldiv').html(data);
                $('#viewbillModal').modal('show');
            }
            $('.loading').addClass('hide');
        }
    });
}

$('body').on('click', '#_custombillModal', function(){
    $('.listcustombilldiv').addClass('hide');
    $('.addcustombilldiv').removeClass('hide');
    $('#custformtitle').html('ADD CUSTOM BILL');
    $('.custom_billid').val('');
    var d = new Date();
    var time = '   '+d.toLocaleTimeString();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var currdate = ((''+day).length<2 ? '0' : '')+day + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + d.getFullYear() ;
    var seconds = d.getSeconds();
    var tkttime = d.getHours() +""+ d.getMinutes() +""+ ((''+seconds).length<2 ? '0' : '')+seconds ;
    var tktdate = day +""+ month +""+ d.getFullYear().toString().substr(-2) ;
    
    $('#custombillingform')[0].reset();
    //$('.custom_billnumber').html(tktdate+tkttime);
    $('input[name="custom_billnumber"]').val(tktdate+tkttime);
    $('.custinvoicelist').remove();
    $('.addcustbillbtn').addClass('hide');
    $.ajax({
        url: base_url+'user/getdetails_custombilling',
        type: 'POST',
        dataType: 'json',
        success: function(data){
            $('#company_namelist').empty().append('<option value="">Select Company</option>');
            $('#company_namelist').append(data.company_list);
            $('#topics_namelist').empty().append('<option value="">Select Product</option>');
            $('#topics_namelist').append(data.topic_list);
            $('input[name="custom_billing_type"]').prop('checked', false);
            $('select[name="custombill_monthwise"] option[value="0"]').prop('selected', true);
            $('select[name="custombill_daywise"] option[value="0"]').prop('selected', true)
            $('.monthly-wise').addClass('hide');
            $('.day-wise').addClass('hide');
            $('input[name="custom_billing_type"]').prop('disabled',false);
            $('select[name="custombill_daywise"]').prop('disabled', false);
        }
    });
    //$('#customBillModal').modal('show');
});

$('select[name="rcptcustom_payment_mode"]').on('change', function() {
    var payoption = $(this).val();
    if ((payoption == 'cheque') || (payoption == 'dd')) {
        $('.rcptcustom_chqddpaytm').removeClass('hide');
        $('.rcptcustom_bill_bankdetails').removeClass('hide');
        $('.req_cheque_dd').attr('required', 'required');
    }
    else{
        $('.rcptcustom_chqddpaytm').addClass('hide');
        if ((payoption == 'viapaytm') || (payoption == 'netbanking')){
            $('.rcptcustom_chqddpaytm').removeClass('hide');
            $('input[name="rcptcustom_cheque_dd_paytm"]').attr('required', 'required');
        }else{
            $('input[name="rcptcustom_cheque_dd_paytm"]').removeAttr('required');
            $('input[name="rcptcustom_cheque_dd_paytm"]').val('');
            $('.req_cheque_dd').val('');
        }
        $('.rcptcustom_bill_bankdetails').addClass('hide');
        $('.req_cheque_dd').removeAttr('required');
    }
});

function showadd_companyModal() {
    $('#addispcompany_form')[0].reset();
    $('#add_companyModal').modal('show');
}

function showadd_topicModal() {
    $('#addisptopic_form')[0].reset();
    $('#add_topicModal').modal('show');
}

function addisp_company() {
    $('#add_companyModal').modal('hide');
    $('.custbillloading').removeClass('hide');
    var formdata =  $('#addispcompany_form').serialize();
    $.ajax({
        url: base_url+'user/addisp_company',
        type: 'POST',
        data: formdata,
        dataType: 'json',
        success: function(data){
            $('.custbillloading').addClass('hide');
            $('#company_namelist').empty().append('<option value="">Select Company</option>');
            $('#company_namelist').append(data.company_list);
        }
    });
}

$('input[type=radio][name=bill_topictype]').change(function() {
    var topic_rate = $('input[name="topic_rate"]').val();
    if ((this.value == 'standard') && (topic_rate == '')) {
        $('input[name="topic_rate"]').attr('required',true);
    }else{
        $('input[name="topic_rate"]').attr('required',false);
    }
});

function addisp_topics() {
    var bill_topictype = $('input[type=radio][name=bill_topictype]:checked').val();
    var topic_rate = $('input[name="topic_rate"]').val();
    var err = 0;
    if ((bill_topictype == 'standard') && (topic_rate == '')) {
        err = 1;
        $('input[name="topic_rate"]').attr('required',true);
    }else{
        err = 0;
        $('input[name="topic_rate"]').attr('required',false);
    }
    
    if (err == 0) {
        $('.custbillloading').removeClass('hide');
        $('#add_topicModal').modal('hide');
        var formdata =  $('#addisptopic_form').serialize();
        $.ajax({
            url: base_url+'user/addisp_topic',
            type: 'POST',
            data: formdata,
            dataType: 'json',
            success: function(data){
                $('.custbillloading').addClass('hide');
                $('#topics_namelist').empty().append('<option value="">Select Product</option>');
                $('#topics_namelist').append(data.topic_list);
            }
        });
    }
}

function gettopicdetails(topicid) {
    if (topicid != '') {    
        $('.otherproductdetails').removeClass('hide');
        $('input[name="billprodprice"]').attr('required', true);
        $('input[name="billprodqty"]').attr('required', true);
        $('input[name="billprodprice"]').val('');
        $('input[name="billprodqty"]').val('1');
        $.ajax({
            url: base_url+'user/gettopicdetails',
            type: 'POST',
            data: 'topicid='+topicid,
            dataType: 'json',
            success: function(data){
                $('input[name="billprodprice"]').val(data.price);
            }
        });
    }else{
        $('.otherproductdetails').addClass('hide');
        $('input[name="billprodprice"]').attr('required', false);
        $('input[name="billprodqty"]').attr('required', false);
        $('input[name="billprodprice"]').val('');
        $('input[name="billprodqty"]').val('');
    }
}

function calculate_custbilltax(companyid) {
    var billid = $('input[name="custom_billid"]').val();
    var cust_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/calculate_custbilltax',
        type: 'POST',
        data: 'companyid='+companyid+'&billid='+billid+'&cust_uuid='+cust_uuid,
        dataType: 'json',
        success: function(data){
            $('#custbilltax_ratio').html('( '+data.billtax+'% )');
            $('#custbilltaxamt').html(data.billtax_amt);
        }
    });
}

function addcustombill_invoice() {
    var cust_uuid = $('.subscriber_uuid').val();
    $('.custbillloading').removeClass('hide');
    var billid = $('input[name="custom_billid"]').val();
    var topicid = $('select[name="billcompany_topics"] option:selected').val();
    var itmprice = $('input[name="billprodprice"]').val();
    var itmqty = $('input[name="billprodqty"]').val();
    var billcompanyid = $('select[name="billcompany_name"] option:selected').val();
    $.ajax({
        url: base_url+'user/addcustombill_invoice',
        type: 'POST',
        data: 'cust_uuid='+cust_uuid+'&billid='+billid+'&topicid='+topicid+'&itmprice='+itmprice+'&itmqty='+itmqty+'&companyid='+billcompanyid,
        dataType: 'json',
        success: function(data){
            $('.custbillloading').addClass('hide');
            $('#prod_blanklist').remove();
            $('#custbill_prodlist').prepend(data.custinvoicelist);
            $('#invoice_subtotal').html(data.subtotal);
            $('#custbilltax_ratio').html('( '+data.billtax+'% )');
            $('#custbilltaxamt').html(data.billtax_amt);
            
            var discount = $('#custbill_discount').val();
            var totalbill = ((parseInt(data.subtotal) + parseInt(data.billtax_amt)) - parseInt(discount));
            $('#totalcustbillamt').html(totalbill);
            $('#topics_namelist option[value=""]').prop('selected', true);
            $('.otherproductdetails').addClass('hide');
            $('input[name="billprodprice"]').attr('required', false);
            $('input[name="billprodqty"]').attr('required', false);
            $('input[name="billprodprice"]').val('');
            $('input[name="billprodqty"]').val('');
            $('select[name="billcompany_topics"]').attr('required', false);
        }
    });
}

function delete_custbillinvoice(billid,invoiceid) {
    var uuid = $('.subscriber_uuid').val();
    var billcompanyid = $('select[name="billcompany_name"] option:selected').val();
    var c = confirm("Are you sure, You want to delete this invoice list ?");
    if (c) {
        $('.custbillloading').removeClass('hide');
        $.ajax({
            url: base_url+'user/delete_custbillinvoice',
            type: 'POST',
            dataType: 'json',
            data: 'billid='+billid+'&uid='+uuid+'&invoiceid='+invoiceid+'&companyid='+billcompanyid,
            success: function(data){
                $('#invid_'+invoiceid).remove();
                if (data.invoices_exists == 0) {
                    $('select[name="billcompany_topics"]').attr('required', true);
                }
                $('#invoice_subtotal').html(data.subtotal);
                $('#custbilltax_ratio').html('( '+data.billtax+'% )');
                $('#custbilltaxamt').html(data.billtax_amt);
                
                var discount = $('#custbill_discount').val();
                var totalbill = ((parseInt(data.subtotal) + parseInt(data.billtax_amt)) - parseInt(discount));
                $('#totalcustbillamt').html(totalbill);
                $('.custbillloading').addClass('hide');
            }
        });
    }
}

function generate_custombilling(){
    $('.loading').removeClass('hide');
    var cust_uuid = $('.subscriber_uuid').val();
    var custom_billnumber = $('input[name="custom_billnumber"]').val();
    var billcompanyid = $('select[name="billcompany_name"] option:selected').val();
    var billdate = $('input[name="custom_billdbdate"]').val();
    var cuctombill_remarks = $('textarea[name="cuctombill_remarks"]').val();
    var cuctombill_comments = $('textarea[name="cuctombill_comments"]').val();
    var billid = $('input[name="custom_billid"]').val();
    var custbill_discount = $('input[name="custbill_discount"]').val();
    
    var custombill_monthwise = $('select[name="custombill_monthwise"] option:selected').val();
    if (typeof custombill_monthwise == 'undefined') {
        custombill_monthwise = 0;
    }
    var custombill_daywise = $('select[name="custombill_daywise"] option:selected').val();
    if (typeof custombill_daywise == 'undefined') {
        custombill_daywise = 0;
    }
    
    
    $('.cust_billbtn').addClass('hide');
    $.ajax({
        url: base_url+'user/generate_custombilling',
        type: 'POST',
        data: 'custom_billnumber='+custom_billnumber+'&cust_uuid='+cust_uuid+'&billcompany_name='+billcompanyid+'&billdate='+billdate+'&remarks='+cuctombill_remarks+'&comments='+cuctombill_comments+'&billid='+billid+'&discount='+custbill_discount+'&custombill_daywise='+custombill_daywise+'&custombill_monthwise='+custombill_monthwise,
        dataType: 'json',
        async: false,
        success: function(data){
            if (data.message == 'ok') {
                custombill_listing();
            }else{
                $('.custbill_errortext').html(data.message);
                $('.cust_billbtn').removeClass('hide');
                $('.loading').addClass('hide');
            }
        }
    });
}

function update_custombilling(){
    $('.loading').removeClass('hide');
    var custom_billnumber = $('input[name="custom_billnumber_toedit"]').val();
    var billcompanyid = $('#custombillingform_toedit select[name="billcompany_name"] option:selected').val();
    var billid_toedit = $('input[name="custom_billid_toedit"]').val();
    var cust_uuid = $('.subscriber_uuid').val();
    $('.cust_billbtn').addClass('hide');
    $.ajax({
        url: base_url+'user/update_custombilling',
        type: 'POST',
        data: 'custom_billnumber='+custom_billnumber+'&cust_uuid='+cust_uuid+'&billcompany_name='+billcompanyid+'&billid_toedit='+billid_toedit,
        dataType: 'json',
        async: false,
        success: function(data){
            if (data.message == 'ok') {
                $('#editcustomBillModal').modal('hide');
                custombill_listing();
            }else{
                $('.custbill_errortext').html(data.message);
                $('.cust_billbtn').removeClass('hide');
                $('.loading').addClass('hide');
            }
        }
    });
}

function custombill_listing(){
    $('.loading').removeClass('hide');
    var subsid = $('.subscriber_userid').val();
    var subs_uuid = $('.subscriber_uuid').val();
    $('.listcustombilldiv').removeClass('hide');
    $('.addcustombilldiv').addClass('hide');
    $('.addcustbillbtn').removeClass('hide');
    $.ajax({
        url: base_url+'user/custombilling_listing',
        type: 'POST',
        dataType: 'json',
        data: 'subsid='+subsid+'&subs_uuid='+subs_uuid,
        async: false,
        success: function(data){
            $('.loading').addClass('hide');
            $('#custombill_summary').html(data.custombill_list);
            $('#custombill_rcptsummary').html(data.customrcpt_list);
            $('#showtotalcustbillamt').html('Total Bill: '+data.totalbillamt);
            $('#showtotalcustbillrcpt').html('Amount Received: '+data.totalrcptamt);
            $('#showbalcustbillamt').html(data.totalbalamt);
            if (data.superadmin_perm != 1) {
                if (data.custbilltab_readperm == 1) {
                    $('#custom_billing_pane').find('a').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                    $('#custom_billing_pane').find('button, input').prop('disabled', true);
                }else{
                    if ((typeof data.custbill_addperm != 'undefined') && (data.custbill_addperm == '1')) {
                        $('#custom_billing_pane').find('button, input').prop('disabled', false);
                    }else{
                        $('#custom_billing_pane').find('button, input').prop('disabled', true);
                    }
                    if (((typeof data.custbill_editperm != 'undefined') && (data.custbill_editperm == '1')) && ((typeof data.custbill_deleteperm != 'undefined') && (data.custbill_deleteperm == '1'))) {
                    }else{
                        if ((typeof data.custbill_editperm != 'undefined') && (data.custbill_editperm == '1')) {
                            $('#custom_billing_pane a.delete_cbillperm').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                        }
                        if ((typeof data.custbill_deleteperm != 'undefined') && (data.custbill_deleteperm == '1')) {
                            $('#custom_billing_pane a.edit_cbillperm').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                        }
                    }
                }
            }
        }
    });
}

function edit_custbillentry(billid, billtype='') {
    $('.loading').removeClass('hide');
    $('.listcustombilldiv').addClass('hide');
    $('.addcustombilldiv').removeClass('hide');
    $('.addcustbillbtn').addClass('hide');
    $('#custformtitle').html('EDIT CUSTOM BILL');
    $.ajax({
        url: base_url+'user/getdetails_custombilling',
        type: 'POST',
        async: false,
        dataType: 'json',
        success: function(data){
            $('#company_namelist').empty().append('<option value="">Select Company</option>');
            $('#company_namelist').append(data.company_list);
            $('#topics_namelist').empty().append('<option value="">Select Topic</option>');
            $('#topics_namelist').append(data.topic_list);
        }
    });
    $.ajax({
        url: base_url+'user/getcustombill_details',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: 'billid='+billid,
        success: function(data){
            $('.custinvoicelist').remove();
            $('input[name="custom_billnumber"]').val(data.bill_number);
            $('.custom_billid').val(data.id);
            //$('.hidewhenedit').addClass('hide');
            $('.custom_billdate').val(data.datepicker);
            $('input[name="custom_billdbdate"]').val(data.added_on);
            $('select#company_namelist option[value="'+data.bill_companyid+'"]').prop('selected', true);
            
            $('#custbill_prodlist').prepend(data.custinvoicelist);
            $('#invoice_subtotal').html(data.subtotal);
            $('#custbilltax_ratio').html('( '+data.billtax+'% )');
            $('#custbilltaxamt').html(data.billtax_amt);
            
            var totalbill = ((parseInt(data.subtotal) + parseInt(data.billtax_amt)) - parseInt(data.discount));
            $('#totalcustbillamt').html(totalbill);
            
            $('#custbill_discount').val(data.discount);
            $('#topics_namelist option[value=""]').prop('selected', true);
            $('textarea[name="cuctombill_remarks"]').val(data.bill_remarks);
            $('textarea[name="cuctombill_comments"]').val(data.bill_comments);
            if (data.invoice_exists == 0) {
                $('select[name="billcompany_topics"]').attr('required', true);
            }else{
                $('select[name="billcompany_topics"]').attr('required', false);
                $('input[name="billprodprice"]').attr('required', false);
                $('input[name="billprodqty"]').attr('required', false);
            }
            
            if ((data.billgenerated_afterdays == 0) && (data.billgenerated_aftermonth == 0)) {
                $('input[name="custom_billing_type"]').prop('checked', false);
                $('select[name="custombill_monthwise"] option[value="0"]').prop('selected', true);
                $('select[name="custombill_daywise"] option[value="0"]').prop('selected', true)
                $('.monthly-wise').addClass('hide');
                $('.day-wise').addClass('hide');
            }
            if ((data.billgenerated_afterdays > 0)){
                $('input[name="custom_billing_type"][value="day"]').prop('checked',true);
                $('select[name="custombill_daywise"] option[value="'+data.billgenerated_afterdays+'"]').prop('selected', true);
                $('.monthly-wise').addClass('hide');
                $('.day-wise').removeClass('hide');
                var isnext_billgenerated = data.isnext_billgenerated;
                if (isnext_billgenerated == 1) {
                    $('input[name="custom_billing_type"]').prop('disabled',true);
                    $('select[name="custombill_daywise"]').prop('disabled', true);
                }else{
                    $('input[name="custom_billing_type"]').prop('disabled',false);
                    $('select[name="custombill_daywise"]').prop('disabled', false);
                }
            }
            if ((data.billgenerated_aftermonth > 0)){
                $('input[name="custom_billing_type"][value="month"]').prop('checked',true);
                $('select[name="custombill_monthwise"] option[value="'+data.billgenerated_aftermonth+'"]').prop('selected', true);
                $('.monthly-wise').removeClass('hide');
                $('.day-wise').addClass('hide');
                var isnext_billgenerated = data.isnext_billgenerated;
                if (isnext_billgenerated == 1) {
                    $('input[name="custom_billing_type"]').prop('disabled',true);
                    $('select[name="custombill_daywise"]').prop('disabled', true);
                }else{
                    $('input[name="custom_billing_type"]').prop('disabled',false);
                    $('select[name="custombill_daywise"]').prop('disabled', false);
                }
            }
            
            $('.loading').addClass('hide');
        }
    });
    
}

function custombill_editinvoice() {
    var formdata = $('#custombillingform_toedit').serialize();
    var cust_uuid = $('.subscriber_uuid').val();
    var billcompanyid = $('#custombillingform_toedit select[name="billcompany_name"] option:selected').val();
    var billid_toedit = $('input[name="custom_billid_toedit"]').val();
    $.ajax({
        url: base_url+'user/addcustombill_invoice',
        type: 'POST',
        data: formdata+'&cust_uuid='+cust_uuid+'&billid='+billid_toedit,
        dataType: 'json',
        success: function(data){
            $('#custinvoicelist_toedit').html(data.custinvoicelist);
            $('#custombillingform_toedit')[0].reset();
            $('#custombillingform_toedit select[name="billcompany_name"]').val(billcompanyid);
        }
    });
}

function show_customReceiptModal(billid, bill_no, pending_amount) {
    $('form#custombill_receiptform')[0].reset();
    $('input[name="rcptcustom_billid"]').val(billid);
    $('input[name="rcptcustom_billno"]').val(bill_no);
    $('input[name="rcptcustom_amount"]').val(pending_amount);
    $('input[name="rcptcustom_amount"]').attr('max', pending_amount);
    
    $('#customBillReceiptModal').modal('show');
}

function addcustombillrcpt() {
    var formdata = $('form#custombill_receiptform').serialize();
    $.ajax({
        url: base_url+'user/addcustombillrcpt',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        async: false,
        success: function(data){
            $('#customBillReceiptModal').modal('hide');
            custombill_listing();
        }
    });
}

function delete_custbillrcptentry(rcptid, billid) {
    var c = confirm('Are you sure, you want to delete this receipt?');
    if (c) {
        $('.loading').removeClass('hide');
        $.ajax({
            url: base_url+'user/delete_custbillrcptentry',
            type: 'POST',
            dataType: 'json',
            data: 'rcptid='+rcptid+'&billid='+billid,
            success: function(data){
                custombill_listing();
            }
        });
    }
}

function closeCustomBillHandler(delconf=''){
    var cust_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/customBillHandler',
        type: 'POST',
        data: 'cust_uuid='+cust_uuid+'&deleteconfirm='+delconf,
        dataType: 'json',
        success: function(data){
            if (delconf == 0) {
                if (data.isexists == 0) {
                    $('#confirm_cancel_custombill').modal('hide');
                    $('#customBillModal').modal('hide');
                }else if(data.isexists == 1){
                    $('#confirm_cancel_custombill').modal('show');
                }
            }else{
                $('#confirm_cancel_custombill').modal('hide');
                $('#customBillModal').modal('hide');
                user_billing_listing();
                
            }
        }
    });
}

$('body').on('change', 'input[type=radio][name=custom_billing_type]', function(){
    var billtype = $(this).val();
    $('select[name="custombill_monthwise"] option[value="0"]').prop('selected', true);
    $('select[name="custombill_daywise"] option[value="0"]').prop('selected', true);
    if (billtype == 'monthly') {
        $('.monthly-wise').removeClass('hide');
        $('.day-wise').addClass('hide');
    }else if (billtype == 'day') {
        $('.monthly-wise').addClass('hide');
        $('.day-wise').removeClass('hide');
    }
});

$('body').on('change', 'input[type=radio][name=discounttype]', function(){
    var disctype = $(this).val();
    $('select[name="editdbbill_percentdiscount"] option[value="0"]').prop('selected', true);
    $('input[name="editdbbill_flatdiscount"]').val('0');
    if (disctype == 'percent') {
        $('select[name="editdbbill_percentdiscount"]').removeClass('hide');
        $('input[name="editdbbill_flatdiscount"]').addClass('hide');
    }else if (disctype == 'flat') {
        $('select[name="editdbbill_percentdiscount"]').addClass('hide');
        $('input[name="editdbbill_flatdiscount"]').removeClass('hide');
    }
});

function edit_billentry(billid, billtype) {
    $('#billamount_errtxt').html('');
    if (billid != '') {
        $.ajax({
            url: base_url+'user/getbilldetail',
            type: 'POST',
            data: 'billid='+billid,
            dataType: 'json',
            async: false,
            success: function(data){
                if (typeof data.billid != 'undefined') {
                    if (billtype == 'advprepay') {
                        
                        $('input[name="advprepay_bill_datetime"]').remove();
                        $('input[name="advprepay_bill_datetimeformat"]').remove();
                        
                        $('#advprepay_billdtme').append('<input name="advprepay_bill_datetime" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="'+data.bill_added_on+'">');
                        $('input[name="advprepay_chqddrecpt"]').val(data.receipt_number);
                        $('.adverr').addClass('hide');
                        $('#prevadvpayid').val(billid);
                        $('input[name="advprepay_bill_number"]').val(data.bill_number);
                        $('select[name="advprepay_bill_payment_mode"]').val(data.paymode);
                        $('select[name="advprepay_month_count"]').val(data.advancepay_for_months);
                        $('select[name="advprepay_freemonth_count"]').val(data.number_of_free_months);
                        $('input[name="advprepay_bill_amount"]').val(data.actual_amount);
                        
                        $('input[name="advprepay_discounttype"]').prop('checked', false);
                        $('input[name="advprepay_discounttype"][value="'+data.discounttype+'"]').prop('checked', true);
                        $('select[name="advprepay_bill_percentdiscount"]').addClass('hide');
                        $('input[name="advprepay_bill_flatdiscount"]').addClass('hide');
                        if (data.discounttype == 'percent') {
                            $('select[name="advprepay_bill_percentdiscount"]').removeClass('hide');
                            $('input[name="advprepay_bill_flatdiscount"]').addClass('hide');
                            $('select[name="advprepay_bill_percentdiscount"]').val(data.discount);
                        }else{
                            $('select[name="advprepay_bill_percentdiscount"]').addClass('hide');
                            $('input[name="advprepay_bill_flatdiscount"]').removeClass('hide');
                            $('input[name="advprepay_bill_flatdiscount"]').val(data.discount);
                        }
                        
                        //$('select[name="advprepay_bill_discount"]').val(data.discount);
                        $('input[name="advprepay_bill_total_amount"]').val(data.total_amount);
                        $('#plan_monthly_cost').val(data.plan_cost);
                        $('#userassoc_planid').val(data.srvid);
                        
                        $('#advance_prepayModal').modal('show');
                    }else{
                        $('form#editbilling_form')[0].reset();
                        $('#editbillid').val(billid);
                        $('#editbilltype').val(billtype);
                        $('#editdbbilldtme').val(data.bill_added_on);
                        $('#editdbbill_number').val(data.bill_number);
                        $('input[name="is_advanced_paid"][value="'+data.is_advanced_paid+'"]').prop('checked', true);
                        
                        $('#editbill_receiptamount').val(data.receipt_amount);
                        $('#editdbbill_billchqddrecpt').val(data.receipt_number);
                        $('#editbill_prevbillamount').val(data.total_amount);
                        $('#editdbbill_netamount').val(data.total_amount);
                        $('.editbillpaid').val(data.billpaid);
                        
                        var calctotal = '';
                        $('input[name="discounttype"]').prop('checked', false);
                        $('input[name="discounttype"][value="'+data.discounttype+'"]').prop('checked', true);
                        $('select[name="editdbbill_percentdiscount"]').addClass('hide');
                        $('input[name="editdbbill_flatdiscount"]').addClass('hide');
                        if (data.discounttype == 'percent') {
                            $('select[name="editdbbill_percentdiscount"]').removeClass('hide');
                            $('input[name="editdbbill_flatdiscount"]').addClass('hide');
                            $('select[name="editdbbill_percentdiscount"]').val(data.discount);
                            /*if (data.discount != '0') {
                                calctotal = parseInt((data.total_amount * 100) / (100-data.discount)) - parseInt(data.addiamount) ;
                            }else{
                                calctotal = parseInt(data.total_amount) - parseInt(data.addiamount);
                            }
                            
                            $('#editdbbill_billamount').val(calctotal);*/
                            
                            $('#editdbbill_billamount').prop('readonly', true);
                            $('#editdbbill_billamount').val(data.plan_cost);
                        }else{
                            $('select[name="editdbbill_percentdiscount"]').addClass('hide');
                            $('input[name="editdbbill_flatdiscount"]').removeClass('hide');
                            $('input[name="editdbbill_flatdiscount"]').val(data.discount);
                            /*calctotal = parseInt(data.total_amount)  - parseInt(data.addiamount) + parseInt(data.discount);
                            $('#editdbbill_billamount').val(calctotal);*/
                            
                            $('#editdbbill_billamount').prop('readonly', true);
                            $('#editdbbill_billamount').val(data.plan_cost);
                        }
                        
                        //calctotal = parseInt(data.total_amount) - parseInt(data.addiamount);
                        //$('#editdbbill_billamount').val(calctotal);
                        if (data.additional_chargesdiv != '') {
                            $('div.addichrgrow').not(':first').remove();
                            $(data.additional_chargesdiv).insertAfter('div.addichrgrow:last');
                        }else{
                            $('div.addichrgrow').not(':first').remove();
                        }
                        
                        $('textarea[name="editdbbill_comments"]').val(data.bill_comments);
                        $('textarea[name="editdbbill_remarks"]').val(data.bill_remarks);
                        
                        if (data.is_advanced_paid == 1) {
                            $('.disabledwhenpayadvanced').find('a').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
                            $('.disabledwhenpayadvanced').find('input,select').prop('disabled', true);
                        }else{
                            $('.disabledwhenpayadvanced').find('a').removeAttr('style');
                            $('.disabledwhenpayadvanced').find('input,select').prop('disabled', false);
                        }
                        
                        $('#editbillingModal').modal('show');
                    }
                }
            }
        });
    }
}

$('body').on('change', 'input[type=radio][name=is_advanced_paid]', function(){
    var is_advanced_paid = $(this).val();
    if (is_advanced_paid == 1) {
        $('.disabledwhenpayadvanced').find('a').css({'pointer-events' : 'none', 'cursor' : 'not-allowed'});
        $('.disabledwhenpayadvanced').find('input,select').prop('disabled', true);
    }else{
        $('.disabledwhenpayadvanced').find('a').removeAttr('style');
        $('.disabledwhenpayadvanced').find('input,select').prop('disabled', false);
    }
});

function showadded_topicModal() {
    $.ajax({
	url: base_url+'user/viewadded_topiclist',
	type: 'POST',
	dataType: 'json',
	success: function(data){
	    $('#viewadded_topiclist').html(data.topic_list);
	    $('#showadded_topicmodal').modal('show');
	}
    });
}

function delete_isptopics(topicid) {
    $('.loading').removeClass('hide');
    var c = confirm("Are you sure, You want to delete this ?");
    if (c) {
	$.ajax({
	    url: base_url+'user/delete_isptopics',
	    type: 'POST',
	    data: 'topic_id='+topicid,
	    dataType: 'json',
	    success: function(data){
		$('#topics_namelist').empty().append('<option value="">Select Product</option>');
                $('#topics_namelist').append(data.topic_list);
		$('#showadded_topicmodal').modal('hide');
                $('.loading').addClass('hide');
	    }
	});
    }else{
        $('.loading').addClass('hide');
    }
}

function updatebilldetails() {
    var billamount_err = $('#billamount_err').val();
    if (billamount_err != 1) {
        $('.loading').removeClass('hide');
        var formdata = $('form#editbilling_form').serialize();
        $.ajax({
            url: base_url+'user/updatebilling_charges',
            type: 'POST',
            data: formdata,
            dataType: 'json',
            async: false,
            success: function(data){
                $('#editbillingModal').modal('hide');  
                user_billing_listing();
                $('.loading').addClass('hide');
            }
        });
    }
}

function addmorecharges() {
   var addmore = '<div class="addichrgrow row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><input type="text" name="additional_chrgtxt[]" class="form-control" value="" placeholder="Enter Charges Details" style="font-size:12px" /></div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><input type="number" name="additional_chrgval[]" class="form-control" value=""  style="font-size:12px"/></div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 addrmvclass"><a href="javascript:void(0)" onclick="addmorecharges()"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add</a></div></div>';
   var removechrge = '<a href="javascript:void(0)" onclick="removemorecharges(this)"><i class="fa fa-minus" aria-hidden="true"></i> &nbsp;Remove</a>';
   
   $('div.addichrgrow').first().before(addmore);
   $('.addrmvclass').not(':first').html(removechrge);
   
}

function removemorecharges(obj) {
    var classname = $(obj).parent().parent().remove();
}

$('select[name="editdbbill_percentdiscount"]').on('change',  function(){
    var billamount = $('#editdbbill_billamount').val();
    var discount = $(this).val();
    var netamount = Math.round(billamount - ((billamount * discount)/100 ));
    var tax = $('.current_taxapplicable').val();
    netamount = Math.round(netamount) + Math.round((netamount * tax)/100);
    $('#editdbbill_netamount').val(netamount);
});

$('body').on('keyup', 'input[name="editdbbill_flatdiscount"]', function(){
    var billamount = $('#editdbbill_billamount').val();
    var discount = $(this).val();
    var netamount = Math.round(billamount - discount);
    var tax = $('.current_taxapplicable').val();
    netamount = Math.round(netamount) + Math.round((netamount * tax)/100);
    $('#editdbbill_netamount').val(netamount);
});

$('body').on('keyup', '#editdbbill_billamount', function(){
    var receiptamount = $('#editbill_receiptamount').val();
    var billamt = $(this).val();
    
    var disctype = $('input[type=radio][name=discounttype]:checked').val();
    if (disctype == 'percent') {
        var amtdisc = $('select[name="editdbbill_percentdiscount"] option:selected').val();
        billamt = Math.round(billamt - ((billamt * amtdisc) / 100));
    }else{
        var amtdisc = $('input[name="editdbbill_flatdiscount"]').val();
        billamt = Math.round(billamt -  amtdisc);
    }
    
    if (parseInt(billamt) < parseInt(receiptamount)) {
        $('#billamount_errtxt').html("Bill Amount can't be less than Receipt Amount");
        $('#billamount_err').val(1);
    }else{
        $('#billamount_errtxt').html("");
        $('#billamount_err').val(0);
    }
    var tax = $('.current_taxapplicable').val();
    billamt = Math.round(billamt) + Math.round((billamt * tax)/100);
    $('#editdbbill_netamount').val(billamt);
});

$('select[name="update_payment_mode"]').on('change', function() {
    var payoption = $(this).val();
    if (payoption == 'viawallet') {
        var cust_uuid = $('.subscriber_uuid').val();
        var billamt_topay = $('input[name="update_receipt_amount"]').val();
        $('.chqddpaytm').addClass('hide');
        $('input[name="update_cheque_dd_paytm"]').val('');
        $('input[name="update_cheque_dd_paytm"]').removeAttr('required');
        $('.bill_bankdetails').addClass('hide');
        $('.req_cheque_dd').val('');
        $('.req_cheque_dd').removeAttr('required');
        $.ajax({
            url: base_url+'user/getuser_walletbalance',
            type: 'POST',
            data: 'cust_uuid='+cust_uuid,
            async: 'false',
            success: function(data){
                var wbalance = data;
                if ( ((parseInt(wbalance) > 0) && (parseInt(wbalance) < parseInt(billamt_topay))) || (parseInt(wbalance) <= 0) ) {
                    $('#receiptamount_errtxt').html("You don't have sufficient wallet balance.");
                    $('#receiptamount_err').val(1);
                }else{
                    $('#receiptamount_errtxt').html("");
                    $('#receiptamount_err').val(0);
                }
            }
        })
    }
    else if ((payoption == 'cheque') || (payoption == 'dd')) {
        $('.chqddpaytm').removeClass('hide');
        $('.bill_bankdetails').removeClass('hide');
        $('.req_cheque_dd').attr('required', 'required');
    }
    else{
        $('.chqddpaytm').addClass('hide');
        if (payoption == 'viapaytm'){
            $('.chqddpaytm').removeClass('hide');
            $('input[name="update_cheque_dd_paytm"]').attr('required', 'required');
        }else{
            $('input[name="update_cheque_dd_paytm"]').removeAttr('required');
            $('input[name="update_cheque_dd_paytm"]').val('');
            $('.req_cheque_dd').val('');
        }
        $('.bill_bankdetails').addClass('hide');
        $('.req_cheque_dd').removeAttr('required');
    }
});

function getbillpastreceipts(){
    var billid = $('#billid_toupdate').val();
    $.ajax({
        url: base_url+'user/pastreceiptbill_listing',
        type: 'POST',
        data: 'billid_toupdate='+billid,
        dataType: 'json',
        success: function(data){
            $('#pastbill_listingdiv').html(data);
        }
    });
    
}

$('body').on('keyup', '#update_receipt_amount', function(){
    var billamt = $('#billpendingamount').val();
    var receiptamount = $(this).val();

    if (parseInt(billamt) < parseInt(receiptamount)) {
        $('#receiptamount_errtxt').html("Receipt Amount can't be greater than Bill Amount");
        $('#receiptamount_err').val(1);
    }else{
        $('#receiptamount_errtxt').html("");
        $('#receiptamount_err').val(0);
    }

});

function show_updateReceiptModal(billid, billtype) {
    $('#receiptamount_errtxt').html('');
    $.ajax({
        url: base_url+'user/getbilldetail',
        type: 'POST',
        data: 'billid='+billid+'&billtype='+billtype,
        dataType: 'json',
        async: false,
        success: function(data){
            if (typeof data.billid != 'undefined') {
                if (billtype == 'advprepay') {
                    $('input[name="advprepay_chqddrecpt"]').val(data.receipt_number);
                    $('.adverr').addClass('hide');
                    $('input[name="advprepay_bill_datetime"]').remove();
                    $('#dbbilldatetime').append('<input name="advprepay_bill_datetime" type="text" style="color:#ccc" readonly>');
                    $('#receipt_billid').val(billid);
                    $('input[name="advprepay_bill_datetime"]').val(data.bill_added_on);
                    $('input[name="advprepay_bill_number"]').val(data.bill_number);
                    $('select[name="advprepay_bill_payment_mode"]').val(data.paymode);
                    $('input[name="advprepay_month_count"]').val(data.advancepay_for_months);
                    $('input[name="advprepay_freemonth_count"]').val(data.number_of_free_months);
                    $('input[name="advprepay_bill_amount"]').val(data.actual_amount);
                    $('input[name="advprepay_bill_discount"]').val(data.discount);
                    $('input[name="advprepay_bill_total_amount"]').val(data.total_amount);
                    $('#planmonthly_cost').val(data.plan_cost);
                    $('#user_planid').val(data.srvid);
                    
                    $('#billmonth_count').val(data.advancepay_for_months);
                    $('#billfreemonth_count').val(data.number_of_free_months);
                    $('#bill_monthdiscount').val(data.discount);
                    $('#editadvance_prepayModal').modal('show');
                
                }else if (billtype == 'addtowallet') {
                    $('input[name="addtowallet_receipt_number"]').val(data.receipt_number);
                    $('input[name="addtowallet_bill_datetime"]').remove();
                    $('#dbwallet_billdtme').append('<input name="addtowallet_bill_datetime" type="text" style="color:#ccc" readonly>');
                    $('input[name="addtowallet_bill_datetime"]').val(data.bill_added_on);
                    $('#walletpayment_mode').val(data.paymode);
                    $('input[name="addtowallet_payment_mode"]').val(data.paymode);
                    $('input[name="addtowallet_amount"]').val(data.actual_amount);
                    $('input[name="wallet_billid"]').val(billid);
                    $('#addwallet_receiptModal').modal('show');
                }else{
                    if (data.billpaid == 1) {
                        $('#billpendingcost').html(data.pending_amount);
                        $('#billid_toupdate').val(billid);
                        $('.hideifbillpaid').addClass('hide');
                        mui.tabs.activate('past_receipt_pane');
                        getbillpastreceipts();
                    }else{
                        $('.hideifbillpaid').removeClass('hide');
                        mui.tabs.activate('add_receipt_pane');
                        $('#receiptupdate_form')[0].reset();
                        $('.chqddpaytm').addClass('hide');
                        $('.bill_bankdetails').addClass('hide');
                        
                        $('.subscriber_uuid').val(data.uuid);
                        $('#editbillpaid').val(data.billpaid);
                        $('#billid_toupdate').val(billid);
                        $('#billtype_toupdate').val(billtype);
                        $('#billpendingcost').html(data.pending_amount);
                        $('input[name="update_receipt_amount"]').val(data.pending_amount);
                        $('#billpendingamount').val(data.pending_amount);
                        //$('input[name="update_receipt_number"]').val(data.receipt_number);
                    }
                    $('#enterReceiptModal').modal('show');
                }
            }
        }
    });
    
}

function update_receiptform() {
    var receiptamount_err = $('#receiptamount_err').val();
    if (receiptamount_err != 1) { 
        $('.receiptbtn').addClass('hide');
        $('.loading').removeClass('hide');
        var billtype = $('#billtype_toupdate').val();
        var formdata = $('#receiptupdate_form').serialize();
        $.ajax({
            url: base_url+'user/update_receipt_number',
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function(data){
                $('#enterReceiptModal').modal('hide');
                $('.receiptbtn').removeClass('hide');
                if (billtype == 'custom_invoice') {
                    custombill_listing();
                }else{
                    user_billing_listing();
                }
                $('.loading').addClass('hide');
            }
        });
    }
}

function deletebill_receiptentry(billid, receiptid, receiptamt){
    var c = confirm("Are you sure, you want to delete this receipt entry.");
    if (c) {
        $.ajax({
            url: base_url+'user/deletebill_receiptentry',
            type: 'POST',
            dataType: 'json',
            data: 'billid='+billid+'&receiptid='+receiptid+'&receiptamt='+receiptamt,
            success: function(data){
                user_billing_listing();
                getbillpastreceipts();
            }
        });
    }
}


function triggerbillbanner(filename) {
    $( 'input[name="'+filename+'"]' ).trigger( 'click' );
}

$('body').on('change', '.billbanner1', function(){
    if (this.files && this.files[0]) {
        $('#bb1').html('');
        $('#bb1').append("<img id='previewbb1' src='' style='width:70%'/>");

	var reader = new FileReader();
	reader.onload = function(){
            var dataURL = reader.result;
            $('#previewbb1').attr('src', dataURL);
        };

	reader.readAsDataURL(this.files[0]);
    }
});

$('body').on('change', '.billbanner2', function(){
    if (this.files && this.files[0]) {
        $('#bb2').html('');
        $('#bb2').append("<img id='previewbb2' src='' style='width:70%'/>");

	var reader = new FileReader();
	reader.onload = function(){
            var dataURL = reader.result;
            $('#previewbb2').attr('src', dataURL);
        };

	reader.readAsDataURL(this.files[0]);
    }
});

function customizeinvoice(obj) {
    $('.loading').removeClass('hide');
    var formData = new FormData(obj);
    $.ajax({
        url: base_url+'user/customizeinvoice',
        type: 'POST',
        data: formData,
        async: false,
        success: function (data) {
           $('.loading').addClass('hide');
        },
        cache: false,
        contentType: false,
        processData: false
    });
}

$('body').on('click', '#billinvoicetab', function(){
    $('.loading').removeClass('hide');
    $.ajax({
        url: base_url+'user/getcustom_invoicedetail',
        type: 'POST',
        dataType: 'json',
        success: function(data){
            $('.loading').addClass('hide');
            $('input[name="billinvoicecolor"]').val(data.invoice_color);
            if (typeof data.invoice_banner1 != 'undefined') {
                $('#bb1').html("<img id='previewbb1' src='"+data.invoice_banner1+"' style='width:70%'/>");    
            }
            if (typeof data.invoice_banner2 != 'undefined') {
                $('#bb2').html("<img id='previewbb2' src='"+data.invoice_banner2+"' style='width:70%'/>");
            }
        }
    });
});

function show_wallethistory(uuid) {
    $('.loading').removeClass('hide');
    $.ajax({
        url: base_url+'user/show_wallethistory',
        type: 'POST',
        data: 'uuid='+uuid,
        success: function(data){
            $('.loading').addClass('hide');
            $('#walletbalhistory').html(data);
            $('#walletbalhistoryModal').modal('show');
            
        }
    });
}

$('body').on('change', '.collapse_allbillcheckbox', function(){
    var status = this.checked;
    $('.collapse_billcheckbox').each(function(){ 
       this.checked = status;
    });
    if (status) {
       $('.billpdfexport').removeClass('hide');
       $('.billexcelexport').removeClass('hide');
    }else{
       $('.billpdfexport').addClass('hide');
       $('.billexcelexport').addClass('hide');
    }
 });

$('body').on('change', '.collapse_billcheckbox', function(){
  if(this.checked == false){ 
     $(".collapse_allbillcheckbox")[0].checked = false; 
  }
  if ($('.collapse_billcheckbox:checked').length == $('.collapse_billcheckbox').length ){
     $(".collapse_allbillcheckbox")[0].checked = true; 
  }
  
  if ($('.collapse_billcheckbox:checked').length > 0) {
     $('.billpdfexport').removeClass('hide');
     $('.billexcelexport').removeClass('hide');
  }else{
     $('.billpdfexport').addClass('hide');
     $('.billexcelexport').addClass('hide');
  }
});

function exportbillpdf_tousers() {
    $('.loading').removeClass('hide');
    seluids = [];
    $("input:checkbox[name=export_billid]:checked").each(function(){
       seluids.push($(this).val());
    });
    if (seluids.length > 0) {
       $('form.billpdf_exportform input[name="billids_toexport"]').val(seluids);
       $('.billpdf_exportform').submit();
       $('.loading').addClass('hide');
    }
 }
 
function exportbillexcel_tousers() {
    $('.loading').removeClass('hide');
    seluids = [];
    $("input:checkbox[name=export_billid]:checked").each(function(){
       seluids.push($(this).val());
    });
    if (seluids.length > 0) {
       $('form.billexcel_exportform input[name="billids_toexport"]').val(seluids);
       $('.billexcel_exportform').submit();
       $('.loading').addClass('hide');
    }
 }
 
 
