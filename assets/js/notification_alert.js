$('body').on('click', '#user_notification_listing', function(){
    var subs_uuid = $('.subscriber_uuid').val();
    $('.gateway_message').html('');
    $('#textsms_tosend').val('');
    $('.loading').removeClass('hide');
    $.ajax({
        url: base_url+'user/notification_listing',
        type: 'POST',
        dataType: 'json',
        data: 'uuid='+subs_uuid,
        async: false,
        success: function(data){
            $('input[name=sms_notify][value='+data.sms_notify+']').attr('checked', 'checked');
            $('input[name=email_notify][value='+data.email_notify+']').attr('checked', 'checked');
            $('#notificationlisting').html(data.notifylist);
            $('.loading').addClass('hide');
        }
    });
});

function user_notification_listing() {
    var subs_uuid = $('.subscriber_uuid').val();
    $('.gateway_message').html('');
    $('#textsms_tosend').val('');
    $('.loading').removeClass('hide');
    $.ajax({
        url: base_url+'user/notification_listing',
        type: 'POST',
        dataType: 'json',
        data: 'uuid='+subs_uuid,
        async: false,
        success: function(data){
            $('input[name=sms_notify][value='+data.sms_notify+']').attr('checked', 'checked');
            $('input[name=email_notify][value='+data.email_notify+']').attr('checked', 'checked');
            $('#notificationlisting').html(data.notifylist);
            $('.loading').addClass('hide');
        }
    });
}

function savenotification_alerts() {
    var subs_uuid = $('.subscriber_uuid').val();
    var subsid = $('.subscriber_userid').val();
    var sms_notify = $('input[type=radio][name=sms_notify]:checked').val();
    var email_notify = $('input[type=radio][name=email_notify]:checked').val();
    $('.loading').removeClass('hide');
    $.ajax({
        url: base_url+'user/savenotification_alerts',
        type: 'POST',
        dataType: 'json',
        data: 'uuid='+subs_uuid+'&sms_notify='+sms_notify+'&email_notify='+email_notify,
        async: false,
        success: function(data){
            $('.loading').addClass('hide');
        }
    });
}

function send_txtsms_touser() {
    var subs_uuid = $('.subscriber_uuid').val();
    var textsms_tosend = $('#textsms_tosend').val();
    $('.loading').removeClass('hide');
    if (textsms_tosend != '') {        
        $.ajax({
            url: base_url+'user/send_txtsms_touser',
            type: 'POST',
            dataType: 'json',
            data: 'uuid='+subs_uuid+'&sms_tosend='+textsms_tosend,
            async: false,
            success: function(data){
                $('.gateway_message').html(data.gateway_message);
                $('.loading').addClass('hide');
            }
        });
    }else{
        $('.loading').addClass('hide');
    }
}