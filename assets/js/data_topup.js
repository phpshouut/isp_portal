$('body').on('click', '#editclick_topup_details', function(){
    $('#topup_err').addClass('hide');
    $('#topup_err').html('');
    $('#topup_price').html('&#8377; 0');
    $('#topup_totalcost').html('&#8377; 0');
    $('input[name="topup_totalcost"]').val('0');
    $('#topup_actualcost').val('');
    $('#topup_dayscount').val('0');
    $('#applicable_days').html('-');
    var idclass = $(this).is('[disabled=disabled]');
    if(idclass !== true){
        $('.loading').removeClass('hide');
        var topup_filter = $('input[name="topuptype"]').val();
        var cust_uuid = $('.subscriber_uuid').val();
        $.ajax({
            url: base_url+'user/datatopup_check',
            type: 'POST',
            dataType: 'json',
            data: 'uuid='+cust_uuid+'&topup_filter='+topup_filter,
            async: false,
            success: function(result){
                $('#addtopup_form')[0].reset();
                $('.loading').addClass('hide');
                var plantype = result.plantype;
                var topup_inregion = result.topup_inregion;
                var active_topuplist = result.active_topuplist;
                $('#topuptimer').addClass('hide');
                
                if (topup_inregion == '0') {
                    $('.active_topup_region').addClass('hide');
                    $('.inactive_topup_region').removeClass('hide');
                }else{
                    if (plantype == '1') {
                        $('input[type="radio"][value="1"]').prop("disabled", true);
                        $('input[type="radio"][value="2"]').prop("disabled", true);
                        $('input[type="radio"][value="3"]').attr('checked', 'checked');
                        $('.topup_validate').attr('required', true);
                        $('#topuptimer').removeClass('hide');
                    }
                }
                $('select[name="active_topups"]').empty().append(active_topuplist);
            }
        });
        subscriber_topup_details();
    }
});


function assingtopup_touser() {
    $('.loading').removeClass('hide');
    $('#topup_err').addClass('hide');
    $('#topup_err').html('');
    var formdata = $('#addtopup_form').serialize();
    var cust_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/assingtopup_touser',
        type: 'POST',
        data: formdata+'&uuid='+cust_uuid,
        success: function(data){
            $('#topup_err').removeClass('hide');
            $('#topup_err').html(data);
            userassoc_topuplist();
        }
    });
}

function subscriber_topup_details() {
    $('.loading').removeClass('hide');
    var subsid = $('.subscriber_userid').val();
    var subs_uuid = $('.subscriber_uuid').val();
    
    $.ajax({
       url: base_url+'user/check_topup_authenticy',
       type: 'POST',
       data: 'userid='+subsid+'&uid='+subs_uuid,
       dataType: 'json',
       success: function(data){
            $('.loading').addClass('hide');
            if (data == 0) {
                $('.unauthorized').removeClass('hide');
                $('.onauthorized').addClass('hide');
            }else{
                $('.unauthorized').addClass('hide');
                $('.onauthorized').removeClass('hide');
                userassoc_topuplist();
            }
       }
    });
}

function userassoc_topuplist() {
    var subs_uuid = $('.subscriber_uuid').val();
    $('.loading').removeClass('hide');
    $.ajax({
       url: base_url+'user/userassoc_topuplist',
       type: 'POST',
       data: 'uuid='+subs_uuid,
       dataType: 'json',
       success: function(data){
            $('.loading').addClass('hide');
            $('#userassoc_topuplist').html(data);
       }
    });
}

function defaulttopuptab_details() {
    $('#topup_err').addClass('hide');
    $('#topup_err').html('');
    $('#topup_price').html('&#8377; 0');
    $('#topup_totalcost').html('&#8377; 0');
    $('input[name="topup_totalcost"]').val('0');
    $('#topup_actualcost').val('');
    $('#topup_dayscount').val('0');
    $('#applicable_days').html('-');
    var idclass = $(this).is('[disabled=disabled]');
    if(idclass !== true){
        $('.loading').removeClass('hide');
        var topup_filter = $('input[name="topuptype"]').val();
        var cust_uuid = $('.subscriber_uuid').val();
        $.ajax({
            url: base_url+'user/datatopup_check',
            type: 'POST',
            dataType: 'json',
            data: 'uuid='+cust_uuid+'&topup_filter='+topup_filter,
            async: false,
            success: function(result){
                $('#addtopup_form')[0].reset();
                $('.loading').addClass('hide');
                var plantype = result.plantype;
                var topup_inregion = result.topup_inregion;
                var active_topuplist = result.active_topuplist;
                $('#topuptimer').addClass('hide');
                
                if (topup_inregion == '0') {
                    $('.active_topup_region').addClass('hide');
                    $('.inactive_topup_region').removeClass('hide');
                }else{
                    if (plantype == '1') {
                        $('input[type="radio"][value="1"]').prop("disabled", true);
                        $('input[type="radio"][value="2"]').prop("disabled", true);
                        $('input[type="radio"][value="3"]').attr('checked', 'checked');
                        $('#topuptimer').removeClass('hide');
                    }
                }
                $('select[name="active_topups"]').empty().append(active_topuplist);
            }
        });
        subscriber_topup_details();
    }
}
