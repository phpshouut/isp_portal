
var filecount = 0; var filearr = 0; var filedoctype = []; var kycdocnumb = [];

$('body').on('change','.kycdocnumber', function(){
    var docnumber = $('.kycdocnumber').val();
    if (docnumber != '') {
	activate_docupload();
    }
});

function activate_docupload() {
    var option = $('select[name="kyc_documents"]').val();
    var docnumber = $('.kycdocnumber').val();
    var kyc_documents_arr = $('#kyc_documents_arr').val();
    
    if ((option == '') || (docnumber == '')) {
	$('.subsdocdiv').addClass('hide');
        $('#defaultdocdiv').removeClass('hide');
    }else{
	//$('.subsdocdiv').removeClass('hide');
	$('#subsdocuments'+filearr).parent().removeClass('hide');
	$('#defaultdocdiv').addClass('hide');
	if (kyc_documents_arr.length == 0) {
	    filearr = 1;
	    $('.subsdocdiv').remove();
	    $('.fileperoption').append('<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 subsdocdiv"><input type="file" name="subscriber_documents[]" id="subsdocuments'+filearr+'" accept="image/*" class="subscriber_documents hide" /><span onclick="triggerfile('+filearr+')" class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600">CHOOSE FILE</span></div>');
	    $( '#subsdocuments'+filearr ).trigger( 'click' );
	}
    }
}

var abc = 0; var sno = 0;
$('body').on('change', '.subscriber_documents', function(){
    if (this.files && this.files[0]) {
	sno += 1;
        $('#docpreview').append("<div id='abcd"+ abc +"' class='abcd'><center><img class='previewimg' id='previewimg" + abc + "' src='' style='width:70%'/></center></div>");
        
	
	
	var reader = new FileReader();
	reader.onload = imageIsLoaded;
	reader.readAsDataURL(this.files[0]);
        
        $('#nodocdiv').addClass('hide');
	var kdocnumb = $('.kycdocnumber').val();
        var docname = '<i class="fa fa-newspaper-o" aria-hidden="true"></i> &nbsp;'+ $('select[name="kyc_documents"]').find('option:selected').text();    
        var option = $('select[name="kyc_documents"]').val();
        filedoctype[filecount] = option;
	kycdocnumb[filecount] = kdocnumb;
	filecount += 1;
        filearr += 1;
        $('#kyc_documents_arr').val(JSON.parse(JSON.stringify(filedoctype)));
	$('#kycdocnumber_arr').val(JSON.parse(JSON.stringify(kycdocnumb)));
        $('.subsdocdiv').addClass('hide');
	
	$('#kyclisting').append('<div class="row"><div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><label>'+docname+'</label></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">'+kdocnumb+'</div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="label label-default" onclick="docpreview('+abc+')"><i class="fa fa-eye" aria-hidden="true"></i></span> <span class="label label-default" id="'+filecount+'" onclick="docdelete(this)"><i class="fa fa-trash" aria-hidden="true"></i></span> <span class="label label-default"><i class="fa fa-repeat" aria-hidden="true"></i></span></div></div></div>');
        $('.fileperoption').append('<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 subsdocdiv"><input type="file" name="subscriber_documents[]" id="subsdocuments'+filearr+'" accept="image/*" class="subscriber_documents hide" /><span onclick="triggerfile('+filearr+')" class="mui-btn mui-btn--small mui-btn--primary" style="height:28px; padding: 0px 20px; background-color:#36465f; font-weight:600">CHOOSE FILE</span></div>');
	
	$('select[name="kyc_documents"]').val('');
	$('.kycdocnumber').val('');
	
	$('select[name="kyc_documents"]').attr('required',false);
	$('.kycdocnumber').attr('required',false);
    }
});

function imageIsLoaded(e) {
    $('#previewimg' + abc).attr('src', e.target.result);
    abc += 1;
}

function docpreview(pid) {
    $('#documentModal').modal('show');
    $('.abcd').addClass('hide');
    $('.corp_abcd').addClass('hide');
    $('.idp_abcd').addClass('hide');
    $('#abcd'+pid).removeClass('hide');
}

function docdelete(elem) {
    var id = $(elem).attr("id");
    $(elem).parent().parent().remove();
    //alert(id);
    $('#subsdocuments'+id).remove();
    $('#abcd'+id).remove();
    indx = id-1;
    
    filedoctype.splice(indx,1);
    $('#kyc_documents_arr').val(JSON.parse(JSON.stringify(filedoctype)));
    kycdocnumb.splice(indx,1);
    $('#kycdocnumber_arr').val(JSON.parse(JSON.stringify(kycdocnumb)));
    filecount = filecount - 1;
}

function triggerfile(fileid) {
    $( '#subsdocuments'+fileid ).trigger( 'click' );
}